/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace ppd { namespace pool { namespace thread_types {

template<class WQ>
class steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, sequential_mode>, WQ> : public pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t>, protected non_assignable {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef batch_details<WQ> batch_type;
	typedef typename batch_type::signalled_work_queue_type signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall steal(exit_requested_type&, signalled_work_queue_type&) noexcept(true) {}

private:
	typename thread_traits::api_params_type::states __fastcall execute() {
		return thread_traits::api_params_type::no_kernel_thread;
	}
};

template<class WQ>
class steal<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>
	: public pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t>, protected non_assignable {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef batch_details<WQ> batch_type;
	typedef typename batch_type::signalled_work_queue_type signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall steal(exit_requested_type& exit_requested, signalled_work_queue_type& signalled_work_queue) noexcept(true);
	~steal() noexcept(true);

	void __fastcall process_a_batch(typename os_traits::thread_exception const& exception_thrown_in_thread) {
		batch.process_a_batch(exception_thrown_in_thread, signalled_work_queue);
	}

private:
	batch_type batch;
	signalled_work_queue_type& signalled_work_queue;

	typename thread_traits::api_params_type::states __fastcall process() noexcept(false);
};

template<class WQ>
class slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, sequential_mode>, WQ>
	: public pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t>, public sp_counter_type<thread_os_traits<generic_traits::MS_Win32, sequential_mode>::lock_traits>, protected non_assignable {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef WQ signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type&) noexcept(true)
		: base_t(exit_requested) {
	}

private:
	typename thread_traits::api_params_type::states __fastcall process() noexcept(true) {
		return thread_traits::api_params_type::no_kernel_thread;
	}
};

template<class WQ>
class slave<generic_traits::return_data::element_type::nonjoinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>
	: public pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t>, public sp_counter_type<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>::lock_traits>, protected non_assignable {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef WQ signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type& wk) noexcept(true);
	~slave() noexcept(true);

private:
	typename signalled_work_queue_type::value_type some_work;

	typename thread_traits::api_params_type::states __fastcall process() noexcept(false);
};

/// You can report back exceptions from this thread wrapper type. Oh - and make sure you construct the execution context too (because you get the exceptions through that type).
template<class WQ>
class steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, sequential_mode>, WQ>
	: public pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t> {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef WQ signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall steal(exit_requested_type&, signalled_work_queue_type&) noexcept(true) {}

private:
	typename thread_traits::api_params_type::states __fastcall process() noexcept(true) {
		return thread_traits::api_params_type::no_kernel_thread;
	}
};

/// You can report back exceptions from this thread wrapper type. Oh - and make sure you construct the execution context too (because you get the exceptions through that type).
template<class WQ>
class steal<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>
	: public pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t>, public batch_details<WQ>, protected non_copyable {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef batch_details<WQ> batch_type;
	typedef typename batch_type::signalled_work_queue_type signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall steal(exit_requested_type&, signalled_work_queue_type&) noexcept(true);
	~steal() noexcept(true);

	void __fastcall process_a_batch(typename os_traits::thread_exception const& exception_thrown_in_thread) {
		batch.process_a_batch(exception_thrown_in_thread, signalled_work_queue);
	}

private:
	batch_type batch;
	signalled_work_queue_type& signalled_work_queue;

	typename thread_traits::api_params_type::states __fastcall process() noexcept(false);
};

/**
	\todo JMG: Looks like this needs completing, as some fns are undefined! Maybe not used...
*/
template<class WQ>
class slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, sequential_mode>, WQ>
	: public pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t>, public sp_counter_type<thread_os_traits<generic_traits::MS_Win32, sequential_mode>::lock_traits>, protected non_assignable {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, sequential_mode>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef WQ signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type& wk) noexcept(true)
		: base_t(exit_requested), some_work(wk) {
	}

private:
	typename signalled_work_queue_type::value_type some_work;

	typename thread_traits::api_params_type::states __fastcall process() noexcept(true) {
		return thread_traits::api_params_type::no_kernel_thread;
	}
};

template<class WQ>
class slave<generic_traits::return_data::element_type::joinable, thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, WQ>
	: public pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t>, public sp_counter_type<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>::lock_traits>, protected non_assignable {
public:
	typedef pool_thread<thread_os_traits<generic_traits::MS_Win32, heavyweight_threading>, typename WQ::have_work_type::atomic_t> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename os_traits::thread_traits::model_type model_type;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename os_traits::thread_traits thread_traits;
	typedef WQ signalled_work_queue_type;
	typedef typename base_t::exit_requested_type exit_requested_type;

	__stdcall slave(exit_requested_type& exit_requested, typename signalled_work_queue_type::value_type& wk) noexcept(true);
	~slave() noexcept(true);

private:
	typename signalled_work_queue_type::value_type some_work;

	typename thread_traits::api_params_type::states __fastcall process() noexcept(false);
};

}}}}

#include "pool_thread_impl.hpp"
