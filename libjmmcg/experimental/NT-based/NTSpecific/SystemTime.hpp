/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include"../../../core/ttypes.hpp"
#include<iomanip>
#include<ostream>
#include<sstream>
#include<string>

namespace jmmcg {

	namespace NTUtils {

		inline tostream &SystemTime(tostream &ss) {
			SYSTEMTIME time;
			::GetSystemTime(&time);
			ss<<std::setw(2)<<std::setfill(_T('0'))<<time.wHour<<_T(':')
				<<std::setw(2)<<std::setfill(_T('0'))<<time.wMinute<<_T(':')
				<<std::setw(2)<<std::setfill(_T('0'))<<time.wSecond<<_T('.')
				<<std::setw(3)<<std::setfill(_T('0'))<<time.wMilliseconds<<_T(" - ")
				<<std::setw(2)<<std::setfill(_T('0'))<<time.wDay<<_T('/')
				<<std::setw(2)<<std::setfill(_T('0'))<<time.wMonth<<_T('/')
				<<std::setw(2)<<std::setfill(_T('0'))<<time.wYear;
			return ss;
		}

		inline tstring SystemTimeAsStr(void) {
			tstringstream ss;
			SystemTime(ss);
			return ss.str();
		}

	}

}
