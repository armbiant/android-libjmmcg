/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "LoadLibraryWrapper.hpp"

// For the Service Manager functions. (NT only.)
#include<winsvc.h>

namespace jmmcg { namespace NTUtils {

	class win_exception;

	class AFX_EXT_CLASS ServiceManipulation : virtual protected LoadLibraryWrapper {
	public:
		typedef win_exception exception_type;

		__stdcall ServiceManipulation(const TCHAR * const machine=NULL);
		__stdcall ~ServiceManipulation(void);

		void __fastcall OpenService(const TCHAR * const service_name, const DWORD access);
		inline unsigned long __fastcall ControlService(const DWORD dwControl,SERVICE_STATUS &status) noexcept(true) {if (!(*pControlService)(curr_service,dwControl,&status)) {return ::GetLastError();};return NULL;};
		inline unsigned long __fastcall StartService(const DWORD num_args,const TCHAR **args) noexcept(true) {if (!(*pStartService)(curr_service,num_args,args)) {return ::GetLastError();};return NULL;};

	private:
		typedef /*WINADVAPI*/ BOOL (WINAPI * const CloseServiceHandleType)(SC_HANDLE hSCObject);
		typedef /*WINADVAPI*/ SC_HANDLE (WINAPI * const OpenServiceType)(SC_HANDLE hSCManager,LPCTSTR lpServiceName,DWORD dwDesiredAccess);
		typedef /*WINADVAPI*/ BOOL (WINAPI * const ControlServiceType)(SC_HANDLE hService,DWORD dwControl,LPSERVICE_STATUS lpServiceStatus);
		typedef /*WINADVAPI*/ BOOL (WINAPI * const StartServiceType)(SC_HANDLE hService,DWORD dwNumServiceArgs,LPCTSTR *lpServiceArgVectors);

		const CloseServiceHandleType pCloseServiceHandle;
		const OpenServiceType pOpenService;
		const ControlServiceType pControlService;
		const StartServiceType pStartService;
		SC_HANDLE scm;
		SC_HANDLE curr_service;
		const TCHAR *curr_service_name;

		// Stop any compiler silliness...
		inline ServiceManipulation(const ServiceManipulation &);
		inline ServiceManipulation & __fastcall operator=(const ServiceManipulation &);
	};

} }
