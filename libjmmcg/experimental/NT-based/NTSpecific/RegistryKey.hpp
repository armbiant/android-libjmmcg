/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// This file uses the MFC.

#pragma once

#include <atlbase.h>

#include "../../../core/non_copyable.hpp"
#include "../../../core/ttypes.hpp"

namespace jmmcg { namespace NTUtils {

	class win_exception;

	class AFX_EXT_CLASS RegistryKey : protected non_copyable {
	public:
		typedef win_exception exception_type;

		__stdcall RegistryKey();
		__stdcall RegistryKey(HKEY p, const tstring &n);
		__stdcall ~RegistryKey();
		long __fastcall Create(HKEY p, const tstring &n);
		long __fastcall Open(HKEY parent, const tstring &n);
		long __fastcall SetValue(const tstring &val, const tstring &key_name);
		long __fastcall SetValue(const tstring &val, const tstring &key_name, const DWORD type);
		long __fastcall SetValue(const unsigned long val, const tstring &key_name);
		HKEY __fastcall hkey() noexcept(true);

	private:
		HKEY parent;
		bool delete_key;
		tstring name;
		CRegKey key;
	};

	inline
	RegistryKey::RegistryKey(void) : parent(), delete_key(false) {
	}

	inline
	RegistryKey::RegistryKey(HKEY p, const tstring &n) : parent(p), delete_key(false) {
		Create(parent, n);
	}

	inline
	HKEY RegistryKey::hkey() {
		return key.m_hKey;
	}

} }
