/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// DESCRIPTION:
// Get a GUID, return it as a string, that is _UNICODE compatible.

#pragma once

#include"../../../core/ttypes.hpp"
#include<objbase.h>
#include<string>

// This is all implemented inline in the header file for raw speed. It *must* be
// as FAST AS POSSIBLE.... 

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

	// Used to get unique names for the shared objects. These names are placed on an
	// NT-global list, that other processes can access to get a handle to the
	// underlying memory mapped file.
	typedef tchar guid_type[39];

	inline tstring __fastcall GetGUID(void) {
		GUID guid;
		::CoCreateGuid(&guid);
#ifdef _UNICODE
		guid_type szName;
		::StringFromGUID2(guid, szName, sizeof(guid_type)/sizeof(wchar_t));
#else
		wchar_t wszName[sizeof(wchar_t)*sizeof(guid_type)];
		::StringFromGUID2(guid, wszName, sizeof(wszName)/sizeof(wchar_t));
		guid_type szName;
		::WideCharToMultiByte(CP_ACP, 0, wszName, -1, szName, sizeof(guid_type), NULL, NULL);
#endif _UNICODE
		return szName;
	}

} } }
