# Microsoft Developer Studio Project File - Name="libjmmcg" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=libjmmcg - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "libjmmcg.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "libjmmcg.mak" CFG="libjmmcg - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "libjmmcg - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "libjmmcg - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "libjmmcg - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /G5 /Gr /MD /W3 /GR /GX /O2 /Ob2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x809 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /machine:I386

!ELSEIF  "$(CFG)" == "libjmmcg - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /G5 /Gr /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_WINDLL" /D "_AFXDLL" /D "_MBCS" /D "_AFXEXT" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x809 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "libjmmcg - Win32 Release"
# Name "libjmmcg - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "Core Sources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\core\peano_curve.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# End Group
# Begin Group "NT-Specific Sources"

# PROP Default_Filter ""
# Begin Group "CPU Ticker Sources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\CPUTicker\CPUTicker.cpp
# End Source File
# End Group
# Begin Group "Shared Memory Sources"

# PROP Default_Filter ""
# Begin Group "PerfMon Sources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\shared_memory\PerfMon\NTPerformanceObjects.cpp
# PROP Exclude_From_Build 1
# End Source File
# End Group
# End Group
# Begin Source File

SOURCE=..\AssertToNTLog.cpp
# End Source File
# Begin Source File

SOURCE=..\Directory.cpp
# End Source File
# Begin Source File

SOURCE=..\EventLog.cpp
# End Source File
# Begin Source File

SOURCE=..\EventLogMessages.mc

!IF  "$(CFG)" == "libjmmcg - Win32 Release"

# Begin Custom Build
InputPath=..\EventLogMessages.mc
InputName=EventLogMessages

BuildCmds= \
	mc $(InputPath)

"msg00001.bin" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)

"$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)

"$(InputName).rc" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ELSEIF  "$(CFG)" == "libjmmcg - Win32 Debug"

# Begin Custom Build
InputPath=..\EventLogMessages.mc
InputName=EventLogMessages

BuildCmds= \
	mc $(InputPath)

"msg00001.bin" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)

"$(InputName).h" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)

"$(InputName).rc" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
   $(BuildCmds)
# End Custom Build

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\..\EventLogMessages.rc
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\libjmmcg.cpp
# End Source File
# Begin Source File

SOURCE="..\Load n Save BMPs.cpp"
# End Source File
# Begin Source File

SOURCE=..\NetShare.cpp
# End Source File
# Begin Source File

SOURCE=..\RegistryKey.cpp
# End Source File
# Begin Source File

SOURCE=..\ScheduledTask.cpp
# End Source File
# Begin Source File

SOURCE=..\SecurityDescriptor.cpp
# End Source File
# Begin Source File

SOURCE=..\ServiceManipulation.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\thread_base.cpp
# ADD CPP /Yu
# End Source File
# End Group
# Begin Source File

SOURCE=.\libjmmcg.def
# End Source File
# Begin Source File

SOURCE=.\libjmmcg.rc
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Group "Core Headers"

# PROP Default_Filter ""
# Begin Group "Core Impl Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\core\cache_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\factory_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\map_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\shared_ptr_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_api_traits_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_base_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_exception_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_work_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_wrapper_impl.hpp
# End Source File
# End Group
# Begin Group "Core Threading Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\core\Locking.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\pool_thread.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_api_traits.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_base.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_client_context.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_exception.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_os_traits.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_params_traits.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_pool.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_pool_master.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_pool_sequential.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_pool_traits.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_pool_workers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_safe_collection.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_work.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\thread_wrapper.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\..\..\core\algorithm.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\auto_array_ptr.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\ave_deviation_meter.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\blatant_old_msvc_compiler_hacks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\cache.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\cmd_line_processing.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\debug_defines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\dynamic_cast.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\exception.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\factory.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\file.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\functional.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\generic_app.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\info.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\logging.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\map.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\object_ptr.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\peano_curve.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\shared_ptr.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\temp_file.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\ttypes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\core\unicode_conversions.hpp
# End Source File
# End Group
# Begin Group "NT-Specific Headers"

# PROP Default_Filter ""
# Begin Group "CPU Ticker Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\CPUTicker\CPUTicker.hpp
# End Source File
# End Group
# Begin Group "Shared Memory Headers"

# PROP Default_Filter ""
# Begin Group "PerfMon Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\shared_memory\PerfMon\NTPerformanceObjects.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\shared_memory\SharedMemory.hpp
# End Source File
# End Group
# Begin Group "NT Impl Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\pool_thread_impl.hpp
# End Source File
# Begin Source File

SOURCE=..\thread_api_traits_impl.hpp
# End Source File
# Begin Source File

SOURCE="..\XML_Stuff impl.hpp"
# End Source File
# End Group
# Begin Group "NT Threading Headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\NTLocking.hpp
# End Source File
# Begin Source File

SOURCE=..\pool_thread.hpp
# End Source File
# Begin Source File

SOURCE=..\thread_api_traits.hpp
# End Source File
# Begin Source File

SOURCE=..\thread_params_traits.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\AssertToNTLog.hpp
# End Source File
# Begin Source File

SOURCE=..\AVIStuff.hpp
# End Source File
# Begin Source File

SOURCE=..\COMStuff.hpp
# End Source File
# Begin Source File

SOURCE=..\Directory.hpp
# End Source File
# Begin Source File

SOURCE=..\DumpWinMsg.hpp
# End Source File
# Begin Source File

SOURCE=..\EventLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\EventLogMessages.h
# End Source File
# Begin Source File

SOURCE=..\exception.hpp
# End Source File
# Begin Source File

SOURCE=..\GetGUID.hpp
# End Source File
# Begin Source File

SOURCE=..\GlobalFreeDeallocator.hpp
# End Source File
# Begin Source File

SOURCE="..\Load n Save BMPs.hpp"
# End Source File
# Begin Source File

SOURCE=..\LoadLibraryWrapper.hpp
# End Source File
# Begin Source File

SOURCE=..\LocalFreeDeallocator.hpp
# End Source File
# Begin Source File

SOURCE=..\NetAPIDeallocator.hpp
# End Source File
# Begin Source File

SOURCE=..\NetShare.hpp
# End Source File
# Begin Source File

SOURCE=..\NTSecuritySettings.hpp
# End Source File
# Begin Source File

SOURCE=..\OnNT.hpp
# End Source File
# Begin Source File

SOURCE=..\RegistryKey.hpp
# End Source File
# Begin Source File

SOURCE=..\ScheduledTask.hpp
# End Source File
# Begin Source File

SOURCE=..\SecurityDescriptor.hpp
# End Source File
# Begin Source File

SOURCE=..\ServiceManipulation.hpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\SystemTime.hpp
# End Source File
# Begin Source File

SOURCE=..\XML_Stuff.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\libjmmcg.rc2
# End Source File
# Begin Source File

SOURCE=.\MSG00001.bin
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\..\..\core\config_file.xdr
# End Source File
# End Target
# End Project
