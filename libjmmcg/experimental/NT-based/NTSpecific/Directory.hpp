/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "LoadLibraryWrapper.hpp"
#include "SecurityDescriptor.hpp"

#include "../../../core/ttypes.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

const tchar dir_separator_s[]=_T("\\");
const tchar wildcard[]=_T("*");

/////////////////////////////////////////////////////////////////////////////

class AFX_EXT_CLASS Directory : virtual protected LoadLibraryWrapper {
public:
	typedef win_exception exception_type;

	__stdcall Directory(const tstring &unc_to_dir,const TCHAR * const machine,const TCHAR *user,const DWORD access);
	__stdcall ~Directory(void);

	const tstring & __fastcall UNC(void) const noexcept(true) {
		return unc;
	}

private:
	typedef /*WINADVAPI*/ BOOL (WINAPI * const SetFileSecurityType)(LPCTSTR lpFileName,SECURITY_INFORMATION SecurityInformation,PSECURITY_DESCRIPTOR pSecurityDescriptor);

	const SetFileSecurityType pSetFileSecurity;
	const tstring unc;
	SecurityDescriptor sd;

	// Stop any compiler silliness.
	Directory(void) noexcept(true);
	Directory(const Directory &) noexcept(true);
	Directory &operator=(const Directory &) noexcept(true);
};

} } }
