;/******************************************************************************
;** $Header$
;** 
;** Copyright (C) 2002 by J.M.McGuiness, coder@hussar.me.uk
;**
;** This library is free software; you can redistribute it and/or
;** modify it under the terms of the GNU Lesser General Public
;** License as published by the Free Software Foundation; either
;** version 2.1 of the License, or (at your option) any later version.
;**
;** This library is distributed in the hope that it will be useful,
;** but WITHOUT ANY WARRANTY; without even the implied warranty of
;** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;** Lesser General Public License for more details.
;**
;** You should have received a copy of the GNU Lesser General Public
;** License along with this library; if not, write to the Free Software
;** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
;*/

;// Event Log object categories and messages.
;// Other files are dependant upon this file, for example "EventLog.?pp"...
;// In "EventLog.hpp" there's abn enum called "EventLog::categories" that uses
;// the "Categories" "SymbolicName"s, immediately below. If these are modified,
;// the enum will need modification.
;// Ensure you update them!
;// This file is compled by using the command line "mc EventLogMessages.mc". This
;// outputs "msg00001.bin", "EventLogMessages.h" & "EventLogMessages.rc". The header file is included
;// by "EventLog.hpp". The message resource file "msg00001.bin" (00001 denotes the
;// system default language) is included into the dll by:
;// 1. Adding '1 11 "MSG00001.BIN"' to the "Compile-time directives" edit box in the "View\Resource Includes..." dialog.
;// 2. Or: Add the output resource file "EventLogMessages.rc" to the project.

;// Categories
MessageIdTypedef=WORD

MessageID=0x0000
SymbolicName=CAT_NONE
Language=English
None
.

MessageID=0x0001
SymbolicName=CAT_SUCCESS
Language=English
Success
.

MessageID=0x0002
SymbolicName=CAT_WARNING
Language=English
Warning
.

MessageID=0x0003
SymbolicName=CAT_ERROR
Language=English
Error
.

MessageID=0x0004
SymbolicName=CAT_CRITICAL
Language=English
Critical
.

MessageID=0x0005
SymbolicName=CAT_INFO
Language=English
Information
.

MessageID=0x0006
SymbolicName=CAT_CUSTOM1
Language=English
Custom 1
.

MessageID=0x0007
SymbolicName=CAT_CUSTOM2
Language=English
Custom 2
.

MessageID=0x0008
SymbolicName=CAT_CUSTOM3
Language=English
Custom 3
.

MessageID=0x0009
SymbolicName=CAT_CUSTOM4
Language=English
Custom 4
.

MessageID=0x000A
SymbolicName=CAT_CUSTOM5
Language=English
Custom 5
.

;// Message Strings
MessageIdTypedef=DWORD

MessageID=0x00001000
SymbolicName=DEFAULT_MSG
Language=English
%1
.
