/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace ppd { namespace private_ {

// Implementation details..... Don't look below here!!! ;)

//	"thread_base" implementation...

	template<> inline const typename thread_base<generic_traits::MS_Win32,heavyweight_threading>::thread_traits::api_params_type::suspend_count __fastcall
	thread_base<generic_traits::MS_Win32, heavyweight_threading>::suspend() noexcept(true) {
		if (this->state()<thread_traits::api_params_type::no_kernel_thread) {
			thread_params_lock.lock();
			// Critical sections don't like double-unlocking....
			bool locked=true;
			if (thread_traits::get_current_thread_id()==thread_params.id) {
				// We want to suspend ourselves - better release the lock, otherwise we'll get deadlocks.
				thread_params_lock.unlock();
				locked=false;
			}
			const thread_traits::api_params_type::suspend_count sus_ct=thread_traits::suspend(thread_params);
			if (locked) {
				thread_params_lock.unlock();
			}
			return sus_ct;
		} else {
			return 0;
		}
	}

} } }
