/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#ifdef _DEBUG

#	include "AssertToNTLog.hpp"
#	include "EventLog.hpp"

#	include "../../../core/info.hpp"
#	include "../../../core/unicode_conversions.hpp"

#	include <string>
#	include <sstream>

using namespace libjmmcg;

/////////////////////////////////////////////////////////////////////////////

namespace {
NTUtils::EventLog NTLog_D87A7F40_F129_11d4_B1AC_0001029FBE99_;
}

inline void __fastcall NTUtils::assertlog(const char* exp, const char* file_name, const unsigned long line) {
	tstringstream ss;
	ss << _T("Debug assertion failed. File: '") << StringToTString(file_name)
		<< _T("'. Line: ") << line
		<< _T(". Process handle: 0x") << ::GetCurrentProcess()
		<< _T(". Process ID: ") << ::GetCurrentProcessId()
		<< _T(". Thread handle: 0x") << ::GetCurrentThread()
		<< _T(". Thread ID: ") << ::GetCurrentThreadId()
		<< _T(". Assertion: '") << StringToTString(exp) << _T("'.");
	NTLog_D87A7F40_F129_11d4_B1AC_0001029FBE99_.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_error, ss);
	exit(255);
}

#endif _DEBUG
