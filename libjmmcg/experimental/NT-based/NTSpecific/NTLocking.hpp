/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// DESCRIPTION:
// Various classes for wrapping NT event, mutex, critical section, locked counters and semaphore objects.

#pragma once

#include "GetGUID.hpp"

// This is all implemented inline in the header file for raw speed. It *must* be
// as FAST AS POSSIBLE....

#pragma warning(push)
#pragma warning(disable:4800)	  // forcing value to bool 'true' or 'false' (performance warning)

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace NTUtils {

typedef api_lock_traits<ppd::generic_traits::MS_Win32, heavyweight_threading> lock_traits;

class recursive_anon_mutex : public lock::lockable<lock_traits>, protected non_copyable {
public:
	typedef lock::lockable<api_lock_traits<ppd::generic_traits::MS_Win32, heavyweight_threading> > base_t;
	typedef base_t::lock_traits lock_traits;
	typedef lock_traits::exception_type exception_type;
	typedef lock::in_process<recursive_anon_mutex> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<recursive_anon_mutex> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::erew_memory_access;

	explicit __stdcall recursive_anon_mutex(const SECURITY_ATTRIBUTES* const sa= NULL)
		: sec_attrib(sa),
		  mutex_(::CreateMutex(const_cast<SECURITY_ATTRIBUTES* const>(sec_attrib), false, NULL)) {
		assert(mutex_);
	}
	virtual __stdcall ~recursive_anon_mutex() noexcept(true) {
		::CloseHandle(mutex_);
	}

	const SECURITY_ATTRIBUTES* const __fastcall SA() const noexcept(true) {
		return sec_attrib;
	}
	__fastcall operator handle_type() noexcept(true) {
		return mutex_;
	}
	atomic_state_type __fastcall lock(const timeout_type period) noexcept(true) {
		return static_cast<atomic_state_type>(::WaitForSingleObject(mutex_, period));
	}
	atomic_state_type __fastcall lock() noexcept(true) {
		return lock(infinite_timeout());
	}
	atomic_state_type __fastcall unlock() noexcept(true) {
		return static_cast<atomic_state_type>(::ReleaseMutex(mutex_));
	}
	void decay() noexcept(true) {}

private:
	friend class recursive_mutex;

	const SECURITY_ATTRIBUTES* const sec_attrib;
	handle_type mutex_;

	explicit __stdcall recursive_anon_mutex(const tstring& n, const SECURITY_ATTRIBUTES* const sa)
		: sec_attrib(sa),
		  mutex_(::CreateMutex(const_cast<SECURITY_ATTRIBUTES* const>(sec_attrib), false, n.c_str())) {
	}
};

class recursive_mutex : public lock::lockable<lock_traits>, protected non_assignable {
public:
	typedef lock::lockable<api_lock_traits<ppd::generic_traits::MS_Win32, heavyweight_threading> > base_t;
	typedef base_t::lock_traits lock_traits;
	typedef lock_traits::exception_type exception_type;
	typedef lock::in_process<recursive_mutex> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<recursive_mutex> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::erew_memory_access;

	explicit __stdcall recursive_mutex(const tstring& n= _T(""), const SECURITY_ATTRIBUTES* const sa= NULL)
		: name(n == _T("") ? NTUtils::GetGUID() : n),
		  mutex_(name, sa) {
	}
	__stdcall recursive_mutex(const recursive_mutex& m)
		: name(m.name),
		  mutex_(name, m.mutex_.sec_attrib) {
	}
	virtual __stdcall ~recursive_mutex() noexcept(true) {
	}

	const tstring& __fastcall Name() const noexcept(true) {
		return name;
	}
	const SECURITY_ATTRIBUTES* const __fastcall SA() const noexcept(true) {
		return mutex_.SA();
	}
	__fastcall operator handle_type() noexcept(true) {
		return mutex_.operator handle_type();
	}
	atomic_state_type __fastcall lock(const timeout_type period) noexcept(true) {
		return mutex_.lock(period);
	}
	atomic_state_type __fastcall unlock() noexcept(true) {
		return mutex_.unlock();
	}
	static const atomic_state_type __fastcall lock(const tstring& name, const timeout_type period, const SECURITY_ATTRIBUTES* const sa= NULL) {
		recursive_mutex m(name, sa);
		return m.lock(period);
	}
	atomic_state_type __fastcall lock() noexcept(true) {
		return lock(infinite_timeout());
	}
	static atomic_state_type __fastcall unlock(const tstring& name, const SECURITY_ATTRIBUTES* const sa= NULL) {
		recursive_mutex m(name, sa);
		return m.unlock();
	}
	void decay() noexcept(true) {}

private:
	const tstring name;
	recursive_anon_mutex mutex_;
};

class anon_event : public lock::lockable_settable<lock_traits>, protected non_copyable {
public:
	typedef lock::lockable_settable<api_lock_traits<ppd::generic_traits::MS_Win32, heavyweight_threading> > base_t;
	typedef base_t::lock_traits lock_traits;
	typedef int count_type;
	typedef lock_traits::exception_type exception_type;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef atomic_state_type lock_result_type;
	typedef lock::in_process<anon_event> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;

	/**
		Manual reset and initially signalled.
	*/
	explicit __stdcall anon_event(const atomic_state_type state= atom_set, const SECURITY_ATTRIBUTES* const sa= NULL) noexcept(true)
		: sec_attrib(sa),
		  event_(::CreateEvent(const_cast<SECURITY_ATTRIBUTES* const>(sec_attrib), true, state == atom_set, NULL)) {
		assert(event_);
	}
	virtual __stdcall ~anon_event() noexcept(true) {
		::CloseHandle(event_);
	}

	const SECURITY_ATTRIBUTES* const __fastcall SA() const noexcept(true) {
		return sec_attrib;
	}
	__fastcall operator handle_type() noexcept(true) {
		return event_;
	}
	atomic_state_type __fastcall set() noexcept(true) {
		return static_cast<atomic_state_type>(::SetEvent(event_));
	}
	atomic_state_type __fastcall reset() noexcept(true) {
		return unlock();
	}
	lock_result_type __fastcall lock() noexcept(true) {
		return static_cast<atomic_state_type>(::WaitForSingleObject(event_, infinite_timeout()));
	}
	lock_result_type __fastcall lock(const timeout_type period) noexcept(true) {
		return static_cast<atomic_state_type>(::WaitForSingleObject(event_, period));
	}
	lock_result_type __fastcall unlock() noexcept(true) {
		return static_cast<atomic_state_type>(::ResetEvent(event_));
	}
	void clear() noexcept(true);
	void decay() noexcept(true) {}

private:
	friend class event;

	const SECURITY_ATTRIBUTES* const sec_attrib;
	handle_type event_;

	__stdcall anon_event(const atomic_state_type state, const tstring& n, const SECURITY_ATTRIBUTES* const sa) noexcept(true)
		: sec_attrib(sa),
		  event_(::CreateEvent(const_cast<SECURITY_ATTRIBUTES* const>(sec_attrib), true, state == atom_set, n.c_str())) {
	}
};

class event : public lock::lockable_settable<lock_traits>, protected non_assignable {
public:
	typedef lock::lockable_settable<api_lock_traits<ppd::generic_traits::MS_Win32, heavyweight_threading> > base_t;
	typedef base_t::lock_traits lock_traits;
	typedef int count_type;
	typedef lock_traits::exception_type exception_type;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef atomic_state_type lock_result_type;
	typedef lock::in_process<anon_event> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;

	/**
		Manual reset and initially signalled.
	*/
	explicit __stdcall event(const atomic_state_type s= atom_set, const tstring& n= _T(""), const SECURITY_ATTRIBUTES* const sa= NULL)
		: name(n == _T("") ? NTUtils::GetGUID() : n),
		  state(s),
		  event_(state, name, sa) {
	}
	__stdcall event(const event& ev)
		: name(ev.name),
		  state(ev.state),
		  event_(state, name, ev.event_.sec_attrib) {
	}
	virtual __stdcall ~event() noexcept(true) {
	}

	const tstring& __fastcall Name() const noexcept(true) {
		return name;
	}
	const SECURITY_ATTRIBUTES* const __fastcall SA() const noexcept(true) {
		return event_.SA();
	}
	__fastcall operator handle_type() noexcept(true) {
		return event_.operator handle_type();
	}
	atomic_state_type __fastcall set() noexcept(true) {
		return event_.set();
	}
	atomic_state_type __fastcall reset() noexcept(true) {
		return unlock();
	}
	static atomic_state_type __fastcall set(const tstring& name, const SECURITY_ATTRIBUTES* const sa= NULL) {
		event e(atom_unset, name, sa);
		return e.set();
	}
	static atomic_state_type __fastcall reset(const tstring& name, const SECURITY_ATTRIBUTES* const sa= NULL) {
		return unlock(name, sa);
	}
	lock_result_type __fastcall lock() noexcept(true) {
		return event_.lock();
	}
	lock_result_type __fastcall lock(const timeout_type period) noexcept(true) {
		return event_.lock(period);
	}
	lock_result_type __fastcall unlock() noexcept(true) {
		return event_.unlock();
	}
	static lock_result_type __fastcall lock(const tstring& name, const timeout_type period, const SECURITY_ATTRIBUTES* const sa= NULL) {
		event e(atom_unset, name, sa);
		return e.lock(period);
	}
	static lock_result_type __fastcall unlock(const tstring& name, const SECURITY_ATTRIBUTES* const sa) noexcept(true) {
		event e(atom_unset, name, sa);
		return e.unlock();
	}
	void decay() noexcept(true) {}

private:
	const tstring name;
	const atomic_state_type state;
	anon_event event_;
};

/// Note that Windows NT critical sections cannot be used across processes, as they are un-named. You'll need to use an "event" or "mutex".
class recursive_critical_section : public ppd::lock::lockable<lock_traits>, protected non_copyable {
public:
	typedef lock::lockable<api_lock_traits<ppd::generic_traits::MS_Win32, heavyweight_threading> > base_t;
	typedef base_t::lock_traits lock_traits;
	typedef lock_traits::exception_type exception_type;
	typedef lock::in_process<recursive_critical_section> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef lock::in_process_unlockable<recursive_critical_section> unlockable_type;
	typedef unlockable_type read_unlockable_type;
	typedef unlockable_type write_unlockable_type;
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::erew_memory_access;

	__stdcall recursive_critical_section() noexcept(true) {
		::InitializeCriticalSection(&crit_section);
	}
	explicit __stdcall recursive_critical_section(const timeout_type) noexcept(true) {
		::InitializeCriticalSection(&crit_section);
	}
	__stdcall ~recursive_critical_section() noexcept(true) {
		::DeleteCriticalSection(&crit_section);
	}

	__fastcall operator CRITICAL_SECTION*() noexcept(true) {
		return &crit_section;
	}
	atomic_state_type __fastcall lock(const timeout_type) noexcept(true) {
		::EnterCriticalSection(&crit_section);
		return atom_set;
	}
	atomic_state_type __fastcall lock() noexcept(true) {
		::EnterCriticalSection(&crit_section);
		return atom_set;
	}
	atomic_state_type __fastcall unlock() noexcept(true) {
		::LeaveCriticalSection(&crit_section);
		return atom_unset;
	}
	void decay() noexcept(true) {}

private:
	CRITICAL_SECTION crit_section;
};

class semaphore : public lock::lockable<lock_traits>, protected non_copyable {
public:
	typedef lock::lockable<api_lock_traits<ppd::generic_traits::MS_Win32, heavyweight_threading> > base_t;
	typedef int count_type;
	typedef base_t::lock_traits lock_traits;
	typedef base_t::atomic_state_type atomic_state_type;
	typedef lock::in_process<semaphore> lock_type;
	typedef lock_type read_lock_type;
	typedef lock_type write_lock_type;
	typedef atomic_state_type lock_result_type;

	__stdcall semaphore(const long max_count, const long init_count, tchar const* name) noexcept(true)
		: sema(::CreateSemaphore(NULL, init_count, max_count, name)) {
	}
	__stdcall ~semaphore() noexcept(true) {
		::CloseHandle(sema);
	}

	__fastcall operator handle_type() {
		return sema;
	}

	atomic_state_type __fastcall lock(const timeout_type period) noexcept(true) {
		return static_cast<atomic_state_type>(::WaitForSingleObject(sema, period));
	}
	atomic_state_type __fastcall unlock(const long decrement= 1, long* prev_count= NULL) noexcept(true) {
		return static_cast<atomic_state_type>(::ReleaseSemaphore(sema, decrement, prev_count));
	}

private:
	handle_type sema;
};

class anon_semaphore : public semaphore {
public:
	__stdcall anon_semaphore(const long int max_count, const long int init_count)
		: semaphore(max_count, init_count, NULL) {}
};

}}}}

#pragma warning(default:4800)
#pragma warning(pop)
