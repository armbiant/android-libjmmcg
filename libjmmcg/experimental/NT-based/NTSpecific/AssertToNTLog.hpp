/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include<cassert>

/*
	NOTE: All of this functionality is enabled ONLY in debug mode, i.e. if
	"_DEBUG" is defined.
	Handy for using in NT Services that don't have a user interface - send the
	assertion to the NT Event Log.
	Automatically logs the file and line number. Also the calling thread handle.
	(Which is handy when multi-thread/process debugging!)
	Include this file instead of <cassert> or <assert.h> if you want to log all
	"assert(...)" expressions to the NT Event Log. (But only if you define the
	macro constant "ASSERTIONS_TO_NT_LOG".)
	Use "assert(...)" if you don't have an "EventLog" object in scope, otherwise
	you can use "AssertLog(...)" instead which will make use of your provided
	EventLog object.
*/

#ifdef _DEBUG

	namespace jmmcg {
	
	namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

		inline AFX_EXT_CLASS void __fastcall assertlog(const char *exp, const char *file_name, const unsigned long line);

	} }

#	define AssertLog(exp) (void)( (exp) || (jmmcg::LIBJMMCG_VER_NAMESPACE::NTUtils::assertlog(#exp, __FILE__, __LINE__), 0) )
#	ifdef JMMCG_ASSERTIONS_TO_NT_LOG
#		ifdef assert
#			undef assert
#		endif assert
#		define assert(exp) AssertLog(exp)
#	endif JMMCG_ASSERTIONS_TO_NT_LOG

#else
#	define AssertLog(assertion) ((void)0)
#endif _DEBUG
