/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include "LoadLibraryWrapper.hpp"

namespace jmmcg { namespace NTUtils {

	class win_exception;

	class ACL_wrapper : virtual protected LoadLibraryWrapper {
	public:
		__stdcall ACL_wrapper();
		__stdcall ACL_wrapper(const unsigned long acl_size);
		__stdcall ACL_wrapper(ACL_wrapper &);
		__stdcall ~ACL_wrapper();
		ACL_wrapper & __fastcall operator=(ACL_wrapper &);

		void __fastcall copy(const ACL_wrapper &) noexcept(true);
		bool __fastcall initialize() noexcept(true);
		bool __fastcall add_ACE(const DWORD access_mask,SID *sid) noexcept(true);

		unsigned long size() const noexcept(true);
		const ACL * __fastcall get() const noexcept(true);
		ACL * __fastcall get() noexcept(true);

	private:
		typedef /*WINADVAPI*/ BOOL (WINAPI * const InitializeAclType)(PACL pAcl,DWORD nAclLength,DWORD dwAclRevision);
		typedef /*WINADVAPI*/ BOOL (WINAPI * const AddAccessAllowedAceType)(PACL pAcl,DWORD dwAceRevision,DWORD AccessMask,PSID pSid);
	#ifdef _DEBUG
		typedef /*WINADVAPI*/ BOOL (WINAPI * const IsValidAclType)(PACL pAcl);
	#endif

		const InitializeAclType pInitializeAcl;
		const AddAccessAllowedAceType pAddAccessAllowedAce;
	#ifdef _DEBUG
		const IsValidAclType pIsValidAcl;
	#endif

		unsigned long size_;
		std::auto_ptr<BYTE> buff;
	};

	class SID_wrapper {
	public:
		__stdcall SID_wrapper(const unsigned int sid_size=96)
			: buff(new BYTE[sid_size]) {
		}
		__stdcall SID_wrapper(const SID_wrapper &sw)
			: buff(new BYTE[GetLengthSid(const_cast<SID_wrapper &>(sw).get())]) {
			CopySid(GetLengthSid(const_cast<SID_wrapper &>(sw).get()),get(),const_cast<SID_wrapper &>(sw).get()); 
		}
		__stdcall ~SID_wrapper() {
		}

		const SID * __fastcall get() const noexcept(true) {
			return reinterpret_cast<const SID *>(buff.get());
		}
		SID * __fastcall get() noexcept(true) {
			return reinterpret_cast<SID *>(buff.get());
		}

	private:
		const std::auto_ptr<BYTE> buff;

		SID_wrapper & __fastcall operator=(const SID_wrapper &);
	};

	class AFX_EXT_CLASS SecurityDescriptor : virtual protected LoadLibraryWrapper {
	public:
		typedef win_exception exception_type;

		__stdcall SecurityDescriptor();
		__stdcall ~SecurityDescriptor();

		const SECURITY_DESCRIPTOR & __fastcall SD() const noexcept(true) {
			return sd;
		}
		unsigned long __fastcall Allow(const TCHAR * const machine,const TCHAR * const username,const DWORD access_mask) noexcept(true);

	private:
		typedef /*WINADVAPI*/ BOOL (WINAPI * const LookupAccountNameType)(LPCTSTR lpSystemName,LPCTSTR lpAccountName,PSID Sid,LPDWORD cbSid,LPTSTR ReferencedDomainName,LPDWORD cbReferencedDomainName,PSID_NAME_USE peUse);
		typedef /*WINADVAPI*/ BOOL (WINAPI * const InitializeSecurityDescriptorType)(PSECURITY_DESCRIPTOR pSecurityDescriptor,DWORD dwRevision);
		typedef /*WINADVAPI*/ BOOL (WINAPI * const SetSecurityDescriptorDaclType)(PSECURITY_DESCRIPTOR pSecurityDescriptor,BOOL bDaclPresent,PACL pDacl,BOOL bDaclDefaulted);
		typedef /*WINADVAPI*/ DWORD (WINAPI * const GetLengthSidType)(PSID pSid);
	#ifdef _DEBUG
		typedef /*WINADVAPI*/ BOOL (WINAPI * const IsValidSidType)(PSID pSid);
		typedef /*WINADVAPI*/ BOOL (WINAPI * const IsValidSecurityDescriptorType)(PSECURITY_DESCRIPTOR pSecurityDescriptor);
	#endif

		const LookupAccountNameType pLookupAccountName;
		const InitializeSecurityDescriptorType pInitializeSecurityDescriptor;
		const SetSecurityDescriptorDaclType pSetSecurityDescriptorDacl;
		const GetLengthSidType pGetLengthSid;
	#ifdef _DEBUG
		const IsValidSidType pIsValidSid;
		const IsValidSecurityDescriptorType pIsValidSecurityDescriptor;
	#endif

		std::vector<SID *> sids;
		SECURITY_DESCRIPTOR sd;
		ACL_wrapper acl;

		// Stop any compiler silliness...
		SecurityDescriptor(const SecurityDescriptor &);
		SecurityDescriptor & __fastcall operator=(const SecurityDescriptor &);
	};

} }
