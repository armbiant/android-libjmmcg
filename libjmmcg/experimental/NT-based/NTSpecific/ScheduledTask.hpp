/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "LoadLibraryWrapper.hpp"

#include "../../../core/ttypes.hpp"

// Networking functionality such as browsing.
#include<lm.h>
// AT job scheduling structure.
#include<lmat.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

	class LoadLibraryWrapper;

	class AFX_EXT_CLASS ScheduledTask : virtual protected LoadLibraryWrapper {
	public:
		__stdcall ScheduledTask(void);
		__stdcall ~ScheduledTask(void);

		bool __fastcall AddJob(const std::wstring &machine,const AT_INFO &at,unsigned long &id);
		bool __fastcall DeleteJob(const std::wstring &machine,const unsigned long id);
		static tstring __fastcall Time(const unsigned long) noexcept(true);

	private:
		typedef NET_API_STATUS (NET_API_FUNCTION * const NetScheduleJobAddType)(IN LPCWSTR Servername OPTIONAL,IN LPBYTE Buffer,OUT LPDWORD JobId);
		typedef NET_API_STATUS (NET_API_FUNCTION * const NetScheduleJobDelType)(IN LPCWSTR Servername OPTIONAL,IN DWORD MinJobId,IN DWORD MaxJobId);
		typedef std::vector<std::pair<std::wstring, unsigned long > > jobs_list_type;

		template <class A,class B> struct SameObj : public std::binary_function<A,B,bool> {
			inline bool __fastcall operator()(const A& x, const B& y) const {
				return !x.first.compare(y.first) && x.second==y.second;
			}
		};

		const NetScheduleJobAddType pNetScheduleJobAdd;
		const NetScheduleJobDelType pNetScheduleJobDel;

		jobs_list_type jobs;

		// Stop any compiler silliness...
		inline ScheduledTask(const ScheduledTask &);
		inline ScheduledTask & __fastcall operator=(const ScheduledTask &);
	};

} } }
