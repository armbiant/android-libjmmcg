/******************************************************************************
** Copyright � 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "../../../core/exception.hpp"
#include "../../../core/thread_base.hpp"

// Implementation details..... Don't look below here!!! ;)

using namespace libjmmcg;

//	"thread_base" implementation...

template<>
__declspec(dllexport) ppd::private_::thread_base<ppd::generic_traits::MS_Win32, ppd::sequential_mode>::thread_traits::api_params_type::core_work_fn_ret_t __stdcall ppd::private_::thread_base<ppd::generic_traits::MS_Win32, ppd::sequential_mode>::core_work_fn(thread_traits::api_params_type::core_work_fn_arg_t ptr) {
	assert(ptr);
	thread_base<generic_traits::MS_Win32, sequential_mode>* const pthis= reinterpret_cast<thread_base<generic_traits::MS_Win32, sequential_mode>*>(ptr);
	assert(pthis);
	if(pthis) {
		try {
			const api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::states ret= pthis->process_chk();
			return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(ret);
		} catch(const os_traits::thread_exception::value_type& e) {
			assert(!"JMMCG-exception caught.");
			pthis->exception_thrown_in_thread.set(os_traits::thread_exception::value_type(StringToTString(e.what()), info::function(__LINE__, __PRETTY_FUNCTION__, typeid(core_work_fn), info::function::argument(_T("void *"), tostring(ptr))), JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER))));
			return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::jmmcg_exception);
		} catch(const std::exception& e) {
			assert(!"STL-exception caught.");
			pthis->exception_thrown_in_thread.set(os_traits::thread_exception::value_type(StringToTString(e.what()), info::function(__LINE__, __PRETTY_FUNCTION__, typeid(core_work_fn), info::function::argument(_T("void *"), tostring(ptr))), JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER))));
			return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::stl_exception);
		} catch(...) {
			assert(!"Unknown exception caught.");
			pthis->exception_thrown_in_thread.set(os_traits::thread_exception::value_type(_T("Unknown exception caught."), info::function(__LINE__, __PRETTY_FUNCTION__, typeid(core_work_fn), info::function::argument(_T("void *"), tostring(ptr))), JMMCG_REVISION_HDR(_T(LIBJMMCG_VERSION_NUMBER))));
		}
		return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::unknown_exception);
	}
	// In reality, no errors are actively reported to the user for this case, as the pthis was null.
	// So we almost silently fail, and hope that they aren't sooooo dumb as to not notice that their work isn't being processed...
	// This is because the only "opportunity" I have to report it is as an exception in the destructor. Hooo-boy would that be evil...
	// Just think of the memory leaks.... So I don't do that. That's the breaks....
	return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<generic_traits::MS_Win32, sequential_mode>::api_params_type::null_this_pointer);
}

template<>
__declspec(dllexport) ppd::private_::thread_base<ppd::generic_traits::MS_Win32, ppd::heavyweight_threading>::thread_traits::api_params_type::core_work_fn_ret_t __stdcall ppd::private_::thread_base<ppd::generic_traits::MS_Win32, ppd::heavyweight_threading>::core_work_fn(thread_traits::api_params_type::core_work_fn_arg_t ptr) {
	assert(ptr);
	thread_base<generic_traits::MS_Win32, heavyweight_threading>* const pthis= reinterpret_cast<thread_base<generic_traits::MS_Win32, heavyweight_threading>*>(ptr);
	assert(pthis);
	if(pthis) {
		try {
			const api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::api_params_type::states ret= pthis->process_chk();
			return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(ret);
		} catch(...) {
			BOOST_LOG_TRIVIAL(trace) << "Unknown exception caught. If it is a SIGSEGV, then the ss value in the thread_params ctor may need increasing. Details: " << boost::current_exception_diagnostic_information();
			auto ex= std::current_exception();
			if(ex) {
				pthis->exception_thrown_in_thread.set(ex);
			}
		}
		return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::api_params_type::unknown_exception);
	}
	// In reality, no errors are actively reported to the user for this case, as the pthis was null.
	// So we almost silently fail, and hope that they aren't sooooo dumb as to not notice that their work isn't being processed...
	// This is because the only "opportunity" I have to report it is as an exception in the destructor. Hooo-boy would that be evil...
	// Just think of the memory leaks.... So I don't do that. That's the breaks....
	return static_cast<thread_traits::api_params_type::core_work_fn_ret_t>(api_threading_traits<generic_traits::MS_Win32, heavyweight_threading>::api_params_type::null_this_pointer);
}
