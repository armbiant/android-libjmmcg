/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#include "EventLog.hpp"

#include "../../../core/exception.hpp"

using namespace libjmmcg;
using namespace libNTUtils;

/////////////////////////////////////////////////////////////////////////////

namespace {
const TCHAR* error_msgs[]= {
	_T("Event log error."),
	_T("Failed to register event log source."),
	_T("Failed to get module name."),
	_T("Failed to log event: ")};
}

// NOTE: This string is the embedded name of this dll!!! Don't change it!!!
// I need this because I can't get the name of this dll at run-time.
// "GetModuleHandle(...)" gives the name of the calling process if NULL is
// supplied which is not what I want.
const TCHAR module_name[]= _T("ntutils");
// More constants....
const TCHAR event_log_reg_path[]= _T("SYSTEM\\CurrentControlSet\\Services\\EventLog\\");
const TCHAR event_msg_file_str[]= _T("EventMessageFile");
const TCHAR event_types_supported_str[]= _T("TypesSupported");

/////////////////////////////////////////////////////////////////////////////

inline EventLog::EventLog(const tstring& event_src_name, const unsigned int types, const tstring& server, const tstring& appln_log)
	: event_src(NULL) {
	Create(event_src_name, types, server, appln_log);
}

inline void
EventLog::Create(const tstring& event_src_name, const unsigned int types, const tstring& server, const tstring& appln_log) {
	if(key.Create(HKEY_LOCAL_MACHINE, event_log_reg_path + appln_log + _T("\\") + event_src_name) == ERROR_SUCCESS) {
		TCHAR mod_name[MAX_PATH];
		if(GetModuleFileName(GetModuleHandle(module_name), mod_name, MAX_PATH)) {
			key.SetValue(mod_name, event_msg_file_str);
			key.SetValue(static_cast<unsigned long>(types), event_types_supported_str);
			if(!(event_src= ::RegisterEventSource(server.c_str(), event_src_name.c_str()))) {
				info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&EventLog::Create), info::function::argument(_T("const tstring &event_src_name"), event_src_name));
				fn.add_arg(_T("const unsigned int types"), tostring(types));
				fn.add_arg(_T("const tstring &server"), server);
				fn.add_arg(_T("const tstring &appln_log"), appln_log);
				throw exception_type(error_msgs[1], fn, __REV_INFO__);
			}
			assert(event_src);
		} else {
			info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&EventLog::Create), info::function::argument(_T("const tstring &event_src_name"), event_src_name));
			fn.add_arg(_T("const unsigned int types"), tostring(types));
			fn.add_arg(_T("const tstring &server"), server);
			fn.add_arg(_T("const tstring &appln_log"), appln_log);
			throw exception_type(error_msgs[2], fn, __REV_INFO__);
		}
	}
}

inline void
EventLog::Log(const error_types wType, const categories wCategory, const tstring& str, const unsigned long dwDataSize, void* const lpRawData, SID* const lpUserSid) {
	assert(event_src);
	const TCHAR* tmp= str.c_str();
	if(!::ReportEvent(event_src, (WORD)wType, (WORD)wCategory, DEFAULT_MSG, lpUserSid, 1, dwDataSize, &tmp, lpRawData)) {
		info::function fn(__LINE__, __PRETTY_FUNCTION__, typeid(&EventLog::Log), info::function::argument(_T("const error_types wType"), tostring(wType)));
		fn.add_arg(_T("const categories wCategory"), tostring(wCategory));
		fn.add_arg(_T("const tstring &str"), str);
		fn.add_arg(_T("const unsigned long dwDataSize"), tostring(dwDataSize));
		fn.add_arg(_T("void * const lpRawData"), tostring(lpRawData));
		fn.add_arg(_T("SID * const lpUserSid"), tostring(lpUserSid));
		throw exception_type(error_msgs[3], fn, __REV_INFO__);
	}
}
