/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <boost/stacktrace.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace NTUtils {
class win_exception;
}

namespace ppd {

namespace NTUtils {
class recursive_anon_mutex;
class anon_event;
class recursive_critical_section;
class recursive_mutex;
class event;
class semaphore;
}

template<>
struct api_lock_traits<generic_traits::MS_Win32, heavyweight_threading> {
	static inline constexpr const generic_traits::api_type::element_type api_type= generic_traits::MS_Win32;
	typedef heavyweight_threading model_type;
	enum atomic_state_type {
		atom_set= WAIT_OBJECT_0,
		atom_abandoned= WAIT_ABANDONED,
		atom_already_set,
		atom_max_recurse,
		atom_deadlocked,
		atom_perm_error,
		atom_failed= WAIT_FAILED,
		atom_interrupted,
		atom_unset= WAIT_TIMEOUT,
		atom_errno
	};

	typedef ppd::NTUtils::recursive_anon_mutex anon_mutex_type;
	// TODO implement the non-recursive one.
	typedef ppd::NTUtils::recursive_anon_mutex nonrecursive_anon_mutex_type;
	typedef ppd::NTUtils::recursive_anon_mutex recursive_anon_mutex_type;
	typedef ppd::NTUtils::anon_event anon_event_type;
	typedef ppd::NTUtils::recursive_critical_section critical_section_type;
	typedef ppd::NTUtils::recursive_critical_section recursive_critical_section_type;
	typedef ppd::NTUtils::recursive_mutex mutex_type;
	typedef ppd::NTUtils::recursive_mutex recursive_mutex_type;
	typedef ppd::NTUtils::event event_type;
	typedef ppd::NTUtils::anon_semaphore anon_semaphore_type;
	typedef ppd::NTUtils::semaphore semaphore_type;
	template<class V>
	struct atomic_counter_type : atomic_ctr<V, api_lock_traits<api_type, model_type> > {
		typedef atomic_ctr<V, api_lock_traits<api_type, model_type> > base_t;
		typedef typename base_t::lock_traits lock_traits;
		typedef typename base_t::atomic_t atomic_t;
		typedef typename base_t::value_type value_type;

		atomic_counter_type()
			: base_t() {}
		explicit atomic_counter_type(const value_type v)
			: base_t(v) {}
		atomic_counter_type(atomic_counter_type const& a)
			: base_t(a) {}
		atomic_counter_type(base_t const& a)
			: base_t(a) {}
	};
	typedef HANDLE handle_type;
	typedef DWORD timeout_type;

	typedef NTUtils::win_exception exception_type;

	static const timeout_type __fastcall infinite_timeout() noexcept(true) {
		return static_cast<timeout_type>(INFINITE);
	}
};

template<>
struct api_threading_traits<generic_traits::api_type::no_api, sequential_mode> : api_threading_traits<generic_traits::MS_Win32, sequential_mode> {};

}
}}

#include "thread_api_traits_impl.hpp"
