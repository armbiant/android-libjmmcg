/******************************************************************************
** Copyright � 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// DESCRIPTION:
// This bit of code was copied from an article called:
// "Bitmap & Palette - Drawing a bitmap from a BMP file" by
// Zafir Anjum (05/97). I found it at <a href="http://www.dsp.net/zafir/bitmap/draw_bmp.html"/>
// e-mail zafir@dsp.com .
//
// Variations on the BitBlt theme.
//

// This file uses the MFC.

#include "stdafx.h"

#include "Load n Save BMPs.hpp"

#include <boost/scoped_array.hpp>

/////////////////////////////////////////////////////////////////////////////

const WORD current_version=0x300;
const unsigned short max_entries=256;
const WORD bitmap_type_id=((WORD)('M'<<8)|'B');

/////////////////////////////////////////////////////////////////////////////

using namespace libjmmcg;
using namespace NTUtils;

// LoadBMP	- Loads a BMP file and creates a logical palette for it..
// Returns	- TRUE for success.
// BMPFile	- Full path of the BMP file.
// hDIB		- Pointer to a HGLOBAL variable to hold the loaded bitmap.
// Pal		- Will hold the logical palette.

bool
NTUtils::LoadBMP(const tstring &BMPFile,BitMapInfoWrapper &hDIB,std::auto_ptr<CPalette> &Pal) {
	register CFile file_t(BMPFile.c_str(),CFile::modeRead|CFile::shareDenyWrite);
	return LoadBMP(file_t,hDIB,Pal);
}

bool
NTUtils::LoadBMP(CFile &file_t,BitMapInfoWrapper &hDIB,std::auto_ptr<CPalette> &Pal) {
	BITMAPFILEHEADER bmfHeader;
	// Read file header
	if (file_t.Read(reinterpret_cast<char *>(&bmfHeader), sizeof(BITMAPFILEHEADER)) != sizeof(BITMAPFILEHEADER)) {
		return true;
	}
	// File type should be 'BM'
	if (bmfHeader.bfType!=bitmap_type_id) {
		return true;
	}
	BitMapInfoWrapper biw(bmfHeader.bfSize);
	hDIB=biw;
	// Read the remainder of the bitmap file.
	if (file_t.Read(reinterpret_cast<char *>(hDIB.Info()),bmfHeader.bfSize-sizeof(BITMAPFILEHEADER))!=(bmfHeader.bfSize-sizeof(BITMAPFILEHEADER))) {
		return true;
	}
	register DWORD tmp=(hDIB.Info()->bmiHeader.biClrUsed ? hDIB.Info()->bmiHeader.biClrUsed : (1 << (hDIB.Info()->bmiHeader.biBitCount-1)));
	if (tmp>USHRT_MAX) {
		Pal=std::auto_ptr<CPalette>();
		return false;
	}
	Pal=std::auto_ptr<CPalette>(new CPalette);
	WORD nColors;
	// Create the palette
	if ((nColors=static_cast<WORD>(tmp))<=max_entries && nColors) {
		const std::auto_ptr<BYTE> buff(new BYTE[sizeof(LOGPALETTE)+(sizeof(PALETTEENTRY)*nColors)]);
		register LOGPALETTE * const pLP=reinterpret_cast<LOGPALETTE *>(buff.get());
		if (!pLP) {
			Pal=std::auto_ptr<CPalette>();
			return true;
		}
		pLP->palVersion = current_version;
		pLP->palNumEntries = nColors;
		for (register unsigned int i=0;i<nColors;++i) {
			pLP->palPalEntry[i].peRed=hDIB.Info()->bmiColors[i].rgbRed;
			pLP->palPalEntry[i].peGreen=hDIB.Info()->bmiColors[i].rgbGreen;
			pLP->palPalEntry[i].peBlue=hDIB.Info()->bmiColors[i].rgbBlue;
			pLP->palPalEntry[i].peFlags=0;
		}
		Pal->CreatePalette(pLP);
	}
	return false;
}

NTUtils::BitMapInfoWrapper
NTUtils::CreateBitmapInfoStruct(const CBitmap &Bmp) {
	BITMAP bmp;
	/* Retrieve the bitmap's color format, width, and height. */
	if (!Bmp.GetObject(sizeof(BITMAP),reinterpret_cast<void *>(&bmp))) {
		// Gak, choke, splutter....!
		return *reinterpret_cast<NTUtils::BitMapInfoWrapper *>(NULL);
	}
	/* Convert the color format to a count of bits. */
	WORD cClrBits=static_cast<WORD>(bmp.bmPlanes*bmp.bmBitsPixel);
	if (cClrBits==1) {
		cClrBits=1;
	} else if (cClrBits<=4) {
		cClrBits=4;
	} else if (cClrBits<=8) {
		cClrBits=8;
	} else if (cClrBits<=16) {
		cClrBits=16;
	} else if (cClrBits<=24) {
		cClrBits=24;
	} else {
		cClrBits=32;
	}
	/*
	* Allocate memory for the BITMAPINFO structure. (This structure
	* contains a BITMAPINFOHEADER structure and an array of RGBQUAD data
	* structures.)
	*/
	BitMapInfoWrapper bmi;
	if (cClrBits!=24) {
		BitMapInfoWrapper biw(sizeof(RGBQUAD)*(2^cClrBits));
		bmi=biw;
	/*
	* There is no RGBQUAD array for the 24-bit-per-pixel format.
	*/
	}
	/* Initialize the fields in the BITMAPINFO structure. */
	bmi.Info()->bmiHeader.biWidth=bmp.bmWidth;
	bmi.Info()->bmiHeader.biHeight=bmp.bmHeight;
	bmi.Info()->bmiHeader.biPlanes=bmp.bmPlanes;
	bmi.Info()->bmiHeader.biBitCount=bmp.bmBitsPixel;
	if (cClrBits<24) {
		bmi.Info()->bmiHeader.biClrUsed=2^cClrBits;
	}
	/*
	* Compute the number of bytes in the array of color
	* indices and store the result in biSizeImage.
	*/
	bmi.Info()->bmiHeader.biSizeImage=(bmi.Info()->bmiHeader.biWidth+7)/8*bmi.Info()->bmiHeader.biHeight*cClrBits;
	return bmi;
}

bool
NTUtils::CreateBMPFile(const tstring &pszFile,const CBitmap &BMP,const CDC &DC) {
	register CFile file_t(pszFile.c_str(),CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite);
	register bool ret=CreateBMPFile(file_t,BMP,DC);
	file_t.Close();
	return ret;
}

bool
NTUtils::CreateBMPFile(CFile &File,const CBitmap &BMP,const CDC &DC) {
	BitMapInfoWrapper pbi(NTUtils::CreateBitmapInfoStruct(BMP));
	const boost::scoped_array<BYTE> lpBits(new BYTE[pbi.Header()->biSizeImage]);	/* memory pointer */
	if (!lpBits.get())
		return false;
	/*
	* Retrieve the color table (RGBQUAD array) and the bits
	* (array of palette indices) from the DIB.      */
	if (!GetDIBits(DC.GetSafeHdc(),static_cast<const HBITMAP>(BMP),0,static_cast<WORD>(pbi.Header()->biHeight),lpBits.get(),pbi.Info(),DIB_RGB_COLORS))
		return false;
	BITMAPFILEHEADER hdr;       /* bitmap file-header */
	hdr.bfType=bitmap_type_id;
	/* Compute the size of the entire file. */
	hdr.bfSize=static_cast<DWORD>(sizeof(BITMAPFILEHEADER)+pbi.Header()->biSize+pbi.Header()->biClrUsed*sizeof(RGBQUAD)+pbi.Header()->biSizeImage);
	hdr.bfReserved1=0;
	hdr.bfReserved2=0;
	/* Compute the offset to the array of color indices. */
	hdr.bfOffBits=static_cast<DWORD>(sizeof(BITMAPFILEHEADER))+pbi.Header()->biSize+pbi.Header()->biClrUsed*sizeof(RGBQUAD);
	/* Copy the BITMAPFILEHEADER into the .BMP file. */
	File.Write(reinterpret_cast<const void *>(&hdr),sizeof(BITMAPFILEHEADER));
	/* Copy the BITMAPINFOHEADER and RGBQUAD array into the file. */
	File.Write(reinterpret_cast<const void *>(pbi.Header()),sizeof(BITMAPINFOHEADER)+pbi.Header()->biClrUsed*sizeof(RGBQUAD));
	/* Copy the array of color indices into the .BMP file. */
	File.Write(reinterpret_cast<const void *>(lpBits.get()),static_cast<unsigned int>(pbi.Header()->biSizeImage));
	return true;
}
