/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// TITLE:
// High-Resolution Counter Class.
//
// DESCRIPTION:
// This file declares a class the wraps the Pentium-specific time stamp counter.
// This counter has a resolution in terms of PCLKS (processor clocks) so it can
// be used for direct instruction timings.
// This class has been tested to work on Pentium processors (at least P60's to
// P166's), Pentium Pro's and Pentium II's. It also works on AMD K6-233's.
//
// Extra usage details are in the header file.
//
// VERSION HISTORY:
// 26/3/96	1.0		Created.
// 16/7/97	2.0		PJ Naughter,  A number of additions including:
//							- Support for running on Windows NT.
//							- Now uses the build-in 64 bit data type "__int64".
//							- Improved diagnostic info thanks to the above.
//							- Usage of static variables to improve efficiency.
//							- Addition of a function which will convert from CPU ticks to seconds.
//							- Improved adhereance to the MFC coding style and standards.
//							J.M.McGuiness, further modifications.
// 22/10/1997	3.0	Modified by J.M.McGuiness, made calling conventions explicitly to.
//							- Removed MFC coding style and standards, as I don't like them.
//							- Sped up method for calling appropriate function to read the CPU counter.
//							- Set the calling convention explicitly to for further optimisation.
// 1/12/1997	3.1	Modified to compile with MSVC++ v4.x and to ensure certain functions and structures are defined.
// 2/2/1998		3.2	Updated the contact e-mail addresses.
// 15/6/1999	3.3	Modified to C++ import mechanisim.
//	25/2/2004			See header file for details.
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "CPUTicker.hpp"

#include "../../../../core/exception.hpp"

using namespace libjmmcg;

/////////////////////////////////////////////////////////////////////////////
// Make it more flexible - newer versions of MSVC++ support the opcodes, so use them to help the optimiser.
#if _MSC_VER < 1200
#	define JMMCG_RDTSC \
		_emit 0x0f;      \
		rdtsc            \
			_emit 0x31
#else
#	define JMMCG_RDTSC \
		rdtsc
#endif

#if _MSC_VER < 1200
#	define JMMCG_CPUID \
		_emit 0x0f;      \
		cpuid - serialise the processor _emit 0xa2
#else
#	define JMMCG_CPUID \
		cpuid
#endif

/////////////////////////////////////////////////////////////////////////////

const CPUTicker::CPUCountFn counter;

/////////////////////////////////////////////////////////////////////////////

inline unsigned __int64
CPUTicker::CPUCountFn::GetCPUCountFnNT::operator()(void) const {
	volatile register ULARGE_INTEGER ts;	// Used to allow access to the low and high word parts.
	__asm {
		xor eax,eax
		push ecx
		push edx
		JMMCG_CPUID
		pop ecx
		JMMCG_RDTSC
		mov ts.HighPart,edx
		pop edx
		mov ts.LowPart,eax
	}
	return ts.QuadPart;
}

inline unsigned __int64
CPUTicker::CPUCountFn::GetCPUCountFn9X::operator()(void) const {
	__asm {
		cli
	}
	const unsigned __int64 ret= GetCPUCountFnNT::operator()();
	__asm {
		sti
	}
	return ret;
}

#pragma optimize("", off)
bool
CPUTicker::HasRDTSC(void) {
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);
	if(sys_info.dwProcessorType == PROCESSOR_INTEL_PENTIUM) {
		try {
			__asm {
				JMMCG_RDTSC
			}
		} catch(...) {	  // Check to see if the opcode is defined.
			return false;
		}
		// Check to see if the instruction ticks accesses something that changes.
		volatile ULARGE_INTEGER ts1, ts2;
		__asm {
			xor eax,eax
			JMMCG_CPUID
			JMMCG_RDTSC
			mov ts1.HighPart,edx
			mov ts1.LowPart,eax
			xor eax,eax
			JMMCG_CPUID
			JMMCG_RDTSC
			mov ts2.HighPart,edx
			mov ts2.LowPart,eax
		}
		// If we return true then there's a very good chance it's a real RDTSC instruction!
		if(ts2.HighPart == ts1.HighPart) {
			if(ts2.LowPart > ts1.LowPart) {
				return true;
			} else {
				return false;
			}
		}
		else if(ts2.HighPart > ts1.HighPart) {
			return true;
		}
		else {
			return false;
		}
	} else {
		return false;
	}
}
#pragma optimize("", on)

inline CPUTicker::CPUTicker(void)
	: counter(),
	  TickCount() {
	if(!HasRDTSC()) {
		throw exception_type(_T("RDTSC not supported."), JMMCG_FUNCTION(*this), __REV_INFO__);	  // No point in instanciating the class if there's no RDTSC instruction!
	}
}

inline CPUTicker::CPUTicker(const CPUTicker& ticker)
	: counter(),
	  TickCount(ticker.TickCount),
	  deviation(ticker.deviation),
	  freq(ticker.freq) {
}

inline CPUTicker::~CPUTicker(void) {
}

CPUTicker&
CPUTicker::operator=(const CPUTicker& ticker) {
	TickCount= ticker.TickCount;
	deviation= ticker.deviation;
	freq= ticker.freq;
	return *this;
}

inline void
CPUTicker::GetCPUCount(void) noexcept(true) {
	TickCount= counter.GetCount();
}

signed __int64
CPUTicker::GetTickCountS(void) const {
	register unsigned __int64 ticks;
	if((ticks= TickCount) >= static_cast<unsigned __int64>(std::numeric_limits<__int64>::max())) {
		throw exception_type(_T("__int64 range error"), JMMCG_FUNCTION(&CPUTicker::GetTickCountS), __REV_INFO__);
	}
	return static_cast<signed __int64>(ticks);
}

CPUTicker
CPUTicker::operator-(const CPUTicker& ticker) const {
	register CPUTicker tmp(*this);
	tmp.TickCount= TickCount - ticker.TickCount;
	if(TickCount < ticker.TickCount) {
		throw exception_type(_T("__int64 range error"), JMMCG_FUNCTION(&CPUTicker::operator-), __REV_INFO__);
	}
	return tmp;
}

#pragma optimize("", off)
const bool
CPUTicker::GetAndCalcCPUFrequency(double& frequency, double& target_ave_dev, const unsigned long interval, const unsigned int max_loops) {
	register LARGE_INTEGER goal, period, current;
	register unsigned int ctr= 0;
	double curr_freq, ave_freq;	// In Hz.
	double ave_dev, tmp= 0;
	CPUTicker s, f;
	if(!QueryPerformanceFrequency(&period)) {
		return true;
	}
	period.QuadPart*= interval;
	period.QuadPart/= 1000;

	// Start of tight timed loop.
	QueryPerformanceCounter(&goal);
	goal.QuadPart+= period.QuadPart;
	s.GetCPUCount();
	do {
		QueryPerformanceCounter(&current);
	} while(current.QuadPart < goal.QuadPart);
	f.GetCPUCount();
	// End of tight timed loop.

	register unsigned __int64 ticks;
	if((ticks= f.TickCount - s.TickCount) >= static_cast<unsigned __int64>(std::numeric_limits<__int64>::max())) {
		throw exception_type(_T("__int64 range error"), JMMCG_FUNCTION(&CPUTicker::GetAndCalcCPUFrequency), __REV_INFO__);
	}
	ave_freq= 1000 * static_cast<double>(static_cast<signed __int64>(ticks)) / interval;
	do {
		// Start of tight timed loop.
		QueryPerformanceCounter(&goal);
		goal.QuadPart+= period.QuadPart;
		s.GetCPUCount();
		do {
			QueryPerformanceCounter(&current);
		} while(current.QuadPart < goal.QuadPart);
		f.GetCPUCount();
		// End of tight timed loop.

		// Average of the old frequency plus the new.
		if((ticks= f.TickCount - s.TickCount) >= static_cast<unsigned __int64>(std::numeric_limits<__int64>::max())) {
			throw exception_type(_T("__int64 range error"), JMMCG_FUNCTION(&CPUTicker::GetAndCalcCPUFrequency), __REV_INFO__);
		}
		curr_freq= 1000 * static_cast<double>(static_cast<signed __int64>(ticks)) / interval;
		ave_freq= (curr_freq + ave_freq) / 2;

		// Work out the current average deviation of the frequency.
		tmp+= fabs(curr_freq - ave_freq);
		ave_dev= tmp / ++ctr;
	} while(ave_dev > target_ave_dev && ctr < max_loops);
	deviation= target_ave_dev= ave_dev;
	freq= frequency= ave_freq;
	return false;
}
#pragma optimize("", on)
