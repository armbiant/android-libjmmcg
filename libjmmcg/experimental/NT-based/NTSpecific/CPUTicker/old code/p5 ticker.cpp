// Copyright � 1997-2002 by J.M.McGuiness, all rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include <time.h>
#include <math.h>

#include "p5 ticker.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

using namespace libjmmcg;

IMPLEMENT_DYNAMIC(P5_Ticker, CObject)

#pragma optimize("",off)
inline bool P5_Ticker::Check_Has_RDTSC(void) const
{
	SYSTEM_INFO sys_info;
	GetSystemInfo(&sys_info);
	if (sys_info.dwProcessorType==PROCESSOR_INTEL_PENTIUM)
	{
		try
		{
			_asm
			{
				_emit 0x0f				; rdtsc
				_emit 0x31
				fxch
			}
		}
		catch (...)	// Check to see if the opcode is defined.
		{
			TRACE0("RDTSC instruction NOT present.\n");
			return false;
		}
		// Check to see if the instruction ticks accesses something that changes.
		volatile ULARGE_INTEGER ts1,ts2;
		_asm
		{
			xor eax,eax
			_emit 0x0f					; cpuid
			_emit 0xa2
			_emit 0x0f					; rdtsc
			_emit 0x31
			mov ts1.HighPart,edx
			mov ts1.LowPart,eax
			xor eax,eax
			_emit 0x0f					; cpuid
			_emit 0xa2
			_emit 0x0f					; rdtsc
			_emit 0x31
			mov ts2.HighPart,edx
			mov ts2.LowPart,eax
		}
		// If we return true then there's a very good chance it's a real RDTSC instruction!
		if (ts2.HighPart==ts1.HighPart)
		{
			if (ts2.LowPart>ts1.LowPart)
			{
				TRACE0("RDTSC instruction probably present.\n");
				return true;
			}
			else
			{
				TRACE0("RDTSC instruction NOT present.\n");
				return false;
			}
		}
		else if (ts2.HighPart>ts1.HighPart)
		{
			TRACE0("RDTSC instruction probably present.\n");
			return true;
		}
		else
		{
			TRACE0("RDTSC instruction NOT present.\n");
			return false;
		}
	}
	else
	{
		TRACE0("RDTSC instruction NOT present.\n");
		return false;
	}
}
#pragma optimize("",on)

P5_Ticker::os_type P5_Ticker::Check_Os(void) const
{
	os_type ret=other;
	OSVERSIONINFO osvi;
	osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
	GetVersionEx(&osvi);
	if (osvi.dwPlatformId==VER_PLATFORM_WIN32_WINDOWS)
	{
		ret=win_95;
	}
	else if (osvi.dwPlatformId==VER_PLATFORM_WIN32_NT)
	{
		ret=win_nt;
	}
	return ret;
}

P5_Ticker::P5_Ticker(void)
{
	on_os=Check_Os();
	Has_RDTSC=Check_Has_RDTSC();
	count.QuadPart=0;
}

inline P5_Ticker::P5_Ticker(const P5_Ticker &ts)
{
	Has_RDTSC=ts.Has_RDTSC;
	count.QuadPart=ts.count.QuadPart;
}

P5_Ticker &P5_Ticker::operator=(const P5_Ticker &ts)
{
	Has_RDTSC=ts.Has_RDTSC;
	count.QuadPart=ts.count.QuadPart;
	return (*this);
}

P5_Ticker &P5_Ticker::operator=(const ULARGE_INTEGER &ts)
{
	Has_RDTSC=Check_Has_RDTSC();
	count.QuadPart=ts.QuadPart;
	return (*this);
}

inline P5_Ticker P5_Ticker::operator+(const P5_Ticker &ts1) const
{
	P5_Ticker ts;
	ts.count.QuadPart=count.QuadPart+ts1.count.QuadPart;
	return (ts);
}

inline P5_Ticker P5_Ticker::operator-(const P5_Ticker &ts1) const
{
	P5_Ticker ts;
	ts.count.QuadPart=count.QuadPart-ts1.count.QuadPart;
	return (ts);
}

P5_Ticker &P5_Ticker::Get_Time(void)
{
	if (on_os==win_nt)
	{
		Get_Time_NT();
	}
	else if (on_os==win_95)
	{
		Get_Time_95();
	}
	return (*this);
}

#pragma optimize("",off)
inline P5_Ticker &P5_Ticker::Get_Time_NT(void)
{
	if (Has_RDTSC)
	{
		volatile ULARGE_INTEGER ts;
		_asm
		{
			xor eax,eax
			push ecx
			push edx
			_emit 0x0f					; cpuid - serialise the processor
			_emit 0xa2
			pop edx
			pop ecx
			_emit 0x0f					; rdtsc
			_emit 0x31
			mov ts.HighPart,edx
			mov ts.LowPart,eax
		}
		TRACE1("Time stamp counter value: quad=%lu.\n",ts.QuadPart);
		count.QuadPart=ts.QuadPart;
	}
	else
	{
		count.QuadPart=0;
	}
	return (*this);
}
#pragma optimize("",on)

#pragma optimize("",off)
inline P5_Ticker &P5_Ticker::Get_Time_95(void)
{
	if (Has_RDTSC)
	{
		volatile ULARGE_INTEGER ts;
		_asm
		{
			cli
			xor eax,eax
			push ecx
			push edx
			_emit 0x0f					; cpuid - serialise the processor
			_emit 0xa2
			pop edx
			pop ecx
			_emit 0x0f					; rdtsc
			_emit 0x31
			mov ts.HighPart,edx
			mov ts.LowPart,eax
			sti
		}
		TRACE1("Time stamp counter value: quad=%lu.\n",ts.QuadPart);
		count.QuadPart=ts.QuadPart;
	}
	else
	{
		count.QuadPart=0;
	}
	return (*this);
}
#pragma optimize("",on)

// The following function will work out the processor clock frequency to a
// specified accuracy determined by the target average deviation required.
// Note that the worst average deviation of the result is less than 5MHz for
// a mean frequency of 90MHz. So basically the target average deviation is
// supplied only if you want a more accurate result, it won't let you get a
// worse one. (Units are Hz.)
//
// (The average deviation is a better and more robust measure than it's cousin
// the standard deviation of a quantity. The item determined by each is
// essentially similar. See "Numerical Recipies", W.Press et al for more
// details.)
//
// This function will run for a maximum of 20 seconds before giving up on
// trying to improve the average deviation, with the average deviation
// actually achieved replacing the supplied target value. Use "max_loops" to
// change this. To improve the value the function converges to increase
// "interval" (which is in units of ms, default value=1000ms).

#pragma optimize("",off)
double P5_Ticker::Get_Frequency(double &target_ave_dev,const unsigned long interval,const unsigned int max_loops) const
{
	CWaitCursor wait;
	register LARGE_INTEGER goal,period,current;
	register unsigned int ctr=0;
	double curr_freq,ave_freq;	// In Hz.
	double ave_dev,tmp=0;
	P5_Ticker s,f;
	if (!QueryPerformanceFrequency(&period))
	{
		throw no_performance_counter;
	}
	period.QuadPart*=interval;
	period.QuadPart/=1000;

	// Start of tight timed loop.
	QueryPerformanceCounter(&goal);
	goal.QuadPart+=period.QuadPart;
	s.Get_Time();
	do
	{
		QueryPerformanceCounter(&current);
	} while(current.QuadPart<goal.QuadPart);
	f.Get_Time();
	// End of tight timed loop.

	ave_freq=1000*(f-s).To_Ticks()/interval;
	do
	{
		// Start of tight timed loop.
		QueryPerformanceCounter(&goal);
		goal.QuadPart+=period.QuadPart;
		s.Get_Time();
		do
		{
			QueryPerformanceCounter(&current);
		} while(current.QuadPart<goal.QuadPart);
		f.Get_Time();
		// End of tight timed loop.

		// Average of the old frequency plus the new.
		curr_freq=1000*(f-s).To_Ticks()/interval;
		ave_freq=(curr_freq+ave_freq)/2;

		// Work out the current average deviation of the frequency.
		tmp+=fabs(curr_freq-ave_freq);
		ave_dev=tmp/++ctr;
	} while (ave_dev>target_ave_dev && ctr<max_loops);
	target_ave_dev=ave_dev;
	TRACE2("Estimated the processor clock frequency =%gHz, dev.=�%gHz.\n",ave_freq,ave_dev);
	return ave_freq;
}
#pragma optimize("",on)

/////////////////////////////////////////////////////////////////////////////
// P5_Ticker diagnostics

#ifdef _DEBUG
void P5_Ticker::AssertValid(void) const
{
	CObject::AssertValid();
}

void P5_Ticker::Dump(CDumpContext& dc) const
{
	CObject::Dump(dc);
	dc<<
		_T("RDTSC instruction detected on target processor:%i.\n")<<Has_RDTSC
		<<_T("Current value of Time Stamp counter (truncated to unsigned long):%lu.\n")<<(unsigned long)count.QuadPart
		<<'\n';
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
