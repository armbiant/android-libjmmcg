// Copyright � 1997-2002 by J.M.McGuiness, all rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
/////////////////////////////////////////////////////////////////////////////

namespace jmmcg {

typedef enum {
	no_performance_counter,
} P5_Ticker_Errors_Type;

/**
	This file declares a class the wraps the Pentium-specific time stamp counter. This counter has a resolution in terms of PCLKS (processor clocks) so it can be used for direct instruction timings.
*/
class P5_Ticker : public CObject
{
	DECLARE_DYNAMIC(P5_Ticker)

public:
	P5_Ticker(void);
	inline P5_Ticker(const P5_Ticker &);
	inline ~P5_Ticker(void) {};
	P5_Ticker &operator=(const P5_Ticker &);
	P5_Ticker &operator=(const ULARGE_INTEGER &);
	inline P5_Ticker operator+(const P5_Ticker &) const;
	inline P5_Ticker operator-(const P5_Ticker &) const;
	inline double To_Ticks(void) const {register LARGE_INTEGER tmp;tmp.QuadPart=count.QuadPart;return (double)tmp.QuadPart;};
	P5_Ticker &Get_Time(void);
	double Get_Frequency(double &,const unsigned long =1000,const unsigned int =20) const;
	inline bool P5_Ticker_Available(void) const {return (Has_RDTSC);};

#ifdef _DEBUG
	virtual void AssertValid(void) const;
	virtual void Dump(CDumpContext& dc) const;
#endif

private:
	typedef enum
	{
		win_nt,
		win_95,
		other
	} os_type;
	os_type on_os;
	bool Has_RDTSC;
	ULARGE_INTEGER count;	// unsigned 64-bit integer. (MFC data type.)

	inline bool Check_Has_RDTSC(void) const;
	inline os_type Check_Os(void) const;
	inline P5_Ticker &Get_Time_NT(void);
	inline P5_Ticker &Get_Time_95(void);
};

}
