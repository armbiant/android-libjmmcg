/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// TITLE:
// High-Resolution Counter Class.
//
// DESCRIPTION:
// This file declares a class the wraps the Pentium-specific time stamp counter.
// This counter has a resolution in terms of PCLKS (processor clocks) so it can
// be used for direct instruction timings.
//
// 1. The constructor can be time-expensive. It will throw an exception if the
//    RDTSC instruction is not present.
// 2. The GetAndCalcCPUFrequency() function is also time-expensive, as this
//    function dynamically re-calculates the CPU's core frequency.
// 3. Conversion from unsigned __int64 to double is not implemented in
//    the MSVC++ v5.0 compiler, hence the GetTickCountUS() and GetTickCountS()
//    operators. Also this is the reason for the operator-() function.
//
// This class has been tested to work on Pentium processors (at least P60's to
// P233 MMX's), Pentium Pro's and Pentium II's. It also works on AMD K6-233's.
//
// VERSION HISTORY:
// 26/3/96	1.0		Created.
// 16/7/97	2.0		Modified by P.J.Naughter, please see the source file for comments.
//					Modified then by J.M.McGuiness.
// 22/10/97	3.0		Modified by J.M.McGuiness, made calling conventions explicitly to __fastcall.
//					Please see the source file for more comments.
// 1/12/97	3.1		Modified to compile with MSVC++ v4.x and to ensure certain functions and structures are defined.
// 2/2/98	3.2		Updated the contact e-mail addresses.
// 12/5/99	3.21		Minor improvements to the interface functions.
// 25/2/2004			Removed MFC-isms, exception specifications and pointless comments.

// The MSVC++ v4.x compiler doesn't support the ANSI C++ data type "bool".
// Versions 5.0 and greater do.
#if _MSC_VER < 1100
	typedef BOOL bool;
#endif

#include "../OnNT.hpp"

namespace jmmcg { namespace NTUtils {

	class win_exception;

	class AFX_EXT_CLASS CPUTicker {
	public:
		typedef win_exception exception_type;

		class CPUCountFn {
		public:
			__stdcall CPUCountFn(void) noexcept(true)
			: GetCPUCount(OnNT() ? new GetCPUCountFnNT() : new GetCPUCountFn9X()) {
			}
			__stdcall ~CPUCountFn(void) noexcept(true) {
			}

			unsigned __int64 __fastcall GetCount(void) const noexcept(true) {
				return (*GetCPUCount)();
			}

		private:
			struct GetCPUCountFnNT {
				virtual unsigned __int64 __fastcall operator()(void) const noexcept(true);
			};
			struct GetCPUCountFn9X : public GetCPUCountFnNT {
				unsigned __int64 __fastcall operator()(void) const noexcept(true);
			};

			const std::auto_ptr<GetCPUCountFnNT> GetCPUCount;

			__stdcall CPUCountFn(const CPUCountFn &) noexcept(true);
			CPUCountFn &operator=(const CPUCountFn &) noexcept(true);
		};

		__stdcall CPUTicker(void);
		__stdcall CPUTicker(const CPUTicker &) noexcept(true);
		__stdcall ~CPUTicker(void);

		CPUTicker& __fastcall operator=(const CPUTicker &) noexcept(true);

		// Perform the actual measurement.
		void __fastcall GetCPUCount(void) noexcept(true);

		// Accessors to the actual measured value.
		double __fastcall GetTickCountAsSeconds(void) const {
			return static_cast<double>(GetTickCountS())/freq;
		}
		unsigned __int64 __fastcall GetTickCountUS(void) const noexcept(true) {
			return TickCount;
		}
		signed __int64 __fastcall GetTickCountS(void) const;

		/**
			The following function will work out the processor clock frequency to a
			specified accuracy determined by the target average deviation required.
			Note that the worst average deviation of the result is less than 5MHz for
			a mean frequency of 90MHz. So basically the target average deviation is
			supplied only if you want a more accurate result, it won't let you get a
			worse one. (Units are Hz.)

			(The average deviation is a better and more robust measure than it's cousin
			the standard deviation of a quantity. The item determined by each is
			essentially similar. See "Numerical Recipies", W.Press et al for more
			details.)

			This function will run for a maximum of 20 seconds before giving up on
			trying to improve the average deviation, with the average deviation
			actually achieved replacing the supplied target value. Use "max_loops" to
			change this. To improve the value the function converges to increase
			"interval" (which is in units of ms, default value=1000ms).
		*/
		const bool __fastcall GetAndCalcCPUFrequency(double &frequency,double &target_ave_dev,const unsigned long interval = 1000,const unsigned int max_loops =20);
		void __fastcall GetCPUFrequency(double &frequency,double &target_ave_dev) const noexcept(true) {
			frequency=freq;
			target_ave_dev=deviation;
		}

		CPUTicker __fastcall operator-(const CPUTicker &) const;

	private:
		const CPUCountFn counter;
		unsigned __int64 TickCount;

		// Note: deviation and frequency are undefined until calculated,
		// as we have no idea what they should be.
		double deviation;
		double freq;

		static bool __fastcall HasRDTSC(void) noexcept(true);
		static bool __fastcall InterruptsOK(void) noexcept(true);
	};

} }
