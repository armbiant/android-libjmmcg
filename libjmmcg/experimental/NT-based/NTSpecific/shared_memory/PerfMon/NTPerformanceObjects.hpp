/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include"../../RegistryKey.hpp"
#include"../SharedMemory.hpp"
#include"../../EventLog.hpp"

#include<winperf.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace NTUtils {

	const unsigned long initial_shared_memory_size=16384;
	const char string_shared_memory_name[]="{CA5A83A1-9925-11d4-B182-0001029FBE99}";
	const char string_shared_mutex_name[]="{0883F431-99FC-11d4-B184-0001029FBE99}";
	const char ptr_shared_memory_name[]="{0883F432-99FC-11d4-B184-0001029FBE99}";
	const char ptr_shared_mutex_name[]="{0883F433-99FC-11d4-B184-0001029FBE99}";
	const char list_shared_memory_name[]="{5538F380-A114-11d4-B188-0001029FBE99}";
	const char list_shared_mutex_name[]="{5538F381-A114-11d4-B188-0001029FBE99}";
	const char map_shared_memory_name[]="{88668F70-CF94-11d4-B19D-0001029FBE99}";
	const char map_shared_mutex_name[]="{88668F71-CF94-11d4-B19D-0001029FBE99}";

	const std::pair< std::string, std::string > string_shared_memory_names(
		string_shared_memory_name,
		string_shared_mutex_name
	);
	const std::pair< std::string, std::string > ptr_shared_memory_names(
		ptr_shared_memory_name,
		ptr_shared_mutex_name
	);
	const std::pair< std::string, std::string > list_shared_memory_names(
		list_shared_memory_name,
		list_shared_mutex_name
	);
	const std::pair< std::string, std::string > map_shared_memory_names(
		map_shared_memory_name,
		map_shared_mutex_name
	);

	class NTPerformanceObjects {
	public:
		// All these classes are used to name the performance object, list the counters
		// it contains and the counters' features.
		class PerfObjectDetails {
		public:
			class PerfObjectStrings {
			public:
				std::string name;
				std::string help;
				inline PerfObjectStrings(void) : name( "" ), help( "" ) {}
				inline PerfObjectStrings(const std::string &n, const std::string &h) : name( n ), help( h ) {}
				inline PerfObjectStrings(const PerfObjectStrings &p) : name( p.name ), help( p.help ) {}
				inline ~PerfObjectStrings(void) {}
				inline PerfObjectStrings &operator=(const PerfObjectStrings &p) {name=p.name;help=p.help;return *this;}
				inline bool operator<(const PerfObjectStrings &p) const {return name<p.name || (name==p.name && help<p.help);}
				inline operator std::string (void) const {return "Name: '" + name + "', help: '" + help + "'";};
			};
			class PerfCtrDetails {
			public:
				PerfObjectStrings details;
				unsigned long flags;
				size_t size;
				inline PerfCtrDetails(void) {}
				inline PerfCtrDetails(const PerfObjectStrings &pos, const unsigned long f, const size_t s) : details( pos ), flags( f ), size( s ) {}
				inline PerfCtrDetails(const PerfCtrDetails &pcd) : details( pcd.details ), flags( pcd.flags ), size( pcd.size ) {}
				inline ~PerfCtrDetails(void) {}
				inline PerfCtrDetails &operator=(const PerfCtrDetails &pcd) {details=pcd.details;flags=pcd.flags;size=pcd.size;return *this;}
				inline bool operator<(const PerfCtrDetails &pcd) const {return details<pcd.details || (details==pcd.details && (flags<pcd.flags || (flags==pcd.flags && size<pcd.size)));}
				inline operator std::string (void) const {std::stringstream ss;ss << (std::string)details << ", flags:" << flags << ", size:"<< size << " bytes"; return ss.str();};
			};

			PerfObjectStrings object;
			std::set< PerfCtrDetails > counters;
			inline PerfObjectDetails(const PerfObjectStrings &pos, const std::set< PerfCtrDetails > &c) : object( pos ) {for (std::set< PerfCtrDetails >::const_iterator i(c.begin());i!=c.end();++i) {counters.insert( *i );}}
			inline PerfObjectDetails(const PerfObjectDetails &pcd) : object( pcd.object ) {for (std::set< PerfCtrDetails >::const_iterator i(pcd.counters.begin());i!=pcd.counters.end();++i) {counters.insert( *i );}}
			inline ~PerfObjectDetails(void) {}

			inline operator std::string (void) const {std::stringstream ss;ss << "Performance Object:" << std::endl << (std::string)object << std::endl;for (std::set< PerfCtrDetails >::const_iterator i(counters.begin());i!=counters.end();++i) {ss << "Counter details: " << (std::string)*i << std::endl;};return ss.str();};

		private:
			inline PerfObjectDetails &operator=(const PerfObjectDetails &) noexcept(true);
		};

		inline NTPerformanceObjects(const std::string &obj_prefix, const std::pair< std::string, std::string > &lang_details, const PerfObjectDetails &perf_obj_details, const bool q = true);
		virtual inline ~NTPerformanceObjects(void);

//			typedef ManagedSharedMemorySTLIntf< CrapManagedSharedMemory< char >, 0 > sm_char_heap_type;
		typedef ManagedSharedMemorySTLIntf< KnRManagedSharedMemory< char >, 0 > sm_char_heap_type;
		typedef std::basic_string
		<
			char,
			std::char_traits< char >,
			SMAllocator
			<
				sm_char_heap_type,
				char
			>
		> sm_string_type;
//			typedef ManagedSharedMemorySTLIntf< CrapManagedSharedMemory< sm_string_type >, 2 > sm_string_heap_type;
		typedef ManagedSharedMemorySTLIntf< KnRManagedSharedMemory< sm_string_type >, 2 > sm_string_heap_type;
//			typedef ManagedSharedMemorySTLIntf< CrapManagedSharedMemory< NTPerformanceObjects * >, 1 > nt_po_heap_type;
		typedef ManagedSharedMemorySTLIntf< KnRManagedSharedMemory< NTPerformanceObjects * >, 1 > nt_po_heap_type;
		typedef std::map
		<
			sm_string_type,
			NTPerformanceObjects *,
			std::less< sm_string_type >,
			SMAllocator
			<
				nt_po_heap_type,
				NTPerformanceObjects *
			>
		> cpp_performance_objects_type;
//			typedef ManagedSharedMemorySTLIntf< CrapManagedSharedMemory< cpp_performance_objects_type >, 3 > cpp_po_heap_type;
		typedef ManagedSharedMemorySTLIntf< KnRManagedSharedMemory< cpp_performance_objects_type >, 3 > cpp_po_heap_type;
		// Give everyone full control access to the shared memory heaps.
		static EveryoneSecuritySettings allow_all;
		// Event logging stuff....
		static EventLog nt_event_log;
		// Used to ensure serialised access to the map of performance counters.
		static CComAutoCriticalSection perf_ctr_map_sect;

		// This suite of three functions have to be static because the NT performance counter
		// interface is "C"-style, so uses file-level (hence static) call-back functions.
		// Used by processes to connect to this performance object. Increments the reference count.
		// The addresses of these funtions are taken later, so they are certainly not "inline"!
		static DWORD Open(NTPerformanceObjects * const pthis) {return pthis->Open();}
		// Used by processes to get the current counter values.
		static DWORD Collect(NTPerformanceObjects * const pthis, LPWSTR lpwszValue,LPVOID *lppData,LPDWORD lpcbBytes,LPDWORD lpcObjectTypes) {return pthis->Collect(lpwszValue, lppData, lpcbBytes, lpcObjectTypes);}
		// Used by processes to notify disconnection from the performance objects.
		static DWORD Close(void);

	protected:
		virtual inline DWORD Open(void);
		virtual inline DWORD Collect(LPWSTR lpwszValue,LPVOID *lppData,LPDWORD lpcbBytes,LPDWORD lpcObjectTypes) =0;

	private:
		class TempPerfINIFile {
		public:
			inline TempPerfINIFile(const std::string &obj_prefix, const std::pair< std::string, std::string > &lang_details, const PerfObjectDetails &perf_obj_details);
			inline ~TempPerfINIFile(void);

			inline const std::string &name(void) const noexcept(true);
			static inline std::string make_driver_name(const std::string &);

		private:
			class TempSymbolFile {
			public:
				inline TempSymbolFile(const std::string &obj_prefix, const std::vector< std::string >::size_type size);
				inline ~TempSymbolFile(void);

				inline const std::string &name(void) const noexcept(true);
				inline const std::vector< std::string > &names(void) const noexcept(true);

			private:
				temp_file<std::fstream<char> > symbol;
				std::vector< std::string > object_names;

				// I don't allow copying or assignment.
				inline TempSymbolFile(const TempSymbolFile &) noexcept(true);
				inline TempSymbolFile &operator=(const TempSymbolFile &) noexcept(true);
			};

			temp_file<std::fstream<char> > ini;
			TempSymbolFile symbol;

			// I don't allow assignment.
			inline TempPerfINIFile &operator=(const TempPerfINIFile &) noexcept(true);
		};

		const bool quiet;
		const std::string driver_name;
		const std::set< PerfObjectDetails::PerfCtrDetails >::size_type num_ctrs;
		RegistryKey perf_key, perf_perf_key, perf_link_key;
		// Shared memory heaps for sharing with the NT "Performance Monitor" process.
		sm_char_heap_type sm_char_heap;
		nt_po_heap_type ptr_heap;
		sm_string_heap_type sm_string_heap;
		cpp_po_heap_type cpp_po_heap;
		cpp_performance_objects_type *cpp_performance_objects;

		typedef struct {
			PERF_OBJECT_TYPE object_type;
			PERF_COUNTER_DEFINITION *ctr_defs;
		} perf_objects_details_type;
		perf_objects_details_type *perf_objects_details;

		typedef struct {
			PERF_COUNTER_BLOCK ctr_block;
			DWORD *values;
		} perf_ctrs_details_type;
		perf_ctrs_details_type *perf_ctrs_details;

		// I don't allow assignment.
		inline NTPerformanceObjects &operator=(const NTPerformanceObjects &) noexcept(true);
	};

} } }
