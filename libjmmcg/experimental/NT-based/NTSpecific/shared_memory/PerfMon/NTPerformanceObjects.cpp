/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#ifdef _DEBUG
#	define new DEBUG_NEW
#	undef THIS_FILE
static char THIS_FILE[]= __FILE__;
#endif

#include "../DumpWinMsg.hpp"
#include "NTPerformanceObjects.hpp"

#pragma comment(lib, "loadperf.lib")

using namespace libjmmcg;
using namespace NTUtils;

/////////////////////////////////////////////////////////////////////////////

template<>
HeapID
NTPerformanceObjects::sm_char_heap_type::GetHeapID(void) {
	return HeapID(initial_shared_memory_size, string_shared_memory_name, string_shared_mutex_name, &NTPerformanceObjects::allow_all);
}

template<>
HeapID
NTPerformanceObjects::nt_po_heap_type::GetHeapID(void) {
	return HeapID(initial_shared_memory_size, ptr_shared_memory_name, ptr_shared_mutex_name, &NTPerformanceObjects::allow_all);
}

template<>
HeapID
NTPerformanceObjects::sm_string_heap_type::GetHeapID(void) {
	return HeapID(initial_shared_memory_size, list_shared_memory_name, list_shared_mutex_name, &NTPerformanceObjects::allow_all);
}

template<>
HeapID
NTPerformanceObjects::cpp_po_heap_type::GetHeapID(void) {
	return HeapID(initial_shared_memory_size, map_shared_memory_name, map_shared_mutex_name, &NTPerformanceObjects::allow_all);
}

// I need this linkage to ensure I have a standard decorated name to us in the "NTPerformanceObjects", below.
// I can't put these in a class because of the name mangling.

extern "C" {

	__declspec(dllexport)
		DWORD CALLBACK OpenPerformanceData(LPWSTR lpDeviceNames) {
		NTUtils::CSectionLock lock(NTPerformanceObjects::perf_ctr_map_sect);
		NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_information, NTUtils::EventLog::cat_custom1, "OpenPerformanceData(...) : Connecting ...");
		std::string str;
		try {
			NTPerformanceObjects::cpp_po_heap_type cpp_po_heap(HeapID(initial_shared_memory_size, map_shared_memory_names, &NTPerformanceObjects::allow_all));
			NTPerformanceObjects::cpp_performance_objects_type* const cpp_performance_objects= (NTPerformanceObjects::cpp_performance_objects_type*)cpp_po_heap;
			if(cpp_performance_objects->find(reinterpret_cast<char*>(lpDeviceNames)) == cpp_performance_objects->end()) {
				str= "OpenPerformanceData(...) : The device name does not exist:'";
				str+= reinterpret_cast<char*>(lpDeviceNames);
				str+= "'";
				NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_information, NTUtils::EventLog::cat_custom1, str);
				return ERROR_DEV_NOT_EXIST;
			}
			return NTPerformanceObjects::Open((*cpp_performance_objects)[reinterpret_cast<char*>(lpDeviceNames)]);
		} catch(std::exception err) {
			str= "OpenPerformanceData(...) : Connection failure.\nSTL exception caught.\nDetails:\n";
			str+= err.what();
			NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_custom1, str);
		} catch(...) {
			NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_custom1, "OpenPerformanceData(...) : Connection failure.\nUnknown exception caught.");
		}
		return ERROR_INVALID_FUNCTION;
	}

	__declspec(dllexport)
		DWORD WINAPI CollectPerformanceData(LPWSTR lpwszValue, LPVOID* lppData, LPDWORD lpcbBytes, LPDWORD lpcObjectTypes) {
		NTUtils::CSectionLock lock(NTPerformanceObjects::perf_ctr_map_sect);
		NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_information, NTUtils::EventLog::cat_custom2, "CollectPerformanceData(...) : Collecting...");
		std::string str;
		try {
			NTPerformanceObjects::cpp_po_heap_type cpp_po_heap(HeapID(initial_shared_memory_size, map_shared_memory_names, &NTPerformanceObjects::allow_all));
			NTPerformanceObjects::cpp_performance_objects_type* const cpp_performance_objects= (NTPerformanceObjects::cpp_performance_objects_type*)cpp_po_heap;
			std::auto_ptr<char> n_value(new char[wcslen(lpwszValue)]);
			CharToOemW(lpwszValue, n_value.get());
			str= "CollectPerformanceData(...) : Collection parameters:\nCollection load type:'";
			str+= n_value.get();
			str+= "'";
			NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_information, NTUtils::EventLog::cat_custom2, str);
			if(cpp_performance_objects->find(n_value.get()) == cpp_performance_objects->end()) {
				str= "CollectPerformanceData(...) : The device name does not exist:'";
				str+= n_value.get();
				str+= "'";
				NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_custom2, str);
				return ERROR_DEV_NOT_EXIST;
			}
			return NTPerformanceObjects::Collect((*cpp_performance_objects)[n_value.get()], lpwszValue, lppData, lpcbBytes, lpcObjectTypes);
		} catch(std::exception err) {
			str= "CollectPerformanceData(...) : Connection failure.\nSTL exception caught.\nDetails:";
			str+= err.what();
			NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_custom2, str);
		} catch(...) {
			NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_custom2, "CollectPerformanceData(...) : Connection failure.\nUnknown exception caught.");
		}
		*lpcbBytes= 0;
		*lpcObjectTypes= 0;
		return ERROR_INVALID_FUNCTION;
	}

	__declspec(dllexport)
		DWORD WINAPI ClosePerformanceData(void) {
		NTUtils::CSectionLock lock(NTPerformanceObjects::perf_ctr_map_sect);
		NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_information, NTUtils::EventLog::cat_custom3, "ClosePerformanceData() : Disconnecting.");
		std::string str;
		try {
			return NTPerformanceObjects::Close();
		} catch(std::exception err) {
			str= "CollectPerformanceData(...) : Connection failure.\nSTL exception caught.\nDetails:";
			str+= err.what();
			NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_custom3, str);
		} catch(...) {
			NTPerformanceObjects::nt_event_log.Log(NTUtils::EventLog::err_error, NTUtils::EventLog::cat_custom3, "CollectPerformanceData(...) : Connection failure.\nUnknown exception caught.");
		}
		return ERROR_INVALID_FUNCTION;
	}
}

/*
	NT specific class for creating a generic performance monitoring object.
*/

// Making this a static gives errors when trying to delete the registry key. It is delete, just
// in the wrong order. Serves me right for making it a static.
NTUtils::EventLog NTPerformanceObjects::nt_event_log("NTPerformanceObjects");
CComAutoCriticalSection NTPerformanceObjects::perf_ctr_map_sect;
NTUtils::EveryoneSecuritySettings NTPerformanceObjects::allow_all;

inline NTPerformanceObjects::NTPerformanceObjects(const std::string& obj_prefix, const std::pair<std::string, std::string>& lang_details, const PerfObjectDetails& perf_obj_details, const bool q)
	: quiet(q),
	  driver_name(TempPerfINIFile::make_driver_name(perf_obj_details.object.name)),
	  num_ctrs(perf_obj_details.counters.size()),
	  sm_char_heap(HeapID(initial_shared_memory_size, string_shared_memory_names, &allow_all)),
	  ptr_heap(HeapID(initial_shared_memory_size, ptr_shared_memory_names, &allow_all)),
	  sm_string_heap(HeapID(initial_shared_memory_size, list_shared_memory_names, &allow_all)),
	  cpp_po_heap(HeapID(initial_shared_memory_size, map_shared_memory_names, &allow_all)),
	  perf_objects_details(NULL),
	  perf_ctrs_details(NULL) {
	if(!num_ctrs) {
		std::cerr << "NTPerformanceObjects::NTPerformanceObjects(...) : Performance object: '" << driver_name << "' must have at least one counter." << std::endl;
		return;
	}
	/*	cpp_performance_objects=cpp_po_heap.Allocate(1, NULL);
		cpp_po_heap.Construct(cpp_performance_objects,cpp_performance_objects_type());
		if (cpp_performance_objects->find(driver_name.c_str())!=cpp_performance_objects->end()) {
			std::cerr << "NTPerformanceObjects::NTPerformanceObjects(...) : Performance object: '" << driver_name << "' already exists. Cannot have duplicate performance objects." << std::endl;
			return;
		}
		// Make sure that nothing can access the map whilst it is being modified, especially those
		// "C" global fns (above).
		NTUtils::CSectionLock lock(perf_ctr_map_sect);
		// Add the new performance counter object to the map, so that the "C" fns above will
		// be able to access this new object.
		(*cpp_performance_objects)[driver_name.c_str()]=this;
	*/
	const std::string ctr_name("SYSTEM\\CurrentControlSet\\Services\\" + driver_name);
	//	assert(cpp_performance_objects->find(driver_name.c_str())!=cpp_performance_objects->end());
	if(perf_key.Open(HKEY_LOCAL_MACHINE, ctr_name) == ERROR_SUCCESS) {
		std::cerr << "NTPerformanceObjects::NTPerformanceObjects(...) : Performance counter '" << ctr_name << "' already created. Cannot have duplicate named counters." << std::endl;
	} else if(perf_key.Create(HKEY_LOCAL_MACHINE, ctr_name) == ERROR_SUCCESS
				 && perf_perf_key.Create(perf_key.hkey(), "Performance") == ERROR_SUCCESS
				 && perf_link_key.Create(perf_key.hkey(), "Linkage") == ERROR_SUCCESS) {
		size_t total_ctr_size= 0;
		std::set<PerfObjectDetails::PerfCtrDetails>::const_iterator iter(perf_obj_details.counters.begin());
		while(iter != perf_obj_details.counters.end()) {
			total_ctr_size+= sizeof(PERF_COUNTER_BLOCK) + iter->size;
			++iter;
		}
		perf_ctrs_details= reinterpret_cast<perf_ctrs_details_type*>(new char[total_ctr_size]);
		perf_objects_details= reinterpret_cast<perf_objects_details_type*>(new char[sizeof(PERF_OBJECT_TYPE) + sizeof(PERF_COUNTER_DEFINITION) * perf_obj_details.counters.size()]);
		perf_objects_details->object_type.HeaderLength= sizeof(PERF_OBJECT_TYPE);
		perf_objects_details->object_type.DefinitionLength= perf_objects_details->object_type.HeaderLength + sizeof(PERF_COUNTER_DEFINITION) * perf_obj_details.counters.size();
		perf_objects_details->object_type.TotalByteLength= perf_objects_details->object_type.DefinitionLength + perf_ctrs_details->ctr_block.ByteLength;
		perf_objects_details->object_type.ObjectNameTitleIndex= 0;
		perf_objects_details->object_type.ObjectNameTitle= 0;
		perf_objects_details->object_type.ObjectHelpTitleIndex= 0;
		perf_objects_details->object_type.ObjectHelpTitle= 0;
		perf_objects_details->object_type.DetailLevel= PERF_DETAIL_ADVANCED;
		perf_objects_details->object_type.NumCounters= (perf_objects_details->object_type.DefinitionLength - sizeof(PERF_OBJECT_TYPE)) / sizeof(PERF_COUNTER_DEFINITION);
		perf_objects_details->object_type.DefaultCounter= 0;
		perf_objects_details->object_type.NumInstances= PERF_NO_INSTANCES;
		perf_objects_details->object_type.CodePage= 0;
		iter= perf_obj_details.counters.begin();
		std::set<PerfObjectDetails::PerfCtrDetails>::size_type i= 0;
		while(iter != perf_obj_details.counters.end()) {
			reinterpret_cast<perf_ctrs_details_type*>(perf_ctrs_details + i * (sizeof(PERF_COUNTER_BLOCK) + iter->size))->ctr_block.ByteLength= sizeof(PERF_COUNTER_BLOCK) + iter->size;
			perf_objects_details->ctr_defs[i].ByteLength= sizeof(PERF_COUNTER_DEFINITION);
			perf_objects_details->ctr_defs[i].CounterNameTitleIndex= (i << 1);
			perf_objects_details->ctr_defs[i].CounterNameTitle= 0;
			perf_objects_details->ctr_defs[i].CounterHelpTitleIndex= (i << 1);
			perf_objects_details->ctr_defs[i].CounterHelpTitle= 0;
			perf_objects_details->ctr_defs[i].DefaultScale= 0;
			perf_objects_details->ctr_defs[i].DetailLevel= PERF_DETAIL_ADVANCED;
			perf_objects_details->ctr_defs[i].CounterType= iter->flags;
			perf_objects_details->ctr_defs[i].CounterSize= sizeof(perf_ctrs_details->values[i]);
			perf_objects_details->ctr_defs[i].CounterOffset= (DWORD) & (((perf_ctrs_details_type*)NULL)->values[i]);
			++i;
			++iter;
		}
		char mod_name[MAX_PATH];
		// Note embedded name of this dll!!!
		if(GetModuleFileName(GetModuleHandle("cmduilib"), mod_name, MAX_PATH)) {
			perf_link_key.SetValue(driver_name, "Export", REG_BINARY);
			perf_perf_key.SetValue(mod_name, "Library");
			// Note the use of the decorated names!!!!!
			perf_perf_key.SetValue("_OpenPerformanceData@4", "Open");
			// Note the use of the decorated names!!!!!
			perf_perf_key.SetValue("_CollectPerformanceData@16", "Collect");
			// Note the use of the decorated names!!!!!
			perf_perf_key.SetValue("_ClosePerformanceData@0", "Close");
			TempPerfINIFile ini_file(obj_prefix, lang_details, perf_obj_details);
			long err;
			// Note bug in "LoadPerfCounterTextStrings(...)" - see MSDN article: 'ID: Q188769'.
			if((err= LoadPerfCounterTextStrings(const_cast<char* const>(("x " + ini_file.name()).c_str()), quiet)) != ERROR_SUCCESS) {
				std::cerr << "NTPerformanceObject::NTPerformanceObject(...) : '" << NTUtils::DumpWinMessage(err) << "'" << std::endl;
			}
		} else {
			std::cerr << "NTPerformanceObjects::NTPerformanceObjects(...) : Failed to get module name. Windows error: '" << NTUtils::DumpWinMessage(::GetLastError()) << "'" << std::endl;
		}
	}
}

inline NTPerformanceObjects::~NTPerformanceObjects(void) {
	NTUtils::CSectionLock lock(perf_ctr_map_sect);
	// Remove this object from the map.
	//	cpp_performance_objects->erase(driver_name.c_str());
	//	cpp_po_heap.Deallocate(cpp_performance_objects,1);
	long err;
	// Note bug in "LoadPerfCounterTextStrings(...)" - see MSDN article: 'ID: Q188769'.
	if((err= UnloadPerfCounterTextStrings(const_cast<char* const>(("x " + driver_name).c_str()), quiet)) != ERROR_SUCCESS) {
		std::cerr << "NTPerformanceObjects::~NTPerformanceObjects() : '" << NTUtils::DumpWinMessage(err) << "'" << std::endl;
	}
	if(perf_ctrs_details) {
		delete[] reinterpret_cast<char*>(perf_ctrs_details);
	}
	if(perf_objects_details) {
		delete[] reinterpret_cast<char*>(perf_objects_details);
	}
}

inline DWORD
NTPerformanceObjects::Open(void) {
	// Note we are already locked!
	NTUtils::RegistryKey perf_key;
	long ret;
	if((ret= perf_key.Open(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Services\\" + driver_name + "\\Performance")) == ERROR_SUCCESS) {
		DWORD size= sizeof(DWORD);
		DWORD type;
		DWORD dwFirstCounter;
		DWORD dwFirstHelp;
		if((ret= ::RegQueryValueEx(perf_key.hkey(), "First Counter", 0L, &type, (LPBYTE)&dwFirstCounter, &size)) != ERROR_SUCCESS) {
			std::cerr << "NTPerformanceObjects::Open() : Unable to query key 'HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" << driver_name << "\\Performance\\First Counter'." << std::endl;
			std::cerr << "Error is '" << NTUtils::DumpWinMessage(ret) << "'" << std::endl;
			return ret;
		}
		size= sizeof(DWORD);
		if((ret= ::RegQueryValueEx(perf_key.hkey(), "First Help", 0L, &type, (LPBYTE)&dwFirstHelp, &size)) != ERROR_SUCCESS) {
			std::cerr << "NTPerformanceObjects::Open() : Unable to query key 'HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\" << driver_name << "\\Performance\\First Help'." << std::endl;
			std::cerr << "Error is '" << NTUtils::DumpWinMessage(ret) << "'" << std::endl;
			return ret;
		}
		perf_objects_details->object_type.ObjectNameTitleIndex+= dwFirstCounter;
		perf_objects_details->object_type.ObjectHelpTitleIndex+= dwFirstHelp;
		perf_objects_details->object_type.DefaultCounter= 0;
		std::set<PerfObjectDetails::PerfCtrDetails>::size_type i= 0;
		while(i < num_ctrs) {
			perf_objects_details->ctr_defs[i].CounterNameTitleIndex+= dwFirstCounter;
			perf_objects_details->ctr_defs[i].CounterHelpTitleIndex+= dwFirstHelp;
			++i;
		}
	}
	return ret;
}

inline DWORD
NTPerformanceObjects::Close(void) {
	// Note we are already locked!
	return ERROR_SUCCESS;
}

inline NTPerformanceObjects::TempPerfINIFile::TempPerfINIFile(const std::string& obj_prefix, const std::pair<std::string, std::string>& lang_details, const PerfObjectDetails& perf_obj_details)
	: symbol(obj_prefix, perf_obj_details.counters.size() + 1) {
	ini.file() << "[info]" << std::endl;
	const std::string driver_name(make_driver_name(perf_obj_details.object.name));
	ini.file() << "drivername=" << driver_name << std::endl;
	ini.file() << "symbolfile=" << symbol.name() << std::endl;
	ini.file() << "[languages]" << std::endl;
	ini.file() << lang_details.first << "=" << lang_details.second << std::endl;
	ini.file() << "[text]" << std::endl;
	ini.file() << symbol.names()[0] << "_" << lang_details.first << "_NAME=" << driver_name << std::endl;
	ini.file() << symbol.names()[0] << "_" << lang_details.first << "_HELP=" << perf_obj_details.object.help << std::endl;
	std::vector<std::string>::size_type i= 1;
	std::set<PerfObjectDetails::PerfCtrDetails>::const_iterator iter(perf_obj_details.counters.begin());
	while(i < symbol.names().size()) {
		ini.file() << symbol.names()[i] << "_" << lang_details.first << "_NAME=" << iter->details.name << std::endl;
		ini.file() << symbol.names()[i] << "_" << lang_details.first << "_HELP=" << iter->details.help << std::endl;
		++iter;
		++i;
	}
	ini.file() << std::endl;
	ini.file().close();
}

inline NTPerformanceObjects::TempPerfINIFile::~TempPerfINIFile(void) {
}

inline const std::string&
NTPerformanceObjects::TempPerfINIFile::name(void) const {
	return ini.name();
}

inline std::string
NTPerformanceObjects::TempPerfINIFile::make_driver_name(const std::string& str) {
	char bname[MAX_PATH];
	::GetModuleFileName(NULL, bname, MAX_PATH);
	std::string name(bname);
	const std::string::size_type begin= name.find_last_of('\\') + 1;
	name= name.substr(begin, name.find_last_of('.') - begin);
	return name + " " + str;
}

inline NTPerformanceObjects::TempPerfINIFile::TempSymbolFile::TempSymbolFile(const std::string& obj_prefix, const std::vector<std::string>::size_type size) {
	std::vector<std::string>::size_type i= 0;
	while(i < size) {
		std::stringstream ss;
		ss << obj_prefix << "_" << i;
		object_names.push_back(ss.str());
		symbol.file() << "#define " << object_names[i] << " " << (i << 1) << std::endl;
		++i;
	}
	symbol.file().close();
}

inline NTPerformanceObjects::TempPerfINIFile::TempSymbolFile::~TempSymbolFile(void) {
}

inline const std::string&
NTPerformanceObjects::TempPerfINIFile::TempSymbolFile::name(void) const {
	return symbol.name();
}

inline const std::vector<std::string>&
NTPerformanceObjects::TempPerfINIFile::TempSymbolFile::names(void) const {
	return object_names;
}
