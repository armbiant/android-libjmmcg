## $Header$
##
## Copyright (c) 2016 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
# For more information:
# - http://psy.swansea.ac.uk/staff/carter/gnuplot/gnuplot_histograms.htm
#
# gnuplot -e "datafile='make_histogram.csv'" -e "graphfile='make_histogram.svg'" -e "graph_title='Histogram of the partial round-trip for the MIT/BIT exchange link.'" -e "catchall_bin=60" make_histogram.gnuplot

set macros

if (!exists("datafile")) print "Missing input CSV file, use: -e \"datafile='make_histogram.csv'\"."; exit gnuplot
if (!exists("graphfile")) print "Missing output SVG filname, use: -e \"graphfile='make_histogram.svg'\"."; exit gnuplot
if (!exists("graph_title")) print "Missing the graph title, use: -e \"graph_title='Histogram of the partial round-trip for the MIT/BIT exchange link.'\"."; exit gnuplot
if (!exists("catchall_bin")) print "Missing the last catch-all bin, use: -e \"catchall_bin=60\"."; exit gnuplot

set datafile separator ','
set title graph_title

# firstrow could be used to specify the titles of the axes, etc.
firstrow = system("head -n1 " . datafile . " | sed 's/ /_/g' | sed 's/|/ /g'")
timing_type_mangled = system(sprintf("awk -F \",\" '{print $1}' <<< %s", firstrow))
timing_type = system(sprintf("c++filt -t %s", timing_type_mangled))
timing_units = system(sprintf("awk -F \",\" '{print $4}' <<< %s", firstrow))

set style data histogram

stats datafile using ($2-$1) prefix "A" nooutput
max_value=A_max
min_value=A_min

end_bin=catchall_bin # A_max # This is the last "catch-all" bin.
n=100 #number of intervals
binwidth=(end_bin-min_value)/n #interval width
bin(x,width)=width*floor((x<end_bin?x:end_bin)/width)
set style fill solid border -1
set boxwidth 0.5*binwidth

#set xrange [min_value:max_value]	# Use this to not truncate the histogram with a catch-all bin.
set xrange [min_value:end_bin]	# Truncate the x-range so that there is a catch-all bin.
set yrange [0:]

xlabel_lastbin=sprintf("Latency (%s).", timing_units)
xtic_firstbin=sprintf("%d", min_value)
xtic_lastbin=sprintf("\n[%d, %d]", end_bin, max_value)
key_label=sprintf("Using %s, mean=%d (stddev=%d).", timing_type, A_mean, A_stddev)

set xlabel xlabel_lastbin noenhance
set ylabel "No. samples." noenhance
set key autotitle columnhead noenhance
set title font ",14" noenhance font "Helvetica,24"
set key font ",14" noenhance font "Helvetica,24"
set xtics font ",14" noenhance font "Helvetica,20"
set xtics add (xtic_firstbin min_value)
set xtics add (xtic_lastbin end_bin)
set ytics font ",14" noenhance font "Helvetica,20"
set term svg size 1280,1024 noenhance font "Helvetica,24"
set output graphfile
set rmargin at screen 0.95
set tmargin at screen 0.92
set bmargin at screen 0.12

plot	\
	datafile using (bin(($2-$1), binwidth)):(1.0) smooth freq with boxes title key_label linecolor 1
