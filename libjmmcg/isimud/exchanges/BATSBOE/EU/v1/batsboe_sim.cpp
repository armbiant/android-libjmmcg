/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "batsboe_sim.hpp"

using namespace libisimud;
using namespace libjmmcg;

template class socket::svr<
	socket::server_manager::loopback<
		exchanges::BATSBOE::common::simulator_responses<exchanges::BATSBOE::EU::v1::MsgTypes, exchanges::common::socket_type::socket_t>,
		exchanges::BATSBOE::common::server_hb_t<exchanges::BATSBOE::EU::v1::MsgTypes::ServerHeartbeat_t>,
		exchanges::common::socket_type::socket_t
	>
>;
