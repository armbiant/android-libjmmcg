## Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

project(batsboe_eu CXX)

set(JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EU_SIM_MSGS ${CMAKE_CURRENT_BINARY_DIR}/computed_minimum_perfect_hash_for_BATSBOE_EU_sim_msgs.hpp)

add_custom_command(OUTPUT ${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EU_SIM_MSGS}
	COMMAND $<TARGET_FILE:compute_mph_mask> ARGS --cpp_header=${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EU_SIM_MSGS} --values 30 3 2 5 6 4 254	# WARNING: these values are encoded in the simulator_responses in "processing_rules_impl.hpp". Not changing then in synchrony is likely to cause compilation errors if they are incorrect! These are the values extracted from the BATSBOE messages.
	DEPENDS compute_mph_mask
	COMMENT "Obtain the mask for the hash for client messages to connect to BATSBOE/EU-based exchanges: '${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EU_SIM_MSGS}'."
)
add_custom_target(FindPerfectHashMaskForBATSBOEEUSimMsgs
	DEPENDS ${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EU_SIM_MSGS}
)

add_subdirectory(v1)
add_subdirectory(v2)

install(
	FILES
		${JMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EU_SIM_MSGS}
	DESTINATION include/${PROJECT_NAME}/${VERSION_NUMBER}/${PROJECT_NAME}
	COMPONENT development
	PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ
)
