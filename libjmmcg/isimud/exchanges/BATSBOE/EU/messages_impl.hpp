/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace EU {

inline
TradeCaptureReport::TradeCaptureReport(common::SeqNum_t seqNumber) noexcept(true)
: Header_t(sizeof(TradeCaptureReport)-sizeof(bitfields_to_type_map), seqNumber) {
	length_=static_cast<std::uint16_t>(length_+static_cast<std::uint16_t>(bitfields.size()));
}

inline
TradeCaptureReportAck::TradeCaptureReportAck(common::SeqNum_t seqNumber) noexcept(true)
: Header_t(sizeof(TradeCaptureReportAck)-sizeof(bitfields_to_type_map), seqNumber), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	length_=static_cast<std::uint16_t>(length_+static_cast<std::uint16_t>(bitfields.size()));
}

inline
TradeCaptureReportReject::TradeCaptureReportReject(common::SeqNum_t seqNumber) noexcept(true)
: Header_t(sizeof(TradeCaptureReportReject)-sizeof(bitfields_to_type_map), seqNumber), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	length_=static_cast<std::uint16_t>(length_+static_cast<std::uint16_t>(bitfields.size()));
}

inline
TradeCaptureConfirm::TradeCaptureConfirm(common::SeqNum_t seqNumber) noexcept(true)
: Header_t(sizeof(TradeCaptureConfirm)-sizeof(bitfields_to_type_map), seqNumber), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	length_=static_cast<std::uint16_t>(length_+static_cast<std::uint16_t>(bitfields.size()));
}

inline
TradeCaptureDecline::TradeCaptureDecline(common::SeqNum_t seqNumber) noexcept(true)
: Header_t(sizeof(TradeCaptureDecline)-sizeof(bitfields_to_type_map), seqNumber), transactionTime(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count()) {
	length_=static_cast<std::uint16_t>(length_+static_cast<std::uint16_t>(bitfields.size()));
}

} } } } }
