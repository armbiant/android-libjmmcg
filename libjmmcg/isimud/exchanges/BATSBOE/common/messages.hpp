#ifndef ISIMUD_EXCHANGES_BATSBOE_common_MESSAGES_HPP
#define ISIMUD_EXCHANGES_BATSBOE_common_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "types.hpp"
#include "optional_field_types.hpp"

#include "common/iso_10383_mic_codes.hpp"

#include "core/memops.hpp"

#include <boost/asio/ip/address.hpp>
#include <boost/program_options.hpp>

#include <chrono>
#include <string>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE {

/**
	From <a href="http://cdn.batstrading.com/resources/participant_resources/BATS_Europe_Binary_Order_Entry_Specification.pdf">"BATS Chi-X Europe Binary Order Entry Specification", Version 1.44, 27 November, 2014</a> also see <a href="http://cdn.batstrading.com/resources/participant_resources/BATS_ChiX_Europe_TRF_BOE_Specification.pdf">"BATS Chi-X Europe TRF BOE Specification", Version 1.7, 25th June 2014</a>.
*/
namespace common {

const std::uint16_t msg_start_code=0xBABA;	
struct logon_args_t {
	const SeqNum_t sequenceNumber{};
	const SessionSubID_t sessionSubID{};
	const UserName_t username{{}};
	const Password_t password{{}};
	const bool noUnspec{};
};
struct logoff_args_t {
	const SeqNum_t sequenceNumber{};
};

template<class ExchgMsgDetailsT, class ConnPolT, class SktT, class ThrdT> inline auto
make_ctor_args(boost::program_options::variables_map const &vm) noexcept(false);

/**
	Section: "3.1.1 Login Request"
*/
template<class MsgT, MsgT Msg>
class [[gnu::packed]] Header {
public:
	using MsgType_t=common::MsgType_t;
	using MsgTypes_t=MsgT;
	
	static inline constexpr const MsgTypes_t static_type=Msg;
	enum : bool {
		has_static_size=false	///< The message is dynamically-sized, not statically, so sizeof(MsgT) is the maximum amount to copy, but it could be less, given by length().
	};

	const std::uint16_t start_of_message=msg_start_code;

protected:
	std::uint16_t length_;

public:
	const MsgType_t type_{static_cast<MsgType_t>(Msg)};
	const std::uint8_t matchingUnit=0;
	const SeqNum_t sequenceNumber;

	explicit Header(std::size_t l) noexcept(true);
	Header(std::size_t l, SeqNum_t seqNumber) noexcept(true);

	MsgTypes_t type() const noexcept(true) {
		return static_cast<MsgTypes_t>(type_);
	}

	/// The actual, not the foolish BATSBOE-specified, length which excludes the start_of_message field.
	std::uint16_t length() const noexcept(true);

	bool is_valid() const noexcept(true) {
		return start_of_message==msg_start_code
			&& length()>=sizeof(Header)
			&& matchingUnit==0;
	}

protected:
	~Header() noexcept(true)=default;
};

/**
	Section: "3.1.1 Login Request"
*/
template<class MsgT>
struct [[gnu::packed]] LogonRequest : public Header<MsgT, MsgT::LogonRequest> {
	using Header_t=Header<MsgT, MsgT::LogonRequest>;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const SessionSubID_t sessionSubID;
	const UserName_t userName;
	const Password_t password;
	const std::uint8_t noUnspecifiedUnitReplay;
	optional::LogonTypes::OrderAcknowledgement::bitfields orderAcknowledgement_bitfields;
	optional::LogonTypes::OrderRejected::bitfields orderRejected_bitfields;
	optional::LogonTypes::OrderModified::bitfields orderModified_bitfields;
	optional::LogonTypes::OrderRestated::bitfields orderRestated_bitfields;
	const optional::LogonTypes::UserModifyRejected::bitfields userModifyRejected_bitfields;
	optional::LogonTypes::OrderCancelled::bitfields orderCancelled_bitfields;
	optional::LogonTypes::OrderExecution::bitfields orderExecution_bitfields;
	optional::LogonTypes::TradeCancelCorrect::bitfields tradeCancelCorrect_bitfields;
	optional::LogonTypes::Reserved::bitfields reserved_bitfields;
	optional::LogonTypes::TradeCaptureReportAck::bitfields tradeCaptureReportAck_bitfields;
	optional::LogonTypes::TradeCaptureReportRej::bitfields tradeCaptureReportRej_bitfields;
	optional::LogonTypes::TradeCaptureConfirm::bitfields tradeCaptureConfirm_bitfields;
	optional::LogonTypes::TradeCaptureDecline::bitfields tradeCaptureDecline_bitfields;
	const std::uint8_t reserved[128]={0};
	const std::uint8_t numberOfUnits=0;

	LogonRequest(SeqNum_t seqNum, const SessionSubID_t sessionSubID, const UserName_t &UN, const Password_t &P, const bool noUnspec) noexcept(true);
	explicit LogonRequest(logon_args_t const &a) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> __stdcall
	LogonRequest(SrcMsg const &msg, SeqNum_t seqNum, const SessionSubID_t sessionSubID, const bool noUnspec)=delete;
};

/**
	Section: "3.1.2 Logout request"
*/
template<class MsgT>
struct [[gnu::packed]] LogoutRequest : public Header<MsgT, MsgT::LogoutRequest> {
	using Header_t=Header<MsgT, MsgT::LogoutRequest>;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	explicit LogoutRequest(SeqNum_t seqNum) noexcept(true);
	explicit LogoutRequest(logoff_args_t const &a) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit __stdcall
	LogoutRequest(SrcMsg const &msg, SeqNum_t seqNum) noexcept(true)=delete;
};

/**
	Section: "3.1.3 Client Heartbeat" & "2.4 Heartbeats".
*/
template<class MsgT>
struct [[gnu::packed]] ClientHeartbeat : public Header<MsgT, MsgT::ClientHeartbeat> {
	using Header_t=Header<MsgT, MsgT::ClientHeartbeat>;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	enum : common::Quantity_t {
		seq_num=0
	};

	explicit ClientHeartbeat(SeqNum_t seqNum) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit __stdcall
	ClientHeartbeat(SrcMsg const &msg, SeqNum_t seqNum) noexcept(true)=delete;
};

/**
	Section: "3.2.1 Login Response"
*/
template<class MsgT>
struct [[gnu::packed]] LogonReply : public Header<MsgT, MsgT::LogonReply> {
	using Header_t=Header<MsgT, MsgT::LogonReply>;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	/// Allow a client connected to the exchange to process the LogonReply message.
	struct respond;

	static inline constexpr const LoginResponseStatus logon_success=LoginResponseStatus::LoginAccepted;

	LoginResponseStatus loginResponseStatus;

	LogonReply() noexcept(true);
	explicit LogonReply(SeqNum_t seqNum) noexcept(true);

	LoginResponseStatus rejectCode() const noexcept(true) {
		return loginResponseStatus;
	}
	void rejectCode(LoginResponseStatus const &rc) noexcept(true) {
		loginResponseStatus=rc;
	}
};

/**
	Section: "3.2.2 Logout"
*/
template<class MsgT>
struct [[gnu::packed]] Logout : public Header<MsgT, MsgT::Logout> {
	using Header_t=Header<MsgT, MsgT::Logout>;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	LogoutReason logoutReason;
	LogoutReasonText_t logoutReasonText;
	common::SeqNum_t lastReceivedSequenceNumber;
	const std::uint8_t numberOfUnits=0;

	Logout() noexcept(true);
	Logout(SeqNum_t seqNum, LogoutReason lr) noexcept(true);

	const LogoutReason &reason() const noexcept(true) {
		return logoutReason;
	}
};

/**
	Section: "3.2.3 Server Heartbeat" & "2.4 Heartbeats".
*/
template<class MsgT>
struct [[gnu::packed]] ServerHeartbeat : public Header<MsgT, MsgT::ServerHeartbeat> {
	using Header_t=Header<MsgT, MsgT::ServerHeartbeat>;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	enum : common::Quantity_t {
		seq_num=0
	};

	explicit ServerHeartbeat(SeqNum_t seqNum) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit __stdcall
	ServerHeartbeat(SrcMsg const &msg, SeqNum_t seqNum) noexcept(true)=delete;
};

/**
	Section: "3.2.4 Replay Complete"
*/
template<class MsgT>
struct [[gnu::packed]] ReplayComplete : public Header<MsgT, MsgT::ReplayComplete> {
	using Header_t=Header<MsgT, MsgT::ReplayComplete>;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	explicit ReplayComplete(SeqNum_t seqNum) noexcept(true);
};

/**
	Section: "4.1.1 New Order"
*/
template<class MsgT>
struct [[gnu::packed]] NewOrder : public Header<MsgT, MsgT::NewOrder> {
	using Header_t=Header<MsgT, MsgT::NewOrder>;
	using bitfields_to_type_map=optional::NewOrder::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	explicit NewOrder(SeqNum_t seqNum, ClientOrderID_t const &clID, OrdType const oT, TIF const t, Side const s, Symbol_t const &symb, SecurityID_t const &instID, common::Quantity_t ordQty, Price_t p) noexcept(true);
	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit __stdcall
	NewOrder(SrcMsg const &msg, SeqNum_t seqNum) noexcept(true)=delete;

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	common::Quantity_t orderQty() const noexcept(true) {
		return orderQty_;
	}
	void orderQty(common::Quantity_t i) noexcept(true) {
		orderQty_=i;
	}

	OrdType const &orderType() const noexcept(true);
	void orderType(OrdType const &i) noexcept(true);

	common::Side side() const noexcept(true) {
		return side_;
	}
	void side(common::Side i) noexcept(true) {
		side_=i;
	}

	SecurityID_t const &instrumentID() const noexcept(true);
	void instrumentID(SecurityID_t const &i) noexcept(true);

	Price_t limitPrice() const noexcept(true);
	void limitPrice(Price_t p) noexcept(true);

	TIF tif() const noexcept(true);
	void tif(TIF t) noexcept(true);
	
	Symbol_t const &symbol() const noexcept(true);
	void symbol(Symbol_t const &i) noexcept(true);

private:
	ClientOrderID_t clientOrderID_;
	common::Side side_;
	common::Quantity_t orderQty_;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.1.2 Cancel Order"
*/
template<class MsgT>
struct [[gnu::packed]] CancelOrder : public Header<MsgT, MsgT::CancelOrder> {
	using Header_t=Header<MsgT, MsgT::CancelOrder>;
	using bitfields_to_type_map=optional::CancelOrder::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	CancelOrder(SeqNum_t seqNum, ClientOrderID_t const &origclID) noexcept(true);
	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> __stdcall
	CancelOrder(SrcMsg const &msg, SeqNum_t seqNum) noexcept(true)=delete;

	ClientOrderID_t const &originalClientOrderID() const noexcept(true) {
		return originalClientOrderID_;
	}
	void originalClientOrderID(ClientOrderID_t const &origclID) noexcept(true) {
		libjmmcg::memcpy_opt(origclID, originalClientOrderID_);
	}

private:
	ClientOrderID_t originalClientOrderID_;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.1.3 Modify Order"
*/
template<class MsgT>
struct [[gnu::packed]] ModifyOrder : public Header<MsgT, MsgT::ModifyOrder> {
	using Header_t=Header<MsgT, MsgT::ModifyOrder>;
	using bitfields_to_type_map=optional::ModifyOrder::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	ModifyOrder(SeqNum_t seqNum, ClientOrderID_t const &clID, common::Quantity_t ordQty, Price_t p, Side s) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit __stdcall
	ModifyOrder(SrcMsg const &msg, SeqNum_t seqNum) noexcept(true)=delete;

	ClientOrderID_t const &originalClientOrderID() const noexcept(true) {
		return originalClientOrderID_;
	}
	void originalClientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, originalClientOrderID_);
	}

	Price_t limitPrice() const noexcept(true);
	void limitPrice(Price_t p) noexcept(true);

	common::Quantity_t orderQty() const noexcept(true);
	void orderQty(common::Quantity_t i) noexcept(true);

	Side side() const noexcept(true);
	void side(Side i) noexcept(true);

private:
	ClientOrderID_t clientOrderID_;
	ClientOrderID_t originalClientOrderID_;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.1 Order Acknowledgement"
*/
template<class MsgT>
struct [[gnu::packed]] OrderAcknowledgement : public Header<MsgT, MsgT::OrderAcknowledgement> {
	using Header_t=Header<MsgT, MsgT::OrderAcknowledgement>;
	using bitfields_to_type_map=optional::OrderAcknowledgement::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	explicit OrderAcknowledgement(SeqNum_t seqNum, ClientOrderID_t const &clID) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

private:
	ClientOrderID_t clientOrderID_;
	uint64_t orderID;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.2 Order Rejected"
*/
template<class MsgT>
struct [[gnu::packed]] OrderRejected : public Header<MsgT, MsgT::OrderRejected> {
	using Header_t=Header<MsgT, MsgT::OrderRejected>;
	using bitfields_to_type_map=optional::OrderRejected::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	using RejectCode_t=int;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	static inline constexpr const OrderRejectReason unknown_msg=OrderRejectReason::UnforeseenReason;

	const DateTime_t transactionTime;

	OrderRejected() noexcept(true);
	OrderRejected(SeqNum_t seqNum, ClientOrderID_t const &clID, OrderRejectReason orr) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

private:
	ClientOrderID_t clientOrderID_;

public:
	const OrderRejectReason orderRejectReason;

private:
	Text_t text;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.3 Order Modified"
*/
template<class MsgT>
struct [[gnu::packed]] OrderModified : public Header<MsgT, MsgT::OrderModified> {
	using Header_t=Header<MsgT, MsgT::OrderModified>;
	using bitfields_to_type_map=optional::OrderModified::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	explicit OrderModified() noexcept(true);
	explicit OrderModified(SeqNum_t seqNum, ClientOrderID_t const &clID, Price_t p, Side s, common::Quantity_t ordQty) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	Price_t limitPrice() const noexcept(true);
	void limitPrice(Price_t p) noexcept(true);

	common::Quantity_t orderQty() const noexcept(true);
	void orderQty(common::Quantity_t i) noexcept(true);

	Side side() const noexcept(true);
	void side(Side i) noexcept(true);

private:
	ClientOrderID_t clientOrderID_;
	uint64_t orderID;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.4 Order Restated"
*/
template<class MsgT>
struct [[gnu::packed]] OrderRestated : public Header<MsgT, MsgT::OrderRestated> {
	using Header_t=Header<MsgT, MsgT::OrderRestated>;
	using bitfields_to_type_map=optional::OrderRestated::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	explicit OrderRestated(SeqNum_t seqNum, ClientOrderID_t const &clID) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

private:
	ClientOrderID_t clientOrderID_;
	uint64_t orderID;
	RestatementReason restatementReason;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.5 User Modify Rejected"
*/
template<class MsgT>
struct [[gnu::packed]] UserModifyRejected : public Header<MsgT, MsgT::UserModifyRejected> {
	using Header_t=Header<MsgT, MsgT::UserModifyRejected>;
	using bitfields_to_type_map=optional::UserModifyRejected::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	UserModifyRejected() noexcept(true);
	explicit UserModifyRejected(SeqNum_t seqNum, ClientOrderID_t const &clID, OrderRejectReason orr) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

private:
	ClientOrderID_t clientOrderID_;
	uint64_t orderID;

public:
	const OrderRejectReason modifyRejectReason;

private:
	Text_t text;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.6 Order Cancelled"
*/
template<class MsgT>
struct [[gnu::packed]] OrderCancelled : public Header<MsgT, MsgT::OrderCancelled> {
	using Header_t=Header<MsgT, MsgT::OrderCancelled>;
	using bitfields_to_type_map=optional::OrderCancelled::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	explicit OrderCancelled() noexcept(true);
	explicit OrderCancelled(SeqNum_t seqNum, ClientOrderID_t const &clID, OrderRejectReason orr, Symbol_t const &i, Price_t p, Side s, common::Quantity_t ls, common::Quantity_t ordQty) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	Side side() const noexcept(true);

	common::Quantity_t orderQty() const noexcept(true);

	common::Quantity_t leavesQty() const noexcept(true);

	common::Quantity_t lastShares() const noexcept(true);

	Price_t lastPrice() const noexcept(true);

	Symbol_t const &symbol() const noexcept(true);
	void symbol(Symbol_t const &i) noexcept(true);

private:
	ClientOrderID_t clientOrderID_;

public:
	const OrderRejectReason cancelReason;

private:
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.7 Cancel Rejected"
*/
template<class MsgT>
struct [[gnu::packed]] CancelRejected : public Header<MsgT, MsgT::CancelRejected> {
	using Header_t=Header<MsgT, MsgT::CancelRejected>;
	using bitfields_to_type_map=optional::CancelRejected::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	CancelRejected() noexcept(true);
	CancelRejected(SeqNum_t seqNum, ClientOrderID_t const &clID, OrderRejectReason crr) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

private:
	ClientOrderID_t clientOrderID_;
	
public:
	const OrderRejectReason cancelRejectReason;

private:
	Text_t text;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.8 Order Execution"
*/
template<class MsgT>
struct [[gnu::packed]] OrderExecution : public Header<MsgT, MsgT::OrderExecution> {
	using Header_t=Header<MsgT, MsgT::OrderExecution>;
	using bitfields_to_type_map=optional::OrderExecution::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	OrderExecution() noexcept(true);
	OrderExecution(SeqNum_t seqNum, ClientOrderID_t const &clID, Price_t const price, SecurityID_t const &instID, Side s) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	SecurityID_t const &instrumentID() const noexcept(true);
	void instrumentID(SecurityID_t const &i) noexcept(true);

	Price_t executedPrice() const noexcept(true) {
		return lastPx;
	}
	void executedPrice(Price_t p) noexcept(true) {
		lastPx=p;
	}

	common::Quantity_t executedQty() const noexcept(true) {
		return lastShares;
	}
	void executedQty(common::Quantity_t eq) noexcept(true) {
		lastShares=eq;
	}

	common::Quantity_t leavesQty() const noexcept(true) {
		return leavesQty_;
	}
	void leavesQty(common::Quantity_t eq) noexcept(true) {
		leavesQty_=eq;
	}

	Side side() const noexcept(true);
	void side(Side s) noexcept(true);

private:
	ClientOrderID_t clientOrderID_;
	uint64_t execID;
	common::Quantity_t lastShares;
	Price_t lastPx;
	common::Quantity_t leavesQty_;
	BaseLiquidityIndicator baseLiquidityIndicator;
	SubLiquidityIndicator subLiquidityIndicator;
	SPrice_t accessFee;
	ContraBroker_t contraBroker;
	bitfields_to_type_map bitfields;
};

/**
	Section: "4.2.9 Trade Cancel or Correct"
*/
template<class MsgT>
struct [[gnu::packed]] TradeCancelOrCorrect : public Header<MsgT, MsgT::TradeCancelOrCorrect> {
	using Header_t=Header<MsgT, MsgT::TradeCancelOrCorrect>;
	using bitfields_to_type_map=optional::TradeCancelOrCorrect::bitfields_to_type_map;
	using bitfields_tags_type=bitfields_to_type_map::bitfields_tags_type;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const DateTime_t transactionTime;

	explicit TradeCancelOrCorrect(SeqNum_t seqNum, ClientOrderID_t const &clID) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

private:
	ClientOrderID_t clientOrderID_;
	uint64_t orderID;
	uint64_t execRefID;
	Side side;
	BaseLiquidityIndicator baseLiquidityIndicator;
	ClearingFirm_t clearingFirm;
	ClearingAccount_t clearingAccount;
	Quantity_t lastShares;
	Price_t lastPx;
	Price_t correctedPrice;
	DateTime_t origTime;
	bitfields_to_type_map bitfields;
};

} } } } }

#include "messages_impl.hpp"

#endif
