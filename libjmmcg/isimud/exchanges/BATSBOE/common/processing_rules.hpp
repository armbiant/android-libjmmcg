#ifndef ISIMUD_EXCHANGES_COMMON_MIT_processing_rules_hpp
#define ISIMUD_EXCHANGES_COMMON_MIT_processing_rules_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../common/processing_rules.hpp"

#include LIBJMMCG_PERFECT_HASH_MASK_FOR_BATSBOE_EXCHG_MSGS_HDR
#include LIBJMMCG_PERFECT_HASH_MASK_FOR_FIX_HDR

#include "core/msm.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

/// A client-to-BATSBOE-protocol message handler.
/**
	The behaviour of this handler is derived from the specification in  [1, 2].
	[1] "BATS Chi-X Europe Binary Order Entry Specification Version 1.44"
	[2] "BZX Exchange US Equities BOE Specification Version 1.8.6"
*/
template<class SrcMsgDetails, class DestMsgDetails, class SktT>
class client_to_exchange_transformations final
	: public exchanges::common::message_responses<
		  SrcMsgDetails,
		  DestMsgDetails,
		  SktT> {
public:
	using base_t= exchanges::common::message_responses<
		SrcMsgDetails,
		DestMsgDetails,
		SktT>;
	using src_msg_details_t= typename base_t::src_msg_details_t;
	using dest_msg_details_t= typename base_t::dest_msg_details_t;
	using socket_t= typename base_t::socket_t;

	explicit client_to_exchange_transformations(typename dest_msg_details_t::SeqNum_t sn) noexcept(true)
		: sequenceNumber(sn) {
	}

	/**
		\return False to continue processing messages, true otherwise.
	*/
	REALLY_FORCE_INLINE [[gnu::flatten]] bool process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& dest_skt);

	std::string to_string() const noexcept(false);

private:
	struct state_machine_t;
	using machine= libjmmcg::msm::hash::machine<state_machine_t>;

	typename dest_msg_details_t::SeqNum_t sequenceNumber;
	const machine msm{std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber)};
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false);

/// A BATSBOE-protocol-to-client message handler.
/**
	The behaviour of this handler is derived from the specification in  [1, 2].
	[1] "BATS Chi-X Europe Binary Order Entry Specification Version 1.44"
	[2] "BZX Exchange US Equities BOE Specification Version 1.8.6"
*/
template<class SrcMsgDetails, class DestMsgDetails, class SktT>
class exchange_to_client_transformations final
	: public exchanges::common::message_responses<
		  SrcMsgDetails,
		  DestMsgDetails,
		  SktT> {
public:
	using base_t= exchanges::common::message_responses<
		SrcMsgDetails,
		DestMsgDetails,
		SktT>;
	using src_msg_details_t= typename base_t::src_msg_details_t;
	using dest_msg_details_t= typename base_t::dest_msg_details_t;
	using socket_t= typename base_t::socket_t;

	explicit constexpr exchange_to_client_transformations(typename dest_msg_details_t::SeqNum_t seq_num) noexcept(true)
		: sequenceNumber(seq_num) {
	}

	/**
	 * \param client_skt	This is a client-connection not a socket because a client may not be connected yet, but we must still respond to heartbeats from the server and other messages from the server.
	 * \return False to continue processing messages, true otherwise.
	 */
	REALLY_FORCE_INLINE [[gnu::flatten]] bool process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn);

	std::string to_string() const noexcept(false);

private:
	struct state_machine_t;
	using machine= libjmmcg::msm::hash::machine<state_machine_t>;

	typename dest_msg_details_t::SeqNum_t sequenceNumber;
	const machine msm{std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber), std::ref(sequenceNumber)};
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false);

/// A simple, BATSBOE-protocol exchange simulator.
/**
	The behaviour of this simulator is a simplification derived from the specification in [1, 2].
	[1] "BATS Chi-X Europe Binary Order Entry Specification Version 1.44"
	[2] "BZX Exchange US Equities BOE Specification Version 1.8.6"
*/
template<class SrcMsgDetails, class SktT>
class simulator_responses final : public exchanges::common::simulator_responses<SrcMsgDetails, SktT> {
public:
	using base_t= exchanges::common::simulator_responses<SrcMsgDetails, SktT>;
	using msg_details_t= typename base_t::msg_details_t;
	using socket_t= typename base_t::socket_t;

	/// The only valid symbol, all others will be rejected.
	static inline constexpr const typename msg_details_t::Symbol_t symbol{"4HKS39"};
	/// The only valid instrument, all others will be rejected.
	static inline constexpr const typename msg_details_t::SecurityID_t instrumentID{"GB00BH4HKS39"};
	/// An invalid symbol, that will be rejected.
	static inline constexpr const typename msg_details_t::Symbol_t invalidSymbol{"595859"};
	/// An invalid instrument, that will be rejected.
	static inline constexpr const typename msg_details_t::SecurityID_t invalidInstrumentID{"GB0000595859"};

	simulator_responses() noexcept(true)
		: base_t(), msm{std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this)} {
	}
	simulator_responses(simulator_responses const& sr) noexcept(true)
		: base_t(sr), msm{std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this)} {
	}

	/**
		\return False to continue processing messages, true otherwise.
	*/
	bool process_msg(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& client_skt);

	[[nodiscard]] std::string to_string() const noexcept(false) override;

private:
	struct state_machine_t;
	using machine= libjmmcg::msm::hash::machine<state_machine_t>;

	const machine msm;
};

template<class SrcMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, simulator_responses<SrcMsgDetails, SktT> const& ec) noexcept(false);

}}}}}

#include "processing_rules_impl.hpp"

#endif
