/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "messages.hpp"

#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

std::ostream &
MsgTypes::to_stream(std::ostream &os) noexcept(false) {
	os
		<<"Version: '" ISIMUD_FIX_EXCHANGE_VERSION "'"
		", FIXML Data-types: '" ISIMUD_FIXML_DATATYPES_HDR_GENERATED_DATE "'"
		", FIXML Fields-base: '" ISIMUD_FIXML_FIELDS_BASE_HDR_GENERATED_DATE "'"
		", FIXML Fields-impl: '" ISIMUD_FIXML_FIELDS_IMPL_HDR_GENERATED_DATE "'"
		", FIXML Fields-fast: '" ISIMUD_FIXML_FIELDS_FAST_HDR_GENERATED_DATE "'"
		", VersionSpecific::fix_template_to_msg_type: '";
	common::to_stream(os, VersionSpecific::fix_template_to_msg_type);
	os<<"'"
		", VersionSpecific::fix_template_msg_type_offset: '"<<VersionSpecific::fix_template_msg_type_offset<<"'"
		", VersionSpecific::MsgVer: '"<<VersionSpecific::MsgVer<<"'"
		", max_size_of_fix_message="<<exchanges::FIX::common::max_size_of_fix_message
		<<", CheckSumLength="<<exchanges::FIX::common::CheckSumLength
		<<", minimum size of client-to-exchange message="<<min_size_client_to_exchange_msg
		<<", maximum size of client-to-exchange message="<<max_size_client_to_exchange_msg
		<<", minimum size of exchange-to-client message="<<min_size_exchange_to_client_msg
		<<", maximum size of exchange-to-client message="<<max_size_exchange_to_client_msg
		<<", minimum message size="<<min_msg_size
		<<", maximum message size="<<max_msg_size
		<<", header size="<<header_t_size
		<<", implied_decimal_places="<<implied_decimal_places;
	return os;
}

} } } } }
