
#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_ORDER_BASE_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_FIXML_ORDER_BASE_HPP

/******************************************************************************
**
**	$Header$
**
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: 2020-03-11T03:21:43.358Z

#include "fixml-components-base-v5_0sp2.hpp"

#define ISIMUD_FIXML_ORDER_BASE_HDR_GENERATED_DATE "2020-03-11T03:21:43.358Z"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

/**
	Only required attributes have been generated.
*/
struct ExecutionReportElements {
	Instrument_Block_t Instrmt;

};

/**
	Only required attributes have been generated.
*/
struct OrderCancelRejectElements {

};

/**
	Only required attributes have been generated.
*/
struct NewOrderSingleElements {
	Instrument_Block_t Instrmt;
	OrderQtyData_Block_t OrdQty;

};

/**
	Only required attributes have been generated.
*/
struct OrderCancelRequestElements {
	Instrument_Block_t Instrmt;

};

/**
	Only required attributes have been generated.
*/
struct OrderCancelReplaceRequestElements {
	Instrument_Block_t Instrmt;
	OrderQtyData_Block_t OrdQty;

};

/**
	Only required attributes have been generated.
*/
struct OrderStatusRequestElements {
	Instrument_Block_t Instrmt;

};

/**
	Only required attributes have been generated.
*/
struct DontKnowTradeElements {
	Instrument_Block_t Instrmt;
	OrderQtyData_Block_t OrdQty;

};

/**
	Only required attributes have been generated.
*/
struct ExecutionAckElements {
	Instrument_Block_t Instrmt;

};

/**
	Only required attributes have been generated.
*/
struct FillsGrpElements {

};

/**
	Only required attributes have been generated.
*/
struct OrderEventGrpElements {

};

struct ExecutionReportAttributes {
		OrderID_t OrdID;
		ExecID_t ExecID;
		ExecType_t ExecTyp;
		OrdStatus_t Stat;
		Side_t Side;
		LeavesQty_t LeavesQty;
		CumQty_t CumQty;
	
};

struct OrderCancelRejectAttributes {
		OrderID_t OrdID;
		ClOrdID_t ID;
		OrdStatus_t Stat;
		CxlRejResponseTo_t CxlRejRspTo;
	
};

struct NewOrderSingleAttributes {
		ClOrdID_t ID;
		Side_t Side;
		TransactTime_t TxnTm;
		OrdType_t Typ;
	
};

struct OrderCancelRequestAttributes {
		ClOrdID_t ID;
		Side_t Side;
		TransactTime_t TxnTm;
	
};

struct OrderCancelReplaceRequestAttributes {
		ClOrdID_t ID;
		Side_t Side;
		TransactTime_t TxnTm;
		OrdType_t Typ;
	
};

struct OrderStatusRequestAttributes {
		Side_t Side;
	
};

struct DontKnowTradeAttributes {
		OrderID_t OrdID;
		ExecID_t ExecID;
		DKReason_t DkRsn;
		Side_t Side;
	
};

struct ExecutionAckAttributes {
		OrderID_t OrdID;
		ExecAckStatus_t ExecAckStat;
		ExecID_t ExecID;
		Side_t Side;
	
};

struct FillsGrpAttributes {
	
};

struct OrderEventGrpAttributes {
	
};

/**
	ExecutionReport can be found in Volume 4 of the
						specification
*/
struct ExecutionReport_message_t : public Abstract_message_t {
	ExecutionReportElements ExecutionReportElements_;
	ExecutionReportAttributes ExecutionReportAttributes_;

};

/**
	OrderCancelReject can be found in Volume 4 of the
						specification
*/
struct OrderCancelReject_message_t : public Abstract_message_t {
	OrderCancelRejectElements OrderCancelRejectElements_;
	OrderCancelRejectAttributes OrderCancelRejectAttributes_;

};

/**
	NewOrderSingle can be found in Volume 4 of the
						specification
*/
struct NewOrderSingle_message_t : public Abstract_message_t {
	NewOrderSingleElements NewOrderSingleElements_;
	NewOrderSingleAttributes NewOrderSingleAttributes_;

};

/**
	OrderCancelRequest can be found in Volume 4 of the
						specification
*/
struct OrderCancelRequest_message_t : public Abstract_message_t {
	OrderCancelRequestElements OrderCancelRequestElements_;
	OrderCancelRequestAttributes OrderCancelRequestAttributes_;

};

/**
	OrderCancelReplaceRequest can be found in Volume 4 of the
						specification
*/
struct OrderCancelReplaceRequest_message_t : public Abstract_message_t {
	OrderCancelReplaceRequestElements OrderCancelReplaceRequestElements_;
	OrderCancelReplaceRequestAttributes OrderCancelReplaceRequestAttributes_;

};

/**
	OrderStatusRequest can be found in Volume 4 of the
						specification
*/
struct OrderStatusRequest_message_t : public Abstract_message_t {
	OrderStatusRequestElements OrderStatusRequestElements_;
	OrderStatusRequestAttributes OrderStatusRequestAttributes_;

};

/**
	DontKnowTrade can be found in Volume 4 of the
						specification
*/
struct DontKnowTrade_message_t : public Abstract_message_t {
	DontKnowTradeElements DontKnowTradeElements_;
	DontKnowTradeAttributes DontKnowTradeAttributes_;

};

/**
	ExecutionAck can be found in Volume 4 of the
						specification
*/
struct ExecutionAck_message_t : public Abstract_message_t {
	ExecutionAckElements ExecutionAckElements_;
	ExecutionAckAttributes ExecutionAckAttributes_;

};

/**
	
*/
struct FillsGrp_Block_t : public  {
	FillsGrpElements FillsGrpElements_;
	FillsGrpAttributes FillsGrpAttributes_;

};

/**
	
*/
struct OrderEventGrp_Block_t : public  {
	OrderEventGrpElements OrderEventGrpElements_;
	OrderEventGrpAttributes OrderEventGrpAttributes_;

};

using ExecRpt=ExecutionReport_message_t;

using OrdCxlRej=OrderCancelReject_message_t;

using Order=NewOrderSingle_message_t;

using OrdCxlReq=OrderCancelRequest_message_t;

using OrdCxlRplcReq=OrderCancelReplaceRequest_message_t;

using OrdStatReq=OrderStatusRequest_message_t;

using DkTrd=DontKnowTrade_message_t;

using ExecAck=ExecutionAck_message_t;

namespace FIXML_ORDER_BASE {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_FIXML_ORDER_BASE_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif
