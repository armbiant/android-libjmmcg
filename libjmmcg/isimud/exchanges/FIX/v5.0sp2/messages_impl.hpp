/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX {

namespace common {

template<> inline __stdcall
Message<v5_0sp2::NewOrderSingleSpecific>::Message(SeqNum_t seqNum, ClientOrderID_t const &clID, OrderType const oT, TIF const t, Side const s, SecurityID_t instID, Quantity_t ordQty, Price_t p, ExDestination_t mic) noexcept(true)
: Header_t() {
	underlying_fix_data_buffer::iterator data=set_sequence_num_int<MsgTypes_t::NewOrderSingle>(seqNum);
	data=write_ClOrdID(clID, data);
	data=add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data=static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data=add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i=convert(instID, data, this->data_.size()-(data-this->data_.begin()));
	assert(i>0);
	data+=i;
	data=add_field_tag<FieldsFast::OrdType>(data);
	*data=static_cast<std::underlying_type<OrderType>::type>(oT);
	++data;
	data=add_field_tag<FieldsFast::TimeInForce>(data);
	*data=static_cast<std::underlying_type<TIF>::type>(t);
	++data;
	data=add_field_tag<FieldsFast::Price>(data);
	const std::size_t j=convert(p, data, this->data_.size()-(data-this->data_.begin()));
	assert(j>0);
	data+=j;
	data=add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k=convert(ordQty, data, this->data_.size()-(data-this->data_.begin()));
	assert(k>0);
	data+=k;
	data=add_field_tag<FieldsFast::Side>(data);
	*data=std::underlying_type<Side>::type(s);
	++data;
	data=write_ExDest(mic, data);
	data=write_TransactTime(data);
// TODO	msg.orderRejectCode();
	assert(static_cast<size_type>(data-static_cast<pointer>(begin_string))<sizeof(Header));
	finalise_msg(data);
}

template<> inline __stdcall
Message<v5_0sp2::ExecutionReportSpecific>::Message(SeqNum_t seqNum, ClientOrderID_t const &clID, ExecType eT, Price_t const price, SecurityID_t instID, Side s, Quantity_t execdQty, Quantity_t leavesQty, ExDestination_t mic) noexcept(true)
: Header_t() {
	underlying_fix_data_buffer::iterator data=set_sequence_num_int<MsgTypes_t::ExecutionReport>(seqNum);
	data=write_ClOrdID(clID, data);
	data=add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data=static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data=add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i=convert(instID, data, this->data_.size()-(data-this->data_.begin()));
	assert(i>0);
	data+=i;
	data=add_field_tag<FieldsFast::ExecType>(data);
	*data=static_cast<std::underlying_type<ExecType>::type>(eT);
	++data;
	data=add_field_tag<FieldsFast::Price>(data);
	const std::size_t j=convert(price, data, this->data_.size()-(data-this->data_.begin()));
	assert(j>0);
	data+=j;
	data=add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k=convert(execdQty, data, this->data_.size()-(data-this->data_.begin()));
	assert(k>0);
	data+=k;
	data=add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l=convert(leavesQty, data, this->data_.size()-(data-this->data_.begin()));
	assert(l>0);
	data+=l;
	data=add_field_tag<FieldsFast::Side>(data);
	*data=std::underlying_type<Side>::type(s);
	++data;
	data=add_field_tag<FieldsFast::OrdStatus>(data);
	*data=static_cast<std::underlying_type<common::OrdStatus>::type>(common::OrdStatus::New);
	++data;
	data=write_ExDest(mic, data);
	data=write_TransactTime(data);
// TODO	msg.orderRejectCode();
	assert(static_cast<size_type>(data-static_cast<pointer>(begin_string))<sizeof(Header));
	finalise_msg(data);
}

}

namespace v5_0sp2 {

template<class Msg>
inline constexpr bool
VersionSpecific::is_valid(Msg const &) noexcept(true) {
	return true;
}

template<class Msg>
inline bool
LogoutRequestSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO
	return has_MsgType;
}

template<class Msg>
inline bool
HeartbeatSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	return has_MsgType;
}

template<class Msg>
inline bool
LogonSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	const bool has_username=m.template search<common::FieldsFast::Username>();
	const bool has_password=m.template search<common::FieldsFast::Password>();
	return has_MsgType && has_username && has_password;
}

template<class Msg>
inline bool
NewOrderSingleSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	const bool has_ClOrdID=m.template search<common::FieldsFast::ClOrdID>();
	const bool has_Side=m.template search<common::FieldsFast::Side>();
	const bool has_TransactTime=m.template search<common::FieldsFast::TransactTime>();
	const bool has_OrderQty=m.template search<common::FieldsFast::OrderQty>();
	const bool has_OrdType=m.template search<common::FieldsFast::OrdType>();
	return has_MsgType && has_ClOrdID && has_Side && has_TransactTime && has_OrderQty && has_OrdType && m.has_ExDest() && m.has_ISIN();
}

template<class Msg>
inline bool
OrderCancelRequestSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	const bool has_ClOrdID=m.template search<common::FieldsFast::ClOrdID>();
	const bool has_OrderQty=m.template search<common::FieldsFast::OrderQty>();
	return has_MsgType && has_ClOrdID && has_OrderQty && m.has_ExDest();
}

template<class Msg>
inline bool
OrderCancelReplaceSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	const bool has_SenderCompID=m.template search<common::FieldsFast::SenderCompID>();
	const bool has_TargetCompID=m.template search<common::FieldsFast::TargetCompID>();
	const bool has_SendingTime=m.template search<common::FieldsFast::SendingTime>();
	const bool has_MsgSeqNum=m.template search<common::FieldsFast::MsgSeqNum>();
	const bool has_ClOrdID=m.template search<common::FieldsFast::ClOrdID>();
	const bool has_Side=m.template search<common::FieldsFast::Side>();
	const bool has_TransactTime=m.template search<common::FieldsFast::TransactTime>();
	const bool has_OrderQty=m.template search<common::FieldsFast::OrderQty>();
	const bool has_OrdType=m.template search<common::FieldsFast::OrdType>();
	return has_MsgType && has_SenderCompID && has_TargetCompID && has_SendingTime && has_MsgSeqNum && has_ClOrdID && m.has_ISIN() && has_Side && has_TransactTime && has_OrderQty && has_OrdType;
}

template<class Msg>
inline bool
ExecutionReportSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	const bool has_MsgSeqNum=m.template search<common::FieldsFast::MsgSeqNum>();
	const bool has_ExecType=m.template search<common::FieldsFast::ExecType>();
	const bool has_OrdStatus=m.template search<common::FieldsFast::OrdStatus>();
	const bool has_Side=m.template search<common::FieldsFast::Side>();
	const bool has_OrderQty=m.template search<common::FieldsFast::OrderQty>();
	const bool has_LeavesQty=m.template search<common::FieldsFast::LeavesQty>();
	// TODO verify that the required fields are present.
	return has_MsgType && has_MsgSeqNum && has_ExecType && has_OrdStatus && m.has_ISIN() && has_Side && has_OrderQty && has_LeavesQty;
}

template<class Msg>
inline bool
LogonReplySpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
LogoutSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
CancelOrderSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
BusinessMessageRejectSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	const bool has_RefSeqNum=m.template search<common::FieldsFast::RefSeqNum>();
	// TODO verify that the required fields are present.
	return has_MsgType && has_RefSeqNum;
}

template<class Msg>
inline bool
ModifyOrderSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
TradeCaptureReportSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
TradeCaptureReportRequestSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
OrderAcknowledgementSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
OrderRejectedSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	return has_MsgType;
}

template<class Msg>
inline bool
OrderModifiedSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
OrderRestatedSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
UserModifyRejectedSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
OrderCancelledSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
CancelRejectedSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	const bool has_MsgSeqNum=m.template search<common::FieldsFast::MsgSeqNum>();
	const bool has_ClOrdID=m.template search<common::FieldsFast::ClOrdID>();
	// TODO verify that the required fields are present.
	return has_MsgType && has_MsgSeqNum && has_ClOrdID;
}

template<class Msg>
inline bool
OrderExecutionSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
TradeCaptureReportAckSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
TradeCaptureReportRequestAckSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

template<class Msg>
inline bool
RejectSpecific::is_valid(Msg const &m) noexcept(true) {
	auto const &msg_type=m.template find<common::FieldsFast::MsgType>();
	const bool has_MsgType=std::equal(msg_type.first, msg_type.second, Msg::msg_version_t::msg_type::value);
	// TODO verify that the required fields are present.
	return has_MsgType;
}

}

} } } }
