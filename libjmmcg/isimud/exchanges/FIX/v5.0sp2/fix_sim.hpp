#ifndef ISIMUD_EXCHANGES_FIX_v5_0sp2_fix_sim_hpp
#define ISIMUD_EXCHANGES_FIX_v5_0sp2_fix_sim_hpp

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "messages.hpp"

#include "../common/connectivity_policy.hpp"
#include "../common/processing_rules.hpp"

#include "../../common/socket_type.hpp"
#include "../../common/thread_traits.hpp"

#include "core/socket_server.hpp"
#include "core/socket_server_manager.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

using simulator_t=libjmmcg::socket::svr<
	libjmmcg::socket::server_manager::loopback<
		common::simulator_responses<MsgTypes, exchanges::common::socket_type::socket_t>,
		common::server_hb_t<typename MsgTypes::ServerHeartbeat_t>,
		exchanges::common::socket_type::socket_t
	>
>;

} } } } }

#endif
