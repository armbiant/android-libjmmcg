<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nl "<xsl:text>
</xsl:text>">
	<!ENTITY tab "<xsl:text>&#x9;</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fm="http://www.fixprotocol.org/FIXML-5-0-SP2/METADATA">
<xsl:output omit-xml-declaration="yes" />
<xsl:param name="fixml_datatypes_hdr" required="yes" as="xs:string"/>
<xsl:param name="fix_version" required="yes" as="xs:string"/>
<xsl:include href="generate_bits.xsl"/>
<xsl:template name="make_char_enum_operator">
	<xsl:param name="enum_name" required="yes"/>
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
operator<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os, <xsl:value-of select="$enum_name"/> msg) noexcept(false) {
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text>std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$enum_name"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(msg);
	return os;
}
</xsl:template>
<xsl:template name="gen_simple_enum_tags">
	<xsl:param name="appInfo" required="yes"/>
	<xsl:for-each select="$appInfo/fm:EnumDoc">
		&tab;<xsl:call-template name="sanitise_identifier">
			<xsl:with-param name="str" select="."/>
		</xsl:call-template>=&apos;<xsl:value-of select="@value"/>&apos;<xsl:if test="string(./following-sibling::*[1])">,&nl;</xsl:if>
	</xsl:for-each>
</xsl:template>
<xsl:template name="make_mpl_enum">
	<xsl:param name="enum_name" required="yes"/>
	<xsl:param name="enum_tags" required="yes"/>
	<xsl:param name="appInfo" required="yes"/>
	<xsl:variable name="max_attr_len" select="max($enum_tags/@value/string-length(.))"/>
enum class <xsl:value-of select="$enum_name"/> : <xsl:call-template name="select_builtin">
		<xsl:with-param name="max_attr_len" select="$max_attr_len"/>
	</xsl:call-template> {&nl;
	<xsl:choose>
		<xsl:when test="$max_attr_len=1">
			<xsl:call-template name="gen_simple_enum_tags">
				<xsl:with-param name="appInfo" select="$appInfo"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="$appInfo/fm:EnumDoc">
					<xsl:for-each select="$appInfo/fm:EnumDoc">
						&tab;<xsl:call-template name="sanitise_identifier">
							<xsl:with-param name="str" select="."/>
						</xsl:call-template><xsl:text>=libjmmcg::enum_tags::mpl::to_tag</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>
						<xsl:call-template name="string_to_quoted_chars">
							<xsl:with-param name="text" select="@value"/>
						</xsl:call-template>
						<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text>::value,</xsl:text>&nl;
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<!-- There is no description, so we have to use the tag-value as the tag-name... -->
					<xsl:for-each select="$enum_tags/@value">
						tag_<xsl:value-of select="."/><xsl:text>=libjmmcg::enum_tags::mpl::to_tag</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>
						<xsl:call-template name="string_to_quoted_chars">
							<xsl:with-param name="text" select="."/>
						</xsl:call-template>
						<xsl:text disable-output-escaping="yes">&gt;</xsl:text><xsl:text>::value,</xsl:text>&nl;
					</xsl:for-each>
				</xsl:otherwise>
			</xsl:choose>
	MatchAll=std::numeric_limits<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:call-template name="select_builtin">
		<xsl:with-param name="max_attr_len" select="$max_attr_len"/>
	</xsl:call-template>
	<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::max()-1,	///<xsl:text disable-output-escaping="yes">&lt;</xsl:text> For the meta-state machine to allow a catch-all rule to reject anything unhandled.
	Exit=std::numeric_limits<xsl:text disable-output-escaping="yes">&lt;</xsl:text>
	<xsl:call-template name="select_builtin">
		<xsl:with-param name="max_attr_len" select="$max_attr_len"/>
	</xsl:call-template>
	<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::max()	///<xsl:text disable-output-escaping="yes">&lt;</xsl:text> For the meta-state machine: the exit state to exit the msm.
		</xsl:otherwise>
	</xsl:choose>
};</xsl:template>
<xsl:template name="make_char_enum">
	<xsl:param name="enum_name" required="yes"/>
	<xsl:param name="appInfo" required="yes"/>
enum class <xsl:value-of select="$enum_name"/> : char {
<xsl:call-template name="gen_simple_enum_tags">
	<xsl:with-param name="appInfo" select="$appInfo"/>
</xsl:call-template>
};
<xsl:call-template name="make_char_enum_operator">
	<xsl:with-param name="enum_name" select="$enum_name"/>
</xsl:call-template>
</xsl:template>
<xsl:template name="generate_cpp_type">
	<xsl:param name="FIX_name"/>
	<xsl:param name="FIX_type_node"/>
	<xsl:param name="FIX_appinfo"/>
	<xsl:variable name="FIX_type" select="$FIX_type_node/@base"/>
	<xsl:choose>
		<xsl:when test="$FIX_name='char'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>char</xsl:text>;
		</xsl:when>
		<xsl:when test="$FIX_name='Boolean'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>private_::boolean_type</xsl:text>;
		</xsl:when>
		<xsl:when test="$FIX_name='Country'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>common::ctry_codes::alpha_2::ISO_3166_Country_Codes</xsl:text>;
		</xsl:when>
		<xsl:when test="$FIX_name='Currency'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>common::ccy_codes::ISO_4217_Currency_Codes</xsl:text>;
		</xsl:when>
		<xsl:when test="$FIX_name='Exchange'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>common::mic_codes::ISO_10383_MIC_Codes</xsl:text>;
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="$FIX_type='xs:nonNegativeInteger'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>unsigned long</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:positiveInteger'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>unsigned long</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:integer'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>long</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:base64Binary'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>private_::base64Binary_type</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:decimal'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>double</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:dateTime'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>std::string</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:time'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>std::string</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:language'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>private_::rfc_1766_language_code_type</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:IDREF'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>private_::IDREF_type</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:ID'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>private_::ID_type</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:date'">
using <xsl:value-of select="$FIX_name"/>=<xsl:text>std::array</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text>char, 8</xsl:text><xsl:text disable-output-escaping="yes">&gt;</xsl:text>;
				</xsl:when>
				<xsl:when test="$FIX_type='xs:string'">
					<xsl:choose>
						<xsl:when test="$FIX_type_node/xs:enumeration">
							<xsl:variable name="max_attr_len" select="max($FIX_type_node/xs:enumeration/@value/string-length(.))"/>
							<xsl:choose>
								<xsl:when test="$max_attr_len lt 9">
									<xsl:call-template name="make_mpl_enum">
										<xsl:with-param name="enum_name" select="$FIX_name"/>
										<xsl:with-param name="enum_tags" select="$FIX_type_node/xs:enumeration"/>
										<xsl:with-param name="appInfo" select="$FIX_appinfo"/>
									</xsl:call-template>
									<xsl:choose>
										<xsl:when test="$max_attr_len gt 1">
											<!-- We only need to generate the operators if char is not the underlying type. -->
											<xsl:call-template name="make_mpl_enum_operators">
												<xsl:with-param name="enum_name" select="$FIX_name"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
	
											<xsl:call-template name="make_char_enum_operator">
												<xsl:with-param name="enum_name" select="$FIX_name"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:otherwise>
using <xsl:value-of select="$FIX_name"/>=<xsl:text>std::string</xsl:text>;
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
using <xsl:value-of select="$FIX_name"/>=<xsl:text>std::string</xsl:text>;
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$FIX_type='char'">
					<xsl:choose>
						<xsl:when test="$FIX_appinfo/fm:EnumDoc">
							<xsl:call-template name="make_char_enum">
								<xsl:with-param name="enum_name" select="$FIX_name"/>
								<xsl:with-param name="appInfo" select="$FIX_appinfo"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
using <xsl:value-of select="$FIX_name"/>=<xsl:text>char</xsl:text>;
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
using <xsl:value-of select="$FIX_name"/>=<xsl:value-of select="$FIX_type"/>;
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="/">
#ifndef ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP
#define ISIMUD_EXCHANGES_FIX_V5_0_SP2_<xsl:call-template name="base_filename"/>_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
//	Generated: <xsl:value-of select="current-dateTime()"/>
<!--
	Developed using:
	- <a href="http://www.freeformatter.com/xsl-transformer.html"/>
	- <a href="https://xslttest.appspot.com"/>
	- <a href="https://www.iso20022.org/10383/iso-10383-market-identifier-code"/>
-->
#include "<xsl:value-of select="$fixml_datatypes_hdr"/>"

#include "core/enum_as_char_array.hpp"

#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>cstdint<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>iostream<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>limits<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>string<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>vector<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
#include <xsl:text disable-output-escaping="yes">&lt;</xsl:text>type_traits<xsl:text disable-output-escaping="yes">&gt;</xsl:text>

#define ISIMUD_<xsl:call-template name="base_filename"/>_HDR_GENERATED_DATE "<xsl:value-of select="current-dateTime()"/>"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace <xsl:value-of select="$fix_version"/> {

namespace private_ {

	using base64Binary_type=std::vector<xsl:text disable-output-escaping="yes">&lt;</xsl:text>std::uint64_t<xsl:text disable-output-escaping="yes">&gt;</xsl:text>;

	/**
		\todo Need to implement this properly..

		See:
		-# http://books.xmlschemata.org/relaxng/ch19-77191.html
		-# https://datatracker.ietf.org/doc/rfc1766/
	*/
	using rfc_1766_language_code_type=std::string;

	/**
		\todo Need to implement this properly..

		See:
		-# https://www.w3.org/TR/xml-id/
		-# http://books.xmlschemata.org/relaxng/ch19-77151.html
	*/
	using ID_type=std::string;

	/**
		\todo Need to implement this properly..

		See:
		-# https://www.w3.org/TR/xml-idref/
		-# http://books.xmlschemata.org/relaxng/ch19-77159.html
	*/
	using IDREF_type=std::string;

}

<xsl:for-each select="/xs:schema/xs:simpleType">
/// <xsl:value-of select="tokenize(xs:annotation/xs:documentation, '\n')[normalize-space()][1]"/>
/**
	<xsl:value-of select="xs:annotation/xs:documentation"/>
*/<xsl:call-template name="generate_cpp_type">
		<xsl:with-param name="FIX_name" select="@name"/>
		<xsl:with-param name="FIX_type_node" select="xs:restriction"/>
		<xsl:with-param name="FIX_appinfo" select="xs:annotation/xs:appinfo"/>
	</xsl:call-template>
</xsl:for-each>
namespace <xsl:call-template name="base_filename"/> {

inline std::string
to_string() noexcept(false) {
	return std::string("Built with FIX data-types generated on: '" ISIMUD_<xsl:call-template name="base_filename"/>_HDR_GENERATED_DATE "'.");
}

}

} } } } }

#endif&nl;
</xsl:template>
</xsl:stylesheet>
