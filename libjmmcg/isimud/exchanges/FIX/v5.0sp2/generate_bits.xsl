<?xml version="1.0" encoding="UTF-8"?>
<!--
	Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public
	License as published by the Free Software Foundation; either
	version 2.1 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nl "<xsl:text>
</xsl:text>">
	<!ENTITY tab "<xsl:text>&#9;</xsl:text>">
]>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:output omit-xml-declaration="yes" />

<xsl:template name="base_filename">
	<xsl:variable name="str" select="upper-case(replace(tokenize(base-uri(), '/')[last()], '[-\./]', '_'))"/>
	<xsl:value-of select="substring($str, 1, string-length($str)-12)"/>
</xsl:template>

<!-- There is a lot of faffing around as the FIX spec is shite: the tag-names we want to generate have got quotes and all sorts of nonsense around them, commas, spaces, etc in them, etc, which we need to remove.... -->
<xsl:template name="output_tag_name">
	<xsl:param name="raw_tag_name1" required="yes"/>
	<xsl:variable name="raw_tag_name" select="replace(replace($raw_tag_name1, '[ -\./,]', '_'), '__', '_')"/>
	<xsl:variable name="last_char" select="substring($raw_tag_name, string-length($raw_tag_name), string-length($raw_tag_name)+1)"/>
	<xsl:variable name="first_char" select="substring($raw_tag_name, 1, 1)"/>
	<xsl:choose>
		<xsl:when test="$last_char='_'">
			<xsl:variable name="tag_name" select="substring($raw_tag_name, 1, string-length($raw_tag_name)-1)"/>
			<xsl:choose>
				<xsl:when test="$first_char='_'">
					<xsl:variable name="tag_name1" select="substring($tag_name, 2, string-length($tag_name))"/>
						<xsl:value-of select="$tag_name1"/>
				</xsl:when>
				<xsl:when test="matches($first_char, '[0-9]')">
					<!-- C++ identifiers cannot begin with a character. -->
					<xsl:variable name="tag_name1" select="substring($tag_name, 1, string-length($tag_name))"/>
						<xsl:text>tag_</xsl:text><xsl:value-of select="$tag_name1"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$tag_name"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="$first_char='_'">
					<xsl:variable name="tag_name" select="substring($raw_tag_name, 2, string-length($raw_tag_name))"/>
					<xsl:value-of select="$tag_name"/>
				</xsl:when>
				<xsl:when test="matches($first_char, '[0-9]')">
					<!-- C++ identifiers cannot begin with a character. -->
					<xsl:variable name="tag_name" select="substring($raw_tag_name, 1, string-length($raw_tag_name))"/>
						<xsl:text>tag_</xsl:text><xsl:value-of select="$tag_name"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$raw_tag_name"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="sanitise_identifier">
	<xsl:param name="str" required="yes"/>
	<!-- Comments are identified in the input xsd by finding a "(" or "-" in the text. Yes this is crap, FIX lads... -->
	<xsl:variable name="paren_comment" select="tokenize($str, '\(')[2]"/>
	<xsl:variable name="minus_comment" select="tokenize($str, '-')[2]"/>
	<xsl:choose>
		<xsl:when test="string-length($paren_comment)>0">
			<!-- Yay! We have a comment! Output it! -->
			<xsl:text>/// </xsl:text>(<xsl:value-of select="$paren_comment"/>&nl;&tab;
			<xsl:choose>
				<xsl:when test="starts-with($str, '(')">
					<!-- The name begins with a parenthesis, so is no a comment... -->
					<xsl:variable name="raw_tag_name" select="replace($str, '[ -\./,:]', '_')"/>
					<xsl:call-template name="output_tag_name">
						<xsl:with-param name="raw_tag_name1" select="$raw_tag_name"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="raw_tag_name" select="replace(tokenize($str, '\(')[1], '[ -\./,:]', '_')"/>
					<xsl:call-template name="output_tag_name">
						<xsl:with-param name="raw_tag_name1" select="$raw_tag_name"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="string-length($minus_comment)>0">
			<!-- Yay! We have a comment! Output it! -->
			<xsl:text>/// </xsl:text><xsl:value-of select="$minus_comment"/>&nl;&tab;
			<!-- Now the tag-name. -->
			<xsl:variable name="remove_rhs_comment" select="substring-before($str, ' -')"/>
			<xsl:choose>
				<xsl:when test="string-length(substring-before($remove_rhs_comment, ' -')) gt 0">
					<xsl:variable name="raw_tag_name" select="replace(tokenize($remove_rhs_comment, '\(')[1], '[ -\./,:]', '_')"/>
					<xsl:call-template name="output_tag_name">
						<xsl:with-param name="raw_tag_name1" select="$raw_tag_name"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="raw_tag_name" select="replace(tokenize($str, '\(')[1], '[ -\./,:]', '_')"/>
					<xsl:call-template name="output_tag_name">
						<xsl:with-param name="raw_tag_name1" select="$raw_tag_name"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<!-- Not a comment, it is a tag-name. -->
			<xsl:variable name="remove_rhs_comment" select="substring-before($str, ' -')"/>
			<xsl:choose>
				<xsl:when test="string-length(substring-before($remove_rhs_comment, ' -')) gt 0">
					<xsl:variable name="raw_tag_name" select="replace(tokenize($remove_rhs_comment, '\(')[1], '[ -\./,:]', '_')"/>
					<xsl:call-template name="output_tag_name">
						<xsl:with-param name="raw_tag_name1" select="$raw_tag_name"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="raw_tag_name" select="replace(tokenize($str, '\(')[1], '[ -\./,:]', '_')"/>
					<xsl:call-template name="output_tag_name">
						<xsl:with-param name="raw_tag_name1" select="$raw_tag_name"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="string_to_quoted_chars">
	<xsl:param name="text" required="yes" as="xs:string"/>
	<xsl:choose>
		<xsl:when test="string-length($text)>1">
			<xsl:variable name="letter" select="substring($text, 1, 1)" />
			<xsl:text>&apos;</xsl:text><xsl:value-of select="$letter" /><xsl:text>&apos;, </xsl:text>
			<xsl:call-template name="string_to_quoted_chars">
				<xsl:with-param name="text" select="substring-after($text, $letter)" />
			</xsl:call-template>
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="letter" select="substring($text, 1, 1)" />
			<xsl:text>&apos;</xsl:text><xsl:value-of select="$letter" /><xsl:text>&apos;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="no_last_sep_in_list">
	<xsl:param name="sep" required="yes" as="xs:string"/>
	<xsl:if test="string(./following-sibling::*[1])"><xsl:value-of select="$sep"/></xsl:if>
</xsl:template>

<xsl:template name="select_builtin">
	<xsl:param name="max_attr_len" required="yes" as="xs:integer"/>
	<xsl:choose>
		<xsl:when test="$max_attr_len=1">
			<xsl:text>char</xsl:text>
		</xsl:when>
		<xsl:when test="$max_attr_len=2">
			<xsl:text>std::uint16_t</xsl:text>
		</xsl:when>
		<xsl:when test="$max_attr_len le 4">
			<xsl:text>std::uint32_t</xsl:text>
		</xsl:when>
		<xsl:when test="$max_attr_len le 8">
			<xsl:text>std::uint64_t</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			FIXME
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="make_mpl_enum_operators">
	<xsl:param name="enum_name" required="yes" as="xs:string"/>
using <xsl:value-of select="$enum_name"/>_ul_t=std::underlying_type<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$enum_name"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>::type;

constexpr inline <xsl:value-of select="$enum_name"/>
operator|(const <xsl:value-of select="$enum_name"/> lhs, const <xsl:value-of select="$enum_name"/> rhs) noexcept(true) {
	return static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$enum_name"/><xsl:text disable-output-escaping="yes">&gt;</xsl:text>(
		static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$enum_name"/>_ul_t<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(lhs)|static_cast<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$enum_name"/>_ul_t<xsl:text disable-output-escaping="yes">&gt;</xsl:text>(rhs)
	);
}

template<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$enum_name"/> Msg<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
to_stream(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os) noexcept(false) {
	const constexpr auto <xsl:text disable-output-escaping="yes">&amp;</xsl:text>msg_str=libjmmcg::enum_tags::mpl::to_array<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:value-of select="$enum_name"/>, Msg<xsl:text disable-output-escaping="yes">&gt;</xsl:text>::value;
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>msg_str;
	return os;
}

inline std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>
operator<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>(std::ostream <xsl:text disable-output-escaping="yes">&amp;</xsl:text>os, <xsl:value-of select="$enum_name"/> msg) noexcept(false) {
	union convertor {
		char str_[sizeof(<xsl:value-of select="$enum_name"/>)]{};
		<xsl:value-of select="$enum_name"/> msg_;
	};
	convertor conv;
	conv.msg_=msg;
	os<xsl:text disable-output-escaping="yes">&lt;</xsl:text><xsl:text disable-output-escaping="yes">&lt;</xsl:text>conv.str_;
	return os;
}
</xsl:template>

</xsl:stylesheet>
