#ifndef ISIMUD_EXCHANGES_FIX_v5_0sp2_FIX_CLIENT_HPP
#define ISIMUD_EXCHANGES_FIX_v5_0sp2_FIX_CLIENT_HPP
/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "fix.hpp"

#include "core/application.hpp"
#include "core/ave_deviation_meter.hpp"
#include "core/latency_timestamps.hpp"
#include "core/lock_mem.hpp"
#include "core/socket_wrapper.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace v5_0sp2 {

class fix_client : public libjmmcg::application {
public:
	using base_t= libjmmcg::application;
	using timed_results_t= libjmmcg::ave_deviation_meter<double>;

	/// The FIX message to be sent to the server.
	ALIGN_TO_L1_CACHE static inline constexpr const FIX::common::underlying_fix_data_buffer fix_buffer= {
		"8=FIX.5.0\0019=170\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB0000595859\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=MTAA\00110=153\001"};
	static inline MsgTypes::NewOrderSingle_t const& as_fix_msg_type= reinterpret_cast<MsgTypes::NewOrderSingle_t const&>(*fix_buffer.begin());
	/// A buffer for the received fix message.
	MsgTypes::BusinessMessageReject_t receive_fix_msg;

	fix_client(boost::asio::ip::address const& addr, unsigned short port_num, libjmmcg::socket::socket_priority priority, std::size_t incoming_cpu, libjmmcg::latency_timestamps_itf& ts) noexcept(false);

	/// Send the FIX message, as_fix_msg_type to the connected server, once.
	void send() noexcept(false);
	/// Receive the response, if any, from the server.
	[[nodiscard]] bool receive() noexcept(false);

	/// In a loop, send() then receive() num_messages messages.
	/**
	 * \see send(), receive()
	 */
	void in_order_tx_rx(unsigned long num_messages) noexcept(false);
	/// Gather statisitcs for the call to in_order_tx_rx().
	/**
	 * Assuming a suitable implementation of libjmmcg::latency_timestamps_itf has been provided in the ctor.
	 */
	std::pair<timed_results_t, bool> in_order_tx_rx_stats(unsigned long num_loops, unsigned short loops_for_conv, double perc_conv_estimate) noexcept(false);

	friend std::ostream&
	operator<<(std::ostream& os, fix_client const& fc) noexcept(false);

private:
	connection_t client;
	libjmmcg::latency_timestamps_itf& timestamps;
};

}}}}}

#include "fix_client_impl.hpp"

#endif
