/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <functional>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
struct client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::state_machine_t : libjmmcg::msm::unroll::state_transition_table<state_machine_t> {
	using msm_base_t=libjmmcg::msm::unroll::state_transition_table<state_machine_t>;
	using row_t=libjmmcg::msm::unroll::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
	using transition_table=typename msm_base_t::template rows<
// TODO finish implementing this...
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			src_msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename src_msg_details_t::Reject_t, false, typename dest_msg_details_t::MsgTypes_t>,
			dest_msg_details_t::Reject_t::static_type
		>
	>;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline bool
client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>
::process_msg(typename src_msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, socket_t &dest_skt) {
	typename src_msg_details_t::Header_t const &hdr=reinterpret_cast<typename src_msg_details_t::Header_t const &>(buff);
	const auto last_state=msm.process(hdr.type(), buff, exchg_skt, dest_skt);
	return last_state==dest_msg_details_t::Exit;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::string
client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT>::to_string() const noexcept(false) {
	return base_t::to_string();
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::ostream &
operator<<(std::ostream &os, client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
struct exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::business_state_machine_t : libjmmcg::msm::unroll::state_transition_table<business_state_machine_t> {
	using msm_base_t=libjmmcg::msm::unroll::state_transition_table<business_state_machine_t>;
	using row_t=libjmmcg::msm::unroll::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
	using transition_table=typename msm_base_t::template rows<
// TODO finish implementing this...
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			src_msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename dest_msg_details_t::Reject_t, true, typename dest_msg_details_t::MsgTypes_t>,
			dest_msg_details_t::Reject_t::static_type
		>
	>;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
struct exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::admin_state_machine_t : libjmmcg::msm::unroll::state_transition_table<admin_state_machine_t> {
	using msm_base_t=libjmmcg::msm::unroll::state_transition_table<admin_state_machine_t>;
	using row_t=libjmmcg::msm::unroll::row_types<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
/**
 * \todo Finish implementing the msm: add more states.
 */
	using transition_table=typename msm_base_t::template rows<
		/**
			The response to a server Heartbeat is a Heartbeat.
		*/
		typename row_t::template row<
			src_msg_details_t::ServerHeartbeat_t::static_type,
			typename base_t::template just_send_to_exchg<typename src_msg_details_t::ClientHeartbeat_t>,
			src_msg_details_t::ClientHeartbeat_t::static_type
		>,
		typename row_t::template row<
			src_msg_details_t::Logout_t::static_type,
			typename base_t::template no_op<typename dest_msg_details_t::MsgTypes_t>, // TODO
			dest_msg_details_t::MsgTypes_t::Exit
		>,
// TODO finish implementing this...
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			src_msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template match_all_response<business_machine>,
			dest_msg_details_t::Reject_t::static_type
		>
	>;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
template<class ClientCxn>
inline bool
exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>
::process_msg(typename src_msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, ClientCxn &client_cxn) {
	typename src_msg_details_t::Header_t const &hdr=reinterpret_cast<typename src_msg_details_t::Header_t const &>(buff);
	const auto last_state=admin_msm.process(hdr.type(), buff, exchg_skt, client_cxn);
	return last_state==dest_msg_details_t::Exit;
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::string
exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT>::to_string() const noexcept(false) {
	return base_t::to_string();
}

template<class SrcMsgDetails, class DestMsgDetails, class SktT> inline std::ostream &
operator<<(std::ostream &os, exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

template<class SrcMsgDetails, class SktT>
struct simulator_responses<SrcMsgDetails, SktT>::state_machine_t : libjmmcg::msm::unroll::state_transition_table<state_machine_t> {
	using msm_base_t=libjmmcg::msm::unroll::state_transition_table<state_machine_t>;
	using row_t=libjmmcg::msm::unroll::row_types<typename msg_details_t::MsgTypes_t, typename msg_details_t::MsgTypes_t>;
/**
 * \todo Finish implementing the msm: add more states.
 */
	using transition_table=typename msm_base_t::template rows<
		/**
			The response to a Heartbeat is nothing.
		*/
		typename row_t::template row<
			msg_details_t::ClientHeartbeat_t::static_type,
			typename base_t::template no_op<typename msg_details_t::MsgTypes_t>,
			msg_details_t::ServerHeartbeat_t::static_type
		>,
		/// Reject any message that has not been recognised.
		typename row_t::template row<
			msg_details_t::MsgTypes_t::MatchAll,
			typename base_t::template send_specified_msg<typename msg_details_t::Reject_t, true, typename msg_details_t::MsgTypes_t>,
			msg_details_t::Reject_t::static_type
		>
	>;
};

template<class SrcMsgDetails, class SktT>
inline bool
simulator_responses<SrcMsgDetails, SktT>::process_msg(typename msg_details_t::msg_buffer_t const &buff, socket_t &exchg_skt, socket_t &client_skt) {
	++(this->sequenceNumber);
	typename msg_details_t::Header_t const &hdr=reinterpret_cast<typename msg_details_t::Header_t const &>(buff);
	const auto last_state=msm.process(hdr.type(), buff, exchg_skt, client_skt);
	return last_state==msg_details_t::MsgTypes_t::Exit;
}

template<class SrcMsgDetails, class SktT> inline std::string
simulator_responses<SrcMsgDetails, SktT>::to_string() const noexcept(false) {
	return base_t::to_string();
}

template<class SrcMsgDetails, class SktT> inline std::ostream &
operator<<(std::ostream &os, simulator_responses<SrcMsgDetails, SktT> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

} } } } }
