/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

template<class MsgVer>
inline constexpr __stdcall Header<MsgVer>::Header() noexcept(true) {
	using fix_template_to_msg_type_t= char[MsgVer::fix_template_msg_type_offset];
	libjmmcg::memcpy_opt(
		MsgVer::fix_template_to_msg_type,
		reinterpret_cast<fix_template_to_msg_type_t&>(*begin_string));
}

template<class MsgVer>
inline constexpr typename Header<MsgVer>::size_type
Header<MsgVer>::length() const noexcept(true) {
	const size_type body_size= libjmmcg::fromstring<size_type>(body_length_value, sizeof(body_length_value));
	std::ptrdiff_t const ret= msg_type_tag - begin_string + body_size + libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::size + CheckSumLength + sizeof(Separator);
	assert(ret > 0);
	return static_cast<size_type>(ret);
}

template<class MsgVer>
template<FieldsFast field>
inline constexpr field_str_range_t __fastcall Header<MsgVer>::find() noexcept(true) {
	using body_type= char[max_size_of_fix_message];
	[[maybe_unused]] const_pointer const start= this->body_length_tag;	// Convert from an offset in the class to a pointer-to-char in memory.
	pointer field_ptr= this->body_length_tag;
	const constexpr size_type field_len= libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::size;
	field_ptr= const_cast<pointer>(libjmmcg::strstr_opt(reinterpret_cast<body_type&>(*field_ptr), libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::value_no_null));
	assert(field_ptr);
	assert(static_cast<size_type>(field_ptr - start) < this->length());
	field_ptr+= field_len;
	assert(static_cast<size_type>(field_ptr - start) < this->length());
	pointer end= field_ptr;
	end= const_cast<pointer>(libjmmcg::strchr_opt<Separator>(reinterpret_cast<body_type&>(*end)) + 1);
	assert((end - field_ptr) > 0);
	assert(static_cast<size_type>(end - field_ptr) < this->length());
	assert(static_cast<size_type>(end - start) < this->length());
	return field_str_range_t(field_ptr, end - 1);
}

template<class MsgVer>
template<FieldsFast field>
inline constexpr field_str_const_range_t __fastcall Header<MsgVer>::find() const noexcept(true) {
	using body_type= char const[max_size_of_fix_message];
	[[maybe_unused]] const_pointer const start= this->body_length_tag;	// Convert from an offset in the class to a pointer-to-char in memory.
	const_pointer field_ptr= this->body_length_tag;
	const constexpr size_type field_len= libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::size;
	field_ptr= libjmmcg::strstr_opt(reinterpret_cast<body_type&>(*field_ptr), libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::value_no_null);
	assert(field_ptr);
	assert(static_cast<size_type>(field_ptr - start) < this->length());
	field_ptr+= field_len;
	assert(static_cast<size_type>(field_ptr - start) < this->length());
	const_pointer end= field_ptr;
	end= libjmmcg::strchr_opt<Separator>(reinterpret_cast<body_type&>(*end)) + 1;
	assert((end - field_ptr) > 0);
	assert(static_cast<size_type>(end - field_ptr) < this->length());
	assert(static_cast<size_type>(end - start) < this->length());
	return field_str_const_range_t(field_ptr, end - 1);
}

template<class MsgVer>
constexpr inline field_str_const_range_t
Header<MsgVer>::find_ExDestination() const noexcept(true) {
	switch(type()) {
	case MsgTypes_t::CrossOrderCancelReplaceRequest:
	case MsgTypes_t::ExecutionReport:
	case MsgTypes_t::MultilegOrderCancelReplace:
	case MsgTypes_t::NewOrderCross:
	case MsgTypes_t::NewOrderMultileg:
	case MsgTypes_t::NewOrderSingle:
	case MsgTypes_t::OrderCancelReject:
	case MsgTypes_t::OrderCancelReplaceRequest:
	case MsgTypes_t::OrderCancelRequest:
	case MsgTypes_t::Quote:
	case MsgTypes_t::QuoteResponse:
	case MsgTypes_t::QuoteStatusReport:
		return find<FieldsFast::ExDestination>();
	default:
		return field_str_const_range_t{};
	}
}

template<class MsgVer>
constexpr inline ExDestination_t
Header<MsgVer>::find_MIC() const noexcept(true) {
	auto const& exdest= find_ExDestination();
	if(exdest != field_str_const_range_t{}) {
		return libjmmcg::enum_tags::convert<ExDestination_t>(reinterpret_cast<std::array<char, sizeof(ExDestination_t)> const&>(*exdest.first));
	} else {
		return static_cast<ExDestination_t>(std::numeric_limits<std::underlying_type<ExDestination_t>::type>::max());
	}
}

template<class MsgVer>
inline typename Header<MsgVer>::MsgTypes_t
Header<MsgVer>::type() const noexcept(true) {
	field_str_const_range_t const& type_str= find<FieldsFast::MsgType>();
	if((type_str.second - type_str.first) == 1) {
		const auto type_int= static_cast<MsgTypes_t>(*type_str.first);
		return static_cast<MsgTypes_t>(type_int);
	} else {
		union {
			char c[2];
			MsgTypes_t type_int;
		} conv;
		conv.c[0]= type_str.first[0];
		conv.c[1]= type_str.first[1];
		return static_cast<MsgTypes_t>(conv.type_int);
	}
}

template<class MsgVer>
inline checksum_t
Header<MsgVer>::generate_checksum(size_type body_len) const noexcept(true) {
	checksum_t fix_chk_sum{};
	assert(body_len <= std::numeric_limits<std::ptrdiff_t>::max());
	static_assert((std::numeric_limits<element_type>::max() * max_size_of_fix_message) <= std::numeric_limits<std::size_t>::max(), "The maximum value that the checksum type can hold could overflow in the summation. Use a wider type for 'checksum'.");
	const std::size_t checksum= std::accumulate(this->begin_string, std::next(this->begin_string, static_cast<std::ptrdiff_t>(body_len) + 1), 0UL);
	const std::size_t checksum_mod= (checksum % 256UL);
	BOOST_MPL_ASSERT_RELATION(CheckSumLength, ==, 3);
	[[maybe_unused]] const std::size_t i= libjmmcg::tostring_zero_pad_right_justify<10, CheckSumLength>(static_cast<std::uint32_t>(checksum_mod), reinterpret_cast<char(&)[CheckSumLength]>(*fix_chk_sum.data()));
	assert(i == CheckSumLength);
	return fix_chk_sum;
}

template<class MsgVer>
constexpr inline bool
Header<MsgVer>::is_checksum_valid(size_type body_len, const_pointer start_of_checksum_value) const noexcept(true) {
	auto const& computed_chksum= generate_checksum(body_len + sizeof(begin_string) + sizeof(body_length_tag) + sizeof(body_length_value));
	return std::equal(computed_chksum.begin(), computed_chksum.end() - 1, start_of_checksum_value);
}

template<class MsgVer>
constexpr inline bool
Header<MsgVer>::is_valid() const noexcept(true) {
	const bool has_msg_ver= std::equal(this->begin_string, this->body_length_tag, msg_version_t::MsgVer);
	if(has_msg_ver) {
		auto constexpr const& msg_str= libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::BodyLength>::value;
		const bool has_msg_size= std::equal(this->body_length_tag, this->body_length_value, msg_str);
		if(has_msg_size) {
			const_pointer next_field= this->body_length_value;
			const size_type body_size= libjmmcg::fromstring<size_type>(next_field, sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) - 1);
			underlying_fix_data_buffer::const_iterator end_of_body_size= this->msg_type_tag;
			if(
				body_size > 0
				&& (body_size < (max_size_of_fix_message - sizeof(msg_version_t::MsgVer) - msg_version_t::fix_template_body_length_offset - sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) - 1))
				&& ((end_of_body_size - this->begin_string) > 0)) {
				auto const& checksum_str= libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::value;
				underlying_fix_data_buffer::const_iterator start_of_checksum= end_of_body_size + body_size;
				const bool has_checksum= std::equal(start_of_checksum, start_of_checksum + libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::size, checksum_str);
				if(has_checksum) {
					const bool has_terminal_separator= (*(end_of_body_size + body_size + libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::size + CheckSumLength) == Separator);
					if(has_terminal_separator) {
						const_pointer start_of_checksum_value= end_of_body_size + body_size + libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::size;
						return is_checksum_valid(body_size, start_of_checksum_value);
					}
				}
			}
		}
	}
	return false;
}

template<class MsgVer>
template<typename Header<MsgVer>::MsgTypes_t MsgType>
inline constexpr underlying_fix_data_buffer::iterator
Header<MsgVer>::set_header() noexcept(true) {
	using JMMCG_FIX_MSG_SEQ_NUM_TAG_t= libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::MsgSeqNum>::element_type_no_null;
	pointer data= static_cast<pointer>(begin_string) + MsgVer::fix_template_msg_type_offset;
	libjmmcg::memcpy_opt(
		libjmmcg::enum_tags::mpl::to_array<MsgTypes_t, MsgType>::value_no_null,
		reinterpret_cast<typename libjmmcg::enum_tags::mpl::to_array<MsgTypes_t, MsgType>::element_type_no_null&>(*data));
	data+= libjmmcg::enum_tags::mpl::to_array<MsgTypes_t, MsgType>::size;
	[[maybe_unused]] const_pointer const start= this->body_length_tag;	// Convert from an offset in the class to a pointer-to-char in memory.
	assert(static_cast<size_type>(data - start) < this->length());
	libjmmcg::memcpy_opt(
		libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::MsgSeqNum>::value_no_null,
		reinterpret_cast<JMMCG_FIX_MSG_SEQ_NUM_TAG_t&>(*data));
	data+= libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::MsgSeqNum>::size;
	assert(static_cast<size_type>(data - start) < this->length());
	return data;
}

template<class MsgVer>
template<typename Header<MsgVer>::MsgTypes_t MsgType>
inline constexpr underlying_fix_data_buffer::iterator
Header<MsgVer>::set_sequence_num_int(SeqNum_t seq_num) noexcept(true) {
	pointer data= set_header<MsgType>();
	[[maybe_unused]] const_pointer const start= this->body_length_tag;	// Convert from an offset in the class to a pointer-to-char in memory.
	assert(static_cast<size_type>(data - start) < this->length());
	const std::size_t i= libjmmcg::tostring(
		seq_num,
		data,
		data_.size() - MsgVer::fix_template_msg_type_offset - libjmmcg::enum_tags::mpl::to_array<MsgTypes_t, MsgType>::size - libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::MsgSeqNum>::size + 1);
	assert(i > 0);
	data+= i;
	assert(static_cast<size_type>(data - start) < this->length());
	return data;
}

template<class MsgVer>
template<typename Header<MsgVer>::MsgTypes_t MsgType, class SrcMsg, class Ret>
inline constexpr Ret
Header<MsgVer>::set_sequence_num(SrcMsg const& msg) noexcept(true) {
	return this->set_sequence_num_int<MsgType>(msg.sequenceNumber);
}

template<class MsgVer>
constexpr inline __stdcall Message<MsgVer>::Message() noexcept(true)
	: Header_t() {
}

template<class MsgVer>
constexpr inline __stdcall Message<MsgVer>::Message(logon_args_t const&) noexcept(true)
	: Header_t() {
}

template<class MsgVer>
constexpr inline __stdcall Message<MsgVer>::Message(logoff_args_t const&) noexcept(true)
	: Header_t() {
}

template<class MsgVer>
template<std::size_t N, typename std::enable_if<(N > 1), std::size_t>::type Sz>
constexpr inline Message<MsgVer>::Message(SeqNum_t ref_seq_num, RejectCode_t rc, char const (&msg)[N]) noexcept(true)
	: Header_t() {
	static_assert(sizeof(RejectCode_t) <= sizeof(std::uint64_t));
	pointer data= this->template set_header<MsgTypes_t::Reject>();
	data= add_field_tag<FieldsFast::RefSeqNum>(data);
	std::size_t i= libjmmcg::tostring(
		ref_seq_num,
		data,
		this->data_.size() - (data - static_cast<pointer>(this->begin_string)));
	assert(i > 0);
	data+= i;
	data= add_field_tag<FieldsFast::Text>(data);
	libjmmcg::memcpy_opt(msg, reinterpret_cast<char(&)[Sz]>(*data));
	data+= Sz;
	data= add_field_tag<FieldsFast::SessionRejectReason>(data);
	i= libjmmcg::tostring(
		static_cast<std::uint64_t>(rc),
		data,
		this->data_.size() - (data - static_cast<pointer>(this->begin_string)));
	assert(i > 0);
	data+= i;
	assert(static_cast<size_type>(data - static_cast<pointer>(this->begin_string)) < sizeof(Header_t));
	finalise_msg(data);
}

template<class MsgVer>
constexpr inline typename Message<MsgVer>::size_type __fastcall Message<MsgVer>::size() const noexcept(true) {
	const size_type body_size= libjmmcg::fromstring<size_type>(this->body_length_value, sizeof(this->body_length_value));
	assert(body_size < this->length());
	return body_size;
}

template<class MsgVer>
template<FieldsFast field>
constexpr inline bool __fastcall Message<MsgVer>::search() const noexcept(true) {
	using body_type= char const[max_size_of_fix_message];
	const_pointer const start= this->msg_type_tag;	 // Convert from an offset in the class to a pointer-to-char in memory.
	const_pointer field_ptr= this->body_length_tag;
	field_ptr= libjmmcg::strstr_opt(reinterpret_cast<body_type&>(*field_ptr), libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::value_no_null);
	assert(field_ptr >= start);
	assert(static_cast<size_type>(field_ptr - start) <= this->size());
	return static_cast<size_type>(field_ptr - start) <= this->size();
}

template<class MsgVer>
constexpr inline bool
Message<MsgVer>::is_valid() const noexcept(true) {
	return Header_t::is_valid() && msg_version_t::is_valid(*this);
}

template<class MsgVer>
constexpr inline bool
Message<MsgVer>::has_ISIN() const noexcept(true) {
	const bool has_SecurityIDSource= this->template search<common::FieldsFast::SecurityIDSource>();
	const bool is_ISIN= (has_SecurityIDSource && (this->template find<common::FieldsFast::SecurityIDSource>().first[0] == static_cast<std::underlying_type<common::SecurityIDSource>::type>(common::SecurityIDSource::ISIN)));
	const bool has_ISIN= (is_ISIN && this->template search<common::FieldsFast::SecurityID>());
	return has_ISIN;
}

template<class MsgVer>
constexpr inline bool
Message<MsgVer>::has_ExDest() const noexcept(true) {
	const bool has_ExDestinationIDSource= this->template search<common::FieldsFast::ExDestinationIDSource>();
	const bool is_MIC= (has_ExDestinationIDSource && (this->template find<common::FieldsFast::ExDestinationIDSource>().first[0] == static_cast<std::underlying_type<common::ExDestinationIDSource>::type>(common::ExDestinationIDSource::MIC)));
	const bool has_DestinationID= (is_MIC && this->template search<common::FieldsFast::ExDestination>());
	return has_DestinationID;
}

template<class MsgVer>
constexpr common::ClientOrderID_t
Message<MsgVer>::clientOrderID() const noexcept(true) {
	field_str_const_range_t const& type_str= this->template find<FieldsFast::ClOrdID>();
	assert((type_str.second - type_str.first) > 0);
	common::ClientOrderID_t ret{};
	assert(static_cast<size_type>(type_str.second - type_str.first) <= ret.max_size());
	std::copy(type_str.first, type_str.second, ret.begin());
	return ret;
}

template<class MsgVer>
constexpr inline common::Side
Message<MsgVer>::side() const noexcept(true) {
	field_str_const_range_t const& type_str= this->template find<FieldsFast::Side>();
	assert((type_str.second - type_str.first) == 1);
	return static_cast<common::Side>(*type_str.first);
}

template<class MsgVer>
constexpr inline Price_t
Message<MsgVer>::limitPrice() const noexcept(true) {
	field_str_const_range_t const& type_str= this->template find<FieldsFast::Price>();
	assert((type_str.second - type_str.first) > 0);
	const auto price= libjmmcg::fromstring<Price_t>(type_str.first, type_str.second - type_str.first);
	return price * implied_decimal_places;
}

template<class MsgVer>
constexpr inline Quantity_t
Message<MsgVer>::orderQty() const noexcept(true) {
	field_str_const_range_t const& type_str= this->template find<FieldsFast::OrderQty>();
	assert((type_str.second - type_str.first) > 0);
	const auto qty= libjmmcg::fromstring<Quantity_t>(type_str.first, type_str.second - type_str.first - 1);
	return qty;
}

template<class MsgVer>
constexpr inline void
Message<MsgVer>::orderQty(Quantity_t q) noexcept(true) {
	field_str_range_t type_str= this->template find<FieldsFast::OrderQty>();
	assert((type_str.second - type_str.first) > 0);
	[[maybe_unused]] std::size_t const i= libjmmcg::tostring(q, type_str.first, type_str.second - type_str.first);
	assert(i > 0);
}

template<class MsgVer>
constexpr inline SecurityID_t
Message<MsgVer>::instrumentID() const noexcept(true) {
	field_str_const_range_t const& type_str= this->template find<FieldsFast::SecurityID>();
	assert((type_str.second - type_str.first) > 0);
	SecurityID_t ret{};
	assert(static_cast<size_type>(type_str.second - type_str.first) <= ret.max_size());
	std::copy(type_str.first, type_str.second, ret.begin());
	return ret;
}

template<class MsgVer>
template<class OutIter>
inline OutIter
Message<MsgVer>::write_ClOrdID(ClientOrderID_t const& clID, OutIter data) noexcept(true) {
	data= add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(clID, reinterpret_cast<ClientOrderID_t&>(*data));
	data+= clID.size() - 1;
	return data;
}

template<class MsgVer>
template<class OutIter>
inline OutIter
Message<MsgVer>::write_TransactTime(OutIter data) noexcept(true) {
	data= add_field_tag<FieldsFast::TransactTime>(data);
	boost::posix_time::ptime const timeUTC= boost::posix_time::microsec_clock::universal_time();
	auto const& as_ISO_str= boost::posix_time::to_iso_extended_string(timeUTC);
	auto const end= std::copy(as_ISO_str.begin(), as_ISO_str.end(), data);
	assert(end > data);
	return end;
}

template<class MsgVer>
template<class OutIter>
inline OutIter
Message<MsgVer>::write_ExDest(ExDestination_t mic, OutIter data) noexcept(true) {
	data= add_field_tag<FieldsFast::ExDestinationIDSource>(data);
	*data= static_cast<std::underlying_type<ExDestinationIDSource>::type>(ExDestinationIDSource::MIC);
	++data;
	data= add_field_tag<FieldsFast::ExDestination>(data);
	libjmmcg::memcpy_opt(
		reinterpret_cast<std::array<char, sizeof(ExDestination_t)> const&>(mic),
		reinterpret_cast<std::array<char, sizeof(ExDestination_t)>&>(*data));
	data+= sizeof(ExDestination_t);
	return data;
}

template<class MsgVer>
template<FieldsFast field>
constexpr inline underlying_fix_data_buffer::iterator
Message<MsgVer>::add_field_tag(underlying_fix_data_buffer::iterator data) noexcept(true) {
	libjmmcg::memcpy_opt(
		libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::value_no_null,
		reinterpret_cast<typename libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::element_type_no_null&>(*data));
	data+= libjmmcg::enum_tags::mpl::to_array<FieldsFast, field>::size;
	return data;
}

template<class MsgVer>
inline std::string
Message<MsgVer>::to_string() const noexcept(false) {
	std::stringstream ss;
	ss
		<< this->begin_string
		<< this->body_length_tag
		<< this->body_length_value
		<< this->msg_type_tag
		<< this->data_;
	return ss.str();
}

template<class MsgVer>
constexpr inline void
Message<MsgVer>::finalise_msg(underlying_fix_data_buffer::iterator data) noexcept(true) {
	assert(static_cast<size_type>(data - static_cast<pointer>(this->begin_string)) < sizeof(Message));
	const std::ptrdiff_t length= data - this->begin_string - MsgVer::fix_template_body_length_offset - sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) + 1;
	assert(length > 0);
	BOOST_MPL_ASSERT_RELATION(sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) - 1, ==, 3);
	[[maybe_unused]] const std::size_t i= libjmmcg::tostring_zero_pad_right_justify<10, sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) - 1>(
		static_cast<std::uint32_t>(length),
		reinterpret_cast<char(&)[sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL) - 1]>(*(this->body_length_value)));
	assert(i > 0 && static_cast<size_type>(i) < sizeof(JMMCG_FIX_MSG_BODY_LENGTH_NULL));
	underlying_fix_data_buffer::iterator ptr= data;
	using JMMCG_FIX_MSG_CHECKSUM_TAG_t= libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::element_type_no_null;
	libjmmcg::memcpy_opt(libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::value_no_null, reinterpret_cast<JMMCG_FIX_MSG_CHECKSUM_TAG_t&>(*ptr));
	ptr+= libjmmcg::enum_tags::mpl::to_array<FieldsFast, FieldsFast::CheckSum>::size;
	checksum_t const& checksum= this->generate_checksum(data - this->begin_string);
	libjmmcg::memcpy_opt(checksum, reinterpret_cast<checksum_t&>(*ptr));
	ptr+= CheckSumLength;
	*ptr= Separator;
	assert(this->is_valid());
	assert((ptr - this->begin_string) > 0);
	assert(static_cast<size_type>(ptr - static_cast<pointer>(this->begin_string)) < this->length());
	assert(static_cast<size_type>(ptr - static_cast<pointer>(this->begin_string)) < this->data_.size());
	assert(this->is_valid());
}

template<class MsgVer>
inline std::ostream&
operator<<(std::ostream& os, Message<MsgVer> const& m) {
	os << m.to_string();
	return os;
}

}}}}}
