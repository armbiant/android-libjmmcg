/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

inline client_order_id::client_order_id(element_type::prefix_type const& prefix) noexcept(true) {
	order_id_.converter_.prefix_= prefix;
}

inline client_order_id::element_type::value_type const&
client_order_id::get() const noexcept(true) {
	auto const lfsr_value= lfsr_.next();
	static_assert(lfsr_t::size <= (sizeof(unsigned long) * 8));
	const unsigned long id= lfsr_value.to_ulong();
	// TODO	libjmmcg::tostring(static_cast<std::uint64_t>(id), order_id_.converter_.order_id_.id_); ?
	fmt::format_to_n(order_id_.converter_.order_id_.id_.begin(), id_size, "{:0>{}}", id, id_size);
	return order_id_.converter_.element_;
	return order_id_.converter_.element_;
}

}}}}}
