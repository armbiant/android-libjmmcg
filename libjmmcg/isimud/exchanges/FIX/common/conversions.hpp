#ifndef ISIMUD_EXCHANGES_FIX_common_CONVERSIONS_HPP
#define ISIMUD_EXCHANGES_FIX_common_CONVERSIONS_HPP
/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "types.hpp"

#include "core/memops.hpp"
#include "core/ttypes.hpp"

#include <algorithm>
#include <cassert>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

/**
	Assumes the symbol is an ISIN.
*/	
inline std::size_t
convert(SecurityID_t const &i, pointer buff, std::size_t sz) {
	assert(i.max_size()<=sz);
	const std::size_t sz_to_copy=std::min(i.max_size(), sz);
	std::copy_n(i.begin(), sz_to_copy, buff);
	return sz_to_copy;
}

inline std::size_t
convert(std::int32_t a, pointer  buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(std::uint32_t a, pointer  buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(std::int64_t a, pointer buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(std::uint64_t a, pointer  buff, std::size_t sz) noexcept(true) {
	return libjmmcg::tostring(a, buff, sz);
}

inline std::size_t
convert(Price_t const &a, pointer  buff, std::size_t sz) {
	const double c=static_cast<double>(a)/implied_decimal_places;
	return libjmmcg::tostring(c, buff, sz);
}

template<class Ret> [[gnu::pure]] Ret
convert(FIX::common::field_str_const_range_t const &)=delete;

template<> inline std::int32_t
convert<std::int32_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return static_cast<std::int32_t>(libjmmcg::fromstring<long>(a.first, static_cast<std::size_t>(a.second-a.first)));
}

template<> inline std::uint32_t
convert<std::uint32_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return static_cast<std::uint32_t>(libjmmcg::fromstring<long>(a.first, static_cast<std::size_t>(a.second-a.first)));
}

template<> inline std::uint64_t
convert<std::uint64_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return static_cast<std::uint64_t>(libjmmcg::fromstring<long>(reinterpret_cast<char const *>(a.first), static_cast<std::size_t>(a.second-a.first)));
}

template<> inline std::string
convert<std::string>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return std::string(a.first, a.second);
}

template<> inline Quantity_t
convert<Quantity_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return libjmmcg::fromstring<Quantity_t>(reinterpret_cast<char const *>(a.first), static_cast<std::size_t>(a.second-a.first));
}

template<> inline SecurityID_t
convert<SecurityID_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	assert((a.second-a.first)<=static_cast<long>(sizeof(SecurityID_t)));
	SecurityID_t tmp{};
	std::memcpy(tmp.data(), a.first, static_cast<std::size_t>(a.second-a.first));
	return tmp;
}

} } } } }

#endif
