#ifndef ISIMUD_EXCHANGES_COMMON_FIX_PROCESSING_RULES_HPP
#define ISIMUD_EXCHANGES_COMMON_FIX_PROCESSING_RULES_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../common/processing_rules.hpp"

#include "core/msm.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

/// An FIX-protocol message handler.
template<class SrcMsgDetails, class DestMsgDetails, class SktT>
class client_to_exchange_transformations final : public exchanges::common::message_responses<SrcMsgDetails, DestMsgDetails, SktT> {
public:
	using base_t= exchanges::common::message_responses<SrcMsgDetails, DestMsgDetails, SktT>;
	using src_msg_details_t= typename base_t::src_msg_details_t;
	using dest_msg_details_t= typename base_t::dest_msg_details_t;
	using socket_t= typename base_t::socket_t;

	/**
		\return False to continue processing messages, true otherwise.
	*/
	REALLY_FORCE_INLINE [[gnu::flatten]] bool process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& dest_skt);

	std::string to_string() const noexcept(false);

private:
	struct state_machine_t;
	/**
	 * \todo Consider using msm here. c.f. FindPerfectHashMaskForMITClientToExchangeMSMs.
	 *
	 * \see FindPerfectHashMaskForMITClientToExchangeMSMs
	 */
	using machine= libjmmcg::msm::unroll::machine<state_machine_t>;

	static inline const machine msm{};
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false);

/// A FIX-protocol-to-client message handler.
/**
	The behaviour of this handler is derived from the specification in [1].
*/
template<class SrcMsgDetails, class DestMsgDetails, class SktT>
class exchange_to_client_transformations final : public exchanges::common::message_responses<SrcMsgDetails, DestMsgDetails, SktT> {
public:
	using base_t= exchanges::common::message_responses<SrcMsgDetails, DestMsgDetails, SktT>;
	using src_msg_details_t= typename base_t::src_msg_details_t;
	using dest_msg_details_t= typename base_t::dest_msg_details_t;
	using socket_t= typename base_t::socket_t;

	/**
	 * \param client_skt	This is a client-connection not a socket because a client may not be connected yet, but we must still respond to heartbeats from the server and other messages from the server. (Though one is unlikely to have a FIX-based exchange with ISIMUD as FIX would be too slow a protocol with which to implement an HFT exchange.)
	 * \return False to continue processing messages, true otherwise.
	 */
	template<class ClientCxn>
	REALLY_FORCE_INLINE [[gnu::flatten]] bool process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, ClientCxn& client_cxn);

	std::string to_string() const noexcept(false);

private:
	struct business_state_machine_t;
	struct admin_state_machine_t;
	/**
	 * \todo Consider using msm here. c.f. FindPerfectHashMaskForMITClientToExchangeMSMs.
	 *
	 * \see FindPerfectHashMaskForMITClientToExchangeMSMs
	 */
	using business_machine= libjmmcg::msm::unroll::machine<business_state_machine_t>;
	using admin_machine= libjmmcg::msm::unroll::machine<admin_state_machine_t>;

	static inline const business_machine business_msm{};
	static inline const admin_machine admin_msm{business_msm, business_msm, business_msm, business_msm};
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false);

/// A simple, FIX-protocol exchange simulator.
/**
	The behaviour of this simulator is a simplification derived from the specification in [1].
*/
template<class SrcMsgDetails, class SktT>
class simulator_responses final : public exchanges::common::simulator_responses<SrcMsgDetails, SktT> {
public:
	using base_t= exchanges::common::simulator_responses<SrcMsgDetails, SktT>;
	using msg_details_t= typename base_t::msg_details_t;
	using socket_t= typename base_t::socket_t;

	static inline constexpr const Reason_t logout_reason{'U', 's', 'e', 'r', ' ', 'l', 'o', 'g', 'o', 'u', 't', ' ', 'r', 'e', 'c', 'e', 'i', 'v', 'e', 'd'};
	/// The only valid instrument, all others will be rejected.
	static inline constexpr const typename msg_details_t::SecurityID_t instrumentID{'1'};
	/// An invalid instrument, that will be rejected.
	static inline constexpr const typename msg_details_t::SecurityID_t invalidInstrumentID{'2'};

	simulator_responses() noexcept(true)
		: base_t(), msm{std::ref(*this), std::ref(*this)} {
	}
	simulator_responses(simulator_responses const& sr) noexcept(true)
		: base_t(sr), msm{std::ref(*this), std::ref(*this)} {
	}

	/**
		\return False to continue processing messages, true otherwise.
	*/
	[[nodiscard]] bool process_msg(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& client_skt);

	[[nodiscard]] std::string to_string() const noexcept(false) override;

private:
	struct state_machine_t;
	using machine= libjmmcg::msm::unroll::machine<state_machine_t>;

	const machine msm;
};

template<class SrcMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, simulator_responses<SrcMsgDetails, SktT> const& ec) noexcept(false);

}}}}}

#include "processing_rules_impl.hpp"

#endif
