#ifndef ISIMUD_EXCHANGES_FIX_common_CLIENT_ORDER_ID_HPP
#define ISIMUD_EXCHANGES_FIX_common_CLIENT_ORDER_ID_HPP

/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "common/isimud_config.h"

#include "core/lfsr_counter.hpp"
#include "core/ttypes.hpp"

#include <fmt/format.h>

#include <algorithm>
#include <array>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

class client_order_id {
public:
	enum : std::size_t {
		prefix_size= 3,
		id_size= 13,
		size= prefix_size + id_size
	};
	struct element_type {
		using prefix_type= std::array<char, prefix_size>;
		using id_type= std::array<char, id_size>;
		using value_type= std::array<char, size>;

		union converter_type {
			struct [[gnu::packed]] order_type_internal {
				prefix_type prefix_;
				id_type id_;
			};
			order_type_internal order_id_;
			value_type element_;
		};
		converter_type converter_;
	};

	explicit client_order_id(element_type::prefix_type const& prefix) noexcept(true);

	[[nodiscard]] element_type::value_type const& get() const noexcept(true);

private:
	using lfsr_t= libjmmcg::lfsr::secure<46U>;

	static inline lfsr_t lfsr_{};

	element_type order_id_;
};

}}}}}

#include "client_order_id_impl.hpp"

#endif
