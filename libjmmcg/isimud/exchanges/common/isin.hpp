#ifndef ISIMUD_EXCHANGES_COMMON_ISIN_HPP
#define ISIMUD_EXCHANGES_COMMON_ISIN_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "iso_3166_country_codes.hpp"

#include "core/accumulate.hpp"
#include "core/int128_compatibility.hpp"
#include "core/memops.hpp"
#include "core/ttypes.hpp"

#include <boost/mpl/assert.hpp>

#include <iostream>
#include <string>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

/// The <a href="http://www.isin.org/isin/">ISIN code</a> for an instrument.
class ISIN_t {
public:
	using Country_Code_t=exchanges::common::ctry_codes::alpha_2::country_t;	///< The <a href="https://www.iso.org/obp/ui/#search">ISO 3166 Country-Code</a>, Alpha-2 format.
	BOOST_MPL_ASSERT_RELATION(sizeof(Country_Code_t), ==, 2);
	using NSIN_t=std::array<char, 9>;	///< Must be printable characters.
	using CUSIP_t=NSIN_t;	///< When appropriate this may be the CUSIP.
	BOOST_MPL_ASSERT_RELATION(sizeof(CUSIP_t), ==, 9);
	/// When appropriate the last 7 characters may be the SEDOL.
	union [[gnu::packed]] SEDOL_t {
		NSIN_t nsin;
		struct [[gnu::packed]] padded_SEDOL {
			using element_type=std::array<NSIN_t::value_type, 7>;

			const NSIN_t::value_type prefix[sizeof(NSIN_t)-sizeof(element_type)]={'0', '0'};
			element_type value;
		} padded_sedol;
	};
	BOOST_MPL_ASSERT_RELATION(sizeof(SEDOL_t), ==, 9);

public:
	/// Get at the internal components.
	struct [[gnu::packed]] details_t {
		Country_Code_t country;
		union [[gnu::packed]] {
			NSIN_t nsin;
			CUSIP_t cusip;
			SEDOL_t sedol;
		};
		char check_digit;	///< The check digit.
	};
	BOOST_MPL_ASSERT_RELATION(sizeof(details_t), ==, 12);

	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written from a socket must be able to use this, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using block_t=std::array<std::byte, sizeof(details_t)>;
	BOOST_MPL_ASSERT_RELATION(sizeof(block_t), ==, 12);

	constexpr ISIN_t() noexcept(true);
	explicit constexpr ISIN_t(block_t const &b) noexcept(true);
	template<std::size_t Sz>
	explicit constexpr ISIN_t(char const (&b)[Sz]) noexcept(true);
	constexpr ISIN_t(ISIN_t const &i) noexcept(true);
	constexpr ISIN_t &operator=(ISIN_t const &i) noexcept(true);

	bool operator==(ISIN_t const &i) const noexcept(true);
	bool operator<(ISIN_t const &i) const noexcept(true);

	constexpr block_t const &data() const noexcept(true) {
		return value.block;
	}

	constexpr details_t const &details() const noexcept(true) {
		return value.components;
	}

	std::string to_string() const noexcept(false);

	std::size_t hash() const noexcept(true);

	friend std::ostream &operator<<(std::ostream &os, ISIN_t const &i) noexcept(false);
	friend std::istream &operator>>(std::istream &is, ISIN_t &i) noexcept(false);

private:
	union converter_t {
		block_t block{};	///< Deal with the ISIN as a flat data-block.
		details_t components;
		std::array<char, sizeof(details_t)> as_chars;
	} value{};
	BOOST_MPL_ASSERT_RELATION(sizeof(converter_t), ==, 12);
};

BOOST_MPL_ASSERT_RELATION(sizeof(ISIN_t), ==, 12);

} } } }

#include "isin_impl.hpp"

#endif
