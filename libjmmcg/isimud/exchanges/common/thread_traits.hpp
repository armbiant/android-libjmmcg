#ifndef ISIMUD_EXCHANGES_COMMON_THREAD_TRAITS_HPP
#	define ISIMUD_EXCHANGES_COMMON_THREAD_TRAITS_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/numa_traits.hpp"

#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

/// From the detected NUMA layout, assign cores to the logical threads in the FIX-to-exchange translator.
struct thread_traits {
	using api_threading_traits=libjmmcg::ppd::api_threading_traits<libjmmcg::ppd::platform_api, libjmmcg::ppd::heavyweight_threading>;
	struct thread_info {
		const libjmmcg::ppd::numa_cpu_traits::element_type::value_type::value_type core{};
		const api_threading_traits::api_params_type::priority_type priority{};

		friend std::ostream &operator<<(std::ostream &os, thread_info const &ti) noexcept(false);
	};

	/// The numa-node upon which the code is compiled to run optimally.
	/**
		This value should be the same as that used by numactl in the start-script (e.g. "start_all_links.sh"), otherwise the code will run on the wrong processor. (One can determine which node the code has been compiled for by running the executable with the "--help" option.)

		\see NUMA_INDEX
	*/
	static inline constexpr auto numa_index=(libjmmcg::ppd::numa_cpu_traits::is_numa ? libjmmcg::ppd::numa_cpu_traits::node_mapping.max_size()-1 : 0);
	static inline constexpr auto max_threads_on_node=libjmmcg::ppd::numa_cpu_traits::node_mapping[numa_index].max_size();

	/**
		Put this well out of the way of anything important.
	*/
	static inline constexpr auto main_thread=thread_info{
		libjmmcg::ppd::numa_cpu_traits::node_mapping[0][0%max_threads_on_node],
		api_threading_traits::api_params_type::priority_type::idle
	};
	static inline constexpr auto heartbeat_thread=thread_info{
		libjmmcg::ppd::numa_cpu_traits::node_mapping[numa_index][0%max_threads_on_node],
		api_threading_traits::api_params_type::priority_type::idle
	};
	static inline constexpr auto exchange_to_client_thread=thread_info{
		libjmmcg::ppd::numa_cpu_traits::node_mapping[numa_index][1%max_threads_on_node],
		api_threading_traits::api_params_type::priority_type::normal
	};
	static inline constexpr auto client_to_exchange_thread=thread_info{
		libjmmcg::ppd::numa_cpu_traits::node_mapping[numa_index][2%max_threads_on_node],
		api_threading_traits::api_params_type::priority_type::time_critical
	};
	static inline constexpr auto exchange_simulator_thread=thread_info{
		libjmmcg::ppd::numa_cpu_traits::node_mapping[numa_index][3%max_threads_on_node],
		api_threading_traits::api_params_type::priority_type::idle
	};
};

inline std::ostream &
operator<<(std::ostream &os, thread_traits::thread_info const &ti) noexcept(false) {
	os<<"core="<<ti.core<<", priority="<<ti.priority;
	return os;
}

} } } }

#endif
