#ifndef ISIMUD_EXCHANGES_COMMON_EXCHANGE_SOCKET_TYPE_HPP
#	define ISIMUD_EXCHANGES_COMMON_EXCHANGE_SOCKET_TYPE_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/socket_client_manager.hpp"
#include "core/socket_server_manager.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

/// Allow one to easily choose the socket implementation used by the FIX-to-exchange translators.
namespace socket_type {
	/// The socket write() methods, under heavy load, may not make a complete write of the buffer, and need to be repeatedly called. This means they may not be atomic, so this is a lock to make it atomic under such heavy loads.
// TODO	using lock_type=libjmmcg::ppd::api_lock_traits<libjmmcg::ppd::platform_api, libjmmcg::ppd::heavyweight_threading>::critical_section_type::lock_type;
	using lock_type=libjmmcg::ppd::api_lock_traits<libjmmcg::ppd::platform_api, libjmmcg::ppd::heavyweight_threading>::anon_spin_event_type::lock_type;
// TODO	using lock_type=libjmmcg::ppd::api_lock_traits<libjmmcg::ppd::generic_traits::api_type::no_api, libjmmcg::ppd::sequential_mode>::critical_section_type::lock_type;

	using skt_mgr_t=libjmmcg::socket::asio::client_manager<lock_type>;
	// TODO	using skt_mgr_t=libjmmcg::socket::glibc::client_manager<lock_type>;

	using socket_t=skt_mgr_t::socket_t;
	using client_cxn_ptr_t=libjmmcg::socket::server_manager::manager<socket_t>::session::ptr_type;
	
}

} } } }

#endif
