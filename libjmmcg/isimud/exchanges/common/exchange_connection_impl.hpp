/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace common {

template<template<class> class ClientCxn, class... ExchgCxns>
template<class... ExchCxnCtorArgs, typename std::enable_if<sizeof...(ExchgCxns)==sizeof...(ExchCxnCtorArgs), bool>::type>
inline
exchange_connection<ClientCxn, ExchgCxns...>::exchange_connection(typename client_link_t::ctor_args const &client_cxn_details, typename exchg_links_t::report_error_fn_t &exchg_report_error, typename client_link_t::report_error_fn_t &client_report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf &timestamps, thread_traits::api_threading_traits::thread_name_t const &svr_name, ExchCxnCtorArgs &&...exchange_cxn_args)
:	exchg_links_(exit_requested_, exchg_report_error, cpu_core, cpu_priority, timestamps, std::forward<ExchCxnCtorArgs>(exchange_cxn_args)...),
	client_link(
		client_cxn_details,
		exchg_links_,
		client_report_error,
		timestamps,
		svr_name,
		[this](typename client_link_t::svr_mgr_t::session::ptr_type client_connection) {
			exchg_links_.client_connected(client_connection);
		}
	) {
}

template<template<class> class ClientCxn, class... ExchgCxns>
template<class... ExchCxnCtorArgs, typename std::enable_if<sizeof...(ExchgCxns)==sizeof...(ExchCxnCtorArgs), bool>::type>
inline
exchange_connection<ClientCxn, ExchgCxns...>::exchange_connection(typename client_link_t::ctor_args const &client_cxn_details, typename exchg_links_t::report_error_fn_t &exchg_report_error, typename client_link_t::report_error_fn_t &client_report_error, std::size_t cpu_core, thread_traits::api_threading_traits::api_params_type::priority_type cpu_priority, libjmmcg::latency_timestamps_itf &timestamps, thread_traits::api_threading_traits::thread_name_t const &svr_name, std::tuple<ExchCxnCtorArgs...> exchange_cxn_args)
:	exchg_links_(exit_requested_, exchg_report_error, cpu_core, cpu_priority, timestamps, std::move(exchange_cxn_args), std::index_sequence_for<ExchCxnCtorArgs...>{}),
	client_link(
		client_cxn_details,
		exchg_links_,
		client_report_error,
		timestamps,
		svr_name,
		[this](typename client_link_t::svr_mgr_t::session::ptr_type client_connection) {
			exchg_links_.client_connected(client_connection);
		}
	) {
}

template<template<class> class ClientCxn, class... ExchgCxns> inline
exchange_connection<ClientCxn, ExchgCxns...>::~exchange_connection() noexcept(false) {
	exit_requested_.test_and_set(std::memory_order_seq_cst);
	client_link.stop();
}

template<template<class> class ClientCxn, class... ExchgCxns> inline bool
exchange_connection<ClientCxn, ExchgCxns...>::are_all_logged_on() const noexcept(true) {
	return exchg_links_.are_all_logged_on();
}

template<template<class> class ClientCxn, class... ExchgCxns> inline std::atomic_flag &
exchange_connection<ClientCxn, ExchgCxns...>::exit_requested() noexcept(true) {
	return client_link.exit_requested();
}

template<template<class> class ClientCxn, class... ExchgCxns> inline std::string
exchange_connection<ClientCxn, ExchgCxns...>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<<boost::core::demangle(typeid(*this).name())
		<<",\nexit="<<exit_requested_.test(std::memory_order_relaxed)
		<<". "<<mic_codes::to_string()
		<<" "<<ccy_codes::to_string()
		<<" "<<ctry_codes::to_string()
		<<" Client link: "<<client_link
		<<", client-to-exchange core: "<<common::thread_traits::client_to_exchange_thread
		<<", exchange links: "<<exchg_links_;
	return ss.str();
}

template<template<class> class ClientCxn, class... ExchgCxns> inline std::ostream &
operator<<(std::ostream &os, exchange_connection<ClientCxn, ExchgCxns...> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

} } } }
