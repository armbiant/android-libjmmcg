#ifndef ISIMUD_EXCHANGES_OUCH_NYSE_MESSAGES_HPP
#define ISIMUD_EXCHANGES_OUCH_NYSE_MESSAGES_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core/max_min.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/max_element.hpp>
#include <boost/mpl/min_element.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/variant/variant.hpp>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace OUCH {

/**
	From <a href="https://nasdaqtrader.com/content/technicalsupport/specifications/TradingProducts/OUCH4.2.pdf">O*U*C*H, Version 4.2, February 29, 2016</a>.
*/
namespace NYSE {

/// \todo

/**
	\test OUCH NYSE size tests.
*/
namespace tests {

/// \todo

}

} } } } }

#endif
