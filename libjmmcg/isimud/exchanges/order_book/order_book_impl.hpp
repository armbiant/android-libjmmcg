/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE {

template<class MsgType>
inline std::ostream&
to_stream(std::ostream& os, typename isimud::ISIMUD_VER_NAMESPACE::order_book<MsgType>::orders_by_time const& o) {
	os << o.front().limitPrice() << ": ";
	std::transform(
		o.begin(),
		o.end(),
		std::experimental::make_ostream_joiner(os, " "),
		[](auto const& v) {
			return v.orderQty();
		});
	os << '\n';
	return os;
}

template<class MsgType>
inline order_book<MsgType>::internal_order::internal_order(sequence_number_t seq, element_type const& o) noexcept(true)
	: order_t(o), sequence_number_(seq) {
}

template<class MsgType>
inline typename order_book<MsgType>::internal_order&
order_book<MsgType>::internal_order::operator=(internal_order const& o) noexcept(true) {
	this->~internal_order();
	new(this) internal_order(o);
	return *this;
}

template<class MsgType>
inline bool
order_book<MsgType>::cheapest_t::operator()(orders_by_time const& l, orders_by_time const& r) const noexcept(true) {
	return l.front().limitPrice() < r.front().limitPrice();
}

template<class MsgType>
inline bool
order_book<MsgType>::most_expensive_t::operator()(orders_by_time const& l, orders_by_time const& r) const noexcept(true) {
	return r.front().limitPrice() < l.front().limitPrice();
}

template<class MsgType>
inline bool
order_book<MsgType>::cheapest_orders_by_time_t::empty() const noexcept(true) {
	return std::find_if_not(
				 this->c.begin(),
				 this->c.end(),
				 [](auto const& v) {
					 return v.empty();
				 })
			 == this->c.end();
}

template<class MsgType>
inline typename order_book<MsgType>::cheapest_orders_by_time_t::reference
order_book<MsgType>::cheapest_orders_by_time_t::top() noexcept(true) {
	return this->c.front();
}

template<class MsgType>
inline void
order_book<MsgType>::cheapest_orders_by_time_t::insert(typename value_type::value_type::element_type const& o, sequence_number_t& sequence_number) {
	assert(sequence_number < std::numeric_limits<sequence_number_t>::max() - 1);
	if(this->c.empty()) {
		this->emplace(1, typename value_type::value_type{sequence_number++, o});
	} else {
		auto iter= std::lower_bound(
			this->c.begin(),
			this->c.end(),
			o,
			[](auto const& l, auto const& r) {
				return l.front().limitPrice() > r.limitPrice();
			});
		if(iter != this->c.end()) {
			assert(iter->front().limitPrice() <= o.limitPrice());
			if(iter->front().limitPrice() == o.limitPrice()) {
				iter->emplace_back(sequence_number++, o);
			} else {
				this->emplace(1, typename value_type::value_type{sequence_number++, o});
			}
		} else {
			this->emplace(1, typename value_type::value_type{sequence_number++, o});
		}
	}
}

template<class MsgType>
inline void
order_book<MsgType>::cheapest_orders_by_time_t::erase(typename value_type::value_type::element_type const& o) {
	auto iter= std::remove_if(
		this->c.begin(),
		this->c.end(),
		[&o](auto& v) {
			auto iter= std::remove_if(
				v.begin(),
				v.end(),
				[&o](auto const& v) {
					return o.clientOrderID() == v.clientOrderID();
				});
			typename std::remove_reference<decltype(v)>::type tmp(v.begin(), iter);
			tmp.swap(v);
			return v.empty();
		});
	typename std::remove_reference<decltype(this->c)>::type tmp(this->c.begin(), iter);
	tmp.swap(this->c);
	std::sort(this->c.rbegin(), this->c.rend(), this->comp);
}

template<class MsgType>
inline std::ostream&
order_book<MsgType>::cheapest_orders_by_time_t::to_stream(std::ostream& os) const {
	std::sort(const_cast<cheapest_orders_by_time_t*>(this)->c.rbegin(), const_cast<cheapest_orders_by_time_t*>(this)->c.rend(), this->comp);
	std::for_each(
		this->c.begin(),
		this->c.end(),
		[&os](auto const& v) {
			libisimud::to_stream<MsgType>(os, v);
		});
	return os;
}

template<class MsgType>
inline bool
order_book<MsgType>::most_expensive_by_time_t::empty() const noexcept(true) {
	return std::find_if_not(
				 this->c.begin(),
				 this->c.end(),
				 [](auto const& v) {
					 return v.empty();
				 })
			 == this->c.end();
}

template<class MsgType>
inline typename order_book<MsgType>::most_expensive_by_time_t::reference
order_book<MsgType>::most_expensive_by_time_t::top() noexcept(true) {
	return this->c.front();
}

template<class MsgType>
inline void
order_book<MsgType>::most_expensive_by_time_t::insert(typename value_type::value_type::element_type const& o, sequence_number_t& sequence_number) {
	assert(sequence_number < std::numeric_limits<sequence_number_t>::max() - 1);
	if(this->c.empty()) {
		this->emplace(1, typename value_type::value_type{sequence_number++, o});
	} else {
		auto iter= std::lower_bound(
			this->c.begin(),
			this->c.end(),
			o,
			[](auto const& l, auto const& r) {
				const auto lp= l.front().limitPrice();
				return lp < r.limitPrice();
			});
		if(iter != this->c.end()) {
			assert(iter->front().limitPrice() >= o.limitPrice());
			if(iter->front().limitPrice() == o.limitPrice()) {
				iter->emplace_back(sequence_number++, o);
			} else {
				this->emplace(1, typename value_type::value_type{sequence_number++, o});
			}
		} else {
			this->emplace(1, typename value_type::value_type{sequence_number++, o});
		}
	}
}

template<class MsgType>
inline void
order_book<MsgType>::most_expensive_by_time_t::erase(typename value_type::value_type::element_type const& o) {
	auto iter= std::remove_if(
		this->c.begin(),
		this->c.end(),
		[&o](auto& v) {
			auto iter= std::remove_if(
				v.begin(),
				v.end(),
				[&o](auto const& v) {
					return o.clientOrderID() == v.clientOrderID();
				});
			typename std::remove_reference<decltype(v)>::type tmp(v.begin(), iter);
			tmp.swap(v);
			return v.empty();
		});
	typename std::remove_reference<decltype(this->c)>::type tmp(this->c.begin(), iter);
	tmp.swap(this->c);
	std::sort(this->c.begin(), this->c.end(), this->comp);
}

template<class MsgType>
inline std::ostream&
order_book<MsgType>::most_expensive_by_time_t::to_stream(std::ostream& os) const {
	std::for_each(
		this->c.rbegin(),
		this->c.rend(),
		[&os](auto const& v) {
			libisimud::to_stream<MsgType>(os, v);
		});
	return os;
}

template<class MsgType>
inline bool
order_book<MsgType>::order_id_t::operator==(order_id_t const& o) noexcept(true) {
	return order_id_ == o.order_id_;
}

template<class MsgType>
inline typename order_book<MsgType>::order_id_t&
order_book<MsgType>::order_id_t::operator++() noexcept(true) {
	auto tmp= libjmmcg::fromstring<std::uint64_t>(order_id_.data(), order_id_.size());
	++tmp;
	[[maybe_unused]] auto const quash_warning= libjmmcg::tostring(tmp, order_id_.data(), order_id_.size());
	return *this;
}

template<class MsgType>
inline typename order_book<MsgType>::order_id_t
order_book<MsgType>::order_id_t::operator++(int) noexcept(true) {
	const auto tmp= *this;
	++*this;
	return tmp;
}

template<class MsgType>
inline order_book<MsgType>::order_book() {
}

template<class MsgType>
inline order_book<MsgType>::~order_book() {
}

template<class MsgType>
inline bool
order_book<MsgType>::empty() const noexcept(true) {
	return all_open_orders_.empty();
}

template<class MsgType>
inline bool
order_book<MsgType>::is_crossed() const noexcept(true) {
	for(auto const& open_orders: all_open_orders_) {
		auto const ask_empty= open_orders.second.open_ask_orders.empty();
		auto const bid_empty= open_orders.second.open_bid_orders.empty();
		if((!ask_empty && !bid_empty) && (open_orders.second.best_bid().limitPrice() >= open_orders.second.best_ask().limitPrice())) {
			return true;
		}
	}
	return false;
}

template<class MsgType>
inline typename order_book<MsgType>::price_t
order_book<MsgType>::agreed_spread(symbol_t const& symbol) const noexcept(true) {
	auto iter= all_open_orders_.find(symbol);
	if(iter != all_open_orders_.end()) {
		auto const ask_empty= iter->second.open_ask_orders.empty();
		auto const bid_empty= iter->second.open_bid_orders.empty();
		return (ask_empty || bid_empty) ? false : iter->second.best_ask().limitPrice() - iter->second.best_bid().limitPrice();
	} else {
		return price_t{};
	}
}

template<class MsgType>
inline void
order_book<MsgType>::place(order_t const& order) noexcept(false) {
	using namespace libjmmcg;

	if(sequence_number_ >= std::numeric_limits<sequence_number_t>::max() - 1) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Sequence number wrapped. Code must restart. order={}, sequence number={}", tostring(order), sequence_number_), &order_book<MsgType>::place));
	}
	switch(order.side()) {
	case side_t::Buy:
		all_open_orders_[order.instrumentID()].open_bid_orders.insert(order, sequence_number_);
		assert(!all_open_orders_[order.instrumentID()].open_bid_orders.empty());
		break;
	case side_t::Sell:
		all_open_orders_[order.instrumentID()].open_ask_orders.insert(order, sequence_number_);
		assert(!all_open_orders_[order.instrumentID()].open_ask_orders.empty());
		break;
	default:
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Unhandled side to the order. order={}", tostring(order)), &order_book<MsgType>::place));
	};
	assert(!empty());
}

template<class MsgType>
inline void
order_book<MsgType>::cancel(order_t const& order) noexcept(false) {
	using namespace libjmmcg;

	switch(order.side()) {
	case side_t::Buy:
		all_open_orders_[order.instrumentID()].open_bid_orders.erase(order);
		break;
	case side_t::Sell:
		all_open_orders_[order.instrumentID()].open_ask_orders.erase(order);
		break;
	default:
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Unhandled side to the order. order={}", tostring(order)), &order_book<MsgType>::cancel));
	};
	if(empty()) {
		sequence_number_= sequence_number_t{};
	}
}

template<class MsgType>
inline typename order_book<MsgType>::trades_t
order_book<MsgType>::match() {
	trades_t trades;
	while(is_crossed()) {
		for(auto& open_orders: all_open_orders_) {
			auto& best_bid_orders= open_orders.second.open_bid_orders.top();
			auto& best_ask_orders= open_orders.second.open_ask_orders.top();
			while(!best_ask_orders.empty() && !best_bid_orders.empty()) {
				auto& best_ask_order= best_ask_orders.front();
				auto& best_bid_order= best_bid_orders.front();
				assert(best_ask_order.instrumentID() == best_bid_order.instrumentID());
				assert(is_crossed());
				// We'd better be aggressive, as we must still be crossed....
				assert(best_bid_order.limitPrice() >= best_ask_order.limitPrice());
				auto const traded_quantity= std::min(best_ask_order.orderQty(), best_bid_order.orderQty());
				// Must have something to trade as crossed & aggressive.
				assert(traded_quantity > quantity_t{});
				// Sequence numbers must be unique.
				assert(best_ask_order.sequence_number_ != best_bid_order.sequence_number_);
				auto const& order_to_trade_against= best_ask_order.sequence_number_ < best_bid_order.sequence_number_ ? best_ask_order : best_bid_order;
				trades.emplace_back(
					sequence_number_++,
					order_to_trade_against.clientOrderID(),
					msg_details_t::ExecType::Trade,
					order_to_trade_against.limitPrice(),
					best_ask_order.instrumentID(),
					best_ask_order.side(),
					traded_quantity,
					order_to_trade_against.orderQty() - traded_quantity,
					order_to_trade_against.find_MIC());
				assert(best_bid_order.orderQty() >= traded_quantity);
				best_bid_order.orderQty(best_bid_order.orderQty() - traded_quantity);
				if(best_bid_order.orderQty() <= quantity_t{}) {
					// Order has been traded, so delete it.
					best_bid_orders.pop_front();
				}
				best_ask_order.orderQty(best_ask_order.orderQty() - traded_quantity);
				if(best_ask_order.orderQty() <= quantity_t{}) {
					// Order has been traded, so delete it.
					best_ask_orders.pop_front();
				}
			}
			if(best_ask_orders.empty()) {
				open_orders.second.open_ask_orders.pop();
			}
			if(best_bid_orders.empty()) {
				open_orders.second.open_bid_orders.pop();
			}
		}
	}
	if(empty()) {
		sequence_number_= sequence_number_t{};
	}
	return trades;
}

template<class MsgType>
inline typename order_book<MsgType>::cheapest_orders_by_time_t::value_type::value_type const&
order_book<MsgType>::open_orders_t::best_bid() const noexcept(true) {
	assert(!open_bid_orders.empty());
	return open_bid_orders.top().front();
}

template<class MsgType>
inline typename order_book<MsgType>::most_expensive_by_time_t::value_type::value_type const&
order_book<MsgType>::open_orders_t::best_ask() const noexcept(true) {
	assert(!open_ask_orders.empty());
	return open_ask_orders.top().front();
}

template<class MsgType>
inline std::ostream&
operator<<(std::ostream& os, order_book<MsgType> const& ob) {
	//	order_book<MsgType>::msg_details_t::to_stream(os);
	//	os<<"\nSequence number="<<ob.sequence_number_
	//		<<", order ID: '"<<ob.order_id_.order_id_<<"'"
	//		"\n=================\n"
	os << "=================\n"
			"ASK\n";
	if(ob.all_open_orders_.empty()) {
		os << "------------\n";
	} else {
		for(auto const& open_orders: ob.all_open_orders_) {
			open_orders.second.open_ask_orders.to_stream(os);
			os << "------------\n";
			open_orders.second.open_bid_orders.to_stream(os);
		}
	}
	os << "BID\n"
			"=================\n";
	return os;
}

}}
