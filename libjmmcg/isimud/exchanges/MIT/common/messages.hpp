#ifndef ISIMUD_EXCHANGES_MIT_common_MESSAGES_HPP
#define ISIMUD_EXCHANGES_MIT_common_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "ref_data.hpp"
#include "types.hpp"

#include "../../common/iso_10383_mic_codes.hpp"

#include "core/blatant_old_msvc_compiler_hacks.hpp"
#include "core/memops.hpp"

#include <boost/asio/ip/address.hpp>
#include <boost/program_options.hpp>

#include <fstream>
#include <string>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

/**
	From <a href="http://www.londonstockexchange.com/products-and-services/technical-library/millennium-exchange-technical-specifications/mit203160615.pdf">"MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway", Issue 11.6,
17 August 2015</a>
*/
namespace common {

struct logon_args_t {
	using UserName_t=common::UserName_t;
	using Password_t=common::Password_t;

	const UserName_t username{{}};
	const Password_t password{{}};
	const Password_t new_password{{}};
};
struct logoff_args_t {
	const Reason_t reason{{}};
};

template<class ExchgMsgDetailsT, class ConnPolT, class SktT, class ThrdT> inline auto
make_ctor_args(boost::program_options::variables_map const &vm) noexcept(false);

/**
	Section: "8.2 Message header"
*/
class [[gnu::packed]] Header {
public:
	enum : bool {
		has_static_size=true	///< The message is statically-sized, not dynamically, so sizeof(the derived message-type) is the amount to copy, i.e. length() returns sizeof(the derived message-type).
	};
	using ClientOrderID_t=common::ClientOrderID_t;
	using OrderID_t=common::OrderID_t;
	using MsgTypes_t=common::MsgType;
	using SeqNum_t=common::SeqNum_t;

	const std::int8_t start_of_message=2;
	const std::int16_t length_;
	const MsgType_t type_;

	constexpr MsgTypes_t type() const noexcept(true) {
		return static_cast<MsgTypes_t>(type_);
	}
	constexpr std::uint16_t length() const noexcept(true) {
		assert(length_>=0);
		BOOST_MPL_ASSERT_RELATION(sizeof(Header), <=, std::numeric_limits<std::uint16_t>::max());
		return static_cast<std::uint16_t>(length_+static_cast<std::uint16_t>(sizeof(Header)));
	}

	constexpr bool is_valid() const noexcept(true) {
		return start_of_message==2 && length()>=sizeof(Header);
	}

protected:
	template<class MsgT>
	explicit constexpr Header(MsgT const *) noexcept(true);
};

/**
	Section: "8.3.1 Logon"
*/
struct [[gnu::packed]] LogonRequest : public Header {
	using Header_t=Header;
	using logon_args_t=common::logon_args_t;
	static inline constexpr const MsgType static_type=MsgType::LogonRequest;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const logon_args_t::UserName_t userName;
	const logon_args_t::Password_t password;
	const logon_args_t::Password_t newPassword;
	const std::uint8_t messageVersion=1;

	constexpr LogonRequest(const logon_args_t::UserName_t &UN, const logon_args_t::Password_t &P, const logon_args_t::Password_t &NP) noexcept(true);
	explicit constexpr LogonRequest(logon_args_t const &a) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit __stdcall
	LogonRequest(SrcMsg const &msg) noexcept(true)=delete;
};

/**
	Section: "8.3.2 Logon Reply"
*/
template<class RejectCode>
struct [[gnu::packed]] LogonReply : public Header {
	using Header_t=Header;
	using RejectCode_t=RejectCode;
	static inline constexpr const MsgType static_type=MsgType::LogonReply;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	/// Allow a client connected to the exchange to process the LogonReply message.
	struct respond;

	RejectCode_t rejectCode_;
	PasswordExpiryDayCount_t passwordExpiryDayCount;

	LogonReply() noexcept(true);

	RejectCode_t rejectCode() const noexcept(true) {
		return rejectCode_;
	}
	void rejectCode(RejectCode_t const &rc) noexcept(true) {
		rejectCode_=rc;
	}
};

/**
	Section: "8.3.3 Logout"
*/
struct [[gnu::packed]] LogoutRequest : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::LogoutRequest;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const Reason_t reason_;

	constexpr explicit LogoutRequest() noexcept(true);
	constexpr explicit LogoutRequest(Reason_t r) noexcept(true);
	constexpr explicit LogoutRequest(logoff_args_t const &a) noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit constexpr __stdcall
	LogoutRequest(SrcMsg const &msg, Reason_t r) noexcept(true)=delete;
	
	const Reason_t &reason() const noexcept(true) {
		return reason_;
	}
};

/**
	Section: "8.3.4 Heartbeat"
*/
struct [[gnu::packed]] Heartbeat : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::Heartbeat;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	constexpr Heartbeat() noexcept(true);

	/// Create a message from the source message.
	/**
		If an error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg> explicit constexpr __stdcall
	Heartbeat(SrcMsg const &msg) noexcept(true)=delete;
};

/**
	Section: "8.3.5 Missed Message Request"
*/
struct [[gnu::packed]] MissedMessageRequest : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::MissedMessageRequest;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	const AppID appID;
	const SeqNum_t lastMsgSeqNum;

	constexpr MissedMessageRequest(AppID a, SeqNum_t l) noexcept(true);
};

/**
	Section: "8.3.6 Missed Message Request Ack"
*/
struct [[gnu::packed]] MissedMessageRequestAck : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::MissedMessageRequestAck;
	enum class ResponseType : uint8_t {
		Successful=0,
		RecoveryRequestlimitReached=1,
		InvalidAppID=2,
		ServiceUnavailable=3
	};
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	ResponseType responseType;

	MissedMessageRequestAck() noexcept(true);
};

/**
	Section: "8.3.7 Missed Message Report"
*/
struct [[gnu::packed]] MissedMessageReport : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::MissedMessageReport;
	enum class ResponseType : int8_t {
		DownloadComplete=0,
		MessageLimitReached=1,
		ServiceUnavailable=3
	};
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	ResponseType responseType;

	MissedMessageReport() noexcept(true);
};

/**
	Section: "8.3.8 Reject"
*/
template<class RejectCode, RejectCode RejectCodeNum>
struct [[gnu::packed]] Reject : public Header {
	using Header_t=Header;
	using RejectCode_t=RejectCode;
	static inline constexpr const MsgType static_type=MsgType::Reject;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	static inline constexpr const RejectCode_t unknown_msg=RejectCodeNum;

	RejectCode_t rejectCode_;
	RejectReason_t rejectReason;
	MsgType_t rejectedMessageType;
	ClientOrderID_t clientOrderID_;

	Reject() noexcept(true);
	template<std::size_t N>
	Reject(SeqNum_t, RejectCode_t rc, char const (&)[N]) noexcept(true);
	Reject(ClientOrderID_t const &clID, RejectCode_t const rc) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	RejectCode_t rejectCode() const noexcept(true) {
		return rejectCode_;
	}
	void rejectCode(RejectCode_t const &rc) noexcept(true) {
		rejectCode_=rc;
	}
};

/**
	Section: "8.3.9 System Status"
*/
struct [[gnu::packed]] SystemStatus : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::SystemStatus;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};

	AppID appID;
	AppStatus appStatus;

	SystemStatus() noexcept(true);
};

template<class Specific1, class Specific2, class Specific3>
struct [[gnu::packed]] NewOrder : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::NewOrder;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific1_t=Specific1;
	using specific2_t=Specific2;
	using specific3_t=Specific3;

	ClientOrderID_t clientOrderID_;
	TraderID_t traderID{"\0\0\0\0\0\0\0\0\0\0"};
	Account_t account{"\0\0\0\0\0\0\0\0\0"};
	ClearingAccount clearingAccount=ClearingAccount::Client;
	specific1_t specific1;
	OrderType orderType_;
	TIF tif_;
	ExpireDateTime_t expireDateTime=0;
	Side side_;
	specific2_t specific2;
	Price_t limitPrice_;
	Capacity capacity=Capacity::Principal;
	const AutoCancel autoCancel=AutoCancel::Cancel;
	OrderSubType orderSubType=OrderSubType::Order;
	specific3_t specific3;

	constexpr __stdcall NewOrder(SeqNum_t seqNum, ClientOrderID_t const &clID, OrderType const oT, TIF const t, Side const s, SecurityID_t instID, typename specific2_t::order_qty_t ordQty, Price_t p) noexcept(true);

	SecurityID_t instrumentID() const noexcept(true) {
		return specific1.instrumentID();
	}
	void instrumentID(SecurityID_t i) noexcept(true) {
		specific1.instrumentID(i);
	}

	typename specific2_t::order_qty_t orderQty() const noexcept(true) {
		return specific2.orderQty();
	}
	void orderQty(typename specific2_t::order_qty_t i) noexcept(true) {
		specific2.orderQty(i);
	}

	OrderType orderType() const noexcept(true) {
		return orderType_;
	}
	void orderType(OrderType i) noexcept(true) {
		orderType_=i;
	}

	Side side() const noexcept(true) {
		return side_;
	}
	void side(Side i) noexcept(true) {
		side_=i;
	}

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	Price_t limitPrice() const noexcept(true) {
		return limitPrice_;
	}
	void limitPrice(Price_t p) noexcept(true) {
		limitPrice_=p;
	}

	TIF tif() const noexcept(true) {
		return tif_;
	}
	void tif(TIF t) noexcept(true) {
		tif_=t;
	}
};

template<class Specific1, class Specific2>
struct [[gnu::packed]] NewQuote : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::NewQuote;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific1_t=Specific1;
	using specific2_t=Specific2;

	ClientOrderID_t clientOrderID_;
	TraderID_t traderID;
	ClearingAccount clearingAccount;
	SecurityID_t instrumentID_;
	specific1_t specific1;
	Capacity capacity;
	AutoCancel autoCancel;
	specific2_t specific2;

	constexpr NewQuote() noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(SecurityID_t i) noexcept(true) {
		instrumentID_=i;
	}
};

template<class Specific1, class Specific2, class Specific3>
struct [[gnu::packed]] OrderCancelReplaceRequest : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::OrderCancelReplaceRequest;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific1_t=Specific1;
	using specific2_t=Specific2;
	using specific3_t=Specific3;

	ClientOrderID_t clientOrderID_;
	ClientOrderID_t originalClientOrderID_;
	OrderID_t orderID;
	specific1_t specific1;
	ExpireDateTime_t expireDateTime;
	specific2_t specific2;
	Price_t limitPrice_;
	Account_t account;
	TIF tif_;
	Side side_;
	Price_t stoppedPrice;
	specific3_t specific3;

	constexpr OrderCancelReplaceRequest(common::ClientOrderID_t const &clID, ClientOrderID_t const &origclID, SecurityID_t instID, typename specific2_t::order_qty_t  ordQty, Price_t const p, TIF t, Side s) noexcept(true);

	SecurityID_t instrumentID() const noexcept(true) {
		return specific1.instrumentID();
	}
	void instrumentID(SecurityID_t i) noexcept(true) {
		specific1.instrumentID(i);
	}

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	ClientOrderID_t const &originalClientOrderID() const noexcept(true) {
		return originalClientOrderID_;
	}
	void originalClientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, originalClientOrderID_);
	}

	Price_t limitPrice() const noexcept(true) {
		return limitPrice_;
	}
	void limitPrice(Price_t p) noexcept(true) {
		limitPrice_=p;
	}

	typename specific2_t::order_qty_t orderQty() const noexcept(true) {
		return specific2.orderQty();
	}
	void orderQty(typename specific2_t::order_qty_t i) noexcept(true) {
		specific2.orderQty(i);
	}

	Side side() const noexcept(true) {
		return side_;
	}
	void side(Side i) noexcept(true) {
		side_=i;
	}

	TIF tif() const noexcept(true) {
		return tif_;
	}
	void tif(TIF t) noexcept(true) {
		tif_=t;
	}
};

struct [[gnu::packed]] OrderCancelRequestSpecific2 {
	TargetBook targetBook=TargetBook::DarkMidpointOrderBook;
	ReservedField9_t reservedField;
};

/**
	Section: "8.4.4 Order Cancel Request"
*/
template<class Specific1, class Specific2=OrderCancelRequestSpecific2>
struct [[gnu::packed]] OrderCancelRequest : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::OrderCancelRequest;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific1_t=Specific1;
	using specific2_t=Specific2;

	ClientOrderID_t clientOrderID_;
	ClientOrderID_t originalClientOrderID_;
	OrderID_t orderID;
	specific1_t specific1;
	Side side_;
	specific2_t specific2;

	constexpr OrderCancelRequest(ClientOrderID_t const &clID, ClientOrderID_t const &origclID, SecurityID_t instID, Side side) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	ClientOrderID_t const &originalClientOrderID() const noexcept(true) {
		return originalClientOrderID_;
	}
	void originalClientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, originalClientOrderID_);
	}

	SecurityID_t instrumentID() const noexcept(true) {
		return specific1.instrumentID();
	}
	void instrumentID(SecurityID_t i) noexcept(true) {
		specific1.instrumentID(i);
	}

	Side side() const noexcept(true) {
		return side_;
	}
	void side(Side s) noexcept(true) {
		side_=s;
	}
};

/**
	Section: "8.4.5 Order Mass Cancel Request"
*/
template<class Specific1, class Specific2=OrderCancelRequestSpecific2>
struct [[gnu::packed]] OrderMassCancelRequest : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::OrderMassCancelRequest;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific1_t=Specific1;
	using specific2_t=Specific2;

	ClientOrderID_t clientOrderID_;
	MassCancelRequestType massCancelRequestType;
	specific1_t specific1;
	OrderSubType orderSubType=OrderSubType::Order;
	specific2_t specific2;

	explicit constexpr OrderMassCancelRequest(SecurityID_t instID) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}
};

template<class Specific1, class Specific2>
struct [[gnu::packed]] ExecutionReport : public Header {
	using Header_t=Header;
	using isin_mapping_data_const_reference=common::isin_mapping_data_const_reference;
	static inline constexpr const MsgType static_type=MsgType::ExecutionReport;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific1_t=Specific1;
	using specific2_t=Specific2;
	using RejectCode_t=typename specific1_t::RejectCode_t;

	AppID appID;
	SeqNum_t sequenceNumber;
	ExecutionID_t executionID;
	ClientOrderID_t clientOrderID_;
	OrderID_t orderID;
	ExecType execType_;
	ExecutionReportRefID_t executionReportRefID;
	OrderStatus orderStatus_;
	RejectCode_t orderRejectCode_;
	Price_t executedPrice_;
	specific1_t specific1;
	SecurityID_t instrumentID_;
	RestatementReason restatementReason;
	const std::int8_t reservedField2=0;
	Side side_;
	uint64_t secondaryOrderID=0;
	Counterparty_t counterparty;
	TradeLiquidityIndicator tradeLiquidityIndicator;
	uint64_t tradeMatchID;
	TransactTime_t transactTime;
	specific2_t specific2;

	constexpr ExecutionReport() noexcept(true);
	constexpr ExecutionReport(SeqNum_t seqNum, ClientOrderID_t const &clID, AppID aID, ExecType eT, Price_t const price, SecurityID_t instID, Side s) noexcept(true);
	constexpr ExecutionReport(SeqNum_t seqNum, ClientOrderID_t const &clID, ExecType eT, Price_t const price, SecurityID_t instID, Side s, typename specific1_t::order_qty_t execdQty, typename specific1_t::order_qty_t leavesQty) noexcept(true);

	ExecType execType() const noexcept(true) {
		return execType_;
	}
	void execType(ExecType e) noexcept(true) {
		execType_=e;
	}

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(SecurityID_t i) noexcept(true) {
		instrumentID_=i;
	}
	
	Price_t executedPrice() const noexcept(true) {
		return executedPrice_;
	}
	void executedPrice(Price_t p) noexcept(true) {
		executedPrice_=p;
	}

	OrderStatus orderStatus() const noexcept(true) {
		return orderStatus_;
	}
	void orderStatus(OrderStatus os) noexcept(true) {
		orderStatus_=os;
	}

	typename specific1_t::order_qty_t executedQty() const noexcept(true) {
		return specific1.executedQty;
	}
	void executedQty(typename specific1_t::order_qty_t eq) noexcept(true) {
		specific1.executedQty=eq;
	}

	typename specific1_t::order_qty_t leavesQty() const noexcept(true) {
		return specific1.leavesQty;
	}
	void leavesQty(typename specific1_t::order_qty_t eq) noexcept(true) {
		specific1.leavesQty=eq;
	}

	Side side() const noexcept(true) {
		return side_;
	}
	void side(Side s) noexcept(true) {
		side_=s;
	}

	RejectCode_t orderRejectCode() const noexcept(true) {
		return orderRejectCode_;
	}
	void orderRejectCode(RejectCode_t r) noexcept(true) {
		orderRejectCode_=r;
	}
};

template<class RejectCode>
struct [[gnu::packed]] OrderCancelRejectSpecific {
	using RejectCode_t=RejectCode;

	RejectCode_t cancelRejectReason_;
	TransactTime_t transactTime;
	ReservedField10_t reservedField;
};

/**
	Section: "8.4.7 Order Cancel Reject"
*/
template<class Specific>
struct [[gnu::packed]] OrderCancelReject : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::OrderCancelReject;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific_t=Specific;
	using RejectCode_t=typename specific_t::RejectCode_t;

	AppID appID;
	SeqNum_t sequenceNumber;
	ClientOrderID_t clientOrderID_;
	OrderID_t orderID;
	specific_t specific;

	OrderCancelReject() noexcept(true);
	OrderCancelReject(SeqNum_t seqNum, ClientOrderID_t const &clID) noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}

	RejectCode_t cancelRejectReason() const noexcept(true) {
		return specific.cancelRejectReason_;
	}
	void cancelRejectReason(RejectCode_t r) noexcept(true) {
		specific.cancelRejectReason_=r;
	}
};

template<class RejectCode>
struct [[gnu::packed]] OrderMassCancelReportSpecific {
	using RejectCode_t=RejectCode;

	std::int32_t totalAffectedOrders;
	TransactTime_t transactTime;
	ReservedField10_t reservedField;
};

/**
	Section: "8.4.8 Order Mass Cancel Report"
*/
template<class Specific>
struct [[gnu::packed]] OrderMassCancelReport : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::OrderMassCancelReport;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific_t=Specific;
	using RejectCode_t=typename specific_t::RejectCode_t;

	AppID appID;
	SeqNum_t sequenceNumber;
	ClientOrderID_t clientOrderID_;
	MassCancelResponse massCancelResponse;
	RejectCode_t massCancelRejectReason;
	specific_t specific;

	OrderMassCancelReport() noexcept(true);

	ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}
};

template<class RejectCode, RejectCode UnknownInstrument>
struct [[gnu::packed]] BusinessRejectSpecific {
	using RejectCode_t=RejectCode;
	static inline constexpr const RejectCode_t unknown_instrument=UnknownInstrument;

	ReservedField10_t reservedField;
};

/**
	Section: "8.5.1 Business Reject"
*/
template<class Specific>
struct [[gnu::packed]] BusinessReject : public Header {
	using Header_t=Header;
	static inline constexpr const MsgType static_type=MsgType::BusinessMessageReject;
	enum : std::size_t {
		header_t_size=sizeof(Header_t)
	};
	using specific_t=Specific;
	using RejectCode_t=typename specific_t::RejectCode_t;
	static inline constexpr const RejectCode_t unknown_instrument=specific_t::unknown_instrument;

	AppID appID;
	SeqNum_t sequenceNumber;
	RejectCode_t rejectCode_;
	ClientOrderID_t clientOrderID_;
	OrderID_t orderID;
	TransactTime_t transactTime;
	specific_t specific;

	BusinessReject() noexcept(true);
	BusinessReject(SeqNum_t seqNum, ClientOrderID_t const &clID) noexcept(true);

	RejectCode_t rejectCode() const noexcept(true) {
		return rejectCode_;
	}
	void rejectCode(RejectCode_t const &rc) noexcept(true) {
		rejectCode_=rc;
	}

		ClientOrderID_t const &clientOrderID() const noexcept(true) {
		return clientOrderID_;
	}
	void clientOrderID(ClientOrderID_t const &clID) noexcept(true) {
		libjmmcg::memcpy_opt(clID, clientOrderID_);
	}
};

} } } } }

#include "messages_impl.hpp"

#endif
