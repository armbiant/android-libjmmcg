#ifndef ISIMUD_EXCHANGES_MIT_COMMON_REF_DATA_HPP
#define ISIMUD_EXCHANGES_MIT_COMMON_REF_DATA_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "types.hpp"

#include "../../common/isin.hpp"

#include "core/csv_iterator.hpp"
#include "core/flat_bijective_map.hpp"
#include "core/line_iterator.hpp"
#include "core/ttypes.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/mpl/assert.hpp>

#include <functional>
#include <sstream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace common {

/// Structures for encapsulating the MIT reference-data details.
/**
	From Section 2.3 "Reference Data" of <a href="https://www.londonstockexchange.com/products-and-services/millennium-exchange/millennium-exchange-migration/mit301issue10.pdf">"London Stock Exchange - MIT301 - Guide to Market Data Services - Issue 10.14 February 2015"</a>.
*/	
class ref_data {
private:
	struct MF_Record;

public:
	/**
		From Section 2.2 "Instrument Identification"
	*/
	struct security_id_key {
		using element_type=exchanges::common::ISIN_t;

		element_type isin_;

		[[nodiscard]] explicit constexpr security_id_key(element_type const &isin) noexcept(true);
		[[nodiscard]] explicit security_id_key(MF_Record const &) noexcept(true);

		[[nodiscard]] bool operator==(security_id_key const &sik) const noexcept(true);
		[[nodiscard]] bool operator<(security_id_key const &sik) const noexcept(true);

		[[nodiscard]] std::string to_string() const noexcept(false);
	};

	/// Convert from a security_id_key to a common::SecurityID_t.
	using container_type=libjmmcg::flat_bijective_map<security_id_key, exchanges::MIT::common::SecurityID_t>;
	using size_type=typename container_type::size_type;
	using key_type=typename container_type::value_type::first_type;
	using mapped_type=typename container_type::value_type::second_type;
	enum : std::size_t {
		instrument_id_field=0,
		isin_field=5
	};
	BOOST_MPL_ASSERT_RELATION(instrument_id_field, <, isin_field);

	/// Read the reference data from the input stream.
	/**
		The reference data should be in the format specified in <a href="http://www.londonstockexchange.com/products-and-services/millennium-exchange/millennium-exchange-migration/mit401.pdf">MIT401 - MILLENNIUM EXCHANGE - Guide to Reference Data Services - Issue 11.3 - 24 September 2013</a>.

		\param	is	The input std::istream that contains the reference data that should be in a restricted CSV format, with no comments, no internal white space, not escaping nor escaped commas.
	*/
	[[nodiscard]] explicit ref_data(std::istream &is) noexcept(false);
	[[nodiscard]] ref_data(ref_data const &)=default;

	[[nodiscard]] bool empty() const noexcept(true);
	[[nodiscard]] size_type size() const noexcept(true);

	auto const &at(auto const &k) const noexcept(false);

	[[nodiscard]] std::string to_string() const noexcept(false);

private:
	/**
		From section 2.3 "Reference Data".
	*/
	struct MF_Record {
		common::SecurityID_t instrument;
		exchanges::common::ISIN_t isin;
	};

	const container_type cont_;
};

using isin_mapping_data_const_reference=ref_data const &;

std::ostream &
operator<<(std::ostream &is, ref_data::security_id_key const &sik) noexcept(false);
std::ostream &
operator<<(std::ostream &is, isin_mapping_data_const_reference rd) noexcept(false);

} } } } }

#include "ref_data_impl.hpp"

#endif
