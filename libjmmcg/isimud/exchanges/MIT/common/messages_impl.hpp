/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace common {

template<class ExchgMsgDetailsT, class ConnPolT, class SktT, class ThrdT> inline auto
make_ctor_args(boost::program_options::variables_map const &vm) noexcept(false) {
	using conn_pol_t=ConnPolT;
	using exchg_msg_details_t=ExchgMsgDetailsT;
	using ref_data_t=typename exchg_msg_details_t::ref_data;
	
	std::string const MIC_code=libjmmcg::enum_tags::mpl::to_array<exchanges::common::mic_codes::ISO_10383_MIC_Codes, exchg_msg_details_t::MIC_code>::value;
	std::ifstream ref_data_src(vm[(MIC_code+"_ref_data").c_str()].as<std::string>().c_str());
	auto ref_data(std::make_shared<ref_data_t>(ref_data_src));
	
	const std::string user(vm[(MIC_code+"_username").c_str()].as<std::string>());
	typename exchg_msg_details_t::UserName_t user_conv{};
	std::memcpy(user_conv.data(), user.data(), std::min(user_conv.size(), user.size()));
	const std::string pass(vm[(MIC_code+"_password").c_str()].as<std::string>());
	typename exchg_msg_details_t::Password_t pass_conv{};
	std::memcpy(pass_conv.data(), pass.data(), std::min(pass_conv.size(), pass.size()));
	const std::string new_pass(vm[(MIC_code+"_new_password").c_str()].as<std::string>());
	typename exchg_msg_details_t::Password_t new_pass_conv{};
	std::memcpy(new_pass_conv.data(), new_pass.data(), std::min(new_pass_conv.size(), new_pass.size()));
	const std::string reason(vm[(MIC_code+"_logout_reason").c_str()].as<std::string>());
	common::Reason_t reason_conv{};
	std::memcpy(reason_conv.data(), reason.data(), std::min(reason_conv.size(), reason.size()));
	return std::make_tuple(
		conn_pol_t(
			typename conn_pol_t::gateways_t(
				std::make_pair(boost::asio::ip::address_v4::from_string(vm[(MIC_code+"_primary_gateway_address").c_str()].as<std::string>()), vm[(MIC_code+"_primary_gateway_port").c_str()].as<unsigned short>()),
				std::make_pair(boost::asio::ip::address_v4::from_string(vm[(MIC_code+"_secondary_gateway_address").c_str()].as<std::string>()), vm[(MIC_code+"_secondary_gateway_port").c_str()].as<unsigned short>())
			),
			typename conn_pol_t::logon_args_t{
				user_conv,
				pass_conv,
				new_pass_conv
			},
			common::logoff_args_t{
				reason_conv
			}
		),
		SktT::socket_priority::high,
		ThrdT::exchange_to_client_thread.core,
		ref_data
	);
}

template<class MsgT>
inline constexpr
Header::Header(MsgT const *) noexcept(true)
: length_(static_cast<std::int16_t>(sizeof(MsgT)-sizeof(Header))), type_(static_cast<MsgType_t>(MsgT::static_type)) {
}

inline constexpr
LogonRequest::LogonRequest(const logon_args_t::UserName_t &UN, const logon_args_t::Password_t &P, const logon_args_t::Password_t &NP) noexcept(true)
: Header_t(static_cast<LogonRequest const *>(nullptr)), userName(UN), password(P), newPassword(NP) {
	assert(length_>0);
}

inline constexpr
LogonRequest::LogonRequest(logon_args_t const &a) noexcept(true)
: LogonRequest(a.username, a.password, a.new_password) {
}

template<class RejectCode>
inline
LogonReply<RejectCode>::LogonReply() noexcept(true)
: Header_t(static_cast<LogonReply const *>(nullptr)) {
}

template<class RejectCode>
struct LogonReply<RejectCode>::respond {
	template<class ReplyMsg, class Op> void
	operator()(ReplyMsg const &msg, Op const &o) const noexcept(false) {
		switch (msg.rejectCode()) {
			case ReplyMsg::logon_success:
				o.operator()();
				break;
			default: {
				std::ostringstream os;
				os<<"Failed to logon. Reject code='"<<msg.rejectCode()<<"'";
				BOOST_THROW_EXCEPTION(std::runtime_error(os.str()));
			}
		}
	}
};

inline constexpr
LogoutRequest::LogoutRequest() noexcept(true)
: Header_t(static_cast<LogoutRequest const *>(nullptr)),
	reason_{"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"} {
}

inline constexpr
LogoutRequest::LogoutRequest(Reason_t r) noexcept(true)
: Header_t(static_cast<LogoutRequest const *>(nullptr)), reason_(r) {
}

inline constexpr
LogoutRequest::LogoutRequest(logoff_args_t const &a) noexcept(true)
: LogoutRequest(a.reason) {
}

inline constexpr
Heartbeat::Heartbeat() noexcept(true)
: Header_t(static_cast<Heartbeat const *>(nullptr)) {
}

inline constexpr
MissedMessageRequest::MissedMessageRequest(AppID a, SeqNum_t l) noexcept(true)
: Header_t(static_cast<MissedMessageRequest const *>(nullptr)), appID(a), lastMsgSeqNum(l) {
}

inline
MissedMessageRequestAck::MissedMessageRequestAck() noexcept(true)
: Header_t(static_cast<MissedMessageRequestAck const *>(nullptr)) {
}

inline
MissedMessageReport::MissedMessageReport() noexcept(true)
: Header_t(static_cast<MissedMessageReport const *>(nullptr)) {
}

template<class RejectCode, RejectCode RejectCodeNum>
inline
Reject<RejectCode, RejectCodeNum>::Reject() noexcept(true)
: Header_t(static_cast<Reject const *>(nullptr)) {
}

template<class RejectCode, RejectCode RejectCodeNum>
template<std::size_t N>
inline
Reject<RejectCode, RejectCodeNum>::Reject(SeqNum_t, RejectCode_t rc, char const (&)[N]) noexcept(true)
: Header_t(static_cast<Reject const *>(nullptr)),
	rejectCode_(rc) {
}

template<class RejectCode, RejectCode RejectCodeNum>
inline
Reject<RejectCode, RejectCodeNum>::Reject(ClientOrderID_t const &clID, RejectCode_t const rc) noexcept(true)
: Reject(SeqNum_t{}, rc, "") {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
}

inline
SystemStatus::SystemStatus() noexcept(true)
: Header_t(static_cast<SystemStatus const *>(nullptr)) {
}

template<class Specific1, class Specific2, class Specific3>
inline constexpr
NewOrder<Specific1, Specific2, Specific3>::NewOrder(SeqNum_t, ClientOrderID_t const &clID, OrderType const oT, TIF const t, Side const s, SecurityID_t instID, typename specific2_t::order_qty_t ordQty, Price_t p) noexcept(true)
: Header_t(static_cast<NewOrder const *>(nullptr)),
	specific1(instID),
	orderType_(oT),
	tif_(t),
	side_(s),
	specific2(ordQty),
	limitPrice_(p)
{
	libjmmcg::memcpy_opt(clID, clientOrderID_);
}

template<class Specific1, class Specific2>
inline constexpr
NewQuote<Specific1, Specific2>::NewQuote() noexcept(true)
: Header_t(static_cast<NewQuote const *>(nullptr)) {
}

template<class Specific1, class Specific2, class Specific3>
inline constexpr
OrderCancelReplaceRequest<Specific1, Specific2, Specific3>::OrderCancelReplaceRequest(common::ClientOrderID_t const &clID, ClientOrderID_t const &origclID, SecurityID_t instID, typename specific2_t::order_qty_t ordQty, Price_t const p, TIF t, Side s) noexcept(true)
: Header_t(static_cast<OrderCancelReplaceRequest const *>(nullptr)),
	specific1(instID),
	specific2(ordQty),
	limitPrice_(p),
	tif_(t),
	side_(s) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	libjmmcg::memcpy_opt(origclID, originalClientOrderID_);
}

template<class Specific1, class Specific2>
inline constexpr
OrderCancelRequest<Specific1, Specific2>::OrderCancelRequest(ClientOrderID_t const &clID, ClientOrderID_t const &origclID, SecurityID_t instID, Side side) noexcept(true)
: Header_t(static_cast<OrderCancelRequest const *>(nullptr)),
	specific1(instID), side_(side) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
	libjmmcg::memcpy_opt(origclID, originalClientOrderID_);
}

template<class Specific1, class Specific2>
inline constexpr
OrderMassCancelRequest<Specific1, Specific2>::OrderMassCancelRequest(SecurityID_t instID) noexcept(true)
: Header_t(static_cast<OrderMassCancelRequest const *>(nullptr)), specific1(instID) {
}

template<class Specific1, class Specific2>
inline constexpr
ExecutionReport<Specific1, Specific2>::ExecutionReport() noexcept(true)
: Header_t(static_cast<ExecutionReport const *>(nullptr)) {
}

template<class Specific1, class Specific2>
inline constexpr
ExecutionReport<Specific1, Specific2>::ExecutionReport(SeqNum_t seqNum, ClientOrderID_t const &clID, AppID aID, ExecType eT, Price_t const price, SecurityID_t instID, Side s) noexcept(true)
: Header_t(static_cast<ExecutionReport const *>(nullptr)),
	appID(aID),
	sequenceNumber(seqNum),
	execType_(eT),
	executedPrice_(price),
	instrumentID_(instID),
	side_(s) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
}

template<class Specific1, class Specific2>
inline constexpr
ExecutionReport<Specific1, Specific2>::ExecutionReport(SeqNum_t seqNum, ClientOrderID_t const &clID, ExecType eT, Price_t const price, SecurityID_t instID, Side s, typename specific1_t::order_qty_t execdQty, typename specific1_t::order_qty_t leavesQty) noexcept(true)
: ExecutionReport(seqNum, clID, default_appID, eT, price, instID, s) {
	specific1.executedQty=execdQty;
	specific1.leavesQty=leavesQty;
}

template<class Specific>
inline
OrderCancelReject<Specific>::OrderCancelReject() noexcept(true)
: Header_t(static_cast<OrderCancelReject const *>(nullptr)) {
}

template<class Specific>
inline
OrderCancelReject<Specific>::OrderCancelReject(SeqNum_t seqNum, ClientOrderID_t const &clID) noexcept(true)
: Header_t(static_cast<OrderCancelReject const *>(nullptr)),
	sequenceNumber(seqNum) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
}

template<class Specific>
inline
OrderMassCancelReport<Specific>::OrderMassCancelReport() noexcept(true)
: Header_t(static_cast<OrderMassCancelReport const *>(nullptr)) {
}

template<class Specific>
inline
BusinessReject<Specific>::BusinessReject() noexcept(true)
: Header_t(static_cast<BusinessReject const *>(nullptr)) {
}

template<class Specific>
inline
BusinessReject<Specific>::BusinessReject(SeqNum_t seqNum, ClientOrderID_t const &clID) noexcept(true)
: Header_t(static_cast<BusinessReject const *>(nullptr)),
	sequenceNumber(seqNum) {
	libjmmcg::memcpy_opt(clID, clientOrderID_);
}

} } } } }
