#ifndef ISIMUD_EXCHANGES_COMMON_MIT_PROCESSING_RULES_HPP
#define ISIMUD_EXCHANGES_COMMON_MIT_PROCESSING_RULES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../common/processing_rules.hpp"

#include LIBJMMCG_PERFECT_HASH_MASK_FOR_FIX_HDR
#include LIBJMMCG_PERFECT_HASH_MASK_FOR_MIT_EXCHG_MSGS_HDR
#include LIBJMMCG_PERFECT_HASH_MASK_FOR_MIT_SIM_HDR

#include "core/exception.hpp"
#include "core/msm.hpp"

#include <chrono>
#include <functional>
#include <thread>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace common {

/// A client-to-MIT-protocol message handler.
/**
	The behaviour of this handler is derived from the specification in [1].
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
template<class SrcMsgDetails, class DestMsgDetails, class SktT>
class client_to_exchange_transformations final
	: public exchanges::common::message_responses<
		  SrcMsgDetails,
		  DestMsgDetails,
		  SktT> {
public:
	using base_t= exchanges::common::message_responses<
		SrcMsgDetails,
		DestMsgDetails,
		SktT>;
	using src_msg_details_t= typename base_t::src_msg_details_t;
	using dest_msg_details_t= typename base_t::dest_msg_details_t;
	using isin_mapping_data_const_reference= typename dest_msg_details_t::isin_mapping_data_const_reference;	  ///< The object containing the reference data that is used to convert the input symbol information to exchange-specific instrument identifiers.
	using ref_data= typename dest_msg_details_t::ref_data;
	using socket_t= typename base_t::socket_t;

	explicit constexpr client_to_exchange_transformations(isin_mapping_data_const_reference rd) noexcept(true)
		: ref_data_(rd), msm(ref_data_, ref_data_, ref_data_, ref_data_) {
		assert(!ref_data_.empty());
	}
	constexpr client_to_exchange_transformations(client_to_exchange_transformations const& v) noexcept(true)
		: base_t(v), ref_data_(v.ref_data_), msm(v.msm) {
		assert(!ref_data_.empty());
	}

	/**
		\return False to continue processing messages, true otherwise.
	*/
	REALLY_FORCE_INLINE [[gnu::flatten]] bool process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& dest_skt);

	[[nodiscard]] std::string to_string() const noexcept(false);

private:
	struct state_machine_t;
	using machine= libjmmcg::msm::hash::machine<state_machine_t>;

	isin_mapping_data_const_reference ref_data_;
	const machine msm;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, client_to_exchange_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false);

/// A MIT-protocol-to-client message handler.
/**
	The behaviour of this handler is derived from the specification in [1].
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
template<class SrcMsgDetails, class DestMsgDetails, class SktT>
class exchange_to_client_transformations final
	: public exchanges::common::message_responses<
		  SrcMsgDetails,
		  DestMsgDetails,
		  SktT> {
public:
	using base_t= exchanges::common::message_responses<
		SrcMsgDetails,
		DestMsgDetails,
		SktT>;
	using src_msg_details_t= typename base_t::src_msg_details_t;
	using dest_msg_details_t= typename base_t::dest_msg_details_t;
	using isin_mapping_data_const_reference= typename src_msg_details_t::isin_mapping_data_const_reference;	 ///< The object containing the reference data that is used to convert the input symbol information to exchange-specific instrument identifiers.
	using ref_data= typename src_msg_details_t::ref_data;
	using socket_t= typename base_t::socket_t;

	explicit constexpr exchange_to_client_transformations(isin_mapping_data_const_reference rd) noexcept(true)
		: ref_data_(rd),
		  msm(ref_data_, ref_data_, ref_data_, ref_data_, ref_data_, ref_data_, ref_data_, ref_data_) {
		assert(!ref_data_.empty());
	}
	constexpr exchange_to_client_transformations(exchange_to_client_transformations const& v) noexcept(true)
		: base_t(v), ref_data_(v.ref_data_), msm(v.msm) {
	}

	/**
	 * \param client_cxn	This is a client-connection not a socket because a client may not be connected yet, but we must still respond to heartbeats from the server and other messages from the server.
	 * \return False to continue processing messages, true otherwise.
	 */
	REALLY_FORCE_INLINE [[gnu::flatten]] bool process_msg(typename src_msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, exchanges::common::socket_type::client_cxn_ptr_t& client_cxn);

	[[nodiscard]] std::string to_string() const noexcept(false);

private:
	struct state_machine_t;
	using machine= libjmmcg::msm::hash::machine<state_machine_t>;

	isin_mapping_data_const_reference ref_data_;
	const machine msm;
};

template<class SrcMsgDetails, class DestMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, exchange_to_client_transformations<SrcMsgDetails, DestMsgDetails, SktT> const& ec) noexcept(false);

/// A simple, MIT-protocol exchange simulator.
/**
	The behaviour of this simulator is a simplification derived from the specification in [1].
	[1] "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway"
*/
template<class SrcMsgDetails, class SktT>
class simulator_responses final : public exchanges::common::simulator_responses<SrcMsgDetails, SktT> {
public:
	using base_t= exchanges::common::simulator_responses<SrcMsgDetails, SktT>;
	using msg_details_t= typename base_t::msg_details_t;
	using exchange_msg_details_t= msg_details_t;
	using socket_t= typename base_t::socket_t;

	static inline constexpr const Reason_t logout_reason{"snafu\0"};
	/// The only valid instrument, all others will be rejected.
	static inline constexpr const typename msg_details_t::SecurityID_t instrumentID= 133215;	 ///< VODAFONE.
	/// An invalid instrument, that will be rejected.
	static inline constexpr const typename msg_details_t::SecurityID_t invalidInstrumentID= 1;
	static inline constexpr const std::chrono::seconds logout_timeout{3};

	simulator_responses() noexcept(true)
		: base_t(), msm{std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this)} {
	}
	constexpr simulator_responses(simulator_responses const& sr) noexcept(true)
		: base_t(sr), msm{std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this), std::ref(*this)} {
	}

	/**
		\return False to continue processing messages, true otherwise.
	*/
	bool process_msg(typename msg_details_t::msg_buffer_t const& buff, socket_t& exchg_skt, socket_t& client_skt);

	[[nodiscard]] std::string to_string() const noexcept(false) override;

private:
	struct state_machine_t;
	using machine= libjmmcg::msm::hash::machine<state_machine_t>;

	const machine msm;
};

template<class SrcMsgDetails, class SktT>
inline std::ostream&
operator<<(std::ostream& os, simulator_responses<SrcMsgDetails, SktT> const& ec) noexcept(false);

}}}}}

#include "processing_rules_impl.hpp"

#endif
