#ifndef ISIMUD_EXCHANGES_MIT_COMMON_TYPES_HPP
#define ISIMUD_EXCHANGES_MIT_COMMON_TYPES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "common/isimud_config.h"

#include <array>
#include <iostream>
#include <limits>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT { namespace common {

using CrossType_t=std::uint8_t;
using MsgType_t=char;
using SeqNum_t=std::int32_t;
using Price_t=std::int64_t;
using SecurityID_t=std::int32_t;
using TransactTime_t=std::uint64_t;

using Account_t=std::array<char, 10>;
using ClientOrderID_t=std::array<char, 20>;
using CommonSymbol_t=std::array<char, 8>;
using Counterparty_t=std::array<char, 11>;
using CrossID_t=std::array<char, 20>;
using ExecutionID_t=std::array<char, 12>;
using ExecutionReportRefID_t=std::array<char, 12>;
using ExpireDateTime_t=std::uint32_t;
using OrderID_t=std::array<char, 12>;
using Password_t=std::array<char, 25>;
using PasswordExpiryDayCount_t=std::array<char, 30>;
using PublicOrderID_t=std::array<char, 12>;
using Reason_t=std::array<char, 20>;
using RejectReason_t=std::array<char, 30>;
using Segment_t=std::array<char, 4>;
using TraderID_t=std::array<char, 11>;
using UserName_t=std::array<char, 25>;

using ReservedField2_t=std::array<char, 2>;
using ReservedField3_t=std::array<char, 3>;
using ReservedField4_t=std::array<char, 4>;
using ReservedField6_t=std::array<char, 6>;
using ReservedField9_t=std::array<char, 9>;
using ReservedField10_t=std::array<char, 10>;
using ReservedField12_t=std::array<char, 12>;
using ReservedField20_t=std::array<char, 20>;

/// The number of implied decimal-places for MIT use.
/**
	From Section 7 "Data Types".
*/
inline constexpr const Price_t implied_decimal_places=100000000;

enum class MsgType : MsgType_t {
	/**
		From "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway", Issue 11.6,
	17 August 2015
		Section: "8.1.1 Administrative messages"
	*/
	LogonRequest='A',
	LogonReply='B',
	LogoutRequest='5',
	Heartbeat='0',
	MissedMessageRequest='M',
	MissedMessageRequestAck='N',
	MissedMessageReport='P',	///< Or TransmissionComplete for JSE.
	Reject='3',
	SystemStatus='n',
	/**
		From "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway", Issue 11.6,
	17 August 2015
		Section: "8.1.2.1 Client-initiated"
	*/
	NewOrder='D',
	OrderCancelRequest='F',
	OrderMassCancelRequest='q',
	OrderCancelReplaceRequest='G',
	NewQuote='S',
	NewOrderCrossMessage='C',
	CrossOrderCancelRequest='H',
	/**
		From "MIT203 - MILLENNIUM EXCHANGE Native Trading Gateway", Issue 11.6,
	17 August 2015
		Section: "8.1.2.2 Server-initiated"
	*/
	ExecutionReport='8',
	OrderCancelReject='9',
	OrderMassCancelReport='r',
	BusinessMessageReject='j',
	MatchAll=std::numeric_limits<MsgType_t>::max()-1,	///< For the meta-state machine to allow a catch-all rule to reject anything unhandled.
	Exit=std::numeric_limits<MsgType_t>::max()	///< For the meta-state machine: the exit state to exit the msm.
};

inline std::ostream &
operator<<(std::ostream &os, MsgType v) noexcept(false) {
	os<<static_cast<std::underlying_type<MsgType>::type>(v);
	return os;
}

enum class ClearingAccount : std::uint8_t {
	Client=1u,
	House=3u
};

inline std::ostream &
operator<<(std::ostream &os, ClearingAccount v) {
	os<<static_cast<std::underlying_type<ClearingAccount>::type>(v);
	return os;
}

enum class Capacity : std::uint8_t {
	RisklessPrincipal=1u,
	Principal=2u,
	Agency=3u,
	CFDGiveUp=4u
};

inline std::ostream &
operator<<(std::ostream &os, Capacity v) {
	os<<static_cast<std::underlying_type<Capacity>::type>(v);
	return os;
}

enum class AutoCancel : std::uint8_t {
	DoNotCancel=0u,
	Cancel=1u
};

inline std::ostream &
operator<<(std::ostream &os, AutoCancel v) {
	os<<static_cast<std::underlying_type<AutoCancel>::type>(v);
	return os;
}

enum class OrderType : std::uint8_t {
	Market=1u,
	Limit=2u,
	Stop=3u,
	StopLimit=4u,
	Pegged=50u,
	PeggedLimitOrder=51u
};

inline std::ostream &
operator<<(std::ostream &os, OrderType v) {
	os<<static_cast<std::underlying_type<OrderType>::type>(v);
	return os;
}

inline std::istream &
operator>>(std::istream &os, OrderType &v) {
	std::underlying_type<OrderType>::type t;
	os>>t;
	v=static_cast<OrderType>(t);
	return os;
}

enum class Anonymity : std::uint8_t {
	Anonymous=0u,
	Named=1u
};

inline std::ostream &
operator<<(std::ostream &os, Anonymity v) {
	os<<static_cast<std::underlying_type<Anonymity>::type>(v);
	return os;
}

enum class TIF : std::uint8_t {
	Day=0u,
	GTC=1u,	///< Good Till Cancel (GTC); JSE.
	IOC=3u,	///< Immediate or Cancel (IOC).
	FOK=4u,	///< Fill or Kill (FOK).
	OPG=5u,	///< At the Opening (OPG).
	GTD=6u,	///< Good Till Date (GTD).
	GTT=8u,	///< Good Till Time (GTT).
	GFA1=9u,	///< Good For Auction (GFA); BIT & JSE.
	ATC=10u,	///< At the Close (ATC).
	CPX=12u,	///< Close Price Cross; JSE.
	GFA2=50u,	///< Good for Auction (GFA); LSE or Good for EOD Volume Auction Uncross (GDX) JSE.
	GFX=51u,	///< Good for Intraday Auction (GFX).
	GFS=52u	///< Good for Scheduled Auction (GFS). 
};

inline std::ostream &
operator<<(std::ostream &os, TIF v) {
	os<<static_cast<std::underlying_type<TIF>::type>(v);
	return os;
}

inline std::istream &
operator>>(std::istream &os, TIF &v) {
	std::underlying_type<TIF>::type t;
	os>>t;
	v=static_cast<TIF>(t);
	return os;
}

enum class Side : std::uint8_t {
	Buy=1u,
	Sell=2u
};

inline std::ostream &
operator<<(std::ostream &os, Side v) {
	os<<static_cast<std::underlying_type<Side>::type>(v);
	return os;
}

inline std::istream &
operator>>(std::istream &os, Side &v) {
	std::underlying_type<Side>::type t;
	os>>t;
	v=static_cast<Side>(t);
	return os;
}

enum class PassiveOnlyOrder : std::uint8_t {
	NoConstraint=0u,
	AcceptOrderIfSettingNewBBOJoiningExistingBBO=1u,
	AcceptOrderIfAtBBOInOneVisiblePricePoint=2u,
	AcceptOrderIfAtBBOInTwoVisiblePricePoints=3u,
	NotMatchWithVisibleContraOrder=99u,
	AcceptOrderIfSettingNewVisibleBBO=100u
};

inline std::ostream &
operator<<(std::ostream &os, PassiveOnlyOrder v) {
	os<<static_cast<std::underlying_type<PassiveOnlyOrder>::type>(v);
	return os;
}

enum class OrderSubType : std::uint8_t {
	Order=0u,
	Quote=3u,
	PeggedOrder=5u,
	PeggedMid=50u,
	Random=51u,	///< Or Pegged To Bid for JSE.
	PeggedOffer=52u
};

inline std::ostream &
operator<<(std::ostream &os, OrderSubType v) {
	os<<static_cast<std::underlying_type<OrderSubType>::type>(v);
	return os;
}

enum class AppID : std::uint8_t {
	Partition1=1u,
	Partition2=2u,
	Partition3=3u
};

inline std::ostream &
operator<<(std::ostream &os, AppID v) {
	os<<static_cast<std::underlying_type<AppID>::type>(v);
	return os;
}

inline constexpr const AppID default_appID=AppID::Partition1;

enum class MESQualifier : std::int8_t {
	NonMES=0,
	MES=1
};

inline std::ostream &
operator<<(std::ostream &os, MESQualifier v) {
	os<<static_cast<std::underlying_type<MESQualifier>::type>(v);
	return os;
}

enum class OrderSource : std::int8_t {
	MarketParticipantDealsOnOwnAccount=1,
	InstitutionalClientMarketParticipant=3,
	RetailClientOrdersRouterDifferentMarketParticipant=7,
	InstitutionalClientOrdersRouterDifferentMarketParticipant=8,
	RetailClientMarketParticipant=9
};

inline std::ostream &
operator<<(std::ostream &os, OrderSource v) {
	os<<static_cast<std::underlying_type<OrderSource>::type>(v);
	return os;
}

enum class RestatementReason : std::uint8_t {
	GTRenewalRestatement=1u,
	OrderRePriced=3u,
	MarketOption=8u,
	PartialDeclineOrderQty=51u,
	OrderReplenishment=100u
};

inline std::ostream &
operator<<(std::ostream &os, RestatementReason v) {
	os<<static_cast<std::underlying_type<RestatementReason>::type>(v);
	return os;
}

enum class ExecType : char {
	New='0',
	Cancelled='4',
	Replaced='5',
	Rejected='8',
	Suspended='9',
	Expired='C',
	Restated='D',
	Trade='F',
	TradeCorrect='G',
	TradeCancel='H',
	Triggered='L'
};

inline std::ostream &
operator<<(std::ostream &os, ExecType v) {
	os<<static_cast<std::underlying_type<ExecType>::type>(v);
	return os;
}

enum class OrderStatus : std::uint8_t {
	New=0u,
	Partiallyfilled=1u,
	Filled=2u,
	Cancelled=4u,
	Expired=6u,
	Rejected=8u,
	Suspended=9u
};

inline std::ostream &
operator<<(std::ostream &os, OrderStatus v) {
	os<<static_cast<std::underlying_type<OrderStatus>::type>(v);
	return os;
}

enum class Container : std::uint8_t {
	None=0u,
	OrderBook=1u,	///< Or Main on JSE.
	MarketOrderContainer=3u,
	ParkedOrderQueue=5u,
	StopOrderQueue=6u,
	PeggedOrderContainer=7u,
	PeggedOrder=20u,	///< On JSE.
	EODVolumeAuctionUncross=21u	///< On JSE.
};

enum class TradeLiquidityIndicator : char {
	AddedLiquidity='A',
	RemovedLiquidity='R',
	Auction='C'
};

enum class PriceDifferential : char {
	Aggressive='A',
	NewVisibleBBO='B',
	JoinVisibleBBO='1',
	JoiningSetting2ndBestVisiblePrice='2',
	JoiningSetting3rdBestVisiblePrice='3',
	JoiningSetting4thBestVisiblePrice='4',
	JoiningSetting5thBestVisiblePrice='5',
	JoiningSetting6thBestVisiblePrice='6',
	JoiningSetting7thBestVisiblePrice='7',
	JoiningSetting8thBestVisiblePrice='8',
	JoiningSetting9thBestVisiblePriceOrWorsePricePoint='9',
	Passive='P'
};

enum class TypeOfTrade : std::uint8_t {
	Visible=0u,
	Hidden=1u,
	NotSpecified=2u
};

enum class MassCancelRequestType : std::uint8_t {
	AllFirmOrdersInstrument=3u,
	AllFirmOrdersOfSegment=4u,
	AllOrdersSubmittedByTrader=7u,
	AllFirmOrders=8u,
	AllOrdersOfInstrument=9u,
	AllOrdersOfSegment=15u
};

enum class AppStatus : std::uint8_t {
	RecoveryServiceResumed=1u,
	RecoveryServiceNotAvailable=2u,
	PartitionSuspended=3u
};

enum class MassCancelResponse : std::uint8_t {
	Rejected=0u,
	Accepted=7u
};

enum class TargetBook : std::int8_t {
	LitOrderBook=0,
	DarkMidpointOrderBook=1	///< Or Regular on JSE.	
};

enum class ExecInstruction : std::int8_t {
	Default=0,
	TurquoiseUncross=1,
	ContinuousOnly=2,
	ContinuousAndTurquoiseUncross=5
};

enum class PegPriceType : std::int8_t {
	MidPoint=0
};

enum class OrderBook : std::int8_t {
	Regular=1
};

enum class ExecutionInstruction : std::int8_t {
	DoNotExcludeHiddenOrders=0,
	ExcludeHiddenOrders=1,
	IncludeInEODVolumeAuctionUncross=2
};

enum class Status : std::uint8_t {
	AllMessagesTransmitted=0u,
	MessageLimitReached=1u,
	ServiceUnavailable=2u
};

enum class CrossType : std::uint8_t {
	InternalCross=5
};

enum class IsMarketOpsRequest : std::uint8_t {
	No=0u,
	Yes=1u
};

} } } } }

#endif
