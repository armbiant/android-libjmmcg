#ifndef ISIMUD_EXCHANGES_MIT_BIT_MESSAGES_HPP
#define ISIMUD_EXCHANGES_MIT_BIT_MESSAGES_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "mit_bit_config.h"
#include "reject_codes.hpp"

#include "../common/messages.hpp"
#include "../common/ref_data.hpp"

#include "core/max_min.hpp"

#include <boost/mpl/assert.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/max_element.hpp>
#include <boost/mpl/min_element.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/sizeof.hpp>
#include <boost/mpl/transform_view.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/variant/variant.hpp>

#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

/**
	From <a href="http://www.borsaitaliana.it/borsaitaliana/gestione-mercati/migrazionemillenniumit-mit/mit203nativetradinggatewayspecification.en_pdf.htm">"MIT203 - BIT - MILLENNIUM EXCHANGE Native Trading Gateway", Issue 6.4, October 2014</a>.
*/
namespace BIT {

/**
	Section: "7.4.1 New Order"
*/
struct [[gnu::packed]] NewOrderSpecific1 {
	common::SecurityID_t instrumentID_;
	const std::int8_t reservedField1= 0;
	const std::int8_t reservedField2= 0;

	explicit constexpr NewOrderSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID) {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
};
struct [[gnu::packed]] NewOrderSpecific2 {
	using order_qty_t= std::uint64_t;

	order_qty_t orderQty_;
	order_qty_t displayQty;

	explicit constexpr NewOrderSpecific2(order_qty_t ordQty) noexcept(true)
		: orderQty_(ordQty), displayQty(ordQty) {
	}
	order_qty_t orderQty() const noexcept(true) {
		return orderQty_;
	}
	void orderQty(order_qty_t i) noexcept(true) {
		orderQty_= i;
	}
};
struct [[gnu::packed]] NewOrderSpecific3 {
	common::Anonymity anonymity= common::Anonymity::Anonymous;
	common::Price_t stoppedPrice= 0;
	common::ReservedField10_t reservedField;
	common::OrderSource orderSource_= common::OrderSource::MarketParticipantDealsOnOwnAccount;

	static constexpr common::PassiveOnlyOrder passiveOnlyOrder() noexcept(true) {
		return common::PassiveOnlyOrder::NoConstraint;
	}
	static void passiveOnlyOrder(common::PassiveOnlyOrder) noexcept(true) {
	}
	common::OrderSource orderSource() const noexcept(true) {
		return orderSource_;
	}
	void orderSource(common::OrderSource os) noexcept(true) {
		orderSource_= os;
	}
};

/**
	Section: "7.4.2 New Quote"
*/
struct [[gnu::packed]] NewQuoteSpecific1 {
	common::Price_t bidPrice;
	uint64_t bidSize;
	common::Price_t askPrice;
	uint64_t askSize;
};
struct [[gnu::packed]] NewQuoteSpecific2 {
	common::ReservedField10_t reservedField;
	common::TIF tif;
	common::Anonymity anonymity;
};

/**
	Section: "7.4.5 Order Modification Request"
*/
struct [[gnu::packed]] OrderCancelReplaceRequestSpecific {
	common::ReservedField10_t reservedField;

	static constexpr common::PassiveOnlyOrder passiveOnlyOrder() noexcept(true) {
		return common::PassiveOnlyOrder::NoConstraint;
	}
	static void passiveOnlyOrder(common::PassiveOnlyOrder) noexcept(true) {
	}
};

/**
	Section: "7.4.8 Execution Report"
*/
struct [[gnu::packed]] ExecutionReportSpecific1 {
	using RejectCode_t= mit_bit::reject_codes_enum;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;
	using order_qty_t= std::uint64_t;

	order_qty_t executedQty;
	order_qty_t leavesQty;
	common::Container container;
	uint64_t displayQty;
};
struct [[gnu::packed]] ExecutionReportSpecific2 {
	common::ReservedField10_t reservedField1;
	common::OrderSource orderSource;
	common::Price_t avgPx;
	common::Price_t impliedPrice;
	common::CrossID_t crossID;
	std::uint8_t crossType;
	common::CrossID_t originalCrossID;
	common::RestatementReason restatementReason;
	common::OrderID_t publicOrderID;
	common::TypeOfTrade typeOfTrade;
};

struct [[gnu::packed]] OrderMassCancelRequestSpecific1 {
	common::SecurityID_t instrumentID_;
	const std::int8_t reservedField1= 0;
	const std::int8_t reservedField2= 0;
	common::Segment_t segment{};

	explicit constexpr OrderMassCancelRequestSpecific1(common::SecurityID_t instID) noexcept(true)
		: instrumentID_(instID) {
	}
	common::SecurityID_t instrumentID() const noexcept(true) {
		return instrumentID_;
	}
	void instrumentID(common::SecurityID_t i) noexcept(true) {
		instrumentID_= i;
	}
};

struct [[gnu::packed]] LogonReply : public common::LogonReply<mit_bit::reject_codes_enum> {
	using base_t= common::LogonReply<mit_bit::reject_codes_enum>;
	using RejectCode_t= base_t::RejectCode_t;
	using RejectCodes_t= std::underlying_type<RejectCode_t>::type;
	using base_t::base_t;

	static inline constexpr const RejectCode_t logon_success= mit_bit::reject_codes_enum::tag_SUCCESS;
	static inline constexpr const RejectCode_t invalid_logon_details= mit_bit::reject_codes_enum::AuthServer_INVALID_USER_OR_CREDENTIALS_1;
	static inline constexpr const RejectCode_t unknown_user= mit_bit::reject_codes_enum::AuthServer_USER_NOT_FOUND_4;
};

struct [[gnu::packed]] NewOrder : public common::NewOrder<NewOrderSpecific1, NewOrderSpecific2, NewOrderSpecific3> {
	using base_t= common::NewOrder<NewOrderSpecific1, NewOrderSpecific2, NewOrderSpecific3>;
	using base_t::base_t;
	using order_qty_t= NewOrderSpecific2::order_qty_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	__stdcall NewOrder(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct [[gnu::packed]] OrderCancelReplaceRequest : public common::OrderCancelReplaceRequest<NewOrderSpecific1, NewOrderSpecific2, OrderCancelReplaceRequestSpecific> {
	using base_t= common::OrderCancelReplaceRequest<NewOrderSpecific1, NewOrderSpecific2, OrderCancelReplaceRequestSpecific>;
	using base_t::base_t;
	using order_qty_t= NewOrderSpecific2::order_qty_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	__stdcall OrderCancelReplaceRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct [[gnu::packed]] OrderCancelRequest : public common::OrderCancelRequest<NewOrderSpecific1> {
	using base_t= common::OrderCancelRequest<NewOrderSpecific1>;
	using base_t::base_t;
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	/// Create a message from the source message.
	/**
		If a linker error is generated, then this function will need to be specialised for the particular Msg-type.

		\param	msg	The source message from which the target message should be created.
	*/
	template<class SrcMsg>
	__stdcall OrderCancelRequest(SrcMsg const& msg, isin_mapping_data_const_reference rd) noexcept(true);
};

struct MsgTypes {
	static inline constexpr const exchanges::common::mic_codes::ISO_10383_MIC_Codes MIC_code= exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_EURONEXT_MILAN_MTAA;

	using ref_data= common::ref_data;
	template<class Op>
	static ref_data create_ref_data(Op&& op) noexcept(true) {
		auto&& ref_data_src= op();
		return ref_data{ref_data_src};
	}
	using isin_mapping_data_const_reference= common::isin_mapping_data_const_reference;

	using MsgType_t= common::MsgType_t;
	using MsgTypes_t= common::MsgType;
	using UserName_t= common::UserName_t;
	using Password_t= common::Password_t;
	using SecurityID_t= common::SecurityID_t;
	using SeqNum_t= common::SeqNum_t;
	using Price_t= common::Price_t;
	using Quantity_t= NewOrderSpecific2::order_qty_t;
	using ClientOrderID_t= common::ClientOrderID_t;
	using OrderType= common::OrderType;
	using Side= common::Side;
	using TIF= common::TIF;
	using ExecType= common::ExecType;
	using AppID= common::AppID;
	using OrderStatus= common::OrderStatus;
	using logon_args_t= common::logon_args_t;

	using Header_t= common::Header;
	using NewOrder_t= BIT::NewOrder;
	using OrderCancelRequest_t= BIT::OrderCancelRequest;
	using OrderMassCancelRequest_t= common::OrderMassCancelRequest<OrderMassCancelRequestSpecific1>;
	using OrderCancelReplaceRequest_t= BIT::OrderCancelReplaceRequest;
	using NewQuote_t= common::NewQuote<NewQuoteSpecific1, NewQuoteSpecific2>;
	using LogonRequest_t= common::LogonRequest;
	using LogoutRequest_t= common::LogoutRequest;
	using ClientHeartbeat_t= common::Heartbeat;
	using ServerHeartbeat_t= common::Heartbeat;
	using MissedMessageRequest_t= common::MissedMessageRequest;
	using ExecutionReport_t= common::ExecutionReport<ExecutionReportSpecific1, ExecutionReportSpecific2>;
	using OrderCancelReject_t= common::OrderCancelReject<common::OrderCancelRejectSpecific<mit_bit::reject_codes_enum>>;
	using OrderMassCancelReport_t= common::OrderMassCancelReport<common::OrderMassCancelReportSpecific<mit_bit::reject_codes_enum>>;
	using BusinessReject_t= common::BusinessReject<common::BusinessRejectSpecific<mit_bit::reject_codes_enum, mit_bit::reject_codes_enum::MatchingEngine_Unknown_instrument_9000>>;
	using LogonReply_t= BIT::LogonReply;
	using MissedMessageRequestAck_t= common::MissedMessageRequestAck;
	using MissedMessageReport_t= common::MissedMessageReport;
	using Reject_t= common::Reject<mit_bit::reject_codes_enum, mit_bit::reject_codes_enum::NativeGateway_Message_not_supported_102>;
	using SystemStatus_t= common::SystemStatus;

	using Logout_t= LogoutRequest_t;

	using client_to_exchange_messages_t= boost::mpl::vector<
		NewOrder_t,
		OrderCancelRequest_t,
		OrderMassCancelRequest_t,
		OrderCancelReplaceRequest_t,
		NewQuote_t,
		LogonRequest_t,
		LogoutRequest_t,
		ClientHeartbeat_t,
		MissedMessageRequest_t>;

	using exchange_to_client_messages_t= boost::mpl::vector<
		ExecutionReport_t,
		OrderCancelReject_t,
		OrderMassCancelReport_t,
		BusinessReject_t,
		LogonReply_t,
		Logout_t,
		ServerHeartbeat_t,
		MissedMessageRequestAck_t,
		MissedMessageReport_t,
		Reject_t,
		SystemStatus_t>;

	enum : std::size_t {
		min_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		max_size_client_to_exchange_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<client_to_exchange_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		min_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::min_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		max_size_exchange_to_client_msg= sizeof(
			boost::mpl::deref<
				boost::mpl::max_element<
					boost::mpl::transform_view<exchange_to_client_messages_t, boost::mpl::sizeof_<boost::mpl::_1>>>::type::base>::type),
		min_msg_size= libjmmcg::min<std::size_t, min_size_client_to_exchange_msg, min_size_exchange_to_client_msg>::value,
		max_msg_size= libjmmcg::max<std::size_t, max_size_client_to_exchange_msg, max_size_exchange_to_client_msg>::value,
		header_t_size= LogonRequest_t::header_t_size
	};

	/**
	 * We must use a std::byte here, because this is used for aliasing: the buffer into which the message is read or written from a socket must be able to use this, which should then permit reinterpret_cast<...>(...) to work correctly, according to the Standard. Unfortunately there is some casting finagling as a side-effect in the implementation to a std::byte and back as that underlying type is not so protected by the Standard.
	 */
	using msg_buffer_t= std::array<std::byte, max_msg_size>;
	using client_to_exchange_messages_container= boost::make_variant_over<client_to_exchange_messages_t>::type;
	using exchange_to_client_messages_container= boost::make_variant_over<exchange_to_client_messages_t>::type;

	static inline constexpr const Price_t implied_decimal_places= common::implied_decimal_places;

	template<class ConnPolT, class SktT, class ThrdT>
	static auto make_ctor_args(boost::program_options::variables_map const& vm) noexcept(false) {
		return common::make_ctor_args<MsgTypes, ConnPolT, SktT, ThrdT>(vm);
	}

	static std::ostream& to_stream(std::ostream&) noexcept(false);
};

/**
	\test MIT BIT size tests.
*/
namespace tests {

BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_client_to_exchange_msg);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, MsgTypes::max_size_exchange_to_client_msg);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_client_to_exchange_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(MsgTypes::max_size_exchange_to_client_msg, <=, MsgTypes::max_msg_size);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Header_t), <=, sizeof(MsgTypes::msg_buffer_t));
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonRequest_t), ==, 80);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogonReply_t), ==, 38);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::LogoutRequest_t), ==, 24);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ClientHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ServerHeartbeat_t), ==, 4);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequest_t), ==, 9);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageRequestAck_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::MissedMessageReport_t), ==, 5);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::Reject_t), ==, 59);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::SystemStatus_t), ==, 6);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewOrder_t), ==, 106);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::NewQuote_t), ==, 86);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReplaceRequest_t), ==, 120);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelRequest_t), ==, 73);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelRequest_t), ==, 46);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::ExecutionReport_t), ==, 229);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderCancelReject_t), ==, 63);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::OrderMassCancelReport_t), ==, 56);
BOOST_MPL_ASSERT_RELATION(sizeof(MsgTypes::BusinessReject_t), ==, 63);

}

}
}}}}

#include "messages_impl.hpp"

#endif
