#ifndef ISIMUD_EXCHANGES_conversions_batsboe_eu_to_fix_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_batsboe_eu_to_fix_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "batsboe_common_to_fix_conversions.hpp"
#include "../../exchanges/BATSBOE/EU/v1/messages.hpp"
#include "../../exchanges/BATSBOE/EU/v2/messages.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

template<>
template<> inline
Message<v5_0sp2::ExecutionReportSpecific>::Message(exchanges::BATSBOE::common::OrderExecution<exchanges::BATSBOE::EU::MsgType> const &msg)
: Header_t() {
	underlying_fix_data_buffer::iterator data=set_sequence_num<MsgTypes_t::ExecutionReport>(msg);
	data=add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<BATSBOE::common::ClientOrderID_t &>(*data));
	data+=msg.clientOrderID().size()-1;
	data=add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data=static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data=add_field_tag<FieldsFast::SecurityID>(data);
	libjmmcg::memcpy_opt(msg.instrumentID(), reinterpret_cast<BATSBOE::common::SecurityID_t &>(*data));
	data+=msg.instrumentID().size()-1;
	data=add_field_tag<FieldsFast::ExecType>(data);
	*data=static_cast<std::underlying_type<FIX::common::ExecType>::type>(FIX::common::ExecType::New);
	++data;
	data=add_field_tag<FieldsFast::OrdStatus>(data);
	*data=static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(FIX::common::OrdStatus::New);
	++data;
	data=add_field_tag<FieldsFast::Price>(data);
	const std::size_t j=convert_price(msg.executedPrice(), data, this->data_.size()-(data-this->data_.begin()));
	data+=j;
	data=add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k=convert(msg.executedQty(), data, this->data_.size()-(data-this->data_.begin()));
	data+=k;
	data=add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l=convert(msg.leavesQty(), data, this->data_.size()-(data-this->data_.begin()));
	data+=l;
	data=add_field_tag<FieldsFast::Side>(data);
	*data=static_cast<std::underlying_type<Side>::type>(convert(msg.side()));
	++data;
// TODO	msg.orderRejectCode();
	finalise_msg(data);
}

template<>
template<> inline
Message<v5_0sp2::OrderRejectedSpecific>::Message(exchanges::BATSBOE::common::OrderRejected<exchanges::BATSBOE::EU::MsgType> const &msg)
: Header_t() {
	underlying_fix_data_buffer::iterator data=set_sequence_num<MsgTypes_t::Reject>(msg);
	data=add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<BATSBOE::common::ClientOrderID_t &>(*data));
	data+=msg.clientOrderID().size()-1;
	data=add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i=convert(msg.sequenceNumber, data, this->data_.size()-(data-this->data_.begin()));
	data+=i;
	finalise_msg(data);
}

template<>
template<> inline
Message<v5_0sp2::CancelRejectedSpecific>::Message(exchanges::BATSBOE::common::CancelRejected<exchanges::BATSBOE::EU::MsgType> const &msg)
: Header_t() {
	underlying_fix_data_buffer::iterator data=set_sequence_num<MsgTypes_t::OrderCancelReject>(msg);
	data=add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<BATSBOE::common::ClientOrderID_t &>(*data));
	data+=msg.clientOrderID().size()-1;
// TODO	cancelRejectReason_;
// TODO	transactTime;
	finalise_msg(data);
}

template<>
template<> inline
Message<v5_0sp2::BusinessMessageRejectSpecific>::Message(exchanges::BATSBOE::common::UserModifyRejected<exchanges::BATSBOE::EU::MsgType> const &msg)
: Header_t() {
	underlying_fix_data_buffer::iterator data=set_sequence_num<MsgTypes_t::BusinessMessageReject>(msg);
	data=add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<BATSBOE::common::ClientOrderID_t &>(*data));
	data+=msg.clientOrderID().size()-1;
	data=add_field_tag<FieldsFast::RefSeqNum>(data);
	const std::size_t i=convert(msg.sequenceNumber, data, this->data_.size()-(data-this->data_.begin()));
	data+=i;
// TODO
	finalise_msg(data);
}

template<>
template<> inline
Message<v5_0sp2::ExecutionReportSpecific>::Message(exchanges::BATSBOE::common::OrderCancelled<exchanges::BATSBOE::EU::MsgType> const &msg)
: Header_t() {
	underlying_fix_data_buffer::iterator data=set_sequence_num<MsgTypes_t::ExecutionReport>(msg);
	data=add_field_tag<FieldsFast::ClOrdID>(data);
	libjmmcg::memcpy_opt(msg.clientOrderID(), reinterpret_cast<BATSBOE::common::ClientOrderID_t &>(*data));
	data+=msg.clientOrderID().size()-1;
	data=add_field_tag<FieldsFast::ExecType>(data);
	*data=static_cast<std::underlying_type<FIX::common::ExecType>::type>(FIX::common::ExecType::Canceled);
	++data;
	data=add_field_tag<FieldsFast::OrdStatus>(data);
	*data=static_cast<std::underlying_type<FIX::common::OrdStatus>::type>(FIX::common::OrdStatus::Canceled);
	++data;
	data=add_field_tag<FieldsFast::SecurityIDSource>(data);
	*data=static_cast<std::underlying_type<SecurityIDSource>::type>(SecurityIDSource::ISIN);
	++data;
	data=add_field_tag<FieldsFast::SecurityID>(data);
	const std::size_t i=convert(msg.symbol(), data, this->data_.size()-(data-this->data_.begin()));
	data+=i;
	data=add_field_tag<FieldsFast::Price>(data);
	const std::size_t j=convert_price(msg.lastPrice(), data, this->data_.size()-(data-this->data_.begin()));
	data+=j;
	data=add_field_tag<FieldsFast::OrderQty>(data);
	const std::size_t k=convert(msg.orderQty(), data, this->data_.size()-(data-this->data_.begin()));
	data+=k;
	data=add_field_tag<FieldsFast::LeavesQty>(data);
	const std::size_t l=convert(msg.leavesQty(), data, this->data_.size()-(data-this->data_.begin()));
	data+=l;
	data=add_field_tag<FieldsFast::Side>(data);
	*data=static_cast<std::underlying_type<Side>::type>(convert(msg.side()));
	++data;
// TODO	msg.orderRejectCode();
	finalise_msg(data);
}

} } } } }

#endif
