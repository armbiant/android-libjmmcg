#ifndef ISIMUD_EXCHANGES_conversions_fix_to_mit_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_fix_to_mit_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../exchanges/FIX/v5.0sp2/messages.hpp"
#include "../../exchanges/FIX/common/conversions.hpp"

#include "../../exchanges/MIT/BIT/messages.hpp"
#include "../../exchanges/MIT/JSE/messages.hpp"
#include "../../exchanges/MIT/LSE/messages.hpp"
#include "../../exchanges/MIT/OSLO/messages.hpp"
#include "../../exchanges/MIT/TRQ/messages.hpp"

#include "core/memops.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

namespace common {

template<class Ret> Ret
convert(FIX::common::field_str_const_range_t const &) [[gnu::pure]] =delete;

/**
	\todo What should be done with OrderType's that cannot be mapped?.
*/	
template<> constexpr inline OrderType
convert<OrderType>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)==1);
	switch (*a.first) {
	case '1':
		return OrderType::Market;
	case '2':
		return OrderType::Limit;
	case '3':
		return OrderType::Stop;
	case '4':
		return OrderType::StopLimit;
	default:
		return static_cast<OrderType>(0);
	};
}

/**
	\todo What should be done with TIF's that cannot be mapped?.
*/	
template<> inline constexpr TIF
convert<TIF>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)==1);
	switch (*a.first) {
	case '0':
		return TIF::Day;
	case '2':
		return TIF::OPG;
	case '3':
		return TIF::IOC;
	case '4':
		return TIF::FOK;
	case '6':
		return TIF::GTD;
	case '7':
		return TIF::ATC;
	default:
		return static_cast<TIF>(0);
	};
}

/**
	\todo What should be done with Side's that cannot be mapped?.
*/	
template<> constexpr inline Side
convert<Side>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)==1);
	switch (*a.first) {
	case '1':
		return Side::Buy;
	case '2':
		return Side::Sell;
	default:
		return static_cast<Side>(0);
	};
}

/**
	Note that only ISIN symbology is supported.
*/
template<class MsgT> constexpr inline SecurityID_t
convert_SecurityID_t(MsgT const &msg, MIT::common::isin_mapping_data_const_reference ref_data) {
	auto const &isin_ptrs=msg.template find<FIX::common::FieldsFast::SecurityID>();
	assert((isin_ptrs.second-isin_ptrs.first)==sizeof(MIT::common::ref_data::key_type));
	const MIT::common::ref_data::key_type::element_type isin(reinterpret_cast<MIT::common::ref_data::key_type::element_type::block_t const &>(*isin_ptrs.first));
	const MIT::common::ref_data::key_type key(isin);
	return ref_data.at(key);
}

template<> inline Price_t
convert<Price_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return static_cast<Price_t>(libjmmcg::fromstring<double>(a.first, a.second-a.first)*implied_decimal_places);
}

template<> inline __stdcall
LogonRequest::LogonRequest(FIX::v5_0sp2::MsgTypes::LogonRequest_t const &msg) noexcept(true)
: 	LogonRequest(
		libjmmcg::copy<typename LogonRequest::logon_args_t::UserName_t>(msg.find<FIX::common::FieldsFast::Username>()),
		libjmmcg::copy<typename LogonRequest::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::Password>()),
		libjmmcg::copy<typename LogonRequest::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::NewPassword>())
	) {
	assert(is_valid());
}

template<> inline __stdcall constexpr
LogoutRequest::LogoutRequest(FIX::v5_0sp2::MsgTypes::LogoutRequest_t const &, Reason_t r) noexcept(true)
: 	LogoutRequest(r) {
	assert(is_valid());
}

template<> inline __stdcall constexpr
Heartbeat::Heartbeat(FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const &) noexcept(true)
: Heartbeat() {
	assert(is_valid());
}

}

namespace BIT {

template<> inline __stdcall
NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	common::convert<common::OrderType>(msg.find<FIX::common::FieldsFast::OrdType>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

}

namespace JSE {

template<> inline __stdcall
Logon::Logon(FIX::v5_0sp2::MsgTypes::LogonRequest_t const &msg) noexcept(true)
: 	Logon(
		libjmmcg::copy<typename Logon::logon_args_t::UserName_t>(msg.find<FIX::common::FieldsFast::Username>()),
		libjmmcg::copy<typename Logon::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::Password>()),
		libjmmcg::copy<typename Logon::logon_args_t::Password_t>(msg.find<FIX::common::FieldsFast::NewPassword>())
	) {
	assert(is_valid());
}

template<> inline __stdcall
NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: NewOrder(
	static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	common::convert<common::OrderType>(msg.find<FIX::common::FieldsFast::OrdType>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: OrderCancelReplaceRequest(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

}

namespace LSE {

template<> inline __stdcall
NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	common::convert<common::OrderType>(msg.find<FIX::common::FieldsFast::OrdType>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

}

namespace OSLO {

template<> inline __stdcall
NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	common::convert<common::OrderType>(msg.find<FIX::common::FieldsFast::OrdType>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

}

namespace TRQ {

template<> inline __stdcall
NewOrder::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	static_cast<MIT::common::SeqNum_t>(FIX::common::convert<FIX::common::SeqNum_t>(msg.find<FIX::common::FieldsFast::MsgSeqNum>())),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	common::convert<common::OrderType>(msg.find<FIX::common::FieldsFast::OrdType>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelRequest::OrderCancelRequest(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

template<> inline __stdcall
OrderCancelReplaceRequest::OrderCancelReplaceRequest(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &msg, isin_mapping_data_const_reference rd) noexcept(true)
: base_t(
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
	libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::OrigClOrdID>()),
	common::convert_SecurityID_t(msg, rd),
	FIX::common::convert<order_qty_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
	common::convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
	common::convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
	common::convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
) {
	assert(is_valid());
}

}

} } } }

#endif
