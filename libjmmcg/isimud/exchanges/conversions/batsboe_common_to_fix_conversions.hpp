#ifndef ISIMUD_EXCHANGES_conversions_batsboe_common_to_fix_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_batsboe_common_to_fix_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../exchanges/FIX/v5.0sp2/messages.hpp"
#include "../BATSBOE/common/messages.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace FIX { namespace common {

constexpr inline Side
convert(BATSBOE::common::Side a) {
	switch (a) {
	case BATSBOE::common::Side::Buy:
		return Side::Buy;
	case BATSBOE::common::Side::Sell:
		return Side::Sell;
	case BATSBOE::common::Side::Sell_short:
		return Side::Sell_short;
	case BATSBOE::common::Side::Sell_short_exempt:
	default:
		return Side::Sell_short_exempt;
	};
}

inline std::size_t
convert_price(BATSBOE::common::Price_t const &a, pointer buff, std::size_t sz) {
	const double c=static_cast<double>(a)/BATSBOE::common::implied_decimal_places;
	return libjmmcg::tostring(c, buff, sz);
}

/**
 *	Note that only ISIN symbology is supported.
 */	
inline std::size_t
convert(BATSBOE::common::Symbol_t const &s, pointer buff, [[maybe_unused]] std::size_t sz) {
	libjmmcg::memcpy_opt(s, reinterpret_cast<BATSBOE::common::Symbol_t &>(*buff));
	return sizeof(BATSBOE::common::Symbol_t);
}

} } } } }

#endif
