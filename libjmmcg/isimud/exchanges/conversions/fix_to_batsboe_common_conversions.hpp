#ifndef ISIMUD_EXCHANGES_conversions_fix_to_batsboe_common_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_fix_to_batsboe_common_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../exchanges/FIX/v5.0sp2/messages.hpp"
#include "../../exchanges/FIX/common/conversions.hpp"
#include "../BATSBOE/common/messages.hpp"

#include "../common/isin.hpp"

#include <cstring>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

template<class Ret> constexpr Ret
convert(FIX::common::field_str_const_range_t const &)=delete;

/**
	\todo What should be done with OrdType's that cannot be mapped?.
*/	
template<> constexpr inline OrdType
convert<OrdType>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)==1);
	switch (*a.first) {
	case '1':
		return OrdType::Market;
	case '2':
		return OrdType::Limit;
	case 'P':
		return OrdType::Pegged;
	default:
		return static_cast<OrdType>(0);
	};
}

/**
	\todo What should be done with TIF's that cannot be mapped?.
*/	
template<> constexpr inline TIF
convert<TIF>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)==1);
	switch (*a.first) {
	case '0':
		return TIF::Day;
	case '1':
		return TIF::GTC;
	case '2':
		return TIF::ATO;
	case '3':
		return TIF::IOC;
	case '6':
		return TIF::GTD;
	case '7':
		return TIF::ATC;
	default:
		return static_cast<TIF>(0);
	};
}

/**
	\todo What should be done with Side's that cannot be mapped?.
*/	
template<> constexpr inline Side
convert<Side>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)==1);
	switch (*a.first) {
	case '1':
		return Side::Buy;
	case '2':
		return Side::Sell;
	case '5':
		return Side::Sell_short;
	case '6':
		return Side::Sell_short_exempt;
	default:
		return static_cast<Side>(0);
	};
}

/**
	\todo We need to consider the symbology used, for example a conversion from ISIN to MIT native symbology.
*/	
template<> inline SecurityID_t
convert<SecurityID_t>(FIX::common::field_str_const_range_t const &a) {
	SecurityID_t ret;
	assert((a.second-a.first)>0);
	assert(static_cast<SecurityID_t::size_type>(a.second-a.first)<ret.size());
	std::memcpy(ret.data(), a.first, (a.second-a.first));
	std::fill_n(ret.data()+(a.second-a.first), ret.size()-(a.second-a.first), '\0');
	return ret;
}

/**
	Note that the SEDOL portion of the ISIN is used as the Symbology.
*/	
template<> inline Symbol_t
convert<Symbol_t>(FIX::common::field_str_const_range_t const &isin_ptrs) {
	BOOST_MPL_ASSERT_RELATION(sizeof(Symbol_t), >=, sizeof(exchanges::common::ISIN_t::SEDOL_t::padded_SEDOL::element_type));
	assert((isin_ptrs.second-isin_ptrs.first)==sizeof(Symbol_t));
	Symbol_t ret;
	assert((isin_ptrs.second-isin_ptrs.first)>0);
	const exchanges::common::ISIN_t isin(reinterpret_cast<exchanges::common::ISIN_t::block_t const &>(*isin_ptrs.first));
	auto const &sedol=isin.details().sedol.padded_sedol.value;
	libjmmcg::memcpy_opt(sedol, ret);
	std::fill_n(ret.data()+sedol.size(), ret.size()-sedol.size(), '\0');
	return ret;
}

template<> inline Price_t
convert<Price_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return static_cast<Price_t>(libjmmcg::fromstring<double>(a.first, a.second-a.first)*implied_decimal_places);
}

template<> inline Quantity_t
convert<Quantity_t>(FIX::common::field_str_const_range_t const &a) {
	assert((a.second-a.first)>0);
	return static_cast<Quantity_t>(libjmmcg::fromstring<double>(a.first, a.second-a.first)*implied_decimal_places);
}

} } } } }

#endif
