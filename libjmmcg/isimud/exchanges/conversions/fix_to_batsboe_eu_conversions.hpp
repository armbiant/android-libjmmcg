#ifndef ISIMUD_EXCHANGES_conversions_fix_to_batsboe_eu_conversions_hpp
#define ISIMUD_EXCHANGES_conversions_fix_to_batsboe_eu_conversions_hpp

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "fix_to_batsboe_common_conversions.hpp"

#include "../../exchanges/FIX/v5.0sp2/messages.hpp"
#include "../../exchanges/BATSBOE/EU/v1/messages.hpp"
#include "../../exchanges/BATSBOE/EU/v2/messages.hpp"

#include "core/memops.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace BATSBOE { namespace common {

template<>
template<> inline
LogoutRequest<EU::MsgType>::LogoutRequest(FIX::v5_0sp2::MsgTypes::LogoutRequest_t const &, SeqNum_t seqNum) noexcept(true)
: LogoutRequest(seqNum) {
}

template<>
template<> inline
ClientHeartbeat<EU::MsgType>::ClientHeartbeat(FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const &, SeqNum_t seqNum) noexcept(true)
: ClientHeartbeat(seqNum) {
}

template<>
template<> inline
ServerHeartbeat<EU::MsgType>::ServerHeartbeat(FIX::v5_0sp2::MsgTypes::ServerHeartbeat_t const &, SeqNum_t seqNum) noexcept(true)
: ServerHeartbeat(seqNum) {
}

/**
	Note that only ISIN symbology is supported.
*/
template<>
template<> inline
NewOrder<EU::MsgType>::NewOrder(FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &msg, SeqNum_t seqNum) noexcept(true)
: NewOrder(
		seqNum,
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		convert<common::OrdType>(msg.find<FIX::common::FieldsFast::OrdType>()),
		convert<common::TIF>(msg.find<FIX::common::FieldsFast::TimeInForce>()),
		convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>()),
		convert<common::Symbol_t>(msg.find<FIX::common::FieldsFast::SecurityID>()),
		convert<common::SecurityID_t>(msg.find<FIX::common::FieldsFast::SecurityID>()),
		FIX::common::convert<common::Quantity_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>())
	) {
	// We only support ISIN.
	assert(bitfields.find<bitfields_tags_type::IDSource>());
}

template<>
template<> inline
CancelOrder<EU::MsgType>::CancelOrder(FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &msg, SeqNum_t seqNum) noexcept(true)
: CancelOrder(
		seqNum,
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>())
	) {
}

template<>
template<> inline
ModifyOrder<EU::MsgType>::ModifyOrder(FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &msg, SeqNum_t seqNum) noexcept(true)
: ModifyOrder(
		seqNum,
		libjmmcg::copy<common::ClientOrderID_t>(msg.find<FIX::common::FieldsFast::ClOrdID>()),
		FIX::common::convert<common::Quantity_t>(msg.find<FIX::common::FieldsFast::OrderQty>()),
		convert<common::Price_t>(msg.find<FIX::common::FieldsFast::Price>()),
		convert<common::Side>(msg.find<FIX::common::FieldsFast::Side>())
	) {
}

template<>
template<> inline
LogonRequest<EU::MsgType>::LogonRequest(FIX::v5_0sp2::MsgTypes::LogonRequest_t const &msg, SeqNum_t seqNum, const SessionSubID_t sessionSubID, const bool noUnspec)
: LogonRequest(
		seqNum,
		sessionSubID,
		libjmmcg::copy<UserName_t>(msg.find<FIX::common::FieldsFast::Username>()),
		libjmmcg::copy<Password_t>(msg.find<FIX::common::FieldsFast::Password>()),
		noUnspec
	) {
}

} } } } }

#endif
