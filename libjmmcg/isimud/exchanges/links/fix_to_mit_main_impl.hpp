/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

template<class Link, class CxnT> inline int
fix_to_mit_link<Link, CxnT>::main(int argc, char const * const *argv) noexcept(true) {
	return base_t::main<link_t>(
		argc,
		argv,
		MIT::create_program_options<connection_t>(create_program_options()),
		[](boost::program_options::variables_map const &vm) {
			return std::tuple{
				base_t::make_client_ctor_args<link_t, connection_t>(vm),
				link_t::exchg_links_t::exchg_link_t::template make_ctor_args<exchanges::common::thread_traits>(vm)
			};
		}
	);
}

} } }
