#ifndef ISIMUD_EXCHANGES_links_fix_to_general_program_options_hpp
#define ISIMUD_EXCHANGES_links_fix_to_general_program_options_hpp

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "common/isimud_config.h"
#include "fix_links_fix_links_program_options_export.h"

#include "core/application.hpp"

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

boost::program_options::options_description
create_program_options() noexcept(false);

} } }

#endif
