/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges { namespace MIT {

template<class CxnT>
inline boost::program_options::options_description
create_program_options(boost::program_options::options_description &&all) noexcept(false) {
	using connection_t=CxnT;
	using exchg_msg_details_t=typename connection_t::msg_details_t;
	using exchg_logon_details=MIT::common::simulator_responses<exchg_msg_details_t, typename connection_t::socket_t>;

	std::string const details=[]() {
		std::ostringstream ss;
		ss<<"ISO 10383: MIC Codes official information:\n";
		exchanges::common::mic_codes::mic_to_stream::result<exchg_msg_details_t::MIC_code>(ss);
		return ss.str();
	}();

	std::string const MIC_code=libjmmcg::enum_tags::mpl::to_array<exchanges::common::mic_codes::ISO_10383_MIC_Codes, exchg_msg_details_t::MIC_code>::value;
	boost::program_options::options_description link_opts((MIC_code+" link options").c_str());
	link_opts.add_options()
	(
		(MIC_code+"_ref_data").c_str(),
		boost::program_options::value<std::string>()->required(),
		"The file-path of a file that contains the full reference-data supplied for the exchange, that must map ISINs to exchange-specific instrument IDs in CSV format separated by semicolons."
	);
	boost::program_options::options_description exchange("Exchange options (the defaults are suitable for use with the simulator)");
	exchange.add_options()
	(
		(MIC_code+"_primary_gateway_address").c_str(),
		boost::program_options::value<std::string>()->required()->default_value(boost::asio::ip::address_v4::loopback().to_string()),
		"IP address (in v4 format) of the primary gateway to which the translator should connect."
	)
	(
		(MIC_code+"_primary_gateway_port").c_str(),
		boost::program_options::value<unsigned short>()->required(),
		"The port of the primary gateway to which the translator should connect."
	)
	(
		(MIC_code+"_secondary_gateway_address").c_str(),
		boost::program_options::value<std::string>()->required()->default_value(boost::asio::ip::address_v4::loopback().to_string()),
		"IP address (in v4 format) of the secondary gateway to which the translator should connect."
	)
	(
		(MIC_code+"_secondary_gateway_port").c_str(), 
		boost::program_options::value<unsigned short>()->required(),
		"The port of the secondary gateway to which the translator should connect."
	)
	(
		(MIC_code+"_username").c_str(),
		boost::program_options::value<std::string>()->required()->default_value(exchg_logon_details::username.begin()),
		"The username with which to connect to the exchange."
	)
	(
		(MIC_code+"_password").c_str(),
		boost::program_options::value<std::string>()->required()->default_value(exchg_logon_details::password.begin()),
		"The password with which to connect to the exchange."
	)
	(
		(MIC_code+"_new_password").c_str(),
		boost::program_options::value<std::string>()->required()->default_value(exchg_logon_details::new_password.begin()),
		"The new password with which to connect to the exchange."
	)
	(
		(MIC_code+"_logout_reason").c_str(),
		boost::program_options::value<std::string>()->required()->default_value("TTFN"),
		"The reason that should be used when logging out of the connected exchange."
	);
	boost::program_options::options_description all_opts(details);
	all_opts.add(link_opts).add(exchange);
	all.add(all_opts);
	return std::move(all);
}

} } } }
