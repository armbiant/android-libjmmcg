/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

template<class... ExchgLinks>
struct fix_to_links::get_links_ctor_args<std::tuple<ExchgLinks...>> {
	static auto result(boost::program_options::variables_map const &vm) noexcept(false) {
		return std::make_tuple(
			ExchgLinks::exchg_link_t::template make_ctor_args<exchanges::common::thread_traits>(vm)...
		);
	}
};

template<class... ExchgLinks>
struct fix_to_links::get_all_ctor_args {
	static auto result(boost::program_options::variables_map const &vm) noexcept(false) {
		return std::make_tuple(
			fix_to_links::base_t::make_client_ctor_args<links_t, FIX::v5_0sp2::connection_t>(vm),
			get_links_ctor_args<ExchgLinks>::result(vm)...
		);
	}
};

inline int
fix_to_links::main(int argc, char const * const *argv) noexcept(true) {
	return base_t::main<links_t>(
		argc,
		argv,
		BATSBOE::create_program_options<exchanges::BATSBOE::EU::v1::connection_t>(
			BATSBOE::create_program_options<exchanges::BATSBOE::US::v1::connection_t>(
				MIT::create_program_options<exchanges::MIT::BIT::connection_t>(
					MIT::create_program_options<exchanges::MIT::JSE::connection_t>(
						MIT::create_program_options<exchanges::MIT::LSE::connection_t>(
							MIT::create_program_options<exchanges::MIT::OSLO::connection_t>(
								MIT::create_program_options<exchanges::MIT::TRQ::connection_t>(
									create_program_options()
								)
							)
						)
					)
				)
			)
		),
		[](boost::program_options::variables_map const &vm) {
			return get_all_ctor_args<links_t::exchg_links_t::exchg_links_t::all_elements_t>::result(vm);
		}
	);
}

} } }
