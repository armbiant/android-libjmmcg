#ifndef ISIMUD_EXCHANGES_links_fix_to_link_main_hpp
#define ISIMUD_EXCHANGES_links_fix_to_link_main_hpp

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../FIX/v5.0sp2/fix.hpp"
#include "../common/iso_10383_mic_codes.hpp"
#include "../common/iso_3166_country_codes.hpp"
#include "../common/iso_4217_currency_codes.hpp"

#include "core/application.hpp"
#include "core/latency_timestamps.hpp"
#include "core/lock_mem.hpp"
#include "core/thread_api_traits.hpp"

#include <boost/exception/diagnostic_information.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/program_options.hpp>

#include <csignal>
#include <iostream>

namespace isimud { namespace ISIMUD_VER_NAMESPACE { namespace exchanges {

class fix_to_link : public libjmmcg::application {
protected:
	using base_t= libjmmcg::application;
	using base_t::application;

	template<class Link, class CxnT>
	static auto make_client_ctor_args(boost::program_options::variables_map const& vm) noexcept(false);
	template<class Link, class Operation>
	static int main(int argc, char const* const* argv, boost::program_options::options_description&& all, Operation&& op) noexcept(true);
};

}}}

#include "fix_to_link_main_impl.hpp"

#endif
