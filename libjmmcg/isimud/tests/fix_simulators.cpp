/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/FIX/v5.0sp2/fix.hpp"
#include "../exchanges/FIX/v5.0sp2/fix_sim.hpp"

#include "core/latency_timestamps.hpp"

using namespace libjmmcg;
using namespace libisimud;

const boost::asio::ip::address localhost(boost::asio::ip::address_v4::loopback());
const unsigned short unused_primary_port=12347u;

typedef boost::mpl::list<
	std::pair<exchanges::FIX::v5_0sp2::connection_t, exchanges::FIX::v5_0sp2::simulator_t>
> exchg_t_types;

using api_thread_traits=ppd::thread_params<ppd::platform_api>;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_SUITE(links)

/**
	\test "Connectivity Policy" Test: gateway unavailable.
			================================================
	Verify that more than 1*5 seconds passes if both gateways are unavailable.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(gateway_unavailable, exchg_t, exchg_t_types) {
	using connection_t=typename exchg_t::first_type;
	using conn_pol_t=typename connection_t::conn_pol_t;
	typename conn_pol_t::gateways_t gateways(
		std::make_pair(localhost, unused_primary_port)
	);
	conn_pol_t conn_pol(gateways);
	auto const &begin=std::chrono::system_clock::now();
	BOOST_CHECK_THROW(
		typename exchg_t::first_type link(
			conn_pol,
			exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::client_to_exchange_thread.core
		),
		std::exception
	);
	auto const &end=std::chrono::system_clock::now();
	BOOST_CHECK_GE(std::chrono::duration_cast<std::chrono::seconds>(end-begin).count(), (conn_pol_t::max_attempts*conn_pol_t::min_timeout).count());
}

/**
	\test "Connectivity Policy" Test: primary gateway available.
			======================================================
	Verify that less than 5 seconds passes if just the primary gateway is available.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(gateway_available, exchg_t, exchg_t_types) {
	using connection_t=typename exchg_t::first_type;
	using simulator_t=typename exchg_t::second_type;
	using conn_pol_t=typename connection_t::conn_pol_t;
	typename simulator_t::proc_rules_t proc_rules;
	no_latency_timestamps ts(0);
	typename simulator_t::report_error_fn_t report_error=[] NEVER_INLINE (std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: "<<svr_details<<", exception information: "<<eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: "<<svr_details<<", unknown exception.");
		}
	};
	simulator_t svr(
		typename simulator_t::ctor_args{
			boost::asio::ip::address(),
			unused_primary_port,
			simulator_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority
		},
		proc_rules,
		report_error,
		ts,
		typename simulator_t::thread_t::thread_traits::thread_name_t{"sim" LIBJMMCG_ENQUOTE(__LINE__)}
	);
	typename conn_pol_t::gateways_t gateways(
		std::make_pair(localhost, unused_primary_port)
	);
	conn_pol_t conn_pol(gateways);
	auto const &begin=std::chrono::system_clock::now();
	BOOST_CHECK_NO_THROW(
		typename exchg_t::first_type link(
			conn_pol,
			exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::client_to_exchange_thread.core
		)
	);
	auto const &end=std::chrono::system_clock::now();
	BOOST_CHECK_LT(std::chrono::duration_cast<std::chrono::seconds>(end-begin).count(), conn_pol_t::min_timeout.count());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(admin)

/**
	\test "Heartbeats" Test: Client Heartbeat.
			====================================
	Verify that the response to a Heartbeat is nothing.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(client_heartbeat, exchg_t, exchg_t_types) {
	using connection_t=typename exchg_t::first_type;
	using simulator_t=typename exchg_t::second_type;
	using conn_pol_t=typename connection_t::conn_pol_t;
	typename simulator_t::proc_rules_t proc_rules;
	no_latency_timestamps ts(0);
	typename simulator_t::report_error_fn_t report_error=[] NEVER_INLINE (std::string svr_details, std::exception_ptr eptr) {
		try {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: "<<svr_details<<", exception information: "<<eptr);
		} catch(...) {
			BOOST_WARN_MESSAGE(false, "Failed whilst running the server. Server details: "<<svr_details<<", unknown exception.");
		}
	};
	simulator_t svr(
		typename simulator_t::ctor_args{
			boost::asio::ip::address(),
			unused_primary_port,
			simulator_t::socket_t::socket_priority::high,
			exchanges::common::thread_traits::exchange_simulator_thread.core,
			exchanges::common::thread_traits::exchange_simulator_thread.priority
		},
		proc_rules,
		report_error,
		ts,
		typename simulator_t::thread_t::thread_traits::thread_name_t{"sim" LIBJMMCG_ENQUOTE(__LINE__)}
	);
	typename conn_pol_t::gateways_t gateways(
		std::make_pair(localhost, unused_primary_port)
	);
	conn_pol_t conn_pol(gateways);
	typename exchg_t::first_type link(
		conn_pol,
		exchg_t::first_type::skt_mgr_t::socket_t::socket_priority::high,
		exchanges::common::thread_traits::client_to_exchange_thread.core
	);
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=005\00135=0\00110=000\001"
	};
	typename connection_t::msg_details_t::ClientHeartbeat_t const &msg=reinterpret_cast<typename connection_t::msg_details_t::ClientHeartbeat_t const &>(*fix_buffer.begin());
	BOOST_CHECK(msg.is_valid());
	BOOST_CHECK_NO_THROW(link.send(msg));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
