/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/order_book/order_book.hpp"

#include "../exchanges/FIX/v5.0sp2/fix.hpp"
#include "../exchanges/MIT/BIT/bit.hpp"
/* TODO  Not bothering to get the rest of these working until I need it..,
#include "../exchanges/MIT/JSE/jse.hpp"
#include "../exchanges/MIT/LSE/lse.hpp"
#include "../exchanges/MIT/OSLO/oslo.hpp"
#include "../exchanges/MIT/TRQ/trq.hpp"
#include "../exchanges/BATSBOE/EU/v1/batsboe.hpp"
#include "../exchanges/BATSBOE/EU/v2/batsboe.hpp"
#include "../exchanges/BATSBOE/US/v1/batsboe.hpp"
#include "../exchanges/BATSBOE/US/v2/batsboe.hpp"
*/

using namespace libjmmcg;
using namespace libisimud;

using ob_t_types=boost::mpl::list<
	order_book<exchanges::FIX::v5_0sp2::MsgTypes>/* TODO Not bothering to get the rest of these working until I need it..,
	order_book<exchanges::MIT::BIT::MsgTypes>,
	order_book<exchanges::MIT::JSE::MsgTypes>,
	order_book<exchanges::MIT::LSE::MsgTypes>,
	order_book<exchanges::MIT::OSLO::MsgTypes>,
	order_book<exchanges::MIT::TRQ::MsgTypes>,
	order_book<exchanges::BATSBOE::v1::EU::MsgTypes>,
	order_book<exchanges::BATSBOE::v2::EU::MsgTypes>,
	order_book<exchanges::BATSBOE::v1::US::MsgTypes>,
	order_book<exchanges::BATSBOE::v2::US::MsgTypes>*/
>;

BOOST_AUTO_TEST_SUITE(order_book_basic)

/**
	\test Ctor
	============================
	Verify it can be simply created.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, ob_t, ob_t_types) {
	BOOST_CHECK_NO_THROW(ob_t{});
}

/**
	\test Initially it has no orders.
	============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(initially_no_orders, ob_t, ob_t_types) {
	using symbol_t=typename ob_t::symbol_t;

	ob_t ob;
	BOOST_CHECK(ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 0);
	BOOST_CHECK(ob.match().empty());
}

/**
	\test Can print default ctor'd order_book.
	============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(stream_out_default_ctor, ob_t, ob_t_types) {
	ob_t ob;
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
}

/**
	\test Place an order - must not be empty.
	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_order_not_empty, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o(SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON);
	BOOST_CHECK_NO_THROW(ob.place(o));
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 0);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5\n------------\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place a sell & a buy order, uncrossed.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_a_sell_and_a_buy_orders, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s(SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON);
	BOOST_CHECK_NO_THROW(ob.place(o_s));
	typename ob_t::order_t o_b{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON};
	BOOST_CHECK_NO_THROW(ob.place(o_b));
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 20);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5\n------------\n90: 10\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place a sell then a buy order, then another sell, uncrossed.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_two_sells_and_a_buy_orders, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s1(SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON);
	BOOST_CHECK_NO_THROW(ob.place(o_s1));
	typename ob_t::order_t o_b{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON};
	BOOST_CHECK_NO_THROW(ob.place(o_b));
	typename ob_t::order_t o_s2{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON};
	BOOST_CHECK_NO_THROW(ob.place(o_s2));
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 20);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n------------\n90: 10\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, uncrossed.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 5);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n105: 3 7\n------------\n100: 4 6\n90: 10 2 3\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then an aggressive one to cross the exchange.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders_then_cross, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{9}, ClientOrderID_t{"9"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 0);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n105: 3 7\n------------\n105: 4\n100: 4 6\n90: 10 2 3\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then an aggressive buy one to cross the exchange, then match for any executions.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders_then_buy_crosses_then_match, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{9}, ClientOrderID_t{"9"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}	// Aggressive buy.
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK(ob.is_crossed());
	auto const &executions=ob.match();
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK(!executions.empty());
	BOOST_REQUIRE_EQUAL(executions.size(), 2);
	BOOST_CHECK_EQUAL(executions[0].limitPrice(), 105);
	BOOST_CHECK_EQUAL(executions[0].orderQty(), 3);
	BOOST_CHECK_EQUAL(executions[1].limitPrice(), 105);
	BOOST_CHECK_EQUAL(executions[1].orderQty(), 1);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n105: 6\n------------\n100: 4 6\n90: 10 2 3\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then an aggressive sell one to cross the exchange, then match for any executions.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders_then_sell_crosses_then_match, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}	// Aggressive sell.
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{9}, ClientOrderID_t{"9"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK(ob.is_crossed());
	auto const &executions=ob.match();
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK(!executions.empty());
	BOOST_REQUIRE_EQUAL(executions.size(), 2);
	BOOST_CHECK_EQUAL(executions[0].limitPrice(), 100);
	BOOST_CHECK_EQUAL(executions[0].orderQty(), 4);
	BOOST_CHECK_EQUAL(executions[1].limitPrice(), 100);
	BOOST_CHECK_EQUAL(executions[1].orderQty(), 3);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n105: 3 7\n------------\n100: 3\n90: 10 2 3\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then an aggressive buy one to cross the exchange, then match for any executions, removing some exactly.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders_then_buy_crosses_then_match_exact, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{9}, ClientOrderID_t{"9"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}	// Aggressive buy.
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK(ob.is_crossed());
	auto const &executions=ob.match();
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK(!executions.empty());
	BOOST_REQUIRE_EQUAL(executions.size(), 2);
	BOOST_CHECK_EQUAL(executions[0].limitPrice(), 105);
	BOOST_CHECK_EQUAL(executions[0].orderQty(), 3);
	BOOST_CHECK_EQUAL(executions[1].limitPrice(), 105);
	BOOST_CHECK_EQUAL(executions[1].orderQty(), 7);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n------------\n100: 4 6\n90: 10 2 3\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then an aggressive sell one to cross the exchange, then match for any executions, removing some exactly.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders_then_sell_crosses_then_match_exact, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}	// Aggressive sell.
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{9}, ClientOrderID_t{"9"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK(ob.is_crossed());
	auto const &executions=ob.match();
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK(!executions.empty());
	BOOST_REQUIRE_EQUAL(executions.size(), 2);
	BOOST_CHECK_EQUAL(executions[0].limitPrice(), 100);
	BOOST_CHECK_EQUAL(executions[0].orderQty(), 4);
	BOOST_CHECK_EQUAL(executions[1].limitPrice(), 100);
	BOOST_CHECK_EQUAL(executions[1].orderQty(), 6);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n105: 3 7\n------------\n90: 10 2 3\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then an aggressive buy then an aggressive sell, both crossing the exchange, then match for any executions.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders_then_buy_and_sell_crosses_then_match, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{9}, ClientOrderID_t{"9"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}	// Aggressive buy.
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK_NO_THROW(ob.match());
	typename ob_t::order_t aggressive_sell{SeqNum_t{10}, ClientOrderID_t{"10"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{23}, Price_t{80}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON};
	ob.place(aggressive_sell);
	BOOST_CHECK(ob.is_crossed());
	auto const &executions=ob.match();
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK(!executions.empty());
	BOOST_REQUIRE_EQUAL(executions.size(), 5);
	BOOST_CHECK_EQUAL(executions[0].limitPrice(), 100);
	BOOST_CHECK_EQUAL(executions[0].orderQty(), 4);
	BOOST_CHECK_EQUAL(executions[1].limitPrice(), 100);
	BOOST_CHECK_EQUAL(executions[1].orderQty(), 6);
	BOOST_CHECK_EQUAL(executions[2].limitPrice(), 90);
	BOOST_CHECK_EQUAL(executions[2].orderQty(), 10);
	BOOST_CHECK_EQUAL(executions[3].limitPrice(), 90);
	BOOST_CHECK_EQUAL(executions[3].orderQty(), 2);
	BOOST_CHECK_EQUAL(executions[4].limitPrice(), 90);
	BOOST_CHECK_EQUAL(executions[4].orderQty(), 1);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n105: 6\n------------\n90: 2\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then an aggressive buy then an aggressive sell then an aggressive buy, all crossing the exchange, then match for any executions.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_more_orders_then_buy_sell_and_buy_crosses_then_match, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{7}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{6}, ClientOrderID_t{"6"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{7}, ClientOrderID_t{"7"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{8}, ClientOrderID_t{"8"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{9}, ClientOrderID_t{"9"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}	// Aggressive buy.
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK_NO_THROW(ob.match());
	typename ob_t::order_t aggressive_sell{SeqNum_t{10}, ClientOrderID_t{"10"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{23}, Price_t{80}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON};
	ob.place(aggressive_sell);
	BOOST_CHECK_NO_THROW(ob.match());
	typename ob_t::order_t aggressive_buy{SeqNum_t{11}, ClientOrderID_t{"11"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{8}, Price_t{107}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON};
	ob.place(aggressive_buy);
	BOOST_CHECK(ob.is_crossed());
	auto const &executions=ob.match();
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK(!executions.empty());
	BOOST_REQUIRE_EQUAL(executions.size(), 1);
	BOOST_CHECK_EQUAL(executions[0].limitPrice(), 105);
	BOOST_CHECK_EQUAL(executions[0].orderQty(), 6);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
	char const output[]="=================\nASK\n110: 5 10\n------------\n107: 2\n90: 2\nBID\n=================\n";
	BOOST_CHECK_EQUAL(ss.str(), output);
}

/**
	\test Place more orders, then cancel one of them.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_orders_then_cancel_one, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{5}, ClientOrderID_t{"5"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 5);
	BOOST_CHECK(ob.match().empty());
	BOOST_CHECK_NO_THROW(ob.cancel(o_s[1]));
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 10);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
}

/**
	\test Place more orders, then cancel one of them then place an aggressive buy and match.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(place_orders_then_cancel_one_buy_match, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK_NO_THROW(ob.cancel(o_s[1]));
	typename ob_t::order_t aggressive_sell{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{23}, Price_t{80}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON};
	ob.place(aggressive_sell);
	auto const &executions=ob.match();
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 0);
	BOOST_CHECK(ob.match().empty());
	BOOST_REQUIRE_EQUAL(executions.size(), 2);
	BOOST_CHECK_EQUAL(executions[0].limitPrice(), 100);
	BOOST_CHECK_EQUAL(executions[0].orderQty(), 4);
	BOOST_CHECK_EQUAL(executions[1].limitPrice(), 90);
	BOOST_CHECK_EQUAL(executions[1].orderQty(), 10);
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
}

/**
	\test Place more orders, then cancel an unknown symbol then place an aggressive buy and match.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(cancel_unknown_symbol, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK_NO_THROW(ob.cancel(typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"ABC"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}));
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 5);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
}

/**
	\test Place more orders, then cancel one of them then readd it.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(cancel_read_symbol, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o_s[]={
		typename ob_t::order_t{SeqNum_t{0}, ClientOrderID_t{"0"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{110}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{2}, ClientOrderID_t{"2"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{6}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	typename ob_t::order_t o_b[]={
		typename ob_t::order_t{SeqNum_t{3}, ClientOrderID_t{"3"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{90}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{4}, ClientOrderID_t{"4"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{4}, Price_t{100}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o_s) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	for (auto const &o : o_b) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK_NO_THROW(ob.cancel(typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}));
	BOOST_CHECK_NO_THROW(ob.place(typename ob_t::order_t{SeqNum_t{1}, ClientOrderID_t{"1"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{105}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}));
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 5);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
}

/**
	\test Place some orders, then cancel some of them then check the executions.
*	=============================
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(process_market_data_example, ob_t, ob_t_types) {
	using SeqNum_t=typename ob_t::msg_details_t::SeqNum_t;
	using OrderType=typename ob_t::msg_details_t::OrderType;
	using TIF=typename ob_t::msg_details_t::TIF;
	using side=typename ob_t::side_t;
	using symbol_t=typename ob_t::symbol_t;
	using ClientOrderID_t=typename ob_t::msg_details_t::ClientOrderID_t;
	using Price_t=typename ob_t::msg_details_t::Price_t;
	using Quantity_t=typename ob_t::msg_details_t::Quantity_t;

	ob_t ob;
	typename ob_t::order_t o1[]={
		typename ob_t::order_t{SeqNum_t{100000}, ClientOrderID_t{"100000"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{1}, Price_t{1075}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{100001}, ClientOrderID_t{"100001"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{9}, Price_t{1000}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{100002}, ClientOrderID_t{"100002"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{30}, Price_t{975}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{100003}, ClientOrderID_t{"100003"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{1050}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{100004}, ClientOrderID_t{"100004"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{950}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{100005}, ClientOrderID_t{"100005"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{1025}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{100006}, ClientOrderID_t{"100006"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{1}, Price_t{1000}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o1) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK_NO_THROW(ob.cancel(typename ob_t::order_t{SeqNum_t{100004}, ClientOrderID_t{"100004"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{10}, Price_t{950}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}));
	typename ob_t::order_t o2[]={
		typename ob_t::order_t{SeqNum_t{100007}, ClientOrderID_t{"100007"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{5}, Price_t{1025}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON},
		typename ob_t::order_t{SeqNum_t{100008}, ClientOrderID_t{"100008"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{1050}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}
	};
	for (auto const &o : o2) {
		BOOST_CHECK_NO_THROW(ob.place(o));
	}
	BOOST_CHECK_NO_THROW(ob.cancel(typename ob_t::order_t{SeqNum_t{100008}, ClientOrderID_t{"100008"}, OrderType::Limit, TIF::Day, side::Buy, symbol_t{"XYZ"}, Quantity_t{3}, Price_t{1050}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}));
	BOOST_CHECK_NO_THROW(ob.cancel(typename ob_t::order_t{SeqNum_t{100005}, ClientOrderID_t{"100005"}, OrderType::Limit, TIF::Day, side::Sell, symbol_t{"XYZ"}, Quantity_t{2}, Price_t{102}, exchanges::common::mic_codes::ISO_10383_MIC_Codes::ISO_10383_LONDON_STOCK_EXCHANGE_XLON}));
	BOOST_CHECK(!ob.empty());
	BOOST_CHECK(!ob.is_crossed());
	BOOST_CHECK_EQUAL(ob.agreed_spread(symbol_t{"XYZ"}), 75);
	BOOST_CHECK(ob.match().empty());
	std::stringstream ss;
	ss<<ob;
	BOOST_CHECK(!ss.str().empty());
}

BOOST_AUTO_TEST_SUITE_END()
