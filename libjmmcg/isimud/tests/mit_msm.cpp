/******************************************************************************
** Copyright © 2018 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include LIBJMMCG_PERFECT_HASH_MASK_FOR_MIT_MSMS_HDR

#include "../exchanges/FIX/v5.0sp2/fix.hpp"
#include "../exchanges/MIT/BIT/messages.hpp"
/* TODO #include "../exchanges/MIT/JSE/messages.hpp"
#include "../exchanges/MIT/LSE/messages.hpp"
#include "../exchanges/MIT/OSLO/messages.hpp"
#include "../exchanges/MIT/TRQ/messages.hpp"
*/
#include "core/msm.hpp"

#include <optional>

using namespace libjmmcg;
using namespace libisimud;

using msg_types=boost::mpl::list<
	exchanges::MIT::BIT::MsgTypes/* TODO,
	exchanges::MIT::JSE::MsgTypes,
	exchanges::MIT::LSE::MsgTypes,
	exchanges::MIT::OSLO::MsgTypes,
	exchanges::MIT::TRQ::MsgTypes*/
>;

struct dummy {};

template<class DestMsgsT>
struct data_with_unsigned {
	using dest_msg_details_t=DestMsgsT;
	using end_states=typename dest_msg_details_t::MsgTypes_t;

	end_states state{};
	unsigned j{68};

	template<class ArgT1, class ArgT2>
	NEVER_INLINE void process(ArgT1 i, ArgT2) {
		j+=static_cast<unsigned>(i);
	}
};

template<class DestMsgsT>
struct data_with_secid {
	using dest_msg_details_t=DestMsgsT;
	using end_states=typename dest_msg_details_t::MsgTypes_t;

	end_states state{};
	std::optional<exchanges::MIT::common::ref_data::key_type> isin{};

	template<class RefData, class ArgT2>
	NEVER_INLINE void process(RefData const &rd, ArgT2) {
		exchanges::MIT::common::ref_data::key_type isin_found=rd.at(133215);
		BOOST_CHECK_EQUAL(isin_found.to_string(), "GB00BH4HKS39");
		isin=std::move(isin_found);
		BOOST_CHECK(isin.has_value());
	}
};

template<
	class SrcMsgsT,
	class DestMsgsT,
	template<class> class MSMType,
	template<class, class> class MSMRowsType,
	template<class> class MachineT,
	class ArgT1,
	class ArgT2,
	class RefData
>
struct assign_msm_states {
	using src_msg_details_t=SrcMsgsT;
	using dest_msg_details_t=DestMsgsT;
	template<class StateMachineT> using msm_t=MSMType<StateMachineT>;
	struct fn_event {
		using arguments_types=std::pair<
			typename dest_msg_details_t::MsgTypes_t,
			std::tuple<
				std::tuple<ArgT1 &, ArgT2 &>
			>
		>;
		using arguments_type=typename std::tuple_element<0, typename arguments_types::second_type>::type;
		using argument_type=typename std::tuple_element<0, arguments_type>::type;
		using second_argument_type=typename std::tuple_element<1, arguments_type>::type;
		using end_states=typename dest_msg_details_t::MsgTypes_t;

		explicit fn_event(RefData const &j)
		: j_(j) {}

		template<auto state, auto next>
		NEVER_INLINE end_states process(argument_type &p1, second_argument_type &p2) const noexcept(true) {
			p1.state=next;
			p1.process(j_, p2);
			return next;
		}

	private:
		RefData const &j_;
	};

	struct state_machine_t final : msm_t<state_machine_t> {
		using base_t=msm_t<state_machine_t>;
		using row_t=MSMRowsType<typename src_msg_details_t::MsgTypes_t, typename dest_msg_details_t::MsgTypes_t>;
		/**
		 * \see common::client_to_exchange_transformations
		 */
		using transition_table=typename base_t::template rows<
			typename row_t::template row<
				src_msg_details_t::NewOrder_t::static_type,
				fn_event,
				dest_msg_details_t::NewOrder_t::static_type
			>,
			typename row_t::template row<
				src_msg_details_t::OrderCancelRequest_t::static_type,
				fn_event,
				dest_msg_details_t::OrderCancelRequest_t::static_type
			>,
			typename row_t::template row<
				src_msg_details_t::OrderCancelReplace_t::static_type,
				fn_event,
				dest_msg_details_t::OrderCancelReplaceRequest_t::static_type
			>,
			typename row_t::template row<
				src_msg_details_t::MsgTypes_t::MatchAll,
				fn_event,
				dest_msg_details_t::Reject_t::static_type
			>
		>;
	};
	using machine=MachineT<state_machine_t>;

	explicit assign_msm_states(RefData const &d)
	: j(d), msm(j, j, j, j) {
	}

	void process(typename src_msg_details_t::MsgTypes_t state, ArgT1 &p1, ArgT2 &p2) noexcept(false) {
		msm.process(state, p1, p2);
	}

	RefData const &j;
	machine msm;
};

BOOST_AUTO_TEST_SUITE(isimud)

BOOST_AUTO_TEST_SUITE(msm_tests)

BOOST_AUTO_TEST_SUITE(unroll)

BOOST_AUTO_TEST_CASE_TEMPLATE(start_state_assign_msm_states_double, msg, msg_types) {
	using assign_msm_states_t=assign_msm_states<
		exchanges::FIX::v5_0sp2::MsgTypes,
		msg,
		msm::unroll::state_transition_table,
		msm::unroll::row_types,
		msm::unroll::machine,
		data_with_unsigned<msg>,
		dummy,
		double
	>;
	dummy l;
	double dbl=42;
	assign_msm_states_t msm(dbl);
	data_with_unsigned<msg> d;
	volatile auto state=exchanges::FIX::v5_0sp2::MsgTypes::NewOrder_t::static_type;
	BOOST_CHECK_NO_THROW(msm.process(state, d, l));
	BOOST_CHECK_EQUAL(d.state, assign_msm_states_t::dest_msg_details_t::NewOrder_t::static_type);
	BOOST_CHECK_EQUAL(d.j, 68+42);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(start_state_assign_msm_states_refdata, msg, msg_types) {
	using assign_msm_states_t=assign_msm_states<
		exchanges::FIX::v5_0sp2::MsgTypes,
		msg,
		msm::unroll::state_transition_table,
		msm::unroll::row_types,
		msm::unroll::machine,
		data_with_secid<msg>,
		dummy,
		exchanges::MIT::common::ref_data
	>;
	const exchanges::MIT::common::ref_data rd(
		[]() {
			const std::string ref_data_file(
				"133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;\n"
				"2926;FTSE100;SET1;PT_T;TP_12;GB0000595859;;20000419;0;1;3000;32438040;1;;1;DE;ARM;ARM HLDGS.;0059585;3861344694;GBX;1;Y;0023;ARARM;ARM HOLDINGS PLC;0;;;7500;ORD 0.05P;;1;1;5;GB;;;FS10;4;;;;2;B;;;;;;"
			);
			std::stringstream ss;
			ss<<ref_data_file;
			return exchanges::MIT::common::ref_data(ss);
		}()
	);
	assign_msm_states_t msm(rd);
	data_with_secid<msg> d;
	dummy l;
	volatile auto state=exchanges::FIX::v5_0sp2::MsgTypes::NewOrder_t::static_type;
	BOOST_CHECK_NO_THROW(msm.process(state, d, l));
	BOOST_CHECK_EQUAL(d.state, assign_msm_states_t::dest_msg_details_t::NewOrder_t::static_type);
	BOOST_CHECK_EQUAL(d.isin->to_string(), "GB00BH4HKS39");
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(hash)

BOOST_AUTO_TEST_CASE_TEMPLATE(start_state_assign_msm_states, msg, msg_types) {
	using assign_msm_states_t=assign_msm_states<
		exchanges::FIX::v5_0sp2::MsgTypes,
		msg,
		msm::hash::state_transition_table,
		msm::hash::row_types,
		msm::hash::machine,
		data_with_unsigned<msg>,
		dummy,
		double
	>;
	dummy l;
	double dbl=42;
	assign_msm_states_t msm(dbl);
	data_with_unsigned<msg> d;
	volatile auto state=exchanges::FIX::v5_0sp2::MsgTypes::NewOrder_t::static_type;
	BOOST_CHECK_NO_THROW(msm.process(state, d, l));
	BOOST_CHECK_EQUAL(d.state, assign_msm_states_t::dest_msg_details_t::NewOrder_t::static_type);
	BOOST_CHECK_EQUAL(d.j, 68+42);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
