/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/conversions/fix_to_mit_conversions.hpp"

using namespace libjmmcg;
using namespace libisimud;

typedef boost::mpl::list<
	std::pair<exchanges::MIT::BIT::MsgTypes, exchanges::MIT::BIT::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::JSE::MsgTypes, exchanges::MIT::JSE::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::LSE::MsgTypes, exchanges::MIT::LSE::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::OSLO::MsgTypes, exchanges::MIT::OSLO::MsgTypes::ref_data>,
	std::pair<exchanges::MIT::TRQ::MsgTypes, exchanges::MIT::TRQ::MsgTypes::ref_data>
> msg_types;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_SUITE(admin)

BOOST_AUTO_TEST_CASE_TEMPLATE(LogonRequest, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=099\00135=A\00149=SENDER\00156=TARGET\00134=1\00152=20000426-12:05:06\00198=0\001108=30\001553=USER\001554=PASSWORD\001925=NEWPASSWD\00110=238\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::LogonRequest_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::LogonRequest_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	typename msg::first_type::LogonRequest_t exchg_msg(fix_msg);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, 2);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.userName.begin()), "USER");
	BOOST_CHECK_EQUAL(std::string(exchg_msg.password.begin()), "PASSWORD");
	BOOST_CHECK_EQUAL(std::string(exchg_msg.newPassword.begin()), "NEWPASSWD");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Logout, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=005\00135=5\00110=005\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::LogoutRequest_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::LogoutRequest_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	typename msg::first_type::LogoutRequest_t exchg_msg(fix_msg, exchanges::MIT::common::Reason_t{'U', 's', 'e', 'r', ' ', 'l', 'o', 'g', 'o', 'u', 't', ' ', 'r', 'e', 'c', 'e', 'i', 'v', 'e', 'd'});
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ClientHeartbeat, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=005\00135=0\00110=000\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	typename msg::first_type::ClientHeartbeat_t exchg_msg(fix_msg);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, 2);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(conversions)

BOOST_AUTO_TEST_CASE_TEMPLATE(NewOrderSingle, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=170\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=69\00148=GB0000595859\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=MTAA\00110=120\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	BOOST_CHECK_EQUAL(exchanges::MIT::common::convert<exchanges::MIT::common::OrderType>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrdType>()), exchanges::MIT::common::OrderType::Limit);
	BOOST_CHECK_EQUAL(exchanges::MIT::common::convert<exchanges::MIT::common::TIF>(fix_msg.find<exchanges::FIX::common::FieldsFast::TimeInForce>()), exchanges::MIT::common::TIF::Day);
	BOOST_CHECK_EQUAL(exchanges::MIT::common::convert<exchanges::MIT::common::Side>(fix_msg.find<exchanges::FIX::common::FieldsFast::Side>()), exchanges::MIT::common::Side::Buy);
	BOOST_CHECK_EQUAL(tostring(exchanges::FIX::common::convert<exchanges::MIT::common::SecurityID_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Symbol>())), "69");
	BOOST_CHECK_EQUAL(exchanges::FIX::common::convert<std::int32_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>()), 10000);
	BOOST_CHECK_CLOSE(exchanges::MIT::common::convert<exchanges::MIT::common::Price_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Price>()), exchanges::MIT::common::implied_decimal_places*76.75, 0.01);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(trade)

BOOST_AUTO_TEST_CASE_TEMPLATE(NewOrderSingle, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=170\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=EK\00148=GB00BH4HKS39\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=MTAA\00110=000\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	const std::string ref_data_file("133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;");
	std::stringstream ss;
	ss<<ref_data_file;
	const typename msg::second_type ref_data(ss);
	typename msg::first_type::NewOrder_t exchg_msg(fix_msg, ref_data);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, 2);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.clientOrderID().begin()), "10");
	BOOST_CHECK_EQUAL(exchg_msg.orderQty(), exchanges::FIX::common::convert<int32_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>()));
	BOOST_CHECK_EQUAL(exchg_msg.orderType(), exchanges::MIT::common::convert<exchanges::MIT::common::OrderType>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrdType>()));
	BOOST_CHECK_EQUAL(exchg_msg.side(), exchanges::MIT::common::convert<exchanges::MIT::common::Side>(fix_msg.find<exchanges::FIX::common::FieldsFast::Side>()));
	BOOST_CHECK_EQUAL(exchg_msg.instrumentID(), 133215);
	BOOST_CHECK_EQUAL(exchg_msg.limitPrice(), exchanges::MIT::common::convert<exchanges::MIT::common::Price_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Price>()));
	BOOST_CHECK_EQUAL(exchg_msg.tif(), exchanges::MIT::common::convert<exchanges::MIT::common::TIF>(fix_msg.find<exchanges::FIX::common::FieldsFast::TimeInForce>()));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderCancelRequest, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=095\00135=F\00111=10\00141=10\00148=GB00BH4HKS39\00155=CUSTOME\00154=1\00160=19980930-09:25:58\00138=10000\0011133=G\001100=MTAA\00110=105\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	const std::string ref_data_file("133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;");
	std::stringstream ss;
	ss<<ref_data_file;
	const typename msg::second_type ref_data(ss);
	typename msg::first_type::OrderCancelRequest_t exchg_msg(fix_msg, ref_data);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, 2);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.originalClientOrderID().begin()), "10");
	BOOST_CHECK_EQUAL(exchg_msg.side(), exchanges::MIT::common::convert<exchanges::MIT::common::Side>(fix_msg.find<exchanges::FIX::common::FieldsFast::Side>()));
	BOOST_CHECK_EQUAL(exchg_msg.instrumentID(), 133215);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderCancelReplace, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=162\00135=G\00111=10\00141=10\00148=GB00BH4HKS39\00122=4\00149=VENDOR\00156=BROKER\00134=10\00152=20000426-12:05:06\00155=EK\00154=1\00160=19980930-09:25:58\00138=99\00140=1\00144=43.000000\00159=0\0011133=G\001100=MTAA\00110=037\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	const std::string ref_data_file("133215;FTSE100;SET0;PT_T;TP_1;GB00BH4HKS39;;20060731;0;1;10000;42467000;1;;1;DE;VOD;VODAFONE GRP.;BH4HKS3;15225662730;GBX;1;Y;0023;VOVOD;VODAFONE GROUP PLC;0;;;15000;ORD USD0.20 20/21;;1;1;5;GB;;;FE00;1;;;;1;A;;;;;;");
	std::stringstream ss;
	ss<<ref_data_file;
	const typename msg::second_type ref_data(ss);
	typename msg::first_type::OrderCancelReplaceRequest_t exchg_msg(fix_msg, ref_data);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, 2);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.originalClientOrderID().begin()), "10");
	BOOST_CHECK_EQUAL(exchg_msg.orderQty(), exchanges::FIX::common::convert<int32_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>()));
	BOOST_CHECK_EQUAL(exchg_msg.side(), exchanges::MIT::common::convert<exchanges::MIT::common::Side>(fix_msg.find<exchanges::FIX::common::FieldsFast::Side>()));
	BOOST_CHECK_EQUAL(exchg_msg.instrumentID(), 133215);
	BOOST_CHECK_EQUAL(exchg_msg.limitPrice(), exchanges::MIT::common::convert<exchanges::MIT::common::Price_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Price>()));
	BOOST_CHECK_EQUAL(exchg_msg.tif(), exchanges::MIT::common::convert<exchanges::MIT::common::TIF>(fix_msg.find<exchanges::FIX::common::FieldsFast::TimeInForce>()));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
