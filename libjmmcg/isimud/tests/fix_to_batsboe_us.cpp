/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/conversions/fix_to_batsboe_us_conversions.hpp"

using namespace libisimud;

const exchanges::BATSBOE::common::SeqNum_t sequenceNumber(1);

typedef boost::mpl::list<
	exchanges::BATSBOE::US::v1::MsgTypes//,
// TODO	exchanges::BATSBOE::US::v2::MsgTypes
> msg_types;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_SUITE(admin)

BOOST_AUTO_TEST_CASE_TEMPLATE(LogonRequest, msg, msg_types)
{
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=099\00135=A\00149=SENDER\00156=TARGET\00134=1\00152=20000426-12:05:06\00198=0\001108=30\001553=USER\001554=PASSWORD\001925=NEWPASSWD\00110=255\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::LogonRequest_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::LogonRequest_t const &>(*fix_buffer.begin());
	typename msg::LogonRequest_t exchg_msg(fix_msg, sequenceNumber, exchanges::BATSBOE::common::SessionSubID_t(), false);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.userName.begin(), exchg_msg.userName.size()), "USER");
	BOOST_CHECK_EQUAL(std::string(exchg_msg.password.begin()), "PASSWORD");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(LogoutRequest, msg, msg_types)
{
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=005\00135=5\00110=092\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::LogoutRequest_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::LogoutRequest_t const &>(*fix_buffer.begin());
	typename msg::LogoutRequest_t exchg_msg(fix_msg, sequenceNumber);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, exchanges::BATSBOE::common::msg_start_code);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ClientHeartbeat, msg, msg_types)
{
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=005\00135=0\00110=092\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::ClientHeartbeat_t const &>(*fix_buffer.begin());
	typename msg::ClientHeartbeat_t exchg_msg(fix_msg, sequenceNumber);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, exchanges::BATSBOE::common::msg_start_code);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ServerHeartbeat, msg, msg_types)
{
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=005\00135=0\00110=092\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::ServerHeartbeat_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::ServerHeartbeat_t const &>(*fix_buffer.begin());
	typename msg::ServerHeartbeat_t exchg_msg(fix_msg, sequenceNumber);
	BOOST_CHECK_EQUAL(exchg_msg.start_of_message, exchanges::BATSBOE::common::msg_start_code);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(conversions)

BOOST_AUTO_TEST_CASE_TEMPLATE(NewOrderSingle, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=167\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=69\00148=GB0000595859\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=BATE\00110=147\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &>(*fix_buffer.begin());
	BOOST_CHECK_EQUAL(exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::OrdType>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrdType>()), exchanges::BATSBOE::common::OrdType::Limit);
	BOOST_CHECK_EQUAL(exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::TIF>(fix_msg.find<exchanges::FIX::common::FieldsFast::TimeInForce>()), exchanges::BATSBOE::common::TIF::Day);
	BOOST_CHECK_EQUAL(exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::Side>(fix_msg.find<exchanges::FIX::common::FieldsFast::Side>()), exchanges::BATSBOE::common::Side::Buy);
	BOOST_CHECK_EQUAL(std::string(exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::SecurityID_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Symbol>()).data()), "69");
	BOOST_CHECK_EQUAL(exchanges::BATSBOE::common::convert<uint32_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>()), 10000*exchanges::BATSBOE::common::implied_decimal_places);
	BOOST_CHECK_CLOSE(exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::Price_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Price>()), exchanges::BATSBOE::common::implied_decimal_places*76.75, 0.01);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(trade)

BOOST_AUTO_TEST_CASE_TEMPLATE(NewOrderSingle, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=167\00135=D\00134=10\00143=N\00149=VENDOR\00150=CUSTOME\00156=BROKER\00160=19980930-09:25:58\0011=XQCCFUND\00111=10\00121=1\00155=69\00148=GB0000595859\00122=4\00154=1\00138=10000\00140=2\00144=76.750000\00159=0\0011133=G\001100=BATE\00110=147\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::NewOrderSingle_t const &>(*fix_buffer.begin());
	typename msg::NewOrder_t exchg_msg(fix_msg, sequenceNumber);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.clientOrderID().begin()), "10");
	BOOST_CHECK_EQUAL(exchg_msg.orderQty()*exchanges::BATSBOE::common::implied_decimal_places, exchanges::BATSBOE::common::convert<uint32_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>()));
	BOOST_CHECK_EQUAL(exchg_msg.orderType(), exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::OrdType>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrdType>()));
	BOOST_CHECK_EQUAL(exchg_msg.side(), exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::Side>(fix_msg.find<exchanges::FIX::common::FieldsFast::Side>()));
	BOOST_CHECK_EQUAL(exchg_msg.instrumentID(), exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::SecurityID_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::SecurityID>()));
	BOOST_CHECK_EQUAL(exchg_msg.limitPrice(), exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::Price_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Price>()));
	BOOST_CHECK_EQUAL(exchg_msg.tif(), exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::TIF>(fix_msg.find<exchanges::FIX::common::FieldsFast::TimeInForce>()));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderCancelRequest, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=157\00135=F\00111=10\00141=10\00148=GB00BH4HKS39\00149=VENDOR\00156=BROKER\00134=10\00152=20000426-12:05:06\00155=EK\00154=1\00160=19980930-09:25:58\00138=99\00140=2\00144=43.000000\00159=0\0011133=G\001100=BATE\00110=076\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelRequest_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	typename msg::CancelOrder_t exchg_msg(fix_msg, sequenceNumber);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.originalClientOrderID().begin()), "10");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(OrderCancelReplace, msg, msg_types) {
	ALIGN_TO_L1_CACHE const exchanges::FIX::common::underlying_fix_data_buffer fix_buffer={
		"8=FIX.5.0\0019=162\00135=G\00111=10\00141=10\00148=GB00BH4HKS39\00122=4\00149=VENDOR\00156=BROKER\00134=10\00152=20000426-12:05:06\00155=EK\00154=1\00160=19980930-09:25:58\00138=99\00140=1\00144=43.000000\00159=0\0011133=G\001100=BATE\00110=030\001"
	};
	exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &fix_msg=reinterpret_cast<exchanges::FIX::v5_0sp2::MsgTypes::OrderCancelReplace_t const &>(*fix_buffer.begin());
	BOOST_CHECK(fix_msg.is_valid());
	typename msg::OrderCancelReplaceRequest_t exchg_msg(fix_msg, sequenceNumber);
	BOOST_CHECK_EQUAL(std::string(exchg_msg.originalClientOrderID().begin()), "10");
	BOOST_CHECK_EQUAL(exchg_msg.orderQty()*exchanges::BATSBOE::common::implied_decimal_places, exchanges::BATSBOE::common::convert<uint32_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::OrderQty>()));
	BOOST_CHECK_EQUAL(exchg_msg.side(), exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::Side>(fix_msg.find<exchanges::FIX::common::FieldsFast::Side>()));
	BOOST_CHECK_EQUAL(exchg_msg.limitPrice(), exchanges::BATSBOE::common::convert<exchanges::BATSBOE::common::Price_t>(fix_msg.find<exchanges::FIX::common::FieldsFast::Price>()));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
