/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, isimud@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE isimud_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "../exchanges/BATSBOE/EU/v1/messages.hpp"
#include "../exchanges/BATSBOE/EU/v2/messages.hpp"

#include "core/ttypes.hpp"

using namespace libisimud;

const exchanges::BATSBOE::common::ClientOrderID_t clientOrderID{"ABCDEFGHIJKLMNOPQRS"};
const exchanges::BATSBOE::common::SecurityID_t instID{"1234567890abcde"};
const exchanges::BATSBOE::common::Symbol_t symbol{"1234567"};
const exchanges::BATSBOE::common::Price_t price(1);
const exchanges::BATSBOE::common::SessionSubID_t sessionSubID{"321"};
const exchanges::BATSBOE::common::SeqNum_t sequenceNumber(1);

typedef boost::mpl::list<
	exchanges::BATSBOE::EU::v1::MsgTypes//,
// TODO	exchanges::BATSBOE::EU::v2::MsgTypes
> msg_types;

BOOST_AUTO_TEST_SUITE(exchange_gateways)

BOOST_AUTO_TEST_CASE_TEMPLATE(print_details, msg, msg_types) {
	std::stringstream ss;
	BOOST_CHECK_NO_THROW(msg::to_stream(ss));
	BOOST_CHECK(!ss.str().empty());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ctor, msg, msg_types)
{
	typename msg::LogonRequest_t logon(sequenceNumber, sessionSubID, exchanges::BATSBOE::common::UserName_t{"use"}, exchanges::BATSBOE::common::Password_t{"fubarsnfu"}, false);
	BOOST_CHECK_EQUAL(logon.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(logon.type(), exchanges::BATSBOE::EU::MsgType::LogonRequest);
	BOOST_CHECK_EQUAL(logon.matchingUnit, 0);
	BOOST_CHECK_EQUAL(logon.sequenceNumber, 1);
	BOOST_CHECK_GE(logon.length(), msg::header_t_size);
	BOOST_CHECK_LE(logon.length(), sizeof(typename msg::LogonRequest_t));
	typename msg::LogoutRequest_t logoutRequest(sequenceNumber);
	BOOST_CHECK_EQUAL(logoutRequest.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(logoutRequest.type(), exchanges::BATSBOE::EU::MsgType::LogoutRequest);
	BOOST_CHECK_EQUAL(logoutRequest.matchingUnit, 0);
	BOOST_CHECK_EQUAL(logoutRequest.sequenceNumber, 1);
	BOOST_CHECK_GE(logoutRequest.length(), msg::header_t_size);
	BOOST_CHECK_LE(logoutRequest.length(), sizeof(typename msg::LogoutRequest_t));
	typename msg::ClientHeartbeat_t clientHeartbeat(sequenceNumber);
	BOOST_CHECK_EQUAL(clientHeartbeat.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(clientHeartbeat.type(), exchanges::BATSBOE::EU::MsgType::ClientHeartbeat);
	BOOST_CHECK_EQUAL(clientHeartbeat.matchingUnit, 0);
	BOOST_CHECK_EQUAL(clientHeartbeat.sequenceNumber, 1);
	BOOST_CHECK_GE(clientHeartbeat.length(), msg::header_t_size);
	BOOST_CHECK_LE(clientHeartbeat.length(), sizeof(typename msg::ClientHeartbeat_t));
	typename msg::LogonReply_t logonReply(sequenceNumber);
	BOOST_CHECK_EQUAL(logonReply.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(logonReply.type(), exchanges::BATSBOE::EU::MsgType::LogonReply);
	BOOST_CHECK_EQUAL(logonReply.matchingUnit, 0);
	BOOST_CHECK_EQUAL(logonReply.sequenceNumber, 1);
	BOOST_CHECK_GE(logonReply.length(), msg::header_t_size);
	BOOST_CHECK_LE(logonReply.length(), sizeof(typename msg::LogonReply_t));
	typename msg::Logout_t logout(sequenceNumber, exchanges::BATSBOE::common::LogoutReason::UserRequested);
	BOOST_CHECK_EQUAL(logout.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(logout.type(), exchanges::BATSBOE::EU::MsgType::Logout);
	BOOST_CHECK_EQUAL(logout.matchingUnit, 0);
	BOOST_CHECK_EQUAL(logout.sequenceNumber, 1);
	BOOST_CHECK_EQUAL(logout.numberOfUnits, 0);
	BOOST_CHECK_GE(logout.length(), msg::header_t_size);
	BOOST_CHECK_LE(logout.length(), sizeof(typename msg::Logout_t));
	typename msg::ServerHeartbeat_t serverHeartbeat(sequenceNumber);
	BOOST_CHECK_EQUAL(serverHeartbeat.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(serverHeartbeat.type(), exchanges::BATSBOE::EU::MsgType::ServerHeartbeat);
	BOOST_CHECK_EQUAL(serverHeartbeat.matchingUnit, 0);
	BOOST_CHECK_EQUAL(serverHeartbeat.sequenceNumber, 1);
	BOOST_CHECK_GE(serverHeartbeat.length(), msg::header_t_size);
	BOOST_CHECK_LE(serverHeartbeat.length(), sizeof(typename msg::ServerHeartbeat_t));
	typename msg::ReplayComplete_t replayComplete(sequenceNumber);
	BOOST_CHECK_EQUAL(replayComplete.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(replayComplete.type(), exchanges::BATSBOE::EU::MsgType::ReplayComplete);
	BOOST_CHECK_EQUAL(replayComplete.matchingUnit, 0);
	BOOST_CHECK_EQUAL(replayComplete.sequenceNumber, 1);
	BOOST_CHECK_GE(replayComplete.length(), msg::header_t_size);
	BOOST_CHECK_LE(replayComplete.length(), sizeof(typename msg::ReplayComplete_t));
	typename msg::NewOrder_t newOrder(sequenceNumber, clientOrderID, exchanges::BATSBOE::common::OrdType::Market, exchanges::BATSBOE::common::TIF::Day, exchanges::BATSBOE::common::Side::Buy, symbol, instID, 0, price);
	BOOST_CHECK_EQUAL(newOrder.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(newOrder.type(), exchanges::BATSBOE::EU::MsgType::NewOrder);
	BOOST_CHECK_EQUAL(newOrder.matchingUnit, 0);
	BOOST_CHECK_EQUAL(newOrder.sequenceNumber, 1);
	BOOST_CHECK_GE(newOrder.length(), msg::header_t_size);
	BOOST_CHECK_LE(newOrder.length(), sizeof(typename msg::NewOrder_t));
	typename msg::CancelOrder_t cancelOrder(sequenceNumber, clientOrderID);
	BOOST_CHECK_EQUAL(cancelOrder.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(cancelOrder.type(), exchanges::BATSBOE::EU::MsgType::CancelOrder);
	BOOST_CHECK_EQUAL(cancelOrder.matchingUnit, 0);
	BOOST_CHECK_EQUAL(cancelOrder.sequenceNumber, 1);
	BOOST_CHECK_GE(cancelOrder.length(), msg::header_t_size);
	BOOST_CHECK_LE(cancelOrder.length(), sizeof(typename msg::CancelOrder_t));
	typename msg::ModifyOrder_t modifyOrder(sequenceNumber, clientOrderID, 0, price, exchanges::BATSBOE::common::Side::Buy);
	BOOST_CHECK_EQUAL(modifyOrder.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(modifyOrder.type(), exchanges::BATSBOE::EU::MsgType::ModifyOrder);
	BOOST_CHECK_EQUAL(modifyOrder.matchingUnit, 0);
	BOOST_CHECK_EQUAL(modifyOrder.sequenceNumber, 1);
	BOOST_CHECK_GE(modifyOrder.length(), msg::header_t_size);
	BOOST_CHECK_LE(modifyOrder.length(), sizeof(typename msg::ModifyOrder_t));
	typename msg::TradeCaptureReport_t tradeCaptureReport(sequenceNumber);
	BOOST_CHECK_EQUAL(tradeCaptureReport.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(tradeCaptureReport.type(), exchanges::BATSBOE::EU::MsgType::TradeCaptureReport);
	BOOST_CHECK_EQUAL(tradeCaptureReport.matchingUnit, 0);
	BOOST_CHECK_EQUAL(tradeCaptureReport.sequenceNumber, 1);
	BOOST_CHECK_GE(tradeCaptureReport.length(), msg::header_t_size);
	BOOST_CHECK_LE(tradeCaptureReport.length(), sizeof(typename msg::TradeCaptureReport_t));
	typename msg::OrderAcknowledgement_t orderAcknowledgement(sequenceNumber, clientOrderID);
	BOOST_CHECK_EQUAL(orderAcknowledgement.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(orderAcknowledgement.type(), exchanges::BATSBOE::EU::MsgType::OrderAcknowledgement);
	BOOST_CHECK_EQUAL(orderAcknowledgement.matchingUnit, 0);
	BOOST_CHECK_EQUAL(orderAcknowledgement.sequenceNumber, 1);
	BOOST_CHECK_GE(orderAcknowledgement.length(), msg::header_t_size);
	BOOST_CHECK_LE(orderAcknowledgement.length(), sizeof(typename msg::OrderAcknowledgement_t));
	typename msg::OrderRejected_t orderRejected(sequenceNumber, clientOrderID, exchanges::BATSBOE::common::OrderRejectReason::Admin);
	BOOST_CHECK_EQUAL(orderRejected.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(orderRejected.type(), exchanges::BATSBOE::EU::MsgType::OrderRejected);
	BOOST_CHECK_EQUAL(orderRejected.matchingUnit, 0);
	BOOST_CHECK_EQUAL(orderRejected.sequenceNumber, 1);
	BOOST_CHECK_GE(orderRejected.length(), msg::header_t_size);
	BOOST_CHECK_LE(orderRejected.length(), sizeof(typename msg::OrderRejected_t));
	typename msg::OrderModified_t orderModified(sequenceNumber, clientOrderID, price, exchanges::BATSBOE::common::Side::Buy, 0);
	BOOST_CHECK_EQUAL(orderModified.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(orderModified.type(), exchanges::BATSBOE::EU::MsgType::OrderModified);
	BOOST_CHECK_EQUAL(orderModified.matchingUnit, 0);
	BOOST_CHECK_EQUAL(orderModified.sequenceNumber, 1);
	BOOST_CHECK_GE(orderModified.length(), msg::header_t_size);
	BOOST_CHECK_LE(orderModified.length(), sizeof(typename msg::OrderModified_t));
	typename msg::OrderRestated_t orderRestated(sequenceNumber, clientOrderID);
	BOOST_CHECK_EQUAL(orderRestated.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(orderRestated.type(), exchanges::BATSBOE::EU::MsgType::OrderRestated);
	BOOST_CHECK_EQUAL(orderRestated.matchingUnit, 0);
	BOOST_CHECK_EQUAL(orderRestated.sequenceNumber, 1);
	BOOST_CHECK_GE(orderRestated.length(), msg::header_t_size);
	BOOST_CHECK_LE(orderRestated.length(), sizeof(typename msg::OrderRestated_t));
	typename msg::UserModifyRejected_t userModifyRejected(sequenceNumber, clientOrderID, exchanges::BATSBOE::common::OrderRejectReason::Admin);
	BOOST_CHECK_EQUAL(userModifyRejected.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(userModifyRejected.type(), exchanges::BATSBOE::EU::MsgType::UserModifyRejected);
	BOOST_CHECK_EQUAL(userModifyRejected.matchingUnit, 0);
	BOOST_CHECK_EQUAL(userModifyRejected.sequenceNumber, 1);
	BOOST_CHECK_GE(userModifyRejected.length(), msg::header_t_size);
	BOOST_CHECK_LE(userModifyRejected.length(), sizeof(typename msg::UserModifyRejected_t));
	typename msg::OrderCancelled_t orderCancelled(sequenceNumber, clientOrderID, exchanges::BATSBOE::common::OrderRejectReason::Admin, symbol, price, exchanges::BATSBOE::common::Side::Buy, 0, 0);
	BOOST_CHECK_EQUAL(orderCancelled.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(orderCancelled.type(), exchanges::BATSBOE::EU::MsgType::OrderCancelled);
	BOOST_CHECK_EQUAL(orderCancelled.matchingUnit, 0);
	BOOST_CHECK_EQUAL(orderCancelled.sequenceNumber, 1);
	BOOST_CHECK_GE(orderCancelled.length(), msg::header_t_size);
	BOOST_CHECK_LE(orderCancelled.length(), sizeof(typename msg::OrderCancelled_t));
	typename msg::CancelRejected_t cancelRejected(sequenceNumber, clientOrderID, exchanges::BATSBOE::common::OrderRejectReason::Admin);
	BOOST_CHECK_EQUAL(cancelRejected.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(cancelRejected.type(), exchanges::BATSBOE::EU::MsgType::CancelRejected);
	BOOST_CHECK_EQUAL(cancelRejected.matchingUnit, 0);
	BOOST_CHECK_EQUAL(cancelRejected.sequenceNumber, 1);
	BOOST_CHECK_GE(cancelRejected.length(), msg::header_t_size);
	BOOST_CHECK_LE(cancelRejected.length(), sizeof(typename msg::CancelRejected_t));
	typename msg::OrderExecution_t orderExecution(sequenceNumber, clientOrderID, price, instID, exchanges::BATSBOE::common::Side::Buy);
	BOOST_CHECK_EQUAL(orderExecution.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(orderExecution.type(), exchanges::BATSBOE::EU::MsgType::OrderExecution);
	BOOST_CHECK_EQUAL(orderExecution.matchingUnit, 0);
	BOOST_CHECK_EQUAL(orderExecution.sequenceNumber, 1);
	BOOST_CHECK_GE(orderExecution.length(), msg::header_t_size);
	BOOST_CHECK_LE(orderExecution.length(), sizeof(typename msg::OrderExecution_t));
	typename msg::TradeCancelOrCorrect_t tradeCancelOrCorrect(sequenceNumber, clientOrderID);
	BOOST_CHECK_EQUAL(tradeCancelOrCorrect.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(tradeCancelOrCorrect.type(), exchanges::BATSBOE::EU::MsgType::TradeCancelOrCorrect);
	BOOST_CHECK_EQUAL(tradeCancelOrCorrect.matchingUnit, 0);
	BOOST_CHECK_EQUAL(tradeCancelOrCorrect.sequenceNumber, 1);
	BOOST_CHECK_GE(tradeCancelOrCorrect.length(), msg::header_t_size);
	BOOST_CHECK_LE(tradeCancelOrCorrect.length(), sizeof(typename msg::TradeCancelOrCorrect_t));
	typename msg::TradeCaptureReportAck_t tradeCaptureReportAck(sequenceNumber);
	BOOST_CHECK_EQUAL(tradeCaptureReportAck.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(tradeCaptureReportAck.type(), exchanges::BATSBOE::EU::MsgType::TradeCaptureReportAck);
	BOOST_CHECK_EQUAL(tradeCaptureReportAck.matchingUnit, 0);
	BOOST_CHECK_EQUAL(tradeCaptureReportAck.sequenceNumber, 1);
	BOOST_CHECK_GE(tradeCaptureReportAck.length(), msg::header_t_size);
	BOOST_CHECK_LE(tradeCaptureReportAck.length(), sizeof(typename msg::TradeCaptureReportAck_t));
	typename msg::TradeCaptureReportReject_t tradeCaptureReportReject(sequenceNumber);
	BOOST_CHECK_EQUAL(tradeCaptureReportReject.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(tradeCaptureReportReject.type(), exchanges::BATSBOE::EU::MsgType::TradeCaptureReportReject);
	BOOST_CHECK_EQUAL(tradeCaptureReportReject.matchingUnit, 0);
	BOOST_CHECK_EQUAL(tradeCaptureReportReject.sequenceNumber, 1);
	BOOST_CHECK_GE(tradeCaptureReportReject.length(), msg::header_t_size);
	BOOST_CHECK_LE(tradeCaptureReportReject.length(), sizeof(typename msg::TradeCaptureReportReject_t));
	typename msg::TradeCaptureConfirm_t tradeCaptureConfirm(sequenceNumber);
	BOOST_CHECK_EQUAL(tradeCaptureConfirm.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(tradeCaptureConfirm.type(), exchanges::BATSBOE::EU::MsgType::TradeCaptureConfirm);
	BOOST_CHECK_EQUAL(tradeCaptureConfirm.matchingUnit, 0);
	BOOST_CHECK_EQUAL(tradeCaptureConfirm.sequenceNumber, 1);
	BOOST_CHECK_GE(tradeCaptureConfirm.length(), msg::header_t_size);
	BOOST_CHECK_LE(tradeCaptureConfirm.length(), sizeof(typename msg::TradeCaptureConfirm_t));
	typename msg::TradeCaptureDecline_t tradeCaptureDecline(sequenceNumber);
	BOOST_CHECK_EQUAL(tradeCaptureDecline.start_of_message, exchanges::BATSBOE::common::msg_start_code);
	BOOST_CHECK_EQUAL(tradeCaptureDecline.type(), exchanges::BATSBOE::EU::MsgType::TradeCaptureDecline);
	BOOST_CHECK_EQUAL(tradeCaptureDecline.matchingUnit, 0);
	BOOST_CHECK_EQUAL(tradeCaptureDecline.sequenceNumber, 1);
	BOOST_CHECK_GE(tradeCaptureDecline.length(), msg::header_t_size);
	BOOST_CHECK_LE(tradeCaptureDecline.length(), sizeof(typename msg::TradeCaptureDecline_t));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(find_value, msg, msg_types)
{
	typename msg::NewOrder_t newOrder(sequenceNumber, clientOrderID, exchanges::BATSBOE::common::OrdType::Market, exchanges::BATSBOE::common::TIF::Day, exchanges::BATSBOE::common::Side::Buy, symbol, instID, 0, price);
	BOOST_CHECK_EQUAL(newOrder.clientOrderID(), clientOrderID);
	BOOST_CHECK_EQUAL(newOrder.orderQty(), 0);
	BOOST_CHECK_EQUAL(newOrder.orderType(), exchanges::BATSBOE::common::OrdType::Market);
	BOOST_CHECK_EQUAL(newOrder.side(), exchanges::BATSBOE::common::Side::Buy);
	BOOST_CHECK_EQUAL(newOrder.instrumentID(), instID);
	BOOST_CHECK_EQUAL(newOrder.limitPrice(), price);
}

BOOST_AUTO_TEST_SUITE_END()
