# Copyright © 2010 by J.M.McGuiness, libjmmcg@hussar.me.uk
#!/bin/sh
echo "Testing..."
cd $WORKSPACE/build/libjmmcg/examples
nice -n 19 ionice -c 3 ctest --no-compress-output --verbose -j 2 -T Test
cd $WORKSPACE/build/libjmmcg/isimud/tests
nice -n 19 ionice -c 3 ctest --no-compress-output --verbose -T Test
echo "Tested."
exit 0
