# Copyright © 2010 by J.M.McGuiness, libjmmcg@hussar.me.uk
#!/bin/sh
echo "Quality checks..."
mkdir -p $WORKSPACE/reports/cppcheck
mkdir -p $WORKSPACE/reports/cloc
nice -n 19 ionice -c 3 cppcheck --enable=all --inconclusive --xml --xml-version=2 $WORKSPACE/libjmmcg/core 2>$WORKSPACE/reports/cppcheck/core-result.xml
nice -n 19 ionice -c 3 cppcheck --enable=all --inconclusive --xml --xml-version=2 $WORKSPACE/libjmmcg/unix 2>$WORKSPACE/reports/cppcheck/unix-result.xml
nice -n 19 ionice -c 3 cppcheck --enable=all --inconclusive --xml --xml-version=2 $WORKSPACE/libjmmcg/isimud 2>$WORKSPACE/reports/cppcheck/isimud-result.xml
nice -n 19 ionice -c 3 cloc --by-file --xml --out=$WORKSPACE/reports/cloc/core.xml $WORKSPACE/libjmmcg/core
nice -n 19 ionice -c 3 cloc --by-file --xml --out=$WORKSPACE/reports/cloc/unix.xml $WORKSPACE/libjmmcg/unix
nice -n 19 ionice -c 3 cloc --by-file --xml --out=$WORKSPACE/reports/cloc/isimud.xml $WORKSPACE/libjmmcg/isimud
echo "Finished checks."
