# Copyright © 2010 by J.M.McGuiness, libjmmcg@hussar.me.uk
#!/bin/sh
echo "Packaging..."
cd $WORKSPACE/build/libjmmcg
nice -n 19 ionice -c 3 make -j$NUM_CONCURRENT_BUILDS package
mkdir -p ../../build_$BUILD_NUMBER
cp $WORKSPACE/build/libjmmcg/ChangeLog ../../build_$BUILD_NUMBER/.
cp $WORKSPACE/build/libjmmcg/README ../../build_$BUILD_NUMBER/.
cp libjmmcg-*-Linux-x86_64-Release.* ../../build_$BUILD_NUMBER/.
echo "Packaged."
