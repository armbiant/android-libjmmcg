# Copyright © 2010 by J.M.McGuiness, libjmmcg@hussar.me.uk
#!/bin/sh
echo "Cleaning..."
rm -rf $WORKSPACE/build
mkdir -p $WORKSPACE/build/src_release
rm -rf $WORKSPACE/build/src_release/libjmmcg-*

rm -rf $WORKSPACE/build_*
rm -rf $WORKSPACE/generatedJUnitFiles
rm -rf $WORKSPACE/reports
rm -rf $WORKSPACE/tests

echo "Creating source tarball..."
mkdir -p $WORKSPACE/build_$BUILD_NUMBER
cp -R $WORKSPACE/libjmmcg $WORKSPACE/build/src_release/libjmmcg-$BUILD_NUMBER
find $WORKSPACE/build/src_release/libjmmcg-$BUILD_NUMBER -type d -name ".git"|xargs rm -rf
rm -rf $WORKSPACE/build/src_release/libjmmcg-$BUILD_NUMBER/docs/html
rm -rf $WORKSPACE/build/src_release/libjmmcg-$BUILD_NUMBER/docs/latex
cd $WORKSPACE/build/src_release
nice -n 19 ionice -c 3 tar --lzma -cf $WORKSPACE/build_$BUILD_NUMBER/libjmmcg-$BUILD_NUMBER.src.tar.lzma libjmmcg-$BUILD_NUMBER
mkdir -p $WORKSPACE/build/libjmmcg
cd $WORKSPACE/build/libjmmcg
rm -rf examples/Testing

echo "Configuring..."
CMAKE_COMMAND="cmake -DCMAKE_BUILD_TYPE=Release -DQUIET_DOXYGEN_BUILD=YES -DBUILD_DOCUMENTATION=$BUILD_DOCUMENTATION -DBUILD_CHANGELOG=$BUILD_CHANGELOG -DJMMCG_PERFORMANCE_TESTS=$JMMCG_PERFORMANCE_TESTS -DJMMCG_OPTIMISATION_WARNINGS=$JMMCG_OPTIMISATION_WARNINGS ../../libjmmcg"
echo "cmake command line: " $CMAKE_COMMAND
$CMAKE_COMMAND
echo "Compiling..."
nice -n 19 ionice -c 3 make -j$NUM_CONCURRENT_BUILDS
echo "Compiled."
