/******************************************************************************
 ** Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "generate_mph_mask.hpp"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>

namespace std {

std::ostream&
operator<<(std::ostream& os, libjmmcg::generate_mph_mask::container_type const& values) noexcept(false) {
	bool is_first= true;
	for(auto const v: values) {
		if(is_first) {
			is_first= false;
		} else {
			os << ", ";
		}
		os << v << "U";
	}
	return os;
}

}

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

generate_mph_mask::generate_mph_mask(container_type const& v, std::size_t const num_threads) noexcept(false)
	: values_(v), pool_(num_threads) {
}

bool
generate_mph_mask::has_collisions(container_type const& hashed_values) noexcept(true) {
	static thread_local std::set<element_type> sorted_hashed;
	sorted_hashed= std::set<element_type>(hashed_values.begin(), hashed_values.end());
	assert(sorted_hashed.size() <= hashed_values.size());
	return sorted_hashed.size() < hashed_values.size();
}

void
generate_mph_mask::make_hashes_from(std::size_t const denominator, element_type const mask, container_type& hashed_values) const noexcept(false) {
	hashed_values.clear();
	hashed_values.reserve(values_.size());
	std::transform(
		values_.begin(),
		values_.end(),
		std::back_inserter(hashed_values),
		[this, denominator, mask](auto const v) {
			return this->generate_hash(v, mask, denominator);
		});
	std::sort(hashed_values.begin(), hashed_values.end());
	assert(hashed_values.size() == values_.size());
	if(has_collisions(hashed_values)) {
		hashed_values.clear();
	}
}

std::optional<generate_mph_mask::element_type>
generate_mph_mask::test_a_mask(std::size_t const denominator, element_type const mask, std::ostream& os) const noexcept(false) {
	static thread_local container_type hashed_values;
	make_hashes_from(denominator, mask, hashed_values);
	if(!hashed_values.empty()) {
		assert(*hashed_values.rbegin() <= denominator);
		std::ostringstream oss;
		oss << "[" << pool_type::thread_traits::get_name(pool_type::thread_traits::get_current_thread()) << ":0x" << std::hex << std::this_thread::get_id() << std::dec << "] Mask=0x" << std::hex << mask << std::dec << ", hashes found: {" << hashed_values << "}.\n";
		os << oss.str() << std::flush;
		return std::make_optional(mask);
	} else {
		return std::nullopt;
	}
}

generate_mph_mask::element_type
generate_mph_mask::find(std::size_t const denominator, std::ostream& os) const noexcept(false) {
	auto const& masks_found_ec= pool_ << pool_type::joinable() << pool_.find_first_of(std::make_pair(element_type{}, std::numeric_limits<element_type>::max()), [this, denominator, &os](element_type const mask) {
		return test_a_mask(denominator, mask, os);
	});
	auto masks_found= *masks_found_ec;
	if(!masks_found.empty()) {
		// Simple heuristic: smaller masks are better... Zero is the best!
		auto smallest_mask= std::min_element(masks_found.begin(), masks_found.end());
		assert(smallest_mask != masks_found.end());
		return *smallest_mask;
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("After an exhaustive search, failed to find a suitable mask for the input set of numbers. The hashing algorithm, 'xor_modulo_hasher()', in this code, may need to be changed or minimality could be relaxed by further increasing the denominator, at the cost of dispersion. thread-pool size={}, denominator={}, values={}", pool_.pool_size(), denominator, tostring(values_)), &generate_mph_mask::find));
	}
}

std::ostream&
operator<<(std::ostream& os, generate_mph_mask const& mph) noexcept(false) {
	os << mph.values();
	return os;
}

}}
