#ifndef LIBJMMCG_COMPUTE_MPH_MASK_XOR_MODULO_HASHER
#  define LIBJMMCG_COMPUTE_MPH_MASK_XOR_MODULO_HASHER
/******************************************************************************
 ** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "generate_mph_mask.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/**
 * Thanks to Dr.GoRAH for inspiring this highly-optimised solution! Especially his inspiring me to use the brute-force, modulo technique. Also for making me review it and improve the performance.
 * 
 * \note Regarding the hash algorithm used in xor_modulo_hasher() is designed to emit as few instructions as reasonable. Moreover it must only rely on immediate values in the final code; those values will be discovered via brute-force by this program (which is too slow via template meta-programming, or just breaks it by excessive depth and number of loops). As there are only immediate values produced, there will be minimal, extra impact on the D-cache(s). When an even number of inputs are provided, the algorithm fails to find a minimal perfect hash. Experimentation found that if the denominator is made the next largest odd number, then the solution can be found in for the examples tested. (Obviously the algorithm is not general, and certain inputs will cause it to fail.) So in the cases of odd inputs a minimal, perfect hash should be found. In the case of even, a perfect hash with only one extra slot from the minimal number is required, which I consider to be acceptable wastage.
 * 
 * \see xor_modulo_hasher(), perfect_hash, hash::msm
 */
class xor_modulo_hasher final : public generate_mph_mask {
public:
	using base_t=generate_mph_mask;
	using container_type=base_t::container_type;
	using element_type=base_t::element_type;

	xor_modulo_hasher(container_type const &v, std::size_t const num_threads) noexcept(false);

	/**
	 * This may generate the x86_64 DIV instruction, that does not take immediate values as arguments, sadly.
	 * But.... Lots of strength-reduction seems to occur (via <a href="https:gcc.godbolt.org/">Godbolt, praise be!</a>):
	 *		xorl		maks, %edi
	 *		movl		%edi, %eax
	 *		imulq		$954437177, %rax, %rax	;	Strength-reduction bit of denominator, in this case the values.
	 *		shrq		$33, %rax					;				"
	 *		leal		(%rax,%rax,8), %edx		;				"
	 *		movl		%edi, %eax
	 *		subl		%edx, %eax
	 */
	REALLY_FORCE_INLINE element_type generate_hash(container_type::value_type const v, element_type const mask, std::size_t const denominator) const noexcept(true) override;
};

} }

#include "xor_modulo_hasher_impl.hpp"

#endif
