#ifndef LIBJMMCG_COMPUTE_MPH_MASK_GENERATE_MPH_MASK_HPP
#define LIBJMMCG_COMPUTE_MPH_MASK_GENERATE_MPH_MASK_HPP
/******************************************************************************
 ** Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "core/blatant_old_msvc_compiler_hacks.hpp"
#include "core/thread_pool_workers.hpp"

#include <cstdint>
#include <iosfwd>
#include <optional>
#include <vector>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/**
 * Thanks to Dr.GoRAH for inspiring this highly-optimised solution! Especially his inspiring me to use the brute-force, modulo technique. Also for making me review it and improve the performance.
 *
 * \note Regarding the hash algorithm used in xor_modulo_hasher() is designed to emit as few instructions as reasonable. Moreover it must only rely on immediate values in the final code; those values will be discovered via brute-force by this program (which is too slow via template meta-programming, or just breaks it by excessive depth and number of loops). As there are only immediate values produced, there will be minimal, extra impact on the D-cache(s). When an even number of inputs are provided, the algorithm fails to find a minimal perfect hash. Experimentation found that if the denominator is made the next largest odd number, then the solution can be found in for the examples tested. (Obviously the algorithm is not general, and certain inputs will cause it to fail.) So in the cases of odd inputs a minimal, perfect hash should be found. In the case of even, a perfect hash with only one extra slot from the minimal number is required, which I consider to be acceptable wastage.
 *
 * \see xor_modulo_hasher(), perfect_hash, hash::msm
 */
class generate_mph_mask {
public:
	using pool_type= ppd::thread_pool<
		ppd::pool_traits::work_distribution_mode_t::worker_threads_get_work<
			ppd::pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>,
		ppd::pool_traits::size_mode_t::fixed_size,
		ppd::pool_aspects<
			ppd::generic_traits::return_data::element_type::joinable,
			ppd::platform_api,
			ppd::heavyweight_threading,
			ppd::pool_traits::normal_lifo,
			std::less,
			1> >;

	using element_type= std::uint32_t;	 ///< A 32-bit int is used as if a DIV instruction is generated, 32-bits is basically the fastest variant of it.
	using container_type= std::vector<element_type>;

	virtual ~generate_mph_mask() noexcept(true)= default;

	container_type const& values() const noexcept(true);
	element_type find(std::size_t const denominator, std::ostream& os) const noexcept(false);

	virtual element_type generate_hash(container_type::value_type const v, element_type const mask, std::size_t const denominator) const noexcept(true)= 0;

	void make_hashes_from(std::size_t const denominator, element_type const mask, container_type& hashed_values) const noexcept(false);

	friend std::ostream&
	operator<<(std::ostream& os, generate_mph_mask const& mph) noexcept(false);

protected:
	generate_mph_mask(container_type const& v, std::size_t const num_threads) noexcept(false);

private:
	container_type const values_;
	mutable pool_type pool_;

	element_type find_min_mask() const noexcept(true);
	element_type find_max_mask() const noexcept(true);
	static bool has_collisions(container_type const& hashed_values) noexcept(true);
	std::optional<element_type>
	test_a_mask(std::size_t const denominator, element_type const mask, std::ostream& os) const noexcept(false);
};

}}

namespace std {

std::ostream&
operator<<(std::ostream& os, libjmmcg::generate_mph_mask::container_type const& values) noexcept(false);

}

#include "generate_mph_mask_impl.hpp"

#endif
