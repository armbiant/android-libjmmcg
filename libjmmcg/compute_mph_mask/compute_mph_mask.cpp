/******************************************************************************
 ** Copyright © 2021 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "xor_modulo_hasher.hpp"

#include "core/application.hpp"
#include "core/stream_header_guard.hpp"

#include <boost/core/verbose_terminate_handler.hpp>
#include <boost/exception/diagnostic_information.hpp>

#include <fstream>

using namespace libjmmcg;

std::pair<xor_modulo_hasher::element_type, xor_modulo_hasher::element_type>
find_max_and_min(xor_modulo_hasher const& mph_mask, xor_modulo_hasher::element_type const mask, std::size_t const denominator) noexcept(false) {
	xor_modulo_hasher::container_type hashed_values;
	mph_mask.make_hashes_from(denominator, mask, hashed_values);
	if(hashed_values.empty()) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Apologies, no perfect could be hash found at all, sadly this is quite likely. The hashing algorithm has failed for these inputs. Perhaps suggest the author permit greater dispersion, i.e. further relax the minimal requirement. Please file a bug report, see the related documentation for details. hasher={}, denominator={}", tostring(mph_mask), denominator), &find_max_and_min));
	} else {
		std::pair<xor_modulo_hasher::element_type, xor_modulo_hasher::element_type> const min_and_max(*hashed_values.begin(), *hashed_values.rbegin());
		if(min_and_max.first > min_and_max.second) {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Apologies, the minimum hash must be less than the maximum hash (or equal if there is only one). The hashing algorithm has failed for these inputs. Please file a bug report, see the related documentation for details. hasher={}, denominator={}, hashed_values={}, mask={}, computed_min_hash={}, computed_max_hash={}", tostring(mph_mask), denominator, tostring(hashed_values), mask, min_and_max.first, min_and_max.second), &find_max_and_min));
		} else if((min_and_max.second - min_and_max.first) >= denominator) {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Apologies, the maximum less the minimum hash must be less than the denominator. The hashing algorithm has failed for these inputs. Please file a bug report, see the related documentation for details. hasher={}, denominator={}, hashed_values={}, mask={}, computed_min_hash={}, computed_max_hash={}, difference={}", tostring(mph_mask), denominator, tostring(hashed_values), mask, min_and_max.first, min_and_max.second, min_and_max.second - min_and_max.first), &find_max_and_min));
		} else {
			return min_and_max;
		}
	}
}

xor_modulo_hasher::element_type
find_mask(std::size_t max_denominator, xor_modulo_hasher& mph_mask, std::size_t& denominator_must_be_odd) noexcept(false) {
	assert(denominator_must_be_odd <= max_denominator);
	bool failed_to_find_a_mask= false;
	try {
		return mph_mask.find(denominator_must_be_odd, std::clog);
	} catch(std::exception const&) {
		failed_to_find_a_mask= true;
	}
	if(failed_to_find_a_mask) {
		// Potentially further relax the minimality requirement - try the next denominator up...
		denominator_must_be_odd= (denominator_must_be_odd + 1) | 0x1;
		if(denominator_must_be_odd <= max_denominator) {
			std::cout << "Had to increment denominator to " << denominator_must_be_odd << ", trying again... " << std::endl;
			return find_mask(max_denominator, mph_mask, denominator_must_be_odd);
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Apologies, dispersion exceeded, the hashing algorithm has failed for these inputs. Please file a bug report, see the related documentation for details. hasher={}, denominator={}, max_denominator={}", tostring(mph_mask), denominator_must_be_odd, max_denominator), &find_mask));
		}
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Apologies, the hashing algorithm has failed for these inputs. Please file a bug report, see the related documentation for details. hasher={}, denominator={}, max_denominator={}", tostring(mph_mask), denominator_must_be_odd, max_denominator), &find_mask));
	}
}

/**
 * \todo Consider Jon Cherterfield's idea of the "Constraint Satisfaction Problem" to reduce the search-space: "Boolean sat is the fast black box but a hard to work with. Constraint satisfaction problem, CSP, is the easy to work with one where you may have to bring your own black box."
 */
int
main(int argc, char const* const* argv) noexcept(true) {
	struct header_guard_t : libjmmcg::header_guard_t {
		header_guard_t(std::string const& exe, std::string const& fname, std::ostream& os) noexcept(false)
			: libjmmcg::header_guard_t(exe, "CORE", fname, os) {
			os
				<< "#include \"core/blatant_old_msvc_compiler_hacks.hpp\"\n"
					"\n"
					"#include <boost/mpl/assert.hpp>\n"
					"\n"
					"#include <cstdint>\n"
					"#include <type_traits>\n"
					"\n"
					"namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {\n";
		}
		~header_guard_t() noexcept(false) {
			os << "} }\n";
		}
	};

	try {
		std::set_terminate(boost::core::verbose_terminate_handler);
		boost::program_options::options_description general(
			application::make_program_options(
				"A program to try and (brute-force) identify the required mask with which a perfect hash (which may be minimal) for the set of input numbers may be found. This is with the constraint that only immediate values and a small number of assembly operations are used. There is ABSOLUTELY no guarantee that such a mask may be found, moreover the exhaustive search could take a long time: think seconds to worst-case: hour-ish." + application::make_help_message()));
		boost::program_options::options_description prog_opts("Program options.");
		prog_opts.add_options()("cpp_header", boost::program_options::value<std::string>()->required(), "The file-path of the output C++ header-file.")("minimum_dispersion", boost::program_options::value<std::size_t>()->default_value(std::size_t{}), "The dispersion that the algorithm shall start from. This shall be set to at least the size of the input collection. This permits the algorithm to start from a larger dispersion to avoid redundant seeking thus time wasted.")("additional_dispersion", boost::program_options::value<std::size_t>()->default_value(std::size_t{}), "The additional dispersion permitted. The default of zero means a requirement on minimality on the hash. A non-zero value given would add to that minimality to permit dispersion. The algorithm will try minimal, then successive relaxations, incrementing by one, until a perfect hash is found or this limit is reached, to ensure termination of the algorithm in reasonable time.")("threads", boost::program_options::value<std::size_t>()->default_value(std::thread::hardware_concurrency() / 2 + 1), "The number of threads (plus one extra) that shall be used in the algorithm. By default, just more than half the available cores. Must be greater than zero.")("values", boost::program_options::value<xor_modulo_hasher::container_type>()->multitoken()->required(), "The set of 32-bit, unsigned numbers for which a very simplistic minimal, perfect, hash is to be attempted to be computed.");
		boost::program_options::options_description all("All options.");
		all.add(general).add(prog_opts);
		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::parse_command_line(argc, argv, all), vm);
		if(exit_codes::codes const exit_code= application::check_basic_options(vm, all, std::cout); exit_code != exit_codes::codes::exit_success) {
			return exit_code;
		}
		if(vm["threads"].as<std::size_t>() == 0) {
			std::cerr << "The number of threads must be greater than zero." << std::endl;
			return exit_codes::exit_parameter_error;
		}
		boost::program_options::notify(vm);
		xor_modulo_hasher mph_mask(vm["values"].as<xor_modulo_hasher::container_type>(), vm["threads"].as<std::size_t>());
		// xor_modulo_hasher() requires an odd denominator to work at all, so with an even set of inputs, we must relax minimality..
		std::size_t denominator_must_be_odd= (std::max(mph_mask.values().size(), vm["minimum_dispersion"].as<std::size_t>())) | 0x1;
		std::cout << "Trying to find the mask for input values: {";
		std::cout << mph_mask.values();
		std::cout << "}.\nPlease wait... " << std::endl;
		auto const mask= find_mask(denominator_must_be_odd + vm["additional_dispersion"].as<std::size_t>(), mph_mask, denominator_must_be_odd);
		if(denominator_must_be_odd >= mph_mask.values().size()) {
			auto const min_and_max= find_max_and_min(mph_mask, mask, denominator_must_be_odd);
			if(mph_mask.values().size() == denominator_must_be_odd) {
				std::cout << "Minimal perfect hash found!\n";
			} else {
				std::cout << "Perfect hash found; as the denominator had to be incremented, minimality could not be satisfied.\n";
			}
			std::cout << "Denominator=" << denominator_must_be_odd << ". Mask found=0x" << std::hex << mask << ".\nWith these values, now writing the C++ header file to: '" << vm["cpp_header"].as<std::string>() << "'...\n"
						 << std::flush;
			std::ofstream cpp_header(vm["cpp_header"].as<std::string>().c_str());
			const header_guard_t header_guard(argv[0], vm["cpp_header"].as<std::string>(), cpp_header);
			cpp_header
				<< "\n"
					"/**\n"
					" * If the compiler gives errors noting attempting to use this declaration, then it is likely the appropriate cmake-script will need updating according to the start-states in the hash::msm used. The error message given, whilst long, lists exactly what needs to be updated in the cmake script."
					" *\n"
					" * \\see compute_mph_mask, hash::msm\n"
					" */\n"
					"template<auto... Values>\n"
					"struct perfect_hash;\n"
					"\n"
					"template<>\n"
					"struct perfect_hash<";
			cpp_header << mph_mask.values();
			cpp_header
				<< "> {\n"
					"\tusing key_type=decltype("
				<< *mph_mask.values().begin() << "U);\n"
															"\tusing element_type=std::uint32_t;\n"
															"\n";
			for(auto const v: mph_mask.values()) {
				cpp_header
					<< "\tstatic_assert(std::is_same<key_type, decltype(" << v << "U)>::value, \"All the keys must be of the same type.\");\t// Hashed-value=" << mph_mask.generate_hash(v, mask, denominator_must_be_odd) << "\n";
			}
			cpp_header
				<< "\tBOOST_MPL_ASSERT_RELATION(sizeof(key_type), <=, sizeof(element_type));\n"
					"\n"
					"\tenum : element_type {\n"
					"\t\tsize="
				<< mph_mask.values().size() << "U,\n"
														 "\t\tmin_hash="
				<< min_and_max.first << "U,\n"
												"\t\tmax_hash="
				<< min_and_max.second << "U,\n"
												 "\t\tmask=0x"
				<< std::hex << mask << std::dec << ",\n"
															  "\t\tdenominator="
				<< denominator_must_be_odd << "U,\n"
														"\t\tis_minimal=(denominator==size)\t///< Evaluates to="
				<< std::boolalpha << (denominator_must_be_odd == mph_mask.values().size()) << "\n"
																														"\t};\n"
																														"\n"
																														"\tREALLY_FORCE_INLINE static constexpr element_type result(key_type const &k) noexcept {\n"
																														"\t\tif constexpr (mask>0) {\n"
																														"\t\t\treturn (static_cast<element_type>(k)^mask)%denominator;\n"
																														"\t\t} else {\n"
																														"\t\t\treturn static_cast<element_type>(k)%denominator;\n"
																														"\t\t}\n"
																														"\t}\n"
																														"\tREALLY_FORCE_INLINE constexpr element_type operator()(key_type const &k) const noexcept {\n"
																														"\t\treturn result(k);\n"
																														"\t}\n"
																														"};\n\n";
			std::cout << "Written." << std::endl;
			return exit_codes::exit_success;
		} else {
			BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Apologies, the hashing algorithm has failed for these inputs. (The denominator must be greater than or equal to the number of unique, hashed values.) Please file a bug report, see the related documentation for details. hash={}, denominator={}, minimum_dispersion={}, additional_dispersion={}, cpp_header='{}', threads={}", tostring(mph_mask), denominator_must_be_odd, vm["minimum_dispersion"].as<std::size_t>(), vm["additional_dispersion"].as<std::size_t>(), vm["cpp_header"].as<std::string>(), vm["threads"].as<std::size_t>())));
		}
	} catch(...) {
		std::cerr << "Unknown exception. Details: " << boost::current_exception_diagnostic_information() << std::endl;
		return exit_codes::exit_unknown_exception;
	}
	return exit_codes::exit_unknown_failure;
}
