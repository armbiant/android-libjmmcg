/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../core/hp_timer.hpp"
#include "posix_locking.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace pthreads {

anon_semaphore::lock_result_type
anon_semaphore::lock(const timeout_type period) noexcept(false) {
	using timer_t=hp_timer<ppd::generic_traits::api_type::posix_pthreads, heavyweight_threading>;

	if (period==0) {
		return try_lock();
	} else if (period<lock_traits::infinite_timeout()) {
		static const timer_t time;
		timer_t::time_utc_t now=time.current_time();
		now.tv_nsec+=(period%1000)*1000000;
		now.tv_sec+=period/1000+now.tv_nsec/1000000000;
		now.tv_nsec=now.tv_nsec%1000000000;
		assert(now.tv_nsec>=0);
		assert(now.tv_nsec<1000000000);
		if (sem_timedwait(&sem, &now)==-1) {
			switch (errno) {
			case ETIMEDOUT: return lock_traits::atom_unset;
			case EINTR: return lock_traits::atom_interrupted;
			case EAGAIN: return lock_traits::atom_max_recurse;
			case EINVAL:
			default: return lock_traits::atom_abandoned;
			};
		}
		return lock_traits::atom_set;
	} else {
		return lock();
	}
}

} } } }
