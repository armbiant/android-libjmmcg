/******************************************************************************
 * * Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "core/syscall_wrapper.hpp"

#include <cstddef>

#include <sys/mman.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

class lock_mem_range final {
public:
	/**
	 * \see ::mlock()
	 */
	[[nodiscard]] lock_mem_range(std::byte volatile const* buff, std::size_t size) noexcept(false);
	~lock_mem_range() noexcept(false);

private:
	std::byte const* const page_aligned_buff_;
	std::size_t const page_aligned_size_;
};

class lock_all_proc_mem final {
public:
	enum class flags : int {
		current= MCL_CURRENT,
		future= MCL_FUTURE,
		on_fault= MCL_ONFAULT
	};

	/**
	 * \see ::mlockall()
	 */
	[[nodiscard]] explicit lock_all_proc_mem(flags f) noexcept(false);
	~lock_all_proc_mem() noexcept(false);

	lock_all_proc_mem(lock_all_proc_mem const&)= delete;
	void operator=(lock_all_proc_mem const&)= delete;
};

constexpr inline lock_all_proc_mem::flags
operator|(lock_all_proc_mem::flags const l, lock_all_proc_mem::flags const r) noexcept(true) {
	return static_cast<lock_all_proc_mem::flags>(static_cast<std::underlying_type<lock_all_proc_mem::flags>::type>(l) | static_cast<std::underlying_type<lock_all_proc_mem::flags>::type>(r));
}

}}
