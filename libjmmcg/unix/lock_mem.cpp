/******************************************************************************
 * * Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "lock_mem.hpp"

#include <fmt/format.h>

#include <unistd.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

lock_mem_range::lock_mem_range(std::byte volatile const* buff, std::size_t size) noexcept(false)
	: page_aligned_buff_(
		[this, buff]() {
			if(buff != nullptr) {
				errno= 0;
				long const page_size_orig= JMMCG_SYSCALL_WRAPPER("Unable to obtain the system page size.", ::sysconf, _SC_PAGESIZE);
				assert(page_size_orig > 0);
				auto const page_size= static_cast<std::size_t>(page_size_orig);
				auto const* aligned_buff= reinterpret_cast<std::byte const*>((reinterpret_cast<std::size_t>(buff) / page_size) * page_size);
				assert(aligned_buff <= buff);
				return aligned_buff;
			} else {
				BOOST_THROW_EXCEPTION(throw_exception<std::invalid_argument>(fmt::format("Pointer to the buffer must be non-null, pointer={}", reinterpret_cast<void const*>(const_cast<std::byte const*>(buff))), this));
			}
		}()),
	  page_aligned_size_(
		  [this, buff, size]() {
			  if(size > 0) {
				  errno= 0;
				  long const page_size_orig= JMMCG_SYSCALL_WRAPPER("Unable to obtain the system page size.", ::sysconf, _SC_PAGESIZE);
				  assert(page_size_orig > 0);
				  auto const page_size= static_cast<std::size_t>(page_size_orig);
				  [[maybe_unused]] std::byte volatile const* end_buff= buff + size;
				  assert(buff < end_buff);
				  std::byte volatile const* end_aligned_buff= reinterpret_cast<std::byte const*>(((reinterpret_cast<std::size_t>(end_buff) / page_size) + 1) * page_size);
				  assert(end_aligned_buff >= end_buff);
				  assert(page_aligned_buff_ < end_aligned_buff);
				  std::ptrdiff_t const aligned_size= end_aligned_buff - page_aligned_buff_;
				  assert(aligned_size >= 0);
				  assert(static_cast<std::size_t>(aligned_size) >= size);
				  return static_cast<std::size_t>(aligned_size);
			  } else {
				  BOOST_THROW_EXCEPTION(throw_exception<std::invalid_argument>(fmt::format("The size of the buffer must be greater than zero, pointer={}, size={}", reinterpret_cast<void const*>(const_cast<std::byte const*>(buff)), size), this));
			  }
		  }()) {
	JMMCG_SYSCALL_WRAPPER("Unable to lock the specified memory region.", ::mlock, page_aligned_buff_, page_aligned_size_);
}

lock_mem_range::~lock_mem_range() noexcept(false) {
	JMMCG_SYSCALL_WRAPPER("Unable to unlock the specified memory region.", ::munlock, page_aligned_buff_, page_aligned_size_);
}

lock_all_proc_mem::lock_all_proc_mem(flags f) noexcept(false) {
	JMMCG_SYSCALL_WRAPPER("Unable to lock all of the memory for the process.", ::mlockall, static_cast<int>(f));
}

lock_all_proc_mem::~lock_all_proc_mem() noexcept(false) {
	JMMCG_SYSCALL_WRAPPER_NO_PARAMS("Unable to unlock all of the memory for the process.", ::munlockall);
}

}}
