## Copyright © 2019 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#!/bin/bash
shopt -s lastpipe

if [ $# != 2 ]
then
	echo "Usage: $0 OUTPUT_FILEPATH VERSIOM"
	echo "	OUTPUT_FILEPATH	Fully-qualified path and name (including extension) to the C++ header file to be written."
	echo "	VERSION	The version number of the file that will be created, from the VCS-provided version."
	exit 1
fi

OUTPUT_HPP=$1
VERSION_NUMBER=$2
NUMACTL_EXE=`which numactl`

if [ -z $NUMACTL_EXE ]
then
	echo "numactl is required for this script to run."
	exit 2
fi

echo "/******************************************************************************
**
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

// Auto-generated header file.
// DO NOT EDIT. IT WILL BE OVERWRITTEN.
// Version: "$VERSION_NUMBER"

/**
	\file The contents of this file may not be correct for your system. It may be wise to regenerate it using '$(basename $0) NUMA_TRAITS_HPP_FILE VERSION_NUMBER' in the installation location as a suitable user.
*/

#include \"core_config.h\"

#include <array>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

	struct numa_cpu_traits {" > $OUTPUT_HPP

NUM_NUMA_NODES=1
CORE_LIST_INIT_LIST="{}"
IS_NUMA=false
NUM_CORES=1

NUMA_OUTPUT=`$NUMACTL_EXE --hardware`
if [ "$NUMA_OUTPUT" == "No NUMA available on this system" ]
then
	RAW_NUMA_OUTPUT=`$NUMACTL_EXE --show`
	NUMA_OUTPUT=`echo "$RAW_NUMA_OUTPUT" | head -1`
	NUM_CORES=$((`echo "$NUMA_OUTPUT" | wc -w`-1))
	CORE_LIST1=`echo "$NUMA_OUTPUT" | cut --complement -f1 --delimiter=:`
	CORE_LIST2=`echo "$CORE_LIST1" | cut -c 2-`
	NUM_CHARS_IN_CORES=$((`echo "$CORE_LIST2" | wc -c`-2))
	CORE_LIST3=`echo "$CORE_LIST2" | cut -c -"$NUM_CHARS_IN_CORES"`
	CORE_LIST=`echo ${CORE_LIST3// /, }`
	CORE_LIST_INIT_LIST="{"$CORE_LIST"}"
else
	IS_NUMA=true
	NUM_NODES_OUTPUT1=`echo "$NUMA_OUTPUT" | head -1`
	NUM_NODES_OUTPUT2=`echo "$NUM_NODES_OUTPUT1" | cut --complement -f1 --delimiter=:`
	NUM_NODES_OUTPUT3=`echo "$NUM_NODES_OUTPUT2" | cut -c 2-`
	NUM_NUMA_NODES=`echo "$NUM_NODES_OUTPUT3" | cut -f1 --delimiter=' '`
	CORE_NUMBERING_OUTPUT=`echo "$NUMA_OUTPUT" | grep "cpus"`
	CORE_LIST_INIT_LIST=""
	echo $"$CORE_NUMBERING_OUTPUT" | while read -r line; do
		CORE_LIST1=`echo "$line" | head -1`
		CORE_LIST2=`echo "$CORE_LIST1" | cut --complement -f1 --delimiter=:`
		CORE_LIST3=`echo "$CORE_LIST2" | cut -c 2-`
		NUM_CORES=`echo "$CORE_LIST3" | wc -w`
		CORE_LIST=`echo ${CORE_LIST3// /, }`
		CORE_LIST_INIT_LIST=$CORE_LIST_INIT_LIST", {"$CORE_LIST"}"
	done
	CORE_LIST_INIT_LIST=`echo "$CORE_LIST_INIT_LIST" | cut -c 3-`
fi

echo "		enum : std::size_t {" >> $OUTPUT_HPP
echo "			total_cores="$(($NUM_CORES*$NUM_NUMA_NODES)) >> $OUTPUT_HPP
echo "		};" >> $OUTPUT_HPP
echo "		enum : bool {" >> $OUTPUT_HPP
echo "			is_numa="$IS_NUMA >> $OUTPUT_HPP
echo "		};" >> $OUTPUT_HPP
echo "		using element_type=std::array<std::array<unsigned short, "$NUM_CORES">, "$NUM_NUMA_NODES">;" >> $OUTPUT_HPP
echo "		/// This provides a mapping of all sequentially-listed NUMA-nodes to the cores they contain and the mapping of those cores to to that returned by numactl and /proc/cpuinfo." >> $OUTPUT_HPP
echo "		/**" >> $OUTPUT_HPP
echo "			So one can use this mapping to pin, set priority, etc physical OS-threads and assign tasks to them as appropriate to make more efficient use of the NUMA topology of the target system." >> $OUTPUT_HPP
echo "			Note that kernel options such as CONFIG_NUMA, CONFIG_NUMA_BALANCING, CONFIG_NUMA_BALANCING_DEFAULT_ENABLED, etc may need to be enabled." >> $OUTPUT_HPP
echo "		*/" >> $OUTPUT_HPP
echo "		static inline constexpr const element_type node_mapping{{"$CORE_LIST_INIT_LIST"}};" >> $OUTPUT_HPP
echo "	};

} } }" >> $OUTPUT_HPP
echo -n >> $OUTPUT_HPP

echo "Written '$OUTPUT_HPP' with version: $VERSION_NUMBER."
exit 0
