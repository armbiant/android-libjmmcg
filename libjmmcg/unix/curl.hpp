#ifndef LIBJMMCG_UNIX_CURL_HPP
#define LIBJMMCG_UNIX_CURL_HPP

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <boost/noncopyable.hpp>

#include <list>
#include <sstream>
#include <string>
#include <vector>

#include <curl/curl.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	/**
	 * <a href="https://curl.haxx.se/libcurl/c/libcurl-tutorial.html">Curl tutorial</a>.
	 */
	class curl : boost::noncopyable {
	public:
		using recipients_t=std::list<std::string>;
		using failures_t=std::vector<std::tuple<std::string, std::string>>;

		class email_recipients : boost::noncopyable {
		public:
			~email_recipients() noexcept(true);

			/**
				Add two recipients, in this particular case they correspond to the To: and Cc: addressees in the header, but they could be any kind of recipient.
			*/
			void push_back(std::string const &addr) noexcept(true);
			bool empty() const noexcept(true);

		private:
			friend class curl;

			curl_slist * recipients_=nullptr;
			recipients_t addrs_;
		};

		/**
			\param	smtp_url The hostname:port to which the email should be sent.
		 */
		curl(std::string const &smtp_url, unsigned short port, std::string const &username, std::string const &password, bool enable_logging, bool enable_ssl_verification=false) noexcept(false);
		~curl() noexcept(true);

		/**
			Note that this option isn't strictly required, omitting it will result in libcurl sending the MAIL FROM command with empty sender data. All autoresponses should have an empty reverse-path, and should be directed to the address in the reverse-path which triggered them. Otherwise, they could cause an endless loop. See RFC 5321 Section 4.5.5 for more details.
		*/
		void from(std::string const &from) noexcept(true);

		/**
			We're using a callback function to specify the payload (the headers and body of the message). You could just use the CURLOPT_READDATA option to specify a FILE pointer to read from.
		*/
		failures_t send(std::string const &subject, std::string const &body, recipients_t const &recipients) noexcept(false);

		friend std::ostream &operator<<(std::ostream &os, curl const &c) noexcept(false) {
			os<<"SMTP server: '"<<c.smtp_url_<<"'"
				", logon username: '"<<c.username_<<"'"
				", from: '"<<c.from_<<"'.";
			return os;
		}

	private:
		using payload_message_t=std::vector<std::string>;
		struct upload_status {
			size_t lines_read=0;
		};

		CURL * const handle_;
		const std::string smtp_url_;
		const std::string username_;
		const std::string password_;
		std::string from_;
		upload_status upload_ctx_;

		static payload_message_t payload_text_strs_;

		/**
			If your server doesn't have a valid certificate, then you can disable part of the Transport Layer Security protection by setting the CURLOPT_SSL_VERIFYPEER and CURLOPT_SSL_VERIFYHOST options to 0 (false). That is, in general, a bad idea. It is still better than sending your authentication details in plain text though. Instead, you should get the issuer certificate (or the host certificate if the certificate is self-signed) and add it to the set of certificates that are known tolibcurl using CURLOPT_CAINFO and/or CURLOPT_CAPATH. See docs/SSLCERTS for more information.
		*/
		void ignore_ssl_verification() noexcept(true);

		/**
			Since the traffic will be encrypted, it is very useful to turn on debug information within libcurl to see what is happening during the transfer.
		*/
		void set_logging() noexcept(true);

		static std::string current_time_for_email() noexcept(false);
		void create_message(std::string const &to, std::string const &subject, std::string const &body) const noexcept(false);
		static size_t payload_source(void *ptr, size_t size, size_t nmemb, void *userp);
	};

} }

#endif
