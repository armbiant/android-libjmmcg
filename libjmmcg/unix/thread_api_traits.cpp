/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../core/thread_api_traits.hpp"

#include <fmt/format.h>

#include <boost/log/trivial.hpp>

static constexpr std::array signal_names{
	"SIGILL /* Illegal instruction (ANSI).  */",
	"SIGABRT /* Abort (ANSI).  */",
	"SIGBUS /* BUS error (4.2 BSD).  */",
	"SIGFPE /* Floating-point exception (ANSI).  */",
	"SIGSEGV /* Segmentation violation (ANSI).  */",
	"SIGSTKFLT /* Stack fault.  */",
	"SIGSYS /* Bad system call.  */",
	"unknown"};

static constexpr char const*
SIG_to_string(int signal_number) noexcept(true) {
	switch(signal_number) {
	case SIGILL:
		return signal_names[0];
	case SIGABRT:
		return signal_names[1];
	case SIGBUS:
		return signal_names[2];
	case SIGFPE:
		return signal_names[3];
	case SIGSEGV:
		return signal_names[4];
	case SIGSTKFLT:
		return signal_names[5];
	case SIGSYS:
		return signal_names[6];
	default:
		return signal_names[7];
	};
}

static std::string
to_string(siginfo_t const& si) noexcept(true) {
	return fmt::format("Signal number={:x}, CRT errno={:x} '{}', signal code={:x}", si.si_signo, si.si_errno, std::strerror(si.si_errno), si.si_code);
}

static struct sigaction old_sa;

extern "C" void
libjmmcg_abort_handler(int signal_number, siginfo_t* info, void* /*context*/) noexcept(true) {
	try {
		[[maybe_unused]] int ret= ::sigaction(SIGSEGV, &old_sa, nullptr);
		assert(ret == 0);
		// TODO output some useful info to the user:			ucontext_t const *cxt=reinterpret_cast<ucontext_t const *>(context);
		BOOST_LOG_TRIVIAL(fatal) << fmt::format("Signal trapped={:x}, '{}', {}\n{}", signal_number, SIG_to_string(signal_number), to_string(*info), boost::stacktrace::to_string(boost::stacktrace::stacktrace()));
	} catch(...) {
		BOOST_LOG_TRIVIAL(fatal) << boost::source_location() << "Fatal error in abort handler!";
	}
}

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE {
namespace ppd {

template<>
void
api_threading_traits<generic_traits::api_type::posix_pthreads, sequential_mode>::set_backtrace_on_signal() noexcept(true) {
	struct sigaction new_sa {};
	::sigemptyset(&new_sa.sa_mask);
	new_sa.sa_handler= nullptr;
	new_sa.sa_sigaction= libjmmcg_abort_handler;
	new_sa.sa_flags= 0;
	new_sa.sa_flags&= SA_SIGINFO;
	[[maybe_unused]] int ret= ::sigaction(SIGABRT, &new_sa, nullptr);
	assert(ret == 0);
	ret= ::sigaction(SIGILL, &new_sa, nullptr);
	assert(ret == 0);
	ret= ::sigaction(SIGTRAP, &new_sa, nullptr);
	assert(ret == 0);
	ret= ::sigaction(SIGABRT, &new_sa, nullptr);
	assert(ret == 0);
	ret= ::sigaction(SIGBUS, &new_sa, nullptr);
	assert(ret == 0);
	ret= ::sigaction(SIGFPE, &new_sa, nullptr);
	assert(ret == 0);
	ret= ::sigaction(SIGSEGV, &new_sa, &old_sa);
	assert(ret == 0);
	ret= ::sigaction(SIGSTKFLT, &new_sa, nullptr);
	assert(ret == 0);
	ret= ::sigaction(SIGSYS, &new_sa, nullptr);
	assert(ret == 0);
}

}}}
