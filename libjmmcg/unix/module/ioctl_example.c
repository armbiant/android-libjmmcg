/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, libjmmcg@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * \file	ioctl_example.c
 * \brief	An example loadable kernel module (LKM) that can display a message
 * in the /var/log/[kern.log|messages] file when the module is loaded and removed. The module can accept an
 * argument when it is loaded -- the name, which appears in the kernel log files.

	Details for how to create this came from:

	-# <a href="http://tldp.org/LDP/lkmpg/2.4/html/x856.html"/>
	-# <a href="linux-kernel-labs.github.io/master/labs/kernel_modules.html"/>
	-# <a href="http://tldp.org/HOWTO/Module-HOWTO/"/>
	-# <a href="http://derekmolloy.ie/writing-a-linux-kernel-module-part-1-introduction"/>
	-# <a href="https://wiki.archlinux.org/index.php/Compile_kernel_module"/>
*/

#include "core_config.h"

#include <linux/init.h>		// Macros used to mark up functions e.g., __init __exit.
#include <linux/module.h>	// Core header for loading LKMs into the kernel.
#include <linux/kernel.h>	// Contains types, macros, functions for the kernel.
 
MODULE_LICENSE(LIBJMMCG_MODULE_LICENSE);	///< The license type -- this affects runtime behavior.
MODULE_AUTHOR(LIBJMMCG_MODULE_AUTHOR);		///< The author -- visible when you use modinfo.
MODULE_DESCRIPTION(LIBJMMCG_MODULE_DESCRIPTION);	///< The description -- see modinfo.
MODULE_VERSION(LIBJMMCG_MODULE_VERSION);	///< The version of the module.
 
static char *name = LIBJMMCG_MODULE_NAME;	///< An LKM argument -- default value is "LIBJMMCG_MODULE_NAME".
module_param(name, charp, S_IRUGO);	///< Param desc. charp = char ptr, S_IRUGO can be read/not changed.
MODULE_PARM_DESC(name, "The module name to display in /var/log/kern.log");	///< Parameter description.
 
/** \brief The LKM initialization function.
 *  The static keyword restricts the visibility of the function to within this C file. The __init
 *  macro means that for a built-in driver (not a LKM) the function is only used at initialization
 *  time and that it can be discarded and its memory freed up after that point.
 *  \return Returns 0 if successful.
 */
static int __init init_the_module(void) {
   printk(KERN_INFO "%s v%s initialised.\n", name, LIBJMMCG_MODULE_VERSION);
   return 0;
}
 
/** \brief The LKM cleanup function.
 *  Similar to the initialization function, it is static. The __exit macro notifies that if this
 *  code is used for a built-in driver (not a LKM) that this function is not required.
 */
static void __exit exit_the_module(void) {
   printk(KERN_INFO "%s unloaded.\n", name);
}
 
/** \brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as
 *  listed above).
 */
module_init(init_the_module);
module_exit(exit_the_module);
