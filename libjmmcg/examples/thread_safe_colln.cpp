/******************************************************************************
** Copyright © 2010 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_safe_adaptors.hpp"
#include "core/rw_locking.hpp"

using namespace libjmmcg;
using namespace ppd;

template<typename Mdl, template<class, class> class Colln>
struct erew_colln_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		Colln<int, std::allocator<int> >,
		typename lock_traits::critical_section_type
	> result;
};

template<typename Mdl, template<class, class> class Colln>
struct crew_colln_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		Colln<int, std::allocator<int> >,
		lock::rw::template locker<lock_traits>,
		typename lock::rw::template locker<lock_traits>::decaying_write_lock_type
	> result;
};

template<typename Mdl, template<class, class> class Colln>
struct erew_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef queue<
		Colln<int, std::allocator<int> >,
		typename lock_traits::critical_section_type
	> result;
};

template<typename Mdl, template<class, class> class Colln>
struct erew_funky_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef funky_queue<
		Colln<int, std::allocator<int> >,
		typename lock_traits::critical_section_type
	> result;
};

typedef boost::mpl::list<
	heavyweight_threading,
	sequential_mode
> test_types;

BOOST_AUTO_TEST_SUITE(thread_safe_colln)

BOOST_AUTO_TEST_SUITE(list_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	typename colln_t::container_type src;
	colln_t s1(src);
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor_1, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	typename colln_t::container_type src;
	src.push_back(1);
	colln_t s1(src);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor_2, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	typename colln_t::container_type src;
	src.push_back(1);
	src.push_back(2);
	colln_t s1(src);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assignment, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	colln_t s1,s2;
	s1=s2;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 1U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s1.colln().front(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_copy, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	colln_t s1,s2;
	s1.push_back(1);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s2.colln().front(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	s1.clear();
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(queue_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, test_types) {
	typedef typename erew_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assignment, Mdl, test_types) {
	typedef typename erew_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1,s2;
	s1=s2;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item, Mdl, test_types) {
	typedef typename erew_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 1U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s1.front(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_copy, Mdl, test_types) {
	typedef typename erew_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1,s2;
	s1.push_back(1);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s2.front(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear, Mdl, test_types) {
	typedef typename erew_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	s1.clear();
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(funky_queue_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, test_types) {
	typedef typename erew_funky_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assignment, Mdl, test_types) {
	typedef typename erew_funky_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1,s2;
	s1=s2;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item, Mdl, test_types) {
	typedef typename erew_funky_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 1U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s1.front(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_copy, Mdl, test_types) {
	typedef typename erew_funky_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1,s2;
	s1.push_back(1);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s2.front(), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear, Mdl, test_types) {
	typedef typename erew_funky_queue_t<Mdl, std::list>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	s1.clear();
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(vector_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1;
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	typename colln_t::container_type src;
	colln_t s1(src);
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor_1, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	typename colln_t::container_type src;
	src.push_back(1);
	colln_t s1(src);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor_2, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	typename colln_t::container_type src;
	src.push_back(1);
	src.push_back(2);
	colln_t s1(src);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assignment, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1,s2;
	s1=s2;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 1U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s1[0], 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_copy, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1,s2;
	s1.push_back(1);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s2[0], 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear, Mdl, test_types) {
	typedef typename erew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	s1.clear();
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(vector_rw_lk_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(default_ctor, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1;
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	typename colln_t::container_type src;
	colln_t s1(src);
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor_1, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	typename colln_t::container_type src;
	src.push_back(1);
	colln_t s1(src);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(cctor_2, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	typename colln_t::container_type src;
	src.push_back(1);
	src.push_back(2);
	colln_t s1(src);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), src.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(assignment, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1,s2;
	s1=s2;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 1U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s1[0], 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(add_item_copy, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1,s2;
	s1.push_back(1);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
	BOOST_CHECK_EQUAL(s2[0], 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(clear, Mdl, test_types) {
	typedef typename crew_colln_t<Mdl, std::vector>::result colln_t;

	colln_t s1;
	s1.push_back(1);
	s1.clear();
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), 0U);
	BOOST_CHECK_EQUAL(s1.size(), s1.colln().size());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
