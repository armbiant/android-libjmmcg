/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include "core/memops.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(memops_tests)

BOOST_AUTO_TEST_SUITE(string)

BOOST_AUTO_TEST_CASE(strchr_char) {
	constexpr const char a='a';
	const char string[]="a";
	BOOST_CHECK_EQUAL(*strchr_opt<a>(string), *std::strchr(string, a));
	constexpr const char space=' ';
	char const * const l=strchr_opt<space>(string);
	char const * const r=std::strchr(string, space);
	BOOST_CHECK_EQUAL(l, r);
}

BOOST_AUTO_TEST_CASE(strchr_16) {
	constexpr const char a='a';
	const char string[]="thequickbrowfox";
	char const * const l1=strchr_opt<a>(string);
	char const * const r1=std::strchr(string, a);
	BOOST_CHECK_EQUAL(l1, r1);
	constexpr const char space=' ';
	char const * const l2=strchr_opt<space>(string);
	char const * const r2=std::strchr(string, space);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strchr_32) {
	constexpr const char a='a';
	const char string[]="thequickbrownfoxjumpsoverthelaz";
	char const * const l1=strchr_opt<a>(string);
	char const * const r1=std::strchr(string, a);
	BOOST_CHECK_EQUAL(l1, r1);
	constexpr const char space=' ';
	char const * const l2=strchr_opt<space>(string);
	char const * const r2=std::strchr(string, space);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strchr_48) {
	constexpr const char a='a';
	const char string[]="thequickbrownfoxjumpsoverthelazydog1234567890-=";
	char const * const l1=strchr_opt<a>(string);
	char const * const r1=std::strchr(string, a);
	BOOST_CHECK_EQUAL(l1, r1);
	constexpr const char space=' ';
	char const * const l2=strchr_opt<space>(string);
	char const * const r2=std::strchr(string, space);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strchr_49) {
	constexpr const char a='a';
	const char string[]="thequickbrownfoxjumpsoverthelazydog1234567890-=+";
	char const * const l1=strchr_opt<a>(string);
	char const * const r1=std::strchr(string, a);
	BOOST_CHECK_EQUAL(l1, r1);
	constexpr const char space=' ';
	char const * const l2=strchr_opt<space>(string);
	char const * const r2=std::strchr(string, space);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_small) {
	const char substr[]="fox";
	const char string[32]="thefox                         ";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, substr);
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[]="foxy";
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, substr1);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_16) {
	const char substr[]="fox";
	const char string[32]="thequickbrowfox                ";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, substr);
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[]="foxy";
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, substr1);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_32) {
	const char substr[]="fox";
	const char string[32]="thequickbrownfoxjumpsoverthelaz";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, substr);
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[]="foxy";
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, substr1);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_48) {
	const char substr[]="fox";
	const char string[64]="thequickbrownfoxjumpsoverthelazydog1234567890-=                ";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, substr);
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[]="foxy";
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, substr1);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_49) {
	const char substr[]="fox";
	const char string[64]="thequickbrownfoxjumpsoverthelazydog1234567890-=+               ";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, substr);
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[]="foxy";
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, substr1);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_FIX_aligned) {
	const char substr[]="\00135=";
	alignas(std::uint64_t) const char string[128]="8=FIX.5.0\0019=099\00135=A\00149=SENDER\00156=TARGET\00134=1\00152=20000426-12:05:06\00198=0\001108=30\001553=USER\001554=PASSWORD\001925=NEWPASSWD\00110=238\001     ";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, substr);
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[]="\0016842=";
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, substr1);
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_FIX_aligned_no_terminal_NULL) {
	const char substr[4]={'\001', '3', '5', '='};
	alignas(std::uint64_t) const char string[128]="8=FIX.5.0\0019=099\00135=A\00149=SENDER\00156=TARGET\00134=1\00152=20000426-12:05:06\00198=0\001108=30\001553=USER\001554=PASSWORD\001925=NEWPASSWD\00110=238\001     ";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, "\00135=");
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[6]={'\002', '6', '8', '4', '2', '='};
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, "\0026842=");
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_CASE(strstr_FIX_misaligned) {
	const char substr[4]={'\001', '3', '5', '='};
	alignas(std::uint64_t) const char string[128]="\0019=099\00135=A\00149=SENDER\00156=TARGET\00134=1\00152=20000426-12:05:06\00198=0\001108=30\001553=USER\001554=PASSWORD\001925=NEWPASSWD\00110=238\001              ";
	char const * const l1=strstr_opt(string, substr);
	char const * const r1=std::strstr(string, "\00135=");
	BOOST_CHECK_EQUAL(l1, r1);
	const char substr1[6]={'\002', '6', '8', '4', '2', '='};
	char const * const l2=strstr_opt(string, substr1);
	char const * const r2=std::strstr(string, "\0026842=");
	BOOST_CHECK_EQUAL(l2, r2);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(memcpy)

/// Create an array that is guaranteed to be mis-aligned.
/**
	Now ASAN reports mis-aligned accesses, [1], which could occur using MOV instructions [2] as in 64-bit mode alignment checking of ring 3 can be enabled. But on Intel & AMD mis-aligned access to memory-operands is not an issue in ring 3, so ubsan is conservative (obeying the Standard).

	[1] <a href="https://developers.redhat.com/blog/2014/10/16/gcc-undefined-behavior-sanitizer-ubsan/"/>
	[2] <a href="https://www.felixcloutier.com/x86/mov"/>
*/
template<std::size_t Sz>
struct [[gnu::packed]] alignas(std::uint64_t) misalign_array {
	char misalign;
	char str[Sz];
};

BOOST_AUTO_TEST_CASE(memcpy_1_byte) {
	alignas(std::uint64_t) const char src[1]={'f'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_2_bytes_aligned) {
	alignas(std::uint64_t) const char src[2]={'f', 'u'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_2_bytes_misaligned) {
	misalign_array<2> src={'\0', {'t', 'h'}};
	misalign_array<2> dest={'\0', {'\0', '\0'}};
	memcpy_opt(src.str, dest.str);
	BOOST_CHECK_EQUAL(std::string(src.str, sizeof(src.str)), std::string(dest.str, sizeof(dest.str)));
}

BOOST_AUTO_TEST_CASE(memcpy_3_bytes) {
	alignas(std::uint64_t) const char src[3]={'f', 'u', 'b'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_4_bytes_aligned) {
	alignas(std::uint64_t) const char src[4]={'f', 'u', 'b', 'a'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_4_bytes_misaligned) {
	misalign_array<4> src={'\0', {'t', 'h', 'e', 'q'}};
	misalign_array<4> dest={'\0', {'\0', '\0', '\0', '\0'}};
	memcpy_opt(src.str, dest.str);
	BOOST_CHECK_EQUAL(std::string(src.str, sizeof(src.str)), std::string(dest.str, sizeof(dest.str)));
}

BOOST_AUTO_TEST_CASE(memcpy_5_bytes) {
	alignas(std::uint64_t) const char src[5]={'f', 'u', 'b', 'a', 'r'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_6_bytes) {
	alignas(std::uint64_t) const char src[6]={'t', 'h', 'e', 'q', 'u', 'i'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_7_bytes) {
	alignas(std::uint64_t) const char src[7]={'t', 'h', 'e', 'q', 'u', 'i', 'c'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_8_bytes_aligned) {
	alignas(std::uint64_t) const char src[8]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_8_bytes_misaligned) {
	misalign_array<8> src={'\0', {'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k'}};
	misalign_array<8> dest={'\0', {'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'}};
	memcpy_opt(src.str, dest.str);
	BOOST_CHECK_EQUAL(std::string(src.str, sizeof(src.str)), std::string(dest.str, sizeof(dest.str)));
}

BOOST_AUTO_TEST_CASE(memcpy_9_bytes) {
	alignas(std::uint64_t) const char src[9]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_10_bytes) {
	alignas(std::uint64_t) const char src[10]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_11_bytes) {
	alignas(std::uint64_t) const char src[11]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_12_bytes) {
	alignas(std::uint64_t) const char src[12]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_13_bytes) {
	alignas(std::uint64_t) const char src[13]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_14_bytes) {
	alignas(std::uint64_t) const char src[14]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_15_bytes) {
	alignas(std::uint64_t) const char src[15]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_CASE(memcpy_16_bytes) {
	alignas(std::uint64_t) const char src[16]={'t', 'h', 'e', 'q', 'u', 'i', 'c', 'k', 'b', 'r', 'o', 'w', 'n', 'f', 'o', 'x'};
	alignas(std::uint64_t) char dest[sizeof(src)]={'\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0', '\0'};
	memcpy_opt(src, dest);
	BOOST_CHECK_EQUAL(std::string(src, sizeof(src)), std::string(dest, sizeof(dest)));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
