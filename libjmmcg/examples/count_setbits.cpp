/******************************************************************************
** Copyright © 2012 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/count_setbits.hpp"
#include "core/ave_deviation_meter.hpp"
#include "core/stats_output.hpp"

#include <boost/mpl/int.hpp>

#include <chrono>
#include <random>

using namespace libjmmcg;

using timed_results_t=ave_deviation_meter<unsigned long long>;

typedef boost::mpl::list<
	std::pair<mpl::count_setbits<0>, boost::mpl::int_<0>>,
	std::pair<mpl::count_setbits<1>, boost::mpl::int_<1>>,
	std::pair<mpl::count_setbits<2>, boost::mpl::int_<1>>,
	std::pair<mpl::count_setbits<3>, boost::mpl::int_<2>>,
	std::pair<mpl::count_setbits<4>, boost::mpl::int_<1>>,
	std::pair<mpl::count_setbits<5>, boost::mpl::int_<2>>,
	std::pair<mpl::count_setbits<6>, boost::mpl::int_<2>>,
	std::pair<mpl::count_setbits<7>, boost::mpl::int_<3>>
> check_mpl_setbits_tests;

typedef boost::mpl::list<
	dyn::basic::count_setbits,
	dyn::builtin::count_setbits,
	dyn::lookup::count_setbits<8>,
	dyn::lookup::count_setbits<16>,
	dyn::lookup::count_setbits<32>,
	dyn::lookup::count_setbits<64>,
	dyn::unroll::count_setbits
> check_dyn_setbits_tests;

BOOST_AUTO_TEST_SUITE(count_setbits_tests)

BOOST_AUTO_TEST_SUITE(mpl)

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value, setbits_t, check_mpl_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::first_type::value, setbits_t::second_type::value);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(dyn)

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_0, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(0), 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_1, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(1), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_2, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(2), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_3, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(3), 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_4, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(4), 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_5, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(5), 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_6, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(6), 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_7, setbits_t, check_dyn_setbits_tests) {
	BOOST_CHECK_EQUAL(setbits_t::result(7), 3);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_value_1023, setbits_t, check_dyn_setbits_tests) {
	std::cout<<"Efficiency="<<setbits_t::efficiency()<<std::endl;
	BOOST_CHECK_EQUAL(setbits_t::result(1023), 10);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("count_setbits.csv"))

/**
	\test <a href="./examples/count_setbits.svg">Graph</a> of performance results for the various count-setbits functions.
			==========================================================================================
	Computing the number of bits set in 2<<21 randomly-generated numbers:
	-# Build 1240: g++v4.5.2:
		- dyn::basic::count_setbits = 1.41313e+07 bit counts/sec
		- dyn::lookup::count_setbits, 8-bit cache = 6.68295e+07 bit counts/sec
		- dyn::unroll::count_setbits = 8.12703e+06 bit counts/sec
	-# Build 1241: g++v4.7.3:
		- dyn::basic::count_setbits = 3.66071e+07 bit counts/sec
		- dyn::lookup::count_setbits, 8-bit cache = 8.60793e+07 bit counts/sec.
		- dyn::lookup::count_setbits, 16-bit cache = 1.09846e+08 bit counts/sec
		- dyn::lookup::count_setbits, 32-bit cache = 1.09919e+08 bit counts/sec
		- dyn::lookup::count_setbits, 64-bit cache = 1.49271e+08 bit counts/sec
		- dyn::unroll::count_setbits = 2.59625e+07 bit counts/sec
	-# Build 1627: g++v4.8.4:
		- dyn::basic::count_setbits bit counts/sec = [16572381, 16907215 ~(+/-4%), 16969377], samples=21, total=355051515
		- dyn::builtin::count_setbits bit counts/sec = [287045168, 291680516 ~(+/-4%), 296752794], samples=22, total=6416971371
		- dyn::lookup::count_setbits, 8-bit cache bit counts/sec = [25221159, 25260814 ~(+/-4%), 25286541], samples=21, total=530477098
		- dyn::lookup::count_setbits, 16-bit cache bit counts/sec = [40636574, 40737874 ~(+/-4%), 40812929], samples=21, total=855495365
		- dyn::lookup::count_setbits, 32-bit cache bit counts/sec = [58788215, 58955639 ~(+/-4%), 59040610], samples=21, total=1238068428
		- dyn::lookup::count_setbits, 64-bit cache bit counts/sec = [71190047, 71557163 ~(+/-4%), 71684025], samples=21, total=1502700440
		- dyn::unroll::count_setbits bit counts/sec = [29459347, 29517343 ~(+/-4%), 29552128], samples=21, total=619864204
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(rate, setbits_t, check_dyn_setbits_tests) {
	typedef std::vector<typename setbits_t::element_type> vtr_colln_t;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=2<<21;
#else
	const unsigned long test_size=2<<8;
#endif
	const unsigned short loops_for_conv=50;
	const double perc_conv_estimate=5.0;
	typename setbits_t::element_type total=0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&total]() {
			vtr_colln_t v;
			v.reserve(test_size);
			std::mt19937_64 gen(42);
			std::uniform_int_distribution<typename vtr_colln_t::value_type> distribution;
			std::generate_n(std::back_inserter(v), test_size, std::bind(distribution, gen));
			const auto t1=std::chrono::high_resolution_clock::now();
			for (auto const &i : v) {
				total+=setbits_t::result(i);
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(test_size)*1000000.0/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()));
		}
	));
	std::cout<<typeid(setbits_t).name()<<" counts/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
