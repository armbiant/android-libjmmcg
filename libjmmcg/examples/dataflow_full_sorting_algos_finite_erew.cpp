/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_master.hpp"
#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

using namespace libjmmcg;
using namespace ppd;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_normal_fifo_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct erew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename erew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type erew_priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

typedef boost::mpl::list<
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,

	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	// TODO	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,

	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>,
	erew_priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2> >
	finite_test_types;

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

template<typename T>
struct square {
	static T last;

	void operator()(T t) {
		last+= t;
	};
};

template<typename T>
T square<T>::last;

inline int
sqr() {
	static int last= 0;
	return last++ << 1;
}

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable_dataflow)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(empty_colln)

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.max_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, std::numeric_limits<typename vtr_colln_t::value_type>::min());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.max_element(v);
	BOOST_CHECK_EQUAL(*context, std::numeric_limits<typename vtr_colln_t::value_type>::min());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.min_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, std::numeric_limits<typename vtr_colln_t::value_type>::max());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	auto const& context= pool << joinable() << pool.min_element(v);
	BOOST_CHECK_EQUAL(*context, std::numeric_limits<typename vtr_colln_t::value_type>::max());
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out, std::less<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(v1.empty(), true);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()), std::less<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(v1.empty(), true);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK(v == v_out);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()));
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v, std::greater<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::sort(v_chk.colln().begin(), v_chk.colln().end(), std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.empty(), true);
	BOOST_CHECK_EQUAL(pool.min_time(context), 0U);
	BOOST_CHECK_EQUAL(pool.min_processors(context), 0U);
	std::sort(v_chk.colln().begin(), v_chk.colln().end());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(nonempty_colln)

BOOST_AUTO_TEST_SUITE(one_element)

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.max_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.max_element(v);
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.min_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	auto const& context= pool << joinable() << pool.min_element(v);
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v1.push_back(1);
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out, std::less<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v1.size(), 1U);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK_EQUAL(v_out[0], v[0]);
	BOOST_CHECK_EQUAL(v_out[1], v1[0]);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()), std::less<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v1.push_back(1);
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v1.size(), 1U);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK_EQUAL(v_out[0], v[0]);
	BOOST_CHECK_EQUAL(v_out[1], v1[0]);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()));
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v, std::greater<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v[0], 1);
	std::sort(v_chk.colln().begin(), v_chk.colln().end(), std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 1U);
	BOOST_CHECK_EQUAL(v[0], 1);
	std::sort(v_chk.colln().begin(), v_chk.colln().end());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(two_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.max_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.max_element(v);
	BOOST_CHECK_EQUAL(*context, 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.min_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, 2);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	auto const& context= pool << joinable() << pool.min_element(v);
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v.push_back(2);
	v1.push_back(1);
	v1.push_back(2);
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out, std::less<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v1.size(), 2U);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK_EQUAL(v_out[0], v[0]);
	BOOST_CHECK_EQUAL(v_out[1], v1[0]);
	BOOST_CHECK_EQUAL(v_out[2], v[1]);
	BOOST_CHECK_EQUAL(v_out[3], v1[1]);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()), std::less<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v.push_back(2);
	v1.push_back(1);
	v1.push_back(2);
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v1.size(), 2U);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK_EQUAL(v_out[0], v[0]);
	BOOST_CHECK_EQUAL(v_out[1], v1[0]);
	BOOST_CHECK_EQUAL(v_out[2], v[1]);
	BOOST_CHECK_EQUAL(v_out[3], v1[1]);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()));
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_op_sorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v, std::greater<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v[0], 2);
	BOOST_CHECK_EQUAL(v[1], 1);
	std::sort(v_chk.colln().begin(), v_chk.colln().end(), std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_op_unsorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(2);
	v.push_back(1);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v, std::greater<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v[0], 2);
	BOOST_CHECK_EQUAL(v[1], 1);
	std::sort(v_chk.colln().begin(), v_chk.colln().end(), std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_sorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v[0], 1);
	BOOST_CHECK_EQUAL(v[1], 2);
	std::sort(v_chk.colln().begin(), v_chk.colln().end());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_unsorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(2);
	v.push_back(1);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 2U);
	BOOST_CHECK_EQUAL(v[0], 1);
	BOOST_CHECK_EQUAL(v[1], 2);
	std::sort(v_chk.colln().begin(), v_chk.colln().end());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(n_elements)

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.max_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(max_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.max_element(v);
	BOOST_CHECK_EQUAL(*context, 8);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.min_element(v, std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK_EQUAL(*context, 8);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(min_element, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	auto const& context= pool << joinable() << pool.min_element(v);
	BOOST_CHECK_EQUAL(*context, 1);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge_op, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v.push_back(3);
	v.push_back(5);
	v.push_back(7);
	v.push_back(9);
	v.push_back(11);
	v.push_back(13);
	v.push_back(15);
	v1.push_back(2);
	v1.push_back(4);
	v1.push_back(6);
	v1.push_back(8);
	v1.push_back(10);
	v1.push_back(12);
	v1.push_back(14);
	v1.push_back(16);
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out, std::less<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 8U);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK_EQUAL(v_out[0], v[0]);
	BOOST_CHECK_EQUAL(v_out[1], v1[0]);
	BOOST_CHECK_EQUAL(v_out[2], v[1]);
	BOOST_CHECK_EQUAL(v_out[3], v1[1]);
	BOOST_CHECK_EQUAL(v_out[4], v[2]);
	BOOST_CHECK_EQUAL(v_out[5], v1[2]);
	BOOST_CHECK_EQUAL(v_out[6], v[3]);
	BOOST_CHECK_EQUAL(v_out[7], v1[3]);
	BOOST_CHECK_EQUAL(v_out[8], v[4]);
	BOOST_CHECK_EQUAL(v_out[9], v1[4]);
	BOOST_CHECK_EQUAL(v_out[10], v[5]);
	BOOST_CHECK_EQUAL(v_out[11], v1[5]);
	BOOST_CHECK_EQUAL(v_out[12], v[6]);
	BOOST_CHECK_EQUAL(v_out[13], v1[6]);
	BOOST_CHECK_EQUAL(v_out[14], v[7]);
	BOOST_CHECK_EQUAL(v_out[15], v1[7]);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()), std::less<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(merge, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v, v1, v_out;
	v.push_back(1);
	v.push_back(3);
	v.push_back(5);
	v.push_back(7);
	v.push_back(9);
	v.push_back(11);
	v.push_back(13);
	v.push_back(15);
	v1.push_back(2);
	v1.push_back(4);
	v1.push_back(6);
	v1.push_back(8);
	v1.push_back(10);
	v1.push_back(12);
	v1.push_back(14);
	v1.push_back(16);
	auto const& context= pool << joinable() << pool.merge(v, v1, v_out);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v1.size(), 8U);
	BOOST_CHECK_EQUAL(v_out.size(), v.size() + v1.size());
	BOOST_CHECK_EQUAL(v_out[0], v[0]);
	BOOST_CHECK_EQUAL(v_out[1], v1[0]);
	BOOST_CHECK_EQUAL(v_out[2], v[1]);
	BOOST_CHECK_EQUAL(v_out[3], v1[1]);
	BOOST_CHECK_EQUAL(v_out[4], v[2]);
	BOOST_CHECK_EQUAL(v_out[5], v1[2]);
	BOOST_CHECK_EQUAL(v_out[6], v[3]);
	BOOST_CHECK_EQUAL(v_out[7], v1[3]);
	BOOST_CHECK_EQUAL(v_out[8], v[4]);
	BOOST_CHECK_EQUAL(v_out[9], v1[4]);
	BOOST_CHECK_EQUAL(v_out[10], v[5]);
	BOOST_CHECK_EQUAL(v_out[11], v1[5]);
	BOOST_CHECK_EQUAL(v_out[12], v[6]);
	BOOST_CHECK_EQUAL(v_out[13], v1[6]);
	BOOST_CHECK_EQUAL(v_out[14], v[7]);
	BOOST_CHECK_EQUAL(v_out[15], v1[7]);
	vtr_colln_t v_chk;
	std::merge(v.colln().begin(), v.colln().end(), v1.colln().begin(), v1.colln().end(), std::back_inserter(v_chk.colln()));
	BOOST_CHECK(v_out == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_op_sorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v, std::greater<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v[0], 8);
	BOOST_CHECK_EQUAL(v[1], 7);
	BOOST_CHECK_EQUAL(v[2], 6);
	BOOST_CHECK_EQUAL(v[3], 5);
	BOOST_CHECK_EQUAL(v[4], 4);
	BOOST_CHECK_EQUAL(v[5], 3);
	BOOST_CHECK_EQUAL(v[6], 2);
	BOOST_CHECK_EQUAL(v[7], 1);
	std::sort(v_chk.colln().begin(), v_chk.colln().end(), std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_op_unsorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(8);
	v.push_back(7);
	v.push_back(6);
	v.push_back(5);
	v.push_back(4);
	v.push_back(3);
	v.push_back(2);
	v.push_back(1);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v, std::greater<typename vtr_colln_t::value_type>());
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v[0], 8);
	BOOST_CHECK_EQUAL(v[1], 7);
	BOOST_CHECK_EQUAL(v[2], 6);
	BOOST_CHECK_EQUAL(v[3], 5);
	BOOST_CHECK_EQUAL(v[4], 4);
	BOOST_CHECK_EQUAL(v[5], 3);
	BOOST_CHECK_EQUAL(v[6], 2);
	BOOST_CHECK_EQUAL(v[7], 1);
	std::sort(v_chk.colln().begin(), v_chk.colln().end(), std::greater<typename vtr_colln_t::value_type>());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_sorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	v.push_back(7);
	v.push_back(8);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v[0], 1);
	BOOST_CHECK_EQUAL(v[1], 2);
	BOOST_CHECK_EQUAL(v[2], 3);
	BOOST_CHECK_EQUAL(v[3], 4);
	BOOST_CHECK_EQUAL(v[4], 5);
	BOOST_CHECK_EQUAL(v[5], 6);
	BOOST_CHECK_EQUAL(v[6], 7);
	BOOST_CHECK_EQUAL(v[7], 8);
	std::sort(v_chk.colln().begin(), v_chk.colln().end());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(sort_unsorted, T, finite_test_types) {
	typedef typename T::vtr_colln_t vtr_colln_t;
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	pool_type pool(T::pool_size);
	vtr_colln_t v;
	v.push_back(8);
	v.push_back(7);
	v.push_back(6);
	v.push_back(5);
	v.push_back(4);
	v.push_back(3);
	v.push_back(2);
	v.push_back(1);
	vtr_colln_t v_chk(v);
	auto const& context= pool << joinable() << pool.sort(v);
	pool.min_time(vtr_colln_t::memory_access_mode);
	pool.min_processors(vtr_colln_t::memory_access_mode);
	*context;
	BOOST_CHECK_EQUAL(v.size(), 8U);
	BOOST_CHECK_EQUAL(v[0], 1);
	BOOST_CHECK_EQUAL(v[1], 2);
	BOOST_CHECK_EQUAL(v[2], 3);
	BOOST_CHECK_EQUAL(v[3], 4);
	BOOST_CHECK_EQUAL(v[4], 5);
	BOOST_CHECK_EQUAL(v[5], 6);
	BOOST_CHECK_EQUAL(v[6], 7);
	BOOST_CHECK_EQUAL(v[7], 8);
	std::sort(v_chk.colln().begin(), v_chk.colln().end());
	BOOST_CHECK(v == v_chk);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
