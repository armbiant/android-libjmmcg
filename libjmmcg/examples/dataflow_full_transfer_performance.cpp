/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

#include <boost/graph/graphviz.hpp>

#include <chrono>
#include <random>

using namespace libjmmcg;
using namespace ppd;

using timed_results_t= ave_deviation_meter<unsigned long long>;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct erew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct erew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct erew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock_traits::nonrecursive_anon_mutex_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1UL>
struct crew_normal_fifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_fifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1UL>
struct crew_normal_lifo_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1UL>
struct crew_normal_lifo_lockfree_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::normal_lifo_lockfree,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned long GSSk= 1>
struct crew_priority_queue_t {
	typedef api_lock_traits<platform_api, Mdl> lock_traits;
	typedef safe_colln<
		std::vector<long>,
		typename lock::rw::locker<lock_traits>,
		typename lock::rw::locker<lock_traits>::decaying_write_lock_type>
		vtr_colln_t;

	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk /*,
		 basic_statistics,
		 control_flow_graph*/
		>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static inline constexpr const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

typedef boost::mpl::list<
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	crew_normal_lifo_lockfree_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>,
	crew_normal_lifo_lockfree_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::thread_owns_queue<pool_traits::work_distribution_mode_t::queue_model_t::stealing_mode_t::random>>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>>
	dataflow_ps1_test_types;

typedef boost::mpl::list<
	erew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12>,
	erew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12>,
	crew_normal_fifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12>,
	crew_normal_lifo_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12> /*,
 TODO	crew_normal_lifo_lockfree_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 12>*/
	>
	dataflow_ps12_test_types;

struct trivial_work {
	constexpr trivial_work() noexcept(true) {
	}
	void process() noexcept(true) {
	}

	constexpr bool __fastcall operator<(trivial_work const&) const noexcept(true) {
		return true;
	}
};

template<class PT>
struct recusive_work {
	typedef PT pool_type;
	typedef typename pool_type::joinable joinable;

	const unsigned long depth;
	pool_type& pool;

	constexpr recusive_work(const unsigned long d, pool_type& p) noexcept(true)
		: depth(d), pool(p) {
	}
	void process() noexcept(true) {
		if(depth > 0) {
			auto const& context1= pool << joinable() << recusive_work(depth - 1, pool);
			auto const& context2= pool << joinable() << recusive_work(depth - 1, pool);
			*context1;
			*context2;
		}
	}

	constexpr bool __fastcall operator<(recusive_work const&) const noexcept(true) {
		return true;
	}
};

template<class PT>
struct pool_1ctx_work {
	typedef PT pool_type;
	using joinable= typename pool_type::joinable;

	unsigned long num_xfers;
	pool_type& pool;

	constexpr pool_1ctx_work(const unsigned long x, pool_type& p) noexcept(true)
		: num_xfers(x), pool(p) {
	}
	void process() noexcept(true) {
		while(--num_xfers) {
			auto const& context= pool << joinable() << trivial_work();
			*context;
		}
	}

	constexpr bool __fastcall operator<(pool_1ctx_work const&) const noexcept(true) {
		return true;
	}
};

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable_dataflow)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(n_elements)

/**
	\test Graphs of <a href="./examples/dataflow_full_transfer_performance.svg">vertical & horizontal</a> and <a href="./examples/dataflow_full_vertical_transfer_performance_pool_size_1.svg">vertical with different pool_types</a> dataflow (pool_size()=1) operations rate.
			=========================================
	This tests how long it takes to:
	-# Create an execution_context on the stack.
	-# Add the work to the queue.
	-# Increment and decrement the number of active work items in the queue.
	-# Have one vertical thread wake up and remove the work, i.e. the thread pool does not saturate as only the main thread produces work, and one thread in the pool consumes that work and a context-switch is involved.
	-# Signal that the work has been completed.
	-# It does not test the scalability of multiple threads contending to add work to, or remove work from, the queue.
	-# Results:
		-#	Build 1405:
			-	Pool=1, vertical dataflow transfers per second: [14063, 36066 ~(+/-9%), 287956], samples=1001, total=36102451
				i.e. @2.6GHz, [71microsec, 28microsec, 3.5microsec] per data transfer.
		-#	Build 1447:
			-	Pool=1, vertical dataflow transfers per second: [8438, 53961 ~(+/-29%), 260460], samples=1001, total=54015303
				i.e. @2.6GHz, [119microsec, 19microsec, 3.8microsec] per data transfer.
		-#	Build 1561:
			-	Pool=1, time taken: [3023, 23473 ~(+/-174%), 130458], samples=2001, total=46971159 usec.
				i.e. @2.6GHz, [130microsec, 23microsec, 3.0microsec] per data transfer.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(single_vertical_dataflow_operations_rate, T, dataflow_ps1_test_types) {
	typedef typename T::pool_type pool_type;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size= 2 << 9;
	const unsigned long num_reps= 20000;
#else
	const unsigned long test_size= 2 << 3;
	const unsigned long num_reps= 2;
#endif
	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), 1UL);	  // The pool_size() must be 1 for this test.
	std::cout << "Pool=" << pool.pool_size() << std::flush;
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		1.0,
		num_reps,
		[&pool]() {
			typedef typename pool_type::joinable joinable;

			const auto t1= std::chrono::high_resolution_clock::now();
			for(unsigned long num_loops= 0; num_loops < test_size; ++num_loops) {
				auto const& context= pool << joinable() << trivial_work();
				*context;
			}
			const auto t2= std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count());
		}));
	std::cout << ", time taken: " << timed_results.first << " usec." << std::endl;
	const double rescale= 1.0 / double(test_size * pool.pool_size());
	std::cout << "Vertical dataflow transfer took (usec): " << rescale * timed_results.first.arithmetic_mean() << std::endl;
	std::cout << pool.statistics() << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/** TODO
	\test <a href="./examples/dataflow_full_transfer_performance.svg">Graph</a> of vertical dataflow-transfers, avoiding a context switch, (pool_size()=1) operations rate.
			=========================================
	This tests how long it takes to:
	-# Create an execution_context on the stack.
	-# Add the work to the queue, and wait for it to complete.
	-# The work task is to transfer a trivial work-item to the queue and wait for it to complete. This is performed repeatedly.
	-# The concept is that the transferred work should be run by the same thread that transferred it, potentially avoiding any context switch.
	-# Results:
		-#	Build 1671:
			-	Pool=1, vertical dataflow transfers per second: TODO
				i.e. @2.6GHz, [71microsec, 28microsec, 3.5microsec] per data transfer.
*/
/* TODO
BOOST_AUTO_TEST_CASE_TEMPLATE(vertical_dataflow_1ctx_operations_rate, T, dataflow_ps1_test_types) {
	typedef typename T::pool_type pool_type;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=2<<4;
	const unsigned long num_reps=1000;
#else
	const unsigned long test_size=2<<2;
	const unsigned long num_reps=2;
#endif
	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), 1UL);	// The pool_size() must be 1 for this test.
	std::cout<<"Pool="<<pool.pool_size()<<std::flush;
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		1.0,
		num_reps,
		[test_size, &pool]() {
			typedef typename pool_type::joinable joinable;

			const auto t1=std::chrono::high_resolution_clock::now();
			using work_type=pool_1ctx_work<pool_type>;
			auto const &context=pool<<joinable()<<work_type(test_size, pool);
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count());
		}
	));
	std::cout<<", time taken: "<<timed_results.first<<" usec."<<std::endl;
	const double rescale=1.0/(test_size*pool.pool_size());
	std::cout<<"Vertical dataflow transfer took (usec): "<<rescale*timed_results.first.arithmetic_mean()<<std::endl;
	std::cout<<pool.statistics()<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}
*/
/**
	\test Graphs of <a href="./examples/dataflow_full_transfer_performance.svg">vertical & horizontal</a> and <a href="./examples/dataflow_full_vertical_transfer_performance_pool_size_12.svg">vertical with different pool_types</a> dataflow (pool_size()=12) operations rate.
			==================================
	This tests how long it takes to:
	-# Creates twelve execution_contexts on the stack, enqueuing the work.
	-# The pool of threads process the work, i.e. a context-switch is involved.
	-# This rapidly creates work, which is processed, usually using only one of the threads in the pool. With twelve threads there is a lot of contention on the queue for the work. The vast majority is executed vertically.
	-# Results:
		-#	Build 1405:
			-	Pool=12, vertical dataflow transfers per second: [681, 1223 ~(+/-0%), 1811], samples=64, total=78278
				i.e. @2.6GHz, [122microsec, 68microsec, 46microsec] per data transfer.
		-#	Build 1415:
			-	Pool=12, vertical dataflow transfers per second: [3368, 3547 ~(+/-0%), 3855], samples=181, total=642030
				i.e. @2.6GHz, [25microsec, 23microsec, 22microsec] per data transfer.
		-#	Build 1447:
			-	Pool=12, vertical dataflow transfers per second: [4242, 4622 ~(+/-0%), 5879], samples=757, total=3499459
				i.e. @2.6GHz, [20microsec, 18microsec, 14microsec] per data transfer.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(vertical_dataflow_operations_rate, T, dataflow_ps12_test_types) {
	typedef typename T::pool_type pool_type;

#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size= 2 << 10;
	const unsigned long num_reps= 1000;
#else
	const unsigned long test_size= 2 << 3;
	const unsigned long num_reps= 2;
#endif
	// This parameter is heavily dependent upon the max_stack_size of the pool_threads in the thread_pool.
	pool_type pool(T::pool_size);
	BOOST_CHECK_EQUAL(pool.pool_size(), 12UL);	// The pool_size() must be 12 for this test.
	std::cout << "Pool=" << pool.pool_size() << std::flush;
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		10.0,
		num_reps,
		[&pool]() {
			typedef typename pool_type::joinable joinable;

			const auto t1= std::chrono::high_resolution_clock::now();
			for(unsigned long num_loops= 0; num_loops < test_size; ++num_loops) {
				auto const& context1= pool << joinable() << trivial_work();
				auto const& context2= pool << joinable() << trivial_work();
				auto const& context3= pool << joinable() << trivial_work();
				auto const& context4= pool << joinable() << trivial_work();
				auto const& context5= pool << joinable() << trivial_work();
				auto const& context6= pool << joinable() << trivial_work();
				auto const& context7= pool << joinable() << trivial_work();
				auto const& context8= pool << joinable() << trivial_work();
				auto const& context9= pool << joinable() << trivial_work();
				auto const& context10= pool << joinable() << trivial_work();
				auto const& context11= pool << joinable() << trivial_work();
				auto const& context12= pool << joinable() << trivial_work();
				*context1;
				*context2;
				*context3;
				*context4;
				*context5;
				*context6;
				*context7;
				*context8;
				*context9;
				*context10;
				*context11;
				*context12;
			}
			const auto t2= std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count());
		}));
	std::cout << ", time taken: " << timed_results.first << " usec." << std::endl;
	const double rescale= 1.0 / (test_size * pool.pool_size());
	std::cout << "Vertical dataflow transfer took (usec): " << rescale * timed_results.first.arithmetic_mean() << std::endl;
	std::cout << pool.statistics() << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/dataflow_full_transfer_performance.svg">Graph</a> of tree-creation dataflow (pool_size()=12) transfers rate.
			===================================
	This tests how long it takes to:
	-# Recursively creates two execution_contexts on the stack, waiting for the tree to be created.
	-# The leaf-nodes are processed & return, the tree reduces. Related sibling-nodes are executed by the same thread, others may involve a context-switch.
	-# If the test_depth is small, e.g. 3, then eight leaf-nodes are created, for a total of 14 nodes . With a pool_size of 12, the work will be mutated vertically. A test-depth of 4 would produce 16 leaf-nodes, and a total of 30 nodes.
	-# This can rapidly flood the pool full of work, if the depth generates many more leaf-nodes than pool_threads, in which case the vast majority would be executed horizontally.
	-# The contention on the queue is extremely high from all of the spawned horizontal threads.
	-# Results:
		-#	Build 1447:
			-	Pool=12, vertical dataflow transfers per second: [8438, 53961 ~(+/-29%), 260460], samples=1001, total=54015303
				i.e. @2.6GHz, [119microsec, 19microsec, 3.8microsec] per data transfer.
		-#	Build 1561:
			-	Pool=12, time taken: [54029, 103259 ~(+/-19%), 478100], samples=2001, total=206621313 usec.
				i.e. @2.6GHz, 0.53microsec per data transfer.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(horizontal_dataflow_transfers_rate, T, dataflow_ps12_test_types) {
	typedef typename T::pool_type pool_type;

	// This parameter is heavily dependent upon the max_stack_size of the pool_threads in the thread_pool.
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size= 2 << 13;
	const unsigned long num_reps= 20000;
	const unsigned long test_depth= 3;
#else
	const unsigned long test_size= 2 << 3;
	const unsigned long num_reps= 2;
	const unsigned long test_depth= 1;
#endif
	pool_type pool(T::pool_size);
	std::cout << "Pool=" << pool.pool_size() << std::flush;
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		5.0,
		num_reps,
		[&pool]() {
			typedef typename pool_type::joinable joinable;
			typedef recusive_work<pool_type> work_type;

			const unsigned long test_size= 1 << 6;
			const auto t1= std::chrono::high_resolution_clock::now();
			for(unsigned long num_loops= 0; num_loops < test_size; ++num_loops) {
				auto const& context= pool << joinable() << work_type(test_depth, pool);
				*context;
			}
			const auto t2= std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count());
		}));
	std::cout << ", time taken: " << timed_results.first << " usec." << std::endl;
	const double rescale= 1.0 / (test_size * pool.pool_size());
	std::cout << "Tree-constructions transfer took (usec): " << rescale * timed_results.first.arithmetic_mean() << std::endl;
	std::cout << pool.statistics() << std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
