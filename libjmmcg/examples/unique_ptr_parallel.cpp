/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/ave_deviation_meter.hpp"
#include "core/stats_output.hpp"
#include "core/thread_wrapper.hpp"
#include "core/unique_ptr.hpp"

#include <boost/move/unique_ptr.hpp>

#include <chrono>

using namespace libjmmcg;
using namespace ppd;

template<class Mdl>
struct obj {
	using lock_traits=api_lock_traits<platform_api, Mdl>;
	using element_type=int;
	using deleter_t=default_delete<obj>;

	element_type const init;

	obj(element_type i) noexcept(true) : init(i) {}
	~obj() noexcept(true) {}

	void deleter() {
		deleter_t().operator()(this);
	}
};

using ptr_types=boost::mpl::list<
	std::unique_ptr<obj<heavyweight_threading>>,
	boost::movelib::unique_ptr<obj<heavyweight_threading>>,
	unique_ptr<obj<sequential_mode>, api_lock_traits<platform_api, sequential_mode>>,
	unique_ptr<obj<heavyweight_threading>, api_lock_traits<platform_api, heavyweight_threading>>
>;
using timed_results_t=ave_deviation_meter<unsigned long long>;

template<class Element>
struct cctor_work {
	typedef std::vector<Element> cont_t;
	struct make {
		typename cont_t::value_type::element_type::element_type i;

		make() noexcept(true) : i() {}

		typename cont_t::value_type operator ()() {
			return typename cont_t::value_type(new typename cont_t::value_type::element_type(++i));
		}
	};

	cont_t &cont;

	explicit  __stdcall cctor_work(cont_t &c) noexcept(true)
	: cont(c) {
	}

	void operator()(std::atomic_flag &exit_requested) {
		while (!cont.empty()) {
			const typename cont_t::value_type tmp(std::move(cont.back()));
			cont.pop_back();
		}
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<class Element>
using cctor_thread=ppd::wrapper<cctor_work<Element>, ppd::platform_api, heavyweight_threading>;

template<class Element>
struct dtor_work final {
	typedef std::vector<Element> cont_t;
	struct make {
		typename cont_t::value_type::element_type::element_type i;

		make() noexcept(true) : i() {}

		typename cont_t::value_type operator ()() {
			return typename cont_t::value_type(new typename cont_t::value_type::element_type(++i));
		}
	};

	cont_t &cont;

	explicit  __stdcall dtor_work(cont_t &c) noexcept(true)
	: cont(c) {
	}

	void operator()(std::atomic_flag &exit_requested) {
		while (!cont.empty()) {
			cont.back().reset();
			cont.pop_back();
		}
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<class Element>
using dtor_thread=ppd::wrapper<dtor_work<Element>, ppd::platform_api, heavyweight_threading>;

BOOST_AUTO_TEST_SUITE(performance_cctor, *stats_to_csv::make_fixture("unique_ptr_parallel_cctor.csv"))

BOOST_AUTO_TEST_SUITE(unique_ptr_parallel_tests)

/**
	\test <a href="./examples/unique_ptr_parallel_cctor.svg">Graph</a> of performance results for parallel unique_ptr cctors.
			==========================================================================================
	Results for 100000000 items:
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(parallel_cctor, ptr_t, ptr_types) {
	typedef cctor_thread<ptr_t> thread_t;
#ifdef JMMCG_PERFORMANCE_TESTS
	const std::size_t num_items=100000000;
#else
	const std::size_t num_items=100;
#endif
	const unsigned short loops_for_conv=50;
	const double perc_conv_estimate=5.0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			typename thread_t::mutator_t::cont_t c;
			std::generate_n(std::back_inserter(c), num_items, typename thread_t::mutator_t::make());
			thread_t th1(c);
			thread_t th2(c);
			c.clear();
			const auto t1=std::chrono::high_resolution_clock::now();
			th1.create_running();
			th2.create_running();
			do {
				api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
			} while (th1.is_running() || th2.is_running());
			const auto t2=std::chrono::high_resolution_clock::now();
			BOOST_CHECK(th1->cont.empty());
			BOOST_CHECK(th2->cont.empty());
			BOOST_CHECK(c.empty());
			return timed_results_t::value_type(num_items/(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count())/1000000));
		}
	));
	std::cout<<boost::core::demangle(typeid(ptr_t).name())<<" rate cctors/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(performance_cctor, *stats_to_csv::make_fixture("unique_ptr_parallel_dtor.csv"))

/**
	\test <a href="./examples/unique_ptr_parallel_dtor.svg">Graph</a> of performance results for parallel unique_ptr dtors.
			==========================================================================================
	Results for 100000000 items:
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(parallel_deletes, ptr_t, ptr_types) {
	typedef dtor_thread<ptr_t> thread_t;
#ifdef JMMCG_PERFORMANCE_TESTS
	const std::size_t num_items=100000000;
#else
	const std::size_t num_items=100;
#endif
	const unsigned short loops_for_conv=50;
	const double perc_conv_estimate=5.0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			typename thread_t::mutator_t::cont_t c;
			std::generate_n(std::back_inserter(c), num_items, typename thread_t::mutator_t::make());
			thread_t th1(c);
			thread_t th2(c);
			c.clear();
			const auto t1=std::chrono::high_resolution_clock::now();
			th1.create_running();
			th2.create_running();
			do {
				api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
			} while (th1.is_running() || th2.is_running());
			const auto t2=std::chrono::high_resolution_clock::now();
			BOOST_CHECK(th1->cont.empty());
			BOOST_CHECK(th2->cont.empty());
			BOOST_CHECK(c.empty());
			return timed_results_t::value_type(num_items/(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count())/1000000));
		}
	));
	std::cout<<boost::core::demangle(typeid(ptr_t).name())<<" rate dtors/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
