/******************************************************************************
** Copyright © 2010 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_api_traits.hpp"

using namespace libjmmcg;

typedef boost::mpl::list<ppd::heavyweight_threading, ppd::sequential_mode> thread_types;

BOOST_AUTO_TEST_SUITE(hp_timers)

BOOST_AUTO_TEST_CASE(check_cpu_frequency) {
	BOOST_CHECK_GT(cpu_timer::private_::start_ticks, 0);
	BOOST_CHECK_GT(cpu_timer::private_::start_UTC.time_of_day().total_milliseconds(), 0);
	BOOST_CHECK_GT(cpu_timer::private_::ticks_per_microsec.min, 0);
	BOOST_CHECK_GT(cpu_timer::private_::ticks_per_microsec.mean, cpu_timer::private_::ticks_per_microsec.min);
	BOOST_CHECK_GT(cpu_timer::private_::ticks_per_microsec.max, cpu_timer::private_::ticks_per_microsec.mean);
	BOOST_CHECK_GE(cpu_timer::private_::ticks_per_microsec.mean_average_dev, 0);
	BOOST_CHECK_LT(cpu_timer::private_::ticks_per_microsec.mean_average_dev, 1);
	BOOST_CHECK_GT(cpu_timer::out_of_order::now(), 0);
	BOOST_CHECK_GT(cpu_timer::in_order::now(), 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(create_timer, Mdl, thread_types) {
	typedef hp_timer<ppd::platform_api,Mdl> hp_timer_t;

	hp_timer_t t;
	BOOST_CHECK_GT(t.start_up_count, 0);
	BOOST_CHECK_GT(t.to_usec(t.start_up_time), 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_timer_counter_counts, Mdl, thread_types) {
	typedef hp_timer<ppd::platform_api,Mdl> hp_timer_t;

	hp_timer_t t;
	BOOST_CHECK_GE(t.current_count(), t.start_up_count);
	BOOST_CHECK_GE(t.current_count(), t.start_up_count);
	BOOST_CHECK_GE(t.current_count(), t.start_up_count);
	BOOST_CHECK_GE(t.current_count(), t.start_up_count);
	BOOST_CHECK_GE(t.current_count(), t.start_up_count);
	BOOST_CHECK_GE(t.current_count(), t.start_up_count);
	BOOST_CHECK_GE(t.current_count(), t.start_up_count);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_timer_usec_simply_increments, Mdl, thread_types) {
	typedef hp_timer<ppd::platform_api,Mdl> hp_timer_t;

	hp_timer_t t;
	BOOST_CHECK_GE(t.to_usec(t.current_time()), t.start_up_count);
	BOOST_CHECK_GE(t.to_usec(t.current_time()), t.start_up_count);
	BOOST_CHECK_GE(t.to_usec(t.current_time()), t.start_up_count);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(check_timer_usec_increments, Mdl, thread_types) {
	typedef hp_timer<ppd::platform_api,Mdl> hp_timer_t;

	hp_timer_t t;
	const typename hp_timer_t::value_type t1=t.to_usec(t.current_time());
	const typename hp_timer_t::value_type t2=t.to_usec(t.current_time());
	BOOST_CHECK_GE(t2, t1);
	const typename hp_timer_t::value_type t3=t.to_usec(t.current_time());
	BOOST_CHECK_GE(t3, t2);
	BOOST_CHECK_GT(t3, t1);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(hp_intervals)

BOOST_AUTO_TEST_CASE_TEMPLATE(create_interval, Mdl, thread_types) {
	typedef hp_timer<ppd::platform_api,Mdl> hp_timer_t;
	typedef hp_interval<hp_timer_t> hp_interval_t;

	hp_timer_t timer;
	typename hp_timer_t::time_utc_t interval;
	hp_interval_t measure(timer,interval);
	BOOST_CHECK_GE(measure.timer.to_usec(measure.start), measure.timer.start_up_count);
}

BOOST_AUTO_TEST_CASE(measure_one_sec_thread_interval) {
	typedef ppd::api_threading_traits<ppd::platform_api,ppd::heavyweight_threading> thread_traits;
	typedef hp_timer<ppd::platform_api,ppd::heavyweight_threading> hp_timer_t;
	typedef hp_interval<hp_timer_t> hp_interval_t;

	const thread_traits::api_params_type::suspend_period_ms one_sec=1000;
	hp_timer_t timer;
	hp_timer_t::time_utc_t interval;
	{
		hp_interval_t measure(timer,interval);
		thread_traits::sleep(one_sec);
	}
	BOOST_CHECK_GT(timer.to_usec(interval), static_cast<hp_timer_t::value_type>(one_sec*0.9*1000));
	BOOST_CHECK_LT(timer.to_usec(interval), static_cast<hp_timer_t::value_type>(one_sec*2*1000));
}

BOOST_AUTO_TEST_CASE(measure_one_sec_seq_interval) {
	typedef ppd::api_threading_traits<ppd::platform_api,ppd::sequential_mode> thread_traits;
	typedef hp_timer<ppd::platform_api,ppd::sequential_mode> hp_timer_t;
	typedef hp_interval<hp_timer_t> hp_interval_t;

	const thread_traits::api_params_type::suspend_period_ms one_sec=1000;
	hp_timer_t timer;
	hp_timer_t::time_utc_t interval;
	{
		const hp_interval_t measure(timer, interval);
		thread_traits::sleep(one_sec);
	}
	BOOST_CHECK_GE(timer.to_usec(interval), 0);
}

BOOST_AUTO_TEST_CASE(measure_one_sec_pause) {
	using hp_timer_t=hp_timer<ppd::platform_api,ppd::sequential_mode>;
	using hp_interval_t=hp_interval<hp_timer_t>;

	const double one_sec=1000000;	// In usec.
	hp_timer_t timer;
	hp_timer_t::time_utc_t interval;
	{
		const hp_interval_t measure(timer, interval);
		const auto paused=cpu_timer::pause_for_usec(one_sec);
		BOOST_CHECK_GE(paused, one_sec);
	}
	BOOST_CHECK_GE(timer.to_usec(interval), 0.9*one_sec);
}

BOOST_AUTO_TEST_SUITE_END()
