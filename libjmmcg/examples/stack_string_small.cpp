/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include "core/stack_string.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(string_tests)

BOOST_AUTO_TEST_SUITE(small_string)

BOOST_AUTO_TEST_CASE(ctor)
{
	char const src[]="sm";
	BOOST_CHECK_LT(sizeof(src)-1, stack_string::small_string_max_size);
	stack_string s(src);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_GE(s.capacity(), s.size());
}

BOOST_AUTO_TEST_CASE(ctor_cctor)
{
	char const src[]="sm";
	stack_string s1(src);
	stack_string s2(s1);
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), s2.begin()));
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(ctor_move_ctor)
{
	char const src[]="sm";
	stack_string s1(src);
	stack_string s2(std::move(s1));
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src));
	BOOST_CHECK_EQUAL(s1.capacity(), stack_string::size_type());
	BOOST_CHECK_GE(s2.capacity(), s2.size());
}

BOOST_AUTO_TEST_CASE(index)
{
	char const src[]="sm";
	stack_string s(src);
	BOOST_CHECK_EQUAL(s[0], 's');
	BOOST_CHECK_EQUAL(s[1], 'm');
}

BOOST_AUTO_TEST_CASE(default_ctor_swap)
{
	char const src[]="sm";
	stack_string s1(src);
	stack_string s2;
	s1.swap(s2);
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src));
	BOOST_CHECK_EQUAL(s1.capacity(), stack_string::size_type());
	BOOST_CHECK_GE(s2.capacity(), s2.size());
}

BOOST_AUTO_TEST_CASE(ctor_swap)
{
	char const src1[]="sm";
	char const src2[]="ms";
	stack_string s1(src1);
	stack_string s2(src2);
	s1.swap(s2);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src2)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src2));
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src1)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src1));
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(cctor_swap)
{
	char const src[]="sm";
	stack_string s1(src);
	stack_string s2(s1);
	s1.swap(s2);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src));
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src));
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(ctor_assignment)
{
	char const src[]="sm";
	stack_string s1(src);
	stack_string s2;
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(two_ctor_assignment)
{
	char const src1[]="sm";
	char const src2[]="ms";
	stack_string s1(src1);
	stack_string s2(src2);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), s2.begin()));
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(ctor_move_assignment)
{
	char const src[]="sm";
	stack_string s1(src);
	stack_string s2;
	s2=std::move(s1);
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(s1.capacity(), s1.size());
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src));
	BOOST_CHECK_GE(s2.capacity(), s2.size());
}

BOOST_AUTO_TEST_CASE(two_ctor_move_assignment)
{
	char const src1[]="sm";
	char const src2[]="ms";
	stack_string s1(src1);
	stack_string s2(src2);
	s2=std::move(s1);
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src1)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src1));
	BOOST_CHECK_EQUAL(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(self_assignment)
{
	char const src[]="sm";
	stack_string s(src);
	s=s;
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_GE(s.capacity(), s.size());
}

BOOST_AUTO_TEST_CASE(self_move_assignment)
{
	char const src[]="sm";
	stack_string s(src);
	s=std::move(s);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_GE(s.capacity(), s.size());
}

BOOST_AUTO_TEST_CASE(equality)
{
	char const src[]="sm";
	stack_string s1(src), s2(src);
	BOOST_CHECK(s1==s2);
}

BOOST_AUTO_TEST_CASE(inequality)
{
	char const src1[]="sm";
	char const src2[]="ms";
	stack_string s1(src1), s2(src2);
	BOOST_CHECK(s1!=s2);
}

BOOST_AUTO_TEST_CASE(inequality_sizes1)
{
	char const src1[]="sma";
	char const src2[]="ms";
	stack_string s1(src1), s2(src2);
	BOOST_CHECK(s1!=s2);
}

BOOST_AUTO_TEST_CASE(inequality_sizes2)
{
	char const src1[]="sm";
	char const src2[]="ams";
	stack_string s1(src1), s2(src2);
	BOOST_CHECK(s1!=s2);
}

BOOST_AUTO_TEST_CASE(clear)
{
	char const src[]="sm";
	stack_string s(src);
	s.clear();
	BOOST_CHECK(s.empty());
	BOOST_CHECK_EQUAL(s.size(), stack_string::size_type());
	BOOST_CHECK_GE(s.capacity(), sizeof(src)-1);
}

BOOST_AUTO_TEST_CASE(reserve_smaller)
{
	const stack_string::size_type res(1);
	char const src[]="sm";
	stack_string s(src);
	s.reserve(res);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(reserve_bigger)
{
	char const src[]="sm";
	const stack_string::size_type res(sizeof(src)+1);
	stack_string s(src);
	s.reserve(res);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK_EQUAL(s.capacity(), res);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(rereserve_bigger)
{
	char const src[]="sm";
	const stack_string::size_type res1(sizeof(src)+1);
	const stack_string::size_type res2(sizeof(src)+2);
	stack_string s(src);
	s.reserve(res1);
	s.reserve(res2);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK_EQUAL(s.capacity(), res2);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(rereserve_smaller)
{
	char const src[]="sm";
	const stack_string::size_type res1(sizeof(src)+1);
	const stack_string::size_type res2(sizeof(src)-2);
	stack_string s(src);
	s.reserve(res1);
	s.reserve(res2);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK_EQUAL(s.capacity(), res1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_smaller)
{
	char const src[]="sm";
	const stack_string::size_type res(1);
	stack_string s(src);
	s.resize(res);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_bigger)
{
	char const src[]="sm";
	const stack_string::size_type res(sizeof(src)+1);
	BOOST_CHECK_LT(res, stack_string::small_string_max_size);
	stack_string s(src);
	s.resize(res);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(src, src+strlen(src)+1, s.begin()));
}

BOOST_AUTO_TEST_CASE(reresize_bigger)
{
	char const src[]="s";
	const stack_string::size_type res1(sizeof(src)+1);
	const stack_string::size_type res2(res1+1);
	BOOST_CHECK_LT(res1, stack_string::small_string_max_size);
	BOOST_CHECK_LT(res2, stack_string::small_string_max_size);
	stack_string s(src);
	s.resize(res1);
	s.resize(res2);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.begin()+sizeof(src), src));
}

BOOST_AUTO_TEST_CASE(reresize_smaller)
{
	char const src[]="sma";
	const stack_string::size_type res1(sizeof(src)+1);
	const stack_string::size_type res2(res1-1);
	BOOST_CHECK_LT(res1, stack_string::small_string_max_size);
	BOOST_CHECK_LT(res2, stack_string::small_string_max_size);
	stack_string s(src);
	s.resize(res1);
	s.resize(res2);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), res1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_smaller_init)
{
	const stack_string::value_type fill='X';
	char const src[]="sm";
	const stack_string::size_type res(1);
	BOOST_CHECK_LT(res, stack_string::small_string_max_size);
	stack_string s(src);
	s.resize(res, fill);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), sizeof(src)-1);
	BOOST_CHECK_EQUAL(s[0], src[0]);
}

BOOST_AUTO_TEST_CASE(resize_bigger_init)
{
	const stack_string::value_type fill='X';
	char const src[]="sm";
	char const result[]="smXX";
	const stack_string::size_type res(sizeof(src)+1);
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	BOOST_CHECK_LT(res, stack_string::small_string_max_size);
	stack_string s(src);
	s.resize(res, fill);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(reresize_bigger_init)
{
	const stack_string::value_type fill='X';
	char const src[]="s";
	char const result[]="sXXX";
	const stack_string::size_type res1(sizeof(src)+1);
	const stack_string::size_type res2(res1+1);
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	BOOST_CHECK_LT(res1, stack_string::small_string_max_size);
	BOOST_CHECK_LT(res2, stack_string::small_string_max_size);
	stack_string s(src);
	s.resize(res1, fill);
	s.resize(res2, fill);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(reresize_smaller_init)
{
	const stack_string::value_type fill='X';
	char const src[]="sm";
	char const result[]="smX";
	const stack_string::size_type res1(sizeof(src)+1);
	const stack_string::size_type res2(res1-1);
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	BOOST_CHECK_LT(res1, stack_string::small_string_max_size);
	BOOST_CHECK_LT(res2, stack_string::small_string_max_size);
	stack_string s(src);
	s.resize(res1, fill);
	s.resize(res2, fill);
	BOOST_CHECK_EQUAL(s.size(), res2);
	BOOST_CHECK_GE(s.capacity(), res1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(push_back)
{
	const stack_string::value_type fill='X';
	char const src[]="sm";
	char const result[]="smX";
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	stack_string s(src);
	s.push_back(fill);
	BOOST_CHECK_EQUAL(s.size(), sizeof(src));
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(insert_str)
{
	const stack_string src("sm");
	stack_string s;
	const stack_string::iterator i=s.insert(s.end(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), src.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), src.begin()));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(insert_str_end)
{
	const stack_string src("sm");
	const char result[]="smsm";
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	stack_string s(src);
	const stack_string::iterator i=s.insert(s.end(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(result, result+sizeof(result), s.begin()));
	BOOST_CHECK_EQUAL(i, s.begin()+src.size());
}

BOOST_AUTO_TEST_CASE(insert_str_begin)
{
	const stack_string src("sm");
	const char result[]="smsm";
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	stack_string s(src);
	const stack_string::iterator i=s.insert(s.begin(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(insert_str_internal)
{
	const stack_string src("sm");
	const char result[]="ssmm";
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	stack_string s(src);
	const stack_string::iterator i=s.insert(std::next(s.begin()), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin()));
}

BOOST_AUTO_TEST_CASE(erase_all)
{
	stack_string src("sm");
	const stack_string::iterator i=src.erase(src.begin(), src.end());
	BOOST_CHECK(src.empty());
	BOOST_CHECK_EQUAL(src.size(), stack_string::size_type());
	BOOST_CHECK_EQUAL(i, src.end());
}

BOOST_AUTO_TEST_CASE(erase_none)
{
	char const src[]="sm";
	stack_string s(src);
	const stack_string::iterator i=s.erase(s.begin(), s.begin());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(erase_begin_one)
{
	char const src[]="sm";
	char const result[]="m";
	stack_string s(src);
	const stack_string::iterator i=s.erase(s.begin(), std::next(s.begin()));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(erase_end_one)
{
	char const src[]="sm";
	char const result[]="s";
	stack_string s(src);
	const stack_string::iterator i=s.erase(std::prev(s.end()), s.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.end());
}

BOOST_AUTO_TEST_CASE(erase_middle_one)
{
	char const src[]="smal";
	char const result[]="sal";
	BOOST_CHECK_LT(sizeof(src)-1, stack_string::small_string_max_size);
	stack_string s(src);
	const stack_string::iterator i=s.erase(std::next(s.begin(), 1), std::next(s.begin(), 2));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin(), 1));
}

BOOST_AUTO_TEST_CASE(erase_middle_two)
{
	char const src[]="smal";
	char const result[]="sl";
	BOOST_CHECK_LT(sizeof(src)-1, stack_string::small_string_max_size);
	stack_string s(src);
	const stack_string::iterator i=s.erase(std::next(s.begin(), 1), std::next(s.begin(), 3));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin(), 1));
}

BOOST_AUTO_TEST_CASE(replace_dest_empty)
{
	char const src[]="sm";
	stack_string s1, s2(src);
	s1.replace(s1.begin(), s1.begin(), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src));
}

BOOST_AUTO_TEST_CASE(replace_src_empty)
{
	char const src[]="sm";
	stack_string s1(src), s2;
	s1.replace(s1.begin(), s1.end(), s2.begin(), s2.begin());
	BOOST_CHECK(s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), stack_string::size_type());
}

BOOST_AUTO_TEST_CASE(replace_both_empty)
{
	char const src[]="sm";
	stack_string s1(src), s2;
	s1.replace(s1.begin(), s1.begin(), s2.begin(), s2.begin());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src));
}

BOOST_AUTO_TEST_CASE(replace_begin_shrink)
{
	char const src[]="smal";
	char const result[]="sl";
	BOOST_CHECK_LT(sizeof(src)-1, stack_string::small_string_max_size);
	stack_string s1(src), s2(src);
	s1.replace(s1.begin(), std::next(s1.begin(), 3), s2.begin(), std::next(s2.begin()));
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_end_shrink)
{
	char const src[]="smal";
	char const result[]="sms";
	BOOST_CHECK_LT(sizeof(src)-1, stack_string::small_string_max_size);
	stack_string s1(src), s2(src);
	s1.replace(std::prev(s1.end(), 2), s1.end(), s2.begin(), std::next(s2.begin()));
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_middle_shrink)
{
	char const src[]="smal";
	char const result[]="ssl";
	BOOST_CHECK_LT(sizeof(src)-1, stack_string::small_string_max_size);
	stack_string s1(src), s2(src);
	s1.replace(std::next(s1.begin()), std::prev(s1.end()), s2.begin(), std::next(s2.begin()));
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_begin_grow)
{
	char const src[]="sm";
	char const result[]="smm";
	stack_string s1(src), s2(src);
	s1.replace(s1.begin(), std::next(s1.begin()), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_end_grow)
{
	char const src[]="sma";
	char const result[]="ssma";
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	stack_string s1(src), s2(src);
	s1.replace(std::prev(s1.end(), 2), s1.end(), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(replace_middle_grow)
{
	char const src[]="sm";
	char const result[]="ssmm";
	BOOST_CHECK_LT(sizeof(result)-1, stack_string::small_string_max_size);
	stack_string s1(src), s2(src);
	s1.replace(std::next(s1.begin()), std::next(s1.begin(), 1), s2.begin(), s2.end());
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), result));
}

BOOST_AUTO_TEST_CASE(hash)
{
	char const src[]="sm";
	stack_string s(src);
	BOOST_CHECK_EQUAL(std::hash<stack_string>()(s), std::size_t(115));
}

BOOST_AUTO_TEST_CASE(insert)
{
	char const src[]="sm";
	stack_string s(src);
	std::stringstream ss;
	ss<<s;
	BOOST_CHECK_EQUAL(ss.str(), std::string(src));
}

BOOST_AUTO_TEST_CASE(extract)
{
	char const src[]="sm";
	stack_string s;
	const stack_string res(src);
	std::stringstream ss;
	ss<<src;
	ss>>s;
	BOOST_CHECK_EQUAL(s, res);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
