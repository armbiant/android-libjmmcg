/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>
#include <boost/thread/mutex.hpp>

#include "core/ave_deviation_meter.hpp"
#include "core/stats_output.hpp"

#include "core/jthread.hpp"

#include <chrono>

using namespace libjmmcg;
using namespace ppd;

using timed_results_t=ave_deviation_meter<double>;
using jthread_type=ppd::jthread<platform_api, heavyweight_threading>;

template<class LkT>
struct details;
template<>
struct details<api_lock_traits<platform_api, heavyweight_threading>::anon_spin_event_type> {
	using mutex_t=api_lock_traits<platform_api, heavyweight_threading>::anon_spin_event_type;
	using lock_t=mutex_t::lock_type;
	using thread_t=jthread_type;
};
template<>
struct details<api_lock_traits<platform_api, heavyweight_threading>::mutex_type> {
	using mutex_t=api_lock_traits<platform_api, heavyweight_threading>::mutex_type;
	using lock_t=mutex_t::lock_type;
	using thread_t=jthread_type;
};
template<>
struct details<std::mutex> {
	using mutex_t=std::mutex;
	using lock_t=std::scoped_lock<mutex_t>;
	using thread_t=jthread_type;
};
template<>
struct details<boost::mutex> {
	using mutex_t=boost::mutex;
	using lock_t=boost::mutex::scoped_lock;
	using thread_t=jthread_type;
};

using lock_types=boost::mpl::list<
	details<api_lock_traits<platform_api, heavyweight_threading>::anon_spin_event_type>,
	details<api_lock_traits<platform_api, heavyweight_threading>::mutex_type>,
	details<std::mutex>,
	details<boost::mutex>
>;

BOOST_AUTO_TEST_SUITE(mutex_tests)

BOOST_AUTO_TEST_CASE(ctor) {
	using mutex_t=api_lock_traits<platform_api, heavyweight_threading>::anon_spin_event_type;

	mutex_t lk;
	BOOST_CHECK_NO_THROW(lk.lock());
	BOOST_CHECK_NO_THROW(lk.unlock());
}

BOOST_AUTO_TEST_CASE(scoped_lock) {
	using mutex_t=api_lock_traits<platform_api, heavyweight_threading>::anon_spin_event_type;
	using lock_t=mutex_t::lock_type;

	mutex_t lk;
	BOOST_CHECK_NO_THROW(const lock_t lker(lk));
}

BOOST_AUTO_TEST_CASE(multithreaded_scoped_lock) {
	using mutex_t=api_lock_traits<platform_api, heavyweight_threading>::anon_spin_event_type;
	using lock_t=mutex_t::lock_type;

	const std::size_t num_loops=1000;
	mutex_t lk;
	unsigned long i=0;
	{
		const jthread_type thr1(
			[&lk, &i]() {
				for (std::size_t j=0; j<num_loops; ++j) {
					const lock_t lker(lk);
					++i;
				}
			}
		);
		const jthread_type thr2(
			[&lk, &i]() {
				for (std::size_t j=0; j<num_loops; ++j) {
					const lock_t lker(lk);
					++i;
				}
			}
		);
		const jthread_type thr3(
			[&lk, &i]() {
				for (std::size_t j=0; j<num_loops; ++j) {
					const lock_t lker(lk);
					++i;
				}
			}
		);
		const jthread_type thr4(
			[&lk, &i]() {
				for (std::size_t j=0; j<num_loops; ++j) {
					const lock_t lker(lk);
					++i;
				}
			}
		);
	}
	BOOST_CHECK_EQUAL(i, 4*num_loops);
}

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("mutex.csv"))

/**
	\test <a href="./examples/mutex.svg">Graph</a> of performance results for a spin-lock based mutex versus other common mutexes.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(multithreaded_scoped_lock, details_t, lock_types) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const std::size_t num_tests=5000;
	const std::size_t num_loops=500000;
#else
	const std::size_t num_tests=1;
	const std::size_t num_loops=1000;
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
			using mutex_t=typename details_t::mutex_t;
			using lock_t=typename details_t::lock_t;
			using thread_t=typename details_t::thread_t;

			mutex_t lk;
			unsigned long i=0;
			const std::chrono::high_resolution_clock::time_point t1=std::chrono::high_resolution_clock::now();
			{
				const thread_t thr1(
					[&lk, &i]() {
						for (std::size_t j=0; j<num_loops; ++j) {
							const lock_t lker(lk);
							++i;
						}
					}
				);
				const thread_t thr2(
					[&lk, &i]() {
						for (std::size_t j=0; j<num_loops; ++j) {
							const lock_t lker(lk);
							++i;
						}
					}
				);
				const thread_t thr3(
					[&lk, &i]() {
						for (std::size_t j=0; j<num_loops; ++j) {
							const lock_t lker(lk);
							++i;
						}
					}
				);
				const thread_t thr4(
					[&lk, &i]() {
						for (std::size_t j=0; j<num_loops; ++j) {
							const lock_t lker(lk);
							++i;
						}
					}
				);
			}
			const std::chrono::high_resolution_clock::time_point t2=std::chrono::high_resolution_clock::now();
			BOOST_CHECK_EQUAL(i, 4*num_loops);
			return timed_results_t::value_type(static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count())/(4*num_loops));
		}
	));
	std::cout<<boost::core::demangle(typeid(typename details_t::mutex_t).name())<<" locks-unlock in usec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
