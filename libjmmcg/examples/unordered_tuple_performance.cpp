/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include LIBJMMCG_TEST_PERFECT_HASH_MASK_HDR

#include "core/ave_deviation_meter.hpp"
#include "core/stats_output.hpp"
#include "core/unordered_tuple.hpp"

#include <chrono>
#include <memory>
#include <unordered_map>

using namespace libjmmcg;

using timed_results_t=ave_deviation_meter<unsigned long long>;

struct base {
	using key_type=std::int32_t;
	static_assert(sizeof(key_type)<=sizeof(std::size_t));

	virtual ~base()=default;

	virtual unsigned long fn(unsigned long j) const=0;
};
struct derived1 final : base {
	/**
	 * \warning The hash values for these types is replicated in the related "CMakeLists.txt" and must be changed in synchrony otherwise crashes will occur!
	 */
	static inline constexpr const base::key_type hash=0;

	const unsigned long i_;

	explicit derived1(unsigned long i)
	: i_(i) {}

	unsigned long fn(unsigned long j) const override {
		return i_*j;
	}
};
struct derived2 final : base {
	/**
	 * \warning The hash values for these types is replicated in the related "CMakeLists.txt" and must be changed in synchrony otherwise crashes will occur!
	 */
	static inline constexpr const base::key_type hash=1;

	const unsigned long i_;

	explicit derived2(unsigned long i)
	: i_(i) {}

	unsigned long fn(unsigned long j) const override {
		return i_+j;
	}
};

template<class T>
struct extract {
	static inline constexpr const typename T::key_type value=T::hash;
};

BOOST_AUTO_TEST_SUITE(unordered_tuple_tests)

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("unordered_tuple_performance.csv"))

/**
	\test <a href="./examples/unordered_tuple_performance.svg">Graph</a> of performance results for operations on an unordered_tuple & unordered_map.
			==========================================================================================
	Call a pure-virtual member-function in a base class of a class found in an unordered collection.
*/
BOOST_AUTO_TEST_CASE(unordered_tuple_perf) {
	using collection_type=unordered_tuple<
		base::key_type,
		base,
		perfect_hash<
			static_cast<unsigned>(extract<derived1>::value),
			static_cast<unsigned>(extract<derived2>::value)
		>,
		extract,
		derived1,
		derived2
	>;

	collection_type colln(derived1(667), derived2(42));
	unsigned long res=0;
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=2<<22;
#else
	const unsigned long test_size=2<<2;
#endif
	const unsigned short loops_for_conv=1000;
	const double perc_conv_estimate=2.0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&colln, &res]() {
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long i=0; i<test_size; ++i) {
				res+=colln[i%2].fn(i);
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(test_size)*1000000.0/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()));
		}
	));
	BOOST_CHECK_GT(res, 0);
	std::cout<<typeid(collection_type).name()<<" operations/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/unordered_tuple_performance.svg">Graph</a> of performance results for operations on an unordered_map.
			==========================================================================================
	Call a pure-virtual member-function in a base class of a class found in a std::unordered_map.
*/
BOOST_AUTO_TEST_CASE(unordered_map_perf) {
	using base_t=std::shared_ptr<base>;
	using collection_type=std::unordered_map<base::key_type, base_t>;

	collection_type colln;
	colln.emplace(derived1::hash, std::make_shared<derived1>(667));
	colln.emplace(derived2::hash, std::make_shared<derived2>(42));
	unsigned long res=0;
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=2<<22;
#else
	const unsigned long test_size=2<<2;
#endif
	const unsigned short loops_for_conv=1000;
	const double perc_conv_estimate=2.0;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&colln, &res]() {
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long i=0; i<test_size; ++i) {
				res+=colln[i%2]->fn(i);
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(static_cast<double>(test_size)*1000000.0/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()));
		}
	));
	BOOST_CHECK_GT(res, 0);
	std::cout<<typeid(collection_type).name()<<" operations/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
