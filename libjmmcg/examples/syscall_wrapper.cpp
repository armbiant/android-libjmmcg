/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include "core/syscall_wrapper.hpp"

#include <cstdlib>

#include <sys/mman.h>

using namespace libjmmcg;

extern "C" [[gnu::const]] int
test_fn_int_return(int i, long j) {
	return static_cast<int>(i + j);
}

extern "C" [[gnu::const]] void*
test_fn_void_ptr_return(void* p) {
	return p;
}

extern "C" [[gnu::const]] sighandler_t
test_fn_sighandler_t(sighandler_t p) {
	return p;
}

template<class>
struct failure_code_mmap {
	static inline constexpr const std::int64_t value= -1;
};

BOOST_AUTO_TEST_SUITE(syscall_wrapper_tests)

BOOST_AUTO_TEST_CASE(malloc_free_default_failure_traits) {
	void* is= nullptr;
	BOOST_CHECK_NO_THROW((is= JMMCG_SYSCALL_WRAPPER("Error message.", ::malloc, std::size_t(sizeof(int)))));
	BOOST_CHECK_NE(is, nullptr);
	BOOST_CHECK_NO_THROW((JMMCG_SYSCALL_WRAPPER("Error message.", ::free, is)));
}

BOOST_AUTO_TEST_CASE(int_return_function_succeeds_default_failure_traits) {
	int ret= 0;
	BOOST_CHECK_NO_THROW((ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_int_return, 1, 2L)));
	BOOST_CHECK_EQUAL(ret, 3);
}

BOOST_AUTO_TEST_CASE(int_return_function_fails_default_failure_traits) {
	int ret= 0;
	BOOST_CHECK_THROW((ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_int_return, 1, -2L)), std::exception);
	BOOST_CHECK_EQUAL(ret, 0);
}

BOOST_AUTO_TEST_CASE(int_return_function_fails_default_failure_traits_check_exception_details_catch_via_std_exception) {
	int ret= 0;
	try {
		ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_int_return, 1, -2L);
	} catch(std::exception const& ex) {
		std::string const what= ex.what();
		BOOST_CHECK(!what.empty());
	} catch(...) {
		BOOST_FAIL("Incorrect exception-type thrown, so not correctly caught.");
	}
	BOOST_CHECK_EQUAL(ret, 0);
}

BOOST_AUTO_TEST_CASE(int_return_function_fails_default_failure_traits_check_exception_details_catch_via_catch_all) {
	int ret= 0;
	try {
		ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_int_return, 1, -2L);
	} catch(...) {
		std::string const what= boost::current_exception_diagnostic_information();
		BOOST_CHECK(!what.empty());
	}
	BOOST_CHECK_EQUAL(ret, 0);
}

BOOST_AUTO_TEST_CASE(void_ptr_return_function_succeeds_default_failure_traits) {
	void* ret= nullptr;
	BOOST_CHECK_NO_THROW((ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_void_ptr_return, reinterpret_cast<void*>(0x1))));
	BOOST_CHECK_EQUAL(ret, reinterpret_cast<void*>(0x1));
}

BOOST_AUTO_TEST_CASE(void_ptr_return_function_fails_default_failure_traits) {
	void* ret= nullptr;
	BOOST_CHECK_THROW((ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_void_ptr_return, nullptr)), std::exception);
	BOOST_CHECK_EQUAL(ret, nullptr);
}

BOOST_AUTO_TEST_CASE(void_ptr_return_function_succeeds_custom_failure_traits) {
	void* ret= nullptr;
	BOOST_CHECK_NO_THROW((ret= JMMCG_SYSCALL_WRAPPER_FC(failure_code_mmap, "Error message.", test_fn_void_ptr_return, reinterpret_cast<void*>(0x1))));
	BOOST_CHECK_EQUAL(ret, reinterpret_cast<void*>(0x1));
}

BOOST_AUTO_TEST_CASE(void_ptr_return_function_fails_custom_failure_traits) {
	void* ret= nullptr;
	BOOST_CHECK_THROW((ret= JMMCG_SYSCALL_WRAPPER_FC(failure_code_mmap, "Error message.", test_fn_void_ptr_return, reinterpret_cast<void*>(-1))), std::exception);
	BOOST_CHECK_EQUAL(ret, nullptr);
}

BOOST_AUTO_TEST_CASE(signal_succeeds) {
	sighandler_t ret= nullptr;
	BOOST_CHECK_NO_THROW((ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_sighandler_t, nullptr)));
	BOOST_CHECK_EQUAL(ret, nullptr);
}

BOOST_AUTO_TEST_CASE(signal_fails) {
	sighandler_t ret= nullptr;
	BOOST_CHECK_THROW((ret= JMMCG_SYSCALL_WRAPPER("Error message.", test_fn_sighandler_t, reinterpret_cast<sighandler_t>(-1))), std::exception);
	BOOST_CHECK_EQUAL(ret, nullptr);
}

/* TODO Does not work, as requires C++20, with g++ v9.2.
BOOST_AUTO_TEST_CASE(using_a_switch) {
	using all_case_statements=syscall::case_statements_t<
		syscall::a_case_statement<1, [](){return 1;}>,
		syscall::a_case_statement<2, [](){return 2;}>
	>;

	int i=1;
	long j=1;
	int ret=0;
	BOOST_CHECK_NO_THROW((
		ret=JMMCG_SYSCALL_WRAPPER_SWITCH(
			all_case_statements,
			"Error message.",
			_T(LIBJMMCG_VERSION_NUMBER),
			test_fn_int_return,
			i,
			j
		)
	));
	BOOST_CHECK_EQUAL(ret, 2);
}
*/
BOOST_AUTO_TEST_SUITE_END()
