/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_api_traits.hpp"
#include "core/thread_wrapper.hpp"

using namespace libjmmcg;
using namespace ppd;

typedef boost::mpl::list<
	api_lock_traits<platform_api, heavyweight_threading>::atomic_counter_type<long>
> ctr_types;

template<long exit_val, class Ctr>
struct inc_work final {
	Ctr &i;

	explicit __stdcall inc_work(Ctr &c) noexcept(true)
	: i(c) {
	}

	void operator()(std::atomic_flag &exit_requested) {
		for (unsigned long j=0; j<exit_val; ++j) {
			++i;
		}
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<long exit_val, class Ctr>
using inc_thread=ppd::wrapper<inc_work<exit_val, Ctr>, ppd::platform_api, heavyweight_threading>;

template<long exit_val, long Inc, class Ctr>
struct inc_by_val_work final {
	Ctr &i;

	explicit __stdcall inc_by_val_work(Ctr &c) noexcept(true)
	: i(c) {
	}

	void operator()(std::atomic_flag &exit_requested) {
		for (unsigned long j=0; j<exit_val; ++j) {
			i+=Inc;
		}
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<long exit_val, long Inc, class Ctr>
using inc_by_val_thread=ppd::wrapper<inc_by_val_work<exit_val, Inc, Ctr>, ppd::platform_api, heavyweight_threading>;

template<long exit_val, class Ctr>
struct dec_work final {
	Ctr &i;

	explicit __stdcall dec_work(Ctr &c) noexcept(true)
	: i(c) {
	}

	void operator()(std::atomic_flag &exit_requested) {
		for (unsigned long j=0; j<exit_val; ++j) {
			--i;
		}
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<long exit_val, class Ctr>
using dec_thread=ppd::wrapper<dec_work<exit_val, Ctr>, ppd::platform_api, heavyweight_threading>;

template<long exit_val, long Dec, class Ctr>
struct dec_by_val_work final {
	Ctr &i;

	explicit __stdcall dec_by_val_work(Ctr &c) noexcept(true)
	: i(c) {
	}

	void operator()(std::atomic_flag &exit_requested) {
		for (unsigned long j=0; j<exit_val; ++j) {
			i-=Dec;
		}
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<long exit_val, long Dec, class Ctr>
using dec_by_val_thread=ppd::wrapper<dec_by_val_work<exit_val, Dec, Ctr>, ppd::platform_api, heavyweight_threading>;

BOOST_AUTO_TEST_SUITE(atomic_counter_parallel_tests)

BOOST_AUTO_TEST_CASE_TEMPLATE(parallel_increments, ctr_t, ctr_types) {
	const unsigned int check_value=100;
	typedef inc_thread<check_value, ctr_t> inc_t;

	ctr_t c;
	inc_t inc1(c);
	inc_t inc2(c);
	inc1.create_running();
	inc2.create_running();
	do {
		api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
	} while (inc1.is_running() || inc2.is_running());
	BOOST_CHECK_EQUAL(c.get(), 2*check_value);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(parallel_decrements, ctr_t, ctr_types) {
	const unsigned long check_value=100;
	typedef dec_thread<check_value, ctr_t> dec_t;

	ctr_t c(2*check_value);
	dec_t dec1(c);
	dec_t dec2(c);
	dec1.create_running();
	dec2.create_running();
	do {
		api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
	} while (dec1.is_running() || dec2.is_running());
	BOOST_CHECK_EQUAL(c.get(), 0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(parallel_inc_and_dec, ctr_t, ctr_types) {
	const unsigned long check_value=100;
	typedef inc_thread<check_value, ctr_t> inc_t;
	typedef dec_thread<check_value, ctr_t> dec_t;

	ctr_t c(check_value);
	inc_t inc(c);
	dec_t dec(c);
	inc.create_running();
	dec.create_running();
	do {
		api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
	} while (inc.is_running() || dec.is_running());
	BOOST_CHECK_EQUAL(c.get(), check_value);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(parallel_inc_and_dec_by_val, ctr_t, ctr_types) {
	const unsigned long check_value=100;
	const unsigned long delta=2;
	typedef inc_by_val_thread<check_value, delta, ctr_t> inc_t;
	typedef dec_by_val_thread<check_value, delta, ctr_t> dec_t;

	ctr_t c(delta*check_value);
	inc_t inc(c);
	dec_t dec(c);
	inc.create_running();
	dec.create_running();
	do {
		api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
	} while (inc.is_running() || dec.is_running());
	BOOST_CHECK_EQUAL(c.get(), delta*check_value);
}

BOOST_AUTO_TEST_SUITE_END()
