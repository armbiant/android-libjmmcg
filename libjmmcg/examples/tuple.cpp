/******************************************************************************
** Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include "core/tuple.hpp"

using namespace libjmmcg;

BOOST_AUTO_TEST_SUITE(tuple_tests)

BOOST_AUTO_TEST_CASE(concatenate_three_tuples)
{
	struct test {
		using t0=std::tuple<int, long>;
		using t1=std::tuple<unsigned, long long>;
		using t2=std::tuple<double, float>;
		using type=tuple_cat<t0, t1, t2>::type;

		static_assert(std::is_same<std::tuple_element<0, type>::type, int>::value);
		static_assert(std::is_same<std::tuple_element<1, type>::type, long>::value);
		static_assert(std::is_same<std::tuple_element<2, type>::type, unsigned>::value);
		static_assert(std::is_same<std::tuple_element<3, type>::type, long long>::value);
		static_assert(std::is_same<std::tuple_element<4, type>::type, double>::value);
		static_assert(std::is_same<std::tuple_element<5, type>::type, float>::value);
	};
	test::type t(0, 1L, 2U, 3LL, 4.0, 5.0);

	BOOST_CHECK_EQUAL(std::get<0>(t), 0);
	BOOST_CHECK_EQUAL(std::get<1>(t), 1L);
	BOOST_CHECK_EQUAL(std::get<2>(t), 2U);
	BOOST_CHECK_EQUAL(std::get<3>(t), 3LL);
	BOOST_CHECK_EQUAL(std::get<4>(t), 4.0);
	BOOST_CHECK_EQUAL(std::get<5>(t), 5.0);
}

BOOST_AUTO_TEST_SUITE_END()
