/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include <boost/mpl/list.hpp>

#include "core/thread_pool_master.hpp"
#include "core/thread_pool_sequential.hpp"
#include "core/thread_pool_workers.hpp"

#include <random>

using namespace libjmmcg;
using namespace ppd;

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize= 0, unsigned int GSSk= 1>
struct priority_queue_t {
	typedef pool_aspects<
		Jn,
		platform_api,
		Mdl,
		pool_traits::prioritised_queue,
		std::less,
		GSSk>
		thread_pool_traits;

	typedef thread_pool<Db, Sz, thread_pool_traits> pool_type;

	static const typename pool_type::pool_type::size_type pool_size= PoolSize;
};

template<class Db, pool_traits::size_mode_t::element_type Sz, generic_traits::return_data::element_type Jn, class Mdl, unsigned int PoolSize, unsigned int GSSk>
const typename priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_type::pool_type::size_type priority_queue_t<Db, Sz, Jn, Mdl, PoolSize, GSSk>::pool_size;

typedef boost::mpl::list<
	//	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1>
	// TODO	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 1, 2>,

	//	priority_queue_t<pool_traits::work_distribution_mode_t::worker_threads_get_work<pool_traits::work_distribution_mode_t::queue_model_t::pool_owns_queue>, pool_traits::size_mode_t::fixed_size, generic_traits::return_data::element_type::joinable, heavyweight_threading, 2>
	>
	finite_test_types;

typedef boost::mpl::list<
	//	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::sequential, generic_traits::return_data::element_type::joinable, sequential_mode>,
	//	priority_queue_t<pool_traits::work_distribution_mode_t::one_thread_distributes<>, pool_traits::size_mode_t::infinite, generic_traits::return_data::element_type::joinable, heavyweight_threading>
	>
	infinite_test_types;

// TODO Need to test: pool_traits::size_mode_t::tracks_to_max

struct res_t {
	int i;
};

struct work_type_simple {
	int i_;

	work_type_simple(const int i)
		: i_(i) {
	}
	void __fastcall process(res_t& r) {
		sleep(1);
		r.i= i_;
	}

	bool __fastcall operator<(work_type_simple const& i) const {
		return i_ < i.i_;
	}
};

BOOST_AUTO_TEST_SUITE(thread_pool_tests)

BOOST_AUTO_TEST_SUITE(joinable)

BOOST_AUTO_TEST_SUITE(finite)

BOOST_AUTO_TEST_SUITE(nowait)

BOOST_AUTO_TEST_CASE_TEMPLATE(n_threads, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;
	typedef typename pool_type::nonjoinable nonjoinable;

	constexpr const unsigned test_size= 1000;

	std::mt19937_64 gen(42);
	std::uniform_int_distribution<int> distribution(std::numeric_limits<int>::min() + 1, std::numeric_limits<int>::max() - 1);
	auto rand= std::bind(distribution, gen);

	pool_type pool(T::pool_size);
	for(unsigned i= 0; i < test_size; ++i) {
		pool << nonjoinable() << work_type_simple(rand());
	}
	BOOST_CHECK_GT(pool.queue_size(), 0U);
	auto const& exec= pool << joinable() << work_type_simple(std::numeric_limits<int>::min());
	*exec;
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(wait_dataflow)

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, finite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	constexpr const unsigned test_size= 10;

	std::mt19937_64 gen(42);
	std::uniform_int_distribution<int> distribution(std::numeric_limits<int>::min() + 1, std::numeric_limits<int>::max() - 1);
	auto rand= std::bind(distribution, gen);

	pool_type pool(T::pool_size);
	for(unsigned i= 0; i < test_size; ++i) {
		auto const& exec0= pool << joinable() << work_type_simple(rand());
		auto const& exec1= pool << joinable() << work_type_simple(rand());
		auto const& exec2= pool << joinable() << work_type_simple(rand());
		auto const& exec3= pool << joinable() << work_type_simple(rand());
		auto const& exec4= pool << joinable() << work_type_simple(rand());
		auto const& exec5= pool << joinable() << work_type_simple(rand());
		auto const& exec6= pool << joinable() << work_type_simple(rand());
		auto const& exec7= pool << joinable() << work_type_simple(rand());
		auto const& exec8= pool << joinable() << work_type_simple(rand());
		auto const& exec9= pool << joinable() << work_type_simple(rand());
		*exec0;
		*exec1;
		*exec2;
		*exec3;
		*exec4;
		*exec5;
		*exec6;
		*exec7;
		*exec8;
		*exec9;
	}
	BOOST_CHECK_EQUAL(pool.pool_size(), T::pool_size);
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(infinite)

BOOST_AUTO_TEST_SUITE(wait_dataflow)

BOOST_AUTO_TEST_CASE_TEMPLATE(add_one_work, T, infinite_test_types) {
	typedef typename T::pool_type pool_type;
	typedef typename pool_type::joinable joinable;

	constexpr const unsigned test_size= 100;

	std::mt19937_64 gen(42);
	std::uniform_int_distribution<int> distribution(std::numeric_limits<int>::min() + 1, std::numeric_limits<int>::max() - 1);
	auto rand= std::bind(distribution, gen);

	pool_type pool;
	for(unsigned i= 0; i < test_size; ++i) {
		auto const& exec0= pool << joinable() << work_type_simple(rand());
		auto const& exec1= pool << joinable() << work_type_simple(rand());
		auto const& exec2= pool << joinable() << work_type_simple(rand());
		auto const& exec3= pool << joinable() << work_type_simple(rand());
		auto const& exec4= pool << joinable() << work_type_simple(rand());
		auto const& exec5= pool << joinable() << work_type_simple(rand());
		auto const& exec6= pool << joinable() << work_type_simple(rand());
		auto const& exec7= pool << joinable() << work_type_simple(rand());
		auto const& exec8= pool << joinable() << work_type_simple(rand());
		auto const& exec9= pool << joinable() << work_type_simple(rand());
		*exec0;
		*exec1;
		*exec2;
		*exec3;
		*exec4;
		*exec5;
		*exec6;
		*exec7;
		*exec8;
		*exec9;
	}
	BOOST_CHECK_EQUAL(pool.queue_size(), 0U);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
