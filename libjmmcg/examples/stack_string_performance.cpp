/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

// TODO #include "folly/FixedString.h"

#include <boost/mpl/list.hpp>

#include "core/stack_string.hpp"

#include "core/ave_deviation_meter.hpp"
#include "core/stats_output.hpp"

#include <chrono>
#include <iostream>
#include <string>
#include <ext/vstring.h>

using namespace libjmmcg;

using timed_results_t=ave_deviation_meter<unsigned long long>;

typedef boost::mpl::list<
	basic_stack_string<12, char>,
	std::string,
	__gnu_cxx::__vstring//,
// TODO	folly::FixedString<sizeof(char *)>
> string_types;

const unsigned short loops_for_conv=50;
const double perc_conv_estimate=5.0;
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=100000000U;
#else
	const unsigned long test_size=100U;
#endif

/**
	\test <a href="./examples/stack_string_performance_ctor_dtor.svg">Graph of ctor & dtor</a>, <a href="./examples/stack_string_performance_assignment.svg">Graph of ctor, dtor & assignment</a> and <a href="./examples/stack_string_performance_replace.svg">Graph of ctor, dtor & replace</a> performance comparison of stack_string vs std::string and __gnucxx::__vstring'
			==============================================================================
	Results for 100000000 repetitions.
	On my dual, 6-core AMD Opteron 4180s at 2.6GHz box with 16Gb RAM (not quiescent), I measured the following statistics for a release build:
	-# Build 1414:
		-	stack_string small string (ctors+dtors)/sec=[135662569, 136631149 ~(+/-4%), 136727642], samples=21, total=2869254133
		-	std::string small string (ctors+dtors)/sec=[14223043, 14258612 ~(+/-4%), 14268891], samples=21, total=299430869
		-	__gnucxx::__vstring small string (ctors+dtors)/sec=[86412573, 86559127 ~(+/-4%), 86598005], samples=21, total=1817741679
		-	stack_string big string (ctors+dtors)/sec=[17663500, 17665794 ~(+/-4%), 17668187], samples=21, total=370981680
		-	std::string big string (ctors+dtors)/sec=[13455498, 13810127 ~(+/-4%), 14190757], samples=24, total=331443053
		-	__gnucxx::__vstring big string (ctors+dtors)/sec=[17905987, 17909566 ~(+/-4%), 17911321], samples=21, total=376100905
		-	stack_string small string (ctors+dtors+assignment)/sec=[55224728, 55254120 ~(+/-4%), 55267397], samples=21, total=1160336527
		-	std::string small string (ctors+dtors+assignment)/sec=[10093766, 10138325 ~(+/-4%), 10177015], samples=21, total=212904843
		-	__gnucxx::__vstring small string (ctors+dtors+assignment)/sec=[37145984, 39441839 ~(+/-4%), 42999101], samples=41, total=1617115404
		-	stack_string big string (ctors+dtors+assignment)/sec=[8446274, 8726725 ~(+/-4%), 8744792], samples=20, total=174534506
		-	std::string big string (ctors+dtors+assignment)/sec=[9887807, 10000563 ~(+/-4%), 10065898], samples=21, total=210011827
		-	__gnucxx::__vstring big string (ctors+dtors+assignment)/sec=[9993189, 10073089 ~(+/-4%), 10130002], samples=20, total=201461782
		-	stack_string small string (ctors+dtors+replace)/sec=[30779326, 31247464 ~(+/-4%), 31286947], samples=21, total=656196758
		-	std::string small string (ctors+dtors+replace)/sec=[3554754, 3569919 ~(+/-4%), 3580345], samples=21, total=74968318
		-	__gnucxx::__vstring small string (ctors+dtors+replace)/sec=[24622578, 25761126 ~(+/-4%), 25973223], samples=20, total=515222525
		-	stack_string big string (ctors+dtors+replace)/sec=[5175213, 5381860 ~(+/-4%), 5412671], samples=20, total=107637218
		-	std::string big string (ctors+dtors+replace)/sec=[3548994, 3575052 ~(+/-4%), 3605774], samples=21, total=75076100
		-	__gnucxx::__vstring big string (ctors+dtors+replace)/sec=[5421178, 5540581 ~(+/-4%), 5548517], samples=20, total=110811631
	-# Build 1627:
		-	stack_string small string (ctors+dtors)/sec=[77688729, 78157207 ~(+/-4%), 78233671], samples=21, total=1641301351
		-	std::string small string (ctors+dtors)/sec=[15288084, 15429237 ~(+/-4%), 15484286], samples=20, total=308584744
		-	__gnucxx::__vstring small string (ctors+dtors)/sec=[39799648, 41135410 ~(+/-4%), 41391482], samples=20, total=822708201
		-	stack_string big string (ctors+dtors)/sec=[14184497, 14435743 ~(+/-4%), 14460011], samples=21, total=303150620
		-	std::string big string (ctors+dtors)/sec=[14697476, 14779399 ~(+/-4%), 14815372], samples=21, total=310367382
		-	__gnucxx::__vstring big string (ctors+dtors)/sec=[12887788, 13119301 ~(+/-5%), 13231750], samples=21, total=275505323
		-	stack_string small string (ctors+dtors+assignment)/sec=[66254782, 67709907 ~(+/-5%), 67908163], samples=21, total=1421908066
		-	std::string small string (ctors+dtors+assignment)/sec=[10831330, 10883866 ~(+/-4%), 10897227], samples=21, total=228561205
		-	__gnucxx::__vstring small string (ctors+dtors+assignment)/sec=[23789008, 24003363 ~(+/-5%), 24265407], samples=21, total=504070629
		-	stack_string big string (ctors+dtors+assignment)/sec=[7340829, 7656490 ~(+/-6%), 7802339], samples=21, total=160786309
		-	std::string big string (ctors+dtors+assignment)/sec=[10271093, 10534818 ~(+/-5%), 10578410], samples=21, total=221231184
		-	__gnucxx::__vstring big string (ctors+dtors+assignment)/sec=[7732222, 7846254 ~(+/-5%), 7857272], samples=21, total=164771350
		-	stack_string small string (ctors+dtors+replace)/sec=[16342456, 16412117 ~(+/-4%), 16427649], samples=21, total=344654458
		-	std::string small string (ctors+dtors+replace)/sec=[4232467, 4267956 ~(+/-4%), 4281863], samples=21, total=89627089
		-	__gnucxx::__vstring small string (ctors+dtors+replace)/sec=[15143025, 15316453 ~(+/-4%), 15364299], samples=21, total=321645514
		-	stack_string big string (ctors+dtors+replace)/sec=[4185507, 4281924 ~(+/-5%), 4298870], samples=21, total=89920404
		-	std::string big string (ctors+dtors+replace)/sec=[4425767, 4452773 ~(+/-4%), 4463363], samples=20, total=89055461
		-	__gnucxx::__vstring big string (ctors+dtors+replace)/sec=[4680365, 4695952 ~(+/-4%), 4719313], samples=21, total=98615000
	-# Build 1643:
		-	stack_string small string (ctors+dtors)/sec=[77731544, 78352331 ~(+/-4%), 78418333], samples=20, total=1567046620
		-	std::string small string (ctors+dtors)/sec=[15381950, 15471575 ~(+/-4%), 15510609], samples=21, total=324903095
		-	__gnucxx::__vstring small string (ctors+dtors)/sec=[40882288, 42053251 ~(+/-4%), 42234812], samples=20, total=841065030
		-	stack_string big string (ctors+dtors)/sec=[14472412, 14509867 ~(+/-4%), 14522212], samples=21, total=304707207
		-	std::string big string (ctors+dtors)/sec=[14733592, 14750590 ~(+/-4%), 14759905], samples=21, total=309762398
		-	__gnucxx::__vstring big string (ctors+dtors)/sec=[12806720, 13027029 ~(+/-4%), 13219483], samples=35, total=455946018
		-	stack_string small string (ctors+dtors+assignment)/sec=[64906657, 64933195 ~(+/-4%), 64948307], samples=21, total=1363597097
		-	std::string small string (ctors+dtors+assignment)/sec=[9965722, 10753837 ~(+/-4%), 10893439], samples=25, total=268845934
		-	__gnucxx::__vstring small string (ctors+dtors+assignment)/sec=[25880419, 26471226 ~(+/-4%), 26642595], samples=23, total=608838203
		-	stack_string big string (ctors+dtors+assignment)/sec=[7380573, 7479894 ~(+/-4%), 7582891], samples=26, total=194477260
		-	std::string big string (ctors+dtors+assignment)/sec=[10530196, 10532715 ~(+/-4%), 10535628], samples=21, total=221187030
		-	__gnucxx::__vstring big string (ctors+dtors+assignment)/sec=[7574794, 7824633 ~(+/-4%), 7860065], samples=22, total=172141946
		-	stack_string small string (ctors+dtors+replace)/sec=[15697091, 15820997 ~(+/-4%), 15966704], samples=22, total=348061936
		-	std::string small string (ctors+dtors+replace)/sec=[4502732, 4529391 ~(+/-4%), 4540575], samples=21, total=95117212
		-	__gnucxx::__vstring small string (ctors+dtors+replace)/sec=[14205080, 14691759 ~(+/-4%), 15367257], samples=32, total=470136317
		-	stack_string big string (ctors+dtors+replace)/sec=[4185298, 4235308 ~(+/-4%), 4247537], samples=20, total=84706170
		-	std::string big string (ctors+dtors+replace)/sec=[4468726, 4474550 ~(+/-4%), 4475729], samples=21, total=93965557
		-	__gnucxx::__vstring big string (ctors+dtors+replace)/sec=[4673622, 4688568 ~(+/-4%), 4691540], samples=21, total=98459947
	\todo In gcc 5.10 std::string uses small-string optimisation - needs testing.
*/

BOOST_AUTO_TEST_SUITE(string_tests)

BOOST_AUTO_TEST_SUITE(performance_ctor_dtor, *stats_to_csv::make_fixture("stack_string_performance_ctor_dtor.csv"))

BOOST_AUTO_TEST_CASE_TEMPLATE(small_ctor, T, string_types)
{
	typedef T string_t;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			char const src[]="small";
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long num_loops=0;num_loops<test_size;++num_loops) {
				string_t s(src);
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(test_size/(static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/1000000000));
		}
	));
	std::cout<<typeid(string_t).name()<<" small string (ctors+dtors)/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_CASE_TEMPLATE(big_ctor, T, string_types)
{
	typedef T string_t;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			char const src[]="a very very big string";
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long num_loops=0;num_loops<test_size;++num_loops) {
				string_t s(src);
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(test_size/(static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/1000000000));
		}
	));
	std::cout<<typeid(string_t).name()<<" big string (ctors+dtors)/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(performance_assignment, *stats_to_csv::make_fixture("stack_string_performance_assignment.csv"))

BOOST_AUTO_TEST_CASE_TEMPLATE(small_assignment, T, string_types)
{
	typedef T string_t;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			char const src[]="small";
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long num_loops=0;num_loops<test_size;++num_loops) {
				string_t s1(src);
				string_t s2;
				s2=s1;
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(test_size/(static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/1000000000));
		}
	));
	std::cout<<typeid(string_t).name()<<" small string (ctors+dtors+assignment)/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_CASE_TEMPLATE(big_assignment, T, string_types)
{
	typedef T string_t;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			char const src[]="a very very big string";
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long num_loops=0;num_loops<test_size;++num_loops) {
				string_t s1(src);
				string_t s2;
				s2=s1;
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(test_size/(static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/1000000000));
		}
	));
	std::cout<<typeid(string_t).name()<<" big string (ctors+dtors+assignment)/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(performance_replace, *stats_to_csv::make_fixture("stack_string_performance_replace.csv"))

BOOST_AUTO_TEST_CASE_TEMPLATE(small_replace, T, string_types)
{
	typedef T string_t;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			char const src[]="small";
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long num_loops=0;num_loops<test_size;++num_loops) {
				string_t s1(src);
				string_t s2(src);
				s2.replace(std::next(s2.begin()), std::prev(s2.end()), s1.begin(), s1.end());
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(test_size/(static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/1000000000));
		}
	));
	std::cout<<typeid(string_t).name()<<" small string (ctors+dtors+replace)/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_CASE_TEMPLATE(big_replace, T, string_types)
{
	typedef T string_t;

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[]() {
			char const src[]="a very very big string";
			const auto t1=std::chrono::high_resolution_clock::now();
			for (unsigned long num_loops=0;num_loops<test_size;++num_loops) {
				string_t s1(src);
				{
					string_t s2(src);
					{
						s2.replace(std::next(s2.begin()), std::prev(s2.end()), s1.begin(), s1.end());
					}
				}
			}
			const auto t2=std::chrono::high_resolution_clock::now();
			return timed_results_t::value_type(test_size/(static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count())/1000000000));
		}
	));
	std::cout<<typeid(string_t).name()<<" big string (ctors+dtors+replace)/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
