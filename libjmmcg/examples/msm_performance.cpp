/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>
#include <boost/mpl/size.hpp>

#include "core/msm.hpp"

#include "core/ave_deviation_meter.hpp"
#include "core/latency_timestamps.hpp"
#include "core/stats_output.hpp"

#include <chrono>
#include <random>

using namespace libjmmcg;

using timed_results_t=ave_deviation_meter<unsigned long long>;

/**
 * \see libjmmcg::perfect_hash, MIT::common::client_to_exchange_transformations::state_machine_t::transition_table
 */
enum class states : std::uint32_t {
	start=68U,
	assign=70U,
	assign1=71U,
	assign2=std::numeric_limits<std::uint32_t>::max()-1,
	end=std::numeric_limits<std::uint32_t>::max()
};

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/**
 * Note that this happens to be the enum tag-values of states.
 * 
 * \see states
*/
template<>
struct perfect_hash<
	static_cast<std::underlying_type<states>::type>(states::start),
	static_cast<std::underlying_type<states>::type>(states::assign),
	static_cast<std::underlying_type<states>::type>(states::assign1),
	static_cast<std::underlying_type<states>::type>(states::assign2)
> {
	using key_type=std::underlying_type<states>::type;
	using element_type=std::uint32_t;
	
	BOOST_MPL_ASSERT_RELATION(sizeof(key_type), <=, sizeof(element_type));
	
	enum : element_type {
		size=4U,
		min_hash=0U,
		max_hash=4U,
		mask=0x0,	///< If the mask is found to be zero, then it has been verified that the 'k^mask' will be optimised to just 'k' in the hash functions.
		denominator=5U,
		is_minimal=(denominator==size)	///< Evaluates to=false
	};
	
	REALLY_FORCE_INLINE static constexpr element_type result(key_type const &k) noexcept {
		return (static_cast<element_type>(k)^mask)%denominator;
	}
	REALLY_FORCE_INLINE constexpr element_type operator()(key_type const &k) const noexcept {
		return (static_cast<element_type>(k)^mask)%denominator;
	}
};

} }

std::ostream &
operator<<(std::ostream &os, states const s) noexcept(false) {
	os<<static_cast<std::underlying_type<states>::type>(s);
	return os;
}

class data {
public:
	using element_type=unsigned long;

	data()
	: gen(42), distribution(), generator(std::bind(distribution, gen)), i(generator()) {
	}

	REALLY_FORCE_INLINE void add() {
		i+=generator();
	}
	REALLY_FORCE_INLINE void multiply() {
		i*=generator();
	}

private:
	/**
	 *	Try to ensure that the events are sufficiently complex to baffle the optimizer.
	 */
	std::mt19937_64 gen;
	std::uniform_int_distribution<element_type> distribution;
	std::function<element_type()> generator;

public:
	element_type i;
};

struct assign_event {
	using arguments_types=std::pair<
		states,
		std::tuple<
			std::tuple<data &>
		>
	>;

	template<auto state, auto next>
	NEVER_INLINE auto process(data &p) const noexcept(true) {
		p.add();
		return next;
	}
};
struct assign_event1 {
	using arguments_types=std::pair<
		states,
		std::tuple<
			std::tuple<data &>
		>
	>;

	template<auto state, auto next>
	NEVER_INLINE auto process(data &p) const noexcept(true) {
		p.multiply();
		return next;
	}
};
struct assign_event2 {
	using arguments_types=std::pair<
		states,
		std::tuple<
			std::tuple<data &>
		>
	>;

	template<auto state, auto next>
	NEVER_INLINE auto process(data &p) const noexcept(true) {
		p.add();
		p.multiply();
		return next;
	}
};

struct unroll {
	struct state_machine_t final : msm::unroll::state_transition_table<state_machine_t> {
		using row_t=msm::unroll::row_types<states, states>;
		using transition_table=rows<
			row_t::row<states::start, assign_event, states::end>,
			row_t::row<states::assign, assign_event, states::end>,
			row_t::row<states::assign1, assign_event1, states::end>,
			row_t::row<states::assign2, assign_event2, states::end>
		>;
	};
	using machine=msm::unroll::machine<state_machine_t>;

	template<class Arg>
	REALLY_FORCE_INLINE auto process(states state, Arg &p) noexcept(false) {
		return msm.process(state, p);
	}

	machine msm;
};
/* TODO Only works with monotonically increasing states...
struct jump_table {
	struct state_machine_t final : msm::jump_table::state_transition_table<state_machine_t> {
		using row_t=msm::jump_table::row_types<states, states>;
		using transition_table=rows<
			row_t::row<states::start, assign_event, states::end>,
			row_t::row<states::assign, assign_event, states::end>,
			row_t::row<states::assign1, assign_event1, states::end>,
			row_t::row<states::assign2, assign_event2, states::end>
		>;
	};
	using machine=msm::jump_table::machine<state_machine_t>;

	template<class Arg>
	REALLY_FORCE_INLINE auto process(states state, Arg &p) noexcept(false) {
		return msm.process(state, p);
	}

	machine msm;
};

struct computed_goto {
	struct state_machine_t final : msm::computed_goto::state_transition_table<state_machine_t> {
		using row_t=msm::computed_goto::row_types<states, states>;
		using transition_table=rows<
			row_t::row<states::start, assign_event, states::end>,
			row_t::row<states::assign, assign_event, states::end>,
			row_t::row<states::assign1, assign_event1, states::end>,
			row_t::row<states::assign2, assign_event2, states::end>
		>;
	};
	using machine=msm::computed_goto::machine<state_machine_t>;

	template<class Arg>
	REALLY_FORCE_INLINE auto process(states state, Arg &p) noexcept(false) {
		return msm.process(state, p);
	}

	machine msm;
};
*/
struct hash {
	struct state_machine_t final : msm::hash::state_transition_table<state_machine_t> {
		using row_t=msm::hash::row_types<states, states>;
		using transition_table=rows<
			row_t::row<states::start, assign_event, states::end>,
			row_t::row<states::assign, assign_event, states::end>,
			row_t::row<states::assign1, assign_event1, states::end>,
			row_t::row<states::assign2, assign_event2, states::end>
		>;
	};
	using machine=msm::hash::machine<state_machine_t>;

	REALLY_FORCE_INLINE auto process(states state, data &p) noexcept(false) {
		return msm.process(state, p);
	}

	machine msm;
};

using state_machines_t=boost::mpl::list<
	unroll::machine,
// TODO	jump_table::machine,
// TODO	computed_goto::machine,
	hash::machine
>;

BOOST_AUTO_TEST_SUITE(msm_tests)

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("msm_performance.csv"))

/**
	\test <a href="./examples/msm_performance.svg">Graph</a> and <a href="./examples/msm_performance_latencies_" LIBJMMCG_CXX_COMPILER_ID ".svg">histogram></a> of the performance comparison of various implementations of an msm.
			==============================================================================
	Performance of randomly-called state-changes per second.
*/
BOOST_AUTO_TEST_CASE_TEMPLATE(state_changes, state_machine_t, state_machines_t) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const unsigned long test_size=1000000U;
	const unsigned short loops_for_conv=500;
	const double perc_conv_estimate=2.0;
	libjmmcg::latency_timestamps ts(test_size*loops_for_conv);
#else
	const unsigned long test_size=10U;
	const unsigned short loops_for_conv=5;
	const double perc_conv_estimate=5.0;
	libjmmcg::no_latency_timestamps ts(0);
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		perc_conv_estimate,
		loops_for_conv,
		[&ts]() {
			static constexpr std::array select_state{
				states::start,
				states::assign,
				states::assign1,
				states::assign2
			};
			BOOST_CHECK(!select_state.empty());

			state_machine_t msm;
			data d;
			std::mt19937_64 gen(42);
			std::uniform_int_distribution<std::size_t> distribution(0, select_state.size()-1);
			auto generator=std::bind(distribution, gen);
			volatile states exit_state;
			const auto start_time=std::chrono::high_resolution_clock::now();
			for (unsigned long num_loops=0; num_loops<test_size; ++num_loops) {
				const libjmmcg::latency_timestamps_itf::period sample(ts);
				exit_state=msm.process(select_state[generator()], d);
			}
			const auto end_time=std::chrono::high_resolution_clock::now();
			BOOST_REQUIRE(end_time>start_time);
			BOOST_CHECK_EQUAL(exit_state, states::end);
			BOOST_CHECK_GT(d.i, 0);
			return timed_results_t::value_type(test_size/(static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(end_time-start_time).count())/1000000000));
		}
	));
	std::cout<<boost::core::demangle(typeid(state_machine_t).name())<<" state-changes/sec="<<timed_results.first<<std::endl;
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
	ts.write_to_named_csv_file(std::cout, "msm_performance_latencies");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
