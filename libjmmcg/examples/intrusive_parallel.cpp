/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include "core/intrusive.hpp"

#include "core/ave_deviation_meter.hpp"
#include "core/stats_output.hpp"
#include "core/thread_wrapper.hpp"

#include <chrono>
#include <set>

using namespace libjmmcg;
using namespace ppd;

using lock_t=api_lock_traits<platform_api, heavyweight_threading>;
using timed_results_t=ave_deviation_meter<double>;

struct data final : public intrusive::node_details_itf<lock_t>, public sp_counter_type<typename intrusive::node_details_itf<lock_t>::atomic_ctr_t::value_type, lock_t> {
	typedef sp_counter_type<intrusive::node_details_itf<lock_t>::atomic_ctr_t::value_type, lock_t> base_t;
	typedef typename base_t::lock_traits lock_traits;
	typedef typename base_t::atomic_ctr_t atomic_ctr_t;
	typedef typename base_t::deleter_t deleter_t;

	const std::size_t i;

	explicit data(std::size_t j) noexcept(true)
	: i(j) {}
	~data() noexcept(true) {}

	tstring
	to_string() const noexcept(false) override {
		tostringstream ss;
		ss<<"i="<<i;
		return ss.str();
	}
};

template<unsigned short N, class Cont, template<class> class Ins>
struct add_work final {
	typedef Cont container_type;

	const typename container_type::size_type num_elems;

	std::vector<typename container_type::value_type> colln;
	container_type &output_colln_;
	Ins<container_type> inserting;

	add_work(const typename container_type::size_type n, container_type &output_colln) noexcept(true)
	: num_elems(n), output_colln_(output_colln), inserting(Ins<container_type>(output_colln)) {
		colln.reserve(num_elems);
		typename container_type::size_type elem=0;
		std::generate_n(
			std::back_inserter(colln),
			num_elems,
			[&elem, this]() -> typename container_type::value_type {
				// Ensure that each element in the container is distinguishable.
				return typename container_type::value_type(new typename container_type::value_type::value_type(N*num_elems+(++elem)));
			}
		);
		BOOST_CHECK_EQUAL(colln.size(), num_elems);
	}
	void operator()(std::atomic_flag &exit_requested) {
		std::copy_n(colln.begin(), num_elems, inserting);
		BOOST_CHECK_EQUAL(colln.size(), num_elems);
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<unsigned short N, class Cont, template<class> class Ins>
using add_thread=wrapper<add_work<N, Cont, Ins>, platform_api, heavyweight_threading>;

template<class Cont>
struct pop_work final {
	typedef Cont container_type;

	const typename container_type::size_type num_elems;

	container_type &output_colln_;

	pop_work(const typename container_type::size_type n, container_type &c) noexcept(true)
	: num_elems(n), output_colln_(c) {
	}

	void operator()(std::atomic_flag &exit_requested) {
		for (typename container_type::size_type i=0; i<num_elems; ++i) {
			output_colln_.pop_front();
		}
		exit_requested.test_and_set(std::memory_order_seq_cst);
	}
};
template<class Cont>
using pop_thread=wrapper<pop_work<Cont>, platform_api, heavyweight_threading>;

BOOST_AUTO_TEST_SUITE(instrusive_parallel_tests)

BOOST_AUTO_TEST_SUITE(stack)

typedef intrusive::stack<data, lock_t> container_type;

BOOST_AUTO_TEST_SUITE(performance, *stats_to_csv::make_fixture("intrusive_parallel.csv"))

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-stack push() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::front_insert_iterator> thread1(num_items, s);
				add_thread<2, container_type, std::front_insert_iterator> thread2(num_items, s);
				add_thread<3, container_type, std::front_insert_iterator> thread3(num_items, s);
				add_thread<4, container_type, std::front_insert_iterator> thread4(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					thread1.create_running();
					thread2.create_running();
					thread3.create_running();
					thread4.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (thread1.is_running() || thread2.is_running() || thread3.is_running() || thread4.is_running());
					// Added all of the elements. Now make sure the container_type is correctly created.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Checks relating to the integrity of the container_type....
			BOOST_CHECK(!s.empty());
			BOOST_CHECK_EQUAL(s.size(), 4*num_items);	// O.k. the atomic-counter based size() is correct.
			BOOST_CHECK_EQUAL(s.size_n(), 4*num_items);	// Woo-hoo so is the actual size of the list in nodes.
			// Check that the reference count of each of the member-elements is correct.
			for (auto const &iter : s) {
				BOOST_CHECK_GE(iter->sp_count(), 1);	// Must be greater than zero, for the reference in the list. The exact value will be related to the implementation of "for (...)" and the BOOST_... method.
				BOOST_CHECK_EQUAL(iter->sp_count(), 3);	// Check that there is no difference between the reference counts on each node, as they should all be the same.
			}
			{
				std::set<std::size_t> check_list_integrity;
				std::transform(
					s.begin(),
					s.end(),
					std::inserter(check_list_integrity, check_list_integrity.begin()),
					[](container_type::value_type const &v) {
						return v->i;
					}
				);
				BOOST_CHECK_EQUAL(check_list_integrity.size(), 4*num_items);	// O.k. the correct number of elements were actually inserted, i.e. no incorrectly duplicated elements (each element is distinguishable via the address and the member "i").
			}
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-stack pop() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(pop)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			container_type::size_type elem=0;
			std::chrono::high_resolution_clock::time_point t1, t2;
			std::generate_n(
				std::front_inserter(s),
				4*num_items,
				[&elem]() {
					// Ensure that each element in the container is distinguishable.
					return container_type::value_type(new container_type::value_type::value_type(++elem));
				}
			);
			{
				// Delete all of the elements from the container_type in parallel.
				pop_thread<container_type> thread1(num_items, s);
				pop_thread<container_type> thread2(num_items, s);
				pop_thread<container_type> thread3(num_items, s);
				pop_thread<container_type> thread4(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					thread1.create_running();
					thread2.create_running();
					thread3.create_running();
					thread4.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (thread1.is_running() || thread2.is_running() || thread3.is_running() || thread4.is_running());
					// container_type should now be empty.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Check the container_type is actually empty...
			BOOST_CHECK(s.empty());
			BOOST_CHECK_EQUAL(s.size(), container_type::size_type());	// O.k. the atomic-counter based size() is correct.
			BOOST_CHECK_EQUAL(s.size_n(), container_type::size_type());	// Woo-hoo so is the actual size of the list in nodes.
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-stack push() and pop() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push_and_pop)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::front_insert_iterator> add_thread1(num_items, s);
				add_thread<2, container_type, std::front_insert_iterator> add_thread2(num_items, s);
				// Delete all of the elements from the container_type in parallel.
				pop_thread<container_type> pop_thread1(num_items, s);
				pop_thread<container_type> pop_thread2(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					add_thread1.create_running();
					pop_thread1.create_running();
					add_thread2.create_running();
					pop_thread2.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (add_thread1.is_running() || add_thread2.is_running() || pop_thread1.is_running() || pop_thread2.is_running());
					// container_type should now be empty.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Check the container_type is actually empty...
			BOOST_CHECK_LT(s.size(), 2*num_items);
			BOOST_CHECK_LT(s.size_n(), 2*num_items);
			BOOST_CHECK_EQUAL(s.size(), s.size_n());
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(slist)

typedef intrusive::slist<data, lock_t> container_type;

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-slist push_front() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push_front)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::front_insert_iterator> thread1(num_items, s);
				add_thread<2, container_type, std::front_insert_iterator> thread2(num_items, s);
				add_thread<3, container_type, std::front_insert_iterator> thread3(num_items, s);
				add_thread<4, container_type, std::front_insert_iterator> thread4(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					thread1.create_running();
					thread2.create_running();
					thread3.create_running();
					thread4.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (thread1.is_running() || thread2.is_running() || thread3.is_running() || thread4.is_running());
					// Added all of the elements. Now make sure the container_type is correctly created.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Checks relating to the integrity of the container_type....
			BOOST_CHECK(!s.empty());
			BOOST_CHECK_EQUAL(s.size(), 4*num_items);	// O.k. the atomic-counter based size() is correct.
			BOOST_CHECK_EQUAL(s.size_n(), 4*num_items);	// Woo-hoo so is the actual size of the list in nodes.
			BOOST_CHECK(s.tail_reachable_from_head());
			// Check that the reference count of each of the member-elements is correct.
			for (auto const &iter : s) {
				BOOST_CHECK_GE(iter->sp_count(), 1);	// Must be greater than zero, for the reference in the list. The exact value will be related to the implementation of "for (...)" and the BOOST_... method.
				BOOST_CHECK_EQUAL(iter->sp_count(), 3);	// Check that there is no difference between the reference counts on each node, as they should all be the same.
			}
			{
				std::set<std::size_t> check_list_integrity;
				std::transform(
					s.begin(),
					s.end(),
					std::inserter(check_list_integrity, check_list_integrity.begin()),
					[](container_type::value_type const &v) {
						return v->i;
					}
				);
				BOOST_CHECK_EQUAL(check_list_integrity.size(), 4*num_items);	// O.k. the correct number of elements were actually inserted, i.e. no incorrectly duplicated elements (each element is distinguishable via the address and the member "i").
			}
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-slist pop_front() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(pop_front)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif

	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			container_type::size_type elem=0;
			std::chrono::high_resolution_clock::time_point t1, t2;
			std::generate_n(
				std::front_inserter(s),
				4*num_items,
				[&elem]() -> container_type::value_type {
					// Ensure that each element in the container is distinguishable.
					return container_type::value_type(new container_type::value_type::value_type(++elem));
				}
			);
			{
				// Delete all of the elements from the container_type in parallel.
				pop_thread<container_type> thread1(num_items, s);
				pop_thread<container_type> thread2(num_items, s);
				pop_thread<container_type> thread3(num_items, s);
				pop_thread<container_type> thread4(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					thread1.create_running();
					thread2.create_running();
					thread3.create_running();
					thread4.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (thread1.is_running() || thread2.is_running() || thread3.is_running() || thread4.is_running());
					// container_type should now be empty.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Check the container_type is actually empty...
			BOOST_CHECK(s.empty());
			BOOST_CHECK_EQUAL(s.size(), container_type::size_type());	// O.k. the atomic-counter based size() is correct.
			BOOST_CHECK_EQUAL(s.size_n(), container_type::size_type());	// Woo-hoo so is the actual size of the list in nodes.
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}
/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-slist push_front() and pop_front() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push_front_and_pop_front) {
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif
	
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
			#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
			#else
			const container_type::size_type num_items=1000;
			#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::front_insert_iterator> add_thread1(num_items, s);
				add_thread<2, container_type, std::front_insert_iterator> add_thread2(num_items, s);
				// And race deleting them all...
				pop_thread<container_type> pop_thread1(num_items, s);
				pop_thread<container_type> pop_thread2(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					add_thread1.create_running();
					pop_thread1.create_running();
					add_thread2.create_running();
					pop_thread2.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (add_thread1.is_running() || add_thread2.is_running() || pop_thread1.is_running() || pop_thread2.is_running());
					// Added all of the elements. Now make sure the container_type is correctly created.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Checks relating to the integrity of the container_type....
			BOOST_CHECK_LT(s.size(), 2*num_items);
			BOOST_CHECK_LT(s.size_n(), 2*num_items);
			BOOST_CHECK_EQUAL(s.size(), s.size_n());
			BOOST_CHECK(s.tail_reachable_from_head());
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-slist push_back() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push_back)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif
	
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::back_insert_iterator> thread1(num_items, s);
				add_thread<2, container_type, std::back_insert_iterator> thread2(num_items, s);
				add_thread<3, container_type, std::back_insert_iterator> thread3(num_items, s);
				add_thread<4, container_type, std::back_insert_iterator> thread4(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					thread1.create_running();
					thread2.create_running();
					thread3.create_running();
					thread4.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (thread1.is_running() || thread2.is_running() || thread3.is_running() || thread4.is_running());
					// Added all of the elements. Now make sure the container_type is correctly created.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Checks relating to the integrity of the container_type....
			BOOST_CHECK(!s.empty());
			BOOST_CHECK_EQUAL(s.size(), 4*num_items);	// O.k. the atomic-counter based size() is correct.
			BOOST_CHECK_EQUAL(s.size_n(), 4*num_items);	// Woo-hoo so is the actual size of the list in nodes.
			BOOST_CHECK(s.tail_reachable_from_head());
			// Check that the reference count of each of the member-elements is correct.
			for (auto const &iter : s) {
				BOOST_CHECK_GE(iter->sp_count(), 1);	// Must be greater than zero, for the reference in the list. The exact value will be related to the implementation of "for (...)" and the BOOST_... method.
				BOOST_CHECK_EQUAL(iter->sp_count(), 3);	// Check that there is no difference between the reference counts on each node, as they should all be the same.
			}
			{
				std::set<std::size_t> check_list_integrity;
				std::transform(
					s.begin(),
					s.end(),
					std::inserter(check_list_integrity, check_list_integrity.begin()),
					[](container_type::value_type const &v) {
						return v->i;
					}
				);
				BOOST_CHECK_EQUAL(check_list_integrity.size(), 4*num_items);	// O.k. the correct number of elements were actually inserted, i.e. no incorrectly duplicated elements (each element is distinguishable via the address and the member "i").
			}
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-slist push_back() and push_front() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push_back_and_push_front)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif
	
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::back_insert_iterator> thread1(num_items, s);
				add_thread<2, container_type, std::front_insert_iterator> thread2(num_items, s);
				add_thread<3, container_type, std::back_insert_iterator> thread3(num_items, s);
				add_thread<4, container_type, std::front_insert_iterator> thread4(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					thread1.create_running();
					thread2.create_running();
					thread3.create_running();
					thread4.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (thread1.is_running() || thread2.is_running() || thread3.is_running() || thread4.is_running());
					// Added all of the elements. Now make sure the container_type is correctly created.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Checks relating to the integrity of the container_type....
			BOOST_CHECK(!s.empty());
			BOOST_CHECK_EQUAL(s.size(), 4*num_items);	// O.k. the atomic-counter based size() is correct.
			BOOST_CHECK_EQUAL(s.size_n(), 4*num_items);	// Woo-hoo so is the actual size of the list in nodes.
			BOOST_CHECK(s.tail_reachable_from_head());
			// Check that the reference count of each of the member-elements is correct.
			for (auto const &iter : s) {
				BOOST_CHECK_GE(iter->sp_count(), 1);	// Must be greater than zero, for the reference in the list. The exact value will be related to the implementation of "for (...)" and the BOOST_... method.
				BOOST_CHECK_EQUAL(iter->sp_count(), 3);	// Check that there is no difference between the reference counts on each node, as they should all be the same.
			}
			{
				std::set<std::size_t> check_list_integrity;
				std::transform(
					s.begin(),
					s.end(),
					std::inserter(check_list_integrity, check_list_integrity.begin()),
					[](container_type::value_type const &v) {
						return v->i;
					}
				);
				BOOST_CHECK_EQUAL(check_list_integrity.size(), 4*num_items);	// O.k. the correct number of elements were actually inserted, i.e. no incorrectly duplicated elements (each element is distinguishable via the address and the member "i").
			}
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-slist push_back() and pop_front() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push_back_and_pop_front)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif
	
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::back_insert_iterator> add_thread1(num_items, s);
				add_thread<2, container_type, std::front_insert_iterator> add_thread2(num_items, s);
				// And race deleting them all...
				pop_thread<container_type> pop_thread1(num_items, s);
				pop_thread<container_type> pop_thread2(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					add_thread1.create_running();
					pop_thread1.create_running();
					add_thread2.create_running();
					pop_thread2.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (add_thread1.is_running() || add_thread2.is_running() || pop_thread1.is_running() || pop_thread2.is_running());
					// Added all of the elements. Now make sure the container_type is correctly created.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Checks relating to the integrity of the container_type....
			BOOST_CHECK_LT(s.size(), 2*num_items);
			BOOST_CHECK_LT(s.size_n(), 2*num_items);
			BOOST_CHECK_EQUAL(s.size(), s.size_n());
			BOOST_CHECK(s.tail_reachable_from_head());
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

/**
	\test <a href="./examples/intrusive_parallel.svg">Graph</a> of performance results for heavyweight intrusive-slist push_back(), pop_front() and push_front() operations.
			==========================================================================================
	Results for 100*10000 repetitions.
*/
BOOST_AUTO_TEST_CASE(push_back_pop_front_and_push_front)
{
#ifdef JMMCG_PERFORMANCE_TESTS
	const container_type::size_type num_tests=100;
#else
	const container_type::size_type num_tests=1;
#endif
	
	const std::pair<timed_results_t, bool> timed_results(compute_average_deviation<timed_results_t::value_type>(
		2.0,
		num_tests,
		[]() {
#ifdef JMMCG_PERFORMANCE_TESTS
			const container_type::size_type num_items=10000;
#else
			const container_type::size_type num_items=1000;
#endif
			container_type s;
			std::chrono::high_resolution_clock::time_point t1, t2;
			{
				// Add a shed-load of elements in parallel to an empty container_type.
				add_thread<1, container_type, std::back_insert_iterator> add_thread1(num_items, s);
				add_thread<2, container_type, std::front_insert_iterator> add_thread2(num_items, s);
				// And race deleting them all...
				pop_thread<container_type> pop_thread1(num_items, s);
				pop_thread<container_type> pop_thread2(num_items, s);
				{
					t1=std::chrono::high_resolution_clock::now();
					add_thread1.create_running();
					pop_thread1.create_running();
					add_thread2.create_running();
					pop_thread2.create_running();
					do {
						api_threading_traits<platform_api, heavyweight_threading>::sleep(100);
					} while (add_thread1.is_running() || add_thread2.is_running() || pop_thread1.is_running() || pop_thread2.is_running());
					// Added all of the elements. Now make sure the container_type is correctly created.
					t2=std::chrono::high_resolution_clock::now();
				}
			}
			// Checks relating to the integrity of the container_type....
			BOOST_CHECK_LT(s.size(), 2*num_items);
			BOOST_CHECK_LT(s.size_n(), 2*num_items);
			BOOST_CHECK_EQUAL(s.size(), s.size_n());
			BOOST_CHECK(s.tail_reachable_from_head());
			return timed_results_t::value_type(4*1000000/static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()))/num_items;
		}
	));
#ifdef JMMCG_PERFORMANCE_TESTS
	stats_to_csv::handle->stats<<timed_results.first.to_csv()<<std::flush;
	BOOST_WARN_MESSAGE(!timed_results.second, "Failed to converge for measuring performance.");
#endif
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
