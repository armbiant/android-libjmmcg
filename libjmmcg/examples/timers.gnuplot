## $Header$
##
## Copyright (c) 2016 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

set macros
datafile = 'timers.csv'
set datafile separator '|'
set title "Comparison of performance of timers.\nLinux-4.1.15-gentoo, x86\_64, RELEASE, glibc v2.21-r2 p7\nError-bars: % average deviation." noenhance
firstrow = system("head -n1 " . datafile . " | sed 's/ /_/g' | sed 's/|/ /g'")
set xlabel word(firstrow, 1) noenhance
set ylabel word(firstrow, 34) noenhance
set key autotitle columnhead noenhance
set key font ",7" noenhance font "Helvetica,20"
set xtics font ",9" noenhance font "Helvetica,20"
set ytics font ",9" noenhance font "Helvetica,20"
set rmargin at screen 0.95
set bmargin at screen 0.14
set term svg size 1400,1024 noenhance font "Helvetica,20"
set output 'timers.svg'
plot	\
	datafile using 1:2:($2*$3) with yerrorbars linecolor 1,	\
	datafile using 1:2:xticlabels(1) notitle with lines linecolor 1,	\
	datafile using 1:4:($4*$5) with yerrorbars linecolor 2,	\
	datafile using 1:4:xticlabels(1) notitle with lines linecolor 2,	\
	datafile using 1:6:($6*$7) with yerrorbars linecolor 3,	\
	datafile using 1:6:xticlabels(1) notitle with lines linecolor 3,	\
	datafile using 1:8:($8*$9) with yerrorbars linecolor 4,	\
	datafile using 1:8:xticlabels(1) notitle with lines linecolor 4,	\
	datafile using 1:10:($10*$11) with yerrorbars linecolor 5,	\
	datafile using 1:10:xticlabels(1) notitle with lines linecolor 5,	\
	datafile using 1:12:($12*$13) with yerrorbars linecolor 6,	\
	datafile using 1:12:xticlabels(1) notitle with lines linecolor 6,	\
	datafile using 1:14:($14*$15) with yerrorbars linecolor 7,	\
	datafile using 1:14:xticlabels(1) notitle with lines linecolor 7,	\
	datafile using 1:16:($16*$17) with yerrorbars linecolor 8,	\
	datafile using 1:16:xticlabels(1) notitle with lines linecolor 8,	\
	datafile using 1:18:($18*$19) with yerrorbars linecolor 9,	\
	datafile using 1:18:xticlabels(1) notitle with lines linecolor 9,	\
	datafile using 1:20:($20*$21) with yerrorbars linecolor 10,	\
	datafile using 1:20:xticlabels(1) notitle with lines linecolor 10,	\
	datafile using 1:22:($22*$23) with yerrorbars linecolor 11,	\
	datafile using 1:22:xticlabels(1) notitle with lines linecolor 11,	\
	datafile using 1:24:($24*$25) with yerrorbars linecolor 12,	\
	datafile using 1:24:xticlabels(1) notitle with lines linecolor 12,	\
	datafile using 1:26:($26*$27) with yerrorbars linecolor 13,	\
	datafile using 1:26:xticlabels(1) notitle with lines linecolor 13,	\
	datafile using 1:28:($28*$29) with yerrorbars linecolor 14,	\
	datafile using 1:28:xticlabels(1) notitle with lines linecolor 14,	\
	datafile using 1:30:($30*$31) with yerrorbars linecolor 15,	\
	datafile using 1:30:xticlabels(1) notitle with lines linecolor 15,	\
	datafile using 1:32:($32*$33) with yerrorbars linecolor 16,	\
	datafile using 1:32:xticlabels(1) notitle with lines linecolor 16
