/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include "core/stack_string.hpp"

using namespace libjmmcg;

using stack_string_12=basic_stack_string<12, char>;

BOOST_AUTO_TEST_SUITE(string_tests)

BOOST_AUTO_TEST_SUITE(mixed_size_strings)

BOOST_AUTO_TEST_CASE(ctor_swap)
{
	char const src1[]="small";
	char const src2[]="a very big string";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s1.swap(s2);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src2)-1);
	BOOST_CHECK(std::equal(src2, src2+sizeof(src2), s1.begin()));
	BOOST_CHECK_GE(s1.capacity(), sizeof(src2)-1);
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src1)-1);
	BOOST_CHECK(std::equal(src1, src1+sizeof(src1), s2.begin()));
	BOOST_CHECK_GE(s2.capacity(), sizeof(src1)-1);
}

BOOST_AUTO_TEST_CASE(small_big_ctor_assignment)
{
	char const src1[]="small";
	char const src2[]="a very big string";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), s2.begin()));
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(big_small_ctor_assignment)
{
	char const src1[]="a very big string";
	char const src2[]="small";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s2=s1;
	BOOST_CHECK_EQUAL(s1.empty(), s2.empty());
	BOOST_CHECK_EQUAL(s1.size(), s2.size());
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), s2.begin()));
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(small_big_ctor_move_assignment)
{
	char const src1[]="small";
	char const src2[]="a very big string";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s2=std::move(s1);
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src1)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src1));
	BOOST_CHECK_GE(s1.capacity(), s2.capacity());
}

BOOST_AUTO_TEST_CASE(big_small_ctor_move_assignment)
{
	char const src1[]="a very big string";
	char const src2[]="small";
	stack_string_12 s1(src1);
	stack_string_12 s2(src2);
	s2=std::move(s1);
	BOOST_CHECK(!s1.empty());
	BOOST_CHECK_EQUAL(s1.size(), sizeof(src2)-1);
	BOOST_CHECK(std::equal(s1.begin(), s1.end(), src2));
	BOOST_CHECK_GE(s1.capacity(), s1.size());
	BOOST_CHECK(!s2.empty());
	BOOST_CHECK_EQUAL(s2.size(), sizeof(src1)-1);
	BOOST_CHECK(std::equal(s2.begin(), s2.end(), src1));
	BOOST_CHECK_GE(s2.capacity(), s2.size());
}

BOOST_AUTO_TEST_CASE(reserve_big_to_small_str)
{
	char const src[]="a very big string";
	const stack_string_12::size_type res(stack_string_12::small_string_max_size-1);
	stack_string_12 s(src);
	s.reserve(res);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(reserve_small_to_big_str)
{
	char const src[]="small";
	const stack_string_12::size_type res(stack_string_12::small_string_max_size+1);
	stack_string_12 s(src);
	s.reserve(res);
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(src)-1);
	BOOST_CHECK_GE(s.capacity(), res);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_smaller)
{
	char const src[]="a very big string";
	const stack_string_12::size_type res(stack_string_12::small_string_max_size-1);
	stack_string_12 s(src);
	s.resize(res);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_bigger)
{
	char const src[]="small";
	const stack_string_12::size_type res(stack_string_12::small_string_max_size+1);
	stack_string_12 s(src);
	s.resize(res);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(src, src+sizeof(src), s.begin()));
}

BOOST_AUTO_TEST_CASE(resize_smaller_init)
{
	const stack_string_12::value_type fill='X';
	char const src[]="a very big string";
	const stack_string_12::size_type res(stack_string_12::small_string_max_size-1);
	stack_string_12 s(src);
	s.resize(res, fill);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), sizeof(src)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), src));
}

BOOST_AUTO_TEST_CASE(resize_bigger_init)
{
	const stack_string_12::value_type fill='X';
	char const src[]="small";
	char const result[]="smallXXXXXXXXXXXX";
	BOOST_CHECK_GT(sizeof(result)-1, stack_string_12::small_string_max_size);
	const stack_string_12::size_type res(stack_string_12::small_string_max_size+1);
	stack_string_12 s(src);
	s.resize(res, fill);
	BOOST_CHECK_EQUAL(s.size(), res);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(push_back)
{
	const stack_string_12::value_type fill='X';
	char const src[]="smallXXXXXXXXXXX";
	char const result[]="smallXXXXXXXXXXXX";
	BOOST_CHECK_LE(sizeof(src)-1, stack_string_12::small_string_max_size);
	BOOST_CHECK_GT(sizeof(result)-1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	s.push_back(fill);
	BOOST_CHECK_EQUAL(s.size(), sizeof(src));
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
}

BOOST_AUTO_TEST_CASE(insert_str_end)
{
	const stack_string_12 src("smallXXXXXX");
	const char result[]="smallXXXXXXsmallXXXXXX";
	BOOST_CHECK_LT(src.size(), stack_string_12::small_string_max_size);
	BOOST_CHECK_GT(sizeof(result)-1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	const stack_string_12::iterator i=s.insert(s.end(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin()+src.size());
}

BOOST_AUTO_TEST_CASE(insert_str_begin)
{
	const stack_string_12 src("smallXXXXXX");
	const char result[]="smallXXXXXXsmallXXXXXX";
	BOOST_CHECK_LT(src.size(), stack_string_12::small_string_max_size);
	BOOST_CHECK_GT(sizeof(result)-1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	const stack_string_12::iterator i=s.insert(s.begin(), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(insert_str_internal)
{
	const stack_string_12 src("smallXXXXXX");
	const char result[]="ssmallXXXXXXmallXXXXXX";
	BOOST_CHECK_LT(src.size(), stack_string_12::small_string_max_size);
	BOOST_CHECK_GT(sizeof(result)-1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	const stack_string_12::iterator i=s.insert(std::next(s.begin()), src.begin(), src.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK_GE(s.capacity(), s.size());
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin()));
}

BOOST_AUTO_TEST_CASE(erase_begin_one)
{
	char const src[]="smallXXXXXXXXXXXX";
	char const result[]="mallXXXXXXXXXXXX";
	BOOST_CHECK_GT(sizeof(src)-1, stack_string_12::small_string_max_size);
	BOOST_CHECK_LE(sizeof(result)-1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	const stack_string_12::iterator i=s.erase(s.begin(), std::next(s.begin()));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.begin());
}

BOOST_AUTO_TEST_CASE(erase_end_one)
{
	char const src[]="smallXXXXXXXXXXXX";
	char const result[]="smallXXXXXXXXXXX";
	BOOST_CHECK_GT(sizeof(src)-1, stack_string_12::small_string_max_size);
	BOOST_CHECK_LE(sizeof(result)-1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	const stack_string_12::iterator i=s.erase(std::prev(s.end()), s.end());
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, s.end());
}

BOOST_AUTO_TEST_CASE(erase_middle_one)
{
	char const src[]=   "smallXXXXXXXXXXXX";
	char const result[]="smllXXXXXXXXXXXX";
	BOOST_CHECK_GT(sizeof(src)-1, stack_string_12::small_string_max_size);
	BOOST_CHECK_LE(sizeof(result)-1, stack_string_12::small_string_max_size);
	stack_string_12 s(src);
	const stack_string_12::iterator i=s.erase(std::next(s.begin(), 2), std::next(s.begin(), 3));
	BOOST_CHECK(!s.empty());
	BOOST_CHECK_EQUAL(s.size(), sizeof(result)-1);
	BOOST_CHECK(std::equal(s.begin(), s.end(), result));
	BOOST_CHECK_EQUAL(i, std::next(s.begin(), 2));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
