/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "stdafx.h"

#define BOOST_TEST_MODULE libjmmcg_tests
#include <boost/test/included/unit_test.hpp>

#include "core/bogo_sort.hpp"

#include <algorithm>
#include <vector>

using namespace libjmmcg;

typedef std::vector<int> colln_t;

BOOST_AUTO_TEST_SUITE(bogo_sort_tests)

BOOST_AUTO_TEST_CASE(empty)
{
	colln_t c;
	BOOST_CHECK_NO_THROW(bogo_sort(c.begin(), c.end(), std::less<colln_t::value_type>()));
}

BOOST_AUTO_TEST_CASE(sort_one)
{
	colln_t c{1};
	colln_t const c_cpy(c);
	BOOST_CHECK_NO_THROW(bogo_sort(c.begin(), c.end(), std::less<colln_t::value_type>()));
	BOOST_CHECK(c==c_cpy);
}

BOOST_AUTO_TEST_CASE(sort_two_sorted)
{
	colln_t c{1, 2};
	colln_t const c_cpy(c);
	BOOST_CHECK_NO_THROW(bogo_sort(c.begin(), c.end(), std::less<colln_t::value_type>()));
	BOOST_CHECK(c==c_cpy);
}

BOOST_AUTO_TEST_CASE(sort_two_reversed)
{
	colln_t c{2, 1};
	colln_t c_cpy(c);
	std::sort(c_cpy.begin(), c_cpy.end());
	BOOST_CHECK_NO_THROW(bogo_sort(c.begin(), c.end(), std::less<colln_t::value_type>()));
	BOOST_CHECK(c==c_cpy);
}

BOOST_AUTO_TEST_CASE(sort_same)
{
	colln_t c{1, 1, 1, 1};
	colln_t const c_cpy(c);
	BOOST_CHECK_NO_THROW(bogo_sort(c.begin(), c.end(), std::less<colln_t::value_type>()));
	BOOST_CHECK(c==c_cpy);
}

BOOST_AUTO_TEST_CASE(sort_sorted)
{
	colln_t c{1, 2, 3, 4};
	colln_t const c_cpy(c);
	BOOST_CHECK_NO_THROW(bogo_sort(c.begin(), c.end(), std::less<colln_t::value_type>()));
	BOOST_CHECK(c==c_cpy);
}

BOOST_AUTO_TEST_CASE(sort_reversed)
{
	colln_t c{4, 3, 2, 1};
	colln_t c_cpy(c);
	std::sort(c_cpy.begin(), c_cpy.end());
	BOOST_CHECK_NO_THROW(bogo_sort(c.begin(), c.end(), std::less<colln_t::value_type>()));
	BOOST_CHECK(c==c_cpy);
}

BOOST_AUTO_TEST_SUITE_END()
