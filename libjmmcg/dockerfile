## Copyright © 2023 by J.M.McGuiness, libjmmcg@hussar.me.uk
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2.1 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

FROM debian:sid

LABEL version="build_615_git_236_g23341"

RUN set -ex; \
	apt update \
	&& apt -y upgrade \
	&& apt-get -y install bash \
	    build-essential \
	    ca-certificates \
		cmake>=3.27.7 \
		coreutils \
		cppcheck \
		curl>=8.4.0 \
		g++>=13.2.1 \
		git>=2.41.0 \
		gnuplot \
		libbacktrace \
		libboost-all-dev>=1.82.0 \
        libfmt9>=9.1.0 \
        libssl-dev>=3.0.12 \
		lzma \
		ninja \
		numactl \
		p7zip-full \
		rpm \
		systemd-timesyncd \
		tar \
		valgrind \
		wget \
		xz-utils \
	&& update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-13.2.1 100
	&& rm -rf /var/lib/apt/lists/*

RUN set -ex; \
	mkdir -p build/test-results && cd build \
	&& cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=dist -j `nproc` .. \
	&& cmake --build . -j `nproc` \
	&& cmake --install . \
	&& cpack . \
	&& ctest -j `nproc` . \
	&& cppcheck --cppcheck-build-dir=test-results --enable=all -j `nproc` --project=compile_commands.json --output-file=test-results/report.xml --xml

ENTRYPOINT ["server"]
