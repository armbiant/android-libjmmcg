#ifndef LIBJMMCG_CORE_FILE_HPP
#define LIBJMMCG_CORE_FILE_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
	\file Wrap up into a class creating a file, so that it is deleted upon object destruction. This class is completely ANSI.
*/

#include "exception.hpp"

#include <cassert>
#include <fstream>
#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

typedef std::basic_fstream<tchar> tfstream;
typedef std::basic_ifstream<tchar> tifstream;
typedef std::basic_ofstream<tchar> tofstream;

/**
	This templating is done to generalise this class. Why? There are ASCII mode files and binary ones. ASCII mode translates the CR/LF combinations. Binary does not. Also it allows this class to manage Unicode character sets. (One that use double bytes for the character set.)
*/
template<typename basic_file_type, ppd::generic_traits::api_type::element_type API_, typename Mdl_>
class file : public basic_file_type {
public:
	typedef basic_file_type file_type;

	[[nodiscard]] file(const tstring&, const std::ios_base::openmode, const bool= true);
	file(file const&)= delete;
	virtual ~file();
	[[nodiscard]] file& operator=(const file&);
	// Why is "false" always returned? That's because comparing two files
	// is non obvious: Do I compare the file names & the contents? What
	// about the path to the two files? One may be relative, the other
	// may be absolute. Even if they are the *same* file, but created with
	// the names with these different paths they'll compare different,
	// unless I do somethig very tricky with the path situation.
	// Why am I going on about this? Well what if you want to create a set
	// (or map) of these "File<...>" objects you could fall foul of the
	// fact that they require "operator<(...)" to be defined. This in turn
	// implies that "operator==(...)" is defined to do something sensible.
	// If you don't like this, derive from this class and override the
	// function.
	virtual const bool operator==(const file&) const noexcept(true) {
		return false;
	}

	const tstring& name(void) const noexcept(true) {
		return fname;
	}

private:
	bool auto_delete;
	tstring fname;
	std::ios_base::openmode mode;

	void check_open(void);
	void remove(void);

	// I don't allow copying.
	// Why? Well I need the file name for the current object to put the
	// data from the input file object. How do I pass in this file name
	// when the copy constructor only takes one parameter, and there's
	// no way to pass in the destination file name. That's why I don't
	// allow this.
	explicit file(const tstring&) noexcept(true);
};

template<typename basic_file_type, ppd::generic_traits::api_type::element_type API_, typename Mdl_>
inline file<basic_file_type, API_, Mdl_>::file(const tstring& nm, const std::ios_base::openmode flags, const bool ad)
	: basic_file_type(nm.c_str(), flags), auto_delete(ad), fname(nm), mode(flags) {
	check_open();
}

template<typename basic_file_type, ppd::generic_traits::api_type::element_type API_, typename Mdl_>
inline file<basic_file_type, API_, Mdl_>::~file(void) {
	if(auto_delete) {
		remove();
	}
}

template<typename basic_file_type, ppd::generic_traits::api_type::element_type API_, typename Mdl_>
inline file<basic_file_type, API_, Mdl_>&
file<basic_file_type, API_, Mdl_>::operator=(const file<basic_file_type, API_, Mdl_>& tf) {
	remove();
	open(tf.fname, mode|= std::ios_base::out);
	check_open();
	file_type::operator<<(tf.rdbuf());
	return *this;
}

template<typename basic_file_type, ppd::generic_traits::api_type::element_type API_, typename Mdl_>
inline void
file<basic_file_type, API_, Mdl_>::check_open(void) noexcept(false) {
	if(!file_type::is_open()) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to open the specified file. file name='{}', open mode={}", fname, mode), &file::check_open));
	}
}

template<typename basic_file_type, ppd::generic_traits::api_type::element_type API_, typename Mdl_>
inline void
file<basic_file_type, API_, Mdl_>::remove(void) noexcept(false) {
	file_type::close();
	if(::remove(fname.c_str())) {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to delete the specified file. file name='{}', open mode={}", fname, mode), &file::remove));
	}
}

}}

#endif
