#ifndef LIBJMMCG_CORE_ssl_socket_wrapper_glibc_hpp
#define LIBJMMCG_CORE_ssl_socket_wrapper_glibc_hpp

/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "deleter.hpp"
#include "socket_wrapper_glibc.hpp"

#include <memory>
#include <string>
#include <vector>

#include <netdb.h>

#include <openssl/err.h>
#include <openssl/ssl.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace syscall { namespace private_ {

template<>
struct failure_code<ssl_st const*> {
	static inline constexpr ssl_st const* const value= nullptr;
};
template<>
struct failure_code<ssl_st*> {
	static inline constexpr ssl_st* const value= nullptr;
};
template<>
struct failure_code<SSL_CTX const*> {
	static inline constexpr SSL_CTX const* const value= nullptr;
};
template<>
struct failure_code<SSL_CTX*> {
	static inline constexpr SSL_CTX* const value= nullptr;
};

template<>
struct failure_detection<ssl_st const*> {
	static constexpr bool result(ssl_st const* ret_code, ssl_st const* fv) noexcept(true) {
		return ret_code == fv;
	}
};
template<>
struct failure_detection<ssl_st*> {
	static constexpr bool result(ssl_st* ret_code, ssl_st* fv) noexcept(true) {
		return ret_code == fv;
	}
};
template<>
struct failure_detection<SSL_CTX const*> {
	static constexpr bool result(SSL_CTX const* ret_code, SSL_CTX const* fv) noexcept(true) {
		return ret_code == fv;
	}
};
template<>
struct failure_detection<SSL_CTX*> {
	static constexpr bool result(SSL_CTX* ret_code, SSL_CTX* fv) noexcept(true) {
		return ret_code == fv;
	}
};

}}

namespace socket { namespace glibc { namespace client { namespace ssl {

/// A simple wrapper for SSL-over-TCP connections.
template<class LkT>
class wrapper {
public:
	using socket_t= socket::glibc::client::wrapper<LkT>;
	using domain_t= typename socket_t::domain_t;
	using type_t= typename socket_t::type_t;
	using thread_traits= typename socket_t::thread_traits;

	enum verify_SSL_certificates : int {
		none= SSL_VERIFY_NONE,
		peer= SSL_VERIFY_PEER
	};

	const type_t type_;
	const domain_t domain_;
	const verify_SSL_certificates verify_SSL_;

	explicit wrapper(type_t type= type_t::stream, domain_t domain= domain_t::ip_v4, verify_SSL_certificates verify= verify_SSL_certificates::peer);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<
		class MsgT	 ///< The type of the message to write.
		>
	void write(MsgT const& message) noexcept(false);

	/// Write the whole message to the socket in one go.
	/**
		\param	message	The message to write, that must be as-if a POD.
	*/
	template<class V, std::size_t N>
	void write(std::array<V, N> const& message) noexcept(false);

	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this buffer, which may be grown to accommodate the message.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<
		class MsgT	 ///< The type of the message to read, that must be as-if a POD.
		>
	[[nodiscard]] bool
	read(MsgT& dest) noexcept(false);
	/// Read the whole message from the socket in one go.
	/**
		\param	dest	The message will be placed into this stack-based buffer, which must be sufficiently large to accommodate the message read, otherwise UB will result.
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<class V, std::size_t SrcSz>
	[[nodiscard]] bool
		read(V (&dest)[SrcSz]) noexcept(false);

	/**
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	template<class MsgDetails, class V, std::size_t N>
	[[nodiscard]] bool
	read(std::array<V, N>& buff) noexcept(false);

	/// Synchronously connect to the specified host using the specified service.
	/**
	 * This call merely autheticates that the server to which it is connecting has correct credentials.
	 *
	 * \Note No attempt is made to autheticate against an SSH private key.
	 *
	 * \param	hostname	The hostname to which an SSL connection should be made. E.g. "duckduckgo.com"
	 * \param	service	The service requirested. Usually the SSL variant, e.g. "HTTPS" or "443".
	 */
	void connect(std::string const& hostname, std::string const& service) noexcept(false);
	[[nodiscard]] bool is_open() const noexcept(false);
	void close() noexcept(false);

	/// Set various options on the created socket to ensure more optimal behaviour.
	/**
	 * \todo Consider setting MSG_ZEROCOPY.
	 * \param timeout	In seconds.
	 */
	void set_options(std::size_t min_message_size, std::size_t max_message_size, int timeout, socket_priority priority, std::size_t incoming_cpu) noexcept(false);

	[[nodiscard]] std::string local_ip() const noexcept(false);

private:
	using atomic_t= typename socket_t::atomic_t;
	using write_lock_t= typename socket_t::write_lock_t;
	using socket_type= typename socket_t::socket_type;

	class init_SSL_t;
	class addrs_info_t;
	class SSL_context_wrapper;
	class SSL_socket_wrapper;

	mutable atomic_t mutex_;
	const init_SSL_t init_SSL;
	std::vector<std::shared_ptr<socket_t>> underlying_connections_;
	SSL_context_wrapper ctx_;
	std::unique_ptr<SSL_socket_wrapper> ssl_;

	/**
		\return False if all of the message was successfully read into the buffer, true otherwise.
	*/
	[[nodiscard]] bool read_msg(std::byte* dest, std::size_t msg_size) noexcept(false);
};

}}}}
}}

#include "ssl_socket_wrapper_glibc_impl.hpp"

#endif
