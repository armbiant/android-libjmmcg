/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	template<typename MeteredObjType>
	inline tostream & __fastcall
	operator<<(tostream &os, ave_deviation_meter<MeteredObjType> const &p) noexcept(false) {
		os<<p.to_string();
		return os;
	}

	template<class MeteredObjType, class Fn>
	[[nodiscard]] inline ave_deviation_meter<MeteredObjType>
	estimate_average_deviation(typename ave_deviation_meter<MeteredObjType>::value_type const computations, Fn fn) {
		ave_deviation_meter<MeteredObjType> meter(fn.operator()());
		for (auto i=typename ave_deviation_meter<MeteredObjType>::value_type(); i<computations; ++i) {
			meter.update(fn.operator()());
		}
		return meter;
	}

	template<class MeteredObjType, class Fn>
	[[nodiscard]] inline std::pair<ave_deviation_meter<MeteredObjType>, bool>
	compute_average_deviation(double const target_deviation, typename ave_deviation_meter<MeteredObjType>::value_type const computations, Fn fn) {
		std::pair<ave_deviation_meter<MeteredObjType>, bool> meter(ave_deviation_meter<MeteredObjType>(fn.operator()()), true);
		for (auto i=typename ave_deviation_meter<MeteredObjType>::value_type(); i<computations; ++i) {
			meter.first.update(fn.operator()());
			if (meter.first.deviation_percentage()<=target_deviation) {
				meter.second=false;
				break;
			}
		}
		return meter;
	}

	template<typename MeteredObjType> inline void
	ave_deviation_meter<MeteredObjType>::invariant() const noexcept(true) {
#ifndef _REENTRANT
		assert(num_samples_>0 || ((minimum_>=std::numeric_limits<value_type>::max()) && (maximum_<=std::numeric_limits<value_type>::min()) && num_samples_==0));
		assert((minimum_>=std::numeric_limits<value_type>::max()) || (minimum_<=arithmetic_mean_));
		assert((maximum_<=std::numeric_limits<value_type>::min()) || (arithmetic_mean_<=maximum_));
		assert(maximum_<=total_);
//			assert(((minimum_>=std::numeric_limits<value_type>::max()) || (minimum_<=arithmetic_mean_)) || (fprintf(stdout, "minimum_=%lu, arithmetic_mean_=%g, num_samples_=%lu, cond=%i\n", minimum_, arithmetic_mean_, num_samples_, ((minimum_>=std::numeric_limits<value_type>::max()) || (minimum_<=arithmetic_mean_))) && 0));
//			assert(((maximum_<=std::numeric_limits<value_type>::min()) || (arithmetic_mean_<=maximum_)) || (fprintf(stdout, "maximum_=%lu\n", maximum_) && 0));
//			assert((maximum_<=total_) || (fprintf(stdout, "total_=%lu\n", total_) && 0));
#endif
	}

	template<typename MeteredObjType> inline constexpr
	ave_deviation_meter<MeteredObjType>::ave_deviation_meter() noexcept(true)
	: num_samples_(),
		minimum_(std::numeric_limits<value_type>::max()),
		maximum_(std::numeric_limits<value_type>::min()),
		total_(),
		arithmetic_mean_(),
		ave_deviation() {
	}

	template<typename MeteredObjType> inline constexpr
	ave_deviation_meter<MeteredObjType>::ave_deviation_meter(const value_type val) noexcept(true)
	: num_samples_(1),
		minimum_(val), maximum_(val),
		total_(val),
		arithmetic_mean_(static_cast<double>(val)),
		ave_deviation(std::abs(arithmetic_mean_)) {
	}

	template<typename MeteredObjType> inline constexpr
	ave_deviation_meter<MeteredObjType>::ave_deviation_meter(value_type const val, double const dev) noexcept(true)
	: num_samples_(1),
		minimum_(val), maximum_(val),
		total_(val),
		arithmetic_mean_(static_cast<double>(val)),
		ave_deviation(dev) {
	}

	template<typename MeteredObjType> inline
	ave_deviation_meter<MeteredObjType>::ave_deviation_meter(ave_deviation_meter const &rm) noexcept(true)
	: num_samples_(rm.num_samples_),
		minimum_(rm.minimum_), maximum_(rm.maximum_),
		total_(rm.total_),
		arithmetic_mean_(rm.arithmetic_mean_),
		ave_deviation(rm.ave_deviation) {
		invariant();
	}

	template<typename MeteredObjType> inline
	ave_deviation_meter<MeteredObjType>::~ave_deviation_meter() noexcept(true) {
	}

	template<typename MeteredObjType> inline ave_deviation_meter<MeteredObjType> &
	ave_deviation_meter<MeteredObjType>::operator=(ave_deviation_meter const &rm) noexcept(true) {
		num_samples_=rm.num_samples_;
		minimum_=rm.minimum_;
		maximum_=rm.maximum_;
		total_=rm.total_;
		arithmetic_mean_=rm.arithmetic_mean_;
		ave_deviation=rm.ave_deviation;
		invariant();
		return *this;
	}

	template<typename MeteredObjType> inline ave_deviation_meter<MeteredObjType> &
	ave_deviation_meter<MeteredObjType>::operator+=(ave_deviation_meter const &rm) noexcept(true) {
		invariant();
		minimum_=std::min(minimum_, rm.minimum_);
		maximum_=std::max(maximum_, rm.maximum_);
		total_+=rm.total_;
		if (num_samples_+rm.num_samples_) {
			arithmetic_mean_=(arithmetic_mean_*static_cast<double>(num_samples_)+rm.arithmetic_mean_*rm.num_samples_)/static_cast<double>(num_samples_+rm.num_samples_);
		}
		ave_deviation=(ave_deviation+rm.ave_deviation)/2;
		num_samples_+=rm.num_samples_;
		minimum_=std::min(minimum_, rm.minimum_);
		invariant();
		return *this;
	}

	template<typename MeteredObjType> inline ave_deviation_meter<MeteredObjType> &
	ave_deviation_meter<MeteredObjType>::update(value_type const new_val) noexcept(true) {
		invariant();
		minimum_=std::min(minimum_, new_val);
		maximum_=std::max(maximum_, new_val);
		total_+=new_val;
		unsigned long const old_num_samples_=num_samples_++;
		value_type const old_arith_mean=static_cast<value_type>(arithmetic_mean_);
		arithmetic_mean_=(arithmetic_mean_*static_cast<double>(old_num_samples_)+static_cast<double>(new_val))/static_cast<double>(num_samples_);
		if (old_num_samples_) {
			ave_deviation=(std::abs(ave_deviation-(arithmetic_mean_-static_cast<double>(old_arith_mean)))*static_cast<double>(old_num_samples_)+std::abs(static_cast<double>(new_val)-arithmetic_mean_))/static_cast<double>(num_samples_);
		}
		invariant();
		return *this;
	}

	template<typename MeteredObjType> inline constexpr typename ave_deviation_meter<MeteredObjType>::value_type
	ave_deviation_meter<MeteredObjType>::min() const noexcept(true) {
		return minimum_;
	}

	template<typename MeteredObjType> inline constexpr typename ave_deviation_meter<MeteredObjType>::value_type
	ave_deviation_meter<MeteredObjType>::arithmetic_mean() const noexcept(true) {
		return static_cast<value_type>(arithmetic_mean_);
	}

	template<typename MeteredObjType> inline constexpr typename ave_deviation_meter<MeteredObjType>::value_type
	ave_deviation_meter<MeteredObjType>::max() const noexcept(true) {
		return maximum_;
	}

	template<typename MeteredObjType> inline constexpr typename ave_deviation_meter<MeteredObjType>::value_type
	ave_deviation_meter<MeteredObjType>::total_samples() const noexcept(true) {
		return total_;
	}

	template<typename MeteredObjType> inline constexpr double
	ave_deviation_meter<MeteredObjType>::deviation() const noexcept(true) {
		return ave_deviation;
	}

	template<typename MeteredObjType> inline constexpr double
	ave_deviation_meter<MeteredObjType>::deviation_percentage() const noexcept(true) {
		return ave_deviation*100/arithmetic_mean_;
	}

	template<typename MeteredObjType> inline constexpr unsigned short
	ave_deviation_meter<MeteredObjType>::percent() const noexcept(true) {
		return static_cast<unsigned short>(deviation_percentage());
	}

	template<typename MeteredObjType> inline tstring
	ave_deviation_meter<MeteredObjType>::to_string() const noexcept(false) {
		invariant();
		tostringstream os;
		os
			<<_T("[")<<minimum_<<_T(", ")<<value_type(arithmetic_mean_)<<_T(" ~(+/-")<<std::setprecision(2)<<value_type(deviation_percentage())<<_T("%), ")<<maximum_<<_T("]")
			_T(", samples=")<<num_samples_
			<<_T(", total samples=")<<total_;
		return os.str();
	}

	template<typename MeteredObjType> inline tstring
	ave_deviation_meter<MeteredObjType>::to_csv() const noexcept(false) {
		invariant();
		tostringstream os;
		os
			<<_T("|")<<value_type(arithmetic_mean_)<<_T("|")<<std::setprecision(2)<<deviation_percentage()/100;
		return os.str();
	}

} }
