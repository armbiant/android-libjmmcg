/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

template<class Sz>
struct no_statistics<Sz>::process_work_type {
	static void processed() noexcept(true) {}
	static void processed(work_added_type const) noexcept(true) {}
	constexpr static work_added_type total_samples() noexcept(true) {return work_added_type();}

	friend constexpr tostream & __fastcall operator<<(tostream &os, process_work_type const &) noexcept(true) {
		return os;
	}
};

template<class Sz>
class basic_statistics<Sz>::process_work_type {
public:
	constexpr process_work_type() noexcept(true)
	: work() {
	}
	void processed() noexcept(true) {
		++work;
	}
	void processed(work_added_type const wk) noexcept(true) {
		work+=wk;
	}
	work_added_type total_samples() const noexcept(true) {
		return work;
	}

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend tostream & __fastcall operator<<(tostream &os, process_work_type const &p) noexcept(false) {
		os<<_T("processed=")<<p.work;
		return os;
	}

private:
	work_added_type work;
};

template<class OST>
template<class Node>
class no_control_flow_graph<OST>::add_cfg_details {
public:
	struct params {
		typedef no_control_flow_graph cfg_type;
		typedef Node node_type;
		typedef cfg_type::node_property_t node_property_t;

		constexpr params() noexcept(true)=default;
		explicit constexpr params(node_property_t::value_type const *) noexcept(true) {}
		constexpr params(node_property_t::value_type const [], params const &) noexcept(true) {}
		template<class T> constexpr
		params(T const *, node_property_t::value_type const *) noexcept(true) {}
		constexpr params(cfg_type &, params const &) noexcept(true) {}
		template<class T> constexpr
		params(cfg_type &, T const *, node_property_t::value_type const []) noexcept(true) {}
		template<class T> constexpr
		params(cfg_type &, T const *, node_property_t const &) noexcept(true) {}
		template<class T> constexpr
		params(cfg_type &, T const *, params const &) noexcept(true) {}
	};

	constexpr add_cfg_details(params const &) noexcept(true) {}
	add_cfg_details(add_cfg_details const &)=delete;

	static void update_edge(node_property_t::value_type const) noexcept(true) {}

	constexpr vertex_t vertex() const noexcept(true) {
		return vertex_t();
	}
};

template<class OST>
template<class Node>
class control_flow_graph<OST>::add_cfg_details {
public:
	struct params {
		typedef control_flow_graph cfg_type;
		typedef Node node_type;
		typedef cfg_type::node_property_t node_property_t;

		cfg_type * const cfg;
		node_type const * const parent;
		node_property_t const details;

		constexpr params() noexcept(true)
		: cfg(), parent(), details() {
		}
		explicit params(node_property_t::value_type const d[]) noexcept(false)
		: cfg(), parent(), details(d) {
		}
		template<class T>
		params(T const * p, node_property_t::value_type const d[]) noexcept(false)
		: cfg(), parent(dynamic_cast<node_type const *>(p)), details(d) {
			assert(parent==nullptr || dynamic_cast<node_type const *>(parent));
		}
		params(cfg_type &c, params const &p) noexcept(false)
		: cfg(&c), parent(p.parent), details(p.details) {
			assert(parent==nullptr || dynamic_cast<node_type const *>(parent));
		}
		params(node_property_t::value_type const d[], params const &p) noexcept(false)
		: cfg(p.cfg), parent(p.parent), details(d) {
			assert(parent==nullptr || dynamic_cast<node_type const *>(parent));
		}
		template<class T>
		params(cfg_type &c, T const *p, node_property_t::value_type const d[]) noexcept(false)
		: cfg(&c), parent(dynamic_cast<node_type const *>(p)), details(d) {
			assert(parent==nullptr || dynamic_cast<node_type const *>(parent));
		}
		template<class T>
		params(cfg_type &c, T const *p, node_property_t const &d) noexcept(false)
		: cfg(&c), parent(dynamic_cast<node_type const *>(p)), details(d) {
			assert(parent==nullptr || dynamic_cast<node_type const *>(parent));
		}
		template<class T>
		params(cfg_type &c, T const *p, params const &par) noexcept(false)
		: cfg(&c), parent(dynamic_cast<node_type const *>(p)), details(par.details) {
			assert(parent==nullptr || dynamic_cast<node_type const *>(parent));
		}
	};

	add_cfg_details(params const &p) noexcept(false)
	: cfg_(p.cfg) {
		if (cfg_) {
			const typename atomic_t::write_lock_type lk(cfg_->lock_, atomic_t::lock_traits::infinite_timeout());
			vertex_=cfg_->add_vertex_nolk(p.details);
			if (p.parent) {
				cfg_->add_edge_nolk(p.parent->vertex(), vertex_, '?');
			} else {
				cfg_->add_edge_to_root_nolk(vertex_, '?');
			}
		}
	}
	add_cfg_details(add_cfg_details const &)=delete;

	void update_edge(node_property_t::value_type const e_details) noexcept(false) {
		if (cfg_) {
			const typename atomic_t::write_lock_type lk(cfg_->lock_, atomic_t::lock_traits::infinite_timeout());
			cfg_->update_edge_nolk(vertex_, e_details);
		}
	}

	vertex_t const &vertex() const noexcept(true) {
		return vertex_;
	}

private:
	control_flow_graph * const cfg_;
	vertex_t vertex_;
};

} } }
