/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class Colln, unsigned long I> inline
typename front_batch<Colln, I>::value_ret_type
front_batch<Colln, I>::pop_front_nochk_nolk() noexcept(true) {
// TODO Dunno why the partial specialisation doesn't seem to work...
	if (max_size>1) {
		value_ret_type ret;
		typename value_ret_type::size_type i=0;
		do {
			ret[i]=std::move(this->colln().front());
			assert(!this->colln().empty());
			this->have_work.remove();
			this->colln().pop_front();
		} while (++i<ret.size() && !this->colln().empty());
		std::fill_n(ret.begin()+i, ret.size()-i, typename value_ret_type::value_type());
		return ret;
	} else {
		return base_t::pop_front_nochk_nolk();
	}
}

template<class Colln> inline
typename front_batch<Colln, 1UL>::value_ret_type
front_batch<Colln, 1UL>::pop_front_nochk_nolk() noexcept(true) {
	return base_t::pop_front_nochk_nolk();
}

/* TODO Implement these properly.
template<class Colln, unsigned long I> inline void
back_batch<Colln, I>::push_back(const value_type &data_item) noexcept(false) {
	if (!this->empty()) {
		const typename container_type::write_lock_type lk(this->push_lock(), container_type::lock_traits::infinite_timeout());
		value_type &agg_data_item=this->back_nolk();
		if (agg_data_item->size()<max_size) {
			agg_data_item->reserve(max_size);
			agg_data_item->insert(agg_data_item->end(), data_item->begin(), data_item->end());
		} else {
			container_type::push_back_nolk(data_item);
		}
	} else {
		container_type::push_back_nolk(data_item);
	}
}

template<class Colln, unsigned long I> inline void
back_batch<Colln, I>::push(const value_type &data_item) noexcept(false) {
	if (!this->empty()) {
		const typename container_type::write_lock_type lk(this->push_lock(), container_type::lock_traits::infinite_timeout());
		value_type &agg_data_item=this->back_nolk();
		if (agg_data_item->size()<max_size) {
			agg_data_item->reserve(max_size);
			agg_data_item->insert(agg_data_item->end(), data_item->begin(), data_item->end());
		} else {
			container_type::push_nolk(data_item);
		}
	} else {
		container_type::push_nolk(data_item);
	}
}
*/
template<class Colln> inline void
back_batch<Colln, 1UL>::push_back(const value_type &data_item) noexcept(true) {
	base_t::push_back(data_item);
}

template<class Colln> inline void
back_batch<Colln, 1UL>::push_back(value_type &&data_item) noexcept(true) {
	base_t::push_back(data_item);
}

template<class Colln> inline void
back_batch<Colln, 1UL>::push(const value_type &data_item) noexcept(true) {
	base_t::push(data_item);
}

template<class Colln> inline void
back_batch<Colln, 1UL>::push(value_type &&data_item) noexcept(true) {
	base_t::push(data_item);
}

} }
