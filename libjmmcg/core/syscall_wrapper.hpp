#ifndef LIBJMMCG_CORE_SYSCALL_WRAPPER_HPP
#define LIBJMMCG_CORE_SYSCALL_WRAPPER_HPP

/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"

#include <boost/lexical_cast.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/throw_exception.hpp>
#include <fmt/format.h>

#include <csignal>
#include <stdexcept>
#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A wrapper for glibc or syscalls, etc. This function captures the return-code and if it is the specified failure-code, then according to the traits may capture any error information and throw it as an exception.
namespace syscall {

using thread_traits= ppd::api_threading_traits<ppd::platform_api, ppd::heavyweight_threading>;

namespace private_ {

template<class ReturnType>
struct failure_code;

template<>
struct failure_code<ssize_t> {
	static inline constexpr const ssize_t value= 0;
};
template<>
struct failure_code<int> {
	static inline constexpr const int value= -1;
};
template<>
struct failure_code<void const*> {
	static inline constexpr void const* const value= nullptr;
};
template<>
struct failure_code<void*> {
	static inline constexpr void* const value= nullptr;
};
template<>
struct failure_code<char const*> {
	static inline constexpr char const* const value= nullptr;
};
template<>
struct failure_code<char*> {
	static inline constexpr char* const value= nullptr;
};
template<>
struct failure_code<sighandler_t> {
	static inline const sighandler_t value= reinterpret_cast<sighandler_t>(-1);
};

template<>
struct failure_code<void> {
};

template<class ReturnType>
struct failure_detection;

template<>
struct failure_detection<ssize_t> {
	static constexpr bool result(ssize_t ret_code, ssize_t fv) noexcept(true) {
		return ret_code < fv;
	}
};
template<>
struct failure_detection<int> {
	static constexpr bool result(int ret_code, int fv) noexcept(true) {
		return ret_code <= fv;
	}
};
template<>
struct failure_detection<void const*> {
	static constexpr bool result(void const* ret_code, void const* fv) noexcept(true) {
		return ret_code == fv;
	}
};
template<>
struct failure_detection<void*> {
	static constexpr bool result(void* ret_code, void* fv) noexcept(true) {
		return ret_code == fv;
	}
};
template<>
struct failure_detection<char const*> {
	static constexpr bool result(char const* ret_code, char const* fv) noexcept(true) {
		return ret_code == fv;
	}
};
template<>
struct failure_detection<char*> {
	static constexpr bool result(char* ret_code, char* fv) noexcept(true) {
		return ret_code == fv;
	}
};
template<>
struct failure_detection<sighandler_t> {
	static constexpr bool result(sighandler_t ret_code, sighandler_t fv) noexcept(true) {
		return ret_code == fv;
	}
};

template<>
struct failure_detection<void> {
	static constexpr void result() noexcept(true) {
	}
};

template<std::size_t N, class CaseStatements>
struct unroller {
	using type= typename std::tuple_element<N, CaseStatements>::type;

	template<class ReportError>
	static auto result(int v, ReportError&& report_error) noexcept(false) {
		if(v == type::label) {
			return type::statement();
		} else {
			return unroller<N - 1, CaseStatements>::result(v, std::forward<ReportError>(report_error));
		}
	}
};
template<class CaseStatements>
struct unroller<0, CaseStatements> {
	using type= typename std::tuple_element<0, CaseStatements>::type;

	template<class ReportError>
	static auto result(int v, ReportError&& report_error) noexcept(false /*noexcept(type::statement()) && noexcept(report_error())*/) {
		if(v == type::label) {
			return type::statement();
		} else {
			report_error();
			return decltype(type::statement())();
		}
	}
};

}

template<
	int RetCode,
	auto Stmnt>
struct a_case_statement {
	enum : int {
		label= RetCode
	};
	static inline constexpr auto statement{Stmnt};
};

template<class... CaseLabels>
struct case_statements_t {
	using case_labels= std::tuple<CaseLabels...>;
};

template<
	class ReturnType,
	template<class>
	class FailureCode,
	template<class>
	class FailureDetection>
struct failure_traits {
	using fn_ret_code= ReturnType;
	using failure_code= FailureCode<fn_ret_code>;
	using failure_detection= FailureDetection<fn_ret_code>;
};

/// The base of any trait in this file. This trait calls handle_error() if it detects a failure, otherwise returns the result of the wrapped function.
template<
	class FailureTraits,
	class... Args>
struct traits_base {
	using failure_traits= FailureTraits;
	using fn_ret_code= typename failure_traits::fn_ret_code;
	using failure_detection= typename failure_traits::failure_detection;
	using failure_code= typename failure_traits::failure_code;

	/// This operation is run to either return the result of the wrapped function or execute handle_error(), which should throw an exception. (Otherwise what is the point of this wrapper?)
	template<class HandleError>
	static auto report(fn_ret_code ret, HandleError&& handle_error) noexcept(false);

	/// How the error should be reported to the client, in this case a crt_exception with suitable details is thrown.
	NEVER_INLINE static void report_error(int err, ::boost::source_location const& src_loc, std::string const& err_msg, fn_ret_code (*fn)(Args...), Args... args) noexcept(false);
};

/// This is the default trait.
template<
	class FailureTraits,
	class... Args>
struct simple_report_traits : public traits_base<FailureTraits, Args...> {
	using base_t= traits_base<FailureTraits, Args...>;
	using typename base_t::failure_code;
	using typename base_t::failure_detection;
	using typename base_t::fn_ret_code;

	/// The way an error is identified and operated upon.
	static auto select_error(int err, ::boost::source_location const& src_loc, std::string const& err_msg, fn_ret_code (*fn)(Args...), Args... args);
};

/// If different operations need to be performed on different return-codes, then this trait may be used.
template<
	class CaseStatements,	///< A collection of exit-code & operation pairs.
	class FailureTraits,
	class... Args>
struct switch_traits : public traits_base<FailureTraits, Args...> {
	using base_t= traits_base<FailureTraits, Args...>;
	using typename base_t::failure_code;
	using typename base_t::failure_detection;
	using typename base_t::fn_ret_code;

	/**
		Create this type like this example:

			using all_case_statements=syscall::case_statements_t<
				syscall::a_case_statement<1, [](){return 1;}>,
				syscall::a_case_statement<2, [](){return 2;}>
			>;

		\see case_statements_t, a_case_statement
	*/
	using case_statements= typename CaseStatements::case_labels;

	static constexpr auto select_error(int err, ::boost::source_location const& src_loc, std::string const& err_msg, fn_ret_code (*fn)(Args...), Args... args);
};

template<
	class CaseStatements>
struct switch_wrapper_t {
	template<
		class FailureTraits,
		class... Args>
	using type= switch_traits<CaseStatements, FailureTraits, Args...>;
};

/// The function that wraps calling the C-style function.
template<
	template<class, class...>
	class Traits,
	template<class, template<class> class, template<class> class>
	class FailureTraits,
	template<class>
	class FailureCode,
	template<class>
	class FailureDetection,
	class FnReturnType,	 ///< The return-type deduced from the passed in C-style function.
	class... FnArgs,	 ///< The types of the arguments deduced from the passed in C-style function.
	class... PassedInArgs	///< In C++ one may commonly use slightly different parameter-types, e.g. an enum for a set of integer-values or a bitmask, so we need to accept these, as implicit conversions are not permitted in template-matching for function overloads. Minor conversions are permitted, using static_cast<...>(...).
	>
	requires(sizeof...(FnArgs) == sizeof...(PassedInArgs))
FnReturnType
process(::boost::source_location const& src_loc, std::string const& err_msg, FnReturnType (*fn)(FnArgs...), PassedInArgs... args) noexcept(false);
}

}}

/// Use this nasty macro to wrap calling the C-style function for which the return code is basically a "single" failure-code and a range of successful ones.
/**
	\todo This define could be removed in C++20 with the use of std::source_location, does not work with g++ v9.2.
*/
#define JMMCG_SYSCALL_WRAPPER(ErrMsg, Fn, ...)                        \
	jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::process<                   \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::simple_report_traits,   \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::failure_traits,         \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::private_::failure_code, \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::private_::failure_detection>(::boost::source_location(__FILE__, __LINE__, LIBJMMCG_ENQUOTE(Fn) "(" LIBJMMCG_ENQUOTE(__VA_ARGS__) ")"), ErrMsg, Fn, __VA_ARGS__)
#define JMMCG_SYSCALL_WRAPPER_NO_PARAMS(ErrMsg, Fn)                   \
	jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::process<                   \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::simple_report_traits,   \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::failure_traits,         \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::private_::failure_code, \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::private_::failure_detection>(::boost::source_location(__FILE__, __LINE__, LIBJMMCG_ENQUOTE(Fn) "()"), ErrMsg, Fn)

#define JMMCG_SYSCALL_WRAPPER_FC(FailureCode, ErrMsg, Fn, ...)      \
	jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::process<                 \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::simple_report_traits, \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::failure_traits,       \
		FailureCode,                                                  \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::private_::failure_detection>(::boost::source_location(__FILE__, __LINE__, LIBJMMCG_ENQUOTE(Fn) "(" LIBJMMCG_ENQUOTE(__VA_ARGS__) ")"), ErrMsg, Fn, __VA_ARGS__)

/// Use this nasty macro to wrap calling the C-style function for which there may be many failure codes that need different handling for each.
/**
	\todo This define could be removed in C++20 with the use of std::source_location, does not work with g++ v9.2.
*/
#define JMMCG_SYSCALL_WRAPPER_SWITCH(CaseLabels, ErrMsg, Fn, ...)     \
	jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::process<                   \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::switch_wrapper_t<       \
			CaseLabels>::type,                                           \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::failure_traits,         \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::private_::failure_code, \
		jmmcg::LIBJMMCG_VER_NAMESPACE::syscall::private_::failure_detection>(::boost::source_location(__FILE__, __LINE__, LIBJMMCG_ENQUOTE(Fn) "(" LIBJMMCG_ENQUOTE(__VA_ARGS__) ")"), ErrMsg, Fn, __VA_ARGS__)

#include "syscall_wrapper_impl.hpp"

#endif
