/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

// Implementation details..... Don't look below here!!! ;)

//	"api_threading_traits" implementation...

template<generic_traits::api_type::element_type API_, typename Mdl_>
inline bool __fastcall api_threading_traits<API_, Mdl_>::is_running(api_params_type& parms) noexcept(true) {
	if(parms.id && parms.id != get_current_thread_id()) {
		const typename api_params_type::suspend_count prev_count= suspend(parms);
		resume(parms);
		return !prev_count;
	} else {
		return static_cast<bool>(parms.id);
	}
}

}}}
