#ifndef LIBJMMCG_CORE_ANON_SPIN_EVENT_HPP
#define LIBJMMCG_CORE_ANON_SPIN_EVENT_HPP

/******************************************************************************
** Copyright © 2014 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <atomic>
#include <thread>

// TODO: see comment in "anon_spin_event::set()": #include <emmintrin.h>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace lock {

/// A very simple, atomic, lock-free mutex that is implemented with a busy-wait.
/**
	Compare to the Win32 Critical Section object or futex. This is not ideal for heavily contended locks.
	Moreover some platforms do not provide kernel objects such as mutexes, so one needs this to "bootsrap" protecting shared data structures in a nicer manner,

	c.f. <a href=" https://github.com/facebook/folly/blob/master/folly/synchronization/MicroSpinLock.h">folly::MicroSpinLock</a>. Also read the important notes in "man pthread_spin_init".
	\see
*/
template<class LkT>
class anon_spin_event final : public lock::lockable_settable<LkT> {
public:
	using base_t= lock::lockable_settable<LkT>;
	using lock_traits= typename base_t::lock_traits;
	using atomic_state_type= typename lock_traits::atomic_state_type;
	using lock_result_type= atomic_state_type;
	using timeout_type= typename lock_traits::timeout_type;
	using count_type= int;
	using lock_type= lock::template in_process<anon_spin_event>;
	using read_lock_type= lock_type;
	using write_lock_type= lock_type;
	using atomic_t= std::atomic_flag;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const generic_traits::memory_access_modes::element_type memory_access_mode= generic_traits::memory_access_modes::crew_memory_access;

	/**
		Initialise as "atom_unset".
	*/
	REALLY_FORCE_INLINE __fastcall anon_spin_event() noexcept(true);
	REALLY_FORCE_INLINE explicit __fastcall anon_spin_event(const atomic_state_type state) noexcept(true);
	anon_spin_event(anon_spin_event const&)= delete;
	anon_spin_event& operator=(anon_spin_event const&)= delete;

	REALLY_FORCE_INLINE atomic_state_type __fastcall set() noexcept(true) override;
	REALLY_FORCE_INLINE lock_result_type __fastcall unlock() noexcept(true) override;
	REALLY_FORCE_INLINE atomic_state_type __fastcall reset() noexcept(true) override;
	REALLY_FORCE_INLINE lock_result_type __fastcall lock() noexcept(true) override;
	REALLY_FORCE_INLINE lock_result_type __fastcall lock(const timeout_type) noexcept(true) override;

private:
	atomic_t state{};
};

}}}}

#include "anon_spin_event_impl.hpp"

#endif
