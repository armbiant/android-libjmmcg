#ifndef LIBJMMCG_CORE_FACTORY_HPP
#define LIBJMMCG_CORE_FACTORY_HPP

/******************************************************************************
** Copyright © 2005 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"
#include "multimap.hpp"
#include "shared_ptr.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace factory {

	/// The error information if a requested key-type is not registered with the factory.
	template<
		typename ID_,	///< The type of the key.
		class Ret_,	///< The type that the key should have returned.
		class Excpt_	///< The base exception class from which this should derive to place it in the appropriate position in the exception hierarchy.
	>
	struct not_found {
		typedef Excpt_ exception_type;

		/// This function throws the above exception for the specified ID_ type.
		static Ret_ __fastcall execute(const ID_ &);
	};

	/// The base factory-type.
	/**
		Note that this class is threading-agnostic.
	*/
	template<
		typename ID_,	///< The key-type of the factory.
		class Obj_,	///< The type of item to be returned by the key-type.
		class Except_,	///< The exception-type to root the exception hierarchy onto.
		class Ret_,	///<	Exactly how the item returned by the key-type should be returned, e.g. via transfer of ownership.
		class NotFound_,	///< The type of exception thrown if the specified key-type is not registered with the factory.
		typename CreatFn_,	///< Some kind of function or functor that is used to create the item to be returned from a specific key-type.
		class Cont_	///< The type of associative container that should be used by the factory for identifying the method by which the specified instance of key-type should return the value-type.
	>
	class base {
	public:
		typedef Cont_ container_type;
		typedef typename container_type::key_type id_type;
		typedef typename container_type::value_type value_type;
		typedef typename container_type::mapped_type createable_type;
		typedef typename container_type::mapped_type mapped_type;
		typedef typename container_type::size_type size_type;
		typedef Ret_ created_type;

		base(base const &)=delete;
		virtual __stdcall ~base();

		/// Register a specific key & manufacturing method with the factory.
		bool __fastcall insert(const value_type &);
		/// Register a specific key & manufacturing method with the factory.
		bool __fastcall insert(const id_type &,const createable_type &);
		/// Unregister a specific key from the factory.
		const typename container_type::size_type __fastcall erase(const id_type &id) {
			return createables_.erase(id);
		}
		/// Return true if that key has been registered with the factory.
		bool __fastcall find(const id_type &) const;
		/// Return true if any key has been registered with the factory.
		bool __fastcall empty(void) const noexcept(true);
		/// Return the total number of unique keys registered with the factory.
		const size_type __fastcall size(void) const noexcept(true);
		/// Reserve capacity for a specified number of keys with the factory.
		void __fastcall reserve(const size_type);

		/// Access the internal associative collection used to store the keys and methods for creating the return items.
		const container_type & __fastcall createables(void) const noexcept(true);
		/// Access the internal associative collection used to store the keys and methods for creating the return items.
		container_type & __fastcall createables(void) noexcept(true);

	protected:
		container_type createables_;

		[[nodiscard]] __stdcall base(void);
	};

	/// A factory that manufactures a new, unique instance of the specified return-type for a specified key that has been registered with the factory.
	/**
		Note that this class is not thread-safe by default, at a minimum the container would need to be thread-safe.
	*/
	template<
		typename ID_,	///< The key-type of the factory.
		class Obj_,	///< The type of item to be returned by the key-type.
		class Except_,	///< The exception-type to root the exception hierarchy onto.
		class Ret_=std::unique_ptr<Obj_>,	///<	Exactly how the item returned by the key-type should be returned, e.g. via transfer of ownership.
		class NotFound_=not_found<ID_, Ret_, Except_>,	///< The type of exception thrown if the specified key-type is not registered with the factory.
		typename CreatFn_=Ret_ (__fastcall *)(void),	///< The type of method by which the factory may manufacture a new, unique instance of the specified item to be returned.
		class Cont_=rapid_insert_lookup::multimap<ID_, CreatFn_> 	///< The type of associative container that should be used by the factory for identifying the method by which the specified instance of key-type should return the value-type.
	>
	class creator final : public base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_> {
	public:
		typedef base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_> base_t;
		typedef typename base_t::id_type id_type;
		typedef typename base_t::created_type created_type;

		[[nodiscard]] constexpr __stdcall creator(void);
		virtual __stdcall ~creator(void);

		/// Returns a new, unique new, unique instance of the specified return-type for a specified key that has been registered with the factory.
		virtual created_type __fastcall make(const id_type &id) const;
	};

	/// A factory that manufactures a clone of an instance of the specified return-type for a specified key that has been registered with the factory.
	/**
		Note that this class is not thread-safe by default, at a minimum the container and shared pointer-type would need to be thread-safe.
	*/
	template<
		typename ID_,	///< The key-type of the factory.
		class Obj_,	///< The type of item to be returned by the key-type.
		class Except_,	///< The exception-type to root the exception hierarchy onto.
		class Ret_=std::unique_ptr<Obj_>,	///<	Exactly how the item returned by the key-type should be returned, e.g. via transfer of ownership.
		class NotFound_=not_found<ID_, Ret_, Except_>,	///< The type of exception thrown if the specified key-type is not registered with the factory.
		typename CreatFn_=Ret_ (__fastcall *)(const Obj_ &),	///< Return a copy of the instance of the specified item to be returned.
		class Cont_=rapid_insert_lookup::multimap<ID_, std::pair<shared_ptr<Obj_, ppd::api_lock_traits<ppd::platform_api, ppd::sequential_mode>>, CreatFn_>	///< The type of associative container that should be used by the factory for identifying the method by which the specified instance of key-type should return the value-type.
		>
	>
	class clone final : public base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_> {
	public:
		typedef base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_> base_t;
		typedef typename base_t::id_type id_type;
		typedef typename base_t::createable_type createable_type;
		typedef typename base_t::created_type created_type;
		typedef typename Cont_::mapped_type::first_type::value_type::lock_traits lock_traits;

		[[nodiscard]] constexpr __stdcall clone();
		virtual __stdcall ~clone();

		/// Returns a clone of an instance of the specified return-type for a specified key that has been registered with the factory.
		virtual created_type __fastcall make(const id_type &) const;
	};

} } }

#include "factory_impl.hpp"

#endif
