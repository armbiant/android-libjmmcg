#ifndef LIBJMMCG_CORE_AVE_DEVIATION_METER_HPP
#define LIBJMMCG_CORE_AVE_DEVIATION_METER_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "lock_mem.hpp"
#include "thread_api_traits.hpp"
#include "ttypes.hpp"

#include <cstdlib>
#include <iomanip>
#include <limits>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	/// A class used to compute the arithmetic mean and mean-average deviation [1] of a series of events.
	/**
		Notes:
		-# If this class is used in a multi-threaded manner, it is not thread-safe, so these invariants are not guaranteed.
		-# The process is locked in memory to reduce errors in obtaining the measurements.

		[1] <a href="http://mathbits.com/MathBits/TISection/Statistics1/MAD.html"/>

		\see invariant()
	*/
	template<typename MeteredObjType>
	class ave_deviation_meter {
	public:
		/**
			This is an overly-complicated way of saying "although this might look atomic - I want a sequential version" so it is just the MeteredObjType. std::atomic with program-consistency is not used, because it introduces a lock prefix, which would slow the program being measured. Speed has been traded off for reliable numbers...
		*/
		using atomic_counter_type=ppd::atomic_ctr<MeteredObjType, ppd::api_lock_traits<ppd::platform_api, ppd::sequential_mode>>;
		/// The type of the object upon which the events will be recorded.
		using value_type=typename atomic_counter_type::value_type;

		[[nodiscard]] constexpr ave_deviation_meter() noexcept(true);
		[[nodiscard]] explicit constexpr ave_deviation_meter(const value_type val) noexcept(true);
		[[nodiscard]] constexpr ave_deviation_meter(value_type const val, double const dev) noexcept(true);
		[[nodiscard]] ave_deviation_meter(ave_deviation_meter const &rm) noexcept(true);
		~ave_deviation_meter() noexcept(true);

		ave_deviation_meter & __fastcall operator=(ave_deviation_meter const &rm) noexcept(true);

		ave_deviation_meter & __fastcall operator+=(ave_deviation_meter const &rm) noexcept(true);

		/// Record a new event that occurred.
		/**
			This will update the minimum, maximum, running arithmetic mean and average-deviation measurements.
		*/
		ave_deviation_meter & __fastcall update(value_type const new_val) noexcept(true);

		/// Return the minimum of the samples taken.
		constexpr value_type __fastcall min() const noexcept(true);
		/// Return the arithmetic mean of the samples taken.
		constexpr value_type __fastcall arithmetic_mean() const noexcept(true);
		/// Return the maximum of the samples taken.
		constexpr value_type __fastcall max() const noexcept(true);
		/// Return the total of all of the samples taken.
		constexpr value_type __fastcall total_samples() const noexcept(true);

		/// Return the average deviation of the samples taken.
		/**
			Note that when using this value you should round to one significant figure.
			\return The average deviation.
		*/
		constexpr double __fastcall deviation() const noexcept(true);
		/// Return the average deviation, as a percent of the mean, of the samples taken.
		/**
			Note that when using this value you should round to one significant figure.
			\return The average deviation as a percentage.
		*/
		constexpr double __fastcall deviation_percentage() const noexcept(true);
		/// Return the average deviation as a percentage of the arithmetic mean.
		constexpr unsigned short percent() const noexcept(true);

		tstring __fastcall to_string() const noexcept(false);

		tstring __fastcall to_csv() const noexcept(false);

	private:
		unsigned long num_samples_;
		value_type minimum_, maximum_, total_;
		double arithmetic_mean_;
		double ave_deviation;

		void invariant() const noexcept(true);
	};

	template<typename MeteredObjType>
	inline tostream & __fastcall
	operator<<(tostream &os, ave_deviation_meter<MeteredObjType> const &p) noexcept(false);

	/// An algorithm to estimate the average deviation returned after a functor is computed N times.
	/**
		\param computations	The number of times the functor fn should be computed.
		\param fn	The functor to be computed. It should take no arguments and return ave_deviation_meter::value_type.
		\return	The computed ave_deviation_meter.
	*/
	template<class MeteredObjType, class Fn>
	[[nodiscard]] inline ave_deviation_meter<MeteredObjType>
	estimate_average_deviation(typename ave_deviation_meter<MeteredObjType>::value_type const computations, Fn fn);

	/// An algorithm to compute the ave_deviation_meter of a functor when a specific average deviation is requested.
	/**
		\param target_deviation	The maximum, target average deviation, as a percentage number, that should be achieved.
		\param max_computations	The maximum number of times the functor fn should be computed, to ensure termination.
		\param fn	The functor to be computed. It should take no arguments and return ave_deviation_meter::value_type.
		\return	The computed ave_deviation_meter and a boolean that if set indicates that the target_deviation could not be achieved within max_computations attempts.
	*/
	template<class MeteredObjType, class Fn>
	[[nodiscard]] inline std::pair<ave_deviation_meter<MeteredObjType>, bool>
	compute_average_deviation(double const target_deviation, typename ave_deviation_meter<MeteredObjType>::value_type const max_computations, Fn fn);

} }

#include "ave_deviation_meter_impl.hpp"

#endif
