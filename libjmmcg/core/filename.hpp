#ifndef LIBJMMCG_CORE_FILENAME_HPP
#define LIBJMMCG_CORE_FILENAME_HPP
/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	/// Return a string suitable for using to name a file.
	/**
		\param	root	The string that shall be decorated to generate the resultant file name.
		\param	mask	The mask used to form the random string, where '%' is a hexadecimal character.
		\return The resultant filename. By default, this shall be of the form: "root-%%%%%.PID".
	*/
	[[nodiscard]] std::string make_filename(std::string &&root, char const * const mask="-%%%%%") noexcept(false);
	[[nodiscard]] std::string make_filename(char const * const root, char const * const mask="-%%%%%") noexcept(false);

} }

#endif
