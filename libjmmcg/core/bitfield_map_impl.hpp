/******************************************************************************
** Copyright © 2014 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace private_ {

	using underlying_type=uint64_t;

	template<underlying_type bit_pos, class Iter, class End, bool AtEnd=std::is_same<Iter, End>::value>
	struct index {
		/// Compute the number of bytes offset that the bit_pos represents given a bitmask mask that maps to the range of types contained in the range [Iter, End).
		/**
			\param mask	The bitmaps that maps to the range of enabled types contained in the range [Iter, End).
			\return		The number of bytes offest.
		*/
		template<class BMsk>
		static constexpr std::size_t
		result(BMsk mask) noexcept(true) {
			using selected_obj_type=typename boost::mpl::deref<Iter>::type::second;

			if (mask!=BMsk()) {
				const std::size_t current_size=((mask & 0x1u) ? static_cast<std::size_t>(give_void_a_size_of<selected_obj_type>::value) : 0u);
				return current_size+index<bit_pos-1u, typename boost::mpl::next<Iter>::type, End>::result(mask>>1u);
			} else {
				return std::size_t();
			}
		}
	};
	/**
		Nothing left from the collection of bit fields types, so terminate. In principle we should never get here, the bit position should terminate beforehand or at the same time.
	*/
	template<underlying_type bit_pos, class Iter, class End>
	struct index<bit_pos, Iter, End, true> {
		template<class BMsk>
		static constexpr std::size_t
		result(BMsk) noexcept(true) {
			return std::size_t();
		}
	};
	/**
		Remember that bit positions are 1-based in C++, but collections are zero-indexed, so ignore the first type entry in the collection if the user requests the first bit position.
	*/
	template<class Iter, class End, bool AtEnd>
	struct index<1u, Iter, End, AtEnd> {
		template<class BMsk>
		static constexpr std::size_t
		result(BMsk) noexcept(true) {
			return std::size_t();
		}
	};
	/**
		Remember that bit positions are 1-based in C++, but collections are zero-indexed, so ignore the first type entry in the collection if the user requests the first bit position.
	*/
	template<class Iter, class End>
	struct index<1u, Iter, End, true> {
		template<class BMsk>
		static constexpr std::size_t
		result(BMsk) noexcept(true) {
			return std::size_t();
		}
	};
	/**
		Nothing left from the selected bit positions, so terminate.
	*/
	template<class Iter, class End>
	struct index<0u, Iter, End, false> {
		template<class BMsk>
		static constexpr std::size_t
		result(BMsk) noexcept(true) {
			return std::size_t();
		}
	};

	template<class Iter, class End, bool AtEnd=std::is_same<Iter, End>::value>
	struct current_size {
		/// Compute the total number of bytes that the bitmask mask that maps to the range of types contained in the range [Iter, End).
		/**
			\param mask	The bitmaps that maps to the range of types contained in the range [Iter, End).
			\return		The total number of bytes of enabled types.
		*/
		static constexpr std::size_t
		result(underlying_type mask) noexcept(true) {
			using selected_obj_type=typename boost::mpl::deref<Iter>::type::second;

			if (mask!=underlying_type()) {
				const std::size_t curr_size=((mask & 0x1u) ? static_cast<std::size_t>(give_void_a_size_of<selected_obj_type>::value) : 0u);
				return curr_size+current_size<typename boost::mpl::next<Iter>::type, End>::result(mask>>1u);
			} else {
				return std::size_t();
			}
		}
	};
	/**
		Nothing left from the collection of bit fields types, so terminate. In principle we should never get here, the bit position should terminate beforehand or at the same time.
	*/
	template<class Iter, class End>
	struct current_size<Iter, End, true> {
		static constexpr std::size_t
		result(underlying_type) noexcept(true) {
			return std::size_t();
		}
	};

	template<
		class Cls,
		typename Cls::bitfields_tags_type SelectedField,
		class AsType,
		class Ret
	>
	inline Ret const &
	at(typename Cls::key_type const &bfs, typename Cls::raw_mapped_data_t const &raw_mapped_data) noexcept(false) {
		BOOST_MPL_ASSERT_NOT((std::is_same<typename boost::mpl::at<typename Cls::mapped_types, AsType>::type, boost::mpl::void_>));

		if (bfs & static_cast<typename Cls::key_type>(SelectedField)) {
			using indexer=index<
				mpl::bit_position<static_cast<typename Cls::underlying_key_type>(SelectedField)>::value,
				typename boost::mpl::begin<typename Cls::mapped_types>::type,
				typename boost::mpl::end<typename Cls::mapped_types>::type
			>;
			const std::size_t offset_in_range_types=indexer::result(bfs);
			assert(offset_in_range_types<Cls::range_mapped_types_size);
			const typename Cls::raw_mapped_data_t::value_type &data=raw_mapped_data[offset_in_range_types];
			return *reinterpret_cast<Ret const *>(&data);
		} else {
			std::ostringstream os;
			os<<"Selected field: 0x"<<std::hex<<static_cast<typename Cls::underlying_key_type>(SelectedField)<<", not found in mask=0x"<<std::hex<<bfs;
			BOOST_THROW_EXCEPTION(std::range_error(os.str()));
		}
	}

	template<
		class Cls,
		typename Cls::bitfields_tags_type SelectedField,
		class AsType,
		class Ret
	>
	inline Ret &
	at(typename Cls::key_type const &bfs, typename Cls::raw_mapped_data_t &raw_mapped_data) noexcept(false) {
		BOOST_MPL_ASSERT_NOT((std::is_same<typename boost::mpl::at<typename Cls::mapped_types, AsType>::type, boost::mpl::void_>));

		if (bfs & static_cast<typename Cls::underlying_key_type>(SelectedField)) {
			using indexer=index<
				mpl::bit_position<static_cast<typename Cls::underlying_key_type>(SelectedField)>::value,
				typename boost::mpl::begin<typename Cls::mapped_types>::type,
				typename boost::mpl::end<typename Cls::mapped_types>::type
			>;
			const std::size_t offset_in_range_types=indexer::result(bfs);
			assert(offset_in_range_types<Cls::range_mapped_types_size);
			typename Cls::raw_mapped_data_t::value_type &data=raw_mapped_data[offset_in_range_types];
			return *reinterpret_cast<Ret *>(&data);
		} else {
			std::ostringstream os;
			os<<"Selected field: 0x"<<std::hex<<static_cast<typename Cls::underlying_key_type>(SelectedField)<<", not found in mask=0x"<<std::hex<<bfs;
			BOOST_THROW_EXCEPTION(std::range_error(os.str()));
		}
	}

	template<
		class Cls,
		typename Cls::bitfields_tags_type SelectedField,
		class AsType,
		class Ret
	>
	inline constexpr void
	erase(typename Cls::key_type &bfs, typename Cls::raw_mapped_data_t &raw_mapped_data) noexcept(true) {
		BOOST_MPL_ASSERT_NOT((std::is_same<typename boost::mpl::at<typename Cls::mapped_types, AsType>::type, boost::mpl::void_>));
		if (bfs & SelectedField) {
			if (!Cls::all_pod) {
				using indexer=index<
					mpl::bit_position<static_cast<typename Cls::underlying_key_type>(SelectedField)>::value,
					typename boost::mpl::begin<typename Cls::mapped_types>::type,
					typename boost::mpl::end<typename Cls::mapped_types>::type
				>;
				const std::size_t offset_in_range_types=indexer::result(bfs);
				assert(offset_in_range_types<Cls::range_mapped_types_size);
				Ret *data=reinterpret_cast<Ret *>(std::next(raw_mapped_data.begin(), offset_in_range_types));
				data->~Ret();
			}
			bfs&=(~SelectedField);
		}
	}

	template<
		class Cls,
		typename Cls::bitfields_tags_type SelectedField,
		class AsType,
		class Arg
	>
	inline void
	insert(typename Cls::key_type &bfs, typename Cls::raw_mapped_data_t &raw_mapped_data, Arg const &arg) noexcept(false) {
		BOOST_MPL_ASSERT_NOT((std::is_same<typename boost::mpl::at<typename Cls::mapped_types, AsType>::type, boost::mpl::void_>));
// TODO		BOOST_MPL_ASSERT_NOT((static_cast<unsigned long long>(mpl::count_setbits<static_cast<typename Cls::underlying_key_type>(SelectedField)>::value)));

		using indexer=index<
			mpl::bit_position<static_cast<typename Cls::underlying_key_type>(SelectedField)>::value,
			typename boost::mpl::begin<typename Cls::mapped_types>::type,
			typename boost::mpl::end<typename Cls::mapped_types>::type
		>;

		// Verify that there are no higher values set, otherwise adding keys that are lower will cause the higher keys to index uninitialised memory.
		assert(((bfs|mpl::lsb_bitmask<static_cast<unsigned long long>(SelectedField)>::value)^mpl::lsb_bitmask<static_cast<unsigned long long>(SelectedField)>::value)==0u);
		const std::size_t offset_in_range_types=indexer::result(bfs);
		assert(offset_in_range_types<Cls::range_mapped_types_size);
		typename Cls::raw_mapped_data_t::value_type &data=raw_mapped_data[offset_in_range_types];
		if (!(bfs & static_cast<typename Cls::underlying_key_type>(SelectedField))) {
			if (!Cls::all_pod) {
				new(&data) Arg(arg);
			} else {
				*reinterpret_cast<Arg *>(&data)=arg;
			}
			bfs|=static_cast<typename Cls::underlying_key_type>(SelectedField);
		}
		assert(bfs);
	}

	template<class ObjToDel>
	struct delete_non_void {
		template<class Cls, typename Cls::key_type SelectedField>
		static constexpr void
		result(typename Cls::key_type &mask, typename Cls::raw_mapped_data_t &raw_mapped_data) noexcept(true) {
			mask&=(~SelectedField);
			const std::size_t offset_in_range_types=index<
				mpl::bit_position<static_cast<typename Cls::underlying_key_type>(SelectedField)>::value,
				typename boost::mpl::begin<typename Cls::mapped_types>::type,
				typename boost::mpl::end<typename Cls::mapped_types>::type
			>::result(mask);
			assert(offset_in_range_types<Cls::range_mapped_types_size);
			ObjToDel *data=reinterpret_cast<ObjToDel *>(std::next(raw_mapped_data.begin(), offset_in_range_types));
			data->~ObjToDel();
		}
	};
	template<>
	struct delete_non_void<void> {
		template<class Cls, typename Cls::key_type SelectedField>
		static constexpr void
		result(typename Cls::key_type &, typename Cls::raw_mapped_data_t &) noexcept(true) {
		}
	};

	template<class Cls, typename std::underlying_type<typename Cls::bitfields_tags_type>::type SelectedField>
	struct deletor {
		static constexpr typename Cls::key_type
		result(typename Cls::key_type mask, typename Cls::raw_mapped_data_t &raw_mapped_data) noexcept(true) {
			if (mask & SelectedField) {
				using AsType=typename std::integral_constant<typename Cls::bitfields_tags_type, static_cast<typename Cls::bitfields_tags_type>(SelectedField)>::type;
				using Ret=typename boost::mpl::at<typename Cls::mapped_types, AsType>::type;
				delete_non_void<Ret>::template result<Cls, static_cast<typename Cls::key_type>(SelectedField)>(mask, raw_mapped_data);
			}
			return deletor<Cls, (SelectedField>>1u)>::result(mask, raw_mapped_data);
		}
	};
	template<class Cls>
	struct deletor<Cls, 0u> {
		static constexpr typename Cls::key_type
		result(typename Cls::key_type mask, typename Cls::raw_mapped_data_t const &) noexcept(true) {
			return mask;
		}
	};

	template<class Cls>
	inline constexpr void
	clear(typename Cls::key_type &bfs, typename Cls::raw_mapped_data_t &raw_mapped_data) noexcept(true) {
		using type=typename std::underlying_type<typename Cls::bitfields_tags_type>::type;
		enum : type {
			msb_set=static_cast<type>(~(std::numeric_limits<type>::max()>>1))
		};
		if (!Cls::all_pod) {
			bfs=deletor<Cls, msb_set>::result(bfs, raw_mapped_data);
		} else {
			bfs=typename Cls::key_type();
		}
	}

}

template<class BFSM, std::size_t BFSz>
inline constexpr
bitfield_map<BFSM, BFSz>::bitfield_map() noexcept(true)
: bfs() {
	BOOST_MPL_ASSERT_RELATION(range_mapped_types_size, <, sizeof(bitfield_map));
	raw_mapped_data.fill(typename raw_mapped_data_t::value_type());
}

template<class BFSM, std::size_t BFSz>
inline constexpr typename bitfield_map<BFSM, BFSz>::key_type
bitfield_map<BFSM, BFSz>::convert_to_biggest_integral_type() const noexcept(true) {
 	BOOST_MPL_ASSERT_RELATION(sizeof(key_type), <=, sizeof(private_::underlying_type));
	converter conv{
		key_type()
	};
	conv.bfs=bfs;
	return conv.conv_bfs;
}

template<class BFSM, std::size_t BFSz>
inline constexpr void
bitfield_map<BFSM, BFSz>::swap(bitfield_map &bm) noexcept(true) {
	const raw_key_type_t tmp=bfs;
	bfs=bm.bfs;
	bm.bfs=tmp;
	raw_mapped_data.swap(bm.raw_mapped_data);
}

template<class BFSM, std::size_t BFSz>
inline constexpr
bitfield_map<BFSM, BFSz>::bitfield_map(bitfield_map const &bm) noexcept(true)
: bfs(bm.bfs) {
	raw_mapped_data.fill(typename raw_mapped_data_t::value_type());
	memcpy_opt(bm.raw_mapped_data, raw_mapped_data);
}

template<class BFSM, std::size_t BFSz>
inline constexpr
bitfield_map<BFSM, BFSz>::bitfield_map(bitfield_map &&bm) noexcept(true)
: bfs() {
	raw_mapped_data.fill(typename raw_mapped_data_t::value_type());
	swap(bm);
}

template<class BFSM, std::size_t BFSz>
inline constexpr void
bitfield_map<BFSM, BFSz>::clear() noexcept(true) {
	converter conv{
		key_type()
	};
	conv.bfs=bfs;
	private_::clear<bitfield_map>(conv.conv_bfs, raw_mapped_data);
	bfs=conv.bfs;
}

template<class BFSM, std::size_t BFSz>
inline
bitfield_map<BFSM, BFSz>::~bitfield_map() noexcept(true) {
	this->clear();
}

template<class BFSM, std::size_t BFSz>
inline constexpr bitfield_map<BFSM, BFSz> &
bitfield_map<BFSM, BFSz>::operator=(bitfield_map &&bm) noexcept(true) {
	swap(bm);
	return *this;
}

template<class BFSM, std::size_t BFSz>
inline constexpr bool
bitfield_map<BFSM, BFSz>::empty() const noexcept(true) {
	return !static_cast<bool>(convert_to_biggest_integral_type());
}

template<class BFSM, std::size_t BFSz>
inline constexpr typename bitfield_map<BFSM, BFSz>::size_type
bitfield_map<BFSM, BFSz>::size() const noexcept(true) {
	const std::size_t sz=private_::current_size<
		typename boost::mpl::begin<mapped_types>::type,
		typename boost::mpl::end<mapped_types>::type
	>::result(convert_to_biggest_integral_type());
	assert(sz<=range_mapped_types_size);
	return sz+bitfields_size;
}

template<class BFSM, std::size_t BFSz>
inline constexpr typename bitfield_map<BFSM, BFSz>::size_type
bitfield_map<BFSM, BFSz>::max_size() noexcept(true) {
	return static_cast<size_type>(bitfields_size+range_mapped_types_size);
}

template<class BFSM, std::size_t BFSz>
template<typename bitfield_map<BFSM, BFSz>::bitfields_tags_type SelectedField, class AsType, class Ret>
inline constexpr const Ret &
bitfield_map<BFSM, BFSz>::at() const noexcept(false) {
	return private_::at<bitfield_map, SelectedField, AsType, Ret>(convert_to_biggest_integral_type(), raw_mapped_data);
}

template<class BFSM, std::size_t BFSz>
template<typename bitfield_map<BFSM, BFSz>::bitfields_tags_type SelectedField, class AsType, class Ret>
inline constexpr Ret &
bitfield_map<BFSM, BFSz>::at() noexcept(false) {
	return private_::at<bitfield_map, SelectedField, AsType, Ret>(convert_to_biggest_integral_type(), raw_mapped_data);
}

template<class BFSM, std::size_t BFSz>
template<typename bitfield_map<BFSM, BFSz>::bitfields_tags_type SelectedField, class AsType, class Ret>
inline void
bitfield_map<BFSM, BFSz>::erase() noexcept(true) {
	converter conv{
		key_type()
	};
	conv.bfs=bfs;
	private_::erase<bitfield_map, SelectedField, AsType, Ret>(conv.conv_bfs, raw_mapped_data);
	bfs=conv.bfs;
}

template<class BFSM, std::size_t BFSz>
template<typename bitfield_map<BFSM, BFSz>::bitfields_tags_type SelectedField>
inline bool
bitfield_map<BFSM, BFSz>::find() const noexcept(true) {
	converter conv{
		key_type()
	};
	conv.bfs=bfs;
	return convert_to_biggest_integral_type() & static_cast<underlying_key_type>(SelectedField);
}

template<class BFSM, std::size_t BFSz>
template<typename bitfield_map<BFSM, BFSz>::bitfields_tags_type SelectedField, class AsType, class Arg>
inline void
bitfield_map<BFSM, BFSz>::push_back(Arg const &arg) noexcept(false) {
	converter conv{
		key_type()
	};
	conv.bfs=bfs;
	private_::insert<bitfield_map, SelectedField, AsType, Arg>(conv.conv_bfs, raw_mapped_data, arg);
	bfs=conv.bfs;
}

} }
