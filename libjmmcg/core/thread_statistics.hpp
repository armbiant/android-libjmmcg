#ifndef LIBJMMCG_CORE_THREAD_STATISTICS_HPP
#define LIBJMMCG_CORE_THREAD_STATISTICS_HPP

/******************************************************************************
** Copyright © 2011 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "ave_deviation_meter.hpp"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

	/// Just stub statistics - to support the interface. No actual statistics are collected.
	/**
		To gather basic statistics use basic_statistics, or roll your own!

		\see basic_statistics
	*/
	template<class Sz>
	struct no_statistics {
		typedef Sz work_added_type;
		typedef ave_deviation_meter<work_added_type> ave_stats_type;

		struct process_work_type;
		typedef process_work_type vertical_statistics_type;
		typedef process_work_type hrz_statistics_type;

		static constexpr void added_work() noexcept(true) {}
		constexpr static work_added_type __fastcall total_work_added() noexcept(true) {return work_added_type();}

		constexpr void processed_vertical_work() noexcept(true) {}
		static constexpr void add_vertical_work(ave_stats_type const &) noexcept(true) {}
		static ave_stats_type const &__fastcall total_vertical_work() noexcept(true) {
			static const ave_stats_type tmp;
			return tmp;
		}

		constexpr void processed_hrz_work() noexcept(true) {}
		static constexpr void add_hrz_work(ave_stats_type const &) noexcept(true) {}
		static ave_stats_type const &__fastcall total_hrz_work() noexcept(true) {
			static const ave_stats_type tmp;
			return tmp;
		}
		static constexpr void update_max_queue_len(work_added_type const) noexcept(true) {}
		static constexpr void update_colln_stats(work_added_type const) noexcept(true) {}

		friend constexpr tostream & __fastcall operator<<(tostream &os, no_statistics const &) noexcept(true) {
			return os;
		}
	};

	/// Some basic statistics collected about the operation of the wrapping thread_pool.
	/**
		Note that this class does not use add any locking nor wrap the work_added_type as an atomic object, therefore the updates to it may suffer from races, if the work_added_type is not itself atomic. This is by design: performance vs. accuracy and locking reduces performance, and this class is designed to be fast, not accurate, so the statistics gathered may be under-estimates.

		\todo Add work completed/sec.

		\see no_statistics
		\see pool_aspect::statistics_type
	*/
	template<class Sz>
	class basic_statistics {
	public:
		typedef Sz work_added_type;
		typedef ave_deviation_meter<work_added_type> ave_stats_type;

		/**
			Well, this is a bit strict, it could be an atomic integral-type quite happily.
		*/
		static_assert(std::is_integral<work_added_type>::value, "The input type must be an integral type as defined in 3.9.1 of the Standard.");

		class process_work_type;
		typedef process_work_type vertical_statistics_type;
		typedef process_work_type hrz_statistics_type;

		[[nodiscard]] constexpr basic_statistics() noexcept(true)
		: queue_stats(), vertical_work(), hrz_work(), colln_stats(), work_added() {
		}

		void added_work() noexcept(true) {
			++work_added;
		}
		work_added_type __fastcall total_work_added() const noexcept(true) {
			return work_added;
		}

		void processed_vertical_work() noexcept(true) {
			vertical_work.update(1);
		}
		void add_vertical_work(ave_stats_type const &wk) noexcept(true) {
			vertical_work+=wk;
		}
		ave_stats_type const &__fastcall total_vertical_work() const noexcept(true) {
			return vertical_work;
		}

		void processed_hrz_work() noexcept(true) {
			hrz_work.update(1);
		}
		void add_hrz_work(ave_stats_type const &wk) noexcept(true) {
			hrz_work+=wk;
		}
		ave_stats_type const &__fastcall total_hrz_work() const noexcept(true) {
			return hrz_work;
		}

		void update_max_queue_len(work_added_type const l) noexcept(true) {
			queue_stats.update(l);
		}

		void update_colln_stats(work_added_type const l) noexcept(true) {
			colln_stats.update(l);
		}

		/**
			\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
		*/
		friend tostream & __fastcall operator<<(tostream &os, basic_statistics const &p) noexcept(false) {
			os<<_T("work added=")<<p.work_added
				<<_T(", vertical work: ")<<p.vertical_work
				<<_T(", horizontal work: ")<<p.hrz_work
				<<_T(", pool-queue statistics: ")<<p.queue_stats
				<<_T(", data-parallel collection statistics: ")<<p.colln_stats;
			return os;
		}

	private:
		ave_stats_type queue_stats;
		ave_stats_type vertical_work;
		ave_stats_type hrz_work;
		ave_stats_type colln_stats;
		work_added_type work_added;
	};

	/// A dummy class to not generate a Control Flow Graph (CFG), and hopefully and hopefully all calls will be optimised out, so there should be no cost.
	/**
		\see control_flow_graph
	*/
	template<class OST>
	class no_control_flow_graph {
	public:
		typedef OST os_traits;
		typedef typename os_traits::lock_traits lock_traits;
		typedef typename lock_traits::recursive_critical_section_type atomic_t;
		typedef std::string node_property_t;
		typedef char edge_annotation_t;
		typedef int container_type;
		typedef int vertex_t;
		typedef int edge_t;
		template<class Node>
		class add_cfg_details;
		static inline constexpr const edge_annotation_t hrz_edge_annotation='?';
		static inline constexpr const edge_annotation_t vertical_edge_annotation='?';
		static inline constexpr const edge_annotation_t sequential_edge_annotation='?';

		[[nodiscard]] constexpr no_control_flow_graph() noexcept(true) {}
		no_control_flow_graph(no_control_flow_graph const &)=delete;

		static void write_graphviz(std::ostream &) noexcept(true) {}
	};

	/// A class to generate a Control Flow Graph (CFG), if the support has been coded into the joinable, nonjoinable, etc modifiers.
	/**
		Note that this class is coded for simplicity, not speed, so uses a "big lock" (the atomic_t type) to control multi-threaded access. This has at least these ramifications:

		1. All guarantees regarding number of locks & schedule generated by the library are invalidated (basic_statistics does not invalidate these guarantees).
		2. The number of horizontal operations when generating a CFG may be different from that reported by basic_statistics::total_hrz_work() alone. In this case both results are right, but the number reported by basic_statistics::total_hrz_work() is an accurate measure; whereas the horizontal edges in this graph may have been affected by the internal locking within this class. To verify the CFG, compare the output of basic_statistics with both no_control_flow_graph and control_flow_graph in use, if they are the same you can be assured of accuracy. If different then you need to interpret the CFG output with greater care.

		\see no_control_flow_graph
	*/
	template<class OST>
	class control_flow_graph {
	public:
		typedef OST os_traits;
		typedef typename os_traits::lock_traits lock_traits;
		typedef typename lock_traits::recursive_critical_section_type atomic_t;

		/// Output the graph in DOTTY format.
		/**
			This may be converted to a pleasant PNG-format image using this command:
				"nice dot -Tpng test.dot >test.png"
			Assuming the os is created using an std::ofstream named "test.dot". Note that the PNG file can become large! thread_pool::merge() with 12 cores creates a file that is ~500kB in size and thread_pool::sort() is ~1Mb with 12 cores.
		*/
		void write_graphviz(std::ostream &os) const noexcept(false) {
			const typename atomic_t::read_lock_type lk(lock_, atomic_t::lock_traits::infinite_timeout());
			boost::write_graphviz(os, cfg_, boost::make_label_writer(vertex_names), boost::make_label_writer(edge_names));
		}

		// All the stuff below is used for implementing support for the CFG in the code, both inside the library and if required in the user code...

		typedef std::string node_property_t;
		typedef char edge_annotation_t;
		typedef boost::adjacency_list<
			boost::listS,
			boost::vecS,
			boost::bidirectionalS,
			boost::property<boost::vertex_name_t, node_property_t>,
			boost::property<boost::edge_name_t, node_property_t>
		> container_type;
		typedef typename boost::graph_traits<container_type>::vertex_descriptor vertex_t;
		typedef typename boost::graph_traits<container_type>::edge_descriptor edge_t;
		template<class Node>
		class add_cfg_details;

		static inline constexpr const edge_annotation_t hrz_edge_annotation='h';
		static inline constexpr const edge_annotation_t vertical_edge_annotation='v';
		static inline constexpr const edge_annotation_t sequential_edge_annotation='s';

		[[nodiscard]] control_flow_graph() noexcept(false)
		: cfg_(), vertex_names(boost::get(boost::vertex_name, cfg_)), edge_names(boost::get(boost::edge_name, cfg_)), main_(boost::add_vertex(cfg_)) {
			vertex_names[main_]="main()";
		}
		control_flow_graph(control_flow_graph const &)=delete;

	private:
		template<class Node>
		friend class add_cfg_details;

		mutable atomic_t lock_;

		vertex_t add_vertex_nolk(node_property_t const &details) noexcept(false) {
			const vertex_t v=boost::add_vertex(cfg_);
			vertex_names[v]=details;
			return v;
		}

		std::pair<edge_t, bool> add_edge_nolk(vertex_t const &u, vertex_t const &v, node_property_t::value_type const details) noexcept(false) {
			const std::pair<edge_t, bool> added_edge=boost::add_edge(u, v, cfg_);
			edge_names[added_edge.first]=details;
			return added_edge;
		}
		std::pair<edge_t, bool> add_edge_to_root_nolk(vertex_t const &v, node_property_t::value_type const details) noexcept(false) {
			return add_edge_nolk(main_, v, details);
		}

		void update_edge_nolk(vertex_t &v, node_property_t::value_type const e_details) {
			typedef typename boost::graph_traits<container_type>::in_edge_iterator in_edge_iterator;

			std::pair<in_edge_iterator, in_edge_iterator> edges=boost::in_edges(v, cfg_);
			edge_names[*edges.first]=e_details;
		}

		typedef boost::property_map<container_type, boost::vertex_name_t>::type vertex_names_t;
		typedef boost::property_map<container_type, boost::edge_name_t>::type edge_names_t;

		container_type cfg_;
		vertex_names_t vertex_names;
		edge_names_t edge_names;
		const vertex_t main_;
	};

} } }

#include "thread_statistics_impl.hpp"

#endif
