#ifndef LIBJMMCG_CORE_CONSTRAINED_OVERRIDE_TYPE_HPP
#define LIBJMMCG_CORE_CONSTRAINED_OVERRIDE_TYPE_HPP

/******************************************************************************
 * * Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "blatant_old_msvc_compiler_hacks.hpp"

#include <tuple>
#include <utility>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace mpl {

/// This namespace contains a type that permits a set of overloaded member-functions to be called polymorphically via a generated interface & generated derived, concrete type.
/**
 * A compile-type code-generator has been written that, given {member_function_type<result_type, arguments...>...} of size N, generates a type that contains the N pure-virtual functions called process(...) that returns the result_type and takes those arguments. Then a class is generated that derives from those abstract base-classes which overrides each of those pure-virtual functions.
 * 
 * The resultant type is contained in result_type.
 * \Notes:
 * -# Multiple inheritance is not used to avoid undue complexity in implementation by the compiler.
 * -# The algorithmic complexity is O(N).
 * 
 * \see member_function_type, result_type, base_type
 */
namespace constrained_override_type {

/// The ultimate base-class of the type that is generated.
class base_type {
public:
	virtual ~base_type()=default;

protected:
	base_type()=default;
};

template<class RetT, class... ArgsT>
struct member_function_type {
	using result_type=RetT;
	using args_type=std::tuple<ArgsT...>;
};

template<class...>
struct to_member_function_type;
template<class RetT, class... ArgsT>
struct to_member_function_type<RetT, std::tuple<ArgsT...>> {
	using type=member_function_type<RetT, ArgsT...>;
};

namespace private_ {

	template<class T>
	struct finalizer final : T {
		using T::T;
	};

}

namespace polymorphic {

	namespace private_ {

		template<class, class...>
		struct abstract_type_unroller;
		template<class ObjT, class RetT, class... ArgsT>
		struct abstract_type_unroller<ObjT, member_function_type<RetT, ArgsT...>>;
		template<class ObjT, class RetT, class... ArgsT, class... FnTs>
		struct abstract_type_unroller<ObjT, member_function_type<RetT, ArgsT...>, FnTs...>;

		template<class BaseT, class...>
		struct concrete_type_unroller;
		template<class BaseT, class RetT, class... ArgsT>
		struct concrete_type_unroller<BaseT, member_function_type<RetT, ArgsT...>>;
		template<class BaseT, class RetT, class... ArgsT, class... FnTs>
		struct concrete_type_unroller<BaseT, member_function_type<RetT, ArgsT...>, FnTs...>;

	}

	/// The resultant type that is generated.
	/**
	* There must be at least two overloaded functions in object_type,
	*/
	template<class ObjT, class FnT1, class FnT2, class... FnTs>
	struct result_type {
		/// The object to be derived from that defines the set of member-function overloads called process(...) as specified in the set of member_function_types.
		using object_type=ObjT;
		/// The {member_function_type<result_type, arguments...>} for each of the overloads of object_type::process(...).
		/**
		* \see member_function_type, object_type
		*/
		using member_function_types=std::tuple<FnT1, FnT2, FnTs...>;
		/// The ultimate abstract-base type that contains all the pure-virtual overloaded process(...) functions.
		/**
		* This derives from base_type and object_type, such that it is Liskof-substitutable in the "is-a" sense for object_type.
		* This type permits one to call the overloaded object_type::process(...) member-functions via this base-class, i.e. enables polymorphism.
		* 
		* \Notes
		* -# The algorithmic complexity is O(N).
		* =# This functionality might seem an awful lot like an "implementation of C++ virtual, member template-functions" (which is not supported as of C++23, IIRC), but this is not: this permits calls to a *constrained* set of pverloads via an interface class, i.e. polymporphically. But this set of overloads must be known at *the current point [Ed: note singular] of instantiation*. if such a thing as a virtual member template-function could be accurately specified, one could imagine it to be "open" in the sense that an infinite set of overloads could be created, depending upon the point*s* [Ed: notr plural] of instantiation, i.e. previous PoIs would have to be updated according to later ones, yet C++ is mandated to be a single-pass compiler, which this naive description would violate.
		* 
		* \see base_type, object_type
		*/
		using abstract_base_type=private_::abstract_type_unroller<object_type, FnT1, FnT2, FnTs...>;
		/// The concrete resultant-type that may be instantiated and the suitable overload of the functions process(...) may be called that will call the correct overload of object_type::process(...).
		/**
		* This derives from abstract_base_type, therefore object_type, such that it is Liskof-substitutable in the "is-a" sense for object_type.
		* 
		* \Notes
		* -# The algorithmic complexity is O(N).
		* 
		* \see abstract_base_type, object_type
		*/
		using type=private_::concrete_type_unroller<abstract_base_type, FnT1, FnT2, FnTs...>;
		/// A convenience-using to a final version of type.
		using final_type=constrained_override_type::private_::finalizer<type>;
	};

}

namespace compile_time {

	namespace private_ {

		template<class...>
		struct abstract_type_unroller;
		template<class RetT>
		struct abstract_type_unroller<member_function_type<RetT>>;
		template<class RetT, class ArgT1, class... ArgsT>
		struct abstract_type_unroller<member_function_type<RetT, ArgT1, ArgsT...>>;

		template<class RetT, class... FnTs>
		struct abstract_type_unroller<member_function_type<RetT>, FnTs...>;
		template<class RetT, class ArgT1, class... ArgsT, class... FnTs>
		struct abstract_type_unroller<member_function_type<RetT, ArgT1, ArgsT...>, FnTs...>;

		template<class, class, class, class...>
		struct concrete_type;
		template<class BaseT, class ObjT, class RetT>
		struct concrete_type<BaseT, ObjT, member_function_type<RetT>>;
		template<class BaseT, class ObjT, class RetT, class ArgT1, class... ArgsT>
		struct concrete_type<BaseT, ObjT, member_function_type<RetT, ArgT1, ArgsT...>>;
		template<class BaseT, class ObjT, class RetT, class... FnTs>
		struct concrete_type<BaseT, ObjT, member_function_type<RetT>, FnTs...>;

	}

	/// The resultant type that is generated.
	/**
	 * We permit the odd case of generating an "overload" for just one function in object_type, (This is required due to implementation details in he msm; it is reasonable to have one member-function called event_t::process(...) and no overloads.)
	 */
	template<class FnT, class... FnTs>
	struct result_type {
		/// The {member_function_type<result_type, arguments...>} for each of the overloads of object_type::process(...).
		/**
		* \see member_function_type
		*/
		using member_function_types=std::tuple<FnT, FnTs...>;
		/// The ultimate abstract-base type that contains all the pure-virtual overloaded process(...) functions.
		/**
		* This derives from base_type and object_type, such that it is Liskof-substitutable in the "is-a" sense for object_type.
		* This type permits one to call the overloaded object_type::process(...) member-functions via this base-class, i.e. enables polymorphism.
		* 
		* \Notes
		* -# The algorithmic complexity is O(N).
		* =# This functionality might seem an awful lot like an "implementation of C++ virtual, member template-functions" (which is not supported as of C++23, IIRC), but this is not. This code permits calls to a *constrained* set of *overloads* via an interface class, i.e. polymporphically. Moreover this set of overloads must be known at *the current point [Ed: note singular] of instantiation*. If such a thing as a "virtual member template-function" could be accurately specified, one could imagine it to be "open" in the sense that an infinite set of overloads could be created, depending upon the point*s* [Ed: note plural] of instantiation, i.e. previous PoIs would have to be updated according to later ones, yet C++ is mandated to be a single-pass compiler, which this naive description would violate.
		* 
		* \see base_type
		*/
		using abstract_base_type=private_::abstract_type_unroller<FnT, FnTs...>;
		/// The concrete resultant-type that may be instantiated and the suitable overload of the functions process(...) may be called that will call the correct overload of object_type::process(...).
		/**
		* This derives from abstract_base_type, therefore object_type, such that it is Liskof-substitutable in the "is-a" sense for object_type.
		* 
		* \Notes
		* -# The algorithmic complexity is O(N).
		* 
		* \see abstract_base_type, object_type
		*/
		template<class ObjT>
		using type=private_::concrete_type<abstract_base_type, ObjT, FnT, FnTs...>;
		/// A convenience-using to a final version of type.
		template<class ObjT>
		using final_type=constrained_override_type::private_::finalizer<type<ObjT>>;
	};

}

}

} } }

#include "constrained_override_type_impl.hpp"

#endif
