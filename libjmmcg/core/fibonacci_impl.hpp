/******************************************************************************
** Copyright © 2013 by J.M.Hearne-McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace mpl {

	/**
	 *	\test Various tests.
	 */
	namespace tests {

		BOOST_MPL_ASSERT_RELATION(fibonacci<0>::value, ==, 1ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<1>::value, ==, 1ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<2>::value, ==, 2ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<3>::value, ==, 3ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<4>::value, ==, 5ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<5>::value, ==, 8ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<6>::value, ==, 13ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<7>::value, ==, 21ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<8>::value, ==, 34ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<9>::value, ==, 55ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci<10>::value, ==, 89ULL);

	}

}

namespace dyn {

	inline constexpr fibonacci::element_type
	fibonacci::result(element_type N) noexcept(true) {
		// fib(2)=fib(2-1)+fib(2-2)=fib(1)+fib(0)=1+1
		// fib(3)=fib(3-1)+fib(3-2)=fib(2)+fib(1)
		// 1, 1, 2,     3,     5,     8, ...
		// 1, 1, (1+1), (1+2), (2+3), (3+5), ...
		element_type fib_im2=0, fib_im1=1, fib_i=fib_im1+fib_im2;
		for (element_type i=0; i<N; ++i) {
			fib_i=fib_im1+fib_im2;
			fib_im2=fib_im1;
			fib_im1=fib_i;
		}
		// i=0: fib_im2=1, fib_im1=1, fib_i=fib_im1+fib_im2=1+1=2
		// i=1: fib_im2=1, fib_im1=1, fib_i=fib_im1+fib_im2=1+1=2
		// i=2: fib_i=fib_im1+fib_im2=1+1=2, fib_im2=fib_im1=1, fib_im1=fib_i=2
		// i=3: fib_i=fib_im1+fib_im2=2+1=3, fib_im2=fib_im1=2, fib_im1=fib_i=3
		// i=4: fib_i=fib_im1+fib_im2=3+2=5, fib_im2=fib_im1=3, fib_im1=fib_i=5
		return fib_i;
	}

	inline constexpr fibonacci_pe::element_type
	fibonacci_pe::result(element_type n) noexcept(true) {
		// Consider transformation Tpq on variables a and b of successive values of the
		// Fibonacci series starting with (a, b) = (1, 0):
		// Tpq    : a <- bq + aq + ap and b <- bp + aq
		// Tpq**2 : a <- (bp + aq)*q + (bq + aq + ap)*q + (bq + aq + ap)*p and b <- (bp + aq)*p + (bq + aq + ap)*q
		// Tpq**2 : a <- bpq + aqq + bqq + aqq + apq + bqp + aqp + app and b <- bpp + aqp + bqq + aqq + apq
		// Tpq**2 : a <- 2*bpq + 2*aqq + 2*apq + app + bqq and b <- bpp + bqq + 2*apq + aqq
		// Tpq**2 : a <- 2*bpq + bqq + 2*apq + 2*aqq + app and b <- bpp + bqq + 2*apq + aqq
		// Tpq**2 : a <- b*(2*pq + qq) + a*(2*pq + qq) + a*(qq + pp) and b <- b*(qq + pp) + a*(2*pq + qq)
		// therefore p' = qq + pp
		// and q' = 2*pq + qq
		element_type a=1, b=1, p=0, q=1;
		while (n!=0) {
			if (n%2==0) {
				std::tie(p, q)=std::make_tuple(q*q+p*p, 2*p*q+q*q);
				n/=2;
			} else {
				std::tie(a, b)=std::make_tuple(b*q+a*q+a*p, b*p+a*q);
				--n;
			}
		}
		return b;
	}

	/**
	 *	\test Various tests.
	 */
	namespace tests {

		BOOST_MPL_ASSERT_RELATION(fibonacci::result(0), ==, 1ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(1), ==, 1ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(2), ==, 2ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(3), ==, 3ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(4), ==, 5ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(5), ==, 8ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(6), ==, 13ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(7), ==, 21ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(8), ==, 34ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(9), ==, 55ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci::result(10), ==, 89ULL);

		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(0), ==, 1ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(1), ==, 1ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(2), ==, 2ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(3), ==, 3ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(4), ==, 5ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(5), ==, 8ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(6), ==, 13ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(7), ==, 21ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(8), ==, 34ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(9), ==, 55ULL);
		BOOST_MPL_ASSERT_RELATION(fibonacci_pe::result(10), ==, 89ULL);

	}

}

} }
