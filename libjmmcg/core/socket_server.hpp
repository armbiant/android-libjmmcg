#ifndef LIBJMMCG_CORE_SOCKET_SERVER_HPP
#define LIBJMMCG_CORE_SOCKET_SERVER_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "application.hpp"
#include "exception.hpp"
#include "jthread.hpp"
#include "latency_timestamps.hpp"
#include "lock_mem.hpp"
#include "logging.hpp"
#include "socket_server_manager.hpp"

#include <boost/exception/diagnostic_information.hpp>
#include <boost/exception_ptr.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

#include <iostream>
#include <iterator>
#include <utility>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket {

/// A very simple socket-based server.
/**
	This server only handles a single client at any time. Re-connections are possible.
 */
template<
	class SvrMgr	///< The particular type of server that should be run. e.g. does it forward messages, or send them back?
	>
class svr final : public application {
public:
	using base_t= application;
	using svr_mgr_t= SvrMgr;
	using server_to_client_flow_t= typename svr_mgr_t::server_to_client_flow_t;
	using socket_t= typename svr_mgr_t::socket_t;
	using socket_priority= typename svr_mgr_t::socket_priority;
	using proc_rules_t= typename svr_mgr_t::proc_rules_t;
	using msg_details_t= typename svr_mgr_t::src_msg_details_t;
	using src_msg_details_t= typename svr_mgr_t::src_msg_details_t;
	using thread_t= ppd::jthread<ppd::platform_api, ppd::heavyweight_threading>;
	/// If the current processor detects a failure (via an exception thrown) when running the run() method, it will use an instance of this call-back so that this error may be alternatively handled.
	/**
	 * Note that a reference to this operation is taken, therefore it should be re-entrant, i.e. thread-safe. Moreover the operation must not leak any exceptions otherwise the behaviour is undefined.
	 *
	 * \param svr_details	The details of the server from which the error originated.
	 * \param eptr	Any exception-pointer that was caught in the process of running the server.
	 */
	using report_error_fn_t= std::function<void(std::string svr_details, std::exception_ptr eptr)>;

	struct ctor_args {
		using addr_t= boost::asio::ip::address;
		using port_t= unsigned short;

		const addr_t address;	///< The IPv4 or IPv6 address to which the client connection should be made.
		const port_t port_num;	 ///< The port number to which the connection should be made.
		const unsigned short timeout;	  ///< The timeout for the socket that comprises the endpoint.
		const socket_priority qos_priority;	  ///< The priority at which the TCP messages should be sent, relating to QoS.
		const thread_t::thread_traits::api_params_type::processor_mask_type mask;	 ///< The CPU on which the kernel socket-handling thread should run, if supported.
		const thread_t::thread_traits::api_params_type::priority_type cpu_priority;	///< The priority of the server thread that is used to process the messages.

		ctor_args(addr_t addr, const port_t port, const unsigned short t, const socket_priority pri, const unsigned short core, const thread_t::thread_traits::api_params_type::priority_type cpu_pri)
			: address(std::move(addr)), port_num(port), timeout(t), qos_priority(pri), mask(typename thread_t::thread_traits::api_params_type::processor_mask_type(core)), cpu_priority(cpu_pri) {
		}
		ctor_args(addr_t addr, const port_t port, const socket_priority pri, const unsigned short core, const thread_t::thread_traits::api_params_type::priority_type cpu_pri)
			: address(std::move(addr)), port_num(port), timeout((svr_mgr_t::heartbeats_t::max_missed_heartbeats * svr_mgr_t::heartbeats_t::heartbeat_interval).count()), qos_priority(pri), mask(typename thread_t::thread_traits::api_params_type::processor_mask_type(core)), cpu_priority(cpu_pri) {
		}
	};

	/// A simple socket-based server that listens to connections to the specified endpoint (e.g. a client that sends FIX messages) and responds to messages received in the specified manner. i.e. forwards the messages.
	/**
		\param	args	The aggregated arguments to construct the endpoint.
		\param	proc_rules	The processing rules for a loop-back based server.
		\parma	ts	A collection that is used to record certain important timestamps that may be used for generating a histogram (possibly on shut down).
		\param	svr_name	The name to be given to the thread. Useful for identifying the particular thread in a debugger (if the debugger is good enough).
		\param	server_to_client_flow	The functor that is used to implement how the messages from the server are sent to the client.

		\see ctor_args, pthread_name()
	*/
	svr(
		ctor_args const& args,
		typename svr_mgr_t::proc_rules_t const& proc_rules,
		report_error_fn_t& report_error,
		libjmmcg::latency_timestamps_itf& ts,
		thread_t::thread_traits::thread_name_t const& svr_name,
		server_to_client_flow_t&& server_to_client_flow= [](auto const&) {}) noexcept(false);
	/// A simple socket-based server that listens to connections to the specified endpoint (e.g. a client that sends FIX messages) and responds to messages received in the specified manner. i.e. forwards the messages.
	/**
	 * This function needs a template parameter because the *_sims perform a full template specialisation of the whole type, which tries to instantiate this function, but it only works for forwarding and not loopback links. Making it a template means it will not be instantiated. As simulators are only loopback capable, this ctor for the forwarding variant is invalid for a such a full instantiation.
	 *
	 * \todo: Had to inline the definition due to a bug in g++ v<=11.1.0. :(
	 *
		\param	args	The aggregated arguments to construct the endpoint.
		\param	exchg_links	The connections to one or more exchanges and how a message for a particular exchange should be processed.
		\parma	ts	A collection that is used to record certain important timestamps that may be used for generating a histogram (possibly on shut down).
		\param	svr_name	The name to be given to the thread. Useful for identifying the particular thread in a debugger (if the debugger is good enough).
		\param	server_to_client_flow	The functor that is used to implement how the messages from the server are sent to the client.

		\see ctor_args, pthread_name()
	*/
	template<class ExchgLinksT>
		requires(std::is_same<ExchgLinksT, typename svr_mgr_t::exchg_links_t>::value)
	svr(
		ctor_args const& args,
		ExchgLinksT& exchg_links,
		report_error_fn_t& report_error,
		libjmmcg::latency_timestamps_itf& ts,
		thread_t::thread_traits::thread_name_t const& svr_name,
		server_to_client_flow_t&& server_to_client_flow= [](auto const&) {}) noexcept(false)
		: base_t(),
		  address(args.address),
		  port_number(args.port_num),
		  report_error_(report_error),
		  manager(
			  exit_requested_,
			  report_error_,
			  address,
			  port_number,
			  proc_rules_t::src_msg_details_t::min_msg_size,
			  proc_rules_t::src_msg_details_t::max_msg_size,
			  args.timeout,
			  args.qos_priority,
			  args.mask.get_cpu(),
			  exchg_links,
			  ts,
			  std::move(server_to_client_flow)),
		  io_thread(svr_name, args.cpu_priority, args.mask, [this](std::atomic_flag& thread_exit_requested) {
			  try {
				  this->manager.run();
				  exit_requested_.test_and_set(std::memory_order_seq_cst);
				  exit_requested_.notify_all();
				  thread_exit_requested.test_and_set(std::memory_order_seq_cst);
				  thread_exit_requested.notify_all();
			  } catch(...) {
				  exit_requested_.test_and_set(std::memory_order_seq_cst);
				  exit_requested_.notify_all();
				  thread_exit_requested.test_and_set(std::memory_order_seq_cst);
				  thread_exit_requested.notify_all();
				  report_error_(to_string(), std::current_exception());
			  }
		  }) {
	}
	~svr() noexcept(true) override;

	void stop() noexcept(true);

	/// A main() method for instantiating a stand-alone simulator.
	/**
		Use this within the CRT-main().
	*/
	static int main(int argc, char const* const* argv) noexcept(true);

	std::string to_string() const noexcept(false);

private:
	boost::asio::ip::address const address;
	const unsigned short port_number;
	std::atomic_flag exit_requested_{};
	report_error_fn_t& report_error_;
	svr_mgr_t manager;
	thread_t io_thread;
};

template<class SvrMgr>
inline std::ostream&
operator<<(std::ostream& os, svr<SvrMgr> const& ec) noexcept(false);

}}}

#include "socket_server_impl.hpp"

#endif
