#ifndef LIBJMMCG_CORE_DELETER_HPP
#define LIBJMMCG_CORE_DELETER_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "blatant_old_msvc_compiler_hacks.hpp"

#include <cstdlib>
#include <functional>
#include <memory>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// A trivial class to use global free() to deallocate the memory using an RAII-style wrapper.
template<class T>
class free_ptr {
public:
	typedef T element_type;

	constexpr free_ptr() noexcept(true)= default;
	explicit constexpr free_ptr(element_type p) noexcept(true)
		: ptr(p) {
	}
	free_ptr(free_ptr const&)= delete;
	free_ptr(free_ptr&& fp) noexcept(true) {
		std::swap(ptr, fp.ptr);
	}
	~free_ptr() noexcept(true) {
		std::free(ptr);
	}

	free_ptr& operator=(free_ptr&& fp) noexcept(true) {
		std::swap(ptr, fp.ptr);
		return *this;
	}

	[[nodiscard]] element_type get() noexcept(true) {
		return ptr;
	}

private:
	element_type ptr{};
};

template<class T>
inline free_ptr<T>
make_free_ptr(T&& t) {
	return free_ptr<T>(std::forward<T>(t));
}

/**
	Unfortunately std::default_delete does not name the type passed to it. This class fixes that.

	\see std:default_delete
*/
template<class V>
struct default_delete : std::default_delete<V> {
	typedef V element_type;
};

/// Another trivial class to make calling the dtor of an object into a functor.
template<class V>
struct placement_dtor {
	typedef V element_type;

	void operator()(element_type* v) const noexcept(noexcept(std::declval<V>().~V())) {
		v->~V();
	}
};

/// Another trivial class to make calling the dtor of an object into a functor, but actually does nothing.
template<class V>
struct noop_dtor {
	typedef V element_type;

	constexpr void operator()(element_type*) const noexcept(true) {
	}
};

}}

#endif
