/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket {

template<class ProcessingRules> inline
msg_processor<ProcessingRules>::msg_processor(proc_rules_t const &proc_rules)
: processing_rules(proc_rules) {
}

template<class ProcessingRules>
template<class SktT>
inline bool
msg_processor<ProcessingRules>::read_msg_into_buff(SktT &src_skt, msg_buffer_t &buff) noexcept(false) {
	return src_skt.template read<msg_details_t>(buff);
}

template<class ProcessingRules>
template<class SktT>
inline bool
msg_processor<ProcessingRules>::read_and_process_a_msg(msg_buffer_t const &buff, SktT &src_skt, SktT &dest_skt, libjmmcg::latency_timestamps_itf &ts) noexcept(false) {
	const libjmmcg::latency_timestamps_itf::period t(ts);
	return processing_rules.process_msg(buff, src_skt, dest_skt);
}

template<class ProcessingRules>
template<class SktT, class ClientCxn>
inline bool
msg_processor<ProcessingRules>::read_and_process_a_msg(msg_buffer_t const &buff, SktT &src_skt, ClientCxn &dest_cxn, libjmmcg::latency_timestamps_itf &ts) noexcept(false) {
	const libjmmcg::latency_timestamps_itf::period t(ts);
	return processing_rules.process_msg(buff, src_skt, dest_cxn);
}

template<class ProcessingRules> inline std::string
msg_processor<ProcessingRules>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss
		<<boost::core::demangle(typeid(*this).name())
		<<",\nprocessing_rules: "<<processing_rules;
	return ss.str();
}

template<class ProcessingRules> inline std::ostream &
operator<<(std::ostream &os, msg_processor<ProcessingRules> const &ec) noexcept(false) {
	os<<ec.to_string();
	return os;
}

} } }
