#ifndef LIBJMMCG_CORE_EXIT_CODES_HPP
#define LIBJMMCG_CORE_EXIT_CODES_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <cassert>
#include <cerrno>
#include <csignal>
#include <cstdint>
#include <sstream>
#include <string>
#include <tuple>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	/// A nice enum for the exit codes from main().
	/**
		There is some conflicting advice regarding user-defined exit coeds and permissible values:
		-# <a href="https://www.tldp.org/LDP/abs/html/exitcodes.html"/> - between 64-113.
		-# Also see <a href="https://www.freebsd.org/cgi/man.cgi?query=sysexits&apropos=0&sektion=0&manpath=FreeBSD+4.3-RELEASE&format=html">sysexits.h</a> implies values over 128 should be used.
		
		Note use of POSIX compatibility macros & starting our custom errors at 129. Also note that we shouldn't exceed 255, as the exit code is truncated to 8 bits, which would cause problems...
	*/
	class exit_codes {
	public:
		enum codes : std::uint8_t {
			exit_success=EXIT_SUCCESS,
			exit_unknown_failure=EXIT_FAILURE,
			exit_unknown_exception=128+NSIG,	///< User-defined codes start from here according to "signum-generic.h".
			exit_stl_exception,
			exit_jmmcg_exception,
			exit_crt_exception,
			exit_parameter_error,
			exit_could_not_forward_message,
			exit_print_help,
			exit_print_version
		};

		static std::string
		to_string(codes e) noexcept(false);
		static std::string
		to_string() noexcept(false);

	private:
		static inline constexpr const std::tuple descriptions{
			std::tuple{exit_success, "exit_success", "The process completed successfully."},
			std::tuple{exit_unknown_failure, "exit_unknown_failure", "Error: an unknown failure-condition caused the process to exit."},
			std::tuple{exit_unknown_exception, "exit_unknown_exception", "Error: an unknown exception was thrown that caused the process to exit."},
			std::tuple{exit_stl_exception, "exit_stl_exception", "Error: an STL-derived exception was thrown that caused the process to exit."},
			std::tuple{exit_jmmcg_exception, "exit_jmmcg_exception", "Error: a libjmmcg-derived exception was thrown that caused the process to exit."},
			std::tuple{exit_crt_exception, "exit_crt_exception", "Error: a libjmmcg-derived CRT-exception was thrown that caused the process to exit."},
			std::tuple{exit_parameter_error, "exit_parameter_error", "Error: an incorrect set of command-line options caused the process to exit."},
			std::tuple{exit_could_not_forward_message, "exit_could_not_forward_message", "Error: the process could not forward all of the messages to the connect client(s)."},
			std::tuple{exit_print_help, "exit_print_help", "The '--help' command-line option caused the process to exit."},
			std::tuple{exit_print_version, "exit_print_version", "The '--version' command-line option caused the process to exit."}
		};
	};

} }

#include "exit_codes_impl.hpp"

#endif
