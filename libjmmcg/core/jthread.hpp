#ifndef LIBJMMCG_CORE_JTHREAD_HPP
#define LIBJMMCG_CORE_JTHREAD_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_wrapper.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

/// This class is used to implement a class that wraps using a thread, it is an alternative to using a thread pool to manage your threads, in a C++ std::jthread-style manner.
/**
 * Note that this allocates the work to be processed on the heap, like std::jthread, but nothing else should be allocated.
 *
 *	\see ppd::wrapper, std::jthread
 */
template<
	generic_traits::api_type::element_type API,
	typename Mdl>
class jthread final {
private:
	class set_thread_name_thread_worker_context {
	public:
		/**
			To assist in allowing compile-time computation of the algorithmic order of the threading model.
		*/
		static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= ppd::generic_traits::memory_access_modes::crew_memory_access;

		using thread_t= private_::thread_base<API, Mdl>;
		using setter_fn= std::function<void(private_::thread_base<API, Mdl>& thread)>;

		explicit set_thread_name_thread_worker_context(setter_fn&& name_setter)
			: name_setter_(std::move(name_setter)) {}

		void operator()(thread_t& thread) {
			name_setter_.operator()(thread);
		}

	private:
		setter_fn name_setter_;
	};
	/**
	 * This is constructed in the thread_t class as a member, so the ctor will never run in the thread contained in the thread_t.
	 */
	struct mutator final {
		/// Execute the mutator repeatedly within the wrapped thread.
		using mutator_t= std::function<void(std::atomic_flag&)>;

		/**
			To assist in allowing compile-time computation of the algorithmic order of the threading model.
		*/
		static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= ppd::generic_traits::memory_access_modes::crew_memory_access;

		mutator_t mutator_;

		explicit mutator(mutator_t m);

		/**
		 * This function-call operator will be called repeatedly on the contained thread.
		 *
		 * @param exit_requested Set to indicate that upon return, the contained thread should return normally, thereafter this function shall cease to be called.
		 *
		 * \see mutator_t.
		 */
		void operator()(std::atomic_flag& exit_requested, set_thread_name_thread_worker_context&) noexcept(false);
	};

public:
	using thread_t= wrapper<mutator, API, Mdl, set_thread_name_thread_worker_context>;
	using mutator_t= typename thread_t::mutator_t;
	using thread_context_t= typename thread_t::thread_context_t;
	using lock_traits= typename thread_t::lock_traits;
	using thread_traits= typename thread_t::thread_traits;
	using os_traits= typename thread_t::os_traits;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= thread_t::memory_access_mode;

	/// Create the wrapper object. Note that the underlying kernel thread will have been created, so the proc_fn will execute in the underlying thread (if any).
	/**
		This ctor requires a memory allocation for the functor.

		\param	proc_fn	The functor that will executed by the underlying kernel thread (if any).

		\see create_running()
	*/
	[[nodiscard]] explicit __stdcall jthread(typename mutator::mutator_t proc_fn) noexcept(false);
	[[nodiscard]] explicit __stdcall jthread(std::function<void()> proc_fn) noexcept(false);
	[[nodiscard]] explicit __stdcall jthread(typename thread_traits::thread_name_t const& name, typename thread_traits::api_params_type::priority_type const cpu_priority, typename thread_traits::api_params_type::processor_mask_type const mask, typename mutator::mutator_t proc_fn) noexcept(false);
	[[nodiscard]] explicit __stdcall jthread(typename thread_traits::thread_name_t const& name, typename thread_traits::api_params_type::priority_type const cpu_priority, typename thread_traits::api_params_type::processor_mask_type const mask, std::function<void()> proc_fn) noexcept(false);
	template<std::size_t NameSize>
		requires(NameSize <= thread_traits::max_thread_name_length)
	[[nodiscard]] explicit __stdcall jthread(char const (&name)[NameSize], typename mutator::mutator_t proc_fn) noexcept(false);
	template<std::size_t NameSize>
		requires(NameSize <= thread_traits::max_thread_name_length)
	[[nodiscard]] explicit __stdcall jthread(char const (&name)[NameSize], std::function<void()> proc_fn) noexcept(false);

	[[nodiscard]] bool __fastcall is_running() const noexcept(true);

	[[nodiscard]] typename thread_traits::api_params_type::priority_type __fastcall kernel_priority() const noexcept(false);
	void __fastcall kernel_priority(const typename thread_traits::api_params_type::priority_type priority) noexcept(false);
	[[nodiscard]] typename thread_traits::api_params_type::processor_mask_type __fastcall kernel_affinity() const noexcept(false);
	void __fastcall kernel_affinity(const typename thread_traits::api_params_type::processor_mask_type& mask) noexcept(false);
	void __fastcall set_name(typename thread_traits::thread_name_t const& name) noexcept(false);
	template<std::size_t NameSize>
		requires(NameSize <= thread_traits::max_thread_name_length)
	void __fastcall set_name(char const (&name)[NameSize]) noexcept(false);

private:
	thread_t thread_;
};

}}}

#include "jthread_impl.hpp"

#endif
