/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace fma {

namespace private_ {

inline constexpr
dbl_mul_add::dbl_mul_add(dbl_mul const &m, double const a) noexcept(true)
: mul_(m), add_(a) {}

inline constexpr
dbl_mul_add::dbl_mul_add(dbl_add const &a, double const m) noexcept(true)
: mul_(m, a.lhs_), add_(a.rhs_) {}

inline constexpr
dbl_mul_add::dbl_mul_add(dbl_sub const &a, double const m) noexcept(true)
: mul_(m, a.lhs_), add_(-a.rhs_) {}

inline
dbl_mul_add::operator double () const noexcept(true) {
	return std::fma(mul_.lhs_, mul_.rhs_, add_);
}

inline constexpr
dbl_mul::dbl_mul(double const &l, double const &r) noexcept(true)
: lhs_(l), rhs_(r) {}

inline constexpr dbl_mul
dbl_mul::operator*(const double r) const noexcept(true) {
	return dbl_mul(lhs_, rhs_*r);
}

inline constexpr dbl_mul
dbl_mul::operator*(const dbl r) const noexcept(true) {
	return dbl_mul(lhs_, rhs_*r.lhs_);
}

inline constexpr dbl_mul_add
dbl_mul::operator+(const double a) const noexcept(true) {
	return dbl_mul_add(*this, a);
}

inline constexpr dbl_mul_add
dbl_mul::operator+(const dbl a) const noexcept(true) {
	return dbl_mul_add(*this, a.lhs_);
}

inline constexpr dbl_mul_add
dbl_mul::operator-(const double a) const noexcept(true) {
	return dbl_mul_add(*this, -a);
}

inline constexpr dbl_mul_add
dbl_mul::operator-(const dbl a) const noexcept(true) {
	return dbl_mul_add(*this, -a.lhs_);
}

inline constexpr dbl_mul_add
operator+(const double a, dbl_mul const &d) noexcept(true) {
	return dbl_mul_add(d, a);
}

inline constexpr dbl_mul_add
operator-(const double a, dbl_mul const &d) noexcept(true) {
	return dbl_mul_add(dbl_mul(-d.lhs_, d.rhs_), a);
}

inline constexpr
dbl_add::dbl_add(double const l, double const r) noexcept(true)
: lhs_(l), rhs_(r) {}


inline constexpr
dbl_sub::dbl_sub(double const l, double const r) noexcept(true)
: lhs_(l), rhs_(r) {}

inline double &
operator*=(double &l, dbl_add const &r) noexcept(true) {
	l=std::fma(l, r.lhs_, r.rhs_);
	return l;
}

inline double &
operator*=(double &l, dbl_sub const &r) noexcept(true) {
	l=std::fma(l, r.lhs_, -r.rhs_);
	return l;
}

inline double &
operator+=(double &a, dbl_mul const &m) noexcept(true) {
	a=std::fma(m.lhs_, m.rhs_, a);
	return a;
}

inline double &
operator-=(double &a, dbl_mul const &m) noexcept(true) {
	a=std::fma(m.lhs_, m.rhs_, -a);
	return a;
}

}

inline constexpr
dbl::dbl(double const l) noexcept(true)
: lhs_(l) {}

inline
dbl::dbl(private_::dbl_mul_add const &l) noexcept(true)
: lhs_(static_cast<double>(l)) {}

inline constexpr bool
dbl::operator==(const double r) const noexcept(true) {
	return lhs_==r;
}

inline constexpr bool
dbl::operator==(const dbl r) const noexcept(true) {
	return lhs_==r.lhs_;
}

inline constexpr private_::dbl_mul
dbl::operator*(const dbl r) const noexcept(true) {
	return private_::dbl_mul(lhs_*r);
}

inline constexpr private_::dbl_mul
dbl::operator*(const double r) const noexcept(true) {
	return private_::dbl_mul(lhs_, r);
}

inline constexpr private_::dbl_add
dbl::operator+(const double r) const noexcept(true) {
	return private_::dbl_add(lhs_, r);
}

inline constexpr private_::dbl_sub
dbl::operator-(const double r) const noexcept(true) {
	return private_::dbl_sub(lhs_, r);
}

inline constexpr private_::dbl_mul_add
operator+(const dbl a, private_::dbl_mul const &d) noexcept(true) {
	return private_::dbl_mul_add(d, a.lhs_);
}

inline constexpr private_::dbl_mul_add
operator-(const dbl a, private_::dbl_mul const &d) noexcept(true) {
	return private_::dbl_mul_add(private_::dbl_mul(-d.lhs_, d.rhs_), a.lhs_);
}

inline constexpr private_::dbl_mul
operator*(const double l, dbl const r) noexcept(true) {
	return private_::dbl_mul(l, r.lhs_);
}

inline std::ostream &
operator<<(std::ostream &os, dbl const d) noexcept(false) {
	return os<<d.lhs_;
}

} } }
