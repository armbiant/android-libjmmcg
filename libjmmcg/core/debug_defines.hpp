/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#if defined(_DEBUG) && !defined(DEBUG)
#	define DEBUG
#endif
#if defined(DEBUG) && !defined(_DEBUG)
#	define _DEBUG
#endif

#if !defined(NDEBUG) && !defined(DEBUG)
#	define NDEBUG
#endif

#if defined(NDEBUG) && defined(DEBUG)
#	warning You have defined both 'NDEBUG' & 'DEBUG' which is mutually contradictory. Please modify your command-line or make file to correct this. Assuming you want 'DEBUG' defined for now.
#	undef NDEBUG
#endif

#include "blatant_old_msvc_compiler_hacks.hpp"
