/******************************************************************************
 * * Copyright © 2022 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/**
 * \todo Why do I need this? Why am I getting these warnings?
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsuggest-override"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace mpl { namespace constrained_override_type {

namespace polymorphic {

	namespace private_ {

		template<class ObjT, class RetT, class... ArgsT>
		struct abstract_type_unroller<ObjT, member_function_type<RetT, ArgsT...>> : ObjT, base_type {
			using base_t=base_type;
			using object_type=ObjT;
			using fn_type=member_function_type<RetT, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			
			template<class... ArgsT1>
			explicit abstract_type_unroller(ArgsT1 &&...args) noexcept(
				noexcept(object_type(std::forward<ArgsT1>(args)...))
				&& noexcept(std::declval<base_t>())
			)
			: object_type(std::forward<ArgsT1>(args)...), base_t() {
			}
			
			virtual result_type process(ArgsT...)=0;
		};
		template<class ObjT, class RetT, class... ArgsT, class... FnTs>
		struct abstract_type_unroller<ObjT, member_function_type<RetT, ArgsT...>, FnTs...> : abstract_type_unroller<ObjT, FnTs...> {
			using base_t=abstract_type_unroller<ObjT, FnTs...>;
			using object_type=typename base_t::object_type;
			using fn_type=member_function_type<RetT, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::base_t;
			using base_t::process;
			
			virtual result_type process(ArgsT...) noexcept(false)=0;
		};
		
		template<class BaseT, class RetT, class... ArgsT>
		struct concrete_type_unroller<BaseT, member_function_type<RetT, ArgsT...>> : BaseT {
			using base_t=BaseT;
			using object_type=typename base_t::object_type;
			using fn_type=member_function_type<RetT, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::base_t;
			using base_t::process;

			REALLY_FORCE_INLINE result_type process(ArgsT &&...args) noexcept(
				noexcept(
					this->object_type::process(args...)
				)
			) override {
				return this->object_type::process(std::forward<ArgsT>(args)...);
			}
		};
		template<class BaseT, class RetT, class... ArgsT, class... FnTs>
		struct concrete_type_unroller<BaseT, member_function_type<RetT, ArgsT...>, FnTs...> : concrete_type_unroller<BaseT, FnTs...> {
			using base_t=concrete_type_unroller<BaseT, FnTs...>;
			using object_type=typename base_t::object_type;
			using fn_type=member_function_type<RetT, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::base_t;
			using base_t::process;

			REALLY_FORCE_INLINE result_type process(ArgsT &&...args) noexcept(
				noexcept(
					this->object_type::process(args...)
				)
			) override {
				return this->object_type::process(std::forward<ArgsT>(args)...);
			}
		};

	}

}

namespace compile_time {

	namespace private_ {

		template<class RetT>
		struct abstract_type_unroller<member_function_type<RetT>> : base_type {
			using base_t=base_type;
			using fn_type=member_function_type<RetT>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			
			virtual result_type process() const noexcept(false)=0;
			virtual result_type process() noexcept(false)=0;
		};
		template<class RetT, class ArgT1, class... ArgsT>
		struct abstract_type_unroller<member_function_type<RetT, ArgT1, ArgsT...>> : base_type {
			using base_t=base_type;
			using fn_type=member_function_type<RetT, ArgT1, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			
			virtual result_type process(ArgT1 &&, ArgsT &&...) const noexcept(false)=0;
			virtual result_type process(ArgT1 &&, ArgsT &&...) noexcept(false)=0;
		};
		
		template<class RetT, class... FnTs>
		struct abstract_type_unroller<member_function_type<RetT>, FnTs...> : abstract_type_unroller<FnTs...> {
			using base_t=abstract_type_unroller<FnTs...>;
			using fn_type=member_function_type<RetT>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::process;
			
			virtual result_type process() const noexcept(false)=0;
			virtual result_type process() noexcept(false)=0;
		};
		template<class RetT, class ArgT1, class... ArgsT, class... FnTs>
		struct abstract_type_unroller<member_function_type<RetT, ArgT1, ArgsT...>, FnTs...> : abstract_type_unroller<FnTs...> {
			using base_t=abstract_type_unroller<FnTs...>;
			using fn_type=member_function_type<RetT, ArgT1, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::process;
			
			virtual result_type process(ArgT1 &&, ArgsT &&...) const noexcept(false)=0;
			virtual result_type process(ArgT1 &&, ArgsT &&...) noexcept(false)=0;
		};

		template<class BaseT, class ObjT, class RetT>
		struct concrete_type<BaseT, ObjT, member_function_type<RetT>> : ObjT, BaseT {
			using base_t=BaseT;
			using object_type=ObjT;
			using fn_type=member_function_type<RetT>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			
			template<class... ArgsT1>
			explicit concrete_type(ArgsT1 &&...args) noexcept(
				noexcept(object_type(std::forward<ArgsT1>(args)...))
				&& noexcept(std::declval<base_t>())
			)
			: object_type(std::forward<ArgsT1>(args)...), base_t() {
			}

			REALLY_FORCE_INLINE result_type process() const noexcept(
				noexcept(
					this->object_type::process()
				)
			) override {
				return this->object_type::process();
			}
			REALLY_FORCE_INLINE result_type process() noexcept(
				noexcept(
					this->object_type::process()
				)
			) override {
				return this->object_type::process();
			}
		};

		template<class BaseT, class ObjT, class RetT, class... FnTs>
		struct concrete_type<BaseT, ObjT, member_function_type<RetT>, FnTs...> : concrete_type<BaseT, ObjT, FnTs...> {
			using base_t=concrete_type<BaseT, ObjT, FnTs...>;
			using object_type=ObjT;
			using fn_type=member_function_type<RetT>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::base_t;
			using base_t::process;

			REALLY_FORCE_INLINE result_type process() const noexcept(
				noexcept(
					this->object_type::process()
				)
			) override {
				return this->object_type::process();
			}
			REALLY_FORCE_INLINE result_type process() noexcept(
				noexcept(
					this->object_type::process()
				)
			) override {
				return this->object_type::process();
			}
		};

		template<class BaseT, class ObjT, class RetT, class ArgT1, class... ArgsT>
		struct concrete_type<BaseT, ObjT, member_function_type<RetT, ArgT1, ArgsT...>> : ObjT, BaseT {
			using base_t=BaseT;
			using object_type=ObjT;
			using fn_type=member_function_type<RetT, ArgT1, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::process;

			template<class... ArgsT1>
			explicit concrete_type(ArgsT1 &&...args) noexcept(
				noexcept(object_type(std::forward<ArgsT1>(args)...))
				&& noexcept(std::declval<base_t>())
			)
			: object_type(std::forward<ArgsT1>(args)...), base_t() {
			}

			REALLY_FORCE_INLINE result_type process(ArgT1 &&arg1, ArgsT &&...args) const noexcept(
				noexcept(
					std::declval<object_type>().process(arg1, args...)
				)
			) override {
				return this->object_type::process(arg1, args...);
			}
			REALLY_FORCE_INLINE result_type process(ArgT1 &&arg1, ArgsT &&...args) noexcept(
				noexcept(
					std::declval<object_type>().process(arg1, args...)
				)
			) override {
				return this->object_type::process(arg1, args...);
			}
		};

		template<class BaseT, class ObjT, class RetT, class ArgT1, class... ArgsT, class... FnTs>
		struct concrete_type<BaseT, ObjT, member_function_type<RetT, ArgT1, ArgsT...>, FnTs...> : concrete_type<BaseT, ObjT, FnTs...> {
			using base_t=concrete_type<BaseT, ObjT, FnTs...>;
			using object_type=ObjT;
			using fn_type=member_function_type<RetT, ArgT1, ArgsT...>;
			using result_type=typename fn_type::result_type;
			using args_type=typename fn_type::args_type;
			using base_t::base_t;
			using base_t::process;

			REALLY_FORCE_INLINE result_type process(ArgT1 &&arg1, ArgsT &&...args) const noexcept(
				noexcept(
					std::declval<object_type>().process(arg1, args...)
				)
			) override {
				return this->object_type::process(arg1, args...);
			}
			REALLY_FORCE_INLINE result_type process(ArgT1 &&arg1, ArgsT &&...args) noexcept(
				noexcept(
					std::declval<object_type>().process(arg1, args...)
				)
			) override {
				return this->object_type::process(arg1, args...);
			}
		};

	}

}

} } } }

#pragma GCC diagnostic pop
