/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "dynamic_cast.hpp"

#include <boost/mpl/assert.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	template<class V, class LkT> inline typename shared_ptr<V, LkT>::atomic_ptr_t
	shared_ptr<V, LkT>::release() noexcept(true) {
		// Check that another thread hasn't already deleted the controlled object.
		typename atomic_ptr_t::value_type node_to_delete=data_.get();
		data_.compare_exchange_strong(node_to_delete, typename atomic_ptr_t::value_type{});
		if (node_to_delete) {
			assert(dynamic_cast<ctr_type *>(node_to_delete));
			assert(node_to_delete->sp_count()>0);
			// This is now single-threaded, only one thread will occupy this at a time, that one thread will achieve a zero reference count.
			if (node_to_delete->sp_release()==1) [[likely]] {
				assert(node_to_delete->sp_count()<=0);
				// Only one thread will call the deleter, because the sp_release() operation was atomic.
				assert(dynamic_cast<ctr_type *>(node_to_delete));
				node_to_delete->deleter();
			}
		}
		return data_;
	}

	template<class V, class LkT> inline void
	shared_ptr<V, LkT>::reset() noexcept(true) {
		shared_ptr().swap(*this);
	}

	template<class V, class LkT> inline
	shared_ptr<V, LkT>::shared_ptr(value_type *ptr) noexcept(true)
	: data_(ptr) {
		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename value_type::deleter_t>));
		if (data_) [[likely]] {
			assert(dynamic_cast<ctr_type *>(ptr));
			data_->sp_acquire();
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT>
	template<class V1>
	inline
	shared_ptr<V, LkT>::shared_ptr(V1 *ptr) noexcept(true)
	: data_() {
		typedef typename std::remove_const<V1>::type v1_t;

		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, v1_t>));
// This is too stringent a test, as it ignores common bases:		BOOST_MPL_ASSERT((std::is_base_of<value_type, v1_t>));
// This is too stringent a test, as it ignores virtual dtors:		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename V1::deleter_t>));
		if (ptr) [[likely]] {
			assert(dynamic_cast<v1_t *>(ptr));
			assert(dynamic_cast<ctr_type const *>(ptr));
			ptr->sp_acquire();
			data_=static_cast<value_type *>(const_cast<v1_t *>(ptr));
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT> inline
	shared_ptr<V, LkT>::shared_ptr(atomic_ptr_t const &ptr) noexcept(true)
	: data_(ptr) {
		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename value_type::deleter_t>));
		if (data_.get()) [[likely]] {
			assert(dynamic_cast<ctr_type *>(data_.get()));
			data_->sp_acquire();
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT>
	template<class V1>
	inline
	shared_ptr<V, LkT>::shared_ptr(sp_counter_itf_type<V1> const &ptr) noexcept(true)
	: data_(ptr) {
		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename value_type::deleter_t>));
		if (data_.get()) [[likely]] {
			assert(dynamic_cast<ctr_type *>(data_.get()));
			data_->sp_acquire();
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT>
	template<class V1, template<class> class At>
	inline
	shared_ptr<V, LkT>::shared_ptr(At<V1*> const &ptr) noexcept(true)
	: data_(ptr) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V1>));
		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, V1>));
// This is too stringent a test, as it ignores virtual dtors:		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename V1::deleter_t>));
		if (data_.get()) [[likely]] {
			assert(dynamic_cast<V1 *>(data_.get()));
			assert(dynamic_cast<ctr_type *>(data_.get()));
			data_->sp_acquire();
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT> inline
	shared_ptr<V, LkT>::shared_ptr(std::unique_ptr<value_type, deleter_t> &d) noexcept(true)
	: data_() {
		if (d.get()) [[likely]] {
			assert(dynamic_cast<ctr_type *>(d.get()));
			d->sp_acquire();
			data_=d.release();
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT>
	template<class V1>
	inline
	shared_ptr<V, LkT>::shared_ptr(std::unique_ptr<V1, typename V1::deleter_t> &d) noexcept(true)
	: data_() {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V1>));
		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, V1>));
		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename V1::deleter_t>));
		if (d.get()) [[likely]] {
			assert(dynamic_cast<ctr_type *>(d.get()));
			d->sp_acquire();
			data_=static_cast<value_type *>(d.release());
			assert(dynamic_cast<value_type *>(data_));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT> inline
	shared_ptr<V, LkT>::shared_ptr(std::unique_ptr<value_type, deleter_t> &&d) noexcept(true)
	: data_(d.release()) {
		if (data_.get()) [[likely]] {
			assert(dynamic_cast<ctr_type *>(data_.get()));
			data_->sp_acquire();
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT>
	template<class V1>
	inline
	shared_ptr<V, LkT>::shared_ptr(std::unique_ptr<V1, typename V1::deleter_t> &&d) noexcept(true)
	: data_(d.release()) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V1>));
		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, V1>));
		// I want to test that if the dtor is dynamic alloc, then the V1::element_type is derived from value_type, otherwise if placement, the dtor must be the same, otherwise if noop_dtor, don't care.
// This is too stringent a test, as it ignores virtual dtors:		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename V1::deleter_t>));
//		static_assert(std::is_base_of<deleter_t, typename V1::deleter_t>::value || std::is_base_of<typename V1::deleter_t, deleter_t>::value, "TODO");
		if (data_.get()) [[likely]] {
			assert(dynamic_cast<ctr_type *>(d.get()));
			data_->sp_acquire();
			assert(dynamic_cast<value_type *>(data_));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT> inline
	shared_ptr<V, LkT>::shared_ptr(const shared_ptr &s) noexcept(true)
	: data_() {
		// Also check greater than one, because a dtor could delete the object whilst we're doing the "*s.data" operation.
		atomic_ptr_t rhs_data_ptr=s.data_;
		const bool ret=(rhs_data_ptr.get() && rhs_data_ptr->sp_acquire()>1);
		if (ret) [[likely]] {
			assert(dynamic_cast<ctr_type *>(rhs_data_ptr.get()));
			data_=rhs_data_ptr;
			assert(data_==rhs_data_ptr);
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_->sp_count()>0);
		}
	}

	template<class V, class LkT> inline
	shared_ptr<V, LkT>::shared_ptr(shared_ptr &&s) noexcept(true)
	: data_() {
		data_.swap(s.data_);
		assert(data_.get() ? data_->sp_count()>0 : true);
	}

	template<class V, class LkT>
	template<typename V2, class LkT2> inline
	shared_ptr<V, LkT>::shared_ptr(const shared_ptr<V2, LkT2> &s) noexcept(true)
	: data_() {
		typedef typename shared_ptr<V2, LkT2>::ctr_type ctr2_type;
// This is too stringent a test, as it ignores common bases		BOOST_MPL_ASSERT((std::is_base_of<value_type, V2>));
		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, V2>));
		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, ctr2_type>));
// This is too stringent a test, as it ignores virtual dtors:		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename V2::deleter_t>));
		// Also check greater than one, because a dtor could execute in another thread on the same object, whilst we're doing the "*s.data" operation.
		atomic_ptr_t rhs_data_ptr=s.data_;
		const bool ret=(rhs_data_ptr.get() && rhs_data_ptr->sp_acquire()>1);
		if (ret) [[likely]] {
			assert(dynamic_cast<ctr2_type *>(rhs_data_ptr.get()));
			data_=rhs_data_ptr;
// TODO clang++ issue - comment out:			assert(data_==rhs_data_ptr);
			assert(dynamic_cast<value_type *>(data_.get()));
			assert(data_.get() ? data_->sp_count()>0 : true);
		}
	}

	template<class V, class LkT>
	template<typename V2, class LkT2> inline
	shared_ptr<V, LkT>::shared_ptr(shared_ptr<V2, LkT2> &&s) noexcept(true)
	: data_() {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V2>));
		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, V2>));
		BOOST_MPL_ASSERT((std::is_same<base_ctr_type, typename V2::base_ctr_type>));
		data_.swap(s.data_);
		assert(data_.get() ? data_->sp_count()>0 : true);
	}

	template<class V, class LkT> inline
	shared_ptr<V, LkT>::~shared_ptr() noexcept(true) {
		release();
	}

	template<class V, class LkT> inline void
	shared_ptr<V, LkT>::swap(shared_ptr &s) noexcept(true) {
		data_.swap(s.data_);
		assert(data_.get() ? data_->sp_count()>0 : true);
	}

	template<class V, class LkT>
	template<typename V2, class LkT2> inline void
	shared_ptr<V, LkT>::operator=(const shared_ptr<V2, LkT2> &s) noexcept(true) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V2>));
		BOOST_MPL_ASSERT((std::is_base_of<base_ctr_type, V2>));
		BOOST_MPL_ASSERT((std::is_same<base_ctr_type, typename V2::base_ctr_type>));
		shared_ptr tmp(s);
		tmp.swap(*this);
		assert(data_.get() ? data_->sp_count()>0 : true);
	}

	template<class V, class LkT>
	template<typename V2, class LkT2> inline void
	shared_ptr<V, LkT>::operator=(shared_ptr<V2, LkT2> &&s) noexcept(true) {
		data_.swap(s.data_);
		assert(data_.get() ? data_->sp_count()>0 : true);
	}

	template<class V, class LkT> inline void
	shared_ptr<V, LkT>::operator=(const shared_ptr<V, LkT> &s) noexcept(true) {
		shared_ptr tmp(s);
		tmp.swap(*this);
		assert(data_.get() ? data_->sp_count()>0 : true);
	}

	template<class V, class LkT> inline void
	shared_ptr<V, LkT>::operator=(shared_ptr<V, LkT> &&s) noexcept(true) {
		data_.swap(s.data_);
		assert(data_.get() ? data_->sp_count()>0 : true);
	}

	template<class V, class LkT> constexpr inline bool
	shared_ptr<V, LkT>::operator<(const shared_ptr &s) const noexcept(true) {
		return (data_.get() && s.data_.get()) ? (*data_.get()<*s.data_.get()) : false;
	}
	template<class V, class LkT> constexpr inline bool
	shared_ptr<V, LkT>::operator==(const shared_ptr &s) const noexcept(true) {
		return !((data_<s.data_) || (s.data_<data_));
	}
	template<class V, class LkT> inline constexpr bool
	shared_ptr<V, LkT>::operator!=(const shared_ptr &s) const noexcept(true) {
		return !(*this==s);
	}
	template<class V, class LkT> constexpr inline
	shared_ptr<V, LkT>::operator bool() const noexcept(true) {
		return dynamic_cast<value_type const *>(data_.get())!=nullptr;
	}
	template<class V, class LkT> constexpr inline bool
	shared_ptr<V, LkT>::operator>(const shared_ptr &s) const noexcept(true) {
		return (data_!=s.data_) && (s.data_<data_);
	}

	template<class V, class LkT> constexpr inline typename shared_ptr<V, LkT>::atomic_ptr_t const &
	shared_ptr<V, LkT>::get() const noexcept(true) {
		return data_;
	}
	template<class V, class LkT> inline typename shared_ptr<V, LkT>::atomic_ptr_t &
	shared_ptr<V, LkT>::get() noexcept(true) {
		return data_;
	}
	template<class V, class LkT> constexpr inline typename shared_ptr<V, LkT>::value_type const &
	shared_ptr<V, LkT>::operator*() const noexcept(true) {
		assert(dynamic_cast<value_type const *>(data_.get()));
		return *data_.get();
	}
	template<class V, class LkT> inline typename shared_ptr<V, LkT>::value_type &
	shared_ptr<V, LkT>::operator*() noexcept(true) {
		assert(dynamic_cast<value_type *>(data_.get()));
		return *data_.get();
	}
	template<class V, class LkT> constexpr inline typename shared_ptr<V, LkT>::value_type const *
	shared_ptr<V, LkT>::operator->() const noexcept(true) {
		return data_.get();
	}
	template<class V, class LkT> inline typename shared_ptr<V, LkT>::value_type *
	shared_ptr<V, LkT>::operator->() noexcept(true) {
		return data_.get();
	}

	template<class V, class LkT> inline tstring
	shared_ptr<V, LkT>::to_string() const noexcept(false) {
		tostringstream os;
		atomic_ptr_t data_ptr=data_;
		os<<_T("controlled-pointer=")<<data_ptr.get();
		if (dynamic_cast<value_type *>(data_ptr.get())) [[likely]] {
			os<<_T(", data: ")<<data_ptr->to_string()
				<<_T(", ")<<data_ptr.get()->sp_to_string();
		}
		os<<_T(", type: ")<<boost::core::demangle(typeid(shared_ptr).name());
		return os.str();
	}

} }
