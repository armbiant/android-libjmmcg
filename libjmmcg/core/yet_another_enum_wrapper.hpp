#ifndef LIBJMMCG_CORE_YET_ANOTHER_ENUM_WRAPPER_HPP
#define LIBJMMCG_CORE_YET_ANOTHER_ENUM_WRAPPER_HPP

/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <boost/mpl/assert.hpp>
#include <boost/preprocessor/punctuation/comma.hpp>
#include <boost/preprocessor/seq.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/preprocessor/tuple.hpp>
#include <boost/preprocessor/variadic/size.hpp>

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <optional>
#include <string_view>
#include <type_traits>

#define LIBJMMCG_EMIT_ENUM_TAG_ENTRY(R, Data, TagName) BOOST_PP_STRINGIZE(TagName) Data()

#define LIBJMMCG_EMIT_TAG_STRING(EnumName, ...) \
	BOOST_PP_SEQ_FOR_EACH(BOOST_PP_STRINGIZE(EnumName) "::" LIBJMMCG_EMIT_ENUM_TAG_ENTRY, BOOST_PP_COMMA, BOOST_PP_TUPLE_TO_SEQ((__VA_ARGS__)))

#define LIBJMMCG_EMIT_ENUM_TAG_ENTRY_STRING(R, Data, TagName) BOOST_PP_STRINGIZE(TagName) ","

#define LIBJMMCG_EMIT_TAG_STRING_NAMES(...) \
	BOOST_PP_SEQ_FOR_EACH(LIBJMMCG_EMIT_ENUM_TAG_ENTRY_STRING, BOOST_PP_COMMA, BOOST_PP_TUPLE_TO_SEQ((__VA_ARGS__)))

#define LIBJMMCG_MAKE_ENUM(EnumName, UnderlyingType, FirstTagName, ...)                                                \
	struct EnumName {                                                                                                   \
		using underlying_type_t= UnderlyingType;                                                                         \
		static_assert(std::is_integral_v<underlying_type_t>, "The underlying type of an enum must be integral.");        \
		enum enum_t_##EnumName##_ : underlying_type_t{                                                                   \
			FirstTagName                                                                                                  \
				__VA_OPT__(, ) __VA_ARGS__};                                                                               \
		using element_type= enum_t_##EnumName##_;                                                                        \
		enum : std::size_t {                                                                                             \
			size_= 1U + BOOST_PP_VARIADIC_SIZE(__VA_ARGS__)                                                               \
		};                                                                                                               \
		[[nodiscard]] static constexpr std::size_t                                                                       \
		size() noexcept(true) {                                                                                          \
			return size_;                                                                                                 \
		}                                                                                                                \
		[[nodiscard]] static constexpr std::optional<element_type>                                                       \
		begin() noexcept(true) {                                                                                         \
			return std::make_optional<element_type>(FirstTagName);                                                        \
		}                                                                                                                \
		[[nodiscard]] static constexpr std::optional<element_type>                                                       \
		end() noexcept(true) {                                                                                           \
			return std::nullopt;                                                                                          \
		}                                                                                                                \
		[[nodiscard]] static constexpr std::optional<element_type>                                                       \
		next(element_type const e) noexcept(true) {                                                                      \
			return static_cast<std::size_t>(e) < (size_ - 1U) ? std::make_optional<element_type>(                         \
						 static_cast<element_type>(static_cast<underlying_type_t>(e) + 1))                                   \
																			  : std::nullopt;                                             \
		}                                                                                                                \
		[[nodiscard]] static constexpr bool                                                                              \
		contains(element_type const key) noexcept(true) {                                                                \
			auto iter= begin();                                                                                           \
			while(iter != end()) {                                                                                        \
				if(*iter == key) {                                                                                         \
					return true;                                                                                            \
				} else {                                                                                                   \
					iter= next(*iter);                                                                                      \
				}                                                                                                          \
			}                                                                                                             \
			return false;                                                                                                 \
		}                                                                                                                \
		[[nodiscard]] static constexpr bool                                                                              \
		contains(std::string_view const& key) noexcept(true) {                                                           \
			auto iter= begin();                                                                                           \
			while(iter != end()) {                                                                                        \
				if(to_string(*iter).contains(key)) {                                                                       \
					return true;                                                                                            \
				} else {                                                                                                   \
					iter= next(*iter);                                                                                      \
				}                                                                                                          \
			}                                                                                                             \
			return false;                                                                                                 \
		}                                                                                                                \
		[[nodiscard]] static constexpr std::string_view                                                                  \
		to_string(element_type const e) noexcept(true) {                                                                 \
			static constexpr std::array<std::string_view, size_> strings{                                                 \
				LIBJMMCG_EMIT_TAG_STRING(EnumName, FirstTagName __VA_OPT__(, ) __VA_ARGS__)};                              \
			return strings[static_cast<underlying_type_t>(e)];                                                            \
		}                                                                                                                \
		[[nodiscard]] static constexpr std::string_view                                                                  \
		enum_name() noexcept(true) {                                                                                     \
			return std::string_view{BOOST_PP_STRINGIZE(EnumName)};                                                        \
		}                                                                                                                \
		[[nodiscard]] static constexpr std::string_view                                                                  \
		type_info_name() noexcept(true) {                                                                                \
			return std::string_view{                                                                                      \
				BOOST_PP_STRINGIZE(EnumName) " : " BOOST_PP_STRINGIZE(UnderlyingType) " {" LIBJMMCG_EMIT_TAG_STRING_NAMES( \
					FirstTagName __VA_OPT__(, ) __VA_ARGS__) "}"};                                                          \
		}                                                                                                                \
	}

#define LIBJMMCG_EMIT_TAG_VALUE(R, Data, TagNameValue)                     \
	static_cast<underlying_type_t>(BOOST_PP_TUPLE_ELEM(2, 1, TagNameValue)) \
		Data()

#define LIBJMMCG_EMIT_TAG_VALUES(...) \
	BOOST_PP_SEQ_FOR_EACH(LIBJMMCG_EMIT_TAG_VALUE, BOOST_PP_COMMA, BOOST_PP_TUPLE_TO_SEQ((__VA_ARGS__)))

#define LIBJMMCG_EMIT_TAG_VALUE_ENTRY(TagName, TagValue) \
	TagName= static_cast<underlying_type_t>(TagValue)

#define LIBJMMCG_EMIT_ENUM_TAG_VALUE(R, Data, TagNameValue)                                                        \
	LIBJMMCG_EMIT_TAG_VALUE_ENTRY(BOOST_PP_TUPLE_ELEM(2, 0, TagNameValue), BOOST_PP_TUPLE_ELEM(2, 1, TagNameValue)) \
	Data()

#define LIBJMMCG_EMIT_ENUM_TAG_VALUES(...) \
	BOOST_PP_SEQ_FOR_EACH(LIBJMMCG_EMIT_ENUM_TAG_VALUE, BOOST_PP_COMMA, BOOST_PP_TUPLE_TO_SEQ((__VA_ARGS__)))

#define LIBJMMCG_EMIT_TAG_VALUE_STRING_ENTRY(EnumName, TagName, TagValue) \
	BOOST_PP_STRINGIZE(TagName) "=" BOOST_PP_STRINGIZE(TagValue) ","

#define LIBJMMCG_EMIT_TAG_VALUE_STRING(R, EnumName, TagNameValue) \
	LIBJMMCG_EMIT_TAG_VALUE_STRING_ENTRY(EnumName, BOOST_PP_TUPLE_ELEM(2, 0, TagNameValue), BOOST_PP_TUPLE_ELEM(2, 1, TagNameValue))

#define LIBJMMCG_EMIT_TAG_STRING_VALUES_NAMES(EnumName, UnderlyingType, ...) \
	BOOST_PP_SEQ_FOR_EACH(LIBJMMCG_EMIT_TAG_VALUE_STRING, EnumName, BOOST_PP_TUPLE_TO_SEQ((__VA_ARGS__)))

#define LIBJMMCG_EMIT_TAG_STRING_VALUE_ENTRY(EnumName, TagName, TagValue) \
	std::pair<decltype(TagValue), std::string_view>(TagValue, BOOST_PP_STRINGIZE(EnumName) "::" BOOST_PP_STRINGIZE(TagName) "=" BOOST_PP_STRINGIZE(TagValue)),

#define LIBJMMCG_EMIT_ENUM_TAG_VALUE_STRING(R, EnumName, TagNameValue) \
	LIBJMMCG_EMIT_TAG_STRING_VALUE_ENTRY(EnumName, BOOST_PP_TUPLE_ELEM(2, 0, TagNameValue), BOOST_PP_TUPLE_ELEM(2, 1, TagNameValue))

#define LIBJMMCG_EMIT_ENUM_TAG_STRING_VALUES_NAMES(EnumName, UnderlyingType, ...) \
	BOOST_PP_SEQ_FOR_EACH(LIBJMMCG_EMIT_ENUM_TAG_VALUE_STRING, EnumName, BOOST_PP_TUPLE_TO_SEQ((__VA_ARGS__)))

#define LIBJMMCG_MAKE_ENUM_TAG_VALUES(EnumName, UnderlyingType, FirstTagNameValue, ...)                                          \
	struct EnumName {                                                                                                             \
		using underlying_type_t= UnderlyingType;                                                                                   \
		static_assert(std::is_integral_v<underlying_type_t>, "The underlying type of an enum must be integral.");                  \
		enum enum_t_##EnumName##_ : underlying_type_t{                                                                             \
			LIBJMMCG_EMIT_ENUM_TAG_VALUES(FirstTagNameValue __VA_OPT__(, ) __VA_ARGS__)};                                           \
		using element_type= enum_t_##EnumName##_;                                                                                  \
		enum : std::size_t {                                                                                                       \
			size_= 1U + BOOST_PP_VARIADIC_SIZE(__VA_ARGS__)                                                                         \
		};                                                                                                                         \
		[[nodiscard]] static constexpr std::size_t                                                                                 \
		size() noexcept(true) {                                                                                                    \
			return size_;                                                                                                           \
		}                                                                                                                          \
		[[nodiscard]] static constexpr std::optional<element_type>                                                                 \
		begin() noexcept(true) {                                                                                                   \
			return std::make_optional<element_type>(                                                                                \
				static_cast<element_type>(std::min({LIBJMMCG_EMIT_TAG_VALUES(FirstTagNameValue __VA_OPT__(, ) __VA_ARGS__)})));      \
		}                                                                                                                          \
		[[nodiscard]] static constexpr std::optional<element_type>                                                                 \
		end() noexcept(true) {                                                                                                     \
			return std::nullopt;                                                                                                    \
		}                                                                                                                          \
		[[nodiscard]] static constexpr std::optional<element_type>                                                                 \
		next(const element_type e) noexcept(true) {                                                                                \
			static constexpr std::array<underlying_type_t, size_> values{[]() constexpr {                                           \
				std::array<underlying_type_t, size_> values{                                                                         \
					LIBJMMCG_EMIT_TAG_VALUES(FirstTagNameValue __VA_OPT__(, ) __VA_ARGS__)};                                          \
				std::sort(values.begin(), values.end());                                                                             \
				return values;                                                                                                       \
			}()};                                                                                                                   \
			auto const found= std::lower_bound(values.begin(), values.end(), static_cast<underlying_type_t>(e));                    \
			if(found != values.end()) {                                                                                             \
				auto const index= std::distance(values.begin(), found);                                                              \
				assert(index >= 0);                                                                                                  \
				auto const uindex= static_cast<std::size_t>(index);                                                                  \
				if((uindex + 1U) < size_) {                                                                                          \
					return std::make_optional<element_type>(                                                                          \
						static_cast<element_type>(values[uindex + 1U]));                                                               \
				} else {                                                                                                             \
					return std::nullopt;                                                                                              \
				}                                                                                                                    \
			} else {                                                                                                                \
				return std::nullopt;                                                                                                 \
			}                                                                                                                       \
		}                                                                                                                          \
		[[nodiscard]] static constexpr bool                                                                                        \
		contains(element_type const key) noexcept(true) {                                                                          \
			auto iter= begin();                                                                                                     \
			while(iter != end()) {                                                                                                  \
				if(*iter == key) {                                                                                                   \
					return true;                                                                                                      \
				} else {                                                                                                             \
					iter= next(*iter);                                                                                                \
				}                                                                                                                    \
			}                                                                                                                       \
			return false;                                                                                                           \
		}                                                                                                                          \
		[[nodiscard]] static constexpr bool                                                                                        \
		contains(std::string_view const& key) noexcept(true) {                                                                     \
			auto iter= begin();                                                                                                     \
			while(iter != end()) {                                                                                                  \
				if(to_string(*iter).contains(key)) {                                                                                 \
					return true;                                                                                                      \
				} else {                                                                                                             \
					iter= next(*iter);                                                                                                \
				}                                                                                                                    \
			}                                                                                                                       \
			return false;                                                                                                           \
		}                                                                                                                          \
		[[nodiscard]] static constexpr std::string_view                                                                            \
		to_string(element_type const e) noexcept(true) {                                                                           \
			static constexpr std::array<std::pair<underlying_type_t, std::string_view>, size_> strings{                             \
				LIBJMMCG_EMIT_ENUM_TAG_STRING_VALUES_NAMES(EnumName, UnderlyingType, FirstTagNameValue __VA_OPT__(, ) __VA_ARGS__)}; \
			auto const found= std::find_if(                                                                                         \
				strings.begin(),                                                                                                     \
				strings.end(),                                                                                                       \
				[e](auto const& v) constexpr {                                                                                       \
					return v.first == static_cast<underlying_type_t>(e);                                                              \
				});                                                                                                                  \
			return found->second;                                                                                                   \
		}                                                                                                                          \
		[[nodiscard]] static constexpr std::string_view                                                                            \
		enum_name() noexcept(true) {                                                                                               \
			return std::string_view{BOOST_PP_STRINGIZE(EnumName)};                                                                  \
		}                                                                                                                          \
		[[nodiscard]] static constexpr std::string_view                                                                            \
		type_info_name() noexcept(true) {                                                                                          \
			return std::string_view{                                                                                                \
				BOOST_PP_STRINGIZE(EnumName) " : " BOOST_PP_STRINGIZE(UnderlyingType) " {" LIBJMMCG_EMIT_TAG_STRING_VALUES_NAMES(    \
					EnumName, UnderlyingType, FirstTagNameValue __VA_OPT__(, ) __VA_ARGS__) "}"};                                     \
		}                                                                                                                          \
	}

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace tests {

namespace one_tag {
namespace defaulted {
LIBJMMCG_MAKE_ENUM(foo, int, bar);
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : int {bar,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 1);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 0);
	static_assert(foo::next(foo::bar) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::bar).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::bar, ==, 0);
	BOOST_MPL_ASSERT_RELATION(foo::enum_t_foo_::bar, ==, 0);
	static_assert(foo::to_string(foo::bar) == "foo::bar", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::bar), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo::bar"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(42)), ==, false);
};
}
namespace custom {
LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, int, (foo0, 4));
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : int {foo0=4,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 1);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 4);
	static_assert(foo::next(foo::foo0) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::foo0).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::foo0, ==, 4);
	static_assert(foo::to_string(foo::foo0) == "foo::foo0=4", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::foo0), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo::foo0=4"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(42)), ==, false);
};
}
}

namespace two_tags {
namespace defaulted {
LIBJMMCG_MAKE_ENUM(foo, int, bar, baz);
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : int {bar,baz,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 2);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 0);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::bar), ==, foo::baz);
	static_assert(foo::next(foo::baz) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::baz).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::bar, ==, 0);
	BOOST_MPL_ASSERT_RELATION(foo::baz, ==, 1);
	static_assert(foo::to_string(foo::bar) == "foo::bar", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::baz) == "foo::baz", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::bar), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo::bar"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(42)), ==, false);
};
}
namespace custom {
LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, int, (foo0, 4), (foo1, 5));
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : int {foo0=4,foo1=5,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 2);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 4);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo0), ==, foo::foo1);
	static_assert(foo::next(foo::foo1) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::foo1).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::foo0, ==, 4);
	BOOST_MPL_ASSERT_RELATION(foo::foo1, ==, 5);
	static_assert(foo::to_string(foo::foo0) == "foo::foo0=4", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo1) == "foo::foo1=5", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::foo0), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo::foo0=4"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(42)), ==, false);
};
}
}

namespace three_tags {
namespace defaulted {
LIBJMMCG_MAKE_ENUM(foo, int, bar, baz, fubar);
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : int {bar,baz,fubar,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 3);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 0);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::bar), ==, foo::baz);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::baz), ==, foo::fubar);
	static_assert(foo::next(foo::fubar) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::fubar).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::bar, ==, 0);
	BOOST_MPL_ASSERT_RELATION(foo::baz, ==, 1);
	BOOST_MPL_ASSERT_RELATION(foo::fubar, ==, 2);
	static_assert(foo::to_string(foo::bar) == "foo::bar", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::baz) == "foo::baz", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::fubar) == "foo::fubar", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::bar), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo::bar"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(42)), ==, false);
};
}
namespace custom {
LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, int, (foo0, 4), (foo1, 5), (foo2, 42));
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, int>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : int {foo0=4,foo1=5,foo2=42,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 3);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 4);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo0), ==, foo::foo1);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo1), ==, foo::foo2);
	static_assert(foo::next(foo::foo2) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::foo2).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::foo0, ==, 4);
	BOOST_MPL_ASSERT_RELATION(foo::foo1, ==, 5);
	BOOST_MPL_ASSERT_RELATION(foo::foo2, ==, 42);
	static_assert(foo::to_string(foo::foo0) == "foo::foo0=4", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo1) == "foo::foo1=5", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo2) == "foo::foo2=42", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::foo0), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo::foo0=4"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(1066)), ==, false);
};
}
}

namespace four_tags {
namespace defaulted {
LIBJMMCG_MAKE_ENUM(foo, long, foo0, foo1, foo2, foo3);
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, long>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : long {foo0,foo1,foo2,foo3,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 4);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 0);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo0), ==, foo::foo1);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo1), ==, foo::foo2);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo2), ==, foo::foo3);
	static_assert(foo::next(foo::foo3) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::foo3).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::foo0, ==, 0);
	BOOST_MPL_ASSERT_RELATION(foo::foo1, ==, 1);
	BOOST_MPL_ASSERT_RELATION(foo::foo2, ==, 2);
	BOOST_MPL_ASSERT_RELATION(foo::foo3, ==, 3);
	static_assert(foo::to_string(foo::foo0) == "foo::foo0", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo1) == "foo::foo1", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo2) == "foo::foo2", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo3) == "foo::foo3", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::foo0), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo0"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(42)), ==, false);
};
}
namespace custom {
LIBJMMCG_MAKE_ENUM_TAG_VALUES(foo, long, (foo0, 4UL), (foo1, 5UL), (foo2, 42UL), (foo3, 68UL));
struct test {
	static_assert(std::is_same_v<foo::underlying_type_t, long>, "The underlying type for the generated enum is incorrect.");
	static_assert(foo::enum_name() == "foo", "The enum-name returned must be the enum-name supplied.");
	static_assert(foo::type_info_name() == "foo : long {foo0=4UL,foo1=5UL,foo2=42UL,foo3=68UL,}", "The enum type-name returned must be the enum type-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::size(), ==, 4);
	BOOST_MPL_ASSERT_RELATION(*foo::begin(), ==, 4);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo0), ==, foo::foo1);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo1), ==, foo::foo2);
	BOOST_MPL_ASSERT_RELATION(*foo::next(foo::foo2), ==, foo::foo3);
	static_assert(foo::next(foo::foo3) == foo::end(), "Iterating off the end of the list of the tag-names returns end().");
	static_assert(!foo::next(foo::foo3).has_value(), "The next tag is not in the enum-tag list.");
	BOOST_MPL_ASSERT_RELATION(foo::foo0, ==, 4);
	BOOST_MPL_ASSERT_RELATION(foo::foo1, ==, 5);
	BOOST_MPL_ASSERT_RELATION(foo::foo2, ==, 42);
	BOOST_MPL_ASSERT_RELATION(foo::foo3, ==, 68);
	static_assert(foo::to_string(foo::foo0) == "foo::foo0=4UL", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo1) == "foo::foo1=5UL", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo2) == "foo::foo2=42UL", "The tag-name returned must be the tag-name supplied.");
	static_assert(foo::to_string(foo::foo3) == "foo::foo3=68UL", "The tag-name returned must be the tag-name supplied.");
	BOOST_MPL_ASSERT_RELATION(foo::contains(foo::foo0), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains("foo0"), ==, true);
	BOOST_MPL_ASSERT_RELATION(foo::contains(static_cast<foo::element_type>(1066)), ==, false);
};
}
}

}}}

#endif
