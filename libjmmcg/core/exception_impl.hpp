/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_api_traits.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class Exception, ppd::generic_traits::api_type::element_type API, typename Mdl>
NEVER_INLINE auto
throw_exception(std::string&& s, boost::stacktrace::stacktrace&& stacktrace) {
	using thread_traits= ppd::api_threading_traits<API, Mdl>;
	using errinfo_thread_id= boost::error_info<private_::errinfo_thread_id_, typename thread_traits::api_params_type::tid_type>;
	using errinfo_process_id= boost::error_info<private_::errinfo_process_id_, typename thread_traits::api_params_type::pid_type>;

	auto&& thread_name= thread_traits::get_name(thread_traits::get_current_thread());
	auto&& user_name= thread_traits::get_current_username();

	return throw_exception<Exception>(std::move(s), std::move(stacktrace)) << errinfo_thread_id(thread_traits::get_current_thread_id()) << errinfo_process_id(thread_traits::get_current_process_id()) << errinfo_thread_name(std::string(thread_name.begin(), std::find(thread_name.begin(), thread_name.end(), '\0'))) << errinfo_user_name(std::string(user_name.begin(), std::find(user_name.begin(), user_name.end(), '\0')));
}
template<class Exception, ppd::generic_traits::api_type::element_type API, typename Mdl, class Data>
NEVER_INLINE auto
throw_exception(std::string&& s, Data, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception, API, Mdl>(std::move(s), std::move(stacktrace)) << boost::errinfo_type_info_name(boost::core::demangle(typeid(Data).name()));
}

template<class Exception>
NEVER_INLINE auto
throw_api_exception(std::string&& s, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception, ppd::platform_api, ppd::heavyweight_threading>(std::move(s), std::move(stacktrace));
}
template<class Exception, class Data>
NEVER_INLINE auto
throw_api_exception(std::string&& s, Data d, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception, ppd::platform_api, ppd::heavyweight_threading>(std::move(s), d, std::move(stacktrace));
}

template<class Exception, ppd::generic_traits::api_type::element_type API, typename Mdl>
NEVER_INLINE auto
throw_crt_exception(std::string&& s, char const crt_fn_name[], int err, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception, API, Mdl>(std::move(s), std::move(stacktrace)) << boost::errinfo_api_function(crt_fn_name) << boost::errinfo_errno(err);
}
template<class Exception, ppd::generic_traits::api_type::element_type API, typename Mdl, class Data>
NEVER_INLINE auto
throw_crt_exception(std::string&& s, char const crt_fn_name[], Data d, int err, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_exception<Exception, API, Mdl, Data>(std::move(s), d, std::move(stacktrace)) << boost::errinfo_api_function(crt_fn_name) << boost::errinfo_errno(err);
}

template<class Exception>
NEVER_INLINE auto
throw_api_crt_exception(std::string&& s, char const crt_fn_name[], int err, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_api_exception<Exception>(std::move(s), std::move(stacktrace)) << boost::errinfo_api_function(crt_fn_name) << boost::errinfo_errno(err);
}
template<class Exception, class Data>
NEVER_INLINE auto
throw_api_crt_exception(std::string&& s, char const crt_fn_name[], Data d, int err, boost::stacktrace::stacktrace&& stacktrace) {
	return throw_api_exception<Exception, Data>(std::move(s), d, std::move(stacktrace)) << boost::errinfo_api_function(crt_fn_name) << boost::errinfo_errno(err);
}

}}
