/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#pragma once

#include "core_config.h"

#include <algorithm>
#include <functional>
#include <vector>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace rapid_insert_lookup {

	/**
		This collection implementation is efficient when the `inserts()` to the collection come
		in blocks, then the lookups (`find()` or `operator[]()`) in blocks, then the `inserts(...)` again.
		This is because the collection only sorts itself when it is unsorted and a lookup occurs.
		Non-unique keys are allowed, and the groups of non-unique keys are adjacent (after sorting) then the range of the "unique" key is multiple values.

		The complexity is (assuming default container of `std::vector`, which affects these complexities):
		-# Copy: O(n), worst-case.

		More notes:
		===========
		-# After sorting, non-unique keys are guaranteed to be adjacent in the multimap.
		-# The base container can be pretty much any STL container. The obvious candidates are `std::vector` or `std:list`. The choice will affect the complexities above. The advantage of `std::vector` over `std::list` is locality of reference: The data elements are guaranteed to be local to each other, unlike `std::list`. Alternatively `std:list` has better complexity than `std::vector` for some of the operations. Choose wisely.
	*/
	template<typename _Key,typename _Val,typename _Pred=std::less<_Key>,typename _Cont=std::vector<std::pair<_Key,_Val> > >
	class multimap {
	public:
		typedef _Cont base_container;
		typedef typename base_container::allocator_type allocator_type;
		typedef _Key key_type;
		typedef _Val mapped_type;
		typedef _Pred key_compare;
		typedef std::pair<const key_type,mapped_type> value_type;
		typedef typename base_container::difference_type difference_type;
		typedef typename base_container::reference reference;
		typedef typename base_container::const_reference const_reference;
		typedef typename base_container::size_type size_type;
		typedef typename base_container::const_iterator const_iterator;
		typedef typename base_container::iterator iterator;
		typedef typename base_container::reverse_iterator reverse_iterator;
		typedef typename base_container::const_reverse_iterator const_reverse_iterator;
		typedef std::pair<iterator,bool> InsertItr_;

		explicit __stdcall multimap(const _Pred &pr=_Pred(),const allocator_type &al=allocator_type());
		__stdcall multimap(const size_type,const value_type &v=value_type(),const _Pred &pr=_Pred(),const allocator_type &al=allocator_type());
		__stdcall multimap(const multimap &);
		__stdcall multimap(const const_iterator &,const const_iterator &,const _Pred &pr=_Pred(),const allocator_type &al=allocator_type());
		__stdcall ~multimap(void);

		multimap & __fastcall operator=(const multimap &m);

		const_iterator __fastcall begin(void) const noexcept(true);
		iterator __fastcall begin(void) noexcept(true);
		const_reverse_iterator __fastcall rbegin(void) const noexcept(true);
		reverse_iterator __fastcall rbegin(void) noexcept(true);
		const_iterator __fastcall end(void) const noexcept(true);
		iterator __fastcall end(void) noexcept(true);
		const_reverse_iterator __fastcall rend(void) const noexcept(true);
		reverse_iterator __fastcall rend(void) noexcept(true);

		const size_type __fastcall size(void) const noexcept(true);
		const size_type __fastcall max_size(void) const noexcept(true);
		bool __fastcall empty(void) const noexcept(true);
		void __fastcall clear(void);
		void __fastcall resize(const size_type,const value_type &v=value_type());
		void __fastcall reserve(const size_type);

		allocator_type __fastcall get_allocator(void) const;

		/**
			Insert: (with `insert()`) constant time. (But unsorts the multimap.)
			Allows the insertion of multiple keys with the same value, but different referents.
		*/
		InsertItr_ __fastcall insert(const value_type &);
		/**
			Insert: (with `insert()`) constant time. (But unsorts the multimap.)
			Allows the insertion of multiple keys with the same value, but different referents.
		*/
		iterator __fastcall insert(const iterator &,const value_type &);
		/**
			Insert: (with `insert()`) constant time. (But unsorts the multimap.)
			Allows the insertion of multiple keys with the same value, but different referents.
		*/
		void __fastcall insert(const const_iterator &,const const_iterator &);

		/**
			Erase: O(n).
		*/
		iterator __fastcall erase(const iterator &);
		/**
			Erase: O(n).
		*/
		iterator __fastcall erase(const iterator &,const iterator &);
		/**
			Erase: O(nlog(n)).
			All keys with same value are erased. Collection remains sorted.
		*/
		const size_type __fastcall erase(const key_type &);

		/**
			Lookup: O(log(n)+(complexity of sorting a vector)), if unsorted, O(log(n)) if sorted, irrespective if the element is in the multimap or not.
			Will only insert the key if it is not in the map, i.e. it will ensure keys are unique, but will be slower.
			Are guaranteed to return the first key if they are not unique in the multimap.
		*/
		mapped_type & __fastcall operator[](const key_type &);

		/**
			Lookup: O(log(n)+(complexity of sorting a vector)), if unsorted, O(log(n)) if sorted, irrespective if the element is in the multimap or not.
			Are guaranteed to return the first key if they are not unique in the multimap.
		*/
		const_iterator __fastcall find(const key_type &) const;
		/**
			Lookup: O(log(n)+(complexity of sorting a vector)), if unsorted, O(log(n)) if sorted, irrespective if the element is in the multimap or not.
			Are guaranteed to return the first key if they are not unique in the multimap.
		*/
		iterator __fastcall find(const key_type &);

	private:
		class value_compare {
		public:
			using result_type=bool;
			using first_argument_type=value_type;
			using second_argument_type=value_type;

			constexpr value_compare(void) noexcept(true)
			: comp() {
			}

			bool __fastcall operator()(const value_type &X,const value_type &Y) const {
				return comp(X.first, Y.first);
			}

		private:
			const _Pred comp;

			friend class multimap<_Key,_Val,_Pred,_Cont>;

			void __fastcall operator=(const value_compare &)=delete;
		};

		mutable base_container cont;
		mutable bool is_sorted;

		void __fastcall sort(void) const;
		void __fastcall sort(void);
		const_iterator __fastcall find_first_key(const const_iterator &) const;
		iterator __fastcall find_first_key(const iterator &) const;
	};

	/**
		This collection implementation is efficient when the `inserts()` to the collection come
		in blocks, then the lookups (`find()` or `operator[]()`) in blocks, then the `inserts()` again.
		This is because the collection only sorts itself when it is unsorted and a lookup occurs.
		Non-unique keys are allowed, and the groups of non-unique keys are adjacent (after sorting) then the range of the "unique" key is multiple values.

		The complexity is (assuming default container of `std::vector`, which affects these complexities):
		-# Copy: O(n), worst-case.

		More notes:
		===========
		-# After sorting, non-unique keys are guaranteed to be adjacent in the multiset.
		-# The base container can be pretty much any STL container. The obvious candidates are `std::vector` or `std:list`. The choice will affect the complexities above. The advantage of `std::vector` over `std::list` is locality of reference: The data elements are guaranteed to be local to each other, unlike `std::list`. Alternatively `std:list` has better complexity than `std::vector` for some of the operations. Choose wisely.
	*/
	template<typename _Key,typename _Pred=std::less<_Key>,typename _Cont=std::vector<_Key> >
	class multiset {
	public:
		typedef _Cont base_container;
		typedef typename base_container::allocator_type allocator_type;
		typedef _Key key_type;
		typedef _Pred key_compare;
		typedef const key_type value_type;
		typedef typename base_container::difference_type difference_type;
		typedef typename base_container::reference reference;
		typedef typename base_container::const_reference const_reference;
		typedef typename base_container::size_type size_type;
		typedef typename base_container::const_iterator const_iterator;
		typedef typename base_container::iterator iterator;
		typedef typename base_container::reverse_iterator reverse_iterator;
		typedef typename base_container::const_reverse_iterator const_reverse_iterator;
		typedef std::pair<iterator,bool> InsertItr_;

		explicit __stdcall multiset(const _Pred &pr=_Pred(),const allocator_type &al=allocator_type());
		__stdcall multiset(const size_type,const value_type &v=value_type(),const _Pred &pr=_Pred(),const allocator_type &al=allocator_type());
		__stdcall multiset(const multiset &);
		__stdcall multiset(const const_iterator &,const const_iterator &,const _Pred &pr=_Pred(),const allocator_type &al=allocator_type());
		__stdcall ~multiset(void);

		multiset & __fastcall operator=(const multiset &m);

		const_iterator __fastcall begin(void) const noexcept(true);
		iterator __fastcall begin(void) noexcept(true);
		const_reverse_iterator __fastcall rbegin(void) const noexcept(true);
		reverse_iterator __fastcall rbegin(void) noexcept(true);
		const_iterator __fastcall end(void) const noexcept(true);
		iterator __fastcall end(void) noexcept(true);
		const_reverse_iterator __fastcall rend(void) const noexcept(true);
		reverse_iterator __fastcall rend(void) noexcept(true);

		const size_type __fastcall size(void) const noexcept(true);
		const size_type __fastcall max_size(void) const noexcept(true);
		bool __fastcall empty(void) const noexcept(true);
		void __fastcall clear(void);
		void __fastcall resize(const size_type,const value_type &v=value_type());
		void __fastcall reserve(const size_type);

		allocator_type __fastcall get_allocator(void) const;

		/**
			Insert: (with `insert()`) constant time. (But unsorts the multiset.)
			Allows the insertion of multiple keys with the same value.
		*/
		InsertItr_ __fastcall insert(const value_type &);
		/**
			Insert: (with `insert()`) constant time. (But unsorts the multiset.)
			Allows the insertion of multiple keys with the same value.
		*/
		iterator __fastcall insert(const iterator &,const value_type &);
		/**
			Insert: (with `insert()`) constant time. (But unsorts the multiset.)
			Allows the insertion of multiple keys with the same value.
		*/
		void __fastcall insert(const const_iterator &,const const_iterator &);

		/**
			Erase: O(n).
		*/
		iterator __fastcall erase(const iterator &);
		/**
			Erase: O(n).
		*/
		iterator __fastcall erase(const iterator &,const iterator &);
		/**
			Erase: O(nlog(n)).
			All keys with same value are erased. Collection remains sorted.
		*/
		const size_type __fastcall erase(const key_type &);

		/**
			Lookup: O(log(n)+(complexity of sorting a vector)), if unsorted, O(log(n)) if sorted, irrespective if the element is in the multiset or not.
			Are guaranteed to return the first key if they are not unique in the multiset.
		*/
		const_iterator __fastcall find(const key_type &) const;
		/**
			Lookup: O(log(n)+(complexity of sorting a vector)), if unsorted, O(log(n)) if sorted, irrespective if the element is in the multiset or not.
			Are guaranteed to return the first key if they are not unique in the multiset.
		*/
		iterator __fastcall find(const key_type &);

	private:
		class value_compare {
		public:
			using result_type=bool;
			using first_argument_type=value_type;
			using second_argument_type=value_type;

			constexpr value_compare(void) noexcept(true)
			: comp() {
			}

			bool __fastcall operator()(const value_type &X, const value_type &Y) const {
				return comp(X, Y);
			}

		private:
			const _Pred comp;

			friend class multiset<_Key,_Pred,_Cont>;

			void __fastcall operator=(const value_compare &)=delete;
		};

		mutable base_container cont;
		mutable bool is_sorted;

		void __fastcall sort(void) const;
		void __fastcall sort(void);
		const_iterator __fastcall find_first_key(const const_iterator &) const;
		iterator __fastcall find_first_key(const iterator &) const;
	};

} } }

#include "multimap_impl.hpp"
