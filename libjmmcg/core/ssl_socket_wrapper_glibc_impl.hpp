/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg {
namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace glibc { namespace client { namespace ssl {

template<class LkT>
class wrapper<LkT>::init_SSL_t final {
public:
	init_SSL_t()
		: settings_(OPENSSL_INIT_new()) {
		JMMCG_SYSCALL_WRAPPER("Failed to initialise SSL.", ::OPENSSL_init_ssl, OPENSSL_INIT_LOAD_SSL_STRINGS, settings_);
		JMMCG_SYSCALL_WRAPPER("Failed to initialise crypto.", ::OPENSSL_init_crypto, OPENSSL_INIT_LOAD_CRYPTO_STRINGS | OPENSSL_INIT_ADD_ALL_CIPHERS | OPENSSL_INIT_ADD_ALL_DIGESTS | OPENSSL_INIT_LOAD_CONFIG, settings_);
	}
	~init_SSL_t() {
		::OPENSSL_INIT_free(settings_);
	}

private:
	OPENSSL_INIT_SETTINGS* const settings_;
};

template<class LkT>
class wrapper<LkT>::addrs_info_t final {
public:
	struct addrinfo* addrs{};

	[[nodiscard]] addrs_info_t(type_t type, domain_t domain, std::string const& host, std::string const& service) {
		struct addrinfo hints= {};
		hints.ai_family= domain;	// AF_UNSPEC;
		hints.ai_socktype= type;
		hints.ai_flags= AI_CANONNAME | AI_ALL | AI_ADDRCONFIG;
		const int status= ::getaddrinfo(host.c_str(), service.c_str(), &hints, &addrs);
		if(status != 0 && addrs != nullptr) {
			BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::invalid_argument>(fmt::format("Error creating SSL connection. host: '{}', service: '{}', ::getaddinfo return-code translation: '{}'", host, service, ::gai_strerror(status)), "getaddrinfo", this));
		}
		assert(addrs);
	}
	~addrs_info_t() {
		assert(addrs);
		::freeaddrinfo(addrs);
	}
};

template<class LkT>
class wrapper<LkT>::SSL_context_wrapper final {
public:
	SSL_CTX* const ctx_;

	explicit SSL_context_wrapper(verify_SSL_certificates verify_SSL)
		: ctx_(JMMCG_SYSCALL_WRAPPER("Allocating a new SSL context failed.", ::SSL_CTX_new, ::TLS_client_method())) {
		if(verify_SSL == verify_SSL_certificates::none) {
			::SSL_CTX_set_verify(ctx_, verify_SSL, nullptr);	// Disable SSL cert verification
		}
		// For verify_SSL_certificates::peer use the default...?
	}
	~SSL_context_wrapper() {
		::SSL_CTX_free(ctx_);
	}
};

template<class LkT>
class wrapper<LkT>::SSL_socket_wrapper final {
public:
	SSL_socket_wrapper(SSL_context_wrapper& ctx, bool verify_SSL)
		: ssl_(JMMCG_SYSCALL_WRAPPER("Allocating a new SSL state failed.", ::SSL_new, ctx.ctx_)) {
		if(verify_SSL == verify_SSL_certificates::none) {
			::SSL_set_verify(ssl_, verify_SSL, nullptr);	  // Disable SSL cert verification
		}
		// For verify_SSL_certificates::peer use the default...?
	}
	~SSL_socket_wrapper() {
		::SSL_shutdown(ssl_);
		::SSL_shutdown(ssl_);
		::SSL_clear(ssl_);
		::SSL_free(ssl_);
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"

	void connect(std::string const& hostname, socket_type socket) {
		::SSL_set_tlsext_host_name(ssl_, hostname.c_str());
		JMMCG_SYSCALL_WRAPPER("Setting the underlying file handle as the SSL socket handle failed.", ::SSL_set_fd, ssl_, socket);
		const int err= ::SSL_connect(ssl_);
		if(err == 1) {
			assert(!get_certificates_in_use().empty());
			error_buffer_.clear();
		} else {
			// Check, if the problem is just the non-blocking socket. In this case, just continue
			const int ssl_err= ::SSL_get_error(ssl_, err);
			::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
			BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::invalid_argument>(fmt::format("Error creating SSL connection. hostname: '{}', socket: '{}', SSL_get_error error={}, SSL_get_error translation='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", hostname, socket, ssl_err, to_string(ssl_err), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_connect", &wrapper<LkT>::wrapper::SSL_socket_wrapper::connect, err));
		}
	}

#pragma GCC diagnostic pop

	void close() noexcept(false) {
		JMMCG_SYSCALL_WRAPPER("Error sending the shutdown message for the SSL connection.", ::SSL_shutdown, ssl_);
		error_buffer_.clear();
	}

	template<class MsgT>
	void
	write(MsgT const& message) noexcept(false) {
		assert(message.is_valid());
		if constexpr(MsgT::has_static_size) {
			assert(message.length() == sizeof(MsgT));
			auto const& buff= reinterpret_cast<std::array<std::byte, sizeof(MsgT)> const&>(message);
			write(buff);
		} else {
			assert(!message.empty());
			std::size_t bytes_written{};
			const int err= ::SSL_write_ex(ssl_, message, message.length(), &bytes_written);
			if((err < bytes_written) || (bytes_written != message.length())) {
				const int ssl_err= ::SSL_get_error(ssl_, err);
				::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
				switch(ssl_err) {
				case SSL_ERROR_NONE:
				case SSL_ERROR_ZERO_RETURN:
				case SSL_ERROR_WANT_WRITE:	  // Just a temporary error
				case SSL_ERROR_WANT_READ:
					break;
				default:
					BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. Type of message to send: '{}', SSL_get_error error={}, SSL_get_error translation='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", boost::core::demangle(typeid(MsgT).name()), ssl_err, to_string(ssl_err), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_write_ex", &wrapper::SSL_socket_wrapper::write<MsgT>, err));
				}
			}
			assert(bytes_written > 0);
			assert(static_cast<std::size_t>(bytes_written) <= sizeof(MsgT));
			assert(static_cast<std::size_t>(bytes_written) == message.length());
		}
	}
	template<class V, std::size_t N>
	void
	write(std::array<V, N> const& message) noexcept(false) {
		std::size_t bytes_written{};
		const int err= ::SSL_write_ex(ssl_, message.data(), sizeof(V) * N, &bytes_written);
		if(err < 0 || (static_cast<std::size_t>(err) < bytes_written) || (bytes_written != sizeof(V) * N)) {
			const int ssl_err= ::SSL_get_error(ssl_, err);
			::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
			switch(ssl_err) {
			case SSL_ERROR_NONE:
			case SSL_ERROR_ZERO_RETURN:
			case SSL_ERROR_WANT_WRITE:	  // Just a temporary error
			case SSL_ERROR_WANT_READ:
				break;
			default:
				BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to write the message to the socket. Type of message to send: '{}', SSL_get_error error={}, SSL_get_error translation='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", boost::core::demangle(typeid(std::array<V, N>).name()), ssl_err, to_string(ssl_err), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_write_ex", &wrapper::SSL_socket_wrapper::write<V, N>, err));
			}
		}
		assert(bytes_written > 0);
		assert(static_cast<std::size_t>(bytes_written) == sizeof(V) * N);
	}

	[[nodiscard]] bool
	read_msg(std::byte* dest, std::size_t msg_size) noexcept(false) {
		if(msg_size > 0) {
			std::size_t bytes_read= 0;
			const int err= ::SSL_read_ex(ssl_, dest, msg_size, &bytes_read);
			if(err < 0 || (bytes_read != msg_size)) {
				const int ssl_err= ::SSL_get_error(ssl_, err);
				::ERR_print_errors_cb(store_SSL_internal_error, &error_buffer_);
				switch(ssl_err) {
				case SSL_ERROR_NONE:
				case SSL_ERROR_ZERO_RETURN:
				case SSL_ERROR_WANT_WRITE:	  // Just a temporary error
				case SSL_ERROR_WANT_READ:
					return true;
				default:
					BOOST_THROW_EXCEPTION(throw_api_crt_exception<std::runtime_error>(fmt::format("Failed to read the message from the socket. Number of bytes to read={}, SSL_get_error error={}, SSL_get_error translation='{}', SSL certificates in use: '{}', SSL cipher in use: '{}', SSL negotiation errors: '{}'", msg_size, ssl_err, to_string(ssl_err), get_certificates_in_use(), ::SSL_get_cipher(ssl_), error_buffer_), "SSL_read_ex", &wrapper::SSL_socket_wrapper::read_msg, err));
				}
			}
			return false;
		}
		return true;
	}

private:
	std::string error_buffer_;
	SSL* const ssl_;

	[[nodiscard]] static std::string to_string(unsigned long code) {
		std::string buff(1024, '\0');
		::ERR_error_string_n(code, buff.data(), buff.size());
		buff.resize(buff.find('\0'));
		return buff;
	}

	static int store_SSL_internal_error(const char* error_string, std::size_t error_string_length, void* data) noexcept(true) {
		assert(data);
		std::string& error_buffer= *reinterpret_cast<std::string*>(data);
		error_buffer.append(error_string, error_string_length);
		return 0;
	}

	[[nodiscard]] std::string get_certificates_in_use() const {
		struct X509_deletor final {
			X509* cert;

			explicit X509_deletor(SSL* ssl)
				:	 // Get the certificate of the server.
				  cert(::SSL_get_peer_certificate(ssl)) {
			}
			~X509_deletor() {
				// Free the malloc'ed certificate copy.
				::X509_free(cert);
			}
		};
		X509_deletor cert(ssl_);
		if(cert.cert != NULL) {
			std::ostringstream certificates("Server certificates:\n");
			free_ptr<char*> line;
			line= make_free_ptr(X509_NAME_oneline(X509_get_subject_name(cert.cert), 0, 0));
			certificates << "Subject: " << line.get() << "\n";
			line= make_free_ptr(X509_NAME_oneline(X509_get_issuer_name(cert.cert), 0, 0));
			certificates << "Issuer: " << line.get() << "\n";
			return certificates.str();
		} else {
			return {"No client certificates configured."};
		}
	}
};

template<class LkT>
inline wrapper<LkT>::wrapper(type_t type, domain_t domain, verify_SSL_certificates verify_SSL)
	: type_(type), domain_(domain), verify_SSL_(verify_SSL), ctx_(verify_SSL_) {
}

template<class LkT>
inline void
wrapper<LkT>::connect(std::string const& hostname, std::string const& service) {
	ssl_= std::make_unique<SSL_socket_wrapper>(ctx_, verify_SSL_);
	addrs_info_t addr_info(type_, domain_, hostname, service);
	for(struct addrinfo* addr= addr_info.addrs; addr != nullptr; addr= addr->ai_next) {
		auto connection= std::make_shared<socket_t>(static_cast<type_t>(addr_info.addrs->ai_socktype), static_cast<domain_t>(addr_info.addrs->ai_family), addr_info.addrs->ai_protocol);
		connection->connect(addr->ai_addr, addr->ai_addrlen);
		ssl_->connect(hostname, connection->native_handle());
		underlying_connections_.push_back(connection);
	}
}

template<class LkT>
inline bool
wrapper<LkT>::is_open() const noexcept(false) {
	return !underlying_connections_.empty() && std::find_if(underlying_connections_.begin(), underlying_connections_.end(), [](auto const& v) {
		return v->is_open();
	}) != underlying_connections_.end();
}

template<class LkT>
inline void
wrapper<LkT>::close() noexcept(false) {
	ssl_->close();
	underlying_connections_.clear();
}

template<class LkT>
template<class MsgT>
inline void
wrapper<LkT>::write(MsgT const& message) noexcept(false) {
	assert(!underlying_connections_.empty());
	const write_lock_t lk(this->mutex_);
	ssl_.write(message);
}

template<class LkT>
template<class V, std::size_t N>
inline void
wrapper<LkT>::write(std::array<V, N> const& message) noexcept(false) {
	assert(!underlying_connections_.empty());
	const write_lock_t lk(this->mutex_);
	ssl_->write(message);
}

template<class LkT>
inline bool
wrapper<LkT>::read_msg(std::byte* dest, std::size_t msg_size) noexcept(false) {
	return ssl_->read_msg(dest, msg_size);
}

template<class LkT>
template<class MsgT>
inline bool
wrapper<LkT>::read(MsgT& dest) noexcept(false) {
	if constexpr(MsgT::has_static_size) {
		assert(dest.length() <= sizeof(MsgT));
		BOOST_MPL_ASSERT_RELATION(sizeof(MsgT), <=, SSIZE_MAX);
		const bool failed= read_msg(reinterpret_cast<std::byte*>(&dest), sizeof(MsgT));
		assert(failed || dest.is_valid());
		return failed;
	} else {
		constexpr const std::size_t header_t_sz= MsgT::header_t_size;
		const bool failed= read_msg(reinterpret_cast<std::byte*>(&dest), header_t_sz);
		if(!failed) [[likely]] {
			typename MsgT::Header_t const* hdr= reinterpret_cast<typename MsgT::Header_t const*>(&dest);
			const std::size_t length= hdr->length();
			if(length >= header_t_sz && length <= SSIZE_MAX && length <= sizeof(MsgT)) {
				const std::size_t body_size= length - header_t_sz;
				if(body_size > 0) {
					return read_msg(reinterpret_cast<std::byte*>(&dest) + header_t_sz, length - header_t_sz);
				} else {
					assert(hdr->is_valid());
					return failed;
				}
			} else {
				BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to read the message from the socket. Type of message to read={}, SSL cipher in use: '{}'", boost::core::demangle(typeid(MsgT).name()), ::SSL_get_cipher(ssl_)), &wrapper::read<MsgT>));
			}
		} else [[unlikely]] {
			return failed;
		}
	}
}

template<class LkT>
template<class V, std::size_t SrcSz>
inline bool
wrapper<LkT>::read(V (&dest)[SrcSz]) noexcept(false) {
	BOOST_MPL_ASSERT_RELATION(sizeof(V) * SrcSz, <=, SSIZE_MAX);
	return read_msg(reinterpret_cast<std::byte*>(dest), sizeof(V) * SrcSz);
}

template<class LkT>
template<class MsgDetails, class V, std::size_t N>
inline bool
wrapper<LkT>::read(std::array<V, N>& buff) noexcept(false) {
	using msg_details_t= MsgDetails;

	BOOST_MPL_ASSERT_RELATION(msg_details_t::max_msg_size, >=, msg_details_t::header_t_size);

	const bool failed= read_msg(reinterpret_cast<std::byte*>(buff.data()), msg_details_t::header_t_size);
	if(!failed) [[likely]] {
		auto const* hdr= reinterpret_cast<typename msg_details_t::Header_t const*>(buff.data());
		const std::size_t length= hdr->length();
		if(length >= msg_details_t::header_t_size && length <= SSIZE_MAX && length <= (N * sizeof(V))) [[likely]] {
			const std::size_t body_size= length - msg_details_t::header_t_size;
			if(body_size > 0) {
				return read_msg(reinterpret_cast<std::byte*>(buff.data()) + msg_details_t::header_t_size, length - msg_details_t::header_t_size);
			} else {
				assert(hdr->is_valid());
				return false;
			}
		} else [[unlikely]] {
			return true;
		}
	} else [[unlikely]] {
		return failed;
	}
}

template<class LkT>
inline void
wrapper<LkT>::set_options(std::size_t min_message_size, std::size_t max_message_size, int timeout, socket_priority priority, std::size_t incoming_cpu) noexcept(false) {
	std::for_each(underlying_connections_.begin(), underlying_connections_.end(), [min_message_size, max_message_size, timeout, priority, incoming_cpu](auto& socket) {
		socket->set_options(min_message_size, max_message_size, timeout, priority, incoming_cpu);
	});
}

template<class LkT>
inline std::string
wrapper<LkT>::local_ip() const noexcept(false) {
	return std::accumulate(underlying_connections_.begin(), underlying_connections_.end(), std::string{}, [](auto const& acc, auto const& v) {
		return acc + v->local_ip() + ", ";
	});
}

}
}}}}}
