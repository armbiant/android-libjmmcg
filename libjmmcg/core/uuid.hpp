#ifndef LIBJMMCG_CORE_UUID_HPP
#define LIBJMMCG_CORE_UUID_HPP

/******************************************************************************
** Copyright © 2017 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <string>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline std::string
get_uuid_as_string() noexcept(false) {
	const boost::uuids::uuid uuid=boost::uuids::random_generator()();
	std::ostringstream os;
	os<<uuid;
	return os.str();
}

} }

#endif
