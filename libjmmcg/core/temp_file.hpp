#ifndef LIBJMMCG_CORE_TEMP_FILE_HPP
#define LIBJMMCG_CORE_TEMP_FILE_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/*
	Wrap up into a class creating a temporary file, so that it is deleted upon object
	destruction. This class is completely ANSI. Note that the temporary file is created
	in the root of the current drive. Not ideal, but the ANSI function "tmpnam(...)"
	doesn't work any other way.
*/

#include "file.hpp"
#include "syscall_wrapper.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<typename E_, ppd::generic_traits::api_type::element_type API, typename Mdl>
class temp_file : public file<E, API, Mdl> {
public:
	[[nodiscard]] temp_file(const std::ios_base::openmode& om= (std::ios_base::openmode)0);
	[[nodiscard]] temp_file(const temp_file&);
	~temp_file();
	[[nodiscard]] temp_file& operator=(const temp_file&);

private:
	static inline tstring get_file_name();
};

template<typename E, ppd::generic_traits::api_type::element_type API, typename Mdl>
inline temp_file<E, API, Mdl>::temp_file(const std::ios_base::openmode& om)
	: file<E, API, Mdl>(get_file_name(), om | std::ios_base::in | std::ios_base::out | std::ios_base::trunc) {
}

template<typename E, ppd::generic_traits::api_type::element_type API, typename Mdl>
inline temp_file<E, API, Mdl>::temp_file(const temp_file& tf)
	: file<E, API, Mdl>(get_file_name(), std::ios_base::in | std::ios_base::out | std::ios_base::trunc) {
	file<E, API, Mdl>::operator<<(tf.rdbuf());
}

template<typename E, ppd::generic_traits::api_type::element_type API, typename Mdl>
inline temp_file<E, API, Mdl>::~temp_file() {
}

template<typename E, ppd::generic_traits::api_type::element_type API, typename Mdl>
inline temp_file<E, API, Mdl>&
temp_file<E, API, Mdl>::operator=(const temp_file<E, API, Mdl>& tf) {
	file<E, API, Mdl>::remove();
	create(tf.object);
	file<E, API, Mdl>::file_type::operator<<(tf.rdbuf());
	return *this;
}

template<typename E, ppd::generic_traits::api_type::element_type API, typename Mdl>
inline tstring
temp_file<E, API, Mdl>::get_file_name() noexcept(false) {
	char n[L_tmpnam + 1];
	JMMCG_SYSCALL_WRAPPER("Failed to create the temporary name.", _T(LIBJMMCG_VERSION_NUMBER), ::tmpnam, n);
	return StringToTString(n);
}

}}

#endif
