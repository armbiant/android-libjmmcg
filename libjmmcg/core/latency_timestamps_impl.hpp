/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline bool
latency_timestamps_itf::timestamp::is_valid() const noexcept(true) {
	return start>element_type{} && start<end;
}

REALLY_FORCE_INLINE inline
latency_timestamps_itf::period::period(latency_timestamps_itf &lts) noexcept(true)
: lts_(lts) {
	ts.start=timer_t::get_start();
}

REALLY_FORCE_INLINE inline
latency_timestamps_itf::period::~period() noexcept(true) {
	ts.end=timer_t::get_end();
	assert(ts.is_valid());
	lts_.push_back(ts);
}

REALLY_FORCE_INLINE inline latency_timestamps::size_type
latency_timestamps::size() const noexcept(true) {
	const std::ptrdiff_t sz=std::distance(timestamps.get(), current_timestamp.load());
	assert(sz>=0);
	assert(static_cast<size_type>(sz)<=num_timestamps);
	return static_cast<size_type>(sz);
}

REALLY_FORCE_INLINE inline void
latency_timestamps::push_back(element_type const &ts) noexcept(true) {
	if (num_timestamps>0 && size()<num_timestamps) {
		element_type *curr_ts=current_timestamp.fetch_add(1U, std::memory_order_relaxed);
		assert(curr_ts<std::next(timestamps.get(), num_timestamps));
		assert(ts.is_valid());
		PREFETCH_WRITE(curr_ts, _MM_HINT_NTA);
		*curr_ts=ts;
		assert(curr_ts->is_valid());
	}
}

} }
