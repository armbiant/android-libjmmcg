#ifndef LIBJMMCG_CORE_INT128_COMPATIBILITY_HPP
#define LIBJMMCG_CORE_INT128_COMPATIBILITY_HPP

/******************************************************************************
 * * Copyright © 2021 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "core_config.h"

#include <boost/mpl/assert.hpp>

#include <cassert>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <limits>

/**
 * \file	For those architectures that do not support 128-bit integer types as builtins, e.g. ARM & many 32-bit platforms.
 */

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

#ifdef __SIZEOF_INT128__
#	pragma GCC diagnostic push
#	pragma GCC diagnostic ignored "-Wpedantic"
using int128_t= __int128;
using uint128_t= __uint128_t;
#	pragma GCC diagnostic pop

[[gnu::pure]] inline unsigned
count_trailing_zeroes_compat(const uint128_t value) noexcept(true) {
	union [[gnu::packed]] converter {
		const uint128_t v_;
		struct [[gnu::packed]] parts {
			const unsigned long long low;
			const unsigned long long high;
		} const p;
		[[nodiscard]] constexpr explicit converter(uint128_t v) noexcept(true)
			: v_(v) {
		}
	};
	BOOST_MPL_ASSERT_RELATION(sizeof(uint128_t), ==, sizeof(converter));
	assert(value != 0);
	const converter c(value);
	return static_cast<unsigned>(__builtin_ctzll(c.p.high) + __builtin_ctzll(c.p.low));
}

[[gnu::pure]] inline unsigned
count_leading_zeroes_compat(const uint128_t value) noexcept(true) {
	union [[gnu::packed]] converter {
		const uint128_t v_;
		struct [[gnu::packed]] parts {
			const unsigned long long low;
			const unsigned long long high;
		} const p;
		[[nodiscard]] constexpr explicit converter(uint128_t v) noexcept(true)
			: v_(v) {
		}
	};
	BOOST_MPL_ASSERT_RELATION(sizeof(uint128_t), ==, sizeof(converter));
	assert(value != 0);
	const converter c(value);
	return static_cast<unsigned>(__builtin_clzll(c.p.high) + __builtin_clzll(c.p.low));
}

#else
struct [[gnu::packed]] int128_t {
	std::uint64_t low;
	std::int64_t high;
};
struct [[gnu::packed]] uint128_t {
	std::uint64_t low;
	std::uint64_t high;

	constexpr bool
	operator==(std::uint64_t rhs) const noexcept(true) {
		return high == 0 && low < rhs;
	}
	constexpr bool
	operator!=(std::uint64_t rhs) const noexcept(true) {
		return !this->operator==(rhs);
	}
	constexpr bool
	operator<(uint128_t const& rhs) const noexcept(true) {
		return high < rhs.high
				 || (high == rhs.high && low < rhs.low);
	}

	constexpr uint128_t&
	operator~() noexcept(true) {
		low= ~low;
		high= ~high;
		return *this;
	}
	constexpr uint128_t&
	operator&=(uint128_t const& rhs) noexcept(true) {
		low&= rhs.low;
		high&= rhs.high;
		return *this;
	}
	constexpr uint128_t
	operator>>(std::size_t const shift) const noexcept(true) {
		assert(shift <= 128);
		if(shift >= 64) {
			uint128_t tmp{high, 0};
			assert(tmp < *this);
			return tmp >> (shift - 64);
		} else {
			const uint128_t tmp{
				(high << (64 - shift)) | (low >> shift),
				(high >> shift)};
			assert(tmp < *this);
			return tmp;
		}
	}
};

inline unsigned
count_trailing_zeroes_compat(const uint128_t value) noexcept(true) {
	assert(value.high != 0 || value.low != 0);
	return __builtin_ctzll(value.high) + __builtin_ctzll(value.low);
}

inline unsigned
count_leading_zeroes_compat(const uint128_t value) noexcept(true) {
	assert(value.high != 0 || value.low != 0);
	return __builtin_clzll(value.high) + __builtin_clzll(value.low);
}

#endif

template<class V, class T>
	requires((std::is_same_v<V, libjmmcg::int128_t> || std::is_same_v<V, libjmmcg::uint128_t>) && std::is_integral_v<T>)
inline constexpr auto
max_value(V v, T t) noexcept(true) -> decltype(v > t ? v : t) {
	return v > t ? v : t;
}

template<class T, class V>
	requires((std::is_same_v<V, libjmmcg::int128_t> || std::is_same_v<V, libjmmcg::uint128_t>) && std::is_integral_v<T>)
inline constexpr auto
max_value(T t, V v) noexcept(true) -> decltype(v > t ? v : t) {
	return v > t ? v : t;
}

template<class V, class T>
	requires((std::is_same_v<V, libjmmcg::int128_t> || std::is_same_v<V, libjmmcg::uint128_t>) && std::is_integral_v<T>)
inline constexpr auto
min_value(V v, T t) noexcept(true) -> decltype(v > t ? v : t) {
	return v < t ? v : t;
}

template<class T, class V>
	requires((std::is_same_v<V, libjmmcg::int128_t> || std::is_same_v<V, libjmmcg::uint128_t>) && std::is_integral_v<T>)
inline constexpr auto
min_value(T t, V v) noexcept(true) -> decltype(v > t ? v : t) {
	return v < t ? v : t;
}

}}

namespace std {

inline std::ostream&
operator<<(std::ostream& os, libjmmcg::int128_t i) noexcept(false) {
	union converter {
		struct portions {
			std::uint64_t low_;
			std::int64_t hi_;
		};
		portions p;
		libjmmcg::int128_t v_;

		explicit constexpr converter(libjmmcg::int128_t i) noexcept(true)
			: v_(i) {}
	};
	const converter conv(i);
	if(conv.p.hi_ > 0) {
		os << conv.p.hi_ << std::setfill('0') << std::setw(std::numeric_limits<libjmmcg::int128_t>::max_digits10) << conv.p.low_;
	} else {
		os << conv.p.low_;
	}
	return os;
}

inline std::ostream&
operator<<(std::ostream& os, libjmmcg::uint128_t i) noexcept(false) {
	union converter {
		struct portions {
			std::uint64_t low_;
			std::uint64_t hi_;
		};
		portions p;
		libjmmcg::uint128_t v_;

		explicit constexpr converter(libjmmcg::uint128_t i) noexcept(true)
			: v_(i) {}
	};
	const converter conv(i);
	if(conv.p.hi_ > 0) {
		os << conv.p.hi_ << std::setfill('0') << std::setw(std::numeric_limits<libjmmcg::int128_t>::max_digits10) << conv.p.low_;
	} else {
		os << conv.p.low_;
	}
	return os;
}

}

#endif
