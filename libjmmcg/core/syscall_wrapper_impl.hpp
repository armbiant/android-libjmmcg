/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class FailureTraits, class... FnArgs>
template<class HandleError>
inline auto
syscall::traits_base<FailureTraits, FnArgs...>::report(fn_ret_code ret, HandleError&& handle_error) noexcept(false) {
	if(!failure_detection::result(ret, reinterpret_cast<fn_ret_code>(failure_code::value))) [[likely]] {
		return ret;
	} else {
		handle_error();
		return fn_ret_code{};
	}
}

template<class FailureTraits, class... Args>
void
syscall::traits_base<FailureTraits, Args...>::report_error(int err, ::boost::source_location const& src_loc, std::string const& err_msg, fn_ret_code (*fn)(Args...), Args... args) noexcept(false) {
	std::string args_str;
	([&args_str](Args&& arg) {
		args_str+= fmt::format("type='{}', value={}, ", boost::core::demangle(typeid(Args).name()), boost::lexical_cast<std::string>(std::forward<Args>(arg)));
	}(std::forward<Args>(args)),
		...);
	return ::boost::throw_exception(throw_crt_exception<std::runtime_error, ppd::platform_api, ppd::heavyweight_threading>(std::string(err_msg), src_loc.function_name(), fn, err) << errinfo_parameters(args_str), src_loc);
}

template<class FailureTraits, class... Args>
inline auto
syscall::simple_report_traits<FailureTraits, Args...>::select_error(int err, ::boost::source_location const& src_loc, std::string const& err_msg, fn_ret_code (*fn)(Args...), Args... args) noexcept(false) {
	return base_t::report_error(err, src_loc, err_msg, fn, args...);
}

template<class CaseStatements, class FailureTraits, class... Args>
inline constexpr auto
syscall::switch_traits<CaseStatements, FailureTraits, Args...>::select_error(int err, ::boost::source_location const& src_loc, std::string const& err_msg, fn_ret_code (*fn)(Args...), Args... args) {
	enum : std::size_t {
		num_statements= std::tuple_size<std::decay_t<case_statements>>::value
	};
	BOOST_MPL_ASSERT_RELATION(num_statements, >, 1);
	private_::unroller<num_statements - 1, case_statements>::result(
		err,
		[err, src_loc, err_msg, fn, args...]() {
			base_t::report_error(err, src_loc, err_msg, fn, args...);
		});
}

template<
	template<class, class...>
	class Traits,
	template<class, template<class> class, template<class> class>
	class FailureTraits,
	template<class>
	class FailureCode,
	template<class>
	class FailureDetection,
	class FnReturnType,
	class... FnArgs,
	class... PassedInArgs>
	requires(sizeof...(FnArgs) == sizeof...(PassedInArgs))
inline FnReturnType
syscall::process(::boost::source_location const& src_loc, std::string const& err_msg, FnReturnType (*fn)(FnArgs...), PassedInArgs... args) noexcept(false) {
	if constexpr(std::is_same<FnReturnType, void>::value) {
		fn(static_cast<FnArgs>(args)...);
	} else {
		using traits_t= Traits<
			FailureTraits<
				FnReturnType,
				FailureCode,
				FailureDetection>,
			FnArgs...>;
		auto ret= fn(static_cast<FnArgs>(args)...);
		return traits_t::report(ret,
			[src_loc, err_msg, fn, args...]() {
				traits_t::select_error(errno, src_loc, err_msg, fn, static_cast<FnArgs>(args)...);
			});
	}
}

}}
