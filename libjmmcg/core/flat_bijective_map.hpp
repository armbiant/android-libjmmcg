#ifndef LIBJMMCG_CORE_FLAT_BIJECTIVE_MAP_HPP
#define LIBJMMCG_CORE_FLAT_BIJECTIVE_MAP_HPP
/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "flat_map.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<
	class KeyType,
	class MappedType
>
class flat_bijective_map {
public:
	using size_type=std::size_t;
	using value_type=typename flat_map<KeyType, MappedType>::value_type;

	[[nodiscard]] flat_bijective_map() noexcept(true)=default;
	[[nodiscard]] flat_bijective_map(flat_bijective_map const &)=default;
	[[nodiscard]] flat_bijective_map(flat_bijective_map &&)=default;
	/**
		\see flat_map::insert()
	*/
	template<class FwdIter>
	[[nodiscard]] constexpr flat_bijective_map(FwdIter b, FwdIter e) noexcept(false);

	constexpr bool empty() const noexcept(true);
	constexpr size_type size() const noexcept(true);

	void constexpr clear() noexcept(true);

	/**
		\see flat_map::at()
	*/
	MappedType const &at(KeyType const &k) const noexcept(false);
	/**
		\see flat_map::at()
	*/
	KeyType const &at(MappedType const &k) const noexcept(false);

	/**
		\see flat_map::insert()
	*/
	template<class Param1, class... Params>
	void insert(Param1 const &arg1, Params const &...args) noexcept(false);
	/**
		\see flat_map::insert()
	*/
	template<class Param1, class... Params>
	void insert(Param1 &&arg1, Params &&...args) noexcept(false);

	template<class KeyType1, class MappedType1>
	friend std::ostream &operator<<(std::ostream &os, flat_bijective_map<KeyType1, MappedType1> const &v) noexcept(false);

private:
	flat_map<KeyType, MappedType> key_to_mapped_{};
	flat_map<MappedType, KeyType> mapped_to_key_{};
};

} }

#include "flat_bijective_map_impl.hpp"

#endif
