#ifndef LIBJMMCG_CORE_GCD_HPP
#define LIBJMMCG_CORE_GCD_HPP

/******************************************************************************
** Copyright © 1993 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

namespace euclid {

	/// Euclid's Greatest Common Divisor Algorithm.
	/**
	 * This is best used for the general case of finding the gcd.
	 * The Binary method may be faster than Euclid's Algorithm.
	 * 
	 * A one-liner for fun! (But it's slower, as it's got an extra jump...)
	 *		for (u=x,v=y;v;r=u%v,u=v,v=r);
	 *
	 * Thanks to Knuth, et al for Euclid's Algorithm and the Binary GCD algorithm.
	 * See his book "Seminumerical Algorithms. Vol 2", pg 320.
	 * 
	 * \param x A value greater that zero.
	 * \param y A value greater that zero.
	 * 
	 * \see binary::gcd()
	 */
	template<class V> [[nodiscard, gnu::const]] constexpr inline V
	gcd(const V x, const V y) noexcept(true) {
		assert(x>V{} && y>V{});
		V u=x, v=y;
		do {
			const V r=u%v;
			u=v;
			v=r;
		} while (v);
		return u;
	}

}

namespace binary {

	/// Binary Greatest Common Divisor Algorithm.
	/**
	 * This method is best suited to integers.
	 * 
	 * A one-liner for fun! (But it's slower, as it's got an extra jump...)
	 *		for (u=x,v=y;v;r=u%v,u=v,v=r);
	 *
	 * Thanks to Knuth, et al for Euclid's Algorithm and the Binary GCD algorithm.
	 * See his book "Seminumerical Algorithms. Vol 2", pg 320.
	 * 
	 * \param x A value greater that zero.
	 * \param y A value greater that zero.
	 * 
	 * \see euclid::gcd()
	 */
	template<class V> [[nodiscard, gnu::const]] constexpr inline V
	gcd(const V x, const V y) noexcept(true) {
		assert(x>V{} && y>V{});
		V k=0, u=x, v=y;
		for (; !(u&1) && !(v&1); ++k, (u>>=1), (v>>=1));
		V t=u&1 ? -v : u;
		do {
			do {
				t>>=1;
			} while (!(t&1));
			t>0 ? (u=t) : (v=-t);
		} while ((t=u-v));
		return u<<k;
	}

}

} }

#endif
