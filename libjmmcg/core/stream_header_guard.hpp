#ifndef LIBJMMCG_CORE_STREAM_HEADER_GUARD_HPP
#define LIBJMMCG_CORE_STREAM_HEADER_GUARD_HPP
/******************************************************************************
 * * Copyright © 2021 by J.M.McGuiness, coder@hussar.me.uk
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 **
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 ** Lesser General Public License for more details.
 **
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "core_config.h"

#include <filesystem>
#include <iostream>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

class header_guard_t {
public:
	static inline constexpr const char copyright[]=
	"/******************************************************************************\n"
	"** Copyright © 2021 by J.M.McGuiness, coder@hussar.me.uk\n"
	"**\n"
	"** This library is free software; you can redistribute it and/or\n"
	"** modify it under the terms of the GNU Lesser General Public\n"
	"** License as published by the Free Software Foundation; either\n"
	"** version 2.1 of the License, or (at your option) any later version.\n"
	"**\n"
	"** This library is distributed in the hope that it will be useful,\n"
	"** but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU\n"
	"** Lesser General Public License for more details.\n"
	"**\n"
	"** You should have received a copy of the GNU Lesser General Public\n"
	"** License along with this library; if not, write to the Free Software\n"
	"** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA\n"
	"*/\n";
	static inline constexpr const char header_guard_postfix[]="#endif\n";

	[[nodiscard]] header_guard_t(std::string const &exe, std::string const &nm_space, std::string const &fname, std::ostream &s) noexcept(false);

protected:
	std::ostream &os;

	~header_guard_t() noexcept(false);
};

inline
header_guard_t::header_guard_t(std::string const &exe, std::string const &nm_space, std::string const &fname, std::ostream &s) noexcept(false)
: os(s) {
	os
	<<"#ifndef LIBJMMCG_"<<nm_space<<"_"<<std::filesystem::path(fname).stem().string()<<"_HPP\n"
	"#define LIBJMMCG_"<<nm_space<<"_"<<std::filesystem::path(fname).stem().string()<<"_HPP\n"
	<<copyright
	<<"// Auto-generated header file.\n"
	"// DO NOT EDIT. IT WILL BE OVERWRITTEN.\n"
	"// Version: " LIBJMMCG_VERSION_NUMBER "\n"
	"// O/S info: '" LIBJMMCG_SYSTEM_NAME << "', specifically: '" LIBJMMCG_SYSTEM "'\n"
	"/**\n"
	"\t\\file The constants created in this file are used in various places in the code, so must not be manually doctored! To re-generate this file for your O/S, run '"<<std::filesystem::path(exe).filename().string()<<"' in the installation directory as a suitable user.\n"
	"*/\n"
	"\n";
}

inline
header_guard_t::~header_guard_t() noexcept(false) {
	os<<header_guard_postfix;
}

} }

#endif
