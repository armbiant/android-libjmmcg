/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace lfsr {

template<std::size_t N, std::size_t Tap0, std::size_t Tap1, std::size_t Tap2>
	requires(N <= (sizeof(libjmmcg::uint128_t) * 8U) && Tap0 <= N && Tap1 <= N && Tap2 <= N)
inline constexpr libjmmcg::uint128_t
fibonacci<N, Tap0, Tap1, Tap2>::max() noexcept(true) {
	return (1U << size) - 1U;
}

template<std::size_t N, std::size_t Tap0, std::size_t Tap1, std::size_t Tap2>
	requires(N <= (sizeof(libjmmcg::uint128_t) * 8U) && Tap0 <= N && Tap1 <= N && Tap2 <= N)
inline constexpr typename fibonacci<N, Tap0, Tap1, Tap2>::element_type
fibonacci<N, Tap0, Tap1, Tap2>::next(element_type const v) noexcept(true) {
	assert(v != 0);
	element_type lfsr= v;
	const element_type bit= ((lfsr >> 0U) ^ (lfsr >> Tap2) ^ (lfsr >> Tap1) ^ (lfsr >> Tap0)) & element_type{1U};
	auto const lfsr_wide= (lfsr >> 1U) | (bit << (size - 1U));
	lfsr= static_cast<element_type>(lfsr_wide);
	assert(lfsr != v);
	return lfsr;
}

template<std::size_t N>
	requires(N <= (sizeof(libjmmcg::uint128_t) * 8U))
inline constexpr libjmmcg::uint128_t
maximal<N>::max() noexcept(true) {
	return (libjmmcg::uint128_t{1U} << size) - 1U;
}

template<>
constexpr inline maximal<8U>::element_type
maximal<8U>::next(element_type const v) noexcept(true) {
	return fibonacci<8U, 4, 5, 6>::next(v);
}

/**
 * See <a href="https://en.wikipedia.org/wiki/Linear-feedback_shift_register"/> regarding the implementation detials.
 *
 * @param v The value to compute the lfsr for.
 * @return The computed value. Depending upon the specific values of the Taps, this may not be maximal.
 */
template<>
constexpr inline maximal<16U>::element_type
maximal<16U>::next(element_type const v) noexcept(true) {
	// Or: fibonacci<16U, 2, 3, 5>::next(v));
	assert(v != 0);
	element_type lfsr= v;
	lfsr^= (lfsr >> 7U);
	lfsr^= (lfsr << 9U);
	lfsr^= (lfsr >> 13U);
	assert(lfsr != v);
	return lfsr;
}

template<>
constexpr inline maximal<32U>::element_type
maximal<32U>::next(element_type const v) noexcept(true) {
	return fibonacci<32U, 1, 2, 22>::next(v);
}

template<>
constexpr inline maximal<44U>::element_type
maximal<44U>::next(element_type const v) noexcept(true) {
	return fibonacci<44U, 17, 18, 43>::next(v);
}

template<>
constexpr inline maximal<64U>::element_type
maximal<64U>::next(element_type const v) noexcept(true) {
	return fibonacci<64U, 60, 61, 63>::next(v);
}

template<>
constexpr inline maximal<128U>::element_type
maximal<128U>::next(element_type const v) noexcept(true) {
	return fibonacci<128U, 99, 101, 126>::next(v);
}

template<std::size_t N>
	requires(N <= (sizeof(libjmmcg::uint128_t) * 8U))
inline typename maximal<N>::period_type
maximal<N>::period(element_type const start_state) noexcept(true) {
	element_type lfsr= start_state;
	period_type period{};
	do {
		lfsr= next(lfsr);
		assert(period < max());
		++period;
		assert(period > 0);
	} while(lfsr != start_state);
	assert(period == max());
	return period;
}

template<std::size_t N>
inline secure<N>::secure() noexcept(false)
	: lfsr_current_value_(
		[this]() {
			std::random_device rng;
			if(rng.entropy() > 0.0) {
				std::mt19937_64 gen(rng());
				std::uniform_int_distribution<unsigned long long> distribution(0, static_cast<unsigned long long>(libjmmcg::min_value(lfsr_t::max(), std::numeric_limits<unsigned long long>::max())));
				return lfsr_t::next(distribution(gen));
			} else {
				BOOST_THROW_EXCEPTION(throw_api_exception<std::invalid_argument>("The std::random_device reports that it is not a true random device, required for this object.", this));
			}
		}()) {
}

template<std::size_t N>
inline secure<N>::secure(unsigned int seed) noexcept(true)
	: lfsr_current_value_(
		[](unsigned int seed) {
			std::mt19937_64 gen(seed);
			std::uniform_int_distribution<unsigned long long> distribution(0, static_cast<unsigned long long>(libjmmcg::min_value(lfsr_t::max(), std::numeric_limits<unsigned long long>::max())));
			return lfsr_t::next(distribution(gen));
		}(seed)) {
}

template<std::size_t N>
constexpr inline secure<N>::element_type
secure<N>::next() noexcept(true) {
	lfsr_current_value_= lfsr_t::next(lfsr_current_value_);
	return lfsr_current_value_;
}

}}}
