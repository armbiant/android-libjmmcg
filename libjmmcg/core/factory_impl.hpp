/******************************************************************************
** Copyright © 2005 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace factory {

template<typename ID_, class Ret_, class Except_>
inline Ret_
not_found<ID_, Ret_, Except_>::execute(const ID_& id) noexcept(false) {
	BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Unable to create object with the given id='{}'", tostring(id)), &not_found<ID_, Ret_, Except_>::execute));
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::base(void) {
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::~base(void) {
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline bool
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::insert(const value_type& createable) {
	return createables_.insert(createable).second;
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline bool
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::insert(const id_type& id, const createable_type& createable) {
	return insert(value_type(id, createable));
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline bool
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::find(const id_type& id) const {
	return createables_.find(id) != createables_.end();
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline bool
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::empty(void) const noexcept(true) {
	return createables_.empty();
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline const typename base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::size_type
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::size(void) const noexcept(true) {
	return createables_.size();
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline void
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::reserve(const size_type sz) {
	return createables_.reserve(sz);
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline const typename base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::container_type&
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::createables(void) const noexcept(true) {
	return createables_;
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline typename base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::container_type&
base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::createables(void) noexcept(true) {
	return createables_;
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline constexpr creator<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::creator(void)
	: base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>() {
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline creator<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::~creator(void) {
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline typename creator<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::created_type
creator<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::make(const id_type& id) const {
	const typename Cont_::const_iterator createable(base_t::createables_.find(id));
	if(createable != base_t::createables_.end()) {
		return createable->second();
	}
	return NotFound_::execute(id);
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline constexpr clone<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::clone(void)
	: base<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>() {
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline clone<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::~clone(void) {
}

template<typename ID_, class Obj_, class Except_, class Ret_, class NotFound_, typename CreatFn_, class Cont_>
inline typename clone<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::created_type
clone<ID_, Obj_, Except_, Ret_, NotFound_, CreatFn_, Cont_>::make(const id_type& id) const {
	const typename Cont_::const_iterator createable(base_t::createables_.find(id));
	if(createable != base_t::createables_.end()) {
		const createable_type creator(createable->second);
		return creator.second(*creator.first);
	}
	return NotFound_::execute(id);
}

}}}
