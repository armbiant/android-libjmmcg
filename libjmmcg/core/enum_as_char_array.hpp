#ifndef LIBJMMCG_CORE_ENUM_AS_CHAR_ARRAY_HPP
#define LIBJMMCG_CORE_ENUM_AS_CHAR_ARRAY_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, libjmmcg@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <boost/mpl/assert.hpp>

#include <array>
#include <cstdint>
#include <type_traits>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// Construct enum-tag values from a sequence of chars, and extract from the tag-value the string that was encoded.
namespace enum_tags {

/// A function to convert a string-literal or C-style string or array of chars into one of these magic enum-tags-as-a-string.
template<class EnumT, std::size_t N>
constexpr inline typename std::enable_if<N>=sizeof(EnumT), EnumT>::type
convert(const char (&s)[N]) noexcept(true) {
	using u_t=typename std::underlying_type<EnumT>::type;

	std::size_t n=sizeof(EnumT);
	u_t t=0;
	do {
		t+=static_cast<u_t>(s[n-1])<<((n-1)*8);
	} while (--n>0);
	return static_cast<EnumT>(t);
}

/// A function to convert, even faster, a correctly sized array of chars into one of these magic enum-tags-as-a-string.
/**
 * \param	s	A correctly sized array of chars means that the number of characters must exactly match the number of chars in the size of the underlying type of the enum.
 */
template<class EnumT>
constexpr inline EnumT
convert(std::array<char, sizeof(EnumT)> const &s) noexcept(true) {
	using u_t=typename std::underlying_type<EnumT>::type;
	union conv_t {
		std::array<char, sizeof(EnumT)> str;
		u_t t;

		conv_t() noexcept(true)
		: str() {}
	} conv;
	conv.str=s;
	return static_cast<EnumT>(conv.t);
}

namespace mpl {

namespace private_ {

template<char c1, char c2='\0', char c3='\0', char c4='\0', char c5='\0', char c6='\0', char c7='\0', char c8='\0'>
struct select_underlying_type {
	using type=std::uint64_t;
};
template<char c1, char c2, char c3, char c4, char c5, char c6, char c7>
struct select_underlying_type<c1, c2, c3, c4, c5, c6, c7, '\0'> {
	using type=std::uint64_t;
};
template<char c1, char c2, char c3, char c4, char c5, char c6>
struct select_underlying_type<c1, c2, c3, c4, c5, c6, '\0', '\0'> {
	using type=std::uint64_t;
};
template<char c1, char c2, char c3, char c4, char c5>
struct select_underlying_type<c1, c2, c3, c4, c5, '\0', '\0', '\0'> {
	using type=std::uint64_t;
};
template<char c1, char c2, char c3, char c4>
struct select_underlying_type<c1, c2, c3, c4, '\0', '\0', '\0', '\0'> {
	using type=std::uint32_t;
};
template<char c1, char c2, char c3>
struct select_underlying_type<c1, c2, c3, '\0', '\0', '\0', '\0', '\0'> {
	using type=std::uint32_t;
};
template<char c1, char c2>
struct select_underlying_type<c1, c2, '\0', '\0', '\0', '\0', '\0', '\0'> {
	using type=std::uint16_t;
};
template<char c1>
struct select_underlying_type<c1, '\0', '\0', '\0', '\0', '\0', '\0', '\0'> {
	using type=std::uint8_t;
};

template<
	class EnumT,
	EnumT t,
	typename std::underlying_type<EnumT>::type i=(static_cast<typename std::underlying_type<EnumT>::type>(t)>>8),
	bool is_digit=(static_cast<bool>(i))
>
struct count_digits {
	enum : typename std::underlying_type<EnumT>::type {
		value=1+count_digits<EnumT, t, (i>>8)>::value
	};
};
template<
	class EnumT,
	EnumT t,
	typename std::underlying_type<EnumT>::type i
>
struct count_digits<EnumT, t, i, false> {
	enum : typename std::underlying_type<EnumT>::type {
		value=1
	};
};

}

/// Convert the chars into a suitable enum-tag.
/**
	\see to_array
*/
template<char c1, char c2='\0', char c3='\0', char c4='\0', char c5='\0', char c6='\0', char c7='\0', char c8='\0'>
struct to_tag {
	using element_type=typename private_::select_underlying_type<c1, c2, c3, c4, c5, c6, c7, c8>::type;

	enum : element_type {
		value=
			(static_cast<element_type>(c8)<<56)
			+(static_cast<element_type>(c7)<<48)
			+(static_cast<element_type>(c6)<<40)
			+(static_cast<element_type>(c5)<<32)
			+(static_cast<element_type>(c4)<<24)
			+(static_cast<element_type>(c3)<<16)
			+(static_cast<element_type>(c2)<<8)
			+static_cast<element_type>(c1)
	};
};
template<char c1, char c2, char c3, char c4, char c5, char c6, char c7>
struct to_tag<c1, c2, c3, c4, c5, c6, c7, '\0'> {
	using element_type=typename private_::select_underlying_type<c1, c2, c3, c4, c5, c6, c7>::type;

	enum : element_type {
		value=
			(static_cast<element_type>(c7)<<48)
			+(static_cast<element_type>(c6)<<40)
			+(static_cast<element_type>(c5)<<32)
			+(static_cast<element_type>(c4)<<24)
			+(static_cast<element_type>(c3)<<16)
			+(static_cast<element_type>(c2)<<8)
			+static_cast<element_type>(c1)
	};
};
template<char c1, char c2, char c3, char c4, char c5, char c6>
struct to_tag<c1, c2, c3, c4, c5, c6, '\0', '\0'> {
	using element_type=typename private_::select_underlying_type<c1, c2, c3, c4, c5, c6>::type;

	enum : element_type {
		value=
			(static_cast<element_type>(c6)<<40)
			+(static_cast<element_type>(c5)<<32)
			+(static_cast<element_type>(c4)<<24)
			+(static_cast<element_type>(c3)<<16)
			+(static_cast<element_type>(c2)<<8)
			+static_cast<element_type>(c1)
	};
};
template<char c1, char c2, char c3, char c4, char c5>
struct to_tag<c1, c2, c3, c4, c5, '\0', '\0', '\0'> {
	using element_type=typename private_::select_underlying_type<c1, c2, c3, c4, c5>::type;

	enum : element_type {
		value=
			(static_cast<element_type>(c5)<<32)
			+(static_cast<element_type>(c4)<<24)
			+(static_cast<element_type>(c3)<<16)
			+(static_cast<element_type>(c2)<<8)
			+static_cast<element_type>(c1)
	};
};
template<char c1, char c2, char c3, char c4>
struct to_tag<c1, c2, c3, c4, '\0', '\0', '\0', '\0'> {
	using element_type=typename private_::select_underlying_type<c1, c2, c3, c4>::type;

	enum : element_type {
		value=
			(static_cast<element_type>(c4)<<24)
			+(static_cast<element_type>(c3)<<16)
			+(static_cast<element_type>(c2)<<8)
			+static_cast<element_type>(c1)
	};
};
template<char c1, char c2, char c3>
struct to_tag<c1, c2, c3, '\0', '\0', '\0', '\0', '\0'> {
	using element_type=typename private_::select_underlying_type<c1, c2, c3>::type;

	enum : element_type {
		value=
			(static_cast<element_type>(c3)<<16)
			+(static_cast<element_type>(c2)<<8)
			+static_cast<element_type>(c1)
	};
};
template<char c1, char c2>
struct to_tag<c1, c2, '\0', '\0', '\0', '\0', '\0', '\0'> {
	using element_type=typename private_::select_underlying_type<c1, c2>::type;

	enum : element_type {
		value=
			(static_cast<element_type>(c2)<<8)
			+static_cast<element_type>(c1)
	};
};
template<char c1>
struct to_tag<c1, '\0', '\0', '\0', '\0', '\0', '\0', '\0'> {
	using element_type=typename private_::select_underlying_type<c1>::type;

	enum : element_type {
		value=static_cast<element_type>(c1)
	};
};

/// Convert the suitably-created enum-tag into a char array.
/**
	\see to_tag
*/
template<
	class EnumT,
	EnumT t,
	unsigned num_digits=private_::count_digits<EnumT, t>::value
>
struct to_array;
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 8u> {
	enum : std::size_t {
		size=private_::count_digits<EnumT, t>::value	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>40)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>48)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>56)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>40)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>48)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>56)&0xFF),
		'\0'
	};
};
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 7u> {
	enum : std::size_t {
		size=private_::count_digits<EnumT, t>::value	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>40)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>48)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>40)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>48)&0xFF),
		'\0'
	};
};
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 6u> {
	enum : std::size_t {
		size=private_::count_digits<EnumT, t>::value	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>40)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>40)&0xFF),
		'\0'
	};
};
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 5u> {
	enum : std::size_t {
		size=private_::count_digits<EnumT, t>::value	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>32)&0xFF),
		'\0'
	};
};
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 4u> {
	enum : std::size_t {
		size=private_::count_digits<EnumT, t>::value	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>24)&0xFF),
		'\0'
	};
};
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 3u> {
	enum : std::size_t {
		size=private_::count_digits<EnumT, t>::value	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>16)&0xFF),
		'\0'
	};
};
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 2u> {
	enum : std::size_t {
		size=private_::count_digits<EnumT, t>::value	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		static_cast<char>((static_cast<typename std::underlying_type<EnumT>::type>(t)>>8)&0xFF),
		'\0'
	};
};
template<
	class EnumT,
	EnumT t
>
struct to_array<EnumT, t, 1u> {
	enum : std::size_t {
		size=1	///< The size of the extracted string, excluding the terminal NULL.
	};
	enum : typename std::underlying_type<EnumT>::type {
		value_as_int=static_cast<typename std::underlying_type<EnumT>::type>(t)
	};
	using const_element_type=const char [size+1];	///< The type of the const char-array of the extracted string.
	using element_type=char [size+1];	///< The type of the char-array of the extracted string.
	using element_type_no_null=char [size];	///< The type of the char-array of the extracted string without the terminating NULL.
	static inline constexpr const EnumT tag=t;	///< The enum-tag associated the with the encoded string.
	static inline constexpr const element_type_no_null value_no_null={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF)
	};
	/// The array of chars extracted from the enum-tag as a NULL-terminated string.
	static inline constexpr const const_element_type value={
		static_cast<char>(static_cast<typename std::underlying_type<EnumT>::type>(t)&0xFF),
		'\0'
	};
};

/**
	\test Verify the operation of the enum-as-char-arrays.
*/
namespace tests {

enum class enum_tags_as_strs1 : std::uint16_t {
	e1=to_tag<'0', '0'>::value,
	e2=to_tag<'1', '2'>::value,
	e3=to_tag<'3'>::value
};

static_assert((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e1>::tag)==enum_tags_as_strs1::e1, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e1>::size), ==, 2);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e1>::value[0]), ==, '0');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e1>::value[1]), ==, '0');

static_assert((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e2>::tag)==enum_tags_as_strs1::e2, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e2>::size), ==, 2);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e2>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e2>::value[1]), ==, '2');

static_assert((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e3>::tag)==enum_tags_as_strs1::e3, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e3>::size), ==, 1);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs1, enum_tags_as_strs1::e3>::value[0]), ==, '3');

enum class enum_tags_as_strs2 : uint64_t {
	e8=to_tag<'2', '.', '7', '1', '8', '2', '8', '1'>::value,
	e7=to_tag<'1', '2', '3', '4', '5', '6', '7'>::value,
	e6=to_tag<'1', '2', '3', '4', '5', '6'>::value,
	e5=to_tag<'1', '2', '3', '4', '5'>::value,
	e4=to_tag<'1', '2', '3', '4'>::value,
	e3=to_tag<'3', '1', '4'>::value,
	e2=to_tag<'3', '1'>::value,
	e1=to_tag<'3'>::value
};

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::tag)==enum_tags_as_strs2::e8, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::size), ==, 8);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[0]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[1]), ==, '.');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[2]), ==, '7');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[3]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[4]), ==, '8');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[5]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[6]), ==, '8');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e8>::value[7]), ==, '1');

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::tag)==enum_tags_as_strs2::e7, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::size), ==, 7);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::value[3]), ==, '4');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::value[4]), ==, '5');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::value[5]), ==, '6');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e7>::value[6]), ==, '7');

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::tag)==enum_tags_as_strs2::e6, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::size), ==, 6);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::value[3]), ==, '4');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::value[4]), ==, '5');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e6>::value[5]), ==, '6');

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e5>::tag)==enum_tags_as_strs2::e5, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e5>::size), ==, 5);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e5>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e5>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e5>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e5>::value[3]), ==, '4');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e5>::value[4]), ==, '5');

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e4>::tag)==enum_tags_as_strs2::e4, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e4>::size), ==, 4);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e4>::value[0]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e4>::value[1]), ==, '2');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e4>::value[2]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e4>::value[3]), ==, '4');

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e3>::tag)==enum_tags_as_strs2::e3, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e3>::size), ==, 3);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e3>::value[0]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e3>::value[1]), ==, '1');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e3>::value[2]), ==, '4');

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e2>::tag)==enum_tags_as_strs2::e2, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e2>::size), ==, 2);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e2>::value[0]), ==, '3');
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e2>::value[1]), ==, '1');

static_assert((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e1>::tag)==enum_tags_as_strs2::e1, "The two tags must be equal.");
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e1>::size), ==, 1);
BOOST_MPL_ASSERT_RELATION((to_array<enum_tags_as_strs2, enum_tags_as_strs2::e2>::value[0]), ==, '3');

}

}

} } }

#endif
