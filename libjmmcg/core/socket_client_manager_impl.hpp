/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket {

namespace asio {

template<class LkT>
template<class ConnPol>
inline client_manager<LkT>::client_manager(std::size_t min_message_size, std::size_t max_message_size, unsigned short timeout, socket_priority priority, std::size_t incoming_cpu, ConnPol const& conn_pol)
	: socket_(io_context) {
	conn_pol.operator()(
		[this](boost::asio::ip::tcp::endpoint const& endpoint) {
			socket_.connect(endpoint);
		});
	socket_.set_options(min_message_size, max_message_size, timeout, priority, incoming_cpu);
	assert(socket_.is_open());
}

template<class LkT>
template<class MsgT>
inline void
client_manager<LkT>::write(MsgT const& message) {
	socket_.write(message);
}

template<class LkT>
template<class MsgT>
inline bool
client_manager<LkT>::read(MsgT& dest) {
	return socket_.read(dest);
}

template<class LkT>
template<class V, std::size_t SrcSz>
inline bool
client_manager<LkT>::read(V (&dest)[SrcSz]) {
	return socket_.read(dest);
}

template<class LkT>
inline void
client_manager<LkT>::stop() {
	io_context.stop();
}

template<class LkT>
inline std::string
client_manager<LkT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss << socket_;
	return ss.str();
}

template<class LkT>
inline std::string
client_manager<LkT>::local_ip() const noexcept(false) {
	return socket_.local_ip();
}

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, client_manager<LkT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

}

namespace glibc {

template<class LkT>
template<class ConnPol>
inline client_manager<LkT>::client_manager(std::size_t min_message_size, std::size_t max_message_size, unsigned short timeout, socket_priority priority, std::size_t incoming_cpu, ConnPol const& conn_pol) {
	conn_pol.operator()(
		[this](boost::asio::ip::tcp::endpoint const& endpoint) {
			socket_.connect(endpoint.address().to_string().c_str(), endpoint.port());
		});
	socket_.set_options(min_message_size, max_message_size, timeout, priority, incoming_cpu);
}

template<class LkT>
template<class MsgT>
inline void
client_manager<LkT>::write(MsgT const& message) {
	socket_.write(message);
}

template<class LkT>
template<class MsgT>
inline bool
client_manager<LkT>::read(MsgT& dest) {
	return socket_.read(dest);
}

template<class LkT>
template<class V, std::size_t SrcSz>
inline bool
client_manager<LkT>::read(V (&dest)[SrcSz]) {
	return socket_.read(dest);
}

template<class LkT>
inline void
client_manager<LkT>::stop() noexcept(true) {
}

template<class LkT>
inline std::string
client_manager<LkT>::to_string() const noexcept(false) {
	std::ostringstream ss;
	ss << socket_;
	return ss.str();
}

template<class LkT>
inline std::string
client_manager<LkT>::local_ip() const noexcept(false) {
	return socket_.local_ip();
}

template<class LkT>
inline std::ostream&
operator<<(std::ostream& os, client_manager<LkT> const& ec) noexcept(false) {
	os << ec.to_string();
	return os;
}

}

}}}
