/******************************************************************************
** Copyright © 2014 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace lock {

	template<class LkT> REALLY_FORCE_INLINE inline __fastcall
	anon_spin_event<LkT>::anon_spin_event() noexcept(true) {
	}

	template<class LkT> REALLY_FORCE_INLINE inline __fastcall
	anon_spin_event<LkT>::anon_spin_event(const atomic_state_type state) noexcept(true) {
		if (state==atomic_state_type::atom_set) {
			set();
		}
	}

	template<class LkT>
	REALLY_FORCE_INLINE inline typename anon_spin_event<LkT>::atomic_state_type __fastcall
	anon_spin_event<LkT>::set() noexcept(true) {
		// Wait until the state is set to unset, so we are guaranteed to claim it.
		while (state.test_and_set(std::memory_order_seq_cst)) {
			// This is not atomic, but C++20 is awful, so we need to double-check, of course this will not work as another thread could make this true... But still we seem to need it. FFS.
			while (state.test(std::memory_order_relaxed)) {
				// TODO: I do not use "_mm_pause();" because my test revealed that it takes 100+/-5% nsec versus 55+/-4% nsec for "yield()" on my AMD Opteron 4180s.
				std::this_thread::yield();
			}
		}
		return atomic_state_type::atom_set;
	}

	template<class LkT>
	REALLY_FORCE_INLINE inline typename anon_spin_event<LkT>::lock_result_type __fastcall
	anon_spin_event<LkT>::unlock() noexcept(true) {
		state.clear(std::memory_order_release);
		return atomic_state_type::atom_unset;
	}

	template<class LkT>
	REALLY_FORCE_INLINE inline typename anon_spin_event<LkT>::atomic_state_type __fastcall
	anon_spin_event<LkT>::reset() noexcept(true) {
		return unlock();
	}

	template<class LkT>
	REALLY_FORCE_INLINE inline typename anon_spin_event<LkT>::lock_result_type __fastcall
	anon_spin_event<LkT>::lock() noexcept(true) {
		return set();
	}

	template<class LkT>
	REALLY_FORCE_INLINE inline typename anon_spin_event<LkT>::lock_result_type __fastcall
	anon_spin_event<LkT>::lock(const timeout_type) noexcept(true) {
		return lock();
	}

} } } }
