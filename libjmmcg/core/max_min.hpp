#ifndef LIBJMMCG_CORE_MIN_MAX_HPP
#define LIBJMMCG_CORE_MIN_MAX_HPP

/******************************************************************************
** Copyright © 2013 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class Val, Val l, Val r>
struct min {
	enum : Val {
		value= (l < r ? l : r)
	};
};
template<class Val, Val l, Val r>
struct max {
	enum : Val {
		value= (l > r ? l : r)
	};
};

/// Work around the fact that there is no parameter-pack version of std::max(...). *sigh* C++ is great, no?
/**
	See <a href="http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2008/n2772.pdf">Variadic functions: Variadic templates or initializer lists? -- Revision 1</a>.
*/
template<class V>
[[nodiscard]] inline constexpr V
varadic_max(V v) noexcept(true) {
	return v;
}
template<class V, class... Values>
[[nodiscard]] inline constexpr V
varadic_max(V v, Values... values) noexcept(true) {
	return std::max(varadic_max(values...), v);
}

template<class V>
[[nodiscard]] inline constexpr V
varadic_min(V v) noexcept(true) {
	return v;
}
template<class V, class... Values>
[[nodiscard]] inline constexpr V
varadic_min(V v, Values... values) noexcept(true) {
	return std::min(varadic_min(values...), v);
}

}}

#endif
