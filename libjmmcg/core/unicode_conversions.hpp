#ifndef LIBJMMCG_CORE_UNICODE_CONVERSIONS_HPP
#define LIBJMMCG_CORE_UNICODE_CONVERSIONS_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/
// DESCRIPTION:
// Utilities to convert from Unicode to ASCII and back. Also functions that
// do the conversion or nothing, depending upon the definition of "_UNICODE".

#include "ttypes.hpp"

#include <locale>
#include <memory>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	struct widen {
		widen()
		: ct(std::use_facet<std::ctype<wchar_t> >(std::locale())) {
		}
		wchar_t operator()(char c) const {
			return ct.widen(c);
		}

	private:
		std::ctype<wchar_t> const & ct;
	};

	struct narrow {
		narrow()
		: ct(std::use_facet<std::ctype<wchar_t> >(std::locale())) {
		}
		char operator()(wchar_t c) const {
			return ct.narrow(c,' ');
		}

	private:
		std::ctype<wchar_t> const & ct;
	};

	inline std::wstring __fastcall
	StringToWString(const std::string &in) {
		std::wstring ret(in.size(), std::wstring::value_type(0));
		// M$ Broke the declaration of "std::use_facet()" by adding extra, non-default arguments...
		// So we have to hack around it...
#if defined(_MSC_VER) && (_MSC_VER < 1300)
		std::ctype<wchar_t>().widen(in.data(),in.data()+in.size(),&(*ret.begin()));
#else
		std::transform(in.begin(),in.end(),ret.begin(),widen());
#endif	//	_MSC_VER
		return ret;
	}

	inline std::string __fastcall
	WStringToString(
		const std::wstring &in
#if defined(_MSC_VER) && (_MSC_VER < 1300)
		,const char dflt='@'
#endif	//	_MSC_VER
	) {
		std::string ret(in.size(), std::string::value_type(0));
		// M$ Broke the declaration of "std::use_facet()" by adding extra, non-default arguments...
		// So we have to hack around it...
#if defined(_MSC_VER) && (_MSC_VER < 1300)
		std::ctype<wchar_t>().narrow(in.data(),in.data()+in.size(),dflt,&(*ret.begin()));
#else
		std::transform(in.begin(),in.end(),ret.begin(),narrow());
#endif	//	_MSC_VER
		return ret;
	}

	inline tstring __fastcall
	WStringToTString(const std::wstring &in)
#ifdef _UNICODE
	noexcept(true) {
		return in;
#else
	{
		return WStringToString(in);
#endif	// _UNICODE
	}

	inline std::wstring __fastcall
	TStringToWString(const tstring &in)
#ifdef _UNICODE
	noexcept(true) {
		return in;
#else
	{
		return StringToWString(in);
#endif	// _UNICODE
	}

	inline tstring __fastcall
	StringToTString(const std::string &in)
#ifdef _UNICODE
	{
		return StringToWString(in);
#else
	noexcept(true) {
		return in;
#endif	// _UNICODE
	}

	inline std::string __fastcall
	TStringToString(const tstring &in)
#ifdef _UNICODE
	{
		return WStringToString(in);
#else
	noexcept(true) {
		return in;
#endif	// _UNICODE
	}

} }

#ifdef _MSC_VER

#pragma warning(default:4284)

#endif	//	_MSC_VER

#endif
