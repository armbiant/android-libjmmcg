/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include <boost/mpl/assert.hpp>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

	template<class V, class LkT> inline typename unique_ptr<V, LkT>::atomic_ptr_t
	unique_ptr<V, LkT>::release() noexcept(true) {
		atomic_ptr_t tmp(nullptr);
		tmp.swap(this->data_);
		return tmp;
	}

	template<class V, class LkT> inline void
	unique_ptr<V, LkT>::reset() noexcept(true) {
		unique_ptr tmp;
		tmp.swap(*this);
	}

	template<class V, class LkT> inline void
	unique_ptr<V, LkT>::swap(unique_ptr &s) noexcept(true) {
		data_.swap(s.data_);
	}

	template<class V, class LkT>
	template<class V1> inline void
	unique_ptr<V, LkT>::swap(unique_ptr<V1, LkT> &s) noexcept(true) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V1>));
		data_.swap(s.data_);
	}

	template<class V, class LkT>
	template<class V2, class LkT2> inline void
	unique_ptr<V, LkT>::swap(unique_ptr<V2, LkT2> &s) noexcept(true) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V2>));
		data_.swap(s.data_);
	}

	template<class V, class LkT> inline constexpr
	unique_ptr<V, LkT>::unique_ptr(value_type *ptr) noexcept(true)
	: data_(ptr) {
	}

	template<class V, class LkT>
	template<class V1>
	inline
	unique_ptr<V, LkT>::unique_ptr(V1 *ptr) noexcept(true)
	: data_(ptr) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V1>));
	}

	template<class V, class LkT> inline
	unique_ptr<V, LkT>::unique_ptr(atomic_ptr_t &&ptr) noexcept(true)
	: data_(nullptr) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, typename atomic_ptr_t::value_type>));
		data_.swap(ptr);
	}

	template<class V, class LkT>
	template<class V1, template<class> class At>
	inline
	unique_ptr<V, LkT>::unique_ptr(At<V1*> &&ptr) noexcept(true)
	: data_(nullptr) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V1>));
		if (ptr.get()) [[likely]] {
			assert(dynamic_cast<V1 *>(ptr.get()));
			data_.swap(ptr);
			assert(dynamic_cast<value_type *>(data_.get()));
		}
	}

	template<class V, class LkT> inline constexpr
	unique_ptr<V, LkT>::unique_ptr(std::unique_ptr<value_type, deleter_t> &&d) noexcept(true)
	: data_(d.release()) {
	}

	template<class V, class LkT>
	template<class V1>
	inline
	unique_ptr<V, LkT>::unique_ptr(std::unique_ptr<V1, typename V1::deleter_t> &&d) noexcept(true)
	: data_(d.release()) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V1>));
		// I want to test that if the dtor is dynamic alloc, then the V1::element_type is derived from value_type, otherwise if placement, the dtor must be the same, otherwise if noop_dtor, don't care.
// This is too stringent a test, as it ignores virtual dtors:		BOOST_MPL_ASSERT((std::is_same<deleter_t, typename V1::deleter_t>));
//		static_assert(std::is_base_of<deleter_t, typename V1::deleter_t>::value || std::is_base_of<typename V1::deleter_t, deleter_t>::value, "TODO");
	}

	template<class V, class LkT> inline constexpr
	unique_ptr<V, LkT>::unique_ptr(unique_ptr &&s) noexcept(true)
	: data_(s.release()) {
	}

	template<class V, class LkT>
	template<typename V2, class LkT2> inline
	unique_ptr<V, LkT>::unique_ptr(unique_ptr<V2, LkT2> &&s) noexcept(true)
	: data_(s.release()) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V2>));
	}

	template<class V, class LkT> inline
	unique_ptr<V, LkT>::~unique_ptr() noexcept(true) {
		atomic_ptr_t tmp(nullptr);
		tmp.swap(this->data_);
		if (tmp) {
			tmp->deleter();
		}
	}

	template<class V, class LkT>
	template<typename V2, class LkT2> inline void
	unique_ptr<V, LkT>::operator=(unique_ptr<V2, LkT2> &&s) noexcept(true) {
		BOOST_MPL_ASSERT((std::is_base_of<value_type, V2>));
		reset();
		data_=s.release();
	}

	template<class V, class LkT> inline void
	unique_ptr<V, LkT>::operator=(unique_ptr<V, LkT> &&s) noexcept(true) {
		reset();
		data_=s.release();
	}

	template<class V, class LkT> constexpr inline bool
	unique_ptr<V, LkT>::operator<(const unique_ptr &s) const noexcept(true) {
		return (data_.get() && s.data_.get()) ? (*data_.get()<*s.data_.get()) : false;
	}
	template<class V, class LkT> constexpr inline bool
	unique_ptr<V, LkT>::operator==(const unique_ptr &s) const noexcept(true) {
		return !((data_<s.data_) || (s.data_<data_));
	}
	template<class V, class LkT> inline constexpr bool
	unique_ptr<V, LkT>::operator!=(const unique_ptr &s) const noexcept(true) {
		return !(*this==s);
	}
	template<class V, class LkT> constexpr inline
	unique_ptr<V, LkT>::operator bool() const noexcept(true) {
		return dynamic_cast<value_type const *>(data_.get())!=nullptr;
	}
	template<class V, class LkT> constexpr inline bool
	unique_ptr<V, LkT>::operator>(const unique_ptr &s) const noexcept(true) {
		return (data_!=s.data_) && (s.data_<data_);
	}

	template<class V, class LkT> constexpr inline typename unique_ptr<V, LkT>::atomic_ptr_t const &
	unique_ptr<V, LkT>::get() const noexcept(true) {
		return data_;
	}
	template<class V, class LkT> inline typename unique_ptr<V, LkT>::atomic_ptr_t &
	unique_ptr<V, LkT>::get() noexcept(true) {
		return data_;
	}
	template<class V, class LkT> constexpr inline typename unique_ptr<V, LkT>::value_type const &
	unique_ptr<V, LkT>::operator*() const noexcept(true) {
		assert(dynamic_cast<value_type const *>(data_.get()));
		return *data_.get();
	}
	template<class V, class LkT> inline typename unique_ptr<V, LkT>::value_type &
	unique_ptr<V, LkT>::operator*() noexcept(true) {
		assert(dynamic_cast<value_type *>(data_.get()));
		return *data_.get();
	}
	template<class V, class LkT> constexpr inline typename unique_ptr<V, LkT>::value_type const *
	unique_ptr<V, LkT>::operator->() const noexcept(true) {
		return data_.get();
	}
	template<class V, class LkT> inline typename unique_ptr<V, LkT>::value_type *
	unique_ptr<V, LkT>::operator->() noexcept(true) {
		return data_.get();
	}

	template<class V, class LkT> inline tstring
	unique_ptr<V, LkT>::to_string() const noexcept(false) {
		tostringstream os;
		os<<"data_="<<data_.get();
		if (dynamic_cast<value_type *>(data_.get())) [[likely]] {
			os<<", data: "<<data_->to_string()
				<<", type: "<<boost::core::demangle(typeid(unique_ptr).name());
		}
		return os.str();
	}

} }
