/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

// Implementation details..... Don't look below here!!! ;)

//	"wrapper" implementation...
/* Can't delete a sequential-traits thread.
	template<class Mutator, generic_traits::api_type::element_type API, class TWC> inline void
	wrapper<Mutator, API, sequential_mode, TWC>::wait_thread_exit() {
	}
*/
template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
template<class... MutatorCtorArgs>
inline wrapper<Mutator, API, Mdl, TWC>::wrapper(MutatorCtorArgs&&... args) noexcept(false)
	: base_t(exit_wait), mutator_(std::forward<MutatorCtorArgs>(args)...) {
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
template<class... MutatorCtorArgs>
inline wrapper<Mutator, API, Mdl, TWC>::wrapper(thread_context_t&& thread_context, MutatorCtorArgs&&... args) noexcept(false)
	: base_t(exit_wait), thread_context_(std::move(thread_context)), mutator_(std::forward<MutatorCtorArgs>(args)...) {
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline wrapper<Mutator, API, Mdl, TWC>::wrapper(const wrapper& tb) noexcept(true)
	: base_t(tb), thread_context_(tb.thread_context_) {
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline void
wrapper<Mutator, API, Mdl, TWC>::request_exit() const noexcept(true) {
	exit_requested.test_and_set(std::memory_order_seq_cst);
	exit_requested.notify_all();
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline bool
wrapper<Mutator, API, Mdl, TWC>::pre_exit() noexcept(false) {
	return exit_requested.test(std::memory_order_relaxed);
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline typename wrapper<Mutator, API, Mdl, TWC>::thread_traits::api_params_type::states::element_type
wrapper<Mutator, API, Mdl, TWC>::process() noexcept(false) {
	if constexpr(std::is_same<thread_context_t, null_thread_worker_context>::value) {
		do {
			mutator_.operator()(std::reference_wrapper<std::atomic_flag>(exit_requested));
		} while(!this->pre_exit());
	} else {
		thread_context_t obj(std::move(thread_context_));
		if constexpr(std::is_member_function_pointer<decltype(&thread_context_t::operator())>::value) {
			obj.operator()(std::reference_wrapper<wrapper>(*this));
		}
		do {
			mutator_.operator()(std::reference_wrapper<std::atomic_flag>(exit_requested), std::reference_wrapper<thread_context_t>(obj));
		} while(!this->pre_exit());
	}
	return thread_traits::api_params_type::states::no_kernel_thread;
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline wrapper<Mutator, API, Mdl, TWC>::~wrapper() noexcept(false) {
	request_exit();
	base_t::wait_thread_exit();
	// See how I've tried to clean up everything before we might throw here?
	// Yeah - well - apart from all of the base class stuff, etc, etc. So it's really rather bad...
	this->exception_thrown_in_thread.throw_if_set();
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline typename wrapper<Mutator, API, Mdl, TWC>::mutator_t const*
wrapper<Mutator, API, Mdl, TWC>::operator->() const noexcept(true) {
	return &mutator_;
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline typename wrapper<Mutator, API, Mdl, TWC>::mutator_t*
wrapper<Mutator, API, Mdl, TWC>::operator->() noexcept(true) {
	return &mutator_;
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline typename wrapper<Mutator, API, Mdl, TWC>::mutator_t const&
wrapper<Mutator, API, Mdl, TWC>::operator*() const noexcept(true) {
	return mutator_;
}

template<class Mutator, generic_traits::api_type::element_type API, class Mdl, class TWC>
inline typename wrapper<Mutator, API, Mdl, TWC>::mutator_t&
wrapper<Mutator, API, Mdl, TWC>::operator*() noexcept(true) {
	return mutator_;
}

}}}
