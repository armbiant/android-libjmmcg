#ifndef LIBJMMCG_CORE_LATENCY_TIMESTAMPS_HPP
#define LIBJMMCG_CORE_LATENCY_TIMESTAMPS_HPP

/******************************************************************************
** Copyright © 2019 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "filename.hpp"
#include "hp_timer.hpp"
#include "non_allocatable.hpp"

#include <iosfwd>
#include <memory>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

class latency_timestamps_itf {
public:
	using size_type=std::size_t;
	/**
		Experimentation has indicated that using cpu_timer::out_of_order:
		-# No statistical difference in performance, even with no time-stamping at all.
		-# Worse statistics than the method chosen.

		\see cpu_timer::out_of_order
	*/
	using timer_t=cpu_timer::in_order;
	struct timestamp {
		using element_type=cpu_timer::element_type;
		/// The raw timestamp from the selected timer_t.
		/**
			\see timer_t
		 */
		element_type start{};
		/// The raw timestamp from the selected timer_t.
		/**
			\see timer_t
		 */
		element_type end{};

		bool is_valid() const noexcept(true);
		/// Write a timestamp to the output stream in CSV format.
		/**
			The output timestamps are in microseconds since the start of the program.
		*/
		void to_csv(std::ostream &os) const noexcept(false);
	};
	using element_type=timestamp;
	/**
		Designed to assign to the huge collection of timestamps in the dtor, to avoid using it in the hot-path.
	*/
	class ALIGN_TO_L1_CACHE period final : private non_newable {
	public:
		REALLY_FORCE_INLINE [[nodiscard]] explicit period(latency_timestamps_itf &lts) noexcept(true);
		REALLY_FORCE_INLINE ~period() noexcept(true);

	private:
		element_type ts;
		latency_timestamps_itf &lts_;
	};

	virtual ~latency_timestamps_itf()=default;

	virtual void push_back(element_type const &ts) noexcept(true)=0;

protected:
	latency_timestamps_itf()=default;
};

/// A simple class that is used to create a CSV-formatted file of timestamps, from which a histogram might be generated.
/**
	Compatible with no_latency_timestamps.

	\see no_latency_timestamps
*/
class latency_timestamps final : public latency_timestamps_itf {
public:
	[[nodiscard]] explicit latency_timestamps(size_type num_tses) noexcept(false);

	size_type size() const noexcept(true);
	/// Add a timestamp on the hot-path.
	/**
		As this is on the hot-path minimal processing is done. The location to be written to is loaded using a technique that minimises cache usage: this might be slightly slower, but attempts to avoid overwriting the entire contents of each cache in the hierarchy.
		Note that this function is thread-safe & re-entrant safe.
	 */
	REALLY_FORCE_INLINE void push_back(element_type const &ts) noexcept(true) override;

	/// Write the timestamps, if any, to the output-stream.
	/**
		\param	os	The output stream to which the timestamps, in CSV format, should be written.
	*/
	NEVER_INLINE void to_csv(std::ostream &os) const noexcept(false);
	/// Wrtie the timestamps, if any, to a CSV file.
	/**
		\param	os	Write log output to this output stream.
		\param	root	Write the timestamps, in CSV format, to a file with the filename root as specified. Unique identifying text will be appended and ".csv" shall be the extension.
		
		\see make_filename()
	*/
	NEVER_INLINE void write_to_csv_file(std::ostream &os, char const * const root) const noexcept(false);
	/// Wrtie the timestamps, if any, to the named CSV file.
	/**
		\param	os	Write log output to this output stream.
		\param	base_filename	Write the timestamps, in CSV format, to a file with the filename root as specified, the extension ".csv" will be appended.
	*/
	NEVER_INLINE void write_to_named_csv_file(std::ostream &os, std::string const &base_filename) const noexcept(false);

private:
	/**
		Ideally this should be allocated in non-cacheable memory, as it is only written to once on the hot path.
	*/
	std::unique_ptr<element_type[]> timestamps;
	std::atomic<element_type*> current_timestamp;
	const size_type num_timestamps;

	void write_to_csv_file_fullpath(std::ostream &os, std::string const &filename) const noexcept(false);
};

/// A simple class that is used to create no timestamps.
/**
	Compatible with latency_timestamps.

	\see latency_timestamps
*/
class no_latency_timestamps final : public latency_timestamps_itf {
public:
	[[nodiscard]] explicit no_latency_timestamps(std::size_t) noexcept(true) {}

	constexpr size_type size() const noexcept(true) {return 0;}
	void push_back(element_type const &) noexcept(true) override {}

	void to_csv(std::ostream &) const noexcept(true) {}
	void write_to_csv_file(std::ostream &, char const * const) const noexcept(true) {}
	void write_to_csv_file(std::ostream &, std::string const &) const noexcept(true) {}
};

} }

#include "latency_timestamps_impl.hpp"

#endif
