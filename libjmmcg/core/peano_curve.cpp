/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "peano_curve.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

const PeanoCurve::dir_replacements PeanoCurve::replacements[]={
	// North replacements.
	{
		// Replace "green_right_curve".
		{
			PeanoCurve::curve_element(green_left_curve,south),
			PeanoCurve::curve_element(red_right_curve,west),
			PeanoCurve::curve_element(green_right_curve,north),
			PeanoCurve::curve_element(red_straight,north)
		},
		// Replace "red_right_curve".
		{
			PeanoCurve::curve_element(red_straight,west),
			PeanoCurve::curve_element(red_right_curve,north),
			PeanoCurve::curve_element(green_right_curve,east),
			PeanoCurve::curve_element(red_left_curve,north),
		},
		// Replace "green_left_curve".
		{
			PeanoCurve::curve_element(green_straight,east),
			PeanoCurve::curve_element(green_left_curve,north),
			PeanoCurve::curve_element(red_left_curve,west),
			PeanoCurve::curve_element(green_right_curve,north)
		},
		// Replace "red_left_curve".
		{
			PeanoCurve::curve_element(red_right_curve,south),
			PeanoCurve::curve_element(green_left_curve,east),
			PeanoCurve::curve_element(red_left_curve,north),
			PeanoCurve::curve_element(green_straight,north)
		},
		// Replace "green_straight".
		{
			PeanoCurve::curve_element(green_left_curve,west),
			PeanoCurve::curve_element(red_right_curve,north),
			PeanoCurve::curve_element(green_right_curve,east),
			PeanoCurve::curve_element(red_left_curve,north)
		},
		// Replace "red_straight".
		{
			PeanoCurve::curve_element(red_right_curve,east),
			PeanoCurve::curve_element(green_left_curve,north),
			PeanoCurve::curve_element(red_left_curve,west),
			PeanoCurve::curve_element(green_right_curve,north)
		}
	},
	// East replacements.
	{
		// Replace "green_right_curve".
		{
			PeanoCurve::curve_element(green_left_curve,west),
			PeanoCurve::curve_element(red_right_curve,north),
			PeanoCurve::curve_element(green_right_curve,east),
			PeanoCurve::curve_element(red_straight,east)
		},
		// Replace "red_right_curve".
		{
			PeanoCurve::curve_element(red_straight,north),
			PeanoCurve::curve_element(red_right_curve,east),
			PeanoCurve::curve_element(green_right_curve,south),
			PeanoCurve::curve_element(red_left_curve,east),
		},
		// Replace "green_left_curve".
		{
			PeanoCurve::curve_element(green_straight,south),
			PeanoCurve::curve_element(green_left_curve,east),
			PeanoCurve::curve_element(red_left_curve,north),
			PeanoCurve::curve_element(green_right_curve,east)
		},
		// Replace "red_left_curve".
		{
			PeanoCurve::curve_element(red_right_curve,west),
			PeanoCurve::curve_element(green_left_curve,south),
			PeanoCurve::curve_element(red_left_curve,east),
			PeanoCurve::curve_element(green_straight,east)
		},
		// Replace "green_straight".
		{
			PeanoCurve::curve_element(green_left_curve,north),
			PeanoCurve::curve_element(red_right_curve,east),
			PeanoCurve::curve_element(green_right_curve,south),
			PeanoCurve::curve_element(red_left_curve,east)
		},
		// Replace "red_straight".
		{
			PeanoCurve::curve_element(red_right_curve,south),
			PeanoCurve::curve_element(green_left_curve,east),
			PeanoCurve::curve_element(red_left_curve,north),
			PeanoCurve::curve_element(green_right_curve,east)
		}
	},
	// South replacements.
	{
		// Replace "green_right_curve".
		{
			PeanoCurve::curve_element(green_left_curve,north),
			PeanoCurve::curve_element(red_right_curve,east),
			PeanoCurve::curve_element(green_right_curve,south),
			PeanoCurve::curve_element(red_straight,south)
		},
		// Replace "red_right_curve".
		{
			PeanoCurve::curve_element(red_straight,east),
			PeanoCurve::curve_element(red_right_curve,south),
			PeanoCurve::curve_element(green_right_curve,west),
			PeanoCurve::curve_element(red_left_curve,south),
		},
		// Replace "green_left_curve".
		{
			PeanoCurve::curve_element(green_straight,west),
			PeanoCurve::curve_element(green_left_curve,south),
			PeanoCurve::curve_element(red_left_curve,east),
			PeanoCurve::curve_element(green_right_curve,south)
		},
		// Replace "red_left_curve".
		{
			PeanoCurve::curve_element(red_right_curve,north),
			PeanoCurve::curve_element(green_left_curve,west),
			PeanoCurve::curve_element(red_left_curve,south),
			PeanoCurve::curve_element(green_straight,south)
		},
		// Replace "green_straight".
		{
			PeanoCurve::curve_element(green_left_curve,east),
			PeanoCurve::curve_element(red_right_curve,south),
			PeanoCurve::curve_element(green_right_curve,west),
			PeanoCurve::curve_element(red_left_curve,south)
		},
		// Replace "red_straight".
		{
			PeanoCurve::curve_element(red_right_curve,west),
			PeanoCurve::curve_element(green_left_curve,south),
			PeanoCurve::curve_element(red_left_curve,east),
			PeanoCurve::curve_element(green_right_curve,south)
		}
	},
	// West replacements.
	{
		// Replace "green_right_curve".
		{
			PeanoCurve::curve_element(green_left_curve,east),
			PeanoCurve::curve_element(red_right_curve,south),
			PeanoCurve::curve_element(green_right_curve,west),
			PeanoCurve::curve_element(red_straight,west)
		},
		// Replace "red_right_curve".
		{
			PeanoCurve::curve_element(red_straight,south),
			PeanoCurve::curve_element(red_right_curve,west),
			PeanoCurve::curve_element(green_right_curve,north),
			PeanoCurve::curve_element(red_left_curve,west),
		},
		// Replace "green_left_curve".
		{
			PeanoCurve::curve_element(green_straight,north),
			PeanoCurve::curve_element(green_left_curve,west),
			PeanoCurve::curve_element(red_left_curve,south),
			PeanoCurve::curve_element(green_right_curve,west)
		},
		// Replace "red_left_curve".
		{
			PeanoCurve::curve_element(red_right_curve,east),
			PeanoCurve::curve_element(green_left_curve,north),
			PeanoCurve::curve_element(red_left_curve,west),
			PeanoCurve::curve_element(green_straight,west)
		},
		// Replace "green_straight".
		{
			PeanoCurve::curve_element(green_left_curve,south),
			PeanoCurve::curve_element(red_right_curve,west),
			PeanoCurve::curve_element(green_right_curve,north),
			PeanoCurve::curve_element(red_left_curve,west)
		},
		// Replace "red_straight".
		{
			PeanoCurve::curve_element(red_right_curve,north),
			PeanoCurve::curve_element(green_left_curve,west),
			PeanoCurve::curve_element(red_left_curve,south),
			PeanoCurve::curve_element(green_right_curve,west)
		}
	}
};	

} }
