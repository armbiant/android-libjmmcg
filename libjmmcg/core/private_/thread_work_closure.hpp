/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "../../core/intrusive.hpp"
#include "../../core/memory_buffer.hpp"
#include "../../core/thread_os_traits.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

/// Classes related to implementing work within the thread_pool as a closure.
namespace closure {

/// The base-class of the closures used in the thread_pool::signalled_work_queue.
/**
	Note that pointers to this class are also used to within the control-flow graph implementation to obtain the vertex() of the node and update the edges into and out of the vertex().

	\see thread_wk_base, control_flow_graph, no_control_flow_graph
*/
template<
	class CFG	///< The type of the control-flow graph to be created.
	>
class closure_base {
public:
	typedef CFG cfg_type;
	typedef typename cfg_type::template add_cfg_details<closure_base> cfg_details_type;

	/// This function could be overridden in the user's work class to provide an indication of the time it would take to process the transferred closure_base-derived closure. This would only make sense if the pool_aspect was instantiated with a trait of pool_traits::prioritised_queue, and the closure_base-derived closure has the correct operator<() declared for it.
	/**
		\todo This is only an idea at the moment, and needs to be implemented with test cases. Obviously we'd need to be able to adjust this to ensure that all work gets eventually processed. It might make sense to provide this as a new pool_trait, that would automatically supply the correct operator<(). Have a look at the theories in "Parallel Algorithms" by H.Casanova.
	*/
	constexpr int response_time() const noexcept(true) {
		return 0;
	}

	void update_edge(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) {
		cfg_details.update_edge(e_details);
	}
	void delete_edge() noexcept(false) {
		cfg_details.delete_edge();
	}
	void checkpoint_cfg() noexcept(false) {
		cfg_details.checkpoint_cfg();
	}
	typename cfg_type::vertex_t const vertex() const noexcept(true) {
		return cfg_details.vertex();
	}

protected:
	explicit closure_base(typename cfg_details_type::params const& p) noexcept(noexcept(cfg_details_type(std::declval<typename cfg_details_type::params>())))
		: cfg_details(p) {
	}
	/**
		Not deleted polymorphically, but must be polymorphic to allow input work to be dynamically cast to this type to gain access to the functions for updating the control-flow graph.
	*/
	virtual ~closure_base() noexcept(true) {}

private:
	cfg_details_type cfg_details;
};

/// The wrapper for the input work that the user wishes to have mutated.
/**
	The input work to be transferred must be:
	- copy-constructible or move-constructible,
	- may have a member typedef of result_type for declaring the result of the mutation and
	- the mutation function may be specified with either
		- a member function "void __fastcall process(result_type &) noexcept(false)" or,
		- a member function "void __fastcall process(result_type &) const noexcept(false)".

	\see thread_wk_base, control_flow_graph, no_control_flow_graph
*/
template<
	class InpWk,	///< The type of the input work.
	bool NoExcept,
	void (__fastcall InpWk::*Proc)() noexcept(NoExcept),	 ///< The mutator function, by default InpWk::process().
	class CFG	///< The type of the control-flow graph to be created.
	>
class closure_void_static final : public InpWk, public closure_base<CFG> {
public:
	typedef closure_base<CFG> base_t;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef void result_type;	 ///< The result-type of the mutation, to assist in making the derived types look like a std::unary_function.
	typedef InpWk argument_type;	 ///< The argument-type of the mutation, to assist in making the derived types look like a std::unary_function.

	__stdcall closure_void_static(argument_type&& i, typename cfg_details_type::params const& p) noexcept(false);
	~closure_void_static() noexcept(true) {}

	bool __fastcall operator==(const closure_void_static&) const noexcept(true);
	bool __fastcall operator<(const closure_void_static&) const noexcept(true);

	argument_type const& __fastcall input() const noexcept(true);
	argument_type& __fastcall input() noexcept(true);

	/// Process the wrapped work.
	/**
		By default, all exceptions derived from std::exception are caught and wrapped, to be potentially transferred to the client. Also a "catch-all" block traps any other unknown exceptions, that are also potentially transferred. It is a grave programmatic error to allow a thread_pool with un-emitted exceptions to execute it's destructor. To avoid this potential for undefined behaviour, if process()ing the closure_void_static is likely to result in an exception being thrown, the user should capture the result of the transfer in an execution_context and dereference it, when the exception will be safely re-thrown, and the user will have an opportunity to correctly catch it and deal with it.

	\todo We really want to compute the noexcept() from the Proc member fn.
	typedef typename std::remove_pointer<process_ptr_t>::type process_t; - This doesn't seem to remove the pointer.
	BOOST_MPL_ASSERT((std::is_same<process_t, int>));

		\see thread_wrapper, thread_pool, thread_pool_type
	*/
	void __fastcall process();

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream& __fastcall operator<<(tostream& os, closure_void_static const& w) noexcept(false) {
		os
			<< _T("work=0x") << &w
			<< _T(", type: ") << boost::core::demangle(typeid(w).name())
			<< _T(", mutator: ") << boost::core::demangle(typeid(Proc).name())
			<< _T(", argument_type: ") << boost::core::demangle(typeid(argument_type).name())
			// TODO				<<_T(", input: ")<<static_cast<argument_type const &>(w)
			<< _T(", result_type: void");
		return os;
	}
};

/// The wrapper for the input work that the user wishes to have mutated.
/**
	The input work to be transferred must be:
	- copy-constructible or move-constructible,
	- may have a member typedef of result_type for declaring the result of the mutation and
	- the mutation function may be specified with either
		- a member function "void __fastcall process(result_type &) noexcept(false)" or,
		- a member function "void __fastcall process(result_type &) const noexcept(false)".

	\see thread_wk_base, control_flow_graph, no_control_flow_graph
*/
template<
	class InpWk,	///< The type of the input work.
	bool NoExcept,
	void (__fastcall InpWk::*Proc)() const noexcept(NoExcept),	 ///< The mutator function, by default InpWk::process().
	class CFG	///< The type of the control-flow graph to be created.
	>
class closure_void_static_const final : public InpWk, public closure_base<CFG> {
public:
	typedef closure_base<CFG> base_t;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef void result_type;	 ///< The result-type of the mutation, to assist in making the derived types look like a std::unary_function.
	typedef InpWk argument_type;	 ///< The argument-type of the mutation, to assist in making the derived types look like a std::unary_function.

	__stdcall closure_void_static_const(argument_type&& i, typename cfg_details_type::params const& p) noexcept(false);
	~closure_void_static_const() noexcept(true) {}

	bool __fastcall operator==(const closure_void_static_const&) const noexcept(true);
	bool __fastcall operator<(const closure_void_static_const&) const noexcept(true);

	argument_type const& __fastcall input() const noexcept(true);
	argument_type& __fastcall input() noexcept(true);

	/// Process the wrapped work.
	/**
		By default, all exceptions derived from std::exception are caught and wrapped, to be potentially transferred to the client. Also a "catch-all" block traps any other unknown exceptions, that are also potentially transferred. It is a grave programmatic error to allow a thread_pool with un-emitted exceptions to execute it's destructor. To avoid this potential for undefined behaviour, if process()ing the closure_void_static_const is likely to result in an exception being thrown, the user should capture the result of the transfer in an execution_context and dereference it, when the exception will be safely re-thrown, and the user will have an opportunity to correctly catch it and deal with it.

		\see thread_wrapper, thread_pool, thread_pool_type
	*/
	void __fastcall process();

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream& __fastcall operator<<(tostream& os, closure_void_static_const const& w) noexcept(false) {
		os
			<< _T("work=0x") << &w
			<< _T(", type: ") << boost::core::demangle(typeid(w).name())
			<< _T(", mutator: ") << boost::core::demangle(typeid(Proc).name())
			<< _T(", argument_type: ") << boost::core::demangle(typeid(argument_type).name())
			// TODO				<<_T(", input: ")<<static_cast<argument_type const &>(w)
			<< _T(", result_type: void");
		return os;
	}
};

/// The wrapper for the input work that the user wishes to have mutated.
/**
	The input work to be transferred must be:
	- copy-constructible or move-constructible,
	- may have a member typedef of result_type for declaring the result of the mutation and
	- the mutation function may be specified with either
		- a member function "void __fastcall process(result_type &) noexcept(false)" or,
		- a member function "void __fastcall process(result_type &) const noexcept(false)".

	\see thread_wk_base, no_control_flow_graph
*/
template<
	class InpWk,	///< The type of the input work.
	class Res,	 ///< The result type of the mutation.
	bool NoExcept,
	void (__fastcall InpWk::*Proc)(Res&) noexcept(NoExcept),	  ///< The mutator function, by default InpWk::process(Res &).
	class CFG	///< The type of the control-flow graph to be created.
	>
class closure_static final : public InpWk, public closure_base<CFG> {
public:
	typedef closure_base<CFG> base_t;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef Res result_type;	///< The result-type of the mutation, to assist in making the derived types look like a std::unary_function.
	typedef InpWk argument_type;	 ///< The argument-type of the mutation, to assist in making the derived types look like a std::unary_function.

	explicit __stdcall closure_static(argument_type&& i, typename cfg_details_type::params const& p) noexcept(false);
	__stdcall ~closure_static() noexcept(true) {}

	bool __fastcall operator==(const closure_static&) const noexcept(true);
	bool __fastcall operator<(const closure_static&) const noexcept(true);

	argument_type const& __fastcall input() const noexcept(true);
	argument_type& __fastcall input() noexcept(true);

	/// Process the wrapped work.
	/**
		By default, all exceptions derived from std::exception are caught and wrapped, to be potentially transferred to the client. Also a "catch-all" block traps any other unknown exceptions, that are also potentially transferred. It is a grave programmatic error to allow a thread_pool with un-emitted exceptions to execute it's destructor. To avoid this potential for undefined behaviour, if process()ing the closure_static is likely to result in an exception being thrown, the user should capture the result of the transfer in an execution_context and dereference it, when the exception will be safely re-thrown, and the user will have an opportunity to correctly catch it and deal with it.

		\see thread_wrapper, thread_pool, thread_pool_type
	*/
	void __fastcall process();

	/// Return a reference to the wrapped work.
	const result_type& __fastcall get_results() const noexcept(false);
	/// Return a reference to the wrapped work.
	result_type& __fastcall get_results() noexcept(false);

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream& __fastcall operator<<(tostream& os, closure_static const& w) noexcept(false) {
		os
			<< _T("work=0x") << &w
			<< _T(", type: ") << boost::core::demangle(typeid(w).name())
			<< _T(", mutator: ") << boost::core::demangle(typeid(Proc).name())
			<< _T(", argument_type: ") << boost::core::demangle(typeid(argument_type).name())
			// TODO				<<_T(", input: ")<<static_cast<argument_type const &>(w)
			<< _T(", result_type: ") << boost::core::demangle(typeid(result_type).name());
		// TODO				<<_T(", result: ")<<w.result_;
		return os;
	}

private:
	result_type result_;
};

/// The wrapper for the input work that the user wishes to have mutated.
/**
	The input work to be transferred must be:
	- copy-constructible or move-constructible,
	- may have a member typedef of result_type for declaring the result of the mutation and
	- the mutation function may be specified with either
		- a member function "void __fastcall process(result_type &) noexcept(false)" or,
		- a member function "void __fastcall process(result_type &) const noexcept(false)".

	\see thread_wk_base, control_flow_graph
*/
template<
	class InpWk,	///< The type of the input work.
	class Res,	 ///< The result type of the mutation.
	class CFGP,
	bool NoExcept,
	void (__fastcall InpWk::*Proc)(Res&, CFGP const&) noexcept(NoExcept),	///< The mutator function, by default InpWk::process(Res &).
	class CFG	///< The type of the control-flow graph to be created.
	>
class closure_static_cfg final : public InpWk, public closure_base<CFG> {
public:
	typedef closure_base<CFG> base_t;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef Res result_type;	///< The result-type of the mutation, to assist in making the derived types look like a std::unary_function.
	typedef InpWk argument_type;	 ///< The argument-type of the mutation, to assist in making the derived types look like a std::unary_function.

	explicit __stdcall closure_static_cfg(argument_type&& i, typename cfg_details_type::params const& p) noexcept(false);
	__stdcall ~closure_static_cfg() noexcept(true) {}

	bool __fastcall operator==(const closure_static_cfg&) const noexcept(true);
	bool __fastcall operator<(const closure_static_cfg&) const noexcept(true);

	argument_type const& __fastcall input() const noexcept(true);
	argument_type& __fastcall input() noexcept(true);

	/// Process the wrapped work.
	/**
		By default, all exceptions derived from std::exception are caught and wrapped, to be potentially transferred to the client. Also a "catch-all" block traps any other unknown exceptions, that are also potentially transferred. It is a grave programmatic error to allow a thread_pool with un-emitted exceptions to execute it's destructor. To avoid this potential for undefined behaviour, if process()ing the closure_static is likely to result in an exception being thrown, the user should capture the result of the transfer in an execution_context and dereference it, when the exception will be safely re-thrown, and the user will have an opportunity to correctly catch it and deal with it.

		\see thread_wrapper, thread_pool, thread_pool_type
	*/
	void __fastcall process();

	/// Return a reference to the wrapped work.
	const result_type& __fastcall get_results() const noexcept(false);
	/// Return a reference to the wrapped work.
	result_type& __fastcall get_results() noexcept(false);

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream& __fastcall operator<<(tostream& os, closure_static_cfg const& w) noexcept(false) {
		os
			<< _T("work=0x") << &w
			<< _T(", type: ") << boost::core::demangle(typeid(w).name())
			<< _T(", mutator: ") << boost::core::demangle(typeid(Proc).name())
			<< _T(", argument_type: ") << boost::core::demangle(typeid(argument_type).name())
			// TODO				<<_T(", input: ")<<static_cast<argument_type const &>(w)
			<< _T(", result_type: ") << boost::core::demangle(typeid(result_type).name());
		// TODO				<<_T(", result: ")<<w.result_;
		return os;
	}

private:
	result_type result_;
	typename cfg_details_type::params const cfg_params;
};

/// The wrapper for the input work that the user wishes to have mutated.
/**
	The input work to be transferred must be:
	- copy-constructible or move-constructible,
	- may have a member typedef of result_type for declaring the result of the mutation and
	- the mutation function may be specified with either
		- a member function "void __fastcall process(result_type &) noexcept(false)" or,
		- a member function "void __fastcall process(result_type &) const noexcept(false)".

	\see thread_wk_base, no_control_flow_graph
*/
template<
	class InpWk,	///< The type of the input work.
	class Res,	 ///< The result type of the mutation.
	bool NoExcept,
	void (__fastcall InpWk::*Proc)(Res&) const noexcept(NoExcept),	  ///< The mutator function, by default InpWk::process(Res &).
	class CFG	///< The type of the control-flow graph to be created.
	>
class closure_static_const final : public InpWk, public closure_base<CFG> {
public:
	typedef closure_base<CFG> base_t;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef Res result_type;	///< The result-type of the mutation, to assist in making the derived types look like a std::unary_function.
	typedef InpWk argument_type;	 ///< The argument-type of the mutation, to assist in making the derived types look like a std::unary_function.

	explicit __stdcall closure_static_const(argument_type&& i, typename cfg_details_type::params const& p) noexcept(false);
	__stdcall ~closure_static_const() noexcept(true) {}

	bool __fastcall operator==(const closure_static_const&) const noexcept(true);
	bool __fastcall operator<(const closure_static_const&) const noexcept(true);

	argument_type const& __fastcall input() const noexcept(true);
	argument_type& __fastcall input() noexcept(true);

	/// Process the wrapped work.
	/**
		By default, all exceptions derived from std::exception are caught and wrapped, to be potentially transferred to the client. Also a "catch-all" block traps any other unknown exceptions, that are also potentially transferred. It is a grave programmatic error to allow a thread_pool with un-emitted exceptions to execute it's destructor. To avoid this potential for undefined behaviour, if process()ing the closure_static_const is likely to result in an exception being thrown, the user should capture the result of the transfer in an execution_context and dereference it, when the exception will be safely re-thrown, and the user will have an opportunity to correctly catch it and deal with it.to allow a thread_pool with un-emitted exceptions to execute it's destructor.

		\see thread_wrapper, thread_pool, thread_pool_type
	*/
	void __fastcall process();

	/// Return a reference to the wrapped work.
	const result_type& __fastcall get_results() const noexcept(true);
	/// Return a reference to the wrapped work.
	result_type& __fastcall get_results() noexcept(false);

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream& __fastcall operator<<(tostream& os, closure_static_const const& w) noexcept(false) {
		os
			<< _T("work=0x") << &w
			<< _T(", type: ") << boost::core::demangle(typeid(w).name())
			<< _T(", mutator: ") << boost::core::demangle(typeid(Proc).name())
			<< _T(", argument_type: ") << boost::core::demangle(typeid(argument_type).name())
			// TODO				<<_T(", input: ")<<static_cast<argument_type const &>(w)
			<< _T(", result_type: ") << boost::core::demangle(typeid(result_type).name());
		// TODO				<<_T(", result: ")<<w.result_;
		return os;
	}

private:
	result_type result_;
};

/// The wrapper for the input work that the user wishes to have mutated.
/**
	The input work to be transferred must be:
	- copy-constructible or move-constructible,
	- may have a member typedef of result_type for declaring the result of the mutation and
	- the mutation function may be specified with either
		- a member function "void __fastcall process(result_type &) noexcept(false)" or,
		- a member function "void __fastcall process(result_type &) const noexcept(false)".

	\see thread_wk_base, control_flow_graph
*/
template<
	class InpWk,	///< The type of the input work.
	class Res,	 ///< The result type of the mutation.
	class CFGP,
	bool NoExcept,
	void (__fastcall InpWk::*Proc)(Res&, CFGP const&) const noexcept(NoExcept),	///< The mutator function, by default InpWk::process(Res &).
	class CFG	///< The type of the control-flow graph to be created.
	>
class closure_static_cfg_const final : public InpWk, public closure_base<CFG> {
public:
	typedef closure_base<CFG> base_t;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef Res result_type;	///< The result-type of the mutation, to assist in making the derived types look like a std::unary_function.
	typedef InpWk argument_type;	 ///< The argument-type of the mutation, to assist in making the derived types look like a std::unary_function.

	explicit __stdcall closure_static_cfg_const(argument_type&& i, typename cfg_details_type::params const& p) noexcept(false);
	__stdcall ~closure_static_cfg_const() noexcept(true) {}

	bool __fastcall operator==(const closure_static_cfg_const&) const noexcept(true);
	bool __fastcall operator<(const closure_static_cfg_const&) const noexcept(true);

	argument_type const& __fastcall input() const noexcept(true);
	argument_type& __fastcall input() noexcept(true);

	/// Process the wrapped work.
	/**
		By default, all exceptions derived from std::exception are caught and wrapped, to be potentially transferred to the client. Also a "catch-all" block traps any other unknown exceptions, that are also potentially transferred. It is a grave programmatic error to allow a thread_pool with un-emitted exceptions to execute it's destructor. To avoid this potential for undefined behaviour, if process()ing the closure_static_const is likely to result in an exception being thrown, the user should capture the result of the transfer in an execution_context and dereference it, when the exception will be safely re-thrown, and the user will have an opportunity to correctly catch it and deal with it.to allow a thread_pool with un-emitted exceptions to execute it's destructor.

		\see thread_wrapper, thread_pool, thread_pool_type
	*/
	void __fastcall process();

	/// Return a reference to the wrapped work.
	const result_type& __fastcall get_results() const noexcept(true);
	/// Return a reference to the wrapped work.
	result_type& __fastcall get_results() noexcept(false);

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend inline tostream& __fastcall operator<<(tostream& os, closure_static_cfg_const const& w) noexcept(false) {
		os
			<< _T("work=0x") << &w
			<< _T(", type: ") << boost::core::demangle(typeid(w).name())
			<< _T(", mutator: ") << boost::core::demangle(typeid(Proc).name())
			<< _T(", argument_type: ") << boost::core::demangle(typeid(argument_type).name())
			// TODO				<<_T(", input: ")<<static_cast<argument_type const &>(w)
			<< _T(", result_type: ") << boost::core::demangle(typeid(result_type).name());
		// TODO				<<_T(", result: ")<<w.result_;
		return os;
	}

private:
	result_type result_;
	typename cfg_details_type::params const cfg_params;
};

/// These two "thread" classes provide the ability for the user to obtain results from that work, or not, as the case may be.
/**
	These are used by the pool_thread class (which are a specialisation of the type of thread_wrapper used by the thread_pool). This is also why these classes are not local to either the thread_pool_type or pool_thread classes, as you might think. (Because they are used by both.)
	This needs to be thread-safe because copies of the core_work are held by both the pool_thread and the thread that holds the execution_context.
	The default ordering of core_work in the pool_aspect::work_queue_type is based upon the weak order of the work to be mutated.

	We don't actually need this to be guaranteed lockfree, as locking is done elsewhere, so we can gain a smidge of performance by using raw pointers.
*/
template<
	typename OST,
	class CFG>
class thread_wk_base : public intrusive::node_details_itf<
								  typename OST::lock_traits> {
	typedef closure_base<CFG> input_t;

public:
	typedef intrusive::node_details_itf<
		typename OST::lock_traits>
		base_t;
	typedef OST os_traits;
	typedef typename os_traits::lock_traits lock_traits;
	typedef typename input_t::cfg_type cfg_type;
	typedef typename input_t::cfg_details_type cfg_details_type;
	typedef lock::lockable_settable<lock_traits> work_complete_t;	 ///< This atomic object is the object that is used to signal to a waiting future that the work has been completed.
	using base_t::operator<;

	thread_wk_base(thread_wk_base const&)= delete;
	virtual ~thread_wk_base() noexcept(true) {}

	/// The default ordering of core_work in the pool_aspect::work_queue_type is based upon the weak order of the work to be mutated.
	virtual bool __fastcall operator<(thread_wk_base const&) const noexcept(true)= 0;
	bool __fastcall operator==(thread_wk_base const& ctw) const noexcept(true) {
		return !(*this < ctw) && !(ctw < *this);
	}

	virtual generic_traits::return_data::element_type __fastcall result_traits() const noexcept(true)= 0;
	virtual void update_edge(typename cfg_type::node_property_t::value_type const e_details) noexcept(false)= 0;
	virtual void __fastcall process_joinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false)= 0;
	virtual void __fastcall process_nonjoinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false)= 0;
	virtual work_complete_t* __fastcall work_complete() noexcept(true) {
		return nullptr;
	}

	virtual tstring __fastcall to_string() const noexcept(false) override= 0;

	/**
		\todo Implement using the advice given in "Standard C++ IOStreams and Locales" by A.Langer & K.Kreft, page 170.
	*/
	friend tostream& __fastcall
	operator<<(tostream& os, thread_wk_base const& wk) noexcept(false) {
		os << wk.to_string();
		return os;
	}

protected:
	constexpr thread_wk_base() noexcept(true)
		: base_t() {}
};
template<
	generic_traits::return_data::element_type RD,
	typename OST,
	template<class>
	class Del,
	template<class>
	class AtCtr,
	class CFG>
class thread_wk_async_t;
/**
	nonjoinable types have to be allocated on the heap (or a custom allocator), because there is no execution_context available to hold these objects.
*/
template<typename OST, template<class> class Del, template<class> class AtCtr, class CFG>
class thread_wk_async_t<generic_traits::return_data::element_type::nonjoinable, OST, Del, AtCtr, CFG> : public thread_wk_base<OST, CFG>, public sp_counter_type<long, typename thread_wk_base<OST, CFG>::lock_traits, Del, AtCtr> {
public:
	typedef thread_wk_base<OST, CFG> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename base_t::lock_traits lock_traits;
	typedef sp_counter_type<long, typename base_t::lock_traits, Del, AtCtr> counter_type;
	typedef typename counter_type::atomic_ctr_t atomic_ctr_t;
	typedef typename counter_type::deleter_t deleter_t;
	typedef typename lock_traits::template noop_atomic_ctr<typename atomic_ctr_t::value_type> no_ref_counting;
	typedef noop_dtor<typename deleter_t::element_type> no_deletion;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::nonjoinable;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= counter_type::memory_access_mode;

	static_assert(!std::is_same<atomic_ctr_t, no_ref_counting>::value || std::is_same<deleter_t, no_deletion>::value, "If not refcounted, then the object must not have a defined deleter, as it should be stack-based.");

	constexpr generic_traits::return_data::element_type __fastcall result_traits() const noexcept(true) override {
		return generic_traits::return_data::element_type::nonjoinable;
	}

	tstring __fastcall to_string() const noexcept(false) override {
		return _T("result_traits=nonjoinable");
	}

protected:
	~thread_wk_async_t() noexcept(true) {}
};
/**
	\todo joinable types do not have to be allocated on the heap, because there is an execution_context available to hold these objects.
*/
template<typename OST, template<class> class Del, template<class> class AtCtr, class CFG>
class thread_wk_async_t<generic_traits::return_data::element_type::joinable, OST, Del, AtCtr, CFG> : public thread_wk_base<OST, CFG>, public sp_counter_type<long, typename thread_wk_base<OST, CFG>::lock_traits, Del, AtCtr> {
public:
	typedef thread_wk_base<OST, CFG> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename base_t::lock_traits lock_traits;
	typedef typename base_t::work_complete_t work_complete_t;
	typedef sp_counter_type<long, typename base_t::lock_traits, Del, AtCtr> counter_type;
	typedef typename counter_type::atomic_ctr_t atomic_ctr_t;
	typedef typename counter_type::deleter_t deleter_t;
	typedef typename lock_traits::template noop_atomic_ctr<typename atomic_ctr_t::value_type> no_ref_counting;
	typedef noop_dtor<typename deleter_t::element_type> no_deletion;
	static inline constexpr const generic_traits::return_data::element_type result_traits_= generic_traits::return_data::element_type::joinable;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (counter_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && work_complete_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	static_assert(!std::is_same<atomic_ctr_t, no_ref_counting>::value || std::is_same<deleter_t, no_deletion>::value, "If not refcounted, then the object must not have a defined deleter, as it should be stack-based.");

	constexpr generic_traits::return_data::element_type __fastcall result_traits() const noexcept(true) override {
		return generic_traits::return_data::element_type::joinable;
	}
	virtual typename os_traits::thread_exception const& __fastcall exception_thrown_in_thread() const noexcept(true)= 0;
	virtual void throw_any_exception() const noexcept(false)= 0;

	tstring __fastcall to_string() const noexcept(false) override {
		return _T("result_traits=joinable");
	}

protected:
	~thread_wk_async_t() noexcept(true) {}
};

template<generic_traits::return_data::element_type RD, typename OST, class ThrW, class WFlg, template<class> class Del, template<class> class AtCtr, class CFG>
class thread_wk;

/// With this class, the user cannot join with the transferred closure_base-derived closure.
template<typename OST, class ThrW, class WFlg, template<class> class Del, template<class> class AtCtr, class CFG>
class thread_wk<generic_traits::return_data::element_type::nonjoinable, OST, ThrW, WFlg, Del, AtCtr, CFG> : public thread_wk_async_t<generic_traits::return_data::element_type::nonjoinable, OST, Del, AtCtr, CFG> {
public:
	typedef thread_wk_async_t<generic_traits::return_data::element_type::nonjoinable, OST, Del, AtCtr, CFG> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename base_t::lock_traits lock_traits;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef WFlg work_complete_t;	  ///< This atomic object is the object that is used to signal to a waiting future that the work has been completed.
	/**
		We don't directly contain the argument_type in closure_t because we need to be able to perform operator<() on potentially different objects for supporting pool_traits::prioritised_queue, so we need a hierarchy to allow this.
	*/
	typedef ThrW closure_t;	  ///< This is the binding of the input data to the mutator, with a representation of the output type. i.e. a closure.
	using base_t::counter_type::operator<;

	/**
		Note the value transfer semantics....
	*/
	thread_wk(typename closure_t::argument_type&& tw, typename cfg_details_type::params const& p)
		: base_t(), closure_wk_(std::forward<typename closure_t::argument_type>(tw), p) {
	}

	bool __fastcall operator<(typename base_t::counter_type::value_type const v) const noexcept(true) final {
		return base_t::counter_type::operator<(v);
	}
	bool operator<(typename base_t::base_t const& cw) const noexcept(true) final {
		return dynamic_cast<thread_wk const*>(&cw) ? static_cast<thread_wk const&>(cw) < *this : false;
	}
	bool __fastcall operator<(thread_wk const& ctw) const noexcept(true) {
		return closure_wk_ < ctw.closure_wk_;
	}

	closure_t& __fastcall closure() noexcept(true) {
		return closure_wk_;
	}
	closure_t const& __fastcall closure() const noexcept(true) {
		return closure_wk_;
	}
	void update_edge(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		closure_wk_.update_edge(e_details);
	}
	void __fastcall process_joinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		// TODO handle the exception on the thread, as no execution_context, but what if threads already contains an exception...?
		closure_wk_.process();
		closure_wk_.update_edge(e_details);
	}
	void __fastcall process_nonjoinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		closure_wk_.process();
		closure_wk_.update_edge(e_details);
	}

	tstring __fastcall to_string() const noexcept(false) override {
		tostringstream os;
		os
			<< base_t::to_string()
			<< _T(", enqueued work=0x") << this
			<< _T(", enqueued type: ") << boost::core::demangle(typeid(*this).name())
			<< _T(", work details: ") << closure_wk_;
		return os.str();
	}

protected:
	~thread_wk() noexcept(true) {}

private:
	closure_t closure_wk_;
};

/// With this class, the user can explicitly join with the transferred closure_base-derived closure, but only via the execution_context, created via the appropriate thread_pool derived from thread_pool_type.
template<typename OST, class ThrW, class WFlg, template<class> class Del, template<class> class AtCtr, class CFG>
class thread_wk<generic_traits::return_data::element_type::joinable, OST, ThrW, WFlg, Del, AtCtr, CFG> : public thread_wk_async_t<generic_traits::return_data::element_type::joinable, OST, Del, AtCtr, CFG> {
public:
	typedef thread_wk_async_t<generic_traits::return_data::element_type::joinable, OST, Del, AtCtr, CFG> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename base_t::lock_traits lock_traits;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef WFlg work_complete_t;	  ///< This atomic object is the object that is used to signal to a waiting future that the work has been completed.
	/**
		We don't directly contain the argument_type in closure_t because we need to be able to perform operator<() on potentially different objects for supporting pool_traits::prioritised_queue, so we need a hierarchy to allow this.
	*/
	typedef ThrW closure_t;	  ///< This is the binding of the input data to the mutator, with a representation of the output type. i.e. a closure.
	using base_t::counter_type::operator<;

	thread_wk(work_complete_t& w, typename closure_t::argument_type&& tw, typename cfg_details_type::params const& p)
		: closure_wk_(std::forward<typename closure_t::argument_type>(tw), p), work_complete_(w) {
	}

	bool __fastcall operator<(typename base_t::counter_type::value_type const v) const noexcept(true) final {
		return base_t::counter_type::operator<(v);
	}
	bool operator<(typename base_t::base_t const& cw) const noexcept(true) final {
		return dynamic_cast<thread_wk const*>(&cw) ? static_cast<thread_wk const&>(cw) < *this : false;
	}
	bool __fastcall operator<(thread_wk const& ctw) const noexcept(true) {
		return closure_wk_ < ctw.closure_wk_;
	}

	closure_t& __fastcall closure() noexcept(true) {
		return closure_wk_;
	}
	closure_t const& __fastcall closure() const noexcept(true) {
		return closure_wk_;
	}

	typename os_traits::thread_exception const& __fastcall exception_thrown_in_thread() const noexcept(true) override {
		return exception_thrown_in_thread_;
	}
	void throw_any_exception() const noexcept(false) override {
		exception_thrown_in_thread_.throw_if_set();
	}
	work_complete_t* __fastcall work_complete() noexcept(true) final override {
		return &work_complete_;
	}
	void update_edge(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		closure_wk_.update_edge(e_details);
	}
	void __fastcall process_joinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		/* Signal (& release) anyone waiting for the work to be completed.
			It is really important that this gets done, despite any exceptions that may be thrown, this is to release any thread(s) waiting on the result, in case of exceptions being thrown. Any of these exceptions that are uncaught by the user, will be eventually caught in the first registered. Then in the get_results() function, that exception will be re-thrown, to pass it to the user. Hence the need to ensure that the "work_complete" signal is set in all cases, but allow exceptions to propagate transparently (as I will handle them later, in base-class code).
		*/
		try {
			closure_wk_.process();
#if defined(__GNUC__) && !defined(__clang__)
		} catch(abi::__forced_unwind&) {
			throw;
#endif
		} catch(...) {
			exception_thrown_in_thread_.set(std::current_exception());
		}
		closure_wk_.update_edge(e_details);
	}
	void __fastcall process_nonjoinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		closure_wk_.process();
		closure_wk_.update_edge(e_details);
	}

	tstring __fastcall to_string() const noexcept(false) override {
		tostringstream os;
		os
			<< base_t::to_string()
			<< _T(", enqueued work=0x") << this
			<< _T(", enqueued type: ") << boost::core::demangle(typeid(*this).name())
			<< _T(", work complete event=0x") << &work_complete_
			<< _T(", work details: ") << closure_wk_
			<< _T(", exception details: ") << exception_thrown_in_thread_;
		return os.str();
	}

protected:
	~thread_wk() noexcept(true) {}

private:
	closure_t closure_wk_;
	typename os_traits::thread_exception exception_thrown_in_thread_;
	work_complete_t & work_complete_;
};

/// With this class, the user can explicity join with the transferred closure_base-derived closure returned from a parallel algorithm, but only via the execution_context, created via the appropriate thread_pool derived from thread_pool_type.
template<typename OST, class ThrW, class WFlg, template<class> class Del, template<class> class AtCtr, class CFG>
class algo_thread_wk : public thread_wk_async_t<generic_traits::return_data::element_type::joinable, OST, Del, AtCtr, CFG> {
public:
	typedef thread_wk_async_t<generic_traits::return_data::element_type::joinable, OST, Del, AtCtr, CFG> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename base_t::lock_traits lock_traits;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	/// This atomic object is the object that is used to signal to a waiting future that the work has been completed.
	typedef WFlg work_complete_t;
	/// This is the binding of the input data to the mutator, with a representation of the output type. i.e. a closure.
	/**
		We don't directly contain the argument_type in closure_t because we need to be able to perform operator<() on potentially different objects for supporting pool_traits::prioritised_queue, so we need a hierarchy to allow this.
	*/
	typedef ThrW closure_t;
	using base_t::counter_type::operator<;

	__stdcall algo_thread_wk(work_complete_t& w, typename closure_t::argument_type&& tw, typename cfg_details_type::params const& p)
		: closure_wk_(std::forward<typename closure_t::argument_type>(tw), p), work_complete_(w) {
		work_complete_.lock_containers();
	}
	~algo_thread_wk() noexcept(true) {}

	bool operator<(typename base_t::base_t const& cw) const noexcept(true) override {
		return dynamic_cast<algo_thread_wk const*>(&cw) ? static_cast<algo_thread_wk const&>(cw) < *this : false;
	}
	bool __fastcall operator<(algo_thread_wk const& ctw) const noexcept(true) {
		return closure_wk_ < ctw.closure_wk_;
	}

	closure_t& __fastcall closure() noexcept(true) {
		return closure_wk_;
	}
	closure_t const& __fastcall closure() const noexcept(true) {
		return closure_wk_;
	}

	typename os_traits::thread_exception const& __fastcall exception_thrown_in_thread() const noexcept(true) override {
		return exception_thrown_in_thread_;
	}
	void throw_any_exception() const noexcept(false) override {
		exception_thrown_in_thread_.throw_if_set();
	}
	work_complete_t* __fastcall work_complete() noexcept(true) final override {
		return &work_complete_;
	}
	void update_edge(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		closure_wk_.update_edge(e_details);
	}
	void __fastcall process_joinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		/* Signal (& release) anyone waiting for the work to be completed.
			It is really important that this gets done, despite any exceptions that may be thrown, this is to release any thread(s) waiting on the result, in case of exceptions being thrown. Any of these exceptions that are uncaught by the user, will be eventually caught in the first registered. Then in the get_results() function, that exception will be re-thrown, to pass it to the user. Hence the need to ensure that the "work_complete" signal is set in all cases, but allow exceptions to propagate transparently (as I will handle them later, in base-class code).
		*/
		try {
			closure_wk_.process();
#if defined(__GNUC__) && !defined(__clang__)
		} catch(abi::__forced_unwind&) {
			throw;
#endif
		} catch(...) {
			exception_thrown_in_thread_.set(std::current_exception());
		}
		closure_wk_.update_edge(e_details);
	}
	void __fastcall process_nonjoinable(typename cfg_type::node_property_t::value_type const e_details) noexcept(false) override {
		closure_wk_.process();
		closure_wk_.update_edge(e_details);
	}

	void resize_output(typename work_complete_t::containers_type::size_type const out_colln_size) noexcept(false) {
		work_complete_.resize_output(out_colln_size);
	}

	tstring __fastcall to_string() const noexcept(false) override {
		tostringstream os;
		os
			<< base_t::to_string()
			<< _T(", enqueued work=0x") << this
			<< _T(", enqueued type: ") << boost::core::demangle(typeid(*this).name())
			<< _T(", work complete event=0x") << &work_complete_
			<< _T(", work details: ") << closure_wk_
			<< _T(", exception details: ") << exception_thrown_in_thread_;
		return os.str();
	}

private:
	closure_t closure_wk_;
	typename os_traits::thread_exception exception_thrown_in_thread_;
	work_complete_t & work_complete_;
};

/// With this class, the user can explicitly join with the transferred closure_base-derived closure returned from a parallel algorithm, but only via the execution_context, created via the appropriate thread_pool derived from thread_pool_type.
/**
	This class also allocates a custom memory-buffer on the heap for containing the closure_base-derived closure generated by the parallel algorithms within subdivide_n_gen_wk::process(), to reduce the number of calls to the global operator new(). Note that the dtors of these placement-new'd closures must be called before the dtor of the buufedr, otherwise heap corruption may occur. This requirement is guaranteed because within the base class the work_complete_ object is only signalled once allmutations on the collection is complete, and this can occur only after all of the subdivide_n_gen_wk::process() functions have finished recursing, i.e. the work has been distributed, so all of the closures created to do that have been mutated.

	\see algo_thread_wk, nonjoinable_buff_t, subdivide_n_gen_wk::process()
*/
template<typename OST, class ThrW, class WFlg, class SubDivAlgWk, template<class> class Del, template<class> class AtCtr, class CFG>
class algo_thread_wk_buffered : public algo_thread_wk<OST, ThrW, WFlg, Del, AtCtr, CFG> {
public:
	typedef algo_thread_wk<OST, ThrW, WFlg, Del, AtCtr, CFG> base_t;
	typedef typename base_t::os_traits os_traits;
	typedef typename base_t::cfg_type cfg_type;
	typedef typename base_t::cfg_details_type cfg_details_type;
	typedef typename base_t::work_complete_t work_complete_t;
	typedef typename base_t::closure_t closure_t;

	typedef heap::memory_buffer<os_traits, SubDivAlgWk> algo_work_heap_type;

	__stdcall algo_thread_wk_buffered(work_complete_t& w, typename closure_t::argument_type&& tw, typename algo_work_heap_type::size_type const num_objs, typename cfg_details_type::params const& p) noexcept(false)
		: base_t(w, std::forward<typename closure_t::argument_type>(tw), p), algo_work_heap_(num_objs) {
	}
	~algo_thread_wk_buffered() noexcept(true) {}

	/// Access the pre-allocated buffer for the thread_wk_types that subdivide_n_gen_wk::process() creates.
	/**
		This is so that these thread_wk_types can be allocated via placement new into this buffer to reduce load on operator new() in the transfer of nonjoinable_buff() work in subdivide_n_gen_wk::process().
	*/
	algo_work_heap_type& __fastcall algo_work_heap() noexcept(true) {
		return algo_work_heap_;
	}

	tstring __fastcall to_string() const noexcept(false) override {
		tostringstream os;
		os
			<< base_t::to_string()
			<< _T(", algo_work_heap: ") << algo_work_heap_;
		return os.str();
	}

private:
	algo_work_heap_type algo_work_heap_;
};

}
}}}}

#include "thread_work_closure_impl.hpp"
