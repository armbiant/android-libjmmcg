/******************************************************************************
** Copyright © 2010 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

template<class P, class Collns>
inline
	typename unlock_collections<P, Collns>::atomic_state_type
	unlock_collections<P, Collns>::set() noexcept(true) {
	if(num_tasks_spawned.get()) {
		--num_tasks_spawned;
	}
	if(num_tasks_spawned > 0) {
		return pool_traits_type::os_traits::lock_traits::atom_unset;
	} else {
		assert(num_tasks_spawned == 0);
		containers_.unlock();
		return pool_traits_type::os_traits::lock_traits::atom_set;
	}
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
struct subdivide_n_gen_wk<Ps, TPB, Alg>::algo_work_heap_type {
	typedef unsigned char* buffer_type;

	buffer_type const buffer;
	std::ptrdiff_t const size;	  ///< This is units of items, not bytes.
	std::size_t const stride;	 ///< In bytes.

	template<class T>
	algo_work_heap_type(T* const b, std::ptrdiff_t const sz, std::size_t const str) noexcept(true)
		: buffer(reinterpret_cast<buffer_type const>(b)), size(sz), stride(str) {
		assert(buffer);
		assert(size >= 1);
		assert(stride >= 1);
	}
};

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
[[gnu::pure]] inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::thread_pool_type::pool_type::size_type
	subdivide_n_gen_wk<Ps, TPB, Alg>::compute_threads_per_clique(typename thread_pool_type::pool_type::size_type num_threads, typename thread_pool_type::pool_type::size_type const cliques) noexcept(true) {
	const typename thread_pool_type::pool_type::size_type threads= static_cast<typename thread_pool_type::pool_type::size_type>(std::floor(static_cast<double>(num_threads) / static_cast<double>(cliques) + 0.5));
	assert(threads >= 1);
	assert(threads <= num_threads);
	return threads;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
[[gnu::pure]] inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::thread_pool_type::pool_type::size_type
	subdivide_n_gen_wk<Ps, TPB, Alg>::compute_buffer_items(typename thread_pool_type::pool_type::size_type const num_threads_per_clique) noexcept(true) {
	typename thread_pool_type::pool_type::size_type evens= 0, odds= 0;
	for(std::size_t i= 0; i < num_threads_per_clique; ++i) {
		if(i & 1) {
			odds+= i;
		} else {
			evens+= i;
		}
	}
	const typename thread_pool_type::pool_type::size_type num_items= static_cast<typename thread_pool_type::pool_type::size_type>(
																							  std::floor(
																								  (static_cast<double>(num_threads_per_clique * (evens * 2 + odds * 3)) / 2)
																								  + 0.5))
																						  + num_threads_per_clique;
	assert(num_items > 0);
	return num_items;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::in_iterator
	subdivide_n_gen_wk<Ps, TPB, Alg>::compute_end(typename std::iterator_traits<in_iterator>::difference_type const number_subranges) const noexcept(true) {
	if(number_subranges == 1) {
		return all_done.containers().input1.end();
	} else {
		double const end_subrange_dist= double(all_done.containers().input1.size()) / double(number_subranges);
		return std::next(all_done.containers().input1.begin(), static_cast<typename std::iterator_traits<in_iterator>::difference_type>(end_subrange_dist));
	}
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::container_type::size_type
	subdivide_n_gen_wk<Ps, TPB, Alg>::num_wk_items_spawned() const noexcept(true) {
	typename container_type::size_type const sz= all_done.containers().cont1_in.size();
	return std::min(sz, static_cast<typename container_type::size_type>(sz / pool.batch_size(sz)));
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline std::ptrdiff_t
subdivide_n_gen_wk<Ps, TPB, Alg>::odd_third_buff_range() const noexcept(true) {
	const std::ptrdiff_t range= (work_heap.size - 3) / 3;
	assert(range > 0);
	assert(range < work_heap.size);
	return range;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline std::ptrdiff_t
subdivide_n_gen_wk<Ps, TPB, Alg>::even_half_buff_range() const noexcept(true) {
	const std::ptrdiff_t range= (work_heap.size - 2) / 2;
	assert(range > 0);
	assert(range < work_heap.size);
	return range;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::algo_work_heap_type::buffer_type
	subdivide_n_gen_wk<Ps, TPB, Alg>::first_buff_part() const noexcept(true) {
	assert(work_heap.size > 0);
	typename algo_work_heap_type::buffer_type const ret= work_heap.buffer + static_cast<std::size_t>(work_heap.size);
	assert(ret <= (work_heap.buffer + work_heap.stride * static_cast<std::size_t>(work_heap.size)));
	return ret;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::algo_work_heap_type::buffer_type
	subdivide_n_gen_wk<Ps, TPB, Alg>::even_second_buff_part() const noexcept(true) {
	assert(work_heap.size > 0);
	typename algo_work_heap_type::buffer_type const ret= work_heap.buffer + work_heap.stride * static_cast<std::size_t>(work_heap.size) / 2;
	assert(ret <= (work_heap.buffer + work_heap.stride * static_cast<std::size_t>(work_heap.size)));
	return ret;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::algo_work_heap_type::buffer_type
	subdivide_n_gen_wk<Ps, TPB, Alg>::odd_second_buff_part() const noexcept(true) {
	assert(work_heap.size > 0);
	typename algo_work_heap_type::buffer_type const ret= work_heap.buffer + work_heap.stride * static_cast<std::size_t>(work_heap.size) / 3;
	assert(ret <= (work_heap.buffer + work_heap.stride * static_cast<std::size_t>(work_heap.size)));
	return ret;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline
	typename subdivide_n_gen_wk<Ps, TPB, Alg>::algo_work_heap_type::buffer_type
	subdivide_n_gen_wk<Ps, TPB, Alg>::odd_third_buff_part() const noexcept(true) {
	assert(work_heap.size > 0);
	typename algo_work_heap_type::buffer_type const ret= work_heap.buffer + work_heap.stride * 2 * static_cast<std::size_t>(work_heap.size) / 3;
	assert(ret <= (work_heap.buffer + work_heap.stride * static_cast<std::size_t>(work_heap.size)));
	return ret;
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline subdivide_n_gen_wk<Ps, TPB, Alg>::subdivide_n_gen_wk(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w,
	algo_work_heap_type const& wh) noexcept(true)
	:	 // The number of threads in to pool may not be perfectly divisible into the requested number of cliques, so ensure that the number of tasks to be generated for a clique is the next highest integer.
	  threads_per_clique(),
	  work_heap(wh),
	  pool(p),
	  fn(f),
	  all_done(w),
	  begin(all_done.containers().input1.begin()),
	  end(all_done.containers().input1.end()) {
	// Ensure that a sub-task that actually processes work can't mark the whole task complete before any other sub-tasks have completed their processing.
	all_done.add_a_task();
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline subdivide_n_gen_wk<Ps, TPB, Alg>::subdivide_n_gen_wk(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w,
	algo_work_heap_type const& wh,
	typename std::iterator_traits<in_iterator>::difference_type const number_subranges,
	typename thread_pool_type::pool_type::size_type const cliques) noexcept(true)
	:	 // The number of threads in to pool may not be perfectly divisible into the requested number of cliques, so ensure that the number of tasks to be generated for a clique is the next highest integer.
	  threads_per_clique(compute_threads_per_clique(p.pool_size(), cliques)),
	  work_heap(wh),
	  pool(p),
	  fn(f),
	  all_done(w),
	  begin(all_done.containers().input1.begin()),
	  end(compute_end(number_subranges)) {
	assert(threads_per_clique >= 1);
	assert(threads_per_clique <= pool.pool_size());
	// Ensure that a sub-task that actually processes work can't mark the whole task complete before any other sub-tasks have completed their processing.
	all_done.add_a_task();
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline subdivide_n_gen_wk<Ps, TPB, Alg>::subdivide_n_gen_wk(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w,
	algo_work_heap_type const& wh,
	in_iterator const& b,
	in_iterator const& e) noexcept(true)
	: threads_per_clique(),
	  work_heap(wh),
	  pool(p),
	  fn(f),
	  all_done(w),
	  begin(b),
	  end(e) {
	// Ensure that a sub-task that actually processes work can't mark the whole task complete before any other sub-tasks have completed their processing.
	all_done.add_a_task();
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Alg>
inline subdivide_n_gen_wk<Ps, TPB, Alg>::subdivide_n_gen_wk(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w,
	algo_work_heap_type const& wh,
	in_iterator const& b,
	in_iterator const& e,
	typename thread_pool_type::pool_type::size_type const t_per_c) noexcept(true)
	: threads_per_clique(t_per_c),
	  work_heap(wh),
	  pool(p),
	  fn(f),
	  all_done(w),
	  begin(b),
	  end(e) {
	assert(threads_per_clique >= 1);
	assert(threads_per_clique <= pool.pool_size());
	// Ensure that a sub-task that actually processes work can't mark the whole task complete before any other sub-tasks have completed their processing.
	all_done.add_a_task();
}

template<class TPB, class Alg>
struct subdivide_n_gen_wk<pool_traits::size_mode_t::infinite, TPB, Alg>::algo_work_heap_type {
	typedef unsigned char* buffer_type;

	template<class T>
	constexpr algo_work_heap_type(T* const, std::ptrdiff_t const, std::size_t const) noexcept(true) {
	}
};

template<class TPB, class Alg>
inline constexpr typename subdivide_n_gen_wk<pool_traits::size_mode_t::infinite, TPB, Alg>::thread_pool_type::pool_type::size_type
subdivide_n_gen_wk<pool_traits::size_mode_t::infinite, TPB, Alg>::compute_threads_per_clique(typename thread_pool_type::pool_type::size_type, typename thread_pool_type::pool_type::size_type const) noexcept(true) {
	return typename thread_pool_type::pool_type::size_type();
}

template<class TPB, class Alg>
inline constexpr typename subdivide_n_gen_wk<pool_traits::size_mode_t::infinite, TPB, Alg>::thread_pool_type::pool_type::size_type
subdivide_n_gen_wk<pool_traits::size_mode_t::infinite, TPB, Alg>::compute_buffer_items(typename thread_pool_type::pool_type::size_type const) noexcept(true) {
	return typename thread_pool_type::pool_type::size_type(1);
}

template<class TPB, class Alg>
inline subdivide_n_gen_wk<pool_traits::size_mode_t::infinite, TPB, Alg>::subdivide_n_gen_wk(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w) noexcept(true)
	:	 // The number of threads in to pool may not be perfectly divisible into the requested number of cliques, so ensure that the number of tasks to be generated for a clique is the next highest integer.
	  threads_per_clique(),
	  pool(p),
	  fn(f),
	  all_done(w),
	  begin(all_done.containers().input1.begin()),
	  end(all_done.containers().input1.end()) {
	// Ensure that a sub-task that actually processes work can't mark the whole task complete before any other sub-tasks have completed their processing.
	all_done.add_a_task();
}

template<class TPB, class Alg>
inline subdivide_n_gen_wk<pool_traits::size_mode_t::infinite, TPB, Alg>::subdivide_n_gen_wk(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w,
	in_iterator const& b,
	in_iterator const& e) noexcept(true)
	: threads_per_clique(),
	  pool(p),
	  fn(f),
	  all_done(w),
	  begin(b),
	  end(e) {
	// Ensure that a sub-task that actually processes work can't mark the whole task complete before any other sub-tasks have completed their processing.
	all_done.add_a_task();
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Fn, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk1<Ps, TPB, Fn, Conts, Alg>::subdivide_n_gen_wk1(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w,
	algo_work_heap_type const& wh,
	in_iterator const& b,
	in_iterator const& e,
	typename thread_pool_type::pool_type::size_type const threads_per_clique) noexcept(true)
	: base_t(p, f, w, wh, b, e, threads_per_clique) {
	assert(sizeof(alg_wrap_t) <= this->work_heap.stride);
	assert(sizeof(subdivide_n_gen_wk1) <= this->work_heap.stride);
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Fn, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk1<Ps, TPB, Fn, Conts, Alg>::subdivide_n_gen_wk1(thread_pool_type& p, operation_type& f, typename alg_wrap_t::work_complete_t& w, algo_work_heap_type const& wh, typename std::iterator_traits<in_iterator>::difference_type const number_subranges, typename thread_pool_type::pool_type::size_type const cliques) noexcept(true)
	: base_t(p, f, w, wh, number_subranges, cliques) {
	assert(sizeof(alg_wrap_t) <= this->work_heap.stride);
	assert(sizeof(subdivide_n_gen_wk1) <= this->work_heap.stride);
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class Fn, class Conts, template<class, class> class Alg>
inline void
subdivide_n_gen_wk1<Ps, TPB, Fn, Conts, Alg>::process() noexcept(false) {
	typedef typename thread_pool_type::nonjoinable nonjoinable_t;
	// TODO			typedef typename thread_pool_type::nonjoinable_buff nonjoinable_t;

	const ensure_wk_complete_t e(this->all_done);
	const typename in_iterator::difference_type dist(std::distance(this->begin, this->end));
	if(dist > 0) {	  // In case a joker passes in an empty collection...
		assert(std::accumulate(this->work_heap.buffer, this->work_heap.buffer + this->work_heap.size, 0UL) == 0UL);
		if(this->threads_per_clique <= 1) {	  // We're at a leaf task, so generate no more, and directly process() the sub-range.
			alg_wrap_t leaf_wk(typename alg_wrap_t::work_wrap(this->begin, this->end, this->fn), this->all_done);
			PREFETCH_READ(&*this->begin, 3);
			leaf_wk.process();
		} else {
			if(this->threads_per_clique & 0x01) {
				// Note that this over-submits tasks, one of these should not be a sub-tree, but a terminal task, but I'd need to re-compute dist_3 correctly to submit less work to the terminal task.
				const typename in_iterator::difference_type dist_3(dist / 3);
				const in_iterator middle(std::next(this->begin, dist_3));
				// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
				assert(this->work_heap.size > 0);
				if(this->begin != middle) {
					assert(this->first_buff_part() <= (this->work_heap.buffer + this->work_heap.stride * static_cast<std::size_t>(this->work_heap.size)));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->first_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk1(this->pool, this->fn, this->all_done, algo_work_heap_type(this->first_buff_part(), this->odd_third_buff_range(), this->work_heap.stride), this->begin, middle, this->threads_per_clique >> 1);
				}
				const in_iterator middle_next(std::next(middle, dist_3));
				if(middle != middle_next) {
					assert((this->odd_second_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + static_cast<std::size_t>(this->work_heap.size)));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->odd_second_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk1(this->pool, this->fn, this->all_done, algo_work_heap_type(this->odd_second_buff_part() + this->work_heap.stride, this->odd_third_buff_range(), this->work_heap.stride), middle, middle_next, this->threads_per_clique >> 1);
				}
				if(middle_next != this->end) {
					assert((this->odd_third_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + static_cast<std::size_t>(this->work_heap.size)));
					subdivide_n_gen_wk1(this->pool, this->fn, this->all_done, algo_work_heap_type(this->odd_third_buff_part() + this->work_heap.stride, this->odd_third_buff_range(), this->work_heap.stride), middle_next, this->end, this->threads_per_clique >> 1).process();
				}
			} else {
				const in_iterator middle(std::next(this->begin, dist >> 1));
				// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
				if(this->begin != middle) {
					assert(this->first_buff_part() <= (this->work_heap.buffer + this->work_heap.stride * static_cast<std::size_t>(this->work_heap.size)));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->first_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk1(
								this->pool,
								this->fn,
								this->all_done,
								algo_work_heap_type(this->first_buff_part(), this->even_half_buff_range(), this->work_heap.stride),
								this->begin,
								middle,
								this->threads_per_clique >> 1);
				}
				if(middle != this->end) {
					assert((this->even_second_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + static_cast<std::size_t>(this->work_heap.size)));
					subdivide_n_gen_wk1(
						this->pool,
						this->fn,
						this->all_done,
						algo_work_heap_type(this->even_second_buff_part() + this->work_heap.stride, this->even_half_buff_range(), this->work_heap.stride),
						middle,
						this->end,
						this->threads_per_clique >> 1)
						.process();
				}
			}
		}
	}
}

template<class TPB, class Fn, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk1<pool_traits::size_mode_t::infinite, TPB, Fn, Conts, Alg>::subdivide_n_gen_wk1(
	thread_pool_type& p,
	operation_type& f,
	typename alg_wrap_t::work_complete_t& w,
	in_iterator const& b,
	in_iterator const& e) noexcept(true)
	: base_t(p, f, w, b, e) {
}

template<class TPB, class Fn, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk1<pool_traits::size_mode_t::infinite, TPB, Fn, Conts, Alg>::subdivide_n_gen_wk1(thread_pool_type& p, operation_type& f, typename alg_wrap_t::work_complete_t& w, algo_work_heap_type const&, typename std::iterator_traits<in_iterator>::difference_type const, typename thread_pool_type::pool_type::size_type const) noexcept(true)
	: base_t(p, f, w) {
}

template<class TPB, class Fn, class Conts, template<class, class> class Alg>
inline void __fastcall subdivide_n_gen_wk1<pool_traits::size_mode_t::infinite, TPB, Fn, Conts, Alg>::process() noexcept(false) {
	typedef typename thread_pool_type::nonjoinable nonjoinable_t;

	const ensure_wk_complete_t e(this->all_done);
	const typename in_iterator::difference_type dist(std::distance(this->begin, this->end));
	if(dist > 0) {	  // In case a joker passes in an empty collection...
		if(dist <= 2) {	// We're at a leaf task, so generate no more, and directly process() the sub-range.
			alg_wrap_t leaf_wk(typename alg_wrap_t::work_wrap(this->begin, this->end, this->fn), this->all_done);
			PREFETCH_READ(&*this->begin, 3);
			leaf_wk.process();
		} else {
			const in_iterator middle(std::next(this->begin, dist >> 1));
			// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
			if(this->begin != middle) {
				this->pool
					<< nonjoinable_t(this, gen_wk_node_str)
					<< subdivide_n_gen_wk1(
							this->pool,
							this->fn,
							this->all_done,
							this->begin,
							middle);
			}
			if(middle != this->end) {
				subdivide_n_gen_wk1(
					this->pool,
					this->fn,
					this->all_done,
					middle,
					this->end)
					.process();
			}
		}
	}
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class UniOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk2<Ps, TPB, UniOp, Conts, Alg>::subdivide_n_gen_wk2(
	thread_pool_type& p,
	operation_type& o,
	typename alg_wrap_t::work_complete_t& w,
	algo_work_heap_type const& wh,
	in_iterator const& ib,
	in_iterator const& ie,
	out_iterator const& ob,
	out_iterator const& oe,
	const typename thread_pool_type::pool_type::size_type threads_per_clique) noexcept(true)
	: base_t(p, o, w, wh, ib, ie, threads_per_clique), out_begin(ob), out_end(oe) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
	assert(sizeof(alg_wrap_t) <= this->work_heap.stride);
	assert(sizeof(subdivide_n_gen_wk2) <= this->work_heap.stride);
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class UniOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk2<Ps, TPB, UniOp, Conts, Alg>::subdivide_n_gen_wk2(thread_pool_type& p, operation_type& o, typename alg_wrap_t::work_complete_t& w, algo_work_heap_type const& wh, typename std::iterator_traits<in_iterator>::difference_type const number_subranges, typename thread_pool_type::pool_type::size_type const cliques) noexcept(true)
	: base_t(p, o, w, wh, number_subranges, cliques),
	  out_begin(this->all_done.containers().output.begin()),
	  out_end(this->all_done.containers().output.end()) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
	assert(sizeof(alg_wrap_t) <= this->work_heap.stride);
	assert(sizeof(subdivide_n_gen_wk2) <= this->work_heap.stride);
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class UniOp, class Conts, template<class, class> class Alg>
inline void
subdivide_n_gen_wk2<Ps, TPB, UniOp, Conts, Alg>::process() noexcept(false) {
	typedef typename thread_pool_type::nonjoinable nonjoinable_t;
	// TODO			typedef typename thread_pool_type::nonjoinable_buff nonjoinable_t;

	const ensure_wk_complete_t e(this->all_done);
	const typename in_iterator::difference_type dist(std::distance(this->begin, this->end));
	if(dist > 0) {	  // In case a joker passes in an empty collection...
		assert(std::accumulate(this->work_heap.buffer, this->work_heap.buffer + this->work_heap.size, 0UL) == 0UL);
		if(this->threads_per_clique <= 1) {	  // We're at a leaf task, so generate no more, and directly process() the sub-range.
			alg_wrap_t leaf_wk(typename alg_wrap_t::work_wrap(this->begin, this->end, out_begin, this->fn), this->all_done);
			PREFETCH_READ(&*this->begin, 3);
			PREFETCH_WRITE(&*out_begin, 3);
			leaf_wk.process();
		} else {
			if(this->threads_per_clique & 0x01) {
				// Note that this over-submits tasks, one of these should not be a sub-tree, but a terminal task, but I'd need to re-compute dist_3 correctly to submit less work to the terminal task.
				const typename in_iterator::difference_type dist_3(dist / 3);
				const in_iterator in_middle(std::next(this->begin, dist_3));
				const out_iterator out_middle(std::next(out_begin, dist_3));
				// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
				if(this->begin != in_middle) {
					assert(this->first_buff_part() <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->first_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk2(this->pool, this->fn, this->all_done, algo_work_heap_type(this->first_buff_part(), this->odd_third_buff_range(), this->work_heap.stride), this->begin, in_middle, out_begin, out_middle, this->threads_per_clique >> 1);
				}
				const in_iterator in_middle_next(std::next(in_middle, dist_3));
				const out_iterator out_middle_next(std::next(out_middle, dist_3));
				if(in_middle != in_middle_next) {
					assert((this->odd_second_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->odd_second_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk2(this->pool, this->fn, this->all_done, algo_work_heap_type(this->odd_second_buff_part() + this->work_heap.stride, this->odd_third_buff_range(), this->work_heap.stride), in_middle, in_middle_next, out_middle, out_middle_next, this->threads_per_clique >> 1);
				}
				if(in_middle_next != this->end) {
					assert((this->odd_third_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					subdivide_n_gen_wk2(this->pool, this->fn, this->all_done, algo_work_heap_type(this->odd_third_buff_part() + this->work_heap.stride, this->odd_third_buff_range(), this->work_heap.stride), in_middle_next, this->end, out_middle_next, out_end, this->threads_per_clique >> 1).process();
				}
			} else {
				const in_iterator in_middle(std::next(this->begin, dist >> 1));
				const out_iterator out_middle(std::next(out_begin, dist >> 1));
				// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
				if(this->begin != in_middle) {
					assert(this->first_buff_part() <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->first_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk2(
								this->pool,
								this->fn,
								this->all_done,
								algo_work_heap_type(this->first_buff_part(), this->even_half_buff_range(), this->work_heap.stride),
								this->begin,
								in_middle,
								out_begin,
								out_middle,
								this->threads_per_clique >> 1);
				}
				if(in_middle != this->end) {
					assert((this->even_second_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					subdivide_n_gen_wk2(
						this->pool,
						this->fn,
						this->all_done,
						algo_work_heap_type(this->even_second_buff_part() + this->work_heap.stride, this->even_half_buff_range(), this->work_heap.stride),
						in_middle,
						this->end,
						out_middle,
						out_end,
						this->threads_per_clique >> 1)
						.process();
				}
			}
		}
	}
}

template<class TPB, class UniOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk2<pool_traits::size_mode_t::infinite, TPB, UniOp, Conts, Alg>::subdivide_n_gen_wk2(
	thread_pool_type& p,
	operation_type& o,
	typename alg_wrap_t::work_complete_t& w,
	in_iterator const& ib,
	in_iterator const& ie,
	out_iterator const& ob,
	out_iterator const& oe) noexcept(true)
	: base_t(p, o, w, ib, ie), out_begin(ob), out_end(oe) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
}

template<class TPB, class UniOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk2<pool_traits::size_mode_t::infinite, TPB, UniOp, Conts, Alg>::subdivide_n_gen_wk2(thread_pool_type& p, operation_type& o, typename alg_wrap_t::work_complete_t& w, algo_work_heap_type const&, typename std::iterator_traits<in_iterator>::difference_type const, typename thread_pool_type::pool_type::size_type const) noexcept(true)
	: base_t(p, o, w),
	  out_begin(this->all_done.containers().output.begin()),
	  out_end(this->all_done.containers().output.end()) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
}

template<class TPB, class UniOp, class Conts, template<class, class> class Alg>
inline void __fastcall subdivide_n_gen_wk2<pool_traits::size_mode_t::infinite, TPB, UniOp, Conts, Alg>::process() noexcept(false) {
	typedef typename thread_pool_type::nonjoinable nonjoinable_t;

	const ensure_wk_complete_t e(this->all_done);
	const typename in_iterator::difference_type dist(std::distance(this->begin, this->end));
	if(dist > 0) {	  // In case a joker passes in an empty collection...
		if(dist <= 2) {	// We're at a leaf task, so generate no more, and directly process() the sub-range.
			alg_wrap_t leaf_wk(typename alg_wrap_t::work_wrap(this->begin, this->end, out_begin, this->fn), this->all_done);
			PREFETCH_READ(&*this->begin, 3);
			PREFETCH_WRITE(&*out_begin, 3);
			leaf_wk.process();
		} else {
			const in_iterator in_middle(std::next(this->begin, dist >> 1));
			const out_iterator out_middle(std::next(out_begin, dist >> 1));
			// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
			if(this->begin != in_middle) {
				this->pool
					<< nonjoinable_t(this, gen_wk_node_str)
					<< subdivide_n_gen_wk2(
							this->pool,
							this->fn,
							this->all_done,
							this->begin,
							in_middle,
							out_begin,
							out_middle);
			}
			if(in_middle != this->end) {
				subdivide_n_gen_wk2(
					this->pool,
					this->fn,
					this->all_done,
					in_middle,
					this->end,
					out_middle,
					out_end)
					.process();
			}
		}
	}
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class BinOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk3<Ps, TPB, BinOp, Conts, Alg>::subdivide_n_gen_wk3(
	thread_pool_type& p,
	operation_type& o,
	typename alg_wrap_t::work_complete_t& w,
	algo_work_heap_type const& wh,
	in_iterator const& ib1,
	in_iterator const& ie1,
	in2_iterator const& ib2,
	in2_iterator const& ie2,
	out_iterator const& ob,
	out_iterator const& oe,
	typename thread_pool_type::pool_type::size_type const threads_per_clique) noexcept(true)
	: base_t(p, o, w, wh, ib1, ie1, threads_per_clique),
	  in_begin2(ib2),
	  in_end2(ie2),
	  out_begin(ob),
	  out_end(oe) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().input2.size());
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
	assert(sizeof(alg_wrap_t) <= this->work_heap.stride);
	assert(sizeof(subdivide_n_gen_wk3) <= this->work_heap.stride);
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class BinOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk3<Ps, TPB, BinOp, Conts, Alg>::subdivide_n_gen_wk3(thread_pool_type& p, operation_type& o, typename alg_wrap_t::work_complete_t& w, algo_work_heap_type const& wh, typename std::iterator_traits<in_iterator>::difference_type const number_subranges, typename thread_pool_type::pool_type::size_type const cliques) noexcept(true)
	: base_t(p, o, w, wh, number_subranges, cliques),
	  in_begin2(this->all_done.containers().input2.begin()),
	  in_end2(this->all_done.containers().input2.end()),
	  out_begin(this->all_done.containers().output.begin()),
	  out_end(this->all_done.containers().output.end()) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().input2.size());
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
	assert(sizeof(alg_wrap_t) <= this->work_heap.stride);
	assert(sizeof(subdivide_n_gen_wk3) <= this->work_heap.stride);
}

template<pool_traits::size_mode_t::element_type Ps, class TPB, class BinOp, class Conts, template<class, class> class Alg>
inline void
subdivide_n_gen_wk3<Ps, TPB, BinOp, Conts, Alg>::process() noexcept(false) {
	typedef typename thread_pool_type::nonjoinable nonjoinable_t;
	// TODO			typedef typename thread_pool_type::nonjoinable_buff nonjoinable_t;

	const ensure_wk_complete_t e(this->all_done);
	const typename in_iterator::difference_type dist(std::distance(this->begin, this->end));
	if(dist > 0) {	  // In case a joker passes in an empty collection...
		assert(std::accumulate(this->work_heap.buffer, this->work_heap.buffer + this->work_heap.size, 0UL) == 0UL);
		if(this->threads_per_clique <= 1) {	  // We're at a leaf task, so generate no more, and directly process() the sub-range.
			alg_wrap_t leaf_wk(typename alg_wrap_t::work_wrap(this->begin, this->end, in_begin2, out_begin, this->fn), this->all_done);
			PREFETCH_READ(&*this->begin, 3);
			PREFETCH_READ(&*in_begin2, 3);
			PREFETCH_WRITE(&*out_begin, 3);
			leaf_wk.process();
		} else {
			if(this->threads_per_clique & 0x01) {
				// Note that this over-submits tasks, one of these should not be a sub-tree, but a terminal task, but I'd need to re-compute dist_3 correctly to submit less work to the terminal task.
				const typename in_iterator::difference_type dist_3(dist / 3);
				const in_iterator in_middle1(std::next(this->begin, dist_3));
				const in2_iterator in_middle2(std::next(in_begin2, dist_3));
				const out_iterator out_middle(std::next(out_begin, dist_3));
				// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
				if(this->begin != in_middle1) {
					assert(this->first_buff_part() <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->first_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk3(this->pool, this->fn, this->all_done, algo_work_heap_type(this->first_buff_part(), this->odd_third_buff_range(), this->work_heap.stride), this->begin, in_middle1, in_begin2, in_middle2, out_begin, out_middle, this->threads_per_clique >> 1);
				}
				const in_iterator in_middle1_next(std::next(in_middle1, dist_3));
				const in_iterator in_middle2_next(std::next(in_middle2, dist_3));
				const out_iterator out_middle_next(std::next(out_middle, dist_3));
				if(in_middle1 != in_middle1_next) {
					assert((this->odd_second_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->odd_second_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk3(this->pool, this->fn, this->all_done, algo_work_heap_type(this->odd_second_buff_part() + this->work_heap.stride, this->odd_third_buff_range(), this->work_heap.stride), in_middle1, in_middle1_next, in_middle2, in_middle2_next, out_middle, out_middle_next, this->threads_per_clique >> 1);
				}
				if(in_middle1_next != this->end) {
					assert((this->odd_third_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					subdivide_n_gen_wk3(this->pool, this->fn, this->all_done, algo_work_heap_type(this->odd_third_buff_part() + this->work_heap.stride, this->odd_third_buff_range(), this->work_heap.stride), in_middle1_next, this->end, in_middle2_next, in_end2, out_middle_next, out_end, this->threads_per_clique >> 1).process();
				}
			} else {
				const in_iterator in_middle1(std::next(this->begin, dist >> 1));
				const in2_iterator in_middle2(std::next(in_begin2, dist >> 1));
				const out_iterator out_middle(std::next(out_begin, dist >> 1));
				// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
				if(this->begin != in_middle1) {
					assert(this->first_buff_part() <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					this->pool
						<< nonjoinable_t(this, gen_wk_node_str)
						// TODO								<<nonjoinable_t(this->first_buff_part(), this, gen_wk_node_str)
						<< subdivide_n_gen_wk3(
								this->pool,
								this->fn,
								this->all_done,
								algo_work_heap_type(this->first_buff_part(), this->even_half_buff_range(), this->work_heap.stride),
								this->begin,
								in_middle1,
								in_begin2,
								in_middle2,
								out_begin,
								out_middle,
								this->threads_per_clique >> 1);
				}
				if(in_middle1 != this->end) {
					assert((this->even_second_buff_part() + this->work_heap.stride) <= (this->work_heap.buffer + this->work_heap.stride * this->work_heap.size));
					subdivide_n_gen_wk3(
						this->pool,
						this->fn,
						this->all_done,
						algo_work_heap_type(this->even_second_buff_part() + this->work_heap.stride, this->even_half_buff_range(), this->work_heap.stride),
						in_middle1,
						this->end,
						in_middle2,
						in_end2,
						out_middle,
						out_end,
						this->threads_per_clique >> 1)
						.process();
				}
			}
		}
	}
}

template<class TPB, class BinOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk3<pool_traits::size_mode_t::infinite, TPB, BinOp, Conts, Alg>::subdivide_n_gen_wk3(
	thread_pool_type& p,
	operation_type& o,
	typename alg_wrap_t::work_complete_t& w,
	in_iterator const& ib1,
	in_iterator const& ie1,
	in2_iterator const& ib2,
	in2_iterator const& ie2,
	out_iterator const& ob,
	out_iterator const& oe) noexcept(true)
	: base_t(p, o, w, ib1, ie1),
	  in_begin2(ib2),
	  in_end2(ie2),
	  out_begin(ob),
	  out_end(oe) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().input2.size());
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
}

template<class TPB, class BinOp, class Conts, template<class, class> class Alg>
inline subdivide_n_gen_wk3<pool_traits::size_mode_t::infinite, TPB, BinOp, Conts, Alg>::subdivide_n_gen_wk3(thread_pool_type& p, operation_type& o, typename alg_wrap_t::work_complete_t& w, algo_work_heap_type const&, typename std::iterator_traits<in_iterator>::difference_type const, typename thread_pool_type::pool_type::size_type const) noexcept(true)
	: base_t(p, o, w),
	  in_begin2(this->all_done.containers().input2.begin()),
	  in_end2(this->all_done.containers().input2.end()),
	  out_begin(this->all_done.containers().output.begin()),
	  out_end(this->all_done.containers().output.end()) {
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().input2.size());
	assert(this->all_done.containers().input1.size() <= this->all_done.containers().output.size());
}

template<class TPB, class BinOp, class Conts, template<class, class> class Alg>
inline void
subdivide_n_gen_wk3<pool_traits::size_mode_t::infinite, TPB, BinOp, Conts, Alg>::process() noexcept(false) {
	typedef typename thread_pool_type::nonjoinable nonjoinable_t;

	const ensure_wk_complete_t e(this->all_done);
	const typename in_iterator::difference_type dist(std::distance(this->begin, this->end));
	if(dist > 0) {	  // In case a joker passes in an empty collection...
		if(dist <= 2) {	// We're at a leaf task, so generate no more, and directly process() the sub-range.
			alg_wrap_t leaf_wk(typename alg_wrap_t::work_wrap(this->begin, this->end, in_begin2, out_begin, this->fn), this->all_done);
			PREFETCH_READ(&*this->begin, 3);
			PREFETCH_READ(&*in_begin2, 3);
			PREFETCH_WRITE(&*out_begin, 3);
			leaf_wk.process();
		} else {
			const in_iterator in_middle1(std::next(this->begin, dist >> 1));
			const in2_iterator in_middle2(std::next(in_begin2, dist >> 1));
			const out_iterator out_middle(std::next(out_begin, dist >> 1));
			// We create to sub-tasks of the two sub-ranges, to generate a tree of sub-tasks.
			if(this->begin != in_middle1) {
				this->pool
					<< nonjoinable_t(this, gen_wk_node_str)
					<< subdivide_n_gen_wk3(
							this->pool,
							this->fn,
							this->all_done,
							this->begin,
							in_middle1,
							in_begin2,
							in_middle2,
							out_begin,
							out_middle);
			}
			if(in_middle1 != this->end) {
				subdivide_n_gen_wk3(
					this->pool,
					this->fn,
					this->all_done,
					in_middle1,
					this->end,
					in_middle2,
					in_end2,
					out_middle,
					out_end)
					.process();
			}
		}
	}
}

}}}}
