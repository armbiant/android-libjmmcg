/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ { namespace closure {

// Implementation details..... Don't look below here!!! ;)

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() noexcept(NoExcept), class CFG>
	inline __stdcall
	closure_void_static<InpWk, NoExcept, Proc, CFG>::closure_void_static(argument_type &&i, typename cfg_details_type::params const &p) noexcept(false)
	: argument_type(std::forward<argument_type>(i)), base_t(p) {
	}

	// If you have a compile-time error here regarding matching operator<(), then it is possible that the type you are attempting to transfer into the thread_pool does not have an operator<() member declared, because you might be using a pool_traits::prioritised_queue in the thread_pool, which requires it.
	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_void_static<InpWk, NoExcept, Proc, CFG>::operator<(const closure_void_static &tw) const noexcept(true) {
		return static_cast<argument_type const &>(*this)<static_cast<argument_type const &>(tw);
	}

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_void_static<InpWk, NoExcept, Proc, CFG>::operator==(const closure_void_static &tw) const noexcept(true) {
		return !(*this<tw) && !(tw<*this);
	}

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() noexcept(NoExcept), class CFG>
	inline const typename closure_void_static<InpWk, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_void_static<InpWk, NoExcept, Proc, CFG>::input() const noexcept(true) {
		return static_cast<argument_type const &>(*this);
	}

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() noexcept(NoExcept), class CFG>
	inline typename closure_void_static<InpWk, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_void_static<InpWk, NoExcept, Proc, CFG>::input() noexcept(true) {
		return static_cast<argument_type &>(*this);
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() noexcept(NoExcept), class CFG>
	inline void __fastcall
	closure_void_static<InpWk, NoExcept, Proc, CFG>::process() {
		(static_cast<argument_type &>(*this).*Proc)();
	}

#pragma GCC diagnostic pop

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() const noexcept(NoExcept), class CFG>
	inline __stdcall
	closure_void_static_const<InpWk, NoExcept, Proc, CFG>::closure_void_static_const(argument_type &&i, typename cfg_details_type::params const &p) noexcept(false)
	: argument_type(std::forward<argument_type>(i)), base_t(p) {
	}

	// If you have a compile-time error here regarding matching operator<(), then it is possible that the type you are attempting to transfer into the thread_pool does not have an operator<() member declared, because you might be using a pool_traits::prioritised_queue in the thread_pool, which requires it.
	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() const noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_void_static_const<InpWk, NoExcept, Proc, CFG>::operator<(const closure_void_static_const &tw) const noexcept(true) {
		return static_cast<argument_type const &>(*this)<static_cast<argument_type const &>(tw);
	}

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() const noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_void_static_const<InpWk, NoExcept, Proc, CFG>::operator==(const closure_void_static_const &tw) const noexcept(true) {
		return !(*this<tw) && !(tw<*this);
	}

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() const noexcept(NoExcept), class CFG>
	inline const typename closure_void_static_const<InpWk, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_void_static_const<InpWk, NoExcept, Proc, CFG>::input() const noexcept(true) {
		return static_cast<argument_type const &>(*this);
	}

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() const noexcept(NoExcept), class CFG>
	inline typename closure_void_static_const<InpWk, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_void_static_const<InpWk, NoExcept, Proc, CFG>::input() noexcept(true) {
		return static_cast<argument_type &>(*this);
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"

	template<class InpWk, bool NoExcept, void (__fastcall InpWk::*Proc)() const noexcept(NoExcept), class CFG>
	inline void __fastcall
	closure_void_static_const<InpWk, NoExcept, Proc, CFG>::process() {
		(static_cast<argument_type const &>(*this).*Proc)();
	}

#pragma GCC diagnostic pop

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline __stdcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::closure_static(argument_type &&i, typename cfg_details_type::params const &p) noexcept(false)
	: argument_type(std::forward<argument_type>(i)), base_t(p) {
	}

	// If you have a compile-time error here regarding matching operator<(), then it is possible that the type you are attempting to transfer into the thread_pool does not have an operator<() member declared, because you might be using a pool_traits::prioritised_queue in the thread_pool, which requires it.
	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::operator<(const closure_static &tw) const noexcept(true) {
		return static_cast<argument_type const &>(*this)<static_cast<argument_type const &>(tw);
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::operator==(const closure_static &tw) const noexcept(true) {
		return !(*this<tw) && !(tw<*this);
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline const typename closure_static<InpWk, Res, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::input() const noexcept(true) {
		return static_cast<argument_type const &>(*this);
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline typename closure_static<InpWk, Res, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::input() noexcept(true) {
		return static_cast<argument_type &>(*this);
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline void __fastcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::process() {
		(static_cast<argument_type &>(*this).*Proc)(result_);
	}

#pragma GCC diagnostic pop

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline const typename closure_static<InpWk, Res, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::get_results() const noexcept(false) {
		return result_;
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) noexcept(NoExcept), class CFG>
	inline typename closure_static<InpWk, Res, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static<InpWk, Res, NoExcept, Proc, CFG>::get_results() noexcept(false) {
		return result_;
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline __stdcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::closure_static_cfg(argument_type &&i, typename cfg_details_type::params const &p) noexcept(false)
	: argument_type(std::forward<argument_type>(i)), base_t(p), cfg_params(p) {
	}

	// If you have a compile-time error here regarding matching operator<(), then it is possible that the type you are attempting to transfer into the thread_pool does not have an operator<() member declared, because you might be using a pool_traits::prioritised_queue in the thread_pool, which requires it.
	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::operator<(const closure_static_cfg &tw) const noexcept(true) {
		return static_cast<argument_type const &>(*this)<static_cast<argument_type const &>(tw);
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::operator==(const closure_static_cfg &tw) const noexcept(true) {
		return !(*this<tw) && !(tw<*this);
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline const typename closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::input() const noexcept(true) {
		return static_cast<argument_type const &>(*this);
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline typename closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::input() noexcept(true) {
		return static_cast<argument_type &>(*this);
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline void __fastcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::process() {
		(static_cast<argument_type &>(*this).*Proc)(result_, cfg_params);
	}

#pragma GCC diagnostic pop

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline const typename closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::get_results() const noexcept(false) {
		return result_;
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) noexcept(NoExcept), class CFG>
	inline typename closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static_cfg<InpWk, Res, CFGP, NoExcept, Proc, CFG>::get_results() noexcept(false) {
		return result_;
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline __stdcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::closure_static_const(argument_type &&i, typename cfg_details_type::params const &p) noexcept(false)
	: argument_type(std::forward<argument_type>(i)), base_t(p) {
	}

	// If you have a compile-time error here regarding matching operator<(), then it is possible that the type you are attempting to transfer into the thread_pool does not have an operator<() member declared, because you might be using a pool_traits::prioritised_queue in the thread_pool, which requires it.
	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::operator<(const closure_static_const &tw) const noexcept(true) {
		return static_cast<argument_type const &>(*this)<static_cast<argument_type const &>(tw);
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::operator==(const closure_static_const &tw) const noexcept(true) {
		return !(*this<tw) && !(tw<*this);
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline const typename closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::input() const noexcept(true) {
		return static_cast<argument_type const &>(*this);
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline typename closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::input() noexcept(true) {
		return static_cast<argument_type &>(*this);
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline void __fastcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::process() {
		(static_cast<argument_type const &>(*this).*Proc)(result_);
	}

#pragma GCC diagnostic pop

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline const typename closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::get_results() const noexcept(true) {
		return result_;
	}

	template<class InpWk, class Res, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &) const noexcept(NoExcept), class CFG>
	inline typename closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static_const<InpWk, Res, NoExcept, Proc, CFG>::get_results() noexcept(false) {
		return result_;
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline __stdcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::closure_static_cfg_const(argument_type &&i, typename cfg_details_type::params const &p) noexcept(false)
	: argument_type(std::forward<argument_type>(i)), base_t(p), cfg_params(p) {
	}

	// If you have a compile-time error here regarding matching operator<(), then it is possible that the type you are attempting to transfer into the thread_pool does not have an operator<() member declared, because you might be using a pool_traits::prioritised_queue in the thread_pool, which requires it.
	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::operator<(const closure_static_cfg_const &tw) const noexcept(true) {
		return static_cast<argument_type const &>(*this)<static_cast<argument_type const &>(tw);
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline bool __fastcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::operator==(const closure_static_cfg_const &tw) const noexcept(true) {
		return !(*this<tw) && !(tw<*this);
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline const typename closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::input() const noexcept(true) {
		return static_cast<argument_type const &>(*this);
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline typename closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::argument_type & __fastcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::input() noexcept(true) {
		return static_cast<argument_type &>(*this);
	}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-aliasing"

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline void __fastcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::process() {
		(static_cast<argument_type const &>(*this).*Proc)(result_, cfg_params);
	}

#pragma GCC diagnostic pop

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline const typename closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::get_results() const noexcept(true) {
		return result_;
	}

	template<class InpWk, class Res, class CFGP, bool NoExcept, void (__fastcall InpWk::*Proc)(Res &, CFGP const &) const noexcept(NoExcept), class CFG>
	inline typename closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::result_type & __fastcall
	closure_static_cfg_const<InpWk, Res, CFGP, NoExcept, Proc, CFG>::get_results() noexcept(false) {
		return result_;
	}

} } } } }
