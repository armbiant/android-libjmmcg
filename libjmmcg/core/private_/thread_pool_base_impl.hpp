/******************************************************************************
** Copyright © 2014 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace private_ {

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class InpWk>
struct thread_pool_base<DM, Ps, PTT, Pt>::execution_context_stack final : public private_::execution_context_stack_type<work_distribution_mode, pool_traits_type::result_traits_, thread_pool_base, InpWk> {
	using base_t= private_::execution_context_stack_type<work_distribution_mode, pool_traits_type::result_traits_, thread_pool_base, InpWk>;
	// TODO fails:	BOOST_MPL_ASSERT((std::is_same<typename base_t::thread_wk_t::closure_t::argument_type, InpWk>));
	using base_t::base_t;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename InpWk,
	class PtrFnType>
struct thread_pool_base<DM, Ps, PTT, Pt>::create_direct final : public private_::create_direct<pool_traits_type, InpWk, PtrFnType, &std::remove_reference<InpWk>::type::process> {
	typedef private_::create_direct<pool_traits_type, InpWk, PtrFnType, &std::remove_reference<InpWk>::type::process> base_t;
	typedef typename base_t::process_fn_traits process_fn_traits;
	using closure_t= typename base_t::closure_t;
	typedef typename process_fn_traits::result_type result_type;
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see execution_context_stack_type
	*/
	typedef typename thread_pool_base<DM, Ps, PTT, Pt>::template execution_context_stack<InpWk> execution_context_stack;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	class Fn>
class thread_pool_base<DM, Ps, PTT, Pt>::for_each_t {
private:
	typedef Fn operation_type;
	typedef alg_wk_wrap::for_each_work_type<operation_type> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk1<size_mode, thread_pool_base, typename creator_t::closure_t, one_container<Colln>, alg_wk_wrap::for_each_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall for_each_t(thread_pool_base& p, Colln const& c, operation_type const& f) noexcept(true)
		: pool(p), colln(c), fn(f) {
	}

	/// Joinably transfer the work to the pool.
	/**
		\see thread_wk_t, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			cfg_parms,
			typename execution_context::thread_wk_t::closure_t::argument_type(fn),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(colln),
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	Colln const& colln;
	operation_type const& fn;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename Pred>
class thread_pool_base<DM, Ps, PTT, Pt>::count_if_t {
public:
	typedef Pred operation_type;

private:
	typedef typename os_traits::lock_traits::template atomic_counter_type<typename Colln::size_type> num_elems_ct_t;
	typedef alg_wk_wrap::countor_work_type<operation_type, num_elems_ct_t> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk1<size_mode, thread_pool_base, typename creator_t::closure_t, one_container<Colln>, alg_wk_wrap::count_if_reduce> gen_wk_t;

public:
	/// A bit of syntactic sugar: allow the user to not have to double-dereference the execution_context to get at the result_type. This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type, deref::extra_deref, core_work_result::to_zero> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (num_elems_ct_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	/**
		If this assertion fails, then the counter type in num_elems_ct_t needs changing.

		\see num_elems_ct_t
	*/
	BOOST_MPL_ASSERT((std::is_same<typename execution_context::result_type::value_type, typename Colln::size_type>));

	__stdcall count_if_t(thread_pool_base& p, Colln const& c, operation_type const& pr) noexcept(true)
		: pool(p), colln(c), pred(pr) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("count_if", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(pred),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(colln),
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	Colln const& colln;
	operation_type const pred;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename Pred>
class thread_pool_base<DM, Ps, PTT, Pt>::find_first_of_t {
public:
	using operation_type= Pred;
	using element_type= std::uint32_t;
	using result_type= std::vector<element_type>;

private:
	using all_intermendiate_results_type= std::vector<std::optional<element_type>>;

public:
	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= ppd::generic_traits::memory_access_modes::crew_memory_access;

	/**
		\param r	A range is natural numbers [N, ..., M) that should be searched.
	 */
	__stdcall find_first_of_t(thread_pool_base& p, std::pair<element_type, element_type> const& r, operation_type const& pr) noexcept(true)
		: pool(p), range(r), pred(pr) {
		assert(range.second > range.first);
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\todo It would be nice to spawn the work logarithmically for more optimal operation, as the O(2*thread_pool_base::pool_size()) would become O(2*log(thread_pool_base::pool_size())), but this only really matters if thread_pool_base::pool_size()>1000s due to the costs of threading...

		\todo This algorithm does not respect cliques, but does permit horizontal-threading, so resource-starvation in the pool is avoided, if threads become blocked.

		Algorithmic complexity: O((M-N)/thread_pool_base::pool_size()+2*thread_pool_base::pool_size()), where

		\see create_direct, execution_context, cliques, cliques_t
	*/
	void
	process(result_type& res) noexcept(false) {
		std::atomic_flag exit_threads{};
		all_intermendiate_results_type all_intermendiate_results;
		all_intermendiate_results.resize(pool.pool_size() + 1);
		spawn_tasks(0, exit_threads, all_intermendiate_results);
		res.clear();
		res.reserve(all_intermendiate_results.size());
		// I am assuming that the pool-size is not so huge that this O(n) operation will dominate...
		auto last_valid= std::remove(
			all_intermendiate_results.begin(),
			all_intermendiate_results.end(),
			std::nullopt);
		std::transform(
			all_intermendiate_results.begin(),
			last_valid,
			std::back_inserter(res),
			[](auto const& v) noexcept(true) {
				assert(v.has_value());
				return *v;
			});
	}

	bool operator<(find_first_of_t const& i) const noexcept(true) {
		return range < i.range;
	}

private:
	class task {
	public:
		using result_type= std::optional<element_type>;

		task(std::atomic_flag& et, operation_type const& p, element_type b, element_type e) noexcept(false)
			: exit_threads(et), pred(p), begin(b), end(e) {
		}

		void process(result_type& res) noexcept(false) {
			for(element_type mask= begin; mask < end && !exit_threads.test(std::memory_order_relaxed); ++mask) {
				if(auto const ret= pred(mask); ret) {
					exit_threads.test_and_set(std::memory_order_seq_cst);
					res= ret;
					assert(res);
					return;
				}
			}
			res= std::nullopt;
		}

		bool operator<(task const& v) const noexcept(true) {
			return begin < v.begin && end < v.end;
		}

	private:
		std::atomic_flag& exit_threads;
		operation_type pred;
		element_type const begin;
		element_type const end;
	};

	thread_pool_base& pool;
	std::pair<element_type, element_type> const range;
	operation_type const pred;

	void spawn_tasks(std::size_t task_number, std::atomic_flag& exit_threads, all_intermendiate_results_type& res) noexcept(false) {
		if(task_number < pool.pool_size()) {
			const auto size= range.second - range.first;
			auto const& possible_mask_cxt= pool << joinable(&pool, "find_first_of_task") << task(exit_threads, pred, range.first + static_cast<element_type>(task_number * size / pool.pool_size()), range.first + static_cast<element_type>((task_number + 1) * size / pool.pool_size()));
			spawn_tasks(task_number + 1, exit_threads, res);
			res[task_number]= *possible_mask_cxt;
		}
	}
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename Pred>
class thread_pool_base<DM, Ps, PTT, Pt>::find_if_t {
public:
	typedef Pred operation_type;

private:
	typedef stl_functor_result_type<bool> found_t;
	typedef alg_wk_wrap::countor_work_type<operation_type, found_t> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk1<size_mode, thread_pool_base, typename creator_t::closure_t, one_container<Colln>, alg_wk_wrap::find_if_reduce> gen_wk_t;

public:
	/// A bit of syntactic sugar: allow the user to not have to double-dereference the execution_context to get at the result_type. This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type, deref::extra, core_work_result::to_false> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall find_if_t(thread_pool_base& p, Colln const& c, operation_type const& pr) noexcept(true)
		: pool(p), colln(c), pred(pr) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\todo It would be nice if this algorithm checked if the item had been found and didn't subsequently blindly continue to add more work search other sub-ranges, i.e. terminated early.

		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("find_if", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(pred),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(colln),
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	Colln const& colln;
	operation_type const pred;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn,
	typename CollnOut,
	typename UniOp>
class thread_pool_base<DM, Ps, PTT, Pt>::transform_t {
public:
	typedef UniOp operation_type;

private:
	typedef alg_wk_wrap::transform_work_type<operation_type> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk2<size_mode, thread_pool_base, typename creator_t::closure_t, two_containers<CollnIn, CollnOut>, alg_wk_wrap::transform_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnIn::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnOut::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall transform_t(thread_pool_base& p, CollnIn const& i, CollnOut& o, operation_type const& op) noexcept(true)
		: pool(p), in(i), out(o), uniop(op) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		const typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type containers(in, out);
		return execution_context(
			pool,
			cfg_parms,
			typename execution_context::thread_wk_t::closure_t::argument_type(uniop),
			init_num_jobs_par_alg_other,
			containers,
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	CollnIn const& in;
	CollnOut& out;
	operation_type const& uniop;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn,
	typename CollnOut,
	class IterIn,
	typename UniOp>
class thread_pool_base<DM, Ps, PTT, Pt>::transform_iter_t {
public:
	typedef UniOp operation_type;

protected:
	typedef alg_wk_wrap::for_each_work_type<operation_type> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk2<size_mode, thread_pool_base, typename creator_t::closure_t, two_ranges<CollnIn, CollnOut, IterIn, typename CollnOut::container_type::iterator>, alg_wk_wrap::transform_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall transform_iter_t(thread_pool_base& p, IterIn b1, IterIn e1, typename CollnOut::container_type::iterator b2, operation_type const& op) noexcept(true)
		: pool(p), beg1(b1), end1(e1), beg2(b2), uniop(op) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			cfg_parms,
			typename execution_context::thread_wk_t::closure_t::argument_type(uniop),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(beg1, end1, beg2),
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	IterIn beg1;
	IterIn end1;
	typename CollnOut::container_type::iterator beg2;
	operation_type const& uniop;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn1,
	typename CollnIn2,
	typename CollnOut,
	typename BinOp>
class thread_pool_base<DM, Ps, PTT, Pt>::transform2_t {
public:
	typedef BinOp operation_type;

private:
	typedef alg_wk_wrap::transform_work_type<operation_type> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk3<size_mode, thread_pool_base, typename creator_t::closure_t, three_containers<CollnIn1, CollnIn2, CollnOut>, alg_wk_wrap::transform2_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnIn1::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnIn2::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnOut::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall transform2_t(thread_pool_base& p, CollnIn1 const& i1, CollnIn2 const& i2, CollnOut& o, operation_type const& op) noexcept(true)
		: pool(p), in1(i1), in2(i2), out(o), binop(op) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		const typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type containers(in1, in2, out);
		return execution_context(
			pool,
			cfg_parms,
			typename execution_context::thread_wk_t::closure_t::argument_type(binop),
			init_num_jobs_par_alg_other,
			containers,
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	CollnIn1 const& in1;
	CollnIn2 const& in2;
	CollnOut& out;
	operation_type const binop;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class Res>
struct thread_pool_base<DM, Ps, PTT, Pt>::map_red_initialiser {
	typedef void argument_type;
	typedef Res result_type;

	result_type const val;

	explicit map_red_initialiser(result_type const& va)
		: val(va) {}

	result_type const& __fastcall operator()() const noexcept(true) {
		return val;
	}
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename BinOp,
	class V,
	template<class, class>
	class Reduce,
	class Init>
class thread_pool_base<DM, Ps, PTT, Pt>::map_reduce_t {
public:
	typedef BinOp operation_type;

protected:
	typedef Init initialiser;

private:
	typedef typename os_traits::lock_traits::template atomic_counter_type<V> accumulated_res_t;
	typedef alg_wk_wrap::accumulator_work_type<operation_type, accumulated_res_t> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk1<size_mode, thread_pool_base, typename creator_t::closure_t, one_container<Colln>, Reduce> gen_wk_t;

public:
	/// A bit of syntactic sugar: allow the user to not have to double-dereference the execution_context to get at the result_type. This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type, deref::extra_deref, core_work_result::to_op> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (accumulated_res_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	map_reduce_t(thread_pool_base& p, Colln const& c, initialiser const& i, operation_type const& op, typename cfg_type::node_property_t::value_type const* n_d) noexcept(true)
		: pool(p), colln(c), init_val(i), binop(op), node_details(n_d) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename creator_t::closure_t::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params(node_details, cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(binop, typename execution_context::thread_wk_t::closure_t::result_type(init_val.operator()())),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(colln),
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	Colln const& colln;
	initialiser const init_val;
	operation_type const binop;
	typename cfg_type::node_property_t::value_type const* node_details;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class Colln>
struct thread_pool_base<DM, Ps, PTT, Pt>::max_element_initialiser {
	typedef typename Colln::value_type result_type;

	Colln const& colln;

	explicit max_element_initialiser(Colln const& c) noexcept(true)
		: colln(c) {}

	result_type __fastcall operator()() const noexcept(true) {
		return !colln.colln().empty() ? *colln.colln().begin() : std::numeric_limits<result_type>::min();
	}
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<class Colln>
struct thread_pool_base<DM, Ps, PTT, Pt>::min_element_initialiser {
	typedef typename Colln::value_type result_type;

	Colln const& colln;

	explicit min_element_initialiser(Colln const& c) noexcept(true)
		: colln(c) {}

	result_type __fastcall operator()() const noexcept(true) {
		return !colln.colln().empty() ? *colln.colln().begin() : std::numeric_limits<result_type>::max();
	}
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln>
class thread_pool_base<DM, Ps, PTT, Pt>::fill_n_t {
private:
	typedef alg_wk_wrap::pass_value<typename Colln::value_type const> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk1<size_mode, thread_pool_base, typename creator_t::closure_t, one_output_container_rw_lk<Colln>, alg_wk_wrap::fill_n_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall fill_n_t(thread_pool_base& p, Colln& c, typename Colln::size_type sz, typename work_type::element_type& v) noexcept(true)
		: pool(p), colln(c), size(sz), val(v) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("fill_n", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(val),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(colln),
			cliques,
			default_num_subranges,
			size);
	}

private:
	thread_pool_base& pool;
	Colln& colln;
	const typename Colln::size_type size;
	typename work_type::element_type& val;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln>
class thread_pool_base<DM, Ps, PTT, Pt>::fill_t {
private:
	typedef alg_wk_wrap::pass_value<typename Colln::value_type const> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk1<size_mode, thread_pool_base, typename creator_t::closure_t, one_output_container_simple_lk<Colln>, alg_wk_wrap::fill_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall fill_t(thread_pool_base& p, Colln& c, typename work_type::element_type& v) noexcept(true)
		: pool(p), colln(c), val(v) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("fill", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(val),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(colln),
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	Colln& colln;
	typename work_type::element_type& val;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln>
class thread_pool_base<DM, Ps, PTT, Pt>::reverse_t {
private:
	typedef Colln argument_type;
	typedef alg_wk_wrap::reverse_work_type<argument_type> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk1<size_mode, thread_pool_base, typename creator_t::closure_t, one_output_container_simple_lk<Colln>, alg_wk_wrap::reverse_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall reverse_t(thread_pool_base& p, Colln& c) noexcept(true)
		: pool(p), colln(c) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		const unsigned short half_subrange= 2;
		typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type containers(colln);
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("reverse", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(work_type(containers.input1.begin(), containers.input1.end())),
			init_num_jobs_par_alg_other,
			containers,
			cliques,
			half_subrange);
	}

private:
	thread_pool_base& pool;
	Colln& colln;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn1,
	typename CollnIn2,
	typename CollnOut,
	typename Compare>
class thread_pool_base<DM, Ps, PTT, Pt>::merge_t {
public:
	typedef Compare operation_type;

private:
	typedef alg_wk_wrap::merge_work_type<operation_type, thread_pool_base> work_type;
	typedef create_direct<work_type> creator_t;
	typedef alg_wrapper3<pool_traits_type, alg_wk_wrap::batchers_bitonic_merge_reduce<three_containers<CollnIn1, CollnIn2, CollnOut>, typename creator_t::closure_t>, pool_traits_type::result_traits_> alg_wrap_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_stack_type
		\see joinable
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_stack_type<work_distribution_mode, pool_traits_type::result_traits_, thread_pool_base, pool_traits_type::template algo_thread_wk, alg_wrap_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && alg_wrap_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnIn1::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnIn2::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && CollnOut::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall merge_t(thread_pool_base& p, CollnIn1 const& i1, CollnIn2 const& i2, CollnOut& o, operation_type const& op) noexcept(true)
		: pool(p), in1(i1), in2(i2), out(o), comp(op) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\todo Move sorter to the pool.merge() call so that users can specify alternative underlying sort operations.

		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context __fastcall process(const typename thread_pool_base::pool_type::size_type clique, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		const typename alg_wrap_t::work_complete_t::containers_type containers(in1, in2, out);
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("merge", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(comp, pool),
			init_num_jobs_par_alg_other,
			containers,
			clique);
	}

private:
	thread_pool_base& pool;
	CollnIn1 const& in1;
	CollnIn2 const& in2;
	CollnOut& out;
	operation_type const& comp;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename Compare>
class thread_pool_base<DM, Ps, PTT, Pt>::sort_t {
public:
	typedef Compare operation_type;

private:
	typedef alg_wk_wrap::sort_work_type<operation_type, thread_pool_base> work_type;
	typedef create_direct<work_type> creator_t;
	typedef alg_wrapper1<pool_traits_type, alg_wk_wrap::bitonic_sort_reduce<one_output_container_simple_lk<Colln>, typename creator_t::closure_t>, pool_traits_type::result_traits_> alg_wrap_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_stack_type<work_distribution_mode, pool_traits_type::result_traits_, thread_pool_base, pool_traits_type::template algo_thread_wk, alg_wrap_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && alg_wrap_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && Colln::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall sort_t(thread_pool_base& p, Colln& c, operation_type const& op) noexcept(true)
		: pool(p), colln(c), comp(op) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context __fastcall process(const typename thread_pool_base::pool_type::size_type clique, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("sort", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(comp, pool),
			init_num_jobs_par_alg_other,
			typename alg_wrap_t::work_complete_t::containers_type(colln),
			clique);
	}

private:
	thread_pool_base& pool;
	Colln& colln;
	operation_type const& comp;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	class Pred>
class thread_pool_base<DM, Ps, PTT, Pt>::swap_ranges_t {
public:
	typedef Pred operation_type;

private:
	typedef alg_wk_wrap::for_each_work_type<operation_type> work_type;
	typedef create_direct<work_type> creator_t;
	typedef subdivide_n_gen_wk2<size_mode, thread_pool_base, typename creator_t::closure_t, two_out_ranges<Colln, Colln, typename Colln::container_type::iterator, typename Colln::container_type::iterator>, alg_wk_wrap::swap_ranges_reduce> gen_wk_t;

public:
	/// This is a useful typedef to get at the execution_context.
	/**
		The execution_context is created by joinably transferring work into the pool. It has various uses, but is primarily used to atomically and synchronously wait on the results of the work on the closure_base-derived closure-derived object, as specified by the thread_wk_t object transferred into the pool. But it can also pass back specified exceptions that may be thrown by the work. It can also be used to asynchronously test if the work has been completed, and delete the work from the pool, if it has not been started.

		\see create_direct
		\see execution_context_algo_buff_stack_type
		\see joinable_t
		\see thread_wk_t
		\see closure_base
	*/
	typedef execution_context_algo_buff_stack_type<work_distribution_mode, pool_traits_type::result_traits_, pool_traits_type::template algo_thread_wk_buffered, gen_wk_t, work_type> execution_context;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (work_type::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && gen_wk_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && execution_context::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);

	__stdcall swap_ranges_t(thread_pool_base& p, typename Colln::container_type::iterator b1, typename Colln::container_type::iterator e1, typename Colln::container_type::iterator b2, Pred const& pr) noexcept(true)
		: pool(p), beg1(b1), end1(e1), beg2(b2), pred(pr) {
	}

	/// Joinably transfer the predicate to the pool.
	/**
		\see create_direct, execution_context, cliques, cliques_t
	*/
	execution_context
	process(cliques::element_type const cliques, typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params const& cfg_parms) const {
		return execution_context(
			pool,
			typename pool_traits_type::thread_wk_elem_type::cfg_details_type::params("swap_ranges", cfg_parms),
			typename execution_context::thread_wk_t::closure_t::argument_type(pred),
			init_num_jobs_par_alg_other,
			typename gen_wk_t::alg_wrap_t::work_complete_t::containers_type(beg1, end1, beg2),
			cliques,
			default_num_subranges);
	}

private:
	thread_pool_base& pool;
	typename Colln::container_type::iterator beg1;
	typename Colln::container_type::iterator end1;
	typename Colln::container_type::iterator beg2;
	Pred const& pred;
};

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename Fn>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template for_each_t<Colln, Fn>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::for_each(Colln const& c, Fn const& fn) {
	typedef parallel_algorithm<for_each_t<Colln, Fn>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, fn));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	class Pred>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template count_if_t<Colln, Pred>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::count_if(Colln const& c, Pred const& p) {
	typedef parallel_algorithm<count_if_t<Colln, Pred>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, p));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template count_t<Colln>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::count(Colln const& c, typename Colln::value_type const& v) {
	typedef parallel_algorithm<count_t<Colln>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, v));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Pred>
inline auto __fastcall thread_pool_base<DM, Ps, PTT, Pt>::find_first_of(std::pair<std::size_t, std::size_t> const& range, Pred const& p) {
	using reduction_t= find_first_of_t<Pred>;
	assert(range.second > range.first);
	this->set_statistics().update_colln_stats(range.second - range.first);
	return reduction_t(*this, range, p);
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	class Pred>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template find_if_t<Colln, Pred>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::find_if(Colln const& c, Pred const& p) {
	typedef parallel_algorithm<find_if_t<Colln, Pred>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, p));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template find_t<Colln>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::find(Colln const& c, typename Colln::value_type const& v) {
	typedef parallel_algorithm<find_t<Colln>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, v));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn,
	typename CollnOut,
	class UniOp>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template transform_t<CollnIn, CollnOut, UniOp>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::transform(CollnIn const& in, CollnOut& out, UniOp const& uniop) {
	typedef parallel_algorithm<transform_t<CollnIn, CollnOut, UniOp>> reduction_t;
	this->set_statistics().update_colln_stats(in.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, in, out, uniop));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn1,
	typename CollnIn2,
	typename CollnOut,
	class BinOp>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template transform2_t<CollnIn1, CollnIn2, CollnOut, BinOp>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::transform(CollnIn1 const& in1, CollnIn2 const& in2, CollnOut& out, BinOp const& binop) {
	typedef parallel_algorithm<transform2_t<CollnIn1, CollnIn2, CollnOut, BinOp>> reduction_t;
	this->set_statistics().update_colln_stats(in1.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, in1, in2, out, binop));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn,
	typename CollnOut>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template transform_t<CollnIn, CollnOut, noop<typename CollnOut::value_type>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::copy(CollnIn const& in, CollnOut& out) {
	return transform(in, out, noop<typename CollnOut::value_type>());
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename BinOp>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template accumulate_op_processor<Colln, BinOp>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::accumulate(Colln const& c, typename BinOp::result_type const& v, BinOp const& binop) {
	typedef parallel_algorithm<accumulate_op_processor<Colln, BinOp>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, v, binop));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	class V>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template accumulate_processor<Colln, V>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::accumulate(Colln const& c, V const& v) {
	typedef parallel_algorithm<accumulate_processor<Colln, V>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, v));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template fill_n_t<Colln>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::fill_n(Colln& c, typename Colln::size_type sz, typename Colln::value_type const& v) {
	typedef parallel_algorithm<fill_n_t<Colln>> reduction_t;
	this->set_statistics().update_colln_stats(sz);
	return reduction_t(typename reduction_t::operation_type(*this, c, sz, v));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template fill_t<Colln>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::fill(Colln& c, typename Colln::value_type const& v) {
	typedef parallel_algorithm<fill_t<Colln>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, v));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template reverse_t<Colln>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::reverse(Colln& c) {
	typedef parallel_algorithm<reverse_t<Colln>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	class Comp>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template max_element_t<Colln, Comp>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::max_element(Colln const& c, Comp const& comp) {
	typedef parallel_algorithm<max_element_t<Colln, Comp>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, comp));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template max_element_t<Colln, std::less<typename Colln::value_type>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::max_element(Colln const& c) {
	return max_element(c, std::less<typename Colln::value_type>());
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	class Comp>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template min_element_t<Colln, Comp>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::min_element(Colln const& c, Comp const& comp) {
	typedef parallel_algorithm<min_element_t<Colln, Comp>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, comp));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template min_element_t<Colln, std::less<typename Colln::value_type>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::min_element(Colln const& c) {
	return min_element(c, std::less<typename Colln::value_type>());
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn1,
	typename CollnIn2,
	typename CollnOut,
	class Compare>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template merge_t<CollnIn1, CollnIn2, CollnOut, Compare>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::merge(CollnIn1 const& in1, CollnIn2 const& in2, CollnOut& out, Compare const& comp) {
	typedef parallel_algorithm<merge_t<CollnIn1, CollnIn2, CollnOut, Compare>> reduction_t;
	this->set_statistics().update_colln_stats(in1.colln().size() + in2.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, in1, in2, out, comp));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn1,
	typename CollnIn2,
	typename CollnOut>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template merge_t<CollnIn1, CollnIn2, CollnOut, std::less<typename CollnOut::value_type>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::merge(CollnIn1 const& in1, CollnIn2 const& in2, CollnOut& out) {
	return merge(in1, in2, out, std::less<typename CollnIn1::value_type>());
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename Compare>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template sort_t<Colln, Compare>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::sort(Colln& c, Compare const& comp) {
	typedef parallel_algorithm<sort_t<Colln, Compare>> reduction_t;
	this->set_statistics().update_colln_stats(c.colln().size());
	return reduction_t(typename reduction_t::operation_type(*this, c, comp));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename Colln>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template sort_t<Colln, std::less<typename Colln::value_type>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::sort(Colln& in) {
	return sort(in, std::less<typename Colln::value_type>());
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class ArgT,
	class UniFn>
inline typename thread_pool_base<DM, Ps, PTT, Pt>::template execution_context_stack<unary_fun_work_type<ArgT, UniFn, thread_pool_base<DM, Ps, PTT, Pt>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::unary_fun(ArgT&& a, UniFn const& op) {
	typedef unary_fun_work_type<ArgT, UniFn, thread_pool_base> work_type;

	return *this << joinable(this, unary_fun_str) << work_type(std::forward<ArgT>(a), op, *this);
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class LHSArg,
	class RHSArg,
	class BinFn>
inline typename thread_pool_base<DM, Ps, PTT, Pt>::template execution_context_stack<binary_fun_work_type<LHSArg, RHSArg, BinFn, thread_pool_base<DM, Ps, PTT, Pt>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::binary_fun(LHSArg&& lhs, RHSArg&& rhs, BinFn const& op) {
	typedef binary_fun_work_type<LHSArg, RHSArg, BinFn, thread_pool_base> work_type;

	return *this << joinable(this, binary_fun_str) << work_type(std::forward<LHSArg>(lhs), std::forward<RHSArg>(rhs), op, *this);
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class T>
inline typename thread_pool_base<DM, Ps, PTT, Pt>::template execution_context_stack<binary_fun_work_type<T const, T const, std::logical_and<bool>, thread_pool_base<DM, Ps, PTT, Pt>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::logical_and(T&& lhs, T&& rhs) {
	return binary_fun<T const, T const, std::logical_and<bool>>(std::forward<T>(lhs), std::forward<T>(rhs));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class T>
inline typename thread_pool_base<DM, Ps, PTT, Pt>::template execution_context_stack<binary_fun_work_type<T const, T const, std::logical_or<bool>, thread_pool_base<DM, Ps, PTT, Pt>>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::logical_or(T&& lhs, T&& rhs) {
	return binary_fun<T const, T const, std::logical_or<bool>>(std::forward<T>(lhs), std::forward<T>(rhs));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class Colln,
	typename Pred>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template swap_ranges_t<Colln, Pred>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::swap_ranges(typename Colln::container_type::iterator b1, typename Colln::container_type::iterator e1, typename Colln::container_type::iterator b2, Pred const& p) {
	typedef parallel_algorithm<swap_ranges_t<Colln, Pred>> reduction_t;
	this->set_statistics().update_colln_stats(std::distance(b1, e1));
	return reduction_t(typename reduction_t::operation_type(*this, b1, e1, b2, p));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	typename CollnIn,
	typename CollnOut,
	class IterIn,
	class UniOp>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template transform_iter_t<CollnIn, CollnOut, IterIn, UniOp>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::transform(IterIn b1, IterIn e1, typename CollnOut::container_type::iterator b2, UniOp const& uniop) {
	typedef parallel_algorithm<transform_iter_t<CollnIn, CollnOut, IterIn, UniOp>> reduction_t;
	this->set_statistics().update_colln_stats(std::distance(b1, e1));
	return reduction_t(typename reduction_t::operation_type(*this, b1, e1, b2, uniop));
}

template<
	class DM,
	pool_traits::size_mode_t::element_type Ps,
	typename PTT,
	class Pt>
template<
	class CollnIn,
	class CollnOut,
	class IterIn>
inline parallel_algorithm<typename thread_pool_base<DM, Ps, PTT, Pt>::template copy_iter_t<CollnIn, CollnOut, IterIn>> __fastcall thread_pool_base<DM, Ps, PTT, Pt>::copy(IterIn b1, IterIn e1, typename CollnOut::container_type::iterator b2) {
	typedef parallel_algorithm<copy_iter_t<CollnIn, CollnOut, IterIn>> reduction_t;
	this->set_statistics().update_colln_stats(std::distance(b1, e1));
	return reduction_t(typename reduction_t::operation_type(*this, b1, e1, b2));
}

template<
	class DM1,
	pool_traits::size_mode_t::element_type Ps1,
	typename PTT1,
	class Pt1>
inline tostream& __fastcall
operator<<(tostream& os, thread_pool_base<DM1, Ps1, PTT1, Pt1> const& t) {
	os
		<< _T("Pool=0x") << &t
		<< _T(", type: ") << boost::core::demangle(typeid(t).name())
		<< _T(", size_mode=") << t.size_mode
		<< _T(", memory_access_mode=") << t.memory_access_mode
		<< _T(", max_num_threads_in_pool=") << t.max_num_threads_in_pool;
	return os;
}

}}}}
