#ifndef LIBJMMCG_CORE_PRIVATE_POOL_THREAD_HPP
#define LIBJMMCG_CORE_PRIVATE_POOL_THREAD_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "thread_base.hpp"

#include "thread_pool_queue_model.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd { namespace pool { namespace private_ {

/// This class of threads is used within the various thread_pool classes.
/**
	Implementation details:
	1. It uses a shared exit event (which is owned by the thread pool class). This optimisation allows parallel exiting of the threads within the pool. Thus the destructor of the pool is faster.
*/
template<
	typename OST,
	class ExitReq	 ///< \todo is this really needed?
	>
class pool_thread : public ppd::private_::thread_base<OST::thread_traits::api_params_type::api_type, typename OST::thread_traits::model_type> {
public:
	using base_t= ppd::private_::thread_base<OST::thread_traits::api_params_type::api_type, typename OST::thread_traits::model_type>;
	using os_traits= OST;
	using model_type= typename OST::thread_traits::model_type;
	using exit_requested_type= ExitReq;	  ///< The exit event type.

	static inline constexpr const generic_traits::api_type::element_type api_type= OST::thread_traits::api_params_type::api_type;

	~pool_thread() noexcept(false) {}

	const tstring __fastcall to_string() const noexcept(false) override final {
		tostringstream str;
		str << base_t::to_string()
			 << _T("\nexit_requested signal ptr=") << &exit_requested_;
		return str.str();
	}

protected:
	/// This event is used by the pool to request that all thread members of the pool should exit.
	exit_requested_type& exit_requested_;

	explicit __stdcall pool_thread(exit_requested_type& exit_r, const typename os_traits::thread_traits::api_params_type::suspend_period_ms exit_wait_p= 50) noexcept(false)
		: base_t(exit_wait_p), exit_requested_(exit_r) {
	}

private:
	/**
		Pool threads should only exit when the thread_pool exits.
	*/
	void __fastcall request_exit() const override {}
};

/// These are the various thread-types that are used within the various thread pools.
namespace thread_types {
/// For work-stealing type thread_pools.
template<generic_traits::return_data::element_type RD_, class OST, class PTT, class QM>
class steal;

/// For master-slave type thread_pools.
template<generic_traits::return_data::element_type RD_, class OST, class PTT>
class slave;
}

}}}}}

#include "../../core/thread_pool_aspects.hpp"

#ifdef WIN32
#	include "../../experimental/NT-based/NTSpecific/pool_thread.hpp"
#else
#	include "../../unix/pool_thread.hpp"
#endif

#endif
