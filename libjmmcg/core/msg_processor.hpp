#ifndef LIBJMMCG_CORE_MSG_PROCESSOR_HPP
#define LIBJMMCG_CORE_MSG_PROCESSOR_HPP

/******************************************************************************
** Copyright © 2016 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "latency_timestamps.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket {

/// Read a message from the source socket, process it according to the supplied rules and write the resultant message(s) to the destination socket.
template<class ProcessingRules>
class msg_processor {
public:
	/// The processing rules for the particular server instance.
	/**
		These rules specify how an input message should be transformed to one, or more, response messages,
	*/
	using proc_rules_t=ProcessingRules;
	using msg_details_t=typename proc_rules_t::src_msg_details_t;

	/// The buffer into which the incoming messages are received.
	/**
		This should be per-thread, on the stack of the thread, suitably aligned, possibly to 64-bits. For maximum performance it should be a reference to the internal buffer of the network stack or even the hardware buffer in the card (assuming suitable kernel modules).
		It is not recommended to use GCC <=v4.9.* due to sub-optimal code-generation in std::memcpy() (see the performance graphs of libjmmcg::stack_string.
	*/
	using msg_buffer_t=typename msg_details_t::msg_buffer_t;

	explicit msg_processor(proc_rules_t const &proc_rules);

	/// Read a message from the source socket into the destination buffer
	/**
		Synchronously blocks until the entire message is read into the buffer.

		\param src_skt	The connected socket from which to read the message.
		\param buff	Destination buffer for the message, which must be large enough.
		\return False if the operation failed, otherwise true if succeeded.
	*/
	template<class SktT>
	bool read_msg_into_buff(SktT &src_skt, msg_buffer_t &buff) noexcept(false);

	/// Read a message from the source socket, process it and write the resultant message(s) to the destination socket.
	/**
		Note that this call will block until sufficient data has been read from the source socket to comprise a complete source message.

		Throws: an exception if there has been a socket error (the source socket being cleanly closed is not an error).

		\param	src_skt	The socket that is source of the messages.
		\param	dest_skt	The socket that is destination of the messages, may be the same as src_skt.
		\return True if the source socket has been closed, false otherwise.
	*/
	template<class SktT>
	bool read_and_process_a_msg(msg_buffer_t const &buff, SktT &src_skt, SktT &dest_skt, libjmmcg::latency_timestamps_itf &ts) noexcept(false);
	/// Read a message from the source socket, process it and write the resultant message(s) to the destination socket, if it is valid, otherwise write back to the src_skt.
	/**
		Note that this call will block until sufficient data has been read from the source socket to comprise a complete source message.

		Throws: an exception if there has been a socket error (the source socket being cleanly closed is not an error).

		\param	src_skt	The socket that is source of the messages.
		\param	dest_cxn	The connection to the client. This may not yet have occurred, or be occurring.
		\return True if the source socket has been closed, false otherwise.
	*/
	template<class SktT, class ClientCxn>
	bool read_and_process_a_msg(msg_buffer_t const &buff, SktT &src_skt, ClientCxn &dest_cxn, libjmmcg::latency_timestamps_itf &ts) noexcept(false);

	std::string to_string() const noexcept(false);

private:
	proc_rules_t processing_rules;
};

template<class ProcessingRules> inline std::ostream &
operator<<(std::ostream &os, msg_processor<ProcessingRules> const &ec) noexcept(false);

} } }

#include "msg_processor_impl.hpp"

#endif
