/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

template<class KeyType, class MappedType, class ContainerType>
struct flat_map<KeyType, MappedType, ContainerType>::key_compare {
	constexpr bool operator()(value_type const& lhs, value_type const& rhs) const noexcept(noexcept(std::less<typename value_type::first_type>{}(std::declval<typename value_type::first_type>(), std::declval<typename value_type::first_type>()))) {
		return std::less<typename value_type::first_type>{}(lhs.first, rhs.first);
	}
};

template<class KeyType, class MappedType, class ContainerType>
inline constexpr bool
flat_map<KeyType, MappedType, ContainerType>::empty() const noexcept(true) {
	return cont_.empty();
}

template<class KeyType, class MappedType, class ContainerType>
inline constexpr typename flat_map<KeyType, MappedType, ContainerType>::size_type
flat_map<KeyType, MappedType, ContainerType>::size() const noexcept(true) {
	return cont_.size();
}

template<class KeyType, class MappedType, class ContainerType>
inline constexpr typename flat_map<KeyType, MappedType, ContainerType>::size_type
flat_map<KeyType, MappedType, ContainerType>::max_size() noexcept(true) {
	return cont_.max_size();
}

template<class KeyType, class MappedType, class ContainerType>
inline void constexpr flat_map<KeyType, MappedType, ContainerType>::clear() noexcept(true) {
	cont_.clear();
}

template<class KeyType, class MappedType, class ContainerType>
inline typename flat_map<KeyType, MappedType, ContainerType>::mapped_type const&
flat_map<KeyType, MappedType, ContainerType>::at(key_type const& key) const noexcept(false) {
	assert(std::is_sorted(cont_.begin(), cont_.end(), key_compare()));
	const_iterator i= std::lower_bound(
		cont_.begin(),
		cont_.end(),
		key,
		[](value_type const& lhs, key_type const& rhs) {
			return std::less<typename std::common_type<typename value_type::first_type, key_type>::type>{}(lhs.first, rhs);
		});
	if(i != cont_.end()) {
		return i->second;
	} else {
		BOOST_THROW_EXCEPTION(throw_api_exception<std::runtime_error>(fmt::format("Failed to find the specified key. key='{}', ", tostring(key)), &flat_map<KeyType, MappedType, ContainerType>::at));
	}
}

template<class KeyType, class MappedType, class ContainerType>
template<class FwdIter>
inline void
flat_map<KeyType, MappedType, ContainerType>::insert(FwdIter b, FwdIter e) noexcept(false) {
	assert(std::distance(b, e) >= 0);
	cont_.reserve(cont_.size() + static_cast<container_type::size_type>(std::distance(b, e)));
	std::copy(b, e, std::back_inserter(cont_));
	std::sort(cont_.begin(), cont_.end(), key_compare());
}

template<class KeyType, class MappedType, class ContainerType>
template<class Param1, class... Params>
inline void
flat_map<KeyType, MappedType, ContainerType>::insert(Param1 const& arg1, Params const&... args) noexcept(false) {
	cont_.reserve(cont_.size() + 1 + sizeof...(Params));
	cont_.push_back(arg1);
	(cont_.push_back(args), ...);
	std::sort(cont_.begin(), cont_.end(), key_compare());
}

template<class KeyType, class MappedType, class ContainerType>
template<class Param1, class... Params>
inline void
flat_map<KeyType, MappedType, ContainerType>::insert(Param1&& arg1, Params&&... args) noexcept(false) {
	cont_.reserve(cont_.size() + 1 + sizeof...(Params));
	cont_.push_back(std::forward<Param1>(arg1));
	(cont_.push_back(std::forward<Params>(args)), ...);
	std::sort(cont_.begin(), cont_.end(), key_compare());
}

template<class KeyType, class MappedType, class ContainerType>
inline std::ostream&
operator<<(std::ostream& os, flat_map<KeyType, MappedType, ContainerType> const& fm) noexcept(false) {
	for(auto const& v: fm.cont_) {
		os << "(" << v.first << "->" << v.second << "), ";
	}
	return os;
}

}}
