/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <bits/ios_base.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsuggest-override"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// An adaptor for collections that batches up to I items being atomically removed from the collection, Colln.
/**
	This means that as items are removed from the collection, they are potentially added to the last item that was removed from the collection. This implies that the Colln::value_type is some kind of collection to which that work may be added.
*/
template<
	class Colln,	///< The collection to be adapted.
	unsigned long I	///< The maximum number of items per batch.
	>
struct front_batch : public Colln {
	static inline constexpr const unsigned long max_size= I;	  ///< The maximum number of items to batch.
	typedef Colln base_t;
	typedef typename base_t::thread_traits thread_traits;
	typedef Colln container_type;
	typedef typename container_type::value_ret_type value_ret_type;

	static_assert(max_size > 1UL, "Wrong batch size.");

	constexpr front_batch() noexcept(noexcept(base_t()))
		: base_t() {}
	explicit front_batch(typename base_t::have_work_type::atomic_t& a) noexcept(noexcept(base_t(std::declval<base_t>())))
		: base_t(a) {}
	front_batch(front_batch const& fb) noexcept(noexcept(base_t(std::declval<front_batch>())))
		: base_t(fb) {}
	front_batch& __fastcall operator=(front_batch const& fb) noexcept(noexcept(base_t::operator=(std::declval<front_batch>()))) {
		base_t::operator=(fb);
		return *this;
	}

	/// The items are batched when popped from the queue.
	/**
		This is used to return a collection of items from the signalled_work_queue, in the order in which they were inserted. At least one item will be returned, and if there are sufficient items in the signalled_work_queue, then max_size items will be returned. This implies that the thread that extracts items from the queue does the work in batching them.

		\return	A batch of either one or max_size items.
	*/
	value_ret_type __fastcall pop_front_nochk_nolk() noexcept(true);
};

/// Batch-sizes of zero aren't allowed.
template<
	class Colln	  ///< The collection to be adapted.
	>
class front_batch<Colln, 0UL> : public Colln {
};

/// Batch-sizes of zero aren't allowed.
template<
	class Colln	  ///< The collection to be adapted.
	>
struct front_batch<Colln, 1UL> : public Colln {
	static inline constexpr const unsigned long max_size= 1UL;	 ///< The maximum number of items to batch.
	typedef Colln base_t;
	typedef typename base_t::thread_traits thread_traits;
	typedef Colln container_type;
	typedef typename container_type::value_ret_type value_ret_type;

	constexpr front_batch() noexcept(noexcept(base_t()))
		: base_t() {}
	explicit front_batch(typename base_t::have_work_type::atomic_t& a) noexcept(noexcept(base_t(a)))
		: base_t(a) {}
	front_batch(front_batch const& fb) noexcept(noexcept(base_t(std::declval<front_batch>())))
		: base_t(fb) {}
	front_batch& __fastcall operator=(front_batch const& fb) noexcept(noexcept(base_t::operator=(std::declval<front_batch>()))) {
		base_t::operator=(fb);
		return *this;
	}

	value_ret_type __fastcall pop_front_nochk_nolk() noexcept(true);
};

/// An adaptor for collections that batches up to I items being both added to or removed from the collection, Colln.
/**
	This means that as items are added to the collection, they are potentially added to the last item that was added to the collection. This implies that the Colln::value_type is some kind of collection to which the new work may be added.
*/
template<
	class Colln,	///< The collection to be adapted.
	unsigned long I	///< The maximum number of items per batch.
	>
struct back_batch final : public front_batch<Colln, I> {
	typedef front_batch<Colln, I> base_t;
	using base_t::max_size;
	typedef typename base_t::thread_traits thread_traits;
	typedef typename base_t::container_type container_type;
	typedef typename container_type::value_type value_type;

	static_assert(max_size > 1UL, "Wrong batch size.");

	constexpr back_batch() noexcept(noexcept(base_t()))
		: base_t() {}
	explicit back_batch(typename base_t::have_work_type::atomic_t& a) noexcept(noexcept(base_t(a)))
		: base_t(a) {}
	/// The items are batched when pushed onto the queue.
	/**
		The items are batched as they are added to the queue. Therefore the thread that adds the items does the batching work, and that the queue contains a mix of batched and unbatched items, thus potentially reducing the number of items added to the queue, therefore the number of memory allocations done.
	*/
	// TODO Implement this:		void __fastcall push_back(const value_type &data_item) noexcept(false);

	/// The items are batched when pushed onto the queue.
	/**
		The items are batched as they are added to the queue.	Therefore the thread that adds the items does the batching work, and that the queue contains a mix of batched and unbatched items, thus potentially reducing the number of items added to the queue, therefore the number of memory allocations done.
	*/
	// TODO Implement this:		void __fastcall push(const value_type &data_item) noexcept(false);
};

/// Batch-sizes of zero aren't allowed.
template<
	class Colln	  ///< The collection to be adapted.
	>
class back_batch<Colln, 0UL> final : public front_batch<Colln, 0UL> {
};

/// If the batch-size is one, collapse this to an empty wrapper of the collection_type.
template<
	class Colln	  ///< The collection to be adapted.
	>
struct back_batch<Colln, 1UL> final : public front_batch<Colln, 1UL> {
	typedef front_batch<Colln, 1UL> base_t;
	using base_t::max_size;
	typedef typename base_t::thread_traits thread_traits;
	typedef typename base_t::container_type container_type;
	typedef typename container_type::value_type value_type;

	constexpr back_batch() noexcept(noexcept(base_t()))
		: base_t() {}
	explicit back_batch(typename base_t::have_work_type::atomic_t& a) noexcept(noexcept(base_t(a)))
		: base_t(a) {}
	back_batch(back_batch const& bb) noexcept(noexcept(base_t(std::declval<back_batch>())))
		: base_t(bb) {}
	back_batch& __fastcall operator=(back_batch const& bb) noexcept(noexcept(base_t::operator=(std::declval<back_batch>()))) {
		base_t::operator=(bb);
		return *this;
	}

	void __fastcall push_back(const value_type& data_item) noexcept(true);
	void __fastcall push_back(value_type&& data_item) noexcept(true);
	void __fastcall push(const value_type& data_item) noexcept(true);
	void __fastcall push(value_type&& data_item) noexcept(true);
};

}}

#include "batch_impl.hpp"

#pragma GCC diagnostic pop
