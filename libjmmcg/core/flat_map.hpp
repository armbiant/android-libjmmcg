#ifndef LIBJMMCG_CORE_FLAT_MAP_HPP
#define LIBJMMCG_CORE_FLAT_MAP_HPP
/******************************************************************************
** Copyright © 2023 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "exception.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <vector>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

/// An ordered map similar to std::map, but adapts a supplied ContainerType so that it is more cache-efficient.
/**
 * \see std::map
 */
template<
	class KeyType,
	class MappedType,
	class ContainerType= std::vector<std::pair<KeyType, MappedType>>>
class flat_map {
public:
	using container_type= ContainerType;
	using key_type= KeyType;
	using mapped_type= MappedType;
	using value_type= typename ContainerType::value_type;
	using size_type= typename ContainerType::size_type;
	struct key_compare;
	using const_iterator= typename container_type::const_iterator;
	static_assert(std::is_same<key_type, typename value_type::first_type>::value);
	static_assert(std::is_same<mapped_type, typename value_type::second_type>::value);

	constexpr bool empty() const noexcept(true);
	constexpr size_type size() const noexcept(true);
	constexpr size_type max_size() noexcept(true);

	void constexpr clear() noexcept(true);

	/// Find the mapped_type associated with the key.
	/**
		Algorithmic complexity: O(log(n)), where n=size().
		Invariant: !empty()

		\return	The instance of the mapped_type associated with the key. Throws otherwise.
	*/
	mapped_type const& at(key_type const& key) const noexcept(false);

	/// Insert a range into the container_type.
	/**
		Algorithmic complexity: O(nlog(n)), where n=size()+std::distance(b, e).
		Invariant: !empty() iff (size()+std::distance(b, e))>0

		\Note: the performance of this function is sacrified to improve the performance of at().

		\see at()
	*/
	template<class FwdIter>
	void insert(FwdIter b, FwdIter e) noexcept(false);
	/// Insert a range into the container_type.
	/**
		Algorithmic complexity: O(nlog(n)), where n=size()+1+sizeof...(Params).
		Invariant: !empty()

		\Note: the performance of this function is sacrified to improve the performance of at().

		\see at()
	*/
	template<class Param1, class... Params>
	void insert(Param1 const& arg1, Params const&... args) noexcept(false);
	/// Insert a range into the container_type.
	/**
		Algorithmic complexity: O(nlog(n)), where n=size()+1+sizeof...(Params).
		Invariant: !empty()

		\Note: the performance of this function is sacrified to improve the performance of at().

		\see at()
	*/
	template<class Param1, class... Params>
	void insert(Param1&& arg1, Params&&... args) noexcept(false);

	template<class KeyType1, class MappedType1, class ContainerType1>
	friend std::ostream& operator<<(std::ostream& os, flat_map<KeyType1, MappedType1, ContainerType1> const& fm) noexcept(false);

private:
	container_type cont_{};
};

}}

#include "flat_map_impl.hpp"

#endif
