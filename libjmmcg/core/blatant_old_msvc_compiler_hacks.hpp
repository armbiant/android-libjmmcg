#ifndef LIBJMMCG_CORE_BLATANT_OLD_MSVC_COMPILER_HACKS_HPP
#define LIBJMMCG_CORE_BLATANT_OLD_MSVC_COMPILER_HACKS_HPP

/******************************************************************************
** Copyright © 2002 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "core_config.h"

#include <cstddef>
#include <new>

/**
	\mainpage
	\section Overview
	See <a href="http://libjmmcg.sf.net/">the web site</a> for an overview and previously-given presentations regarding the library.
*/

/** \file
	This file contains some of the more blatant, nasty, evil hacks that need
	to be done to work-around the venerable MSVC++ v6.0 sp5 compiler's "features".
	Note that this compiler should really be referred to as "cl.exe" v12.00.8168.
*/

/// Work arounds for the older MSVC++ compilers being very broken regarding the use of various keywords.
#define JMMCG_MSVC_STDCALL_HACK __stdcall
#define JMMCG_MSVC_FASTCALL_HACK __fastcall

#ifndef _CRT_SECURE_NO_WARNINGS
#	define _CRT_SECURE_NO_WARNINGS
#endif
#ifndef _WIN32_WINNT
#	define _WIN32_WINNT 0x0501
#endif
#ifndef WINVER
#	define WINVER _WIN32_WINNT
#endif

#if defined(_MSC_VER) && (_MSC_VER < 1300)
#	define __FUNCTION__ _T("Message to M$: you should have defined __FUNCTION__!")
/// Hacks to get round MS macros in "windef.h" that conflict with the STL definitions of min() and max().
#	undef min
#	undef max
#	ifndef JMMCG_MS_MINMAX_HACK
#		define JMMCG_MS_MINMAX_HACK
#		define NOMINMAX
namespace std {
template<class _Ty>
inline const _Ty& __fastcall max(const _Ty& _X, const _Ty& _Y) {
	return _cpp_max(_X, _Y);
}
template<class _Ty, class _Pr>
inline const _Ty& __fastcall max(const _Ty& _X, const _Ty& _Y, _Pr _P) {
	return _cpp_max(_X, _Y, _P);
}

template<class _Ty>
inline const _Ty& __fastcall min(const _Ty& _X, const _Ty& _Y) {
	return _cpp_min(_X, _Y);
}
template<class _Ty, class _Pr>
inline const _Ty& __fastcall min(const _Ty& _X, const _Ty& _Y, _Pr _P) {
	return _cpp_min(_X, _Y, _P);
}
}
#	endif
#	if(_MSC_VER < 1300)
#		undef JMMCG_MSVC_FASTCALL_HACK
#		define JMMCG_MSVC_FASTCALL_HACK
#		undef JMMCG_MSVC_STDCALL_HACK
#		define JMMCG_MSVC_STDCALL_HACK
#	endif
#endif

#ifdef _MSC_VER
/// Get rid of stupid MSVC warnings.
#	pragma warning(disable:4033)	  ///< Conversion from OMF to COFF warning.
#	pragma warning(disable:4256)	  ///< constructor for class with virtual bases has '...'; calls may not be compatible with older versions of Visual C++
#	pragma warning(disable:4290)	  ///< C++ Exception Specification ignored.
#	pragma warning(disable:4503)	  ///< Decorated name length exceeded, name was truncated.
#	pragma warning(disable:4710)	  ///< function '...' not inlined
#	pragma warning(disable:4711)	  ///< function '...' selected for automatic inline expansion
#	pragma warning(disable:4725)	  ///< Pentium fdiv warning.
#	pragma warning(disable:4786)	  ///< "identifier was truncated to '255' characters in the debug information", (from MSVC++ v5.0 help).
#	pragma warning(disable:4800)	  ///< forcing value to bool 'true' or 'false' (performance warning).

#	define _CRT_SECURE_NO_WARNINGS	 ///< This function or variable may be unsafe.
#	define _SCL_SECURE_NO_WARNINGS	 ///< Function call with parameters that may be unsafe.

#	define __PRETTY_FUNCTION__ __FUNCSIG__

typedef __int16 int16_t;

#	if defined(_MT) && !defined(_REENTRANT)
#		define _REETRANT
#	endif

#	define NEVER_INLINE

#	define POPCOUNTLL(x) __popcnt64(x)

/**
	Include "intrin.h" for this intrinsic. See <a href="https://msdn.microsoft.com/en-us/library/hh977023.aspx">MSDN Intrinsics</a> for more information.

	\param addr	Prefetch a cache line from the address addr for reading.
	\param locality	Not used.
*/
#	define PREFETCH_READ(addr, locality) _m_prefetch(reinterpret_cast<void*>(addr))

/**
	Include "intrin.h" for this intrinsic. See <a href="https://msdn.microsoft.com/en-us/library/hh977023.aspx">MSDN Intrinsics</a> for more information.

	\param addr	Prefetch a cache line from the address addr for writing.
	\param locality	Not used.
*/
#	define PREFETCH_WRITE(addr, locality) _m_prefetchw(reinterpret_cast<void*>(addr))

#else
// Ensure that other 32-bit compilers can safely ignore unnecessary requirement in MSVC++-world for declaring functions with efficient calling conventions.

#	if defined(i386) || defined(__i386__) || defined(__x86_64__)
#		include <x86intrin.h>
#	endif

#	ifdef __GNU__
/// This is the general calling convention: use it anywhere.
#		ifndef __fastcall
#			define __fastcall [[gnu::regparm(3)]]
#		endif

/// Calling convention necessary for constructors/destructors in MSVC.
#		ifndef __stdcall
#			define __stdcall [[gnu::regparm(3)]]
#		endif

/// In MSVC++, the standard, slowest calling convention, used only for "main()" and other required places, e.g. var-arg functions.
#		ifndef __cdecl
#			define __cdecl [[gnu::cdecl]]
#		endif

#		define __CLR_OR_THIS_CALL [[gnu::regparm(3)]]
#	else
#		ifndef __fastcall
#			define __fastcall
#		endif

#		ifndef __stdcall
#			define __stdcall
#		endif

#		ifndef __cdecl
#			define __cdecl
#		endif

#		define __CLR_OR_THIS_CALL
#	endif

#	define __declspec(dllexport)

/**
 * Note that this can cause the compiler to take 100x longer...! And generate *HUGE* images. And create really slow code! BEWARE!
 */
#	define REALLY_FORCE_INLINE [[gnu::hot, gnu::always_inline]]
#	define NEVER_INLINE [[gnu::cold, gnu::noinline]]

/// Yup: MSVC++ doesn't support "long long" properly.... Better use the MSVC++-special types.
using __int64= long long;
using __uint64= unsigned long long;

#	define POPCOUNTLL(x) __builtin_popcountll(x)

/**
	See <a href="https://gcc.gnu.org/onlinedocs/gcc-3.3.6/gcc/Other-Builtins.html">GCC Intrinsics</a> and <a href="http://tomoyo.osdn.jp/cgi-bin/lxr/source/include/linux/prefetch.h">Prefetching strides</a> for more information.

	\param addr	Prefetch a cache line from the address addr for reading. See the kernel marco L1_CACHE_BYTES for an approximation.
	\param locality	A compile-time integer constant in [1 .. 3], default 3. 1 means low locality, 3 highest.
*/
#	define PREFETCH_READ(addr, locality) __builtin_prefetch(reinterpret_cast<void const*>(addr), 0, (locality))

#	ifndef _MM_HINT_NTA
// May be on ARM...
#		define _MM_HINT_NTA 0
#	endif
/**
	See <a href="https://gcc.gnu.org/onlinedocs/gcc-3.3.6/gcc/Other-Builtins.html">GCC Intrinsics</a> and <a href="http://tomoyo.osdn.jp/cgi-bin/lxr/source/include/linux/prefetch.h">Prefetching strides</a> for more information.

	\param addr	Prefetch a cache line from the address addr for writing. See the kernel marco L1_CACHE_BYTES for an approximation.
	\param locality	A compile-time integer constant in [1 .. 3], default 3. 1 means low locality, 3 highest.
*/
#	define PREFETCH_WRITE(addr, locality) __builtin_prefetch(reinterpret_cast<void const*>(addr), 1, (locality))

#endif

#ifdef __clang__
namespace std {
/**
 * 64 bytes on x86-64 │ L1_CACHE_BYTES │ L1_CACHE_SHIFT │ __cacheline_aligned │ ...
 *
 * \todo I'm getting cheesed off with g++ (<=v10.2.0) & clang++ (<=v11.1.0) lack of support for these trivial objects... Just f'ing shove them into the std namespace and once the compilers eventually pull their socks up, I'll get an error and can delete this...
 */
inline constexpr const std::size_t hardware_constructive_interference_size= 2 * sizeof(std::max_align_t);
/**
 * 64 bytes on x86-64 │ L1_CACHE_BYTES │ L1_CACHE_SHIFT │ __cacheline_aligned │ ...
 *
 * \todo I'm getting cheesed off with g++ (<=v10.2.0) & clang++ (<=v11.1.0) lack of support for these trivial objects... Just f'ing shove them into the std namespace and once the compilers eventually pull their socks up, I'll get an error and can delete this...
 */
inline constexpr const std::size_t hardware_destructive_interference_size= 2 * sizeof(max_align_t);
}
#endif
#define ALIGN_TO_L1_CACHE alignas(std::hardware_constructive_interference_size)

#endif
