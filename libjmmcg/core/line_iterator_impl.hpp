/******************************************************************************
** Copyright © 2020 by J.M.McGuiness, coder@hussar.me.uk & M.Waplington
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE {

inline
line_iterator::line_iterator(std::istream &s, std::string::value_type const delimiter) noexcept(false)
: in_(&s),
	delimiter_(delimiter) {
	read();
}

inline line_iterator::reference
line_iterator::operator*() const noexcept(true) {
	return line_;
}

inline line_iterator::pointer
line_iterator::operator->() const noexcept(true) {
	return &line_;
}

inline line_iterator
line_iterator::operator++() noexcept(false) {
	read();
	return *this;
}

inline line_iterator
line_iterator::operator++(int) noexcept(false) {
	line_iterator tmp=*this;
	read();
	return tmp;
}

inline bool
line_iterator::operator==(line_iterator const& i) const noexcept(true) {
	return (in_==i.in_ && valid_==i.valid_)
		|| (!valid_ && !i.valid_);
}

inline bool
line_iterator::operator!=(line_iterator const& i) const noexcept(true) {
	return !(*this==i);
}

inline void
line_iterator::read() noexcept(false) {
	if (in_) {
		std::getline(*in_, line_, delimiter_);
	}
	valid_=static_cast<bool>(*in_);
}

} }
