#ifndef LIBJMMCG_CORE_SOCKET_SERVER_MANAGER_HPP
#define LIBJMMCG_CORE_SOCKET_SERVER_MANAGER_HPP

/******************************************************************************
** Copyright © 2015 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "msg_processor.hpp"
#include "socket_server_manager_asio.hpp"
#include "socket_server_manager_glibc.hpp"
#include "thread_api_traits.hpp"

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace socket { namespace server_manager {

/// A simple TCP/IP socket wrapper using a wrapped socket for servers.
template<class SktT>
class manager;
template<class LkT>
class manager<socket::asio::socket_wrapper<LkT>> : public asio::manager<LkT> {
public:
	using base_t=asio::manager<LkT>;
	using session=typename base_t::session;
	using socket_t=typename base_t::socket_t;
	using base_t::manager;
};
template<class LkT>
class manager<socket::glibc::client::wrapper<LkT>> : public glibc::manager<LkT> {
public:
	using base_t=glibc::manager<LkT>;
	using session=typename base_t::session;
	using socket_t=typename base_t::socket_t;
	using base_t::manager;
};

/// A simple TCP/IP socket wrapper using wrapped socket for loop-back servers.
template<class ProcessingRules, class SvrHBs, class SktT>
class loopback;
template<class ProcessingRules, class SvrHBs, class LkT>
class loopback<ProcessingRules, SvrHBs, socket::asio::socket_wrapper<LkT>> final : public asio::loopback<ProcessingRules, SvrHBs, LkT> {
public:
	using base_t=asio::loopback<ProcessingRules, SvrHBs, LkT>;
	using server_to_client_flow_t=typename base_t::server_to_client_flow_t;
	using socket_t=typename base_t::socket_t;
	using proc_rules_t=typename base_t::proc_rules_t;
	using heartbeats_t=typename base_t::heartbeats_t;
	using base_t::loopback;
};
template<class ProcessingRules, class SvrHBs, class LkT>
class loopback<ProcessingRules, SvrHBs, socket::glibc::client::wrapper<LkT>> final : public glibc::loopback<ProcessingRules, SvrHBs, LkT> {
public:
	using base_t=glibc::loopback<ProcessingRules, SvrHBs, LkT>;
	using server_to_client_flow_t=typename base_t::server_to_client_flow_t;
	using socket_t=typename base_t::socket_t;
	using proc_rules_t=typename base_t::proc_rules_t;
	using heartbeats_t=typename base_t::heartbeats_t;
	using base_t::loopback;
};

/// A simple TCP/IP socket wrapper using a wrapped socket for forwarding servers.
template<class ExchgCxns, class SktT>
class forwarding;
template<class ExchgCxns, class LkT>
class forwarding<ExchgCxns, socket::asio::socket_wrapper<LkT>> final : public asio::forwarding<ExchgCxns, LkT> {
public:
	using base_t=asio::forwarding<ExchgCxns, LkT>;
	using server_to_client_flow_t=typename base_t::server_to_client_flow_t;
	using socket_t=typename base_t::socket_t;
	using proc_rules_t=typename base_t::proc_rules_t;
	using base_t::forwarding;
};
template<class ExchgCxns, class LkT>
class forwarding<ExchgCxns, socket::glibc::client::wrapper<LkT>> final : public glibc::forwarding<ExchgCxns, LkT> {
public:
	using base_t=glibc::forwarding<ExchgCxns, LkT>;
	using server_to_client_flow_t=typename base_t::server_to_client_flow_t;
	using socket_t=typename base_t::socket_t;
	using proc_rules_t=typename base_t::proc_rules_t;
	using base_t::forwarding;
};

} } } }

#endif
