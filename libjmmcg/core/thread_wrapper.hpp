#ifndef LIBJMMCG_CORE_THREAD_WRAPPER_HPP
#define LIBJMMCG_CORE_THREAD_WRAPPER_HPP

/******************************************************************************
** Copyright © 2004 by J.M.McGuiness, coder@hussar.me.uk
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
	\file
	Note that this file should be included after any MFC header files, or you'll get an error about WINDOWS.H being included.
*/

#include "private_/thread_base.hpp"

#include <functional>

namespace jmmcg { namespace LIBJMMCG_VER_NAMESPACE { namespace ppd {

/// A handy utility class for the wrapper class, that is supposed to do nothing.
/**
	The idea is that you could have an automatic instance of this object that is in the scope of the worker_fn() method, is automatically instantiated in the process() method and released upon exit of the process() method.

	\see wrapper
*/
struct null_thread_worker_context {
	// Does nothing! No context for the worker thread.

	using thread_t= private_::thread_base<platform_api, heavyweight_threading>;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= ppd::generic_traits::memory_access_modes::crew_memory_access;

	/// This function is run once, after an instance of the thread has been created, but before the mutator is run.
	/**
	 * Note that for the null_thread_worker_context, this is never called. If the function is omitted, likewise it is not an error. It is only called if declared, adefined and accessible.
	 *
	 * \param thread	A reference to the wrapper for the thread.
	 */
	void operator()(thread_t&) {}
};

/// This class is used to implement a class that wraps using a thread, it is an alternative to using a thread pool to manage your threads.
/**
	Note that the most derived classes' dtor must call private_::thread_base::wait_thread_exit() in it's dtor to ensure that the thread goes out of scope before the object upon which it relies.
	This class does not allocate any memory, unlike std::thread or std::jthread.

	\see ppd::jthread, private_::thread_base, std::thread, std::jthread
 */
template<
	class Mutator,
	generic_traits::api_type::element_type API,
	class Mdl,
	class TWC= null_thread_worker_context	 ///< This now allows for the worker thread to implement worker-thread specific-context, e.g. COM initialisation for apartment threading or setting the thread name or priority! (As the object here is constructed on the stack and within the context of the worker thread in the process() method, but before the worker_fn() method is called. This object must have a default constructor. Thanks to George Velimachitis (he suggested the context template idea)!
	>
class wrapper final : public private_::thread_base<API, Mdl> {
public:
	using base_t= private_::thread_base<API, Mdl>;
	using mutator_t= Mutator;
	using thread_context_t= TWC;
	using lock_traits= typename base_t::lock_traits;
	using thread_traits= typename base_t::thread_traits;
	using os_traits= typename base_t::os_traits;

	/**
		To assist in allowing compile-time computation of the algorithmic order of the threading model.
	*/
	static inline constexpr const ppd::generic_traits::memory_access_modes::element_type memory_access_mode= (base_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && mutator_t::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																						 && thread_context_t ::memory_access_mode == ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 ? ppd::generic_traits::memory_access_modes::crew_memory_access
																																					 : ppd::generic_traits::memory_access_modes::erew_memory_access);
	static inline constexpr typename thread_traits::api_params_type::suspend_period_ms exit_wait= 50;

	template<class... MutatorCtorArgs>
	[[nodiscard]] explicit __stdcall wrapper(MutatorCtorArgs&&... args) noexcept(false);

	template<class... MutatorCtorArgs>
	[[nodiscard]] explicit __stdcall wrapper(thread_context_t&& thread_context, MutatorCtorArgs&&... args) noexcept(false);

	/// Create the wrapper object. Note that the underlying kernel thread will not have been created.
	/**
		\see base_t::create_running()
	*/
	[[nodiscard]] __stdcall wrapper(const wrapper&) noexcept(true);

	__stdcall ~wrapper() noexcept(false) override;

	wrapper(wrapper&&)= delete;
	void operator=(wrapper const&)= delete;
	void operator=(wrapper&&)= delete;

	mutator_t const* operator->() const noexcept(true);
	mutator_t* operator->() noexcept(true);
	mutator_t const& operator*() const noexcept(true);
	mutator_t& operator*() noexcept(true);

	void __fastcall request_exit() const noexcept(true) override;

private:
	/**
		\todo Consider moving this event into the stack of process() as an automatic variable in there. This may be useful in implementing detached threads. Detached threads may be useful in improving performance of horizontal threading, because the work_complete() lock in them won't have to wait for non-dependent work to complete before it returns.
	*/
	mutable std::atomic_flag exit_requested{};
	thread_context_t thread_context_;
	mutator_t mutator_;

	/// The method used to determine if there has been a request for the thread to exit, and therefore finish processing work, called before attempting to process() a work item.
	/**
		\return true if an exit from the thread has been requested, otherwise false to continue processing.
	*/
	[[nodiscard]] bool __fastcall pre_exit() noexcept(false);

	/// Override this function is you want to do some other work to signal the exit of the thread.
	/**
	 *			This is primarily used by the destructor to politely tell the thread function to quit. Note that if you override this function you must still implement the exit functionality, otherwise the destructor will time-out and terminate the worker thread.
	 */
	[[nodiscard]] typename thread_traits::api_params_type::states::element_type __fastcall process() noexcept(false) override;
};

}}}

#include "thread_wrapper_impl.hpp"

#endif
